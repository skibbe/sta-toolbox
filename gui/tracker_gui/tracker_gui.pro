#-------------------------------------------------
#
# Project created by QtCreator 2015-04-08T15:13:04
#
#-------------------------------------------------

QT       += widgets opengl

TARGET = tracker_gui
TEMPLATE = lib
#CONFIG += staticlib


DEFINES += TRACKER_GUI_LIBRARY

SOURCES += tracker_gui.cpp

HEADERS += tracker_gui.h\
        tracker_gui_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
