#ifndef TRACKER_GUI_GLOBAL_H
#define TRACKER_GUI_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QtGui>
#include <QWidget>
#include <QApplication>

#if defined(TRACKER_GUI_LIBRARY)
#  define TRACKER_GUISHARED_EXPORT Q_DECL_EXPORT
#else
#  define TRACKER_GUISHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // TRACKER_GUI_GLOBAL_H
