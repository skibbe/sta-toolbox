#ifndef TRACKER_GUI_H
#define TRACKER_GUI_H


#include "tracker_gui_global.h"


class Tracker_mainwin : public QWidget
{
Q_OBJECT

    public:
    Tracker_mainwin ();
};


class TRACKER_GUISHARED_EXPORT Tracker_gui
{
private:
QApplication * a;
public:
    Tracker_gui();
    ~Tracker_gui();
    int run();
    void stop();
};

#endif // TRACKER_GUI_H
