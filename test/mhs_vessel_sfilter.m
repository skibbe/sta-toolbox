%
%
% [V,VData]=mhs_vesselHoughVote(Img,'scale_range',[1,8],'gscale',-1,'sigman_grad',-1,'sigma_r',0.5,'tube_steps',3,'tube_length',2,'STD',false,'global_grad',true);
% VRec=mhs_FilterBackproj(VData);
%
function [V1,V2]=mhs_vessel_sfilter(img,varargin)

crop=[0,0,0];
STD=false;
nscales=5;         
scale_range=[1.25,8];
sigman=-1;
epsilon=0.01;
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
if ~exist('Scales')
    Scales_fac=(scale_range(2)/scale_range(1))^(1/(nscales-1));
    Scales=scale_range(1);
    for a=2:nscales
        Scales(a)=Scales(a-1)*Scales_fac;
    end;
end;
    
classid=class(img);
    
 shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);    
    

figure(1238);
fcount=1;
clf;

numscales=numel(Scales);

V1=zeros([numscales,cshape]);
V2=zeros([5,cshape]);

if STD
    if (sigman==-1)
        sigman=1.5*max(Scales);
    end;
    mean=mhs_smooth_img(img,sigman,'normalize',true);
    mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
    std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
    std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
end;  


for s=1:numel(Scales)
    scale=Scales(s);
    

    if STD
        HField=scale^2*reshape(repmat(std_dev(:)',6,1),[6,cshape]).*Hessian(img,scale,'crop',crop,'normalize',true);
    else
        HField=scale^2*Hessian(img,scale,'crop',crop,'normalize',true);        
    end;
    Lap=(sum(HField(1:3,:,:,:),1));
    HField(1,:,:,:)=HField(1,:,:,:)-Lap;
    HField(2,:,:,:)=HField(2,:,:,:)-Lap;
    HField(3,:,:,:)=HField(3,:,:,:)-Lap;    
    
    [myevec_main,myev_main]=(sta_EVGSL(HField));
     
    vote=squeeze(myev_main(1,:,:,:));
    
    V1(s,:)=vote(:);
    D=3;
    figure(1238);
    subplot(1,numel(Scales),fcount);imagesc(squeeze(max(vote,[],D)));fcount=fcount+1;colorbar;
    drawnow
    
    DirImg=myevec_main([1,2,3],:,:,:);
    DirImg=reshape(DirImg,[3,1,size(img)]);
    
    if s>1
        sindx=squeeze((V1(s-1,:)<V1(s,:)));
        V2(1:3,sindx)=DirImg(:,sindx);
        V2(4,sindx)=s;
        V2(5,sindx)=vote(sindx);
    else
        V2(1:3,:)=DirImg(:,:);
        V2(4,:)=s;
        V2(5,:)=vote(:);
    end;
end;    
