#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_float.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_multifit_nlin.h>


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_vector.h"
#include "mhs_error.h"




template <typename T>
T eval_polynom1(Vector<T,1>& vector,
	       const T * alphas,
	       int mode=2)
{
  T r;

const   T * & a=alphas;
const T & x1=vector.v[0];

switch (mode)
{
  
  
   case 2: // 4
  {
    T x2=x1*x1;
    r=a[0]+a[1]*x1+a[2]*x2;
  }
  break;

   case 4: // 5
  {
    T x2=x1*x1;
    T x3=x2*x1;
    T x4=x3*x1;
    

r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3+a[4]*x4;
  }
  break;
  
}
  
  return r;
}


template <typename T>
T eval_polynom2(Vector<T,2>& vector,
	       const T * alphas,
	       int mode=4)
{
  T r;

const   T * & a=alphas;
const T & x1=vector.v[0];
const T & y1=vector.v[1];

switch (mode)
{
  
  
   case 2: // 4
  {
    T x2=x1*x1;
    T y2=y1*y1;

    r=a[0]+a[1]*x2+a[2]*x1*y1+a[3]*y2;  
  }
  break;
  
  case 4: //9
  {
    T x2=x1*x1;
    T x3=x2*x1;
    T x4=x3*x1;
    T y2=y1*y1;
    T y3=y2*y1;
    T y4=y3*y1;

    r=a[0]+a[1]*x2+a[2]*x4+a[3]*x1*y1+a[4]*x3*y1+a[5]*y2+a[6]*x2*y2+a[7]*x1*y3+a[8]*y4;
  }
  break;
  
  case 6: //16
  {
    T x2=x1*x1;
    T x3=x2*x1;
    T x4=x3*x1;
    T x5=x4*x1;
    T x6=x5*x1;
    T y2=y1*y1;
    T y3=y2*y1;
    T y4=y3*y1;
    T y5=y4*y1;
    T y6=y5*y1;

    r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x1*y1+a[5]*x3*y1+a[6]*x5*y1+a[7]*y2+a[8]*x2*y2+a[9]*x4*y2+a[10]*x1*y3+a[11]*x3*y3+a[12]*y4+a[13]*x2*y4+a[14]*x1*y5+a[15]*y6;
  }
  break;
  
  case 8: //25 
  {
    T x2=x1*x1;
    T x3=x2*x1;
    T x4=x3*x1;
    T x5=x4*x1;
    T x6=x5*x1;
    T x7=x6*x1;
    T x8=x7*x1;
    T y2=y1*y1;
    T y3=y2*y1;
    T y4=y3*y1;
    T y5=y4*y1;
    T y6=y5*y1;
    T y7=y6*y1;
    T y8=y7*y1;

    r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x8+a[5]*x1*y1+a[6]*x3*y1+a[7]*x5*y1+a[8]*x7*y1+a[9]*y2+a[10]*x2*y2+a[11]*x4*y2+a[12]*x6*y2+a[13]*x1*y3+a[14]*x3*y3+a[15]*x5*y3+a[16]*y4+a[17]*x2*y4+a[18]*x4*y4+a[19]*x1*y5+a[20]*x3*y5+a[21]*y6+a[22]*x2*y6+a[23]*x1*y7+a[24]*y8;
  }
  break;
}
  
  return r;
}




template <typename T>
T eval_polynom3(Vector<T,3>& vector,
	       const T * alphas,
	       int mode=4)
{
  T r;

const   T * & a=alphas;
const T & x1=vector.v[0];
const T & y1=vector.v[1];
const T & z1=vector.v[2];

switch (mode)
{
  
  case 2:
  {
   
    T x2=x1*x1;
    T y2=y1*y1;
    T z2=z1*z1;

    r=a[0]+a[1]*x2+a[2]*x1*y1+a[3]*y2+a[4]*x1*z1+a[5]*y1*z1+a[6]*z2; 
    
  }
  
   case 4: // 4
  {
    T x2=x1*x1;
    T x3=x2*x1;
    T x4=x3*x1;
    T y2=y1*y1;
    T y3=y2*y1;
    T y4=y3*y1;
    T z2=z1*z1;
    T z3=z2*z1;
    T z4=z3*z1;

    r=a[0]+a[1]*x2+a[2]*x4+a[3]*x1*y1+a[4]*x3*y1+a[5]*y2+a[6]*x2*y2+a[7]*x1*y3+a[8]*y4+a[9]*x1*z1+a[10]*x3*z1+a[11]*y1*z1+a[12]*x2*y1*z1+a[13]*x1*y2*z1+a[14]*y3*z1+a[15]*z2+a[16]*x2*z2+a[17]*x1*y1*z2+a[18]*y2*z2+a[19]*x1*z3+a[20]*y1*z3+a[21]*z4;
    
  }
  break;
  
   case 6: // 4
  {  
    T x2=x1*x1;
    T x3=x2*x1;
    T x4=x3*x1;
    T x5=x4*x1;
    T x6=x5*x1;
    T y2=y1*y1;
    T y3=y2*y1;
    T y4=y3*y1;
    T y5=y4*y1;
    T y6=y5*y1;
    T z2=z1*z1;
    T z3=z2*z1;
    T z4=z3*z1;
    T z5=z4*z1;
    T z6=z5*z1;

    r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x1*y1+a[5]*x3*y1+a[6]*x5*y1+a[7]*y2+a[8]*x2*y2+a[9]*x4*y2+a[10]*x1*y3+a[11]*x3*y3+a[12]*y4+a[13]*x2*y4+a[14]*x1*y5+a[15]*y6+a[16]*x1*z1+a[17]*x3*z1+a[18]*x5*z1+a[19]*y1*z1+a[20]*x2*y1*z1+a[21]*x4*y1*z1+a[22]*x1*y2*z1+a[23]*x3*y2*z1+a[24]*y3*z1+a[25]*x2*y3*z1+a[26]*x1*y4*z1+a[27]*y5*z1+a[28]*z2+a[29]*x2*z2+a[30]*x4*z2+a[31]*x1*y1*z2+a[32]*x3*y1*z2+a[33]*y2*z2+a[34]*x2*y2*z2+a[35]*x1*y3*z2+a[36]*y4*z2+a[37]*x1*z3+a[38]*x3*z3+a[39]*y1*z3+a[40]*x2*y1*z3+a[41]*x1*y2*z3+a[42]*y3*z3+a[43]*z4+a[44]*x2*z4+a[45]*x1*y1*z4+a[46]*y2*z4+a[47]*x1*z5+a[48]*y1*z5+a[49]*z6;
    
}
  break;
  
  
   case 8: // 4
  {    
T x2=x1*x1;
T x3=x2*x1;
T x4=x3*x1;
T x5=x4*x1;
T x6=x5*x1;
T x7=x6*x1;
T x8=x7*x1;
T y2=y1*y1;
T y3=y2*y1;
T y4=y3*y1;
T y5=y4*y1;
T y6=y5*y1;
T y7=y6*y1;
T y8=y7*y1;
T z2=z1*z1;
T z3=z2*z1;
T z4=z3*z1;
T z5=z4*z1;
T z6=z5*z1;
T z7=z6*z1;
T z8=z7*z1;

r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x8+a[5]*x1*y1+a[6]*x3*y1+a[7]*x5*y1+a[8]*x7*y1+a[9]*y2+a[10]*x2*y2+a[11]*x4*y2+a[12]*x6*y2+a[13]*x1*y3+a[14]*x3*y3+a[15]*x5*y3+a[16]*y4+a[17]*x2*y4+a[18]*x4*y4+a[19]*x1*y5+a[20]*x3*y5+a[21]*y6+a[22]*x2*y6+a[23]*x1*y7+a[24]*y8+a[25]*x1*z1+a[26]*x3*z1+a[27]*x5*z1+a[28]*x7*z1+a[29]*y1*z1+a[30]*x2*y1*z1+a[31]*x4*y1*z1+a[32]*x6*y1*z1+a[33]*x1*y2*z1+a[34]*x3*y2*z1+a[35]*x5*y2*z1+a[36]*y3*z1+a[37]*x2*y3*z1+a[38]*x4*y3*z1+a[39]*x1*y4*z1+a[40]*x3*y4*z1+a[41]*y5*z1+a[42]*x2*y5*z1+a[43]*x1*y6*z1+a[44]*y7*z1+a[45]*z2+a[46]*x2*z2+a[47]*x4*z2+a[48]*x6*z2+a[49]*x1*y1*z2+a[50]*x3*y1*z2+a[51]*x5*y1*z2+a[52]*y2*z2+a[53]*x2*y2*z2+a[54]*x4*y2*z2+a[55]*x1*y3*z2+a[56]*x3*y3*z2+a[57]*y4*z2+a[58]*x2*y4*z2+a[59]*x1*y5*z2+a[60]*y6*z2+a[61]*x1*z3+a[62]*x3*z3+a[63]*x5*z3+a[64]*y1*z3+a[65]*x2*y1*z3+a[66]*x4*y1*z3+a[67]*x1*y2*z3+a[68]*x3*y2*z3+a[69]*y3*z3+a[70]*x2*y3*z3+a[71]*x1*y4*z3+a[72]*y5*z3+a[73]*z4+a[74]*x2*z4+a[75]*x4*z4+a[76]*x1*y1*z4+a[77]*x3*y1*z4+a[78]*y2*z4+a[79]*x2*y2*z4+a[80]*x1*y3*z4+a[81]*y4*z4+a[82]*x1*z5+a[83]*x3*z5+a[84]*y1*z5+a[85]*x2*y1*z5+a[86]*x1*y2*z5+a[87]*y3*z5+a[88]*z6+a[89]*x2*z6+a[90]*x1*y1*z6+a[91]*y2*z6+a[92]*x1*z7+a[93]*y1*z7+a[94]*z8;
  }
  break;
  

}



  return r;
}


















template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    std::vector<T> vec;
    vec.resize(2);
    vec[0]=1;
    vec[1]=1;
  
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"vec")!=-1)
            vec=mhs::mex_getParam<T>(params,"vec");
    }
    

    
    //mhs::dataArray<T> img=mhs::dataArray<T>(prhs[0]);
    mhs::dataArray<T> alphas=mhs::dataArray<T>(prhs[0]);

    int stride=alphas.dim[alphas.dim.size()-1];
    
    int numvoxel=1;
    for (int i=0;i<alphas.dim.size()-1;i++)
      numvoxel*=alphas.dim[i];
    
    printf("stride: %d\n",stride);
    printf("numvoxel: %d\n",numvoxel);
  
     int ndims[2];
     ndims[0]=1;
     ndims[1]=numvoxel;
     
     plhs[0] = mxCreateNumericArray(2,ndims,mxGetClassID(prhs[0]),mxREAL);
     T *result = (T*) mxGetData(plhs[0]); 
     
     switch (vec.size())
     {
       case 1:
       {
	    Vector<T,1> vector;
	    vector[0]=vec[0];     
	    
	    int mode=-1;
	    switch (stride)
	    {
	      case 3:
		mode=2;
		break;
	      case 5:
		mode=4;
		break;          		
	    }
	    
	    printf("v: %f %f\n",vector[0],vector[1]);
	    for (int i=0;i<numvoxel;i++)
	    {
	      *result++=eval_polynom1(vector,alphas.data+stride*i,mode);
	    }
       } break;       
       
       
       case 2:
       {
	    Vector<T,2> vector;
	    vector[0]=vec[0];
	    vector[1]=vec[1];     
	    
	    int mode=-1;
	    switch (stride)
	    {
	      case 4:
		mode=2;
		break;          
	      
	      case 9:
		mode=4;
		break;       	 
		
	      case 16:
		mode=6;
		break;       
	      
	      case 25:
		mode=8;
		break;
	    }
	    
	    
	    for (int i=0;i<numvoxel;i++)
	    {
	      *result++=eval_polynom2(vector,alphas.data+stride*i,mode);
	    }
       } break;
       
       case 3:
       {
	    Vector<T,3> vector;
	    vector[0]=vec[0];
	    vector[1]=vec[1];     
	    vector[2]=vec[2];     
	    vector.print();
	    int mode=-1;
	    switch (stride)
	    {
	       case 7:
		mode=2;
		break;   
	      
	      case 22:
		mode=4;
		break;       
	      case 50:
		mode=6;
		break; 		
	      case 95:
		mode=8;
		break; 		
			
	      
	    }
	    
	    
	    for (int i=0;i<numvoxel;i++)
	    {
	      *result++=eval_polynom3(vector,alphas.data+stride*i,mode);
	    }
       } break;
	
     }
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<2)
        mexErrMsgTxt("error: nrhs<2\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}