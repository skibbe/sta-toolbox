%
%
% [V,VData]=mhs_vessel_frangi(Img,'scale_range',[1.25,8],'STD',true,'filter_type','steer');
%
% [V,VData]=mhs_vessel_frangi(Img,'scale_range',[1.25,8],'STD',false,'filter_type','hough','sigma_r',0.25,'sigma_v',0.424661);
% [V,VData]=mhs_vessel_frangi(Img,'scale_range',[1.25,8],'STD',false,'filter_type','hough','sigma_r',0.25,'global_grad',1);
%
% VRec=mhs_FilterBackproj(VData);
%
function [V1,V2,vaccu]=mhs_vessel_frangi(img,varargin)

a=0.5;b=0.5;c=0.25;
crop=[0,0,0];
STD=false;
nscales=5;         
scale_range=[1.25,8];
sigman=-1;
epsilon=0.01;
sigma_v=-1;
sigma_v2=-1;
sigma_r=-1;
sigma_h=-1;
median=false;
depug_pt=[];
global_grad=-1;
hough_dis_angle=30;
        tube_steps=3;
        tube_length=2;
fnumber=1238;
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
if ~exist('Scales')
    Scales_fac=(scale_range(2)/scale_range(1))^(1/(nscales-1));
    Scales=scale_range(1);
    for a=2:nscales
        Scales(a)=Scales(a-1)*Scales_fac;
    end;
end;
    
classid=class(img);
    
 shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);    
    

figure(fnumber);
fcount=1;
clf;

numscales=numel(Scales);

V1=zeros([numscales,cshape]);
V2=zeros([5,cshape]);

if STD
    if (sigman==-1)
        sigman=1.5*max(Scales);
    end;
    mean=mhs_smooth_img(img,sigman,'normalize',true);
    mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
    std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
    std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
end;  


for s=1:numel(Scales)
    scale=Scales(s);
    
    if (strcmp('hough',filter_type)==0)
        if STD 
            HField=scale^2*reshape(repmat(std_dev(:)',6,1),[6,cshape]).*Hessian(img,scale,'crop',crop,'normalize',true);
        else
            HField=scale^2*Hessian(img,scale,'crop',crop,'normalize',true);        
        end;
    end
    
     
    if strcmp('frangi',filter_type)~=0
        fprintf('frangi\n');
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        DirImg=myevec_main([7,8,9],:,:,:);
        DirImg=reshape(DirImg,[3,1,size(img)]);
    
        S=squeeze(sqrt(sum(myev_main.^2,1)));
        Ra=squeeze(abs(myev_main(2,:,:,:))./(eps+abs(myev_main(1,:,:,:))));
        Rb=squeeze(abs(myev_main(3,:,:,:))./sqrt((eps+abs(myev_main(1,:,:,:).*abs(myev_main(2,:,:,:))))));
        vote=(1-exp(-Ra.^2/(2*a^2))).*exp(-Rb.^2/(2*b^2)).*(1-exp(-S.^2/(2*c^2))).*squeeze(((myev_main(1,:,:,:)<0)&(myev_main(2,:,:,:)<0)));
    elseif strcmp('medial',filter_type)~=0
        fprintf('medial\n');
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        DirImg=myevec_main([7,8,9],:,:,:);
        DirImg=reshape(DirImg,[3,1,size(img)]);
        
        if STD
            GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
        else
            GField=Grad(img,scale,'crop',crop,'normalize',true);
        end;
        GM=squeeze(sqrt(sum(GField.^2,1)));
        
        %VM=mhs_medialness(GM,DirImg,{'radius',scale,'sigma_r',sigma_r,'sigma_v',sigma_v,'mode',3});
        VM=mhs_medialness(GM,DirImg,{'radius',scale,'sigma_r',sigma_r,'mode',3});
        %vote=scale*VM2.*(VM2>0);
        
        VM2=(squeeze(VM)-GM);
        vote=scale*VM2.*(VM2>0);
    elseif strcmp('hough',filter_type)~=0
        
        fprintf('hough\n');
        HField=scale^2*Hessian(img,sqrt(2)*scale,'crop',crop,'normalize',true);        
        %HField=scale^2*Hessian(img,2*scale,'crop',crop,'normalize',true);        
        
        Lap=(sum(HField(1:3,:,:,:),1));
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;    
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        DirImg=myevec_main([1,2,3],:,:,:);
        DirImg=reshape(DirImg,[3,1,size(img)]);
        EigV=squeeze(myev_main(1,:,:,:));
        
        if global_grad<-1
            if STD
                GField=scale*reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
            else
                GField=scale*Grad(img,scale,'crop',crop,'normalize',true);
            end;
            GM=squeeze(sqrt(sum(GField.^2,1)));
        else
%             if s==1
%                 GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img);
%                 GM2Field=squeeze(sum(GField.^2,1));
%                 GField=mhs_gvf(GField,GM2Field,{'num_iter',100,'alpha',0.1});
%             end;
            if s==1
                if STD
                    GField=global_grad*reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,global_grad,'crop',crop,'normalize',true);

                    %GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,-1,'crop',crop);
                    
                else
                    GField=global_grad*Grad(img,global_grad,'crop',crop,'normalize',true);
                    %GField=Grad(img,-1,'crop',crop);
                end;
                GField_tmp=GField;
                %GM=squeeze(sqrt(sum(GField_tmp.^2,1)));
                %GField_tmp=1./(reshape(repmat(GM(:)',3,1),[3,cshape])+eps).*GField_tmp;
            end;
%             for d=1:3
%                     GField(d,:,:,:)=scale*squeeze(mhs_smooth_img(squeeze(GField_tmp(d,:,:,:)),scale,'normalize',true));
%             end;
            %GM=squeeze(sqrt(sum(GField.^2,1)));
        end;
        

        
        %if ~exist('accu_smooth_sigma')
            hough_dis_angle_rad=2*pi*hough_dis_angle/360;
            accu_smooth_sigma=sqrt(2*scale^2*(1-cos(hough_dis_angle_rad)));
            accu_smooth_sigma=accu_smooth_sigma*1/(2*sqrt(2*log(2)));
            fprintf('angle %3.2f: sigma: %f\n',hough_dis_angle,accu_smooth_sigma);
        %end;
        
        %accu_smooth_sigma=1/sqrt(2);
        accu_smooth_sigma=1;
                
        params={'radius',scale,...
            'sigma_v',accu_smooth_sigma,...%'sigma_v',0.424661*scale,...%'sigma_v',sigma_v,...%'sigma_v',sqrt(scale/2),...%'sigma_v',sqrt(scale),...%'sigma_v',sigma_v,...%'sigma_v',(scale/2),...%sqrt(scale)/2,...%'sigma_v',sqrt(scale),...%sqrt(scale)/2,...
            'sigma_v2',sigma_v2,...
            'sigma_h',sigma_h,...
            'sigma_r',sigma_r,...%'sigma_r',-1,...%'sigma_r',sigma_r,...
            'mode',1,...
            'tube_steps',tube_steps,...
            'tube_length',sqrt(scale)*tube_length,...'tube_length',tube_length,...'tube_length',sqrt(scale)*tube_length,...'tube_length',tube_length,...%'tube_length',sqrt(scale)*tube_length,...'tube_length',tube_length,...%'tube_length',scale*tube_length,...%
            'circle_mode',false,...
            'median',median,...
            'depug_pt',depug_pt,...
            'min_mode',false};
        
        
        %EigV2=(1-exp(-EigV.^2/(2*c^2)));
        %EigV2=EigV;
        EigV2=1;
        
        if numel(depug_pt)>1
            [vote,vaccu]=mhs_medialness(GField,DirImg,params);
            vote=squeeze(vote);
        else
            vote=squeeze(mhs_medialness(GField,DirImg,params)).*EigV2;
        end;
        
        
%     D=3;
%     figure(1239);
%     subplot(1,numel(Scales)+1,fcount+1);
%     imagesc(squeeze(max(EigV2,[],D)));colorbar;
%     title(['scale ',num2str(scale)]);
%     subplot(1,numel(Scales)+1,1);
    %[Mv,Mi]=max(EigV,[],1);
    %imagesc(squeeze(max(Mv,[],D)));
    %title(['MIP']);
    %subplot(1,numel(Scales)+2,2);
    %imagesc(squeeze(max(Mi,[],3)));
    drawnow            
%         if (sigma_r>0)
%             GM=GM.^sigma_r;
%         end;
%         VM=mhs_medialness(GField,DirImg,params);
%         VM2=(squeeze(VM)-GM);
%         vote=VM2;    
        %vote=VM2.*(VM2>0);    
    elseif strcmp('steer',filter_type)~=0
        fprintf('steer\n');
        Lap=(sum(HField(1:3,:,:,:),1));
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;    
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        DirImg=myevec_main([1,2,3],:,:,:);
        DirImg=reshape(DirImg,[3,1,size(img)]);
        vote=squeeze(myev_main(1,:,:,:));
    end;
    
    V1(s,:)=vote(:);
    D=3;
    figure(fnumber);
    subplot(1,numel(Scales)+2,fcount+2);
    imagesc(squeeze(max(vote,[],D)));fcount=fcount+1;colorbar;
    title(['scale ',num2str(scale)]);
    subplot(1,numel(Scales)+2,1);
    imagesc(squeeze(max(img,[],D)));
    title(['MIP Img']);
    subplot(1,numel(Scales)+2,2);
    [Mv,Mi]=max(V1,[],1);
    imagesc(squeeze(max(Mv,[],D+1)));
    title(['MIP']);
    %subplot(1,numel(Scales)+2,2);
    %imagesc(squeeze(max(Mi,[],3)));
    drawnow    
    
    
   
    
    if s>1
        sindx=squeeze((V1(s-1,:)<V1(s,:)));
        V2(1:3,sindx)=DirImg(:,sindx);
        V2(4,sindx)=s;
        V2(5,sindx)=vote(sindx);
    else
        V2(1:3,:)=DirImg(:,:);
        V2(4,:)=s;
        V2(5,:)=vote(:);
    end;
    

end;    
