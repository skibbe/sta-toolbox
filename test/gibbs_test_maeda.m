function gibbs_test2(varargin)
niter=50000;
uniformproposal = false;
nbins=100;
lambda=1;
pami=false;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;        

            % beta = density in #particles/voxel
            %beta =1:16; 
            beta =ones(nbins,1);

            % total volume in voxel
            L = length(beta);
            
            %L = nbins;
            %beta =lambda/L*ones(nbins,1);
            
            % der gauss
            gauss = @(x) exp(-(x-L/2)^2 /(2*L^2)) / sqrt(2*pi*L^2);

            P = []; % array containing particle positions

            h = 0;
            cnt = 0;

            

            for it = 1:niter,
                if rand > 0.5,        
                    if uniformproposal,
                        k = rand*L;
                    else
                        k = randn*L + L/2;        
                    end;
                    kint = floor(k)+1;
                    if kint >= 1 & kint <= L,
                        if parmi
                            if uniformproposal,
                                R = lambda *L /(length(P)+1);
                            else
                                R = lambda * L/(length(P)+1)  / gauss(k);
                            end;
                        else
                            if uniformproposal,
                                R = beta(kint) * 1/(length(P)+1)  / (1/L);
                            else
                                R = beta(kint) * 1/(length(P)+1)  / gauss(k);
                            end;
                        end;
                        if R > rand,
                            P = [P k];
                        end;        
                    end;
                else
                    if length(P) > 0,
                        k = rand*(length(P)) ;
                        kint = floor(k)+1;
                        if parmi
                            if uniformproposal,
                                R = length(P)/lambda* (1/L);
                            else
                                R = length(P)/lambda* gauss(P(kint));
                            end;
                        else
                            if uniformproposal,
                                R = length(P)/beta(floor(P(kint))+1)* (1/L);
                            else
                                R = length(P)/beta(floor(P(kint))+1)* gauss(P(kint));
                            end;
                        end;
                        if R > rand,
                            P = P([1:kint-1 kint+1:end]);
                        end;
                    end;            
                end;
                if it > 1000,
                    h = h + histc(P,[1:length(beta)]);
                    cnt = cnt + 1;
                end;
            end;
            
            figure(123);
            h = h /cnt;
            bar(h);
            title(num2str(numel(P)))




%figure(123);
%hist(P,nbins)
%title(num2str(numel(P)))




function center=binsearch(data,p)
left_pos=0;
right_pos=numel(data);
while (left_pos+1<right_pos)
    
    center=ceil((right_pos+left_pos)/2);
    %fprintf('center %d, %d %d / %d\n',center,left_pos,right_pos,data(center));
    if data(center)>=p
        right_pos=center;
    else
        left_pos=center;
    end;
end;
center=right_pos;

   % fprintf('id %d, value %f / %f\n',center,data(center),p);