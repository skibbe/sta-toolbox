%%

load honda/rawmovei_from_kondo.mat;
img=single(truncated_movie)/255;
clear truncated_movie;

%%
crop=[15,15,5];

Img=mhs_denoise(img);
clear img;
%%

     BLOOD_INTERNET=4;
    SETTINGS=BLOOD_INTERNET;   
    FeatureData=mhs_preprocess(Img,crop,SETTINGS,'online_hesse',true,'trueSF',true,'scale_range',[1.15,5],'addparams',{'poldeg',4});
    %FeatureData=mhs_preprocess(Img,crop,SETTINGS,'online_hesse',true,'trueSF',true,'scale_range',[1.25,10],'addparams',{'poldeg',4});
%%    
%    A=mhs_run(FeatureData,SETTINGS,'override',{'iterations',1000000,'maxcontinue',1,'DataThreshold',-25,'DataScaleGrad',10,'DataScale',16,'PointWeightScale',3*16*[1,1,1],'DataScaleGradVessel',100,'ConnectionScale',3,'ealpha',0.999,'propprop',[1,1,2,3,2,2,1,1,1,1]});
params={'iterations',100000000,...
'maxcontinue',1,'loop_depth',100,...
'AngleScale',pi/4,'ConnectionSearchrad',10,...
'DataThreshold',-20,'DataScaleGrad',10,'DataScaleGradVessel',100,'ConnectionScale',10,...
'ealpha',0.999,'propprop',[1,1,2,3,2,2,1,1,1,1],'showplots',true};

A=mhs_run(FeatureData,SETTINGS,'override',params);


%%