#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>



#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"
#include "mhs_graphics.h"

#define EPSILON 0.00000000000001


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   
    mhs::dataArray<T> data=mhs::dataArray<T>(prhs[0]);
     std::vector<int> depug_pt;
     
     if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;
	 
	 if (mhs::mex_hasParam(params,"depug_pt")!=-1)
             depug_pt=mhs::mex_getParam<int>(params,"depug_pt",3);
    }
    
    bool debug=depug_pt.size()==3;
    
    sta_assert(data.dim.size()==4);
    printf("%d %d %d\n",data.dim[0],data.dim[1],data.dim[2]);
    
    sta_assert(data.dim[3]==5);
    
    
    std::size_t numv=data.dim[0]*data.dim[1]*data.dim[2];
    
    int dims[3];
    dims[0] = data.dim[2];
    dims[1] = data.dim[1];
    dims[2] = data.dim[0];
    plhs[0] = mxCreateNumericArray(3,dims,mhs::mex_getClassId<T>(),mxREAL);
    T *img = (T*) mxGetData(plhs[0]);
    
    int shape[3];
    for (int t=0;t<3;t++)
      shape[t]=data.dim[t];
    
//     return;
    
    for (int z=0;z<shape[0];z++)
    {
      if (!debug)
      printf("[%3.d %3.d]\n",z+1,shape[0]);
      mhs::mex_dumpStringNOW();
      for (int y=0;y<shape[1];y++)
	for (int x=0;x<shape[2];x++)
    {
	  if (debug)
	  {
	   if ((z!=depug_pt[2]-1)||(y!=depug_pt[1]-1)||(x!=depug_pt[0]-1) )
	     continue;
	   
	  }
      
	  std::size_t indx=((z*shape[1]+y)*shape[2]+x);
	  T * data_=data.data+indx*5;
	
	  Vector<T,3> X(z,y,x);
	  Vector<T,3> V(data_[2],data_[1],data_[0]);
	  //Vector<T,3> V(data_[0],data_[1],data_[2]);
	  if (debug)
	  {
	   X.print(); 
	   V.print();
	   printf("%f %f\n",data_[3],data_[4]);
// 	   V[0]=-1;
// 	   V[1]=0;
// 	   V[2]=0;
	   mhs_graphics::renderPlane(X,V,T(10),data_[4],shape,img);
	  }
	  else
	    mhs_graphics::renderPlane(X,V,data_[3],data_[4],shape,img,0);
	    
	  //Vector<T,3> V(data_[0],data_[1],data_[2]);
	  
    }
    }

}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<2\n");

  if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS))
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS))
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}