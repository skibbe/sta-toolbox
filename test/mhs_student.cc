#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>



#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"
#include "gsl/gsl_sf_gamma.h"

#define EPSILON 0.00000000000001


template <typename T>
T student_fact(
	  T & sigma,
	  T & mue,
	  T & v)
{
  T result=gsl_sf_gamma((v+1)/2)/std::sqrt(sigma);
  result/=std::sqrt(M_PI*v)*gsl_sf_gamma(v/2);
}

template <typename T>
T student(T & fact,
	  T & mue,
	  T & sigma,
	  T & v,
	  T * y)
{
 return  fact/std::pow(1+(*y)-mue)*(*y)-mue)/(sigma*v),(v+1)/2);
}

template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    int G=2;
    mhs::dataArray<T> img=mhs::dataArray<T>(prhs[0]);
       
    
    
    T v=100;
    int numit=50;
    
    
    
    int numvoxels=1;
    
    for (int i=0;i<img.dim.size();i++)
      numvoxels*=img.dim[i];
    
    T data_min=std::numeric_limits<T>max();
    T data_max=std::numeric_limits<T>min();
    
    for (int i=0;i<numvoxels;i++)
    {
	data_min=std::min(data_min,img.data[i]);
	data_max=std::max(data_max,img.data[i]);
    }
        
    T * mue=new T[G];
    T * sigma=new T[G];
    T * w=new T[G];
    T * n=new T[G];
    for (int g=0;g<G;g++)
    {
      sigma[g]=1;
      w[g]=1;
      mue[g]=data_min;
    }
    mue[G-1]=data_max;
    

    int problemsize=numvoxels;
    T * Z=new T[G*problemsize];
    T * U=new T[G*problemsize];
    T * Sum=new T[problemsize];
    
    for (int j=0;j<numit;j++)
    {
      // E-Step
      for (int g=0;g<G;g++)
      {
	T * z=Z+g*problemsize; 
	T * u=U+g*problemsize;
	T * y=img.data;
	T fact=student_fact(sigma[g],mue[g],v);

	for (int i=0;i<problemsize;i++)
	{
	  (*z++)=w[g]*student(fact,mue[g],sigma[g],v,y);
	  T tmp=((*y)-mue[g]);
	  (*u++)=(v+1)/(v+(tmp*tmp)/(sigma[g]));
	  y++;
	}
      }
      
      for (int g=0;g<G;g++)
      {
	T * z=Z+g*problemsize;
	T * sum=Sum; 	

	for (int i=0;i<problemsize;i++)
	{
	  if (g==0)
	    (*sum++)=(*z++);
	  else
	    (*sum++)+=(*z++);
	}
      }

      for (int g=0;g<G;g++)
      {
	T * z=Z+g*problemsize;
	T * sum=Sum; 	

	for (int i=0;i<problemsize;i++)
	{
	    (*z++)/=(*sum++);
	}
      }
      
      for (int g=0;g<G;g++)
      {
	n[g]=0;
	T * z=Z+g*problemsize;
	
	for (int i=0;i<problemsize;i++)
	{
	  n[g]+=(*z++);
	}
	
	w[g]=n[g]/problemsize;
      }
      
      for (int g=0;g<G;g++)
      {
	T * z=Z+g*problemsize;
	T * u=U+g*problemsize;
	T * y=img.data;
	T mue_sum1=0;
	T mue_sum2=0;
	T sigma_sum=0;
	for (int i=0;i<problemsize;i++)
	{
	  T tmp=(*z++)*(*u++);
	  sum1+=tmp*(*y);
	  sum2+=tmp;
	  T tmp2=(*y++)-mue[g];
	  sigma_sum+=tmp*tmp2*tmp2;
	}
	
	mue[g]=mue_sum1/mue_sum2;
	sigma[g]=sigma_sum/n[g];
      }
    }

 for (int g=0;g<G;g++)
   printf("%f %f\n",mue[g],sigma[g]);
    
    
    delete [] mue;
    delete [] sigma;
    delete [] w;
    delete [] z;
    delete [] u;
    delete [] n;
    delete [] Sum;
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}