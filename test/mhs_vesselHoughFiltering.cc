#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>
#include "EigDecomp3x3.h"


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"

#define EPSILON 0.00000000000001


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   
    mhs::dataArray<T> HesseTensor=mhs::dataArray<T>(prhs[0]);
    mhs::dataArray<T> directions=mhs::dataArray<T>(prhs[1]);
    
    sta_assert(HesseTensor.dim.size()==4);
    sta_assert(directions.dim.size()==4);
    printf("%d %d %d\n",HesseTensor.dim[0],HesseTensor.dim[1],HesseTensor.dim[2]);
    
    for (int a=0;a<3;a++)
      sta_assert(directions.dim[a]==HesseTensor.dim[a]);
    sta_assert(directions.dim[3]==3);
    sta_assert(HesseTensor.dim[3]==6);
    
    std::size_t numv=directions.dim[0]*directions.dim[1]*directions.dim[2];
    
    int dims[3];
    dims[0] = directions.dim[0];
    dims[1] = directions.dim[1];
    dims[2] = directions.dim[2];
    plhs[0] = mxCreateNumericArray(3,dims,mhs::mex_getClassId<T>(),mxREAL);
    T *votes = (T*) mxGetData(plhs[0]);
    
    
    for (std::size_t i=0;i<numv;i++)
    {
	  T * stensor=HesseTensor.data+i*6;
			T & xx=  stensor[0]; //xx
			T & xy=  stensor[3]; //xy
			T & xz=  stensor[4]; //xz
			T & yy=  stensor[1]; //yy
			T & yz=  stensor[5]; //yz
			T & zz=  stensor[2]; //zz
		    
		    
			
			T & nx=directions.data[3*i];
			T & ny=directions.data[3*i+1];
			T & nz=directions.data[3*i+2];
			  
			  
			  votes[i]=nx*nx*xx+ny*ny*yy+nz*nz*zz+
			   +2*nx*(ny*xy+nz*xz)
			   +2*ny*nz*yz;
      
    }
//  /*    
// /*
// if (numdim==4)
// {
//     mhs::dataArray<T>  directions;
// //     T * grad_weights=NULL;
//     
//     if (nrhs>1)
//     {
//         const mxArray * params=prhs[nrhs-1] ;
// 
// 	if (mhs::mex_hasParam(params,"dirs")!=-1)
// 	  directions=mhs::dataArray<T>(mhs::mex_getParamPtr(params,"dirs"));
// 	
// // 	mhs::dataArray<T>  grad_weight;
// // 	if (mhs::mex_hasParam(params,"grad")!=-1)
// // 	{
// // 	  grad_weight=mhs::dataArray<T>(mhs::mex_getParamPtr(params,"grad"));
// // 	  //if (grad_weight.dim[grad_weight.dim.size()-1]!=6)
// // // 	  for (int i=0;i<grad_weight.dim.size();i++)
// // // 	    printf("%d\n",grad_weight.dim[i]);
// // 	  if (grad_weight.dim[grad_weight.dim.size()-1]!=6)
// // 	    mexErrMsgTxt("error: first dim must be 6\n");
// // 	  grad_weights=grad_weight.data;
// // 	  printf("using gradient weights!\n");
// // 	}
// // 	
// 	
//     }
//     for (int i=0;i<directions.dim.size();i++)
//       printf("%d\n",directions.dim[i]);
//     
// 
//     
//     sta_assert(directions.data!=NULL);
//     sta_assert(directions.dim.size()==2);
//     sta_assert(directions.dim[1]==3);
//     
//     int shape[3];  
//     shape[0] = dims[3];
//     shape[1] = dims[2];
//     shape[2] = dims[1];
//     T *saltensor = (T*) mxGetData(SalTensor);
//     if (dims[0]!=6)
//       mexErrMsgTxt("error: first dim must be 6\n");
//  
//     
//     int numdir=directions.dim[0];
//     
//     
//     
//     
//     int ndims[4];
//     ndims[0]=numdir;
//     ndims[1]=dims[1];
//     ndims[2]=dims[2];
//     ndims[3]=dims[3];
//     plhs[0] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
//     T *ofield = (T*) mxGetData(plhs[0]);
//     
//     T *stensor=saltensor;
// 
//     printf("numdir %d\n",numdir);
//    
//     
// 		std::size_t numv=shape[0]*shape[1]*shape[2];
// 	
// 		
// 	  
// 		
// 		  for (std::size_t idx= 0; idx < numv; idx++)    
// 		  {
// 			T & xx=  stensor[0]; //xx
// 			T & xy=  stensor[3]; //xy
// 			T & xz=  stensor[4]; //xz
// 			T & yy=  stensor[1]; //yy
// 			T & yz=  stensor[5]; //yz
// 			T & zz=  stensor[2]; //zz
// 		    
// 		    
// 			
// 			for (int i=0;i<numdir;i++)
// 			{
// 			  T & nx=directions.data[3*i];
// 			  T & ny=directions.data[3*i+1];
// 			  T & nz=directions.data[3*i+2];
// 			  
// // 			  printf("%f %f %f\n",nx,ny,nz);
// 			
// 			  
// 			  ofield[i]=nx*nx*xx+ny*ny*yy+nz*nz*zz+
// 			   +2*nx*(ny*xy+nz*xz)
// 			   +2*ny*nz*yz;
// 			}
// // 			  return;
// // 			if (grad_weights!=NULL)
// // 			{
// // 			  T & xx=  grad_weights[0]; //xx
// // 			  T & xy=  grad_weights[3]; //xy
// // 			  T & xz=  grad_weights[4]; //xz
// // 			  T & yy=  grad_weights[1]; //yy
// // 			  T & yz=  grad_weights[5]; //yz
// // 			  T & zz=  grad_weights[2]; //zz
// // 			  
// // 			  for (int i=0;i<numdir;i++)
// // 			  {
// // 			    T & nx=directions.data[3*i];
// // 			    T & ny=directions.data[3*i+1];
// // 			    T & nz=directions.data[3*i+2];
// // 			    
// // 			    ofield[i]*=1-(nx*nx*xx+ny*ny*yy+nz*nz*zz+
// // 			    +2*nx*(ny*xy+nz*xz)
// // 			    +2*ny*nz*yz);
// // 			  }			  
// // 			  grad_weights+=6;
// // 			}
// 			
// 			
// 			ofield+=numdir;
// 			stensor+=6;
// 		}
// }
//     */*/
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<2)
        mexErrMsgTxt("error: nrhs<2\n");

  if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS)&&(mxGetClassID(prhs[1])==mxDOUBLE_CLASS))
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS)&&(mxGetClassID(prhs[1])==mxSINGLE_CLASS))
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}