%%
 syms student(y,mue,sigma,v);
 
 %student=( gamma((v+1)/2)*1./sqrt(sigma) )/( sqrt(pi*v)*gamma(v/2)*(1+(y-mue).^2./(sigma.*v))^((v+1)/2) );
 %student=( gamma((v+1)/2)*1/sqrt(sigma) )/( sqrt(pi*v)*gamma(v/2)*(1+(y-mue).^2./(sigma*v))^((v+1)/2) );
 student=( gamma((v+1)/2)*1/sqrt(sigma) )/( sqrt(pi*v)*gamma(v/2)*(1+(y-mue).^2./(sigma*v))^((v+1)/2) );
 
 %student=( gamma((v+1)/2)/(gamma(v/2)*sqrt(pi*v)*sigma) )*(1+1/v*((y-mue)/sigma)^2)^(-(v+1)/2);
 
 studentm = matlabFunction(student);
 
 figure(123);clf
 hold on
 plot(studentm(2,0.1,100,[-10:0.01:10]));
  
 syms normal(y,mue,sigma);
 normal=1/(sqrt(2*pi*sigma))*exp(-(y-mue)^2/(2*sigma));
 
 normalm = matlabFunction(normal);
 figure(123);plot(normalm(2,0.1,[-10:0.01:10]),'r');axis tight
 
 %%
 %y=[5*randn(1,3000)+3,2*randn(1,3000)+17];
 %y=[10*randn(1,3000)+13];
 Mean=[10*rand(1,2)];
 Sigma=[10*rand(1,2)];
 y=[Sigma(1)*randn(1,3000)+Mean(1),Sigma(2)*randn(1,3000)+Mean(2)];
 
 y=y(y>0);

 %
 %y=img2(:)+5;
 
 shape=size(y);
 
 nsamples=numel(y);
 figure(123);clf; 
 hist(y,100)
 %
 v=40;
 G=1;
 
  clear z u StudentSum n w;
  
 mue=repmat(mean(y),1,G);
 Maxdata=max(y);
 mue=[min(y)+eps,Maxdata];
 sigma=0.001*ones(1,G);
 w=ones(1,G);
 

 
 for i=1:50
       % fprintf('%f %f | %f %f\n',mue(1),sigma(1),mue(2),sigma(2));
     mue_old=mue;
     sigma_old=sigma;
     %E step
     for g=1:G
         z{g}=w(g).*studentm(mue(g),sigma(g),v,y);
         u{g}=(v+1)./(v+(y-mue(g)).^2/sigma(g));     
     end;
     StudentSum=zeros(size(z{g}));
     for g=1:G
            StudentSum=StudentSum+z{g};
     end;
     for g=1:G
         z{g}=z{g}./StudentSum;
     end;
     
     %M step
    for g=1:G
        n{g}=sum(z{g});
        w(g)=n{g}/nsamples;
    end;
    
    for g=1:G
        mue(g)=sum(z{g}.*u{g}.*y)./sum(z{g}.*u{g});
        sigma(g)=sum(z{g}.*u{g}.*(y-mue(g)).^2)/n{g};
    end;
     
        
        %err=sum(abs(mue_old-mue)+abs(sigma_old-sigma));
        err=sum(abs(sigma_old-sigma));
        %fprintf('%f: %f %f\n',i,sum(abs(mue_old-mue)),sum(abs(sigma_old-sigma)));
%         if (err<Maxdata/1000)
%             fprintf('err :%f\n',err);
%             break;
%         end;
%     
    
        %fprintf('%f %f\n',mue(1),sigma(1));
 end;
 fprintf('----------------------------\n');
 fprintf('GUESS: %f %f \n',mue(1),sqrt(sigma(1)));
 %fprintf('GUESS: %f %f | %f %f\n',mue(1),sqrt(sigma(1)),mue(2),sqrt(sigma(2)));
 %fprintf('GT   :%f %f | %f %f\n',Mean(1),Sigma(1),Mean(2),Sigma(2));
 
  figure(124);clf; hold on
  plot(studentm(mue(1),sigma(1),v,[min(y):max(y)]));
  %plot(studentm(mue(2),sigma(2),v,[min(y):max(y)]),'r');
  
  
  %%
  %%
 %y=[5*randn(1,3000)+3,2*randn(1,3000)+17];
 %y=[10*randn(1,3000)+13];
 Mean=[10*rand(1,2)];
 Sigma=[10*rand(1,2)];
 y=[Sigma(1)*randn(1,300)+Mean(1),Sigma(2)*randn(1,300)+Mean(2)];
 
 y=y(y>0);

 %
 %y=img(:);
 
 shape=size(y);
 
 nsamples=numel(y);
 figure(123);clf; 
 hist(y,100)
 %
 v=100;
 G=2;
 
  clear z u StudentSum n w;
  
 mue=repmat(mean(y),1,G);
 Maxdata=max(y);
 mue=[min(y)+eps,Maxdata];
 sigma=0.001*ones(1,G);
 w=ones(1,G)/G;

 

 
 for i=1:50
       % fprintf('%f %f | %f %f\n',mue(1),sigma(1),mue(2),sigma(2));
     mue_old=mue;
     sigma_old=sigma;
     %E step
     for g=1:G
         z{g}=w(g).*studentm(mue(g),sigma(g),v,y);
         u{g}=(v+1)./(v+(y-mue(g)).^2/sigma(g));     
     end;
     StudentSum=zeros(size(z{g}));
     for g=1:G
            StudentSum=StudentSum+z{g};
     end;
     for g=1:G
         z{g}=z{g}./StudentSum;
     end;
     
     %M step
    for g=1:G
        n{g}=sum(z{g});
        w(g)=n{g}/nsamples;
    end;
    
    for g=1:G
        mue(g)=sum(z{g}.*u{g}.*y)./sum(z{g}.*u{g});
        sigma(g)=sum(z{g}.*u{g}.*(y-mue(g)).^2)/n{g};
    end;
     
        
        %err=sum(abs(mue_old-mue)+abs(sigma_old-sigma));
        err=sum(abs(sigma_old-sigma));
        %fprintf('%f: %f %f\n',i,sum(abs(mue_old-mue)),sum(abs(sigma_old-sigma)));
%         if (err<Maxdata/1000)
%             fprintf('err :%f\n',err);
%             break;
%         end;
%     
    
        %fprintf('%f %f\n',mue(1),sigma(1));
 end;
 fprintf('----------------------------\n');
 %fprintf('GUESS: %f %f \n',mue(1),sqrt(sigma(1)));
 fprintf('GUESS: %f %f | %f %f\n',mue(1),sqrt(sigma(1)),mue(2),sqrt(sigma(2)));
 fprintf('GT   :%f %f | %f %f\n',Mean(1),Sigma(1),Mean(2),Sigma(2));
 
  figure(124);clf; hold on
  plot(studentm(mue(1),sigma(1),v,[min(y):(max(y)-min(y))/20:max(y)]));
  plot(studentm(mue(2),sigma(2),v,[min(y):(max(y)-min(y))/20:max(y)]),'r');
  
  
  %FB=reshape([studentm(mue(1),sigma(1),v,y),studentm(mue(2),sigma(2),v,y)],[size(img),2]);
  