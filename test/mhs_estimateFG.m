function FB=mhs_estimateFG(img,varargin)

v=4;
iterations=100;

    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    

 syms student(y,mue_,simga_,v_);
 student=( gamma((v_+1)/2)*1/sqrt(simga_) )/( sqrt(pi*v_)*gamma(v_/2)*(1+(y-mue_).^2./(simga_*v_))^((v_+1)/2) );
% student=( gamma((v_+1)/2)/(gamma(v_/2)*sqrt(pi*v_)*simga_) )*(1+1/v_*((y-mue_)/sigma_)^2)^(-(v_+1)/2);
 studentm = matlabFunction(student);
 
 
 
 y=img(:);
 
 shape=size(y);
 
 nsamples=numel(y);
 figure(123);clf; 
 hist(y,100)
 %

 G=2;
 
  clear z u StudentSum n w;
  
 mue=repmat(mean(y),1,G);
 Maxdata=max(y);
 mue=[min(y)+eps,Maxdata];
 sigma=0.001*ones(1,G);
 w=ones(1,G);

 

 fprintf('\n');
 for i=1:iterations
       % fprintf('%f %f | %f %f\n',mue(1),sigma(1),mue(2),sigma(2));
     mue_old=mue;
     sigma_old=sigma;
     %E step
     for g=1:G
         z{g}=w(g).*studentm(mue(g),sigma(g),v,y);
         u{g}=(v+1)./(v+(y-mue(g)).^2/sigma(g));     
     end;
     StudentSum=zeros(size(z{g}));
     for g=1:G
            StudentSum=StudentSum+z{g};
     end;
     for g=1:G
         z{g}=z{g}./StudentSum;
     end;
     
     %M step
    for g=1:G
        n{g}=sum(z{g});
        w(g)=n{g}/nsamples;
    end;
    
    for g=1:G
        mue(g)=sum(z{g}.*u{g}.*y)./sum(z{g}.*u{g});
        sigma(g)=sum(z{g}.*u{g}.*(y-mue(g)).^2)/n{g};
    end;
     
        
        %err=sum(abs(mue_old-mue)+abs(sigma_old-sigma));
        err=sum(abs(sigma_old-sigma));
        %fprintf('%f: %f %f\n',i,sum(abs(mue_old-mue)),sum(abs(sigma_old-sigma)));
%         if (err<Maxdata/1000)
%             fprintf('err :%f\n',err);
%             break;
%         end;
%     
    
        %fprintf('%f %f\n',mue(1),sigma(1));
        fprintf('.');
 end;
 fprintf('\n');
 fprintf('----------------------------\n');
 %fprintf('GUESS: %f %f \n',mue(1),sqrt(sigma(1)));
 fprintf('GUESS: %f %f | %f %f\n',mue(1),sqrt(sigma(1)),mue(2),sqrt(sigma(2)));
 
  figure(124);clf; hold on
  plot(studentm(mue(1),sigma(1),v,[min(y):(max(y)-min(y))/20:max(y)]));
  plot(studentm(mue(2),sigma(2),v,[min(y):(max(y)-min(y))/20:max(y)]),'r');
  

  FB=reshape([studentm(mue(1),sigma(1),v,y),studentm(mue(2),sigma(2),v,y)],[size(img),2]);
  