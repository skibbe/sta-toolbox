
#include "mex.h"
#include <vector>
#include "sta_mex_helpfunc.h"
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include "mhs_vector.h"

#define _POOL_TEST
// #define _NUM_THREADS_ 4
#define _SUPPORT_MATLAB_
 #define USE_OCTREE
#include <algorithm>  



#include "sta_mex_helpfunc.h"


//mex CXXFLAGS="-std=c++11 -fPIC -march=native -fopenmp "  CFLAGS="-std=c++11 -fPIC -march=native -fopenmp "   phantom2.cc -l gsl -L/usr/lib/openblas-base/libopenblas.so  -l gomp



template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  int filter_size=1;
  
  const mxArray *Img;
  Img = prhs[0];       
  const int numdim = mxGetNumberOfDimensions(Img);
  const int *dims = mxGetDimensions(Img);
  const T * imgp=(T* ) mxGetData(Img);
  //std::swap(dims[]);
  
  sta_assert(numdim==3);
  sta_assert(filter_size<=5);
  
  std::size_t shape[3];
  shape[0]=dims[2];
  shape[1]=dims[1];
  shape[2]=dims[0];
  std::size_t numv=shape[0]*shape[1]*shape[2];
  
  plhs[0] = mxCreateNumericArray(numdim,dims,mxGetClassID(Img),mxREAL);
  T *ofield = (T*) mxGetData(plhs[0]);
    
 
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;


        if (mhs::mex_hasParam(params,"filter_size")!=-1)
            filter_size=mhs::mex_getParam<int>(params,"filter_size",1)[0];
    }  
  
    sta_assert(filter_size<=5);

//   
//   filter_buffer[filter_buffer.size()-1]=1;
//   filter_buffer[0]=1;
  
//   return;
   
  #pragma omp parallel for num_threads(omp_get_num_procs())
  for (int z=0;z<shape[0];z++)
  {
    std::vector<T> filter_buffer;
   filter_buffer.resize((2*filter_size+1)*(2*filter_size+1)*(2*filter_size+1));
    for (int y=0;y<shape[1];y++)
    {
      for (int x=0;x<shape[2];x++)
      {
	//T v=imgp[(z*shape[1]+y)*shape[2]+x];
	int buffer_indx=0;
	for (int fz=z-filter_size;fz<=z+filter_size;fz++)
	{
	 if ((fz>-1)&&(fz<shape[0]))
	 for (int fy=y-filter_size;fy<=y+filter_size;fy++)
	 {
	  if ((fy>-1)&&(fy<shape[1]))
	  for (int fx=x-filter_size;fx<=x+filter_size;fx++)
	  {
	    if ((fx>-1)&&(fx<shape[2]))
	    {
	      sta_assert(buffer_indx<filter_buffer.size());
	      sta_assert(buffer_indx>=0);
	      sta_assert((fz*shape[1]+fy)*shape[2]+fx<numv);
	      sta_assert((fz*shape[1]+fy)*shape[2]+fx>=0);
	      //printf("%d %d\n",buffer_indx,filter_buffer.size());
 	      filter_buffer[buffer_indx]=imgp[(fz*shape[1]+fy)*shape[2]+fx];
 	      buffer_indx++;
	     //filter_buffer[filter_buffer.size()-1]=1;
	    }
	  }
	 }
	}
	//filter_buffer.sort();
	//std::sort (filter_buffer.begin(), filter_buffer.end());
	std::sort (filter_buffer.begin(),filter_buffer.begin()+buffer_indx); 
	sta_assert((z*shape[1]+y)*shape[2]+x<numv);
	sta_assert((z*shape[1]+y)*shape[2]+x>=0);
 	ofield[(z*shape[1]+y)*shape[2]+x]=filter_buffer[int(std::floor(buffer_indx/2.0))];
      }
    }
  }
  
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}
