%
%
% [V,VData]=mhs_vesselHoughVote(Img,'scale_range',[1,8],'gscale',-1,'sigman_grad',-1,'sigma_r',0.5,'tube_steps',3,'tube_length',2,'STD',false,'global_grad',true);
% VRec=mhs_FilterBackproj(VData);
%
function [V1,V2,V3]=mhs_vesselHoughVote(img,varargin)

 mode=1;
 sigma=100;
 crop=[0,0,0];
epsilon=0.001;
STD=false;
sigman=-1;
sigman_grad=-1;
gscale=1.5;
vscale=1.5;
min_mode=false;
gamma=-1;
tube_length=1.5;
tube_steps=2;
global_grad=false;
sigma_h=1;

sigma_v=1;

nscales=5;         
scale_range=[1.25,8];

sigma_r=-1;

    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
if ~exist('Scales')
    Scales_fac=scale_range(2)-scale_range(1);
    Scales=scale_range(1):Scales_fac/(nscales-1):scale_range(2);
    %Scales_fac=(scale_range(2)/scale_range(1))^(1/(nscales-1));
    %Scales=scale_range(1);
    %for a=2:nscales
    %    Scales(a)=Scales(a-1)*Scales_fac;
    %end;
end;
    
classid=class(img)    ;
    
 shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);    
    
%if 0    
    if STD
        if (sigman==-1)
            sigman=1.5*max(Scales);
        end;
        mean=mhs_smooth_img(img,sigman,'normalize',true);
        mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
        std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
        std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
        if sigman_grad==-1
            std_dev_grad=std_dev;
        else
            mean=mhs_smooth_img(img,sigman_grad,'normalize',true);
            mean2=mhs_smooth_img(img.^2,sigman_grad,'normalize',true);
            std_dev_grad=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
            std_dev_grad=std_dev_grad(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
        end;
    end;  
%end;

figure(1238);
fcount=1;
clf;

numscales=numel(Scales);

V1=zeros([numscales,cshape]);
V2=zeros([5,cshape]);
%V3=zeros([numscales,cshape]);
% 
if global_grad 
    if STD
        %GField=scale*reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
        GField=reshape(repmat(std_dev_grad(:)',3,1),[3,cshape]).*Grad(img,gscale,'crop',crop,'normalize',true);
    else
        %GField=scale*Grad(img,scale,'crop',crop,'normalize',true);
        GField=Grad(img,gscale,'crop',crop,'normalize',true);
    end;
end;

for s=1:numel(Scales)
    scale=Scales(s);
    
if ~global_grad     
    if STD
         %sigman_grad=1.5*scale;
         %mean=mhs_smooth_img(img,sigman_grad,'normalize',true);
         %mean2=mhs_smooth_img(img.^2,sigman_grad,'normalize',true);
         %std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
         %std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
         GField=scale*reshape(repmat(std_dev_grad(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
        
        %GField=scale*sqrt(scale)*reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
        
        %GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
        %GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
    else
        
%         GField=Grad(img,-1);
%         GM=squeeze(sqrt(sum(GField.^2,1)));
%         GMN=GM.^sigma_r./(GM+eps);
%         for d=1:3
%             GField(d,:,:,:)=scale*mhs_smooth_img(GMN.*squeeze(GField(d,:,:,:)),scale,'crop',crop,'normalize',true);
%         end;
        
        GField=scale*Grad(img,scale,'crop',crop,'normalize',true);
        %GField=Grad(img,scale,'crop',crop,'normalize',true);
    end;
end;    
    
    %if STD
    %    HField=scale^2*reshape(repmat(std_dev(:)',6,1),[6,cshape]).*Hessian(img,scale,'crop',crop,'normalize',true);
    %else
        %HField=scale^2.*Hessian(img,scale,'crop',crop,'normalize',true);
        HField=Hessian(img,scale,'crop',crop,'normalize',true);
    %end;
        Lap=(sum(HField(1:3,:,:,:),1));
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;
    
     [myevec_main,myev_main]=(sta_EVGSL(HField));
     
    DirImg=myevec_main([1,2,3],:,:,:);
    DirImg=reshape(DirImg,[3,1,size(img)]);
    %vote=mhs_medialness(GField,DirImg,{'radius',scale,'sigma_v',scale,'sigma_r',1,'mode',1,'tube_steps',tube_steps,'tube_length',scale*tube_length,'circle_mode',false}); 
    params={'radius',scale,...
            'sigma_v',sqrt(scale),...%'sigma_v',sqrt(scale),...%'sigma_v',sigma_v,...%'sigma_v',(scale/2),...%sqrt(scale)/2,...%'sigma_v',sqrt(scale),...%sqrt(scale)/2,...
            'sigma_r',sigma_r,...%'sigma_r',-1,...%'sigma_r',sigma_r,...
            'mode',1,...
            'tube_steps',tube_steps,...
            'tube_length',sqrt(scale)*tube_length,...'tube_length',tube_length,...%'tube_length',scale*tube_length,...%
            'circle_mode',false,...
            'min_mode',min_mode,...
            'sigma_h',sigma_h};
        
    vote=mhs_medialness(GField,DirImg,params); 
   
    %GM=squeeze(sqrt(sum(GField.^2,1)));
    % if sigma_r>0
    %    GM=GM.^sigma_r;
    % end
     %V1(s,:)=vote(:)-GM(:);
    %V1(s,:)=scale*vote(:);
    V1(s,:)=vote(:);
    D=3;
    figure(1238);
    subplot(1,numel(Scales),fcount);imagesc(squeeze(max(vote,[],D)));fcount=fcount+1;colorbar;
    drawnow
    
    %vote(:)=myev_main(1,:);
    if s>1
        sindx=squeeze((V1(s-1,:)<V1(s,:)));
        V2(1:3,sindx)=DirImg(:,sindx);
        V2(4,sindx)=s;
        V2(5,sindx)=vote(sindx);
    else
        V2(1:3,:)=DirImg(:,:);
        V2(4,:)=s;
        V2(5,:)=vote(:);
    end;
    
    
    
    
    
    
    
    
    
    
    
if 0     
    OT=cat(1,cat(1,myevec_main(1,:).^2,myevec_main(2,:).^2,myevec_main(3,:).^2),myevec_main(1,:).*myevec_main(2,:),myevec_main(1,:).*myevec_main(3,:),myevec_main(2,:).*myevec_main(3,:));
    OT=reshape(OT,[6,cshape]);%.*reshape(repmat(myev{sindx}(1,:)*(GaussNfact),6,1),[6,cshape]);
     
    vote=mhs_vesselHough(GField,{'radius',scale,'mode',mode,'sigma',150,'gamma',gamma});
    GM=sqrt(squeeze(sum(GField.^2,1)));
    if gamma>0
        GM=GM.^gamma;
    end;
    vote=vote/(scale*scale)-GM;
    
    %HFieldVotes=Hessian(vote,scale,'normalize',true);
    HFieldVotes=Hessian(vote,vscale,'normalize',true);
    Lap=(sum(HFieldVotes(1:3,:,:,:),1));
    HFieldVotes(1,:,:,:)=HFieldVotes(1,:,:,:)-Lap;
    HFieldVotes(2,:,:,:)=HFieldVotes(2,:,:,:)-Lap;
    HFieldVotes(3,:,:,:)=HFieldVotes(3,:,:,:)-Lap;
        
    [myevec_votes,myev_votes]=(sta_EVGSL(HFieldVotes));
    
    OTV=cat(1,cat(1,myevec_votes(1,:).^2,myevec_votes(2,:).^2,myevec_votes(3,:).^2),myevec_votes(1,:).*myevec_votes(2,:),myevec_votes(1,:).*myevec_votes(3,:),myevec_votes(2,:).*myevec_votes(3,:));
     OTV=reshape(OTV,[6,cshape]).*reshape(repmat(myev_votes(1,:),6,1),[6,cshape]);
 
    
      V2(s,:)=vote(:);
     
      v1=squeeze(myev_votes(1,:,:,:));%/(4*sigma*sigma*pi);
      V1(s,:)=v1(:);
      D=3;
      subplot(1,numel(Scales),fcount);imagesc(squeeze(max(v1,[],D)));fcount=fcount+1;
end;      
      
     
if 0     
     v1=mhs_vesselHoughFiltering(OTV,myevec_main([1,2,3],:,:,:));
     v2=mhs_vesselHoughFiltering(HFieldVotes,myevec_main([1,2,3],:,:,:));
     v3=squeeze(myev_votes(1,:,:,:));
     
     
     V1(s,:)=v1(:);
     V2(s,:)=v2(:);
     V3(s,:)=v3(:);
     
        D=3;
        subplot(numel(Scales),3,fcount);imagesc(squeeze(max(v1,[],D)));fcount=fcount+1;
        subplot(numel(Scales),3,fcount);imagesc(squeeze(max(v2,[],D)));fcount=fcount+1;
        subplot(numel(Scales),3,fcount);imagesc(squeeze(max(v3,[],D)));fcount=fcount+1;
end;    
end;    