#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"

#define EPSILON 0.00000000000001





template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   
    
    mhs::dataArray<T> img=mhs::dataArray<T>(prhs[0]);

    T radius=5;
    int mode=1;
    T sigma=1;
    T gamma=-1;
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;
         if (mhs::mex_hasParam(params,"radius")!=-1)
             radius=mhs::mex_getParam<T>(params,"radius",1)[0];
         if (mhs::mex_hasParam(params,"mode")!=-1)
             mode=mhs::mex_getParam<int>(params,"mode",1)[0];	
	 
	 if (mhs::mex_hasParam(params,"sigma")!=-1)
             sigma=mhs::mex_getParam<T>(params,"sigma",1)[0];
	 if (mhs::mex_hasParam(params,"gamma")!=-1)
             gamma=mhs::mex_getParam<T>(params,"gamma",1)[0];	 
    }
  
    

    printf("radius is %f\n",radius);
    
    sta_assert(img.dim.size()==4);
    printf("%d %d %d\n",img.dim[0],img.dim[1],img.dim[2]);
    sta_assert(img.dim[3]==3);
    
    int numv=img.dim[0]*img.dim[1]*img.dim[2];
    
    
    int shape[3];
    for (int t=0;t<3;t++)
      shape[t]=img.dim[t];
    
    printf("image shape [%d %d %d]\n",img.dim[0],img.dim[1],img.dim[2]);
    
    
    int dims[3];
    dims[0] = shape[2];
    dims[1] = shape[1];
    dims[2] = shape[0];
    plhs[0] = mxCreateNumericArray(3,dims,mhs::mex_getClassId<T>(),mxREAL);
    T *accu = (T*) mxGetData(plhs[0]);
    
    T * gm=img.data;

    
    #pragma omp parallel for  	
    for (int z=0;z<shape[0];z++)
    {
      for (int y=0;y<shape[1];y++)
      {
	for (int x=0;x<shape[2];x++)
	{
	  int indx=((z*shape[1]+y)*shape[2]+x);
	  
	  Vector<T,3> vessel_grad(gm[indx*3+2],gm[indx*3+1],gm[indx*3]);
	  T vessel_grad_l=std::sqrt(vessel_grad.norm2());
		if (gamma>0)
		    vessel_grad_l=std::pow(vessel_grad_l,gamma);	  
	  Vector<T,3> grad;
	  
	  Vector<T,3> voting_pt(z,y,x);
	  vessel_grad.normalize();
	  voting_pt+=vessel_grad*radius;
	  
	  int Z=std::floor(voting_pt[0]);
	  int Y=std::floor(voting_pt[1]);
	  int X=std::floor(voting_pt[2]);
	  
	    if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
		(Z<0)||(Y<0)||(X<0))
	      continue;

	  T wz=(voting_pt[0]-Z);
	  T wy=(voting_pt[1]-Y);
	  T wx=(voting_pt[2]-X);
	  
	  
	  T voting_value=1;
	  
	  switch (mode)
	  {
	    case 1:
	    {
		grad=T(0);
		for (int i=0;i<3;i++)
		{
		  T & g=grad.v[i];
	// 		      g=gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
		  g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
		  g+=(1-wz)*(1-wy)*(wx)*gm[((Z*shape[1]+Y)*shape[2]+(X+1))*3+i];
		  g+=(1-wz)*(wy)*(1-wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+X)*3+i];
		  g+=(1-wz)*(wy)*(wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
		  g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+X)*3+i];
		  g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+(X+1))*3+i];
		  g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+X)*3+i];
		  g+=(wz)*(wy)*(wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
		}
		std::swap(grad.v[0],grad.v[2]);
		
// 		T gm=std::sqrt(grad.norm2());
// 		if (gamma>0)
// 		    gm=std::pow(gm,gamma);

		
		voting_value=vessel_grad_l;
	    }
		break;
	    case 2:
	    {
	    } 
		break;
	    case 3:
	    {
		grad=T(0);
		for (int i=0;i<3;i++)
		{
		  T & g=grad.v[i];
	// 		      g=gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
		  g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
		  g+=(1-wz)*(1-wy)*(wx)*gm[((Z*shape[1]+Y)*shape[2]+(X+1))*3+i];
		  g+=(1-wz)*(wy)*(1-wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+X)*3+i];
		  g+=(1-wz)*(wy)*(wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
		  g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+X)*3+i];
		  g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+(X+1))*3+i];
		  g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+X)*3+i];
		  g+=(wz)*(wy)*(wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
		}
		std::swap(grad.v[0],grad.v[2]);
		
		T gm=std::sqrt(grad.norm2());
		
		voting_value=gm;//1-std::exp(-vessel_grad_l/(gm*sigma));
	    }
		break;
	     
	  }
	  
	  accu[((Z*shape[1]+Y)*shape[2]+X)]+=(1-wz)*(1-wy)*(1-wx)*voting_value;
	  accu[((Z*shape[1]+Y)*shape[2]+(X+1))]+=(1-wz)*(1-wy)*(wx)*voting_value;
	  accu[((Z*shape[1]+(Y+1))*shape[2]+X)]+=(1-wz)*(wy)*(1-wx)*voting_value;
	  accu[((Z*shape[1]+(Y+1))*shape[2]+(X+1))]+=(1-wz)*(wy)*(wx)*voting_value;
	  accu[(((Z+1)*shape[1]+Y)*shape[2]+X)]+=(wz)*(1-wy)*(1-wx)*voting_value;
	  accu[(((Z+1)*shape[1]+Y)*shape[2]+(X+1))]+=(wz)*(1-wy)*(wx)*voting_value;
	  accu[(((Z+1)*shape[1]+(Y+1))*shape[2]+X)]+=(wz)*(wy)*(1-wx)*voting_value;
	  accu[(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))]+=(wz)*(wy)*(wx)*voting_value;
	  
	}
      }
    }
    
    
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS))
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS))
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}