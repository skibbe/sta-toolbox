#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>


#define _SUPPORT_MATLAB_ 
#include "../ntracker_c/sta_mex_helpfunc.h"
#include "../ntracker_c/mhs_error.h"
#include "../ntracker_c/mhs_vector.h"
#include "../ntracker_c/mhs_graphics.h"

#define EPSILON 0.00000000000001



int compare (const void * a, const void * b)
{
  if (*(int*)a>*(int*)b) return 1;
  if (*(int*)a<*(int*)b) return -1;
  return 0;
  //return ( *(int*)a - *(int*)b );
  
}


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   
    
    mhs::dataArray<T> img=mhs::dataArray<T>(prhs[0]);
    mhs::dataArray<T> directions=mhs::dataArray<T>(prhs[1]);
    
    //mhs::dataArray<T> sfilter=mhs::dataArray<T>(prhs[2]);

    T radius=5;
    T sigma_r=0.5;
    if (nrhs>2)
    {
        const mxArray * params=prhs[nrhs-1] ;
         if (mhs::mex_hasParam(params,"radius")!=-1)
             radius=mhs::mex_getParam<T>(params,"radius",1)[0];
	 

         if (mhs::mex_hasParam(params,"sigma_r")!=-1)
             sigma_r=mhs::mex_getParam<T>(params,"sigma_r",1)[0];
	  
    }
  
  printf("sigma_r :%f \n",sigma_r);
    printf("nrhs :%d\n",nrhs);
  

     printf("radius is %f\n",radius);
    
    bool global_dirs=(directions.dim.size()==2);
    
    if (!global_dirs)
    {
      sta_assert(directions.dim.size()==5);
      printf("%d %d %d %d %d\n",directions.dim[0],directions.dim[1],directions.dim[2],directions.dim[3],directions.dim[4]);
      sta_assert(directions.dim[4]==3);
    }else
    {
      printf("using directions globally\n");
      sta_assert(directions.dim.size()==2);
      printf("%d %d\n",directions.dim[0],directions.dim[1]);
      sta_assert(directions.dim[1]==3);
    }
    
    //sta_assert(sfilter.dim.size()==3);
    
    //int mode=img.dim.size();
    sta_assert((img.dim.size()==3)||(img.dim.size()==4));
    printf("%d %d %d\n",img.dim[0],img.dim[1],img.dim[2]);
  
// printf("A\n");    
    if (!global_dirs)
    for (int t=0;t<3;t++)
    {
     sta_assert(directions.dim[t]==img.dim[t]);
    // sta_assert(sfilter.dim[t]==img.dim[t]);
    }
    
    int numv=img.dim[0]*img.dim[1]*img.dim[2];


/* printf("B\n"); */  
    int numdir;
    if (!global_dirs)
      numdir=directions.dim[3];
    else
      numdir=directions.dim[0];
    printf("numdir %d\n",numdir);
    
    int shape[3];
    for (int t=0;t<3;t++)
      shape[t]=img.dim[t];
    
    printf("image shape [%d %d %d]\n",img.dim[0],img.dim[1],img.dim[2]);
    
/*printf("C\n");   */ 
    //int d=std::ceil(radius+std::max(sigma_r,sigma_v));
    
    int dims[4];
    dims[0] = numdir;    
    dims[1] = shape[2];
    dims[2] = shape[1];
    dims[3] = shape[0];
    plhs[0] = mxCreateNumericArray(4,dims,mhs::mex_getClassId<T>(),mxREAL);
    T *fresp = (T*) mxGetData(plhs[0]);

    
 printf("D\n");
   
	
	  sta_assert((img.dim.size()==3));
	      sta_assert(sigma_r>0);
	      sta_assert(!(sigma_r>1));
	      sigma_r=2*sigma_r*sigma_r;   
    
    
// printf("0\n");
   int N=std::floor(2*M_PI*radius+1);
    
    T * circle_pts=new T[2*N];
    for (int n=0;n<N;n++)
    {
      circle_pts[2*n]=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
      circle_pts[2*n+1]=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
    }
    
    T * gm=img.data;

    
    //#pragma omp parallel for  	
    for (int z=0;z<shape[0];z++)
    {
// printf("1\n");
      
//       T weights[2*N];
      for (int y=0;y<shape[1];y++)
      {
	for (int x=0;x<shape[2];x++)
	{
	  Vector<T,3> P(z,y,x);
	  Vector<T,3> vn1;
	  Vector<T,3> vn2;	  
	  int indx=((z*shape[1]+y)*shape[2]+x);
	  
	  for (int t=0;t<numdir;t++)
	  {
	    T * tdata;
	    if (global_dirs)
	    tdata=directions.data+t*3;
	      else
	    tdata=directions.data+indx*numdir*3+t*3;
	    
	    Vector<T,3> dir(tdata[2],tdata[1],tdata[0]);
	    //Vector<T,3> dir(tdata[0],tdata[1],tdata[2]);
	    mhs_graphics::createOrths(dir,vn1,vn2);
	    
 
	
	//if (false)
	{
	      Vector<T,3> sample_p;
		  
		      T v=0;
		      for (int n=0;n<N;n++)
		      {
			  sample_p=P
				  +vn1*(circle_pts[2*n]*radius)
				  +vn2*(circle_pts[2*n+1]*radius);
			  
			    int Z=std::floor(sample_p[0]);
			    int Y=std::floor(sample_p[1]);
			    int X=std::floor(sample_p[2]);
			    
			      if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				  (Z<0)||(Y<0)||(X<0))
				continue;

			    T wz=(sample_p[0]-Z);
			    T wy=(sample_p[1]-Y);
			    T wx=(sample_p[2]-X);
			    
			    v+=(1-wz)*(1-wy)*(1-wx)*gm[(Z*shape[1]+Y)*shape[2]+X];
			    v+=(1-wz)*(1-wy)*(wx)*gm[(Z*shape[1]+Y)*shape[2]+(X+1)];
			    v+=(1-wz)*(wy)*(1-wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+X];
			    v+=(1-wz)*(wy)*(wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+(X+1)];
			    v+=(wz)*(1-wy)*(1-wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+X];
			    v+=(wz)*(1-wy)*(wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+(X+1)];
			    v+=(wz)*(wy)*(1-wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+X];
			    v+=(wz)*(wy)*(wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1)];
			    
		      }
		      T v0=v/N+std::numeric_limits<T>::epsilon();
		      
		      v=0;
		      for (int n=0;n<N;n++)
		      {
			  sample_p=P
				  +vn1*(circle_pts[2*n]*radius)
				  +vn2*(circle_pts[2*n+1]*radius);
			  
			    int Z=std::floor(sample_p[0]);
			    int Y=std::floor(sample_p[1]);
			    int X=std::floor(sample_p[2]);
			    
			      if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				  (Z<0)||(Y<0)||(X<0))
				continue;

			    T wz=(sample_p[0]-Z);
			    T wy=(sample_p[1]-Y);
			    T wx=(sample_p[2]-X);
			    
			    T tmp=0;
			    tmp+=(1-wz)*(1-wy)*(1-wx)*gm[(Z*shape[1]+Y)*shape[2]+X];
			    tmp+=(1-wz)*(1-wy)*(wx)*gm[(Z*shape[1]+Y)*shape[2]+(X+1)];
			    tmp+=(1-wz)*(wy)*(1-wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+X];
			    tmp+=(1-wz)*(wy)*(wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+(X+1)];
			    tmp+=(wz)*(1-wy)*(1-wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+X];
			    tmp+=(wz)*(1-wy)*(wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+(X+1)];
			    tmp+=(wz)*(wy)*(1-wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+X];
			    tmp+=(wz)*(wy)*(wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1)];
			    T tmp2=(1-tmp/v0);
			    tmp2*=tmp2;
			    v+=tmp*std::exp(-tmp2/(sigma_r));
		      }
		      fresp[indx*numdir+t]=v/N;

	
		
	  
		
		
		
	}
	    
	    
	    }
	   
	    
	    
	    
	    
	  
	}
      }
    }
/*printf("2\n");  */  
    delete [] circle_pts;
    
    
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<2)
        mexErrMsgTxt("error: nrhs<2\n");
  
   if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS)&&(mxGetClassID(prhs[1])==mxDOUBLE_CLASS))//&&(mxGetClassID(prhs[2])==mxDOUBLE_CLASS))
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS)&&(mxGetClassID(prhs[1])==mxSINGLE_CLASS))//&&(mxGetClassID(prhs[2])==mxSINGLE_CLASS))
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  

//   if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS)&&(mxGetClassID(prhs[1])==mxDOUBLE_CLASS))
//    _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
//   else
//     if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS)&&(mxGetClassID(prhs[1])==mxSINGLE_CLASS))
//     _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
//       else 
// 	mexErrMsgTxt("error: unsupported data type\n");
//   
}