#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>


#define _SUPPORT_MATLAB_ 
#include "../ntracker_c/sta_mex_helpfunc.h"
#include "../ntracker_c/mhs_error.h"
#include "../ntracker_c/mhs_vector.h"
#include "../ntracker_c/mhs_graphics.h"

#define EPSILON 0.00000000000001



int compare (const void * a, const void * b)
{
  if (*(int*)a>*(int*)b) return 1;
  if (*(int*)a<*(int*)b) return -1;
  return 0;
  //return ( *(int*)a - *(int*)b );
  
}


// template<typename T>
// void createOrths(Vector<T,3> & v,Vector<T,3> & vn1, Vector<T,3> &vn2)
// {
//     if (std::abs(v[0])>std::abs(v[1]))
//     {
//       if (std::abs(v[1])>std::abs(v[2]))
//       {
// 	vn1=v.cross(Vector<T,3>(0,0,1));
//       }else
//       {
// 	vn1=v.cross(Vector<T,3>(0,1,0));
//       }
//     }else
//     {
//       if (std::abs(v[0])>std::abs(v[2]))
//       {
// 	vn1=v.cross(Vector<T,3>(0,0,1));
//       }else
//       {
// 	vn1=v.cross(Vector<T,3>(1,0,0));
//       }      
//     }
//     vn1.normalize();
//     vn2=vn1.cross(v);
//   
// }



// template<typename REAL>
// void createOrths(REAL *v,REAL *vn1, REAL *vn2)
//     {
// 	    if (std::abs(v[2])+std::numeric_limits<REAL>::epsilon()>1)
//             {
//                 REAL vnorm = sqrt(2);
//                 vn1[2] = 0;
//                 vn1[0] = -1/vnorm;
//                 vn1[1] = 1/vnorm;
//             }
//             else
//             if (v[1] == 0)
//             {
//                 REAL vnorm = sqrt(v[1]*v[1]+v[2]*v[2]);
//                 vn1[0] = 0;
//                 vn1[1] = -v[1]/vnorm;
//                 vn1[2] = v[2]/vnorm;
//             }
//             else
//             {
//                 REAL vnorm = sqrt(v[0]*v[0]+v[2]*v[2]);
//                 vn1[0] = v[2];
//                 vn1[1] = 0;
//                 vn1[2] = -v[0]/vnorm;                
//             }
//                 
//             vn2[0] = vn1[1]*v[2] - vn1[2]*v[1];
//             vn2[1] = vn1[2]*v[0] - vn1[0]*v[2];
//             vn2[2] = vn1[0]*v[1] - vn1[1]*v[0];
//             REAL vnorm2 = sqrt(vn2[0]*vn2[0]+vn2[1]*vn2[1]+vn2[2]*vn2[2]);
//             vn2[0] /= vnorm2;
//             vn2[1] /= vnorm2;
//             vn2[2] /= vnorm2;
//     }


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   
    
    mhs::dataArray<T> img=mhs::dataArray<T>(prhs[0]);
    mhs::dataArray<T> directions=mhs::dataArray<T>(prhs[1]);
    
    //mhs::dataArray<T> sfilter=mhs::dataArray<T>(prhs[2]);

    T radius=5;
    T sigma_v=-1;
    T sigma_r=0.5;
    T sigma_v2=-1;
    T sigma_h=-1;
    int mode=3;
    T tube_length=0;
    int tube_steps=1;
    bool circle_mode=false;
    bool min_mode=false;
    std::vector<int> depug_pt;
    bool median=false;
//     bool vessel_weight=false;
    
    if (nrhs>2)
    {
        const mxArray * params=prhs[nrhs-1] ;
         if (mhs::mex_hasParam(params,"radius")!=-1)
             radius=mhs::mex_getParam<T>(params,"radius",1)[0];
	 
	 if (mhs::mex_hasParam(params,"depug_pt")!=-1)
             depug_pt=mhs::mex_getParam<int>(params,"depug_pt");
	 
         if (mhs::mex_hasParam(params,"sigma_h")!=-1)
             sigma_h=mhs::mex_getParam<T>(params,"sigma_h",1)[0];	 
	 
         if (mhs::mex_hasParam(params,"sigma_v")!=-1)
             sigma_v=mhs::mex_getParam<T>(params,"sigma_v",1)[0];	 
	 if (mhs::mex_hasParam(params,"sigma_v2")!=-1)
             sigma_v2=mhs::mex_getParam<T>(params,"sigma_v2",1)[0];

         if (mhs::mex_hasParam(params,"sigma_r")!=-1)
             sigma_r=mhs::mex_getParam<T>(params,"sigma_r",1)[0];
	 
         if (mhs::mex_hasParam(params,"mode")!=-1)
             mode=mhs::mex_getParam<int>(params,"mode",1)[0];	 
	 
         if (mhs::mex_hasParam(params,"tube_steps")!=-1)
             tube_steps=mhs::mex_getParam<int>(params,"tube_steps",1)[0];
	 
	          if (mhs::mex_hasParam(params,"tube_length")!=-1)
             tube_length=mhs::mex_getParam<T>(params,"tube_length",1)[0];
	 
	  if (mhs::mex_hasParam(params,"min_mode")!=-1)
             min_mode=mhs::mex_getParam<bool>(params,"min_mode",1)[0];
	 
	  if (mhs::mex_hasParam(params,"median")!=-1)
             median=mhs::mex_getParam<bool>(params,"median",1)[0];
	  
	          if (mhs::mex_hasParam(params,"circle_mode")!=-1)
             circle_mode=mhs::mex_getParam<bool>(params,"circle_mode",1)[0];	 
    }
  
  printf("sigma_r :%f \n",sigma_r);
    
  
    bool debug=depug_pt.size()==3;

     printf("radius is %f\n",radius);
     printf("min-mode: %d\n",min_mode);
    
    bool global_dirs=(directions.dim.size()==2);
    
    if (!global_dirs)
    {
      sta_assert(directions.dim.size()==5);
      printf("%d %d %d %d %d\n",directions.dim[0],directions.dim[1],directions.dim[2],directions.dim[3],directions.dim[4]);
      sta_assert(directions.dim[4]==3);
    }else
    {
      printf("using directions globally\n");
      sta_assert(directions.dim.size()==2);
      printf("%d %d\n",directions.dim[0],directions.dim[1]);
      sta_assert(directions.dim[1]==3);
    }
    
    //sta_assert(sfilter.dim.size()==3);
    
    //int mode=img.dim.size();
    sta_assert((img.dim.size()==3)||(img.dim.size()==4));
    printf("%d %d %d\n",img.dim[0],img.dim[1],img.dim[2]);
    if (mode==4)
      sta_assert(img.dim[3]==3);
//     if (mode==3)
//       sta_assert(img.dim[3]==1);
    
    if (!global_dirs)
    for (int t=0;t<3;t++)
    {
     sta_assert(directions.dim[t]==img.dim[t]);
    // sta_assert(sfilter.dim[t]==img.dim[t]);
    }
    
    int numv=img.dim[0]*img.dim[1]*img.dim[2];
    
//    for (int t=0;t<directions.dim[3];t++)    
//      printf("dir %d: %f %f %f\n",t,directions.data[t*3],directions.data[t*3+1],directions.data[t*3+2]);
    
//    return;

    
    int numdir;
    if (!global_dirs)
      numdir=directions.dim[3];
    else
      numdir=directions.dim[0];
    printf("numdir %d\n",numdir);
    
    int shape[3];
    for (int t=0;t<3;t++)
      shape[t]=img.dim[t];
    
    printf("image shape [%d %d %d]\n",img.dim[0],img.dim[1],img.dim[2]);
    
    //int d=std::ceil(radius+std::max(sigma_r,sigma_v));
    
    int dims[4];
    dims[0] = numdir;    
    dims[1] = shape[2];
    dims[2] = shape[1];
    dims[3] = shape[0];
    plhs[0] = mxCreateNumericArray(4,dims,mhs::mex_getClassId<T>(),mxREAL);
    T *fresp = (T*) mxGetData(plhs[0]);
    
    
    T *vote_array;
    int debug_data_dims=4;
    if (debug)
    {
      int dims[4];
      dims[0] = debug_data_dims;    
      dims[1] = shape[2];
      dims[2] = shape[1];
      dims[3] = shape[0];
      plhs[1] = mxCreateNumericArray(4,dims,mhs::mex_getClassId<T>(),mxREAL);
      vote_array= (T*) mxGetData(plhs[1]);
    }   
    
    sta_assert(tube_steps>0);
    T * tube_step_pts=new T[tube_steps];
    T * tube_step_weights=new T[tube_steps];
    switch (mode)
	{
	  case 1:
	    printf("circle hough\n");
	    sta_assert((img.dim.size()==4));
	    if (sigma_v<0)
	      sigma_v=radius;
	    
	    printf("sigma_v: %f\n",sigma_v);
	    sigma_v=2*sigma_v*sigma_v;
	    
	    if (tube_steps==1)
	    {
	      tube_step_pts[0]=0;
	    tube_step_weights[0]=1;
	    }
	    else
	    {
	      T sum=0;
	      for (int a=0;a<tube_steps;a++)
	      {
		tube_step_pts[a]=-tube_length+a*2*tube_length/(tube_steps-1);
		tube_step_weights[a]=1-std::abs(0.5*tube_step_pts[a]/tube_length);
		sum+=tube_step_weights[a];
		printf("%f ",tube_step_pts[a]);
	      }
	      printf("\n");
	      for (int a=0;a<tube_steps;a++)
	      {
		tube_step_weights[a]/=sum;
		printf("%f ",tube_step_weights[a]);
	      }
	      printf("\n");
	    }
	     
	    //sta_assert(sigma_r>0);
	      printf("sigma_r: %f\n",sigma_r);
	      
	      if (sigma_v2>0)
	      {
		sigma_v2=2*sigma_v2*sigma_v2;
		printf("sigma_v2: %f\n",sigma_v2);
	      }
	      
	      if (sigma_h>0)
	      {
		sigma_h=2*sigma_h*sigma_h;
		printf("sigma_h: %f\n",sigma_h);
	      }
	      
	      
		
	    
	    break;
	  case 3:
	    printf("medialness\n");
	    sta_assert((img.dim.size()==3));
	      sta_assert(sigma_r>0);
	      sta_assert(!(sigma_r>1));
	      sigma_r=2*sigma_r*sigma_r;
	    break;
	  case 4:
	    printf("gradient dir medialness\n");
	    sta_assert((img.dim.size()==4));
	    break;
	  default:
	    printf("mode unknown\n");
	    delete [] tube_step_weights;
	    delete [] tube_step_pts;
	    return;
	}
	
	    
    
    
    
   int N=std::floor(2*M_PI*radius+1);
    //int N=std::floor(4*M_PI*radius+1);
//      N=N+(N+1)%2;
    
    T * circle_pts=new T[2*N];
    for (int n=0;n<N;n++)
    {
      circle_pts[2*n]=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
      circle_pts[2*n+1]=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
     // printf("%f %f\n",circle_pts[2*n],circle_pts[2*n+1]);
    }
    
    T * gm=img.data;
    //T * Eigv=sfilter.data;
    
    //return;
    
    #pragma omp parallel for  	
    for (int z=0;z<shape[0];z++)
    {
      T buffer[N*tube_steps];
      
//       T weights[2*N];
      for (int y=0;y<shape[1];y++)
      {
	for (int x=0;x<shape[2];x++)
	{
	  if (debug)
	  {
	   if ((z!=depug_pt[2])||(y!=depug_pt[1])||(x!=depug_pt[0]) )
	     continue;
	  }
	  
	  
	  
	  if (debug)
	  {
	      T voting_value=1;
	      
	      
	      vote_array[debug_data_dims*((z*shape[1]+y)*shape[2]+x)]+=voting_value;
	  }
	  
	  Vector<T,3> P(z,y,x);
	  Vector<T,3> vn1;
	  Vector<T,3> vn2;	  
	  int indx=((z*shape[1]+y)*shape[2]+x);
	  
	  for (int t=0;t<numdir;t++)
	  {
	    T * tdata;
	    if (global_dirs)
	    tdata=directions.data+t*3;
	      else
	    tdata=directions.data+indx*numdir*3+t*3;
	    
	    Vector<T,3> dir(tdata[2],tdata[1],tdata[0]);
	    //Vector<T,3> dir(tdata[0],tdata[1],tdata[2]);
	    mhs_graphics::createOrths(dir,vn1,vn2);
	    
	   if (debug)
	   {
	    dir.print();
	    vn1.print();
	    vn2.print();
	   }  
	switch (mode)
	{
	  case 1:
	  {
		Vector<T,3> vessel_grad(gm[indx*3+2],gm[indx*3+1],gm[indx*3]);
	    //Vector<T,3> vessel_grad(gm[indx*3],gm[indx*3+1],gm[indx*3+2]);
		
		T vessel_grad_l=std::sqrt(vessel_grad.norm2());
		T vessel_grad_l_org=vessel_grad_l;
		if (sigma_r>0)
		  vessel_grad_l=std::pow(vessel_grad_l,sigma_r);		
		//vessel_grad/=(vessel_grad_l+std::numeric_limits<T>::epsilon());
		
		
// 		T eigv0=Eigv[indx];
		
		Vector<T,3> sample_p;
		Vector<T,3> sample_dir;
		Vector<T,3> grad;
		Vector<T,3> voting_pt;
		if (min_mode)
		  fresp[indx*numdir+t]=1000000;
		else
		  fresp[indx*numdir+t]=0;
		
		int bcount=0;
		for (int s=0;s<tube_steps;s++)
		{
		    T & vessel_offset=tube_step_pts[s];
		    Vector<T,3> P_vessel=P+dir*vessel_offset;
		    
		    T mean=0;
		    if (sigma_v2>0)
		    {
		    for (int n=0;n<N;n++)
		    {
			sample_dir=vn1*(circle_pts[2*n])
				  +vn2*(circle_pts[2*n+1]);
				  
			sample_p=P_vessel+sample_dir*radius;
			
			  int Z=std::floor(sample_p[0]);
			  int Y=std::floor(sample_p[1]);
			  int X=std::floor(sample_p[2]);
			  
			    if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				(Z<0)||(Y<0)||(X<0))
			      continue;

			  T wz=(sample_p[0]-Z);
			  T wy=(sample_p[1]-Y);
			  T wx=(sample_p[2]-X);
			  
			  grad=T(0);
			  
			  
			  
			  for (int i=0;i<3;i++)
			  {
			  T & g=grad.v[i];
//       		      g=gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
			  g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
			  g+=(1-wz)*(1-wy)*(wx)*gm[((Z*shape[1]+Y)*shape[2]+(X+1))*3+i];
			  g+=(1-wz)*(wy)*(1-wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+X)*3+i];
			  g+=(1-wz)*(wy)*(wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
			  g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+X)*3+i];
			  g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+(X+1))*3+i];
			  g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+X)*3+i];
			  g+=(wz)*(wy)*(wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
			  }

			  
			  
			  
			  std::swap(grad.v[0],grad.v[2]);
			  
			  if (circle_mode)
			  {
			   grad=grad-dir*dir.dot(grad);   
			  }
			  {
			    T gm=std::sqrt(grad.norm2());
			    T gmorg=gm;
 			    if (sigma_r>0)
 			      gm=std::pow(gm,sigma_r);
			    
			    grad.normalize();
			    voting_pt=sample_p+grad*radius;
			    
			    T value=0;
 			    if (sample_dir.dot(grad)<0)
			      {
				
				if (sigma_h>0)
				{
 				  T eigv=0;
 				  eigv=vessel_grad_l_org/(std::abs(gmorg+std::numeric_limits<T>::epsilon()));				  
				  gm*=std::exp(-eigv*eigv*(1.0/sigma_h));
				}
			  
			  
			  if (sigma_r==-1)
				gm=1;
				
			  
			  gm=std::min(gm,T(1));
// 				if (sigma_v2<0)
				  value=gm*std::exp(-(P_vessel-voting_pt).norm2()/(sigma_v));
// 				else
// 				{
// 				  Vector<T,3> tmp=(P_vessel-voting_pt);
// 				  T d_vessel=tmp.dot(dir);
// 				  T d_plain=(tmp-(dir*d_vessel)).norm2();
// 				  d_vessel*=d_vessel;
// 				  value=gm*std::exp(-(d_plain/sigma_v+d_vessel/sigma_v2));
// 				}
				  
			    }
			    mean+=value;
			  }
		    }
		    mean=mean/N+std::numeric_limits<T>::epsilon();
		    }
		    
		    T v=0;	    
		    for (int n=0;n<N;n++)
		    {
		     
			sample_dir=vn1*(circle_pts[2*n])
				  +vn2*(circle_pts[2*n+1]);
				  
			sample_p=P_vessel+sample_dir*radius;
			
			  int Z=std::floor(sample_p[0]);
			  int Y=std::floor(sample_p[1]);
			  int X=std::floor(sample_p[2]);
			  
			    if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				(Z<0)||(Y<0)||(X<0))
			      continue;

			  T wz=(sample_p[0]-Z);
			  T wy=(sample_p[1]-Y);
			  T wx=(sample_p[2]-X);
			  
			  grad=T(0);
			  
			  
			  
			  for (int i=0;i<3;i++)
			  {
			  T & g=grad.v[i];
//       		      g=gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
			  g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
			  g+=(1-wz)*(1-wy)*(wx)*gm[((Z*shape[1]+Y)*shape[2]+(X+1))*3+i];
			  g+=(1-wz)*(wy)*(1-wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+X)*3+i];
			  g+=(1-wz)*(wy)*(wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
			  g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+X)*3+i];
			  g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+(X+1))*3+i];
			  g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+X)*3+i];
			  g+=(wz)*(wy)*(wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
			  }

			  
			  
			  
			  std::swap(grad.v[0],grad.v[2]);
			  
			  if (circle_mode)
			  {
			   grad=grad-dir*dir.dot(grad);   
			  }
			  {
			    T gm=std::sqrt(grad.norm2());
			    T gmorg=gm;
 			    if (sigma_r>0)
 			      gm=std::pow(gm,sigma_r);
			    
			    grad.normalize();
			    voting_pt=sample_p+grad*radius;
			    
			    T value=0;
 			    if (sample_dir.dot(grad)<0)
			      {
				
				if (sigma_h>0)
				{
 				  T eigv=0;
 				  eigv=vessel_grad_l_org/(std::abs(gmorg+std::numeric_limits<T>::epsilon()));				  
				  gm*=std::exp(-eigv*eigv*(1.0/sigma_h));
				}
			  
			  
			  if (sigma_r==-1)
				gm=1;
			  
			  gm=std::min(gm,T(1));
				
// 				if (sigma_v2<0)
				  value=gm*std::exp(-(P_vessel-voting_pt).norm2()/(sigma_v));
// 				else
// 				{
// 				  Vector<T,3> tmp=(P_vessel-voting_pt);
// 				  T d_vessel=tmp.dot(dir);
// 				  T d_plain=(tmp-(dir*d_vessel)).norm2();
// 				  d_vessel*=d_vessel;
// 				  value=gm*std::exp(-(d_plain/sigma_v+d_vessel/sigma_v2));
// 				}
				  
			    }
			    
			    if (sigma_v2>0)
			    {
			     T tmp2=(1-value/mean);
			      tmp2*=tmp2;
			      value*=std::exp(-tmp2/(sigma_v2)); 
			    }
			    
			    
			     if (!median)
				v+=value;
			     else
			      buffer[bcount++]=value;
			    
			     
			    
			    if (debug)
			    {
				printf("%2.3f ",value);
			      
			        T voting_value=1;
				
				
				vote_array[1+debug_data_dims*((Z*shape[1]+Y)*shape[2]+X)]+=(1-wz)*(1-wy)*(1-wx)*voting_value;
				vote_array[1+debug_data_dims*((Z*shape[1]+Y)*shape[2]+(X+1))]+=(1-wz)*(1-wy)*(wx)*voting_value;
				vote_array[1+debug_data_dims*((Z*shape[1]+(Y+1))*shape[2]+X)]+=(1-wz)*(wy)*(1-wx)*voting_value;
				vote_array[1+debug_data_dims*((Z*shape[1]+(Y+1))*shape[2]+(X+1))]+=(1-wz)*(wy)*(wx)*voting_value;
				vote_array[1+debug_data_dims*(((Z+1)*shape[1]+Y)*shape[2]+X)]+=(wz)*(1-wy)*(1-wx)*voting_value;
				vote_array[1+debug_data_dims*(((Z+1)*shape[1]+Y)*shape[2]+(X+1))]+=(wz)*(1-wy)*(wx)*voting_value;
				vote_array[1+debug_data_dims*(((Z+1)*shape[1]+(Y+1))*shape[2]+X)]+=(wz)*(wy)*(1-wx)*voting_value;
				vote_array[1+debug_data_dims*(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))]+=(wz)*(wy)*(wx)*voting_value;			      
			     
				voting_value=value;
				
				vote_array[3+debug_data_dims*((Z*shape[1]+Y)*shape[2]+X)]+=(1-wz)*(1-wy)*(1-wx)*voting_value;
				vote_array[3+debug_data_dims*((Z*shape[1]+Y)*shape[2]+(X+1))]+=(1-wz)*(1-wy)*(wx)*voting_value;
				vote_array[3+debug_data_dims*((Z*shape[1]+(Y+1))*shape[2]+X)]+=(1-wz)*(wy)*(1-wx)*voting_value;
				vote_array[3+debug_data_dims*((Z*shape[1]+(Y+1))*shape[2]+(X+1))]+=(1-wz)*(wy)*(wx)*voting_value;
				vote_array[3+debug_data_dims*(((Z+1)*shape[1]+Y)*shape[2]+X)]+=(wz)*(1-wy)*(1-wx)*voting_value;
				vote_array[3+debug_data_dims*(((Z+1)*shape[1]+Y)*shape[2]+(X+1))]+=(wz)*(1-wy)*(wx)*voting_value;
				vote_array[3+debug_data_dims*(((Z+1)*shape[1]+(Y+1))*shape[2]+X)]+=(wz)*(wy)*(1-wx)*voting_value;
				vote_array[3+debug_data_dims*(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))]+=(wz)*(wy)*(wx)*voting_value;			      
				
				
			      {
				int Z=std::floor(voting_pt[0]);
				int Y=std::floor(voting_pt[1]);
				int X=std::floor(voting_pt[2]);
				
				  if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				      (Z<0)||(Y<0)||(X<0))
				    continue;

				T wz=(voting_pt[0]-Z);
				T wy=(voting_pt[1]-Y);
				T wx=(voting_pt[2]-X);			      
			        T voting_value=1;
				
				vote_array[2+debug_data_dims*((Z*shape[1]+Y)*shape[2]+X)]+=(1-wz)*(1-wy)*(1-wx)*voting_value;
				vote_array[2+debug_data_dims*((Z*shape[1]+Y)*shape[2]+(X+1))]+=(1-wz)*(1-wy)*(wx)*voting_value;
				vote_array[2+debug_data_dims*((Z*shape[1]+(Y+1))*shape[2]+X)]+=(1-wz)*(wy)*(1-wx)*voting_value;
				vote_array[2+debug_data_dims*((Z*shape[1]+(Y+1))*shape[2]+(X+1))]+=(1-wz)*(wy)*(wx)*voting_value;
				vote_array[2+debug_data_dims*(((Z+1)*shape[1]+Y)*shape[2]+X)]+=(wz)*(1-wy)*(1-wx)*voting_value;
				vote_array[2+debug_data_dims*(((Z+1)*shape[1]+Y)*shape[2]+(X+1))]+=(wz)*(1-wy)*(wx)*voting_value;
				vote_array[2+debug_data_dims*(((Z+1)*shape[1]+(Y+1))*shape[2]+X)]+=(wz)*(wy)*(1-wx)*voting_value;
				vote_array[2+debug_data_dims*(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))]+=(wz)*(wy)*(wx)*voting_value;
				
				      }
			    }
			    
			    
			  }

		    }
		    if (!median)
		    if (min_mode)
		      fresp[indx*numdir+t]=std::min(fresp[indx*numdir+t],tube_step_weights[s]*v/N);
		    else
		      fresp[indx*numdir+t]+=tube_step_weights[s]*v/N;
		    
		}
		if (median)
		    {
		      if (bcount>0)
		      {
		  std::qsort (buffer, bcount, sizeof(T), compare);
		  fresp[indx*numdir+t]=buffer[int(std::ceil(bcount/2.0))];//-vessel_grad_l;
		      }
		      else 
			fresp[indx*numdir+t]=0;
		    }
		    if (debug)
		      printf("\n");
	
	  }
		break;	  
	  
	  
		case 3:
		{
		      
		      Vector<T,3> sample_p;
		  
		      T v=0;
		      for (int n=0;n<N;n++)
		      {
			  sample_p=P
				  +vn1*(circle_pts[2*n]*radius)
				  +vn2*(circle_pts[2*n+1]*radius);
			  
			    int Z=std::floor(sample_p[0]);
			    int Y=std::floor(sample_p[1]);
			    int X=std::floor(sample_p[2]);
			    
			      if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				  (Z<0)||(Y<0)||(X<0))
				continue;

			    T wz=(sample_p[0]-Z);
			    T wy=(sample_p[1]-Y);
			    T wx=(sample_p[2]-X);
			    
			    v+=(1-wz)*(1-wy)*(1-wx)*gm[(Z*shape[1]+Y)*shape[2]+X];
			    v+=(1-wz)*(1-wy)*(wx)*gm[(Z*shape[1]+Y)*shape[2]+(X+1)];
			    v+=(1-wz)*(wy)*(1-wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+X];
			    v+=(1-wz)*(wy)*(wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+(X+1)];
			    v+=(wz)*(1-wy)*(1-wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+X];
			    v+=(wz)*(1-wy)*(wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+(X+1)];
			    v+=(wz)*(wy)*(1-wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+X];
			    v+=(wz)*(wy)*(wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1)];
			    
		      }
		      T v0=v/N+std::numeric_limits<T>::epsilon();
		      
		      v=0;
		      for (int n=0;n<N;n++)
		      {
			  sample_p=P
				  +vn1*(circle_pts[2*n]*radius)
				  +vn2*(circle_pts[2*n+1]*radius);
			  
			    int Z=std::floor(sample_p[0]);
			    int Y=std::floor(sample_p[1]);
			    int X=std::floor(sample_p[2]);
			    
			      if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
				  (Z<0)||(Y<0)||(X<0))
				continue;

			    T wz=(sample_p[0]-Z);
			    T wy=(sample_p[1]-Y);
			    T wx=(sample_p[2]-X);
			    
			    T tmp=0;
			    tmp+=(1-wz)*(1-wy)*(1-wx)*gm[(Z*shape[1]+Y)*shape[2]+X];
			    tmp+=(1-wz)*(1-wy)*(wx)*gm[(Z*shape[1]+Y)*shape[2]+(X+1)];
			    tmp+=(1-wz)*(wy)*(1-wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+X];
			    tmp+=(1-wz)*(wy)*(wx)*gm[(Z*shape[1]+(Y+1))*shape[2]+(X+1)];
			    tmp+=(wz)*(1-wy)*(1-wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+X];
			    tmp+=(wz)*(1-wy)*(wx)*gm[((Z+1)*shape[1]+Y)*shape[2]+(X+1)];
			    tmp+=(wz)*(wy)*(1-wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+X];
			    tmp+=(wz)*(wy)*(wx)*gm[((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1)];
			    T tmp2=(1-tmp/v0);
			    tmp2*=tmp2;
			    v+=tmp*std::exp(-tmp2/(sigma_r));
		      }
		      fresp[indx*numdir+t]=v/N;
		}
		break;
		
	  case 4:
	  {
		Vector<T,3> sample_p;
		Vector<T,3> sample_dir;
		Vector<T,3> grad;
		
		
		T v=0;
		T mean=0;
		for (int n=0;n<N;n++)
		{
		    sample_dir=vn1*(circle_pts[2*n])
			      +vn2*(circle_pts[2*n+1]);
			      
		    sample_p=P+sample_dir*radius;
		    
		      int Z=std::floor(sample_p[0]);
		      int Y=std::floor(sample_p[1]);
		      int X=std::floor(sample_p[2]);
		      
			if ((Z+1>=shape[0])||(Y+1>=shape[1])||(X+1>=shape[2])||
			    (Z<0)||(Y<0)||(X<0))
			  continue;

		      T wz=(sample_p[0]-Z);
		      T wy=(sample_p[1]-Y);
		      T wx=(sample_p[2]-X);
		      
		      grad=T(0);
		      
		      
		      
		      for (int i=0;i<3;i++)
		      {
		      T & g=grad.v[i];
// 		      g=gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
		      g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*shape[1]+Y)*shape[2]+X)*3+i];
		      g+=(1-wz)*(1-wy)*(wx)*gm[((Z*shape[1]+Y)*shape[2]+(X+1))*3+i];
		      g+=(1-wz)*(wy)*(1-wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+X)*3+i];
		      g+=(1-wz)*(wy)*(wx)*gm[((Z*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
		      g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+X)*3+i];
		      g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*shape[1]+Y)*shape[2]+(X+1))*3+i];
		      g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+X)*3+i];
		      g+=(wz)*(wy)*(wx)*gm[(((Z+1)*shape[1]+(Y+1))*shape[2]+(X+1))*3+i];
		      }
		      std::swap(grad.v[0],grad.v[2]);
		      
		      v+=std::max(-sample_dir.dot(grad),T(0));
		}
		fresp[indx*numdir+t]=v/N;
	
	  }
		break;
		
		
		
	}
	    
	    
	  
	    /*
	    int boffset=2+std::ceil(std::max(sigma_r,sigma_v));
	    for (int e=0;e<3;e++)
	    {
	      T low=P[e]-d-boffset;
	      T up=P[e]+d+boffset;
	      bbl[e]=std::max(std::floor(low),T(0));
	      bbu[e]=std::min(std::ceil(up),T(shape[e]));
	    }
	
	    T v=0;
	    T sum=0;

	    for (int z=bbl[0];z<bbu[0];z++)
	    for (int y=bbl[1];y<bbu[1];y++)
	      for (int x=bbl[2];x<bbu[2];x++)
	      {
		Vector<T,3> pos(z,y,x);
		Vector<T,3> localx=pos-P;
		
		T pdist=dir.dot(localx);
		
		T pdistabs=std::abs(pdist);
		if (pdistabs>sigma_v)
		  continue;
		
		T w1=1-pdistabs/sigma_v;
		
		T rad2=(localx-dir*pdist).norm2();
		
		
		
		if (!(rad2<(d+sigma_r)*(d+sigma_r)))
		    continue;
		if (!(rad2>=std::max((d-sigma_r)*(d-sigma_r),T(0))))
		    continue;
		  
		T dists=std::sqrt(rad2);
		T dist=std::abs(dists-radius);
		
		T w2=1-dist/sigma_r;
		
		
		int index=((z*shape[1]+y)*shape[2]+x);
		//int index=((Z*shape[1]+Y)*shape[2]+X);
 		sum+=w1*w2;
 		v+=w1*w2*img.data[index];
// 		sum=1;
// 		v+=img.data[index];
		
		//v=img.data[index]; 
	      }	    
	      //sum=1;
	      //v=img.data[indx]; 
	     //fresp[indx*numdir+t]=indx;//v/sum;
	     fresp[indx*numdir+t]=v/sum;
	     */
	    }
	   
	    
	    
	    
	    
	  
	}
      }
    }
    
    delete [] circle_pts;
    delete [] tube_step_pts;
    
    
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<2)
        mexErrMsgTxt("error: nrhs<2\n");
  
   if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS)&&(mxGetClassID(prhs[1])==mxDOUBLE_CLASS))//&&(mxGetClassID(prhs[2])==mxDOUBLE_CLASS))
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS)&&(mxGetClassID(prhs[1])==mxSINGLE_CLASS))//&&(mxGetClassID(prhs[2])==mxSINGLE_CLASS))
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  

//   if ((mxGetClassID(prhs[0])==mxDOUBLE_CLASS)&&(mxGetClassID(prhs[1])==mxDOUBLE_CLASS))
//    _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
//   else
//     if ((mxGetClassID(prhs[0])==mxSINGLE_CLASS)&&(mxGetClassID(prhs[1])==mxSINGLE_CLASS))
//     _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
//       else 
// 	mexErrMsgTxt("error: unsupported data type\n");
//   
}