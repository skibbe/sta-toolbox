function gibbs_test(mode)
niter=500000;

switch mode
    %#####################################################################
    case 1
        v=40;
        lambda=1000;
        sigma=25;

        P=[];
        W=[];

        f=@(x,s)(1/(sqrt(2*pi*s^2))*(exp(-x.^2/(2*s^2))));

        figure(124);
        plot(f([-v:0.1:v],sigma));
        
        correct=2*sqrt(sigma);
        %correct=2*v;
        
        for a=1:niter

            r=randi(2);

            switch r
                case 1
                    p=sigma*randn;
                    w=f(p,sigma);
                    if (abs(p)>v)
                        continue;
                    end;

                    R=(lambda)/(correct*(numel(P)+1));

                    if (R>=rand-eps)
                        P=[P,p];
                        W=[W,w];
                    end;

                case 2
                    if (numel(P)==0)
                        continue;
                    end;

                    index=randi(numel(P));

                    %R=(correct*W(index)*(numel(P)))/(lambda);
                    R=(W(index)*correct*(numel(P)))/(lambda);

                    if (R>=rand-eps)
                        P(index)=[];
                        W(index)=[];
                    end;
            end;

        end;
        
    %#####################################################################
    case 2
        v=10;
        lambda=1000;

        P=[];
        W=[];

        f=@(x,s)(1/(sqrt(2*pi*s^2))*(exp(-x.^2/(2*s^2))));
        
        saliency_size=1000;
        
        saliency(1:saliency_size)=1;
        for a=2:size(saliency,2)
            saliency(a)=saliency(a-1)+15*randn;
            %saliency(a)=saliency(a-1)+1;
            %saliency(a)=sqrt(a);
        end;
        
        
        saliency=saliency-min(saliency(:));
        
        saliency=saliency/sum(saliency(:));
        %saliency=saliency/max(saliency(:));
        saliency_acc=saliency;
        for a=2:numel(saliency_acc),
            saliency_acc(a)=saliency_acc(a)+saliency_acc(a-1);
        end;
        saliency_acc=saliency_acc./((saliency_acc(end)));
        figure(124);
        plot(saliency);
        
        %sigma=1;%2*v*sqrt(var(saliency));
        %sigma=2*sqrt(var(saliency));

        for a=1:niter

            r=randi(2);

            switch r
                case 1
                    pv=rand;
                    center=binsearch(saliency_acc,pv);
                    w=saliency(center);
                   % w=1;
                    p=2*v*(center/size(saliency,2))-v;

                    if (abs(p)>v)
                        continue;
                    end;

                    R=(lambda)/(sigma*(numel(P)+1));

                    if (R>=rand-eps)
                        P=[P,p];
                        W=[W,w];
                    end;

                case 2
                    if (numel(P)==0)
                        continue;
                    end;

                    index=randi(numel(P));

                    R=(sigma*W(index)*(numel(P)))/(lambda);

                    if (R>=rand-eps)
                        P(index)=[];
                        W(index)=[];
                    end;
            end;

        end;
        
        
        
    case 3
        

            % beta = density in #particles/voxel
            %beta =1:16; 
            beta =50*ones(16,1);

            % total volume in voxel
            L = length(beta);

            % der gauss
            gauss = @(x) exp(-(x-L/2)^2 /(2*L^2)) / sqrt(2*pi*L^2);

            P = []; % array containing particle positions

            h = 0;
            cnt = 0;

            uniformproposal = true;

            for it = 1:30000,
                if rand > 0.5,        
                    if uniformproposal,
                        k = rand*L;
                    else
                        k = randn*L + L/2;        
                    end;
                    kint = floor(k)+1;
                    if kint >= 1 & kint <= L,
                        if uniformproposal,
                            R = beta(kint) * 1/(length(P)+1)  / (1/L);
                        else
                            R = beta(kint) * 1/(length(P)+1)  / gauss(k);
                        end;
                        if R > rand,
                            P = [P k];
                        end;        
                    end;
                else
                    if length(P) > 0,
                        k = rand*(length(P)) ;
                        kint = floor(k)+1;
                        if uniformproposal,
                            R = length(P)/beta(floor(P(kint))+1)* (1/L);
                        else
                            R = length(P)/beta(floor(P(kint))+1)* gauss(P(kint));
                        end;
                        if R > rand,
                            P = P([1:kint-1 kint+1:end]);
                        end;
                    end;            
                end;
                if it > 1000,
                    h = h + histc(P,[1:length(beta)]);
                    cnt = cnt + 1;
                end;
            end;
            h = h /cnt;
            bar(h);


end;

figure(123);
hist(P,50)
title(num2str(numel(P)))





function center=binsearch(data,p)
left_pos=0;
right_pos=numel(data);
while (left_pos+1<right_pos)
    
    center=ceil((right_pos+left_pos)/2);
    %fprintf('center %d, %d %d / %d\n',center,left_pos,right_pos,data(center));
    if data(center)>=p
        right_pos=center;
    else
        left_pos=center;
    end;
end;
center=right_pos;

   % fprintf('id %d, value %f / %f\n',center,data(center),p);