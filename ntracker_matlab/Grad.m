%%


function tensor = Grad(inputimg,sigma,varargin)

normalize=false;
crop=[0,0,0];
for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

shape=size(inputimg);

classid=class(inputimg);

if numel(shape)==2,
    cshape=shape-2*crop(1:2);

    if exist('element_size')
        sigma=sigma.*element_size;
    else
        element_size=[1,1];
    end;

    if (nargin>1) && (sum(abs(sigma))>0)
        inputimg=mhs_smooth_img(inputimg,sigma,'normalize',normalize);
    end;

    wx=1/(2*element_size(1));
    wy=1/(2*element_size(2));


    tensor_=zeros([2,size(inputimg)]);
    
    kernel = zeros(3,3);
    %kernel(2,2) = -1;  kernel(3,2) = 1;
    kernel(1,2) = -1;  kernel(3,2) = 1;
    tensor_(1,:,:) = imfilter(inputimg,kernel*wx,'circular');
    tensor_(2,:,:) = imfilter(inputimg,permute(kernel,[2 1])*wy,'circular');
    
    %tensor=tensor_(:,crop(1):end-crop(1)+1,crop(2):end-crop(2)+1);
    tensor=tensor_(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2));
    
elseif numel(shape)==3,    
cshape=shape-2*crop;
    if exist('element_size')
        sigma=sigma.*element_size;
    else
        element_size=[1,1,1];
    end;

    if (nargin>1) && (sum(abs(sigma))>0)
        inputimg=mhs_smooth_img(inputimg,sigma,'normalize',normalize);
    end;

    wx=1/(2*element_size(1));
    wy=1/(2*element_size(2));
    wz=1/(2*element_size(3));

    tensor_=zeros([3,size(inputimg)],classid);
    
    kernel = zeros(3,3,3);
    %kernel(2,2,2) = -1; kernel(1,2,2) = 0; kernel(3,2,2) = 1;
    kernel(2,2,2) = 0; kernel(1,2,2) = -1; kernel(3,2,2) = 1;
%     tensor_(1,:,:,:) = imfilter(inputimg,kernel*wx,'circular');
%     tensor_(2,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wy,'circular');
%     tensor_(3,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wz,'circular');    
%  tensor_(1,:,:,:) = imfilter(inputimg,kernel*wx,'replicate');
%     tensor_(2,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wy,'replicate');
%     tensor_(3,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wz,'replicate');
    tensor_(1,:,:,:) = imfilter(inputimg,kernel*wx,'symmetric');
    tensor_(2,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wy,'symmetric');
    tensor_(3,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wz,'symmetric');
    
    tensor=tensor_(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
end