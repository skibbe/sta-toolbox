% mex mhs_quatspline.cc -largeArrayDims
function V=mhs_campath_smooth(V,num_new_pts)    


    camData=V.cam(1).path_data;
    
    num_pts=size(camData,2);
    %camData=size(V.cam.path_data,2)

    posI=[1:num_pts];
    Interp_posI= [1:(num_pts-1)/(num_new_pts-1):num_pts];
    if numel(Interp_posI)==2
            assert(false)
    end;
    
    num_features=size(camData,1);
    
    %smoothing=5;
    smoothing=1+num_new_pts/num_pts;
    kernel=exp(-([0:4*smoothing]-2*smoothing).^2/(2*smoothing));
    kernel=kernel/sum(kernel(:));
    
    camData2=zeros([num_features,num_new_pts],class(camData));
    for a=1:num_features
        
        if (true)
            
            if (a<13)||(a>16)
                
                switch a
                    case 36
                    data=interp1(posI,camData(a,:),Interp_posI,'linear');    
                    camData2(a,:)=data;    
                    otherwise
                    data=interp1(posI,camData(a,:),Interp_posI,'spline');    
                    camData2(a,:)=imfilter(data,kernel,'replicate');
                end;
                %camData2(a,:)=interp1(posI,camData(a,:),Interp_posI,'spline');    
                %camData2(a,:)=interp1(posI,camData(a,:),Interp_posI,'linear');    
            else
                if a==13
%                 quat_spline=[interp1(posI,camData(a,:),Interp_posI,'spline');...
%                              interp1(posI,camData(a+1,:),Interp_posI,'spline');...
%                              interp1(posI,camData(a+2,:),Interp_posI,'spline');...
%                              interp1(posI,camData(a+3,:),Interp_posI,'spline')];    
                [quat_slurp,valid]=mhs_quatspline(camData(a:a+3,:),numel(Interp_posI));    
                %camData2(a:a+3,:)=repmat(valid,4,1).*quat_spline+repmat((1-valid),4,1).*quat_slurp;
                %camData2(a:a+3,:)=quat_spline;
                camData2(a:a+3,:)=quat_slurp;
                
                end;
            end;
        
        else
            camData2(a,:)=interp1(posI,camData(a,:),Interp_posI,'linear');    
        end;
    end;
    
    %camData2(36,camData2(36,:)<0)=0;
    
    
    V.cam(1).path_data=camData2;
% 
%     
%     function Q=interp_quat(posI,quat,Interp_posI)
%         
%     1
%     
%     function Q=Qmult(Q1,Q2)
%         
%      q0=Q1(1);
%      q1=Q1(2);
%      q2=Q1(3);
%      q3=Q1(4);
%      
%      r0=Q2(1);
%      r1=Q2(2);
%      r2=Q2(3);
%      r3=Q2(4);
%         
%      prd_0 = (q3 - q2) * (r2 - r3);
% 	 prd_1 = (q0 + q1) * (r0 + r1);
% 	 prd_2 = (q0 - q1) * (r2 + r3);
% 	 prd_3 = (q2 + q3) * (r0 - r1);
% 	 prd_4 = (q3 - q1) * (r1 - r2);
% 	 prd_5 = (q3 + q1) * (r1 + r2);
% 	 prd_6 = (q0 + q2) * (r0 - r3);
% 	 prd_7 = (q0 - q2) * (r0 + r3);
% 
%      prd_8 = prd_5 + prd_6 + prd_7;
%      prd_9 = 0.5 * (prd_4 + prd_8);
% 
%      Q=[prd_0 + prd_9 - prd_5,...
%         prd_1 + prd_9 - prd_8,...
% 	    prd_2 + prd_9 - prd_7,...
% 	    prd_3 + prd_9 - prd_6];
% 
% 	
%     
%     function  Q=lerp(Q1,Q2,t)
% 	
% 	  Q=Q1*(1-t)+Q2*t;
% 	
% 	
%     function Q = slerp(Q1,Q2, t)
%         
%         switch_to_linear=0.05;
% 	  
%         angle = dot(Q1,Q2);
% 
%         if (angle < 0)
%             Q1 = -Q1;
%             angle= - angle;
%         end;
% 
%         if angle <= (1-switch_to_linear)
% 
%             theta = acos(angle);
%             invsintheta = 1/(sin(theta)+eps);
%             scale = sin(theta * (1-t)) * invsintheta;
%             invscale = sin(theta * t) * invsintheta;
%             Q=(Q1*scale) + (Q2*invscale);
%         else 
%             Q=lerp(Q1,Q2,t);
%         end;
%     
%         
%      function Q = Qinv(Q)         
%          l=norm(Q);
%          Q=Q.*[1,-1,-1,-1]/l;
%          
%         
%      function Qs = squad_help(Q0,Q1,Q2)      
%          p0=Qinv(Q1);
%          p1=Qmult(p0,Q2);
%          p2=Qmult(p0,Q0);
%          Qs=Q1*exp(-(log(p1)+log(p2))/4);
%          
%         
%         
%      function Q = squad(Q1,Q2, t,Q0,Q3)   
%          
%          tmp_q1=slerp(Q1,Q2,t);
%          
%          s1=squad_help(Q0,Q1,Q2);      
%          s2=squad_help(Q1,Q2,Q3);
%          tmp_q2=slerp(s1,s2,t);
%          
%          Q=slerp(tmp_q1,tmp_q2,2*t*(1-t));
%          
%          
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
        
        