function B=mhs_distribute(varargin)

if 0
    %% with overlap for devuging
    %Amulti=mhs_distribute('worker',2,'shape',size(FeatureData.img),'mode','single','skip_dim',2);
    if false
        Amulti=mhs_distribute('worker',3,'shape',size(FeatureData.img),'mode','single','skip_dim',2);

        for a=2:numel(Amulti.W)
            Amulti.W(a).offset=Amulti.W(a).offset-[0,10,0]';Amulti.W(a).shape=Amulti.W(a).shape+[0,10,0]';
        end
    else
        Amulti=mhs_distribute('worker',8,'shape',size(FeatureData.img),'mode','cube');
    end
    options=SD_testsettings_new_SIMax('the_merge_make',false,'FeatureData',FeatureData,'nobifurcations',true,'particle_thickness',-1.5,'time_dependent_scale',false);
    A=ntrack_gui(FeatureData,Amulti,{'opt_numiterations',1000000000,options{:},'movie_folder','./movie/','pausing',false});
    %%
    opt_temp=0.1;
    Amerged2=ntrack_graph_tools(A);
    Amerged=ntrack_graph_tools(A,{'tool',6});
    %Amerged=ntrack_graph_tools(Amulti);opt_temp=inf;
    for a=1:numel(A.W),opt_temp=min(opt_temp,A.W(a).A.opt_temp);end;Amerged.opt_temp=opt_temp;
    
    
    %Amerged.opt_temp=1;
    
    [FeatureNoData,Anodata]=init_mhs_NODATA(Amerged);
    
    ConnectionScale=0.1;
    connection_candidate_searchrad=50;
    options=SD_testsettings_new_SIMax('the_merge_make',true,'FeatureData',FeatureNoData,'nobifurcations',true,'EdgeBonus',2.5,'particle_thickness',-1.5,'time_dependent_scale',false,'ConnectionScale',ConnectionScale,'connection_candidate_searchrad',connection_candidate_searchrad);
    %%
    Afinal=ntrack_gui(FeatureNoData,Anodata,{'opt_numiterations',100000000,options{:},'movie_folder','./movie/','pausing',true});
    
    
    %%
    constraint_loop_depth=100;
    options=SD_testsettings_new_SIMax('the_merge_make',false,'constraint_loop_depth',constraint_loop_depth,'the_bif_make_nodata',true,'FeatureData',FeatureNoData,'nobifurcations',false,'EdgeBonus',2.5,'particle_thickness',-1.5,'time_dependent_scale',false,'ConnectionScale',ConnectionScale,'connection_candidate_searchrad',connection_candidate_searchrad);
    %Afinal.options=options;
    AfinalBIF=ntrack_gui(FeatureNoData,Afinal,{'opt_numiterations',100000000,options{:},'movie_folder','./movie/','pausing',true});
    %AfinalBIF=ntrack(FeatureNoData,Afinal,{'opt_numiterations',100000000,options{:},'movie_folder','./movie/','pausing',true});
    
    
    %%
    if true
        %%
        img_s=data.ImgD(:,:,:).^0.75;
        img_s1=img_s(1:end/2,:,:);
        img_s2=img_s(end/2+1:end,:,:);
        alpha=2;
        reliable_boundary=false;
        %options=SD_testsettings_new_SIMax('the_merge_make',false,'FeatureData',FeatureData,'nobifurcations',true,'particle_thickness',-1.5,'time_dependent_scale',false);
        FeatureData1=init_mhs_SD_SHorg(single(img_s1),'element_size',[1,1,1],'scale_range',[1.15,2.0],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',50,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1,'alpha',alpha,'reliable_boundary',reliable_boundary);
        FeatureData2=init_mhs_SD_SHorg(single(img_s2),'element_size',[1,1,1],'scale_range',[1.15,2.0],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',50,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1,'alpha',alpha,'reliable_boundary',reliable_boundary);
        
        %%
        options=SD_testsettings_new_SIMax('the_merge_make',false,'FeatureData',FeatureData1,'nobifurcations',false,'particle_thickness',-1.5,'time_dependent_scale',false);
        %options=SD_testsettings_new_SIMax('the_merge_make',false,'FeatureData',FeatureData1,'nobifurcations',false,'particle_thickness',-0.25,'time_dependent_scale',true);
        
        %options_merge=SD_testsettings_new_SIMax('the_merge_make',true,'FeatureData',FeatureNoData,'nobifurcations',false,'opt_temp',1,'particle_thickness',-1.5,'time_dependent_scale',false);
        A1=ntrack(FeatureData1,{'opt_numiterations',100000000,options{:},'movie_folder','./movie/','pausing',true});
        A2=ntrack(FeatureData2,{'opt_numiterations',100000000,options{:},'movie_folder','./movie/','pausing',true});
        
        %%
        A3=A2;
        A1=mhs_graph_freeze_inner(A1,2*[10,10,10]);
        A3=mhs_graph_freeze_inner(A3,2*[10,10,10]);
        
        A3.data(1,:)=A3.data(1,:)+floor(size(img_s,1)/2);
        
        A=mhs_graph_append(A1,A3);
        [FeatureNoData,Anodata]=init_mhs_NODATA(A);
        %Anodata.opt_temp=0.5;
        %options_merge=SD_testsettings_new_SIMax('the_merge_make',true,'FeatureData',FeatureNoData,'nobifurcations',false,'opt_temp',1,'particle_thickness',-1.5,'time_dependent_scale',false);
        options_merge=SD_testsettings_new_SIMax('the_merge_make',true,'FeatureData',FeatureNoData,'nobifurcations',false,'opt_temp',1,'particle_thickness',-1.5,'time_dependent_scale',false,'DataDistScale',0.01);
        %options_merge=SD_testsettings_new_SIMax('the_merge_make',true,'FeatureData',FeatureNoData,'nobifurcations',false,'opt_temp',1,'particle_thickness',-0.25,'time_dependent_scale',true,'DataDistScale',0.01);
        %Anodata_b=mhs_graph_freeze_inner(Anodata,[10,10,10]);
        %C=ntrack_gui(FeatureNoData,Anodata,{'opt_numiterations',100000000,options_merge{:},'movie_folder','./movie/','pausing',false});
    end;
    
end

    dim=2;
    skip_dim=3;
    mode='default';
    shuffel=false;
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    
if exist('worker','var')    
    switch mode
        case 'cube'
        ROIS=rois_from_shape_q(shape,worker);
        case 'grid'
        ROIS=rois_from_shape_grid(shape,worker,skip_dim);
        case 'multi'
        ROIS=repmat([0,0,0,shape],worker,1);    
        case 'single'
        ROIS=rois_from_shape_single(shape,worker,dim);
        otherwise
            ROIS=rois_from_shape(shape,worker,dim);
    end;
end;



if ~exist('A','var')
    if shuffel
        ROIS=ROIS(randperm(size(ROIS,1)),:);
    end;
    B=mhs_Wsplit_or_merge(ROIS);
else
    is_multi_tracker_data=isfield(A,'W');
    
    
    switch mode
        case 'split'
            if ~is_multi_tracker_data
                B=A;
            else
                numt=numel(A.W);
                for t=1:numt
                    B{t}=A.W(t).A;
                    B{t}.cshape=A.cshape;
                    B{t}.scales=A.scales;
                    B{t}.options=A.options;
                    B{t}.data(1:3,:)=B{t}.data(1:3,:)+repmat(A.W(t).offset,1,size(A.W(t).A.data,2));
                end;
            end;
        otherwise
        if ~is_multi_tracker_data
            B=mhs_Wsplit_or_merge(A,ROIS);
        else
            Amerged=mhs_Wsplit_or_merge(A);
            %Amerged=multi2single(A);
            if (worker>1)
                B=mhs_Wsplit_or_merge(Amerged,ROIS);
            else
                B=Amerged;
            end;
        end;
    end;
end;





% function ROIS=rois_from_shape(shape,worker)
%     oshape=shape;
%     offset=ceil(shape/4);
%     shape=shape-2*offset;
%     
%     dim=3;
%     stepsize=floor((shape(dim)/worker));
%     %stepsize*worker
%     R=[];
%     for w=1:worker
%         R=[R;[(w-1)*stepsize,stepsize]];
%     end;
%     %filling last gab
%     R(end,2)=shape(dim)-R(end,1);
%     
%     ROIS=repmat([0,0,0,shape],worker,1);
%     ROIS(:,dim)=R(:,1);
%     ROIS(:,3+dim)=R(:,2);
%     
%     ROIS(:,1:3)=ROIS(:,1:3)+repmat(offset,size(ROIS,1),1);


function ROIS=rois_from_shape_q(shape,worker)

    %q=floor((worker^(1/3)));
    q=ceil((worker^(1/3)));

    
    for dim=1:3
        stepsize=floor((shape(dim)/q));
        R=[];
        for w=1:q
            R=[R;[(w-1)*stepsize,stepsize]];
        end;
        %filling last gab
        R(end,2)=shape(dim)-R(end,1);
        Rq(dim).R=R;
    end;
    ROIS=[];
    for qx=1:q
        for qy=1:q
            for qz=1:q
                ROIS=[ROIS;[Rq(1).R(qx,1),Rq(2).R(qy,1),Rq(3).R(qz,1),Rq(1).R(qx,2),Rq(2).R(qy,2),Rq(3).R(qz,2)]];
            end;
        end;
    end;
    
 function ROIS=rois_from_shape_grid(shape,worker,skip_dim)

    %q=floor((worker^(1/2)));
    q=ceil((worker^(1/2)));
    
    Q=[q,q,q];
    Q(skip_dim)=1;
    
    for dim=1:3
        if (dim==skip_dim)
            stepsize=shape(dim);
        else
            stepsize=floor((shape(dim)/Q(dim)));
        end;
        R=[];
        for w=1:Q(dim)
            R=[R;[(w-1)*stepsize,stepsize]];
        end;
        %filling last gab
        R(end,2)=shape(dim)-R(end,1);
        Rq(dim).R=R;
    end;
    ROIS=[];
    for qx=1:Q(1)
        for qy=1:Q(2)
            for qz=1:Q(3)
                ROIS=[ROIS;[Rq(1).R(qx,1),Rq(2).R(qy,1),Rq(3).R(qz,1),Rq(1).R(qx,2),Rq(2).R(qy,2),Rq(3).R(qz,2)]];
            end;
        end;
    end;
   % ROIS

    

 function ROIS=rois_from_shape_single(shape,worker,skip_dim)

    q=worker;
    Q=[1,1,1];
    Q(skip_dim)=q;
    
    for dim=1:3
        if (dim~=skip_dim)
            stepsize=shape(dim);
        else
            stepsize=floor((shape(dim)/Q(dim)));
        end;
        R=[];
        for w=1:Q(dim)
            R=[R;[(w-1)*stepsize,stepsize]];
        end;
        %filling last gab
        R(end,2)=shape(dim)-R(end,1);
        Rq(dim).R=R;
    end;
    ROIS=[];
    for qx=1:Q(1)
        for qy=1:Q(2)
            for qz=1:Q(3)
                ROIS=[ROIS;[Rq(1).R(qx,1),Rq(2).R(qy,1),Rq(3).R(qz,1),Rq(1).R(qx,2),Rq(2).R(qy,2),Rq(3).R(qz,2)]];
            end;
        end;
    end;
       
    
    

function ROIS=rois_from_shape(shape,worker,dim)

    stepsize=floor((shape(dim)/worker));
    R=[];
    for w=1:worker
        R=[R;[(w-1)*stepsize,stepsize]];
    end;
    %filling last gab
    R(end,2)=shape(dim)-R(end,1);
    
    ROIS=repmat([0,0,0,shape],worker,1);
    ROIS(:,dim)=R(:,1);
    ROIS(:,3+dim)=R(:,2);
    
