%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3DHough(img,varargin)



crop=[10,10,10];
epsilon=0.01;
Scales_fac=1.5;
sigman=-1;
poldeg=4;

normalization='STD';
%normalization='NONE';
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    classid=class(img);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);


    nscales=numel(Scales);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);


    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    FeatureData.scales=Scales;

    FeatureData.saliency_map=zeros(cshape,classid);
    
    FeatureData.saliency_map_acc=zeros([nscales,cshape],classid);
    
  
    if sigman==-1
        sigman=max(Scales);
    end;
    
     if strcmp(normalization,'STD')
        mean=mhs_smooth_img(img,sigman,'normalize',true);
        mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
        std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
        std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
     end;
    
    
%COMPUTING SALIENCY MAP
    for sindx=1:nscales
        scale=Scales(sindx);

        if strcmp(normalization,'STD')
            HField=reshape(repmat(std_dev(:)',6,1),[6,cshape]).*(scale^2*Hessian(img,scale,'crop',crop,'normalize',true));
        else
            HField=scale^2*Hessian(img,scale,'crop',crop,'normalize',true);
        end
        
        Lap=(sum(HField(1:3,:,:,:),1));

        [myevec_second,myev_second]=(sta_EVGSL(HField));
        
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;
        
        [myevec,myev]=(sta_EVGSL(HField));
        myev(1,:)=myev(1,:).*(myev(1,:)>0).*((myev_second(1,:)<=-eps)&(myev_second(2,:)<=-eps)).*(myev_second(2,:)./(eps+myev_second(1,:))>0.25);
            
        FH=squeeze(myev(1,:,:,:)); 
        
        FeatureData.saliency_map=max(FH,FeatureData.saliency_map);
    end;
        

    prop_min=0.001;
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    accimg=FeatureData.saliency_map;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    for a=2:numel(accimg),
        accimg(a)=accimg(a)+accimg(a-1);
    end;

    accimg=accimg./((accimg(end)));
    FeatureData.saliency_map_acc=accimg;
    
    gradient_img=zeros([3,nscales,cshape],classid);
    for sindx=1:nscales
        scale=Scales(sindx);
        
        if strcmp(normalization,'STD')
            GField=scale*reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
        else
            GField=scale*Grad(img,scale,'crop',crop,'normalize',true);
        end;    
        gradient_img(:,sindx,:)=GField(:,:);
    end;
    
    
    

    mondeg=[poldeg,0,0,0];
    even=false;
    [pm,p]=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);


    radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    positions=radfunc(Scales);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);

    FeatureData.alphas=zeros([3*size(polmat,2),cshape],classid);

    count=0;
    for d=1:3
        values=squeeze(gradient_img(d,:,:));
        alphas=sta_matmult(values,pimat);
        FeatureData.alphas(count+1:count+size(alphas,1),:)=alphas;
        count=count+size(alphas,1);
    end;





    scale=1.5;
    if strcmp(normalization,'STD')
        GField=scale*reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
    else
        GField=scale*Grad(img,scale,'crop',crop,'normalize',true);
    end;    
    FeatureData.vgradient_img=GField;
    
    
    



    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);


    FeatureData.datafunc=single(2);
    FeatureData.cshape=single(cshape);





 
    
    
    
    
    
    
    
 
    