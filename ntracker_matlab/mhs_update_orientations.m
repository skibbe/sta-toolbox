function A = mhs_update_orientations(A,FeatureData)



g=Grad(FeatureData.dist);

spine_indx=(A.data(17,:)==3);

pos=(A.data(1:3,spine_indx));
%D=zeros(Cs.cshape);D(sub2ind(Cs.cshape,ceil(pos(1,:)),ceil(pos(2,:)),ceil(pos(3,:))))=1;

indx=sub2ind(A.cshape,ceil(pos(1,:)),ceil(pos(2,:)),ceil(pos(3,:)));

%g(:,:)=g(:,:)./repmat(sqrt(sum(g(:,:).^2,1)),3,1);
G=g(:,indx)./repmat(eps+sqrt(sum(g(:,indx).^2,1)),3,1);

%A.data([6,5,4],spine_indx)=1;
A.data([4,5,6],spine_indx)=G;
A.data(18,spine_indx)=FeatureData.dist(indx);