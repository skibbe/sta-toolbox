%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3D_GVF(img,varargin)



    crop=[10,10,10];
    epsilon=0.01;
    Scales_fac=1.5;
    poldeg=3;


    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    
    if ~exist('imgGVF','var')
        imgGVF=GVF3D(img,0.05,1000);
    end;
    
    
%%    

     classid=class(img);
    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);
    
    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(1,classid);
    
    FeatureData.saliency_map=zeros(cshape,classid);
    
    
    V=permute(imgGVF./(reshape(repmat(reshape(sqrt(sum(imgGVF.^2,4)),[numel(img),1]),1,3),size(imgGVF))+eps),[4,1,2,3]);
    
    normalize_derivatives=1;
    GX=Grad(squeeze(V(1,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
    GY=Grad(squeeze(V(2,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
    GZ=Grad(squeeze(V(3,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
    HField=[GX(1,:,:,:);GY(2,:,:,:);GZ(3,:,:,:);GX(2,:,:,:);GY(3,:,:,:);GX(3,:,:,:)];

    [myevec,myev]=(sta_EVGSL(HField));

%%
    [myev_main,myev_main_indx]=sort(myev,1,'descend');
    Ra=-myev_main(2,:,:,:).*exp(-myev_main(1,:,:,:).^2./(2*0.5^2*myev_main(2,:,:,:).^2));
    Rb=-myev_main(2,:,:,:).*exp(-myev_main(1,:,:,:).^2./(2*2^2*myev_main(2,:,:,:).^2));
    Hvessel=squeeze((myev_main(2,:,:,:)<0).*(((myev_main(1,:,:,:)<0).*Ra)+((myev_main(1,:,:,:)>0).*Rb)));

%%
   %[myev_main,myev_main_indx]=sort(myev,1,'descend');
   evec_offset=2;
   Hvessel_dir=cat(1,cat(1,myevec(1+evec_offset,:).^2,myevec(2+evec_offset,:).^2,myevec(3+evec_offset,:).^2),myevec(1+evec_offset,:).*myevec(2+evec_offset,:),myevec(2+evec_offset,:).*myevec(3+evec_offset,:),myevec(1+evec_offset,:).*myevec(3+evec_offset,:));
   Hvessel_dir=reshape(Hvessel_dir,[6,cshape]).*reshape(repmat(Hvessel,6,1),[6,cshape]);
   
%%
   FeatureData.scales=cast(Scales,classid);
   FeatureData.Scales_fac=cast(Scales_fac,classid);
   FeatureData.datafunc=cast(5,classid);
   FeatureData.cshape=cast(cshape,classid);
   
   
   FeatureData.NGVF=V(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
   FeatureData.H=Hvessel_dir;
   

%%

    FeatureData.saliency_map=Hvessel;
    assert(~(min(FeatureData.saliency_map(:))<0));
    prop_min=0.1;
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;

    accimg=FeatureData.saliency_map;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    for a=2:numel(accimg),
        accimg(a)=accimg(a)+accimg(a-1);
    end;

    accimg=accimg./((accimg(end)));
    FeatureData.saliency_map_acc=accimg;
    



 
    
    
    
    
    
    
    
 
    