function [timg,tlight,img_brightness]=mhs_create3DtextureNPOT(img,varargin)


% timg=zeros(128,128,128);
% timg(1:10,1:10,1:10)=1;
% timg(end-10:end,end-10:end,end-10:end)=1;
% timg(60:68,60:68,60:68)=1;
% timg=uint8(255*timg./max(timg(:)));
% return;

   s=2000; 
   t=0; 
   crop=[0,0,0]; 
   autocontrast=false;


for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


assert(s>1);

is_color=(numel(size(img))==4);



if (is_color)
    
    if (max(crop)>0)
        img=img(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    end;
    
    img_brightness=squeeze(0.229*img(1,:,:,:)+0.587*img(2,:,:,:)+0.114*img(3,:,:,:));
    
    if autocontrast
        Img=img;
        imgM=mean(Img(:));
        imgV=sqrt(var(Img(:)));
        Img=Img-imgM;
        Img=Img./(imgV);
        Img(:)=max(Img(:),0);
        Img(:)=Img(:)./max(Img(:));
        img=Img;
    end;

    shape=size(img);
    shape=shape(2:4);
    texture_dim_max=[s,s,s];
    texture_dim=floor(min(shape,texture_dim_max));

    %texture_dim=[20,30,40];

    if nargout>1
        if sum(texture_dim~=size(img_brightness))
            timg=scaleimage3D(img_brightness,texture_dim,true);
            tlight=Grad(timg);
        else
            tlight=Grad(img_brightness,1);
        end;

        tlight=uint8(127*tlight([3,2,1],:,:,:)./reshape(repmat(eps+sqrt(sum(tlight(:,:).^2,1)),3,1),size(tlight))+128);
        tlight=permute(tlight,[1,4,3,2]);
    end;

    img=img-min(img(:));
    img(img(:)<t*max(img(:)))=0;
    if sum(texture_dim~=size(img_brightness))
        timg=zeros(texture_dim,class(img));
        for a=1:3
            timg(a,:,:,:)=scaleimage3D(szueeze(img(a,:,:,:)),texture_dim,true);
        end;
    else
        timg=img;
    end;
    timg=timg-min(timg(:));
    timg=uint8(255*timg./max(timg(:)));
    timg=permute(timg,[1,4,3,2]);
else

    
    if (max(crop)>0)
        img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    end;
    
    if autocontrast
        Img=img;
        imgM=mean(Img(:));
        imgV=sqrt(var(Img(:)));
        Img=Img-imgM;
        Img=Img./(imgV);
        Img(:)=max(Img(:),0);
        Img(:)=Img(:)./max(Img(:));
        img=Img;
    else
        img=img-min(img(:));
        img=img./max(img(:));
    end;

    shape=size(img);
    texture_dim_max=[s,s,s];
    texture_dim=floor(min(shape,texture_dim_max));

    %texture_dim=[20,30,40];

    if nargout>1
        if sum(texture_dim~=size(img))
            timg=scaleimage3D(img,texture_dim,true);
            tlight=Grad(timg);
        else
            tlight=Grad(img,1);
        end;

        tlight=uint8(127*tlight([3,2,1],:,:,:)./reshape(repmat(eps+sqrt(sum(tlight(:,:).^2,1)),3,1),size(tlight))+128);
        tlight=permute(tlight,[1,4,3,2]);
    end;

    img=img-min(img(:));
    img(img(:)<t*max(img(:)))=0;
    if sum(texture_dim~=size(img))
        timg=scaleimage3D(img,texture_dim,true);
    else
        timg=img;
    end;
    timg=timg-min(timg(:));
    timg=uint8(255*timg./max(timg(:)));
    timg=permute(timg,[3,2,1]);
    %timg=uint8(255*timg./max(timg(:)));

end;