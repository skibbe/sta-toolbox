function [FeatureData]=init_mhs_SD_SHorg(img,varargin)

    crop=[10,10,10];
    epsilon=0.01;
    epsilon_saliency=0.1;
    
    Scales_fac=1;
    poldeg=3;
    L=4;
    Ldata=2;
    maxit=50;
    debug=false;
    
    adaptive_normalization=false;
    adaptive_normalization_gamma=0.5;
    adaptive_normalization_weight=0.1;
    
    minmax_norm=true;
    
    local_max_norm=false;
    
    min_max_scale=false;
    
    steerable_conv_fact=1.25;
       use_pyramid=false;
    
    scale_range=[1.15,8];
    nscales=4;

    saliancy_offset=-1;
    
    element_size=[1,1,1];
 
    reliable_boundary=true;
    alpha=1;
    aniso = 1;
    lambda = 0.2;

    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    if ~exist('sd_element_size','var')
        sd_element_size=element_size;
    end;
    
    nscales_pol=nscales;
       
    element_size=element_size/min(element_size);
    
    pad=max(crop)<0;
    
    classid=class(img);
    shape=size(img);
    
    if pad
        border=-crop;
        crop=border;
        shape=shape+2*border;
        img_org=img;
        img=padarray(img,border,'symmetric');
    else
        img_org=img;
    end;
    
    scale_stepsize=(scale_range(2)-scale_range(1))/(nscales-1);
    Scales=scale_range(1):scale_stepsize:scale_range(2);
    
    
    scale_stepsize_pol=(scale_range(2)-scale_range(1))/(nscales_pol-1);
    Scales_pol=scale_range(1):scale_stepsize_pol:scale_range(2);
    
    
    Scales=cast(Scales,classid);
    
    if numel(epsilon)<numel(Scales)
        epsilon=repmat(epsilon,1,numel(Scales));
    end;

    FeatureData.Scales_fac=cast(Scales_fac,classid);

    nscales=numel(Scales);
    nscales_pol=numel(Scales_pol);
    
    poldeg=min(poldeg,nscales_pol);

    
    cshape=shape-2*crop;
    assert(min(cshape)>0);

   
    
   
    FeatureData.scales=Scales;
    FeatureData.saliency_map=zeros(cshape,classid);
    
    if exist('saliency_map','var')
        FeatureData.saliency_map(:)=saliency_map(:);
    end;
   
    if ~exist('Ldata','var')
        Ldata=L;
    end;
    
    assert(Ldata<=L);
    assert(mod(Ldata,2)==0);
    
    
 
    
    num_cmp=((Ldata+2)*(Ldata+2))/4;
    zero_value_mask=zeros(1,num_cmp*2)<1;
    Ls=[0:2:Ldata];
    Ls=(((Ls).*(Ls))./4+(Ls+1))*2;
    zero_value_mask(Ls)=false;
    
    num_cmp_org=num_cmp;
    num_cmp=num_cmp*2-numel(Ls);
    
    
    
    FeatureData.sd_data=zeros([nscales,num_cmp,cshape],classid);  
    FeatureData.L=cast(Ldata,classid);
    
    
    fprintf('computing saliency map: ');
            
    
    if min_max_scale
        FeatureData.min_max=zeros([2,cshape],classid);  
        FeatureData.min_max(1,:)=realmax(classid);
        FeatureData.min_max(2,:)=-realmax(classid);
    end
    

    if exist('workers','var')
        [ROIS,workers]=rois_from_shape_q(cshape,workers);

    else
        workers=1;
    end;

    used_mem=mhs_getmem();
    
    img_slocal_sdv=zeros([nscales_pol,cshape],classid);
    img_slocal_mean=zeros([nscales_pol,cshape],classid);
    %FeatureData.img_m=zeros([nscales_pol,cshape],classid);
    
	for w=1:workers
        if workers>1
            R0=[ROIS(w,1:3),ROIS(w,1:3)+ROIS(w,4:6)-1];
            R=R0+[crop,crop];
            
            image=stafieldStruct(img(R(1)-crop(1):R(4)+crop(1),  R(2)-crop(2):R(5)+crop(2),  R(3)-crop(3):R(6)+crop(3)));     
            sub_shape=R0(4:6)-R0(1:3)+1;
        else
            image=stafieldStruct(img);
            R=[1,1,1,cshape];
            R0=R;
            sub_shape=cshape;
        end;


        for sindx=1:nscales
                scale=Scales(sindx);
                res = sta_steerdeconv(image,'tolerance',0.001,'maxit',maxit,'L',L,'element_size',sd_element_size,'sigma',steerable_conv_fact*scale,'alpha',alpha,'lambda',lambda,'aniso',aniso);
                used_mem=max(mhs_getmem(),used_mem);
                
                fprintf('computing local maxima\n');
                [H,m] = getMinMax(res.data,res.L,1,true,res.type,res.storage);
                
                if min_max_scale
                    FeatureData.min_max(1,:,:,:)=min(FeatureData.min_max(1,:,:,:),m(1,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)));
                    FeatureData.min_max(2,:,:,:)=max(FeatureData.min_max(2,:,:,:),m(2,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)));
                end
                
                if ~minmax_norm
                    m=squeeze(m(2,:,:,:));
                else
                    minmax_m=abs(squeeze(m(1,:,:,:)))>abs(squeeze(m(2,:,:,:)));
                    minmax_m=squeeze(m(1,:,:,:)).*minmax_m+squeeze(m(2,:,:,:)).*(~minmax_m);
                    m=squeeze(m(2,:,:,:));
                end;
                
                if exist('fix_sigma','var')
                    sigmanorm=fix_sigma;
                else
                    sigmanorm=max(scale*sqrt(2),2);
                end;
                %[std_dev,local_mean]=mhs_compute_std_mask(m,'sigman',sigmanorm,'epsilon',epsilon(sindx),'crop',crop,'inv',true,'element_size',element_size);
                
                fprintf('normalization\n');
                if local_max_norm
                     %std_dev=mhs_localmax(m,{'kernel',1,'median',0.95,'kernel_rad',scale*2});
                     
                     %std_dev=mhs_localmax(m,{'kernel',1,'median',0.95,'kernel_rad',scale+1});
                     %std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
                     
                     std_dev=m(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
                     local_mean=zeros(size(std_dev),class(std_dev));
                else
                    if ~minmax_norm
                        [std_dev,local_mean]=mhs_compute_std_mask(m,'sigman',sigmanorm,'epsilon',epsilon(sindx),'crop',crop,'inv',false,'element_size',element_size);
                    else
                        [std_dev,local_mean]=mhs_compute_std_mask(minmax_m,'sigman',sigmanorm,'epsilon',epsilon(sindx),'crop',crop,'inv',false,'element_size',element_size);
                    end;
                    
                end;
                used_mem=max(mhs_getmem(),used_mem);
                fprintf('....\n');
                
                if adaptive_normalization
                    fprintf('adaptive normalization coeff ..')
                    %img_med=mhs_localmax(img,{'kernel_rad',sigmanorm,'kernel',1});
                    %simg=adaptive_normalization_weight*max(1-max(mhs_smooth_img(img,sigmanorm,'normalize',true),0).^adaptive_normalization_gamma,0);
                    %simg=adaptive_normalization_weight*max(1-max(img_med,0).^adaptive_normalization_gamma,0);
                    img_med=mhs_localmax(max(img,0).^adaptive_normalization_gamma,{'kernel_rad',sigmanorm,'kernel',1});
                    simg=adaptive_normalization_weight*max(1-max(img_med,0),0);
                    clear img_med;
                    std_dev=std_dev+simg(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))+eps;
                    clear simg;
                    fprintf('done\n')
                end;
                
                %std_dev=max(mhs_localmax(m(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3))-local_mean,{'kernel_rad',5}),0.0);
                
                
                %img_slocal_sdv(sindx,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=1./(std_dev+epsilon(sindx)+eps);
                img_slocal_sdv(sindx,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=std_dev;%1./(std_dev+epsilon(sindx)+eps);
                
                img_slocal_mean(sindx,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=local_mean;

                m=m(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
                
                %FeatureData.img_m(sindx,:,:,:)=m;
                
                res.data=reshape(res.data(:,1:num_cmp_org,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),[num_cmp_org*2,cshape]);
                if ~use_pyramid
                    FeatureData.sd_data(sindx,:,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=res.data(zero_value_mask,:,:,:);
                else
                    assert(workers==1);
                    if (down_sample(sindx)>0)
                        tmp=mhs_img_downscale2_mean(res.data(zero_value_mask,:,:,:));
                        for d=2:(down_sample(sindx))
                            tmp=mhs_img_downscale2_mean(tmp);
                        end;
                        FeatureData.pyramid(sindx).sd_data(:,:,:,:)=tmp;
                    else
                        FeatureData.pyramid(sindx).sd_data(:,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=res.data(zero_value_mask,:,:,:);
                    end;
                end;

                if ~exist('saliency_map','var')
                    FeatureData.saliency_map(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=max((m-local_mean)./(std_dev+epsilon_saliency),FeatureData.saliency_map(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6)));
                end;
                used_mem=max(mhs_getmem(),used_mem);
        end;
        used_mem=max(mhs_getmem(),used_mem);
    end;     
    
     FeatureData.saliency_map=max( FeatureData.saliency_map,0);
 
    clear FH;
    
    clear SD FH res 
        
    mondeg=[poldeg,0,0,0];
    even=false;
    pm=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);


    used_mem=max(mhs_getmem(),used_mem);
    prop_min=0.001;
    if (saliancy_offset>-1)
        assert(~(saliancy_offset>1));
        prop_min=saliancy_offset;
    end;
    
    if debug
        FeatureData.debug_saliency_map=FeatureData.saliency_map;
    end;
    
    if ~exist('saliency_map','var')
        FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    end;
    
    if exist('mask','var')
        FeatureData.saliency_map=FeatureData.saliency_map.*mask;
        if debug
            FeatureData.debug_saliency_map=FeatureData.debug_saliency_map.*mask;
        end;

    end;
   
    
    
    
    
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));

    
if poldeg>1    
    radfunc=@(x)(x);
    positions=radfunc(Scales_pol);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);
    
    if debug
        FeatureData.debug_img_slocal_sdv=img_slocal_sdv;
        FeatureData.debug_epsilon=epsilon;
    end;

    
    for sindx=1:nscales
        if ~adaptive_normalization
            img_slocal_sdv(sindx,:,:,:)=1./(img_slocal_sdv(sindx,:,:,:)+epsilon(sindx)+eps);
        else
            img_slocal_sdv(sindx,:,:,:)=1./(img_slocal_sdv(sindx,:,:,:));
        end;
    end;
    
    FeatureData.alphas_sdv=reshape(sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat),[poldeg+1,cshape]);
    
    if debug
        FeatureData.debug_img_slocal_mean=img_slocal_mean;
    end;
    
    FeatureData.alphas_mean=reshape(sta_matmult(squeeze(img_slocal_mean(:,:)),pimat),[poldeg+1,cshape]);
   
    
end;
    
    

    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);
    
    if exist('override_scales')
        assert(numel(override_scales)==2);
        assert(override_scales(1)<override_scales(2));
        FeatureData.override_scales=cast(override_scales,classid);
    end;

    if ~use_pyramid
        FeatureData.datafunc=cast(11,classid);
    else
        FeatureData.datafunc=cast(10,classid);
    end;
    
    if max(abs(element_size-1))<eps
        FeatureData.cshape=cast(cshape,classid);
        FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    else
        
        cshape=ceil(cshape.*element_size);
        
        FeatureData.cshape=class(cshape,classid);
        FeatureData.img=scaleimage3D(img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),cshape,true);
    end;
        
    
    if pad
        FeatureData.shape_org=cshape;
        FeatureData.shape_new_offset=[0,0,0];
        if reliable_boundary
            FeatureData.shape_boundary=single(crop);
        end;
    else
        FeatureData.shape_org=shape;
        FeatureData.shape_new_offset=crop;
    end;
    FeatureData.used_mem=max(mhs_getmem(),used_mem);
 
    
function [ROIS,workers]=rois_from_shape_q(shape,workers)

    q=floor((workers^(1/3)));

    
    for dim=1:3
        stepsize=floor((shape(dim)/q));
        R=[];
        for w=1:q
            R=[R;[(w-1)*stepsize,stepsize]];
        end;
        %filling last gab
        R(end,2)=shape(dim)-R(end,1);
        Rq(dim).R=R;
    end;
    ROIS=[];
    for qx=1:q
        for qy=1:q
            for qz=1:q
                ROIS=[ROIS;[Rq(1).R(qx,1),Rq(2).R(qy,1),Rq(3).R(qz,1),Rq(1).R(qx,2),Rq(2).R(qy,2),Rq(3).R(qz,2)]];
            end;
        end;
    end;    
    
    ROIS(:,1:3)=ROIS(:,1:3)+1;
    workers=q^3;
    
    
    
 
    