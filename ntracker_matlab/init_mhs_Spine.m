function [FeatureData]=init_mhs_Spine(img,A,varargin)

    crop=[10,10,10];
    epsilon=0.00000001;
    Scales_fac=1;
    poldeg=3;
    L=4;
    Ldata=2;
    maxit=50;
    debug=true;
    maxdist=10;
    
    steerable_conv_fact=1.25;
       
    
    scale_range=[1.15,8];
    nscales=4;

    saliancy_offset=-1;
    
    element_size=[1,1,1];
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    if ~exist('sd_element_size','var')
        sd_element_size=element_size;
    end;
    
    nscales_pol=nscales;
       
    element_size=element_size/min(element_size);
    
    pad=max(crop)<0;
    
    classid=class(img);
    shape=size(img);
    
    if pad
        border=-crop;
        crop=border;
        shape=shape+2*border;
        img_org=img;
        img=padarray(img,border,'symmetric');
    else
        img_org=img;
    end;
    
    scale_stepsize=(scale_range(2)-scale_range(1))/(nscales-1);
    Scales=scale_range(1):scale_stepsize:scale_range(2);
    
    
    scale_stepsize_pol=(scale_range(2)-scale_range(1))/(nscales_pol-1);
    Scales_pol=scale_range(1):scale_stepsize_pol:scale_range(2);
    
    
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);

    nscales=numel(Scales);
    nscales_pol=numel(Scales_pol);
    
    poldeg=min(poldeg,nscales_pol);

    
    cshape=shape-2*crop;
    assert(min(cshape)>0);

   
    
   
    FeatureData.scales=Scales;
    FeatureData.saliency_map=zeros(cshape,classid);
   
    
    
    
    
    FeatureData.img_data=zeros([nscales,cshape],classid);  
    
    
    
    
    
    
    %FeatureData.sd_data=zeros([nscales,2*num_cmp,cshape],classid);  
    FeatureData.saliency_map=zeros(cshape,classid);  
    
    
    
    fprintf('computing saliency map: ');
            
    

if (true)        
     img_slocal_sdv=zeros([nscales_pol,cshape],classid);
    
    
    
         for sindx=1:nscales
            scale=Scales(sindx);
            %N=1/scale^3;
            %N=1/((2*pi*scale^2)^(3/2));
            %tmp=N*mhs_smooth_img(img,scale,'normalize',false,'element_size',element_size);
             
            tmp=mhs_smooth_img(img,scale,'normalize',true,'element_size',element_size);
            
            sigmanorm=max(scale*sqrt(2),2);
            std_dev=mhs_compute_std_mask(tmp,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',true,'element_size',element_size);
            
            img_slocal_sdv(sindx,:,:,:)=std_dev;
            
            tmp2=-squeeze(sum(Lap(tmp,0,'crop',crop,'element_size',element_size),1));
            
            FeatureData.saliency_map=max((tmp2).*std_dev,FeatureData.saliency_map);
            FeatureData.img_data(sindx,:,:,:)=tmp(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
        end;
        
end;    
    
%      
    
    clear FH;
    
    clear SD FH res 
        
    mondeg=[poldeg,0,0,0];
    even=false;
    pm=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);


    
    prop_min=0.001;
    if (saliancy_offset>-1)
        assert(~(saliancy_offset>1));
        prop_min=saliancy_offset;
    end;
    
    
    %maxdist=2*max(Scales);
    
    FeatureData.dist=cast(mhs_distmap(A,A.cshape,maxdist,true),classid);
    FeatureData.maxdist=cast(maxdist,classid);
    
    FeatureData.saliency_map=FeatureData.saliency_map.*(FeatureData.dist<maxdist);
    
    FeatureData.debug_saliency_map=FeatureData.saliency_map;
    
    
    
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    
    
    if exist('mask','var')
        FeatureData.saliency_map=FeatureData.saliency_map.*mask;
        FeatureData.debug_saliency_map=FeatureData.debug_saliency_map.*mask;
    end;
   
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));

    
if poldeg>1    
    radfunc=@(x)(x);
    positions=radfunc(Scales_pol);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);
    
    
    FeatureData.alphas_sdv=reshape(sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat),[poldeg+1,cshape]);
    
    if debug
        FeatureData.debug_img_slocal_sdv=img_slocal_sdv;
    end;
    
end;
    
    

    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);
    
    if exist('override_scales')
        assert(numel(override_scales)==2);
        assert(override_scales(1)<override_scales(2));
        FeatureData.override_scales=cast(override_scales,classid);
    end;

    FeatureData.datafunc=cast(12,classid);
    
    if max(abs(element_size-1))<eps
        FeatureData.cshape=cast(cshape,classid);
        FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    else
        
        cshape=ceil(cshape.*element_size);
        
        FeatureData.cshape=class(cshape,classid);
        FeatureData.img=scaleimage3D(img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),cshape,true);
    end;
        
    
    if pad
        FeatureData.shape_org=cshape;
        FeatureData.shape_new_offset=[0,0,0];
        FeatureData.shape_boundary=single(crop);
    else
        FeatureData.shape_org=shape;
        FeatureData.shape_new_offset=crop;
    end;
    
   
%     B.options=A.options;
%     B.cshape=A.cshape;
%     B.scales=A.scales;
%     B.W(1).A=A;
%     B.W(1).shape=A.cshape;
%     B.W(1).offset=[0,0,0];
%     B.W(1).offline=true;
%     B.W(2).shape=A.cshape;
%     B.W(2).offset=[0,0,0];
    
    
    
