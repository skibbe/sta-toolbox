function Img=mhs_denoise(Img,g)

if nargin<2
%Img=scaleimage3D(mhs_smooth_img(scaleimage3D(Img,size(Img)*2,false,'spline'),1.5,'normalize',true),size(Img),false,'spline');
    Img=scaleimage3D(real(sqrt(mhs_smooth_img(scaleimage3D(Img,size(Img)*2,false,'spline'),1.5,'normalize',true))),size(Img),false,'spline');
else
    Img=scaleimage3D(real((mhs_smooth_img(scaleimage3D(Img,size(Img)*2,false,'spline'),1.5,'normalize',true)).^g),size(Img),false,'spline');
end;