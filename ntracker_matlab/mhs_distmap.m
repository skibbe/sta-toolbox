function D=mhs_distmap(A,shape,varargin)



    bdirs30 = ...
    [0.231033,0.044775,0.971915;... 
    -0.253819,0.180376,0.950284;... 
    -0.118215,-0.313659,0.942148;... 
    0.336252,-0.379082,0.862109;... 
    0.243317,0.461612,0.853060;... 
    -0.186844,0.588051,0.786947;... 
    -0.527216,-0.168503,0.832857;... 
    -0.463819,-0.591655,0.659406;... 
    0.013046,-0.712591,0.701458;... 
    0.707691,-0.219655,0.671509;... 
    0.659146,0.221436,0.718674;... 
    0.102148,0.844197,0.526210;... 
    -0.559318,0.599952,0.572033;... 
    -0.725951,0.215301,0.653177;... 
    -0.837022,-0.232879,0.495138;... 
    0.066256,-0.942950,0.326276;... 
    0.506532,-0.668558,0.544477;... 
    0.943509,-0.048353,0.327800;... 
    0.854669,0.425959,0.296816;... 
    0.526385,0.656409,0.540413;... 
    -0.299857,0.907974,0.292692;... 
    -0.729483,0.663391,0.166634;... 
    -0.938122,0.263361,0.224872;... 
    -0.785015,-0.581423,0.213772;... 
    -0.393033,-0.861877,0.320457;... 
    0.462813,-0.874533,0.144897;... 
    0.840095,-0.484393,0.244137;... 
    0.984120,0.160544,-0.075723;... 
    0.568774,0.812348,0.128791;... 
    0.129253,0.987494,0.090278];

    maxdist=3;
    consider_scale=false;

    sigma=2.5;
    gamma=1;
    mode=0;
    lininterp=true;
    vscale=[1,1,1];
    weight=false;
    normalize=true;
    
    sigma_w = -1;
    L = 4;
    sh_threshold = -1;
    dir_array=[];
    
    offset=[0,0,0];
    for k = 1:2:length(varargin)
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end


    if ~exist('pathid','var')
       idx=reshape((A.connections(1:2,:)+1),[1,2*size(A.connections,2)]);
       dataP=A.data([1:3,8,21],idx);
    else
       path_ids=A.data(21,:);
       valid=(path_ids(A.connections(1,:)+1)==path_id);
       idx=reshape((A.connections(1:2,valid)+1),[1,2*sum(valid)]);
       dataP=A.data([1:3,8,21],idx);
    end;


    if (mode==-1)
        D=mhs_distmapC(single(dataP),{'shape',shape([3,2,1]),'normalize',normalize,'b',maxdist,'lininterp',lininterp,'gamma',gamma,'sigma',sigma,'consider_scale',consider_scale,'mode',mode,'vscale',vscale([3,2,1]),'offset',offset([3,2,1]),'weight',weight,'sigma_w',sigma_w,'L',L,'sh_threshold',sh_threshold,'dirs',single(bdirs30')});
    else
        if mode==3
            D=mhs_distmapC(single(dataP),{'shape',shape([3,2,1]),'normalize',normalize,'b',maxdist,'lininterp',lininterp,'gamma',gamma,'sigma',sigma,'consider_scale',consider_scale,'mode',mode,'vscale',vscale([3,2,1]),'offset',offset([3,2,1]),'weight',weight,'sigma_w',sigma_w,'L',L,'sh_threshold',sh_threshold,'dir_array',dir_array});
        else
            
            D=mhs_distmapC(single(dataP),{'shape',shape([3,2,1]),'normalize',normalize,'b',maxdist,'lininterp',lininterp,'gamma',gamma,'sigma',sigma,'consider_scale',consider_scale,'mode',mode,'vscale',vscale([3,2,1]),'offset',offset([3,2,1]),'weight',weight,'sigma_w',sigma_w,'L',L,'sh_threshold',sh_threshold});
        end
    end


if 0
   %%
    bdirs30 = ...
    [0.231033,0.044775,0.971915;... 
    -0.253819,0.180376,0.950284;... 
    -0.118215,-0.313659,0.942148;... 
    0.336252,-0.379082,0.862109;... 
    0.243317,0.461612,0.853060;... 
    -0.186844,0.588051,0.786947;... 
    -0.527216,-0.168503,0.832857;... 
    -0.463819,-0.591655,0.659406;... 
    0.013046,-0.712591,0.701458;... 
    0.707691,-0.219655,0.671509;... 
    0.659146,0.221436,0.718674;... 
    0.102148,0.844197,0.526210;... 
    -0.559318,0.599952,0.572033;... 
    -0.725951,0.215301,0.653177;... 
    -0.837022,-0.232879,0.495138;... 
    0.066256,-0.942950,0.326276;... 
    0.506532,-0.668558,0.544477;... 
    0.943509,-0.048353,0.327800;... 
    0.854669,0.425959,0.296816;... 
    0.526385,0.656409,0.540413;... 
    -0.299857,0.907974,0.292692;... 
    -0.729483,0.663391,0.166634;... 
    -0.938122,0.263361,0.224872;... 
    -0.785015,-0.581423,0.213772;... 
    -0.393033,-0.861877,0.320457;... 
    0.462813,-0.874533,0.144897;... 
    0.840095,-0.484393,0.244137;... 
    0.984120,0.160544,-0.075723;... 
    0.568774,0.812348,0.128791;... 
    0.129253,0.987494,0.090278];
    

    bdirs = bdirs30;
    
    if 0
        L = 6;
        imgR=mhs_distmap(data.GT,size(data.Img),'mode',-1);
    
        abs_signal = sum(imgR,4)+eps;
        for d=1:size(imgR,4)
            imgR(:,:,:,d) = imgR(:,:,:,d)./abs_signal;
        end
    else
        L = 4;
    end
    if size(imgR,1)~=size(bdirs,1)
        imgR = permute(imgR,[4,1,2,3]);
    end
    

    ofield = discrete2sh(imgR,bdirs(:,[1,2,3])','L',L);

    shape = size(ofield);
    shape = shape(2:4);


    field=stafieldStruct([shape],L,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_R','single');

    field.data(1,:,:,:,:) = real(ofield);
    field.data(2,:,:,:,:) = imag(ofield);

    %field.data(:,2:4,:,:,:) = -field.data(:,2:4,:,:,:);
    %field.data(:,2:4,:,:,:) = -field.data(:,2:4,:,:,:);
    %field.data(:,2:end,:,:,:) = -field.data(:,2:end,:,:,:);

    %field.data(:,1,:,:,:) = 0;
    %field.data(:,2:end,:,:,:) = -field.data(:,2:end,:,:,:);
    %field.data(:,2:end,:,:,:) = -field.data(:,2:end,:,:,:);


    sta_glyph(field,'gamma',-0.5,'init_view',3);
    %bg = cat(3,sqrt(squeeze(sum(buffer_dmri,1))),sqrt(squeeze(sum(buffer_dmri,1))),sqrt(squeeze(sum(buffer_dmri,1))));
    %sta_glyph(field,'scval',0,'init_view',3);
    axis equal
    
end


if 0
    maxdist=3;
    consider_scale=false;

    sigma=2.5;
    gamma=1;
    mode=0;
    lininterp=true;
    vscale=[1,1,1];
    weight=false;
    normalize=true;
    
    offset=[0,0,0];
    for k = 1:2:length(varargin)
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end


    if ~exist('pathid','var')
       idx=reshape((A.connections(1:2,:)+1),[1,2*size(A.connections,2)]);
       dataP=A.data([1:3,8,21],idx);
    else
       path_ids=A.data(21,:);
       valid=(path_ids(A.connections(1,:)+1)==path_id);
       idx=reshape((A.connections(1:2,valid)+1),[1,2*sum(valid)]);
       dataP=A.data([1:3,8,21],idx);
    end;


    D=mhs_distmapC(single(dataP),{'shape',shape([3,2,1]),'normalize',normalize,'b',maxdist,'lininterp',lininterp,'gamma',gamma,'sigma',sigma,'consider_scale',consider_scale,'mode',mode,'vscale',vscale([3,2,1]),'offset',offset([3,2,1]),'weight',weight});

end


% 
% 
% 
% % if nargin<3
% %     maxdist=5;
% % end;
% % 
% % if nargin<4
% %     consider_scale=false;
% % end;
% 
%   maxdist=3;
%   consider_scale=false;
% 
% if ~exist('pathid','var')
%    idx=reshape((A.connections(1:2,:)+1),[1,2*size(A.connections,2)]);
%    dataP=A.data([1:3,8,21],idx);
% else
%    path_ids=A.data(21,:);
%    valid=(path_ids(A.connections(1,:)+1)==path_id);
%    idx=reshape((A.connections(1:2,valid)+1),[1,2*sum(valid)]);
%    dataP=A.data([1:3,8,21],idx);
% end;
% 
% 
% 
% if nargout>1
%     [D,C]=mhs_distmapC(single(dataP),{'shape',shape([3,2,1]),'b',maxdist,'consider_scale',consider_scale,'rgb',true});
% 
% else
%     D=mhs_distmapC(dataP,{'shape',shape([3,2,1]),'b',maxdist,'consider_scale',consider_scale,'rgb',false});
% end
