%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3D_Rayburst(img,varargin)



    crop=[10,10,10];
    epsilon=0.01;
    Scales_fac=1.5;
    poldeg=3;
    sigmanorm=3;
    epsilon=0.05;
    gvf_iters=10;
    
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    
%%    

    classid=class(img);
    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);
    
    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
    %FeatureData.gimg=Grad(img,0,'crop',crop);
    imgGVF=GVF3D(img,0.05,gvf_iters);
    FeatureData.gimg=permute(cast(imgGVF(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3),:),classid),[4,1,2,3]);
    
    
    stdv=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon);
    
    
    FeatureData.gimg=FeatureData.gimg.*reshape(repmat(reshape(stdv(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),[1,prod(cshape)]),[3,1]),[3,cshape]);
    %stdv=mhs_compute_std_mask(squeeze(sqrt(sum(imgGVF.^2,4))),'sigman',sigmanorm,'epsilon',epsilon);
    %%FeatureData.stdv=stdv(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
    
    if exist('Mask','var')
        Mask=single(Mask(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)));
        FeatureData.gimg=FeatureData.gimg.*reshape(repmat(reshape(Mask,[1,prod(cshape)]),[3,1]),[3,cshape]);
        FeatureData.img=FeatureData.img.*Mask;
    end;
    
    
    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(1,classid);
    
    FeatureData.saliency_map=zeros(cshape,classid);
    
    
    
   FeatureData.scales=cast(Scales,classid);
   FeatureData.Scales_fac=cast(Scales_fac,classid);
   FeatureData.datafunc=cast(5,classid);
   FeatureData.cshape=cast(cshape,classid);
   
   
   

    FeatureData.saliency_map=FeatureData.img.*(FeatureData.img>0);
    assert(~(min(FeatureData.saliency_map(:))<0));
    prop_min=0.1;
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;

    accimg=FeatureData.saliency_map;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    for a=2:numel(accimg),
        accimg(a)=accimg(a)+accimg(a-1);
    end;

    accimg=accimg./((accimg(end)));
    FeatureData.saliency_map_acc=accimg;
    

    FeatureData.datafunc=cast(6,classid);
    
    
    
    
    
    
    
 
    