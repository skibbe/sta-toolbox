function [points,pointsNN]=spoints(N)

assert(N<6);
if ~exist(['~/.poitsphere',num2str(N),'.mat'])
        clear faces


        faces(1).v=single([1,0,0;
                    0,1,0;
                    0,0,1]);

        faces(2).v=single([1,0,0;
                    0,1,0;
                    0,0,-1]);

        faces(3).v=single([1,0,0;
                    0,-1,0;
                    0,0,1]);

        faces(4).v=single([1,0,0;
                    0,-1,0;
                    0,0,-1]);

        faces(5).v=single([-1,0,0;
                    0,1,0;
                    0,0,1]);

        faces(6).v=single([-1,0,0;
                    0,1,0;
                    0,0,-1]);

        faces(7).v=single([-1,0,0;
                    0,-1,0;
                    0,0,1]);

        faces(8).v=single([-1,0,0;
                    0,-1,0;
                    0,0,-1]);      




        for a=1:N

            figure(2131223);
            clf;
            hold on;
            for n=1:numel(faces)
            fa=[faces(n).v,faces(n).v(:,1)];
            plot3(fa(1,:),fa(2,:),fa(3,:),'b-');
            end;
            for n=1:numel(faces)
            plot3(faces(n).v(1,:),faces(n).v(2,:),faces(n).v(3,:),'g.');
            end;
            clear faces2
            cn=numel(faces);
            fc=1;
            for b=1:cn
                %for s1=0:1
                    %for s2=0:1
                        %for s3=0:1
                            %faces2(fc).v=[((1-0.5*s1)*faces(b).v(:,1)+(0.5*s1)*faces(b).v(:,2)),...
                            %    ((1-0.5*s2)*faces(b).v(:,2)+(0.5*s2)*faces(b).v(:,3)),...
                            %    ((1-0.5*s3)*faces(b).v(:,3)+(0.5*s3)*faces(b).v(:,1))];
                            faces2(fc).v=[((1-0.5*0)*faces(b).v(:,1)+(0.5*0)*faces(b).v(:,2)),...
                                ((1-0.5*1)*faces(b).v(:,1)+(0.5*1)*faces(b).v(:,2)),...
                                ((1-0.5*1)*faces(b).v(:,1)+(0.5*1)*faces(b).v(:,3))];
                            for t=1:3
                                faces2(fc).v(:,t)=faces2(fc).v(:,t)./norm(faces2(fc).v(:,t));
                            end;
                            fc=fc+1;

                            faces2(fc).v=[((1-0.5*0)*faces(b).v(:,2)+(0.5*0)*faces(b).v(:,2)),...
                                ((1-0.5*1)*faces(b).v(:,2)+(0.5*1)*faces(b).v(:,3)),...
                                ((1-0.5*1)*faces(b).v(:,2)+(0.5*1)*faces(b).v(:,1))];
                            for t=1:3
                                faces2(fc).v(:,t)=faces2(fc).v(:,t)./norm(faces2(fc).v(:,t));
                            end;
                            fc=fc+1;

                            faces2(fc).v=[((1-0.5*0)*faces(b).v(:,3)+(0.5*0)*faces(b).v(:,2)),...
                                ((1-0.5*1)*faces(b).v(:,3)+(0.5*1)*faces(b).v(:,1)),...
                                ((1-0.5*1)*faces(b).v(:,3)+(0.5*1)*faces(b).v(:,2))];
                            for t=1:3
                                faces2(fc).v(:,t)=faces2(fc).v(:,t)./norm(faces2(fc).v(:,t));
                            end;
                            fc=fc+1;     

                            faces2(fc).v=[((1-0.5*1)*faces(b).v(:,1)+(0.5*1)*faces(b).v(:,2)),...
                                ((1-0.5*1)*faces(b).v(:,2)+(0.5*1)*faces(b).v(:,3)),...
                                ((1-0.5*1)*faces(b).v(:,3)+(0.5*1)*faces(b).v(:,1))];
                            for t=1:3
                                faces2(fc).v(:,t)=faces2(fc).v(:,t)./norm(faces2(fc).v(:,t));
                            end;
                            fc=fc+1;                         

                        %end;
                 %   end;
                %end;
            end;
            faces=faces2;

        end;


        vv=[faces.v]';
        clear faces
        dmat=distmat(vv,vv);
        vindx=ones(1,size(vv,1));
        for a=1:size(vv,1) 
            indx=find(dmat(:,a)==0);
            if numel(indx)>1
                vindx(indx(2:end))=0;
            end;
        end;
        points=vv(vindx==1,:)';


            figure(2131224);
            clf;
            hold on;

            plot3(points(1,:),points(2,:),points(3,:),'b.');
    
    save(['~/.poitsphere',num2str(N),'.mat'],'points');
else
    load(['~/.poitsphere',num2str(N),'.mat']);
end;
    
    fprintf('npoints %d\n',size(points,2));
if nargout>1
    dmat=distmat(points',points');
    [B,pointsNN] = sort(dmat,1,'ascend');
    % NN dist:
    % sum((repmat(points(:,1),1,128)-points(:,pointsNN(:,1))).^2)
%     
%     figure(21312223);clf;hold on;
%     select=randi(size(points,2));
%     PID=N*N*N;
%     nnid=pointsNN(:,select);
%     nn=(nnid(2:PID));
%     enn=(nnid(PID+1:end));
%     plot3(points(1,enn),points(2,enn),points(3,enn),'r.');
%     plot3(points(1,select),points(2,select),points(3,select),'g.');
%     plot3(points(1,nn),points(2,nn),points(3,nn),'b.');
end;



