
function tensor = Lap(inputimg,sigma,varargin)

supergauss=false;
normalize=false;
crop=[0,0,0];
psf=[1,1,1];
element_size=[1,1,1];

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

shape=size(inputimg);

element_size=element_size./psf;


if numel(shape)==3,
    cshape=shape-2*crop;

%     if exist('element_size','var')
%         sigma=sigma./element_size;
%     else
%         element_size=[1,1,1];
%     end;

    if (nargin>1) && (sum(abs(sigma))>0)
        inputimg=mhs_smooth_img(inputimg,sigma,'normalize',normalize,'supergauss',supergauss,'element_size',element_size);
    end;

    wxx=1/element_size(1)^2;
    wyy=1/element_size(2)^2;
    wzz=1/element_size(3)^2;

    tensor_=zeros([3,shape],class(inputimg));

    kernel = zeros(3,3,3);
    kernel(2,2,2) = -2; kernel(1,2,2) = 1; kernel(3,2,2) = 1;
    tensor_(1,:,:,:) = imfilter(inputimg,kernel*wxx,'circular');
    tensor_(2,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wyy,'circular');
    tensor_(3,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wzz,'circular');
    
    tensor=tensor_(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));


elseif numel(shape)==2,
    cshape=shape-2*crop(1:2);


    if (nargin>1) && (sum(abs(sigma))>0)
        inputimg=mhs_smooth_img(inputimg,sigma,'normalize',normalize);
    end;

    wxx=1/element_size(1)^2;
    wyy=1/element_size(2)^2;

    tensor_=zeros([2,shape],class(inputimg));
    
    kernel = zeros(3,3);
    kernel(2,2) = -2; kernel(1,2) = 1; kernel(3,2) = 1;
    tensor_(1,:,:) = imfilter(inputimg,kernel*wxx,'circular');
    tensor_(2,:,:) = imfilter(inputimg,permute(kernel,[2 1])*wyy,'circular');
    tensor=tensor_(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2));
end