function I=ntracker_viewer_texturedata(Img,varargin)


    s=2000; 
   t=0; 
   crop=[0,0,0]; 
element_size=[1,1,1];

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


if iscell(Img)
    for a=1:numel(Img)
        if ~isempty(Img{a})
            shape=size(Img{a});
        end;
    end;
    
    if nargin>1
        img=zeros([3,shape-2*crop],'single');
    else
        img=zeros([3,shape],'single');
    end;
    for a=1:numel(Img)
        if ~isempty(Img{a})
            if nargin>1
                img(a,:,:,:)=single(Img{a}(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)));
            else
                img(a,:,:,:)=single(Img{a});    
            end;        
        end;
    end;
    
    [I.texture3D,I.light_texture3D,I.img]=mhs_create3DtextureNPOT(img,'t',t,'s',s);
    I.cshape=single(size(I.img));
    
else
    if nargin>1
        I.img=single(Img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)));
    else
        I.img=single(Img);    
    end;

    I.cshape=single(size(I.img));
    
    [I.texture3D,I.light_texture3D]=mhs_create3DtextureNPOT(I.img,'t',t,'s',s);
end;

if max(abs(element_size-1))>eps    
    element_size=element_size/min(element_size);
    I.cshape=ceil(I.cshape.*element_size);
    I.cshape=single(I.cshape);
    I.img=scaleimage3D(I.img,I.cshape,true);
end;    
    
