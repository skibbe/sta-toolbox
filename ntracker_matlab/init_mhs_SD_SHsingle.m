function [FeatureData]=init_mhs_SD_SHsingle(img,varargin)

    crop=[10,10,10];
    epsilon=0.01;
    epsilon_saliency=0.1;
    
    poldeg=3;
    L=4;
    Ldata=4;
    maxit=50;
    debug=false;
    
    
    steerable_conv_fact=1.25;
    use_pyramid=false;
    
    scale=1.5;

    saliancy_offset=-1;
    
    element_size=[1,1,1];
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    if ~exist('sd_element_size','var')
        sd_element_size=element_size;
    end;
    
    element_size=element_size/min(element_size);
    
    pad=max(crop)<0;
    
    classid=class(img);
    shape=size(img);
    
    if pad
        border=-crop;
        crop=border;
        shape=shape+2*border;
        img_org=img;
        img=padarray(img,border,'symmetric');
    else
        img_org=img;
    end;
    
    Scales=scale;
    
    cshape=shape-2*crop;
    assert(min(cshape)>0);
   
    FeatureData.scales=Scales;
    FeatureData.saliency_map=zeros(cshape,classid);
    
    if exist('saliency_map','var')
        FeatureData.saliency_map(:)=saliency_map(:);
    end;
   
    if ~exist('Ldata','var')
        Ldata=L;
    end;
    
    assert(Ldata<=L);
    assert(mod(Ldata,2)==0);
    
    
 
    
    num_cmp=((Ldata+2)*(Ldata+2))/4;
    zero_value_mask=zeros(1,num_cmp*2)<1;
    Ls=[0:2:Ldata];
    Ls=(((Ls).*(Ls))./4+(Ls+1))*2;
    zero_value_mask(Ls)=false;
    
    num_cmp_org=num_cmp;
    num_cmp=num_cmp*2-numel(Ls);
    
    
    
    FeatureData.sd_data=zeros([num_cmp,cshape],classid);  
    FeatureData.L=cast(Ldata,classid);
    
    
    fprintf('computing saliency map: ');
            
    if exist('workers','var')
        [ROIS,workers]=rois_from_shape_q(cshape,workers);
    else
        workers=1;
    end;

    
    
    img_slocal_sdv=zeros([cshape],classid);
    img_slocal_mean=zeros([cshape],classid);
    
	for w=1:workers
        if workers>1
            R0=[ROIS(w,1:3),ROIS(w,1:3)+ROIS(w,4:6)-1];
            R=R0+[crop,crop];
            
            image=stafieldStruct(img(R(1)-crop(1):R(4)+crop(1),  R(2)-crop(2):R(5)+crop(2),  R(3)-crop(3):R(6)+crop(3)));     
            sub_shape=R0(4:6)-R0(1:3)+1;
        else
            image=stafieldStruct(img);
            R=[1,1,1,cshape];
            R0=R;
            sub_shape=cshape;
        end;


                res = sta_steerdeconv(image,'tolerance',0.001,'maxit',maxit,'L',L,'element_size',sd_element_size,'sigma',steerable_conv_fact*scale);
                
                fprintf('computing local maxima\n');
                [H,m] = getMinMax(res.data,res.L,1,true,res.type,res.storage);
                
                m=squeeze(m(2,:,:,:));
                
                if exist('fix_sigma','var')
                    sigmanorm=fix_sigma;
                else
                    sigmanorm=max(scale*sqrt(2),2);
                end;
                %[std_dev,local_mean]=mhs_compute_std_mask(m,'sigman',sigmanorm,'epsilon',epsilon(sindx),'crop',crop,'inv',true,'element_size',element_size);
                
                fprintf('normalization\n');
                [std_dev,local_mean]=mhs_compute_std_mask(m,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',false,'element_size',element_size);
                
                fprintf('....\n');
                
                img_slocal_sdv(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=std_dev;
                
                img_slocal_mean(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=local_mean;

                m=m(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
                
   
                
                res.data=reshape(res.data(:,1:num_cmp_org,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),[num_cmp_org*2,cshape]);
                    FeatureData.sd_data(:,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=res.data(zero_value_mask,:,:,:);
                    
                if ~exist('saliency_map','var')
                    FeatureData.saliency_map(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=max((m-local_mean)./(std_dev+epsilon_saliency),0);
                end;
        
    end;     
    
    clear FH;
    
    clear SD FH res 

    
    prop_min=0.001;
    if (saliancy_offset>-1)
        assert(~(saliancy_offset>1));
        prop_min=saliancy_offset;
    end;
    
    if debug
        FeatureData.debug_saliency_map=FeatureData.saliency_map;
    end;
    
    if ~exist('saliency_map','var')
        FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    end;
    
    if exist('mask','var')
        FeatureData.saliency_map=FeatureData.saliency_map.*mask;
        if debug
            FeatureData.debug_saliency_map=FeatureData.debug_saliency_map.*mask;
        end;

    end;
   
    
    
    
    
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));

    FeatureData.sd_data(1,:,:,:)=squeeze(FeatureData.sd_data(1,:,:,:))-local_mean;
    
    img_slocal_sdv=1./(img_slocal_sdv+epsilon+eps);
    
    for l=1:numel(FeatureData.sd_data,1), 
        FeatureData.sd_data(l,:,:,:)=squeeze(FeatureData.sd_data(l,:,:,:)).*img_slocal_sdv;
    end;
    
    FeatureData.scales=cast(Scales,classid);
    
    FeatureData.datafunc=cast(13,classid);

    
    if max(abs(element_size-1))<eps
        FeatureData.cshape=cast(cshape,classid);
        FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    else
        
        cshape=ceil(cshape.*element_size);
        
        FeatureData.cshape=class(cshape,classid);
        FeatureData.img=scaleimage3D(img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),cshape,true);
    end;
        
    
    if pad
        FeatureData.shape_org=cshape;
        FeatureData.shape_new_offset=[0,0,0];
        FeatureData.shape_boundary=single(crop);
    else
        FeatureData.shape_org=shape;
        FeatureData.shape_new_offset=crop;
    end;

 
    
function [ROIS,workers]=rois_from_shape_q(shape,workers)

    q=floor((workers^(1/3)));

    
    for dim=1:3
        stepsize=floor((shape(dim)/q));
        R=[];
        for w=1:q
            R=[R;[(w-1)*stepsize,stepsize]];
        end;
        %filling last gab
        R(end,2)=shape(dim)-R(end,1);
        Rq(dim).R=R;
    end;
    ROIS=[];
    for qx=1:q
        for qy=1:q
            for qz=1:q
                ROIS=[ROIS;[Rq(1).R(qx,1),Rq(2).R(qy,1),Rq(3).R(qz,1),Rq(1).R(qx,2),Rq(2).R(qy,2),Rq(3).R(qz,2)]];
            end;
        end;
    end;    
    
    ROIS(:,1:3)=ROIS(:,1:3)+1;
    workers=q^3;
    
    
    
 
    