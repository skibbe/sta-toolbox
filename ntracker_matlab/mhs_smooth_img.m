function [img,gaussin_ft]=mhs_smooth_img(img,sigma,varargin)  
supergauss=false;

normalize=false;
imagereal=true;
verbose=0;
zeropad=false;
pad=false;
psf=[1,1,1];
element_size=[1,1,1];
pad_style = 'symmetric';


for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;
classname=class(img);


sigma=sigma./element_size(1:numel(sigma));
sigma=sigma.*psf(1:numel(sigma));



assert((sum(abs(element_size-1)>eps)==0)||(numel(sigma)>1));
    
    

assert(~zeropad)

if zeropad
    shape=size(img);
    img_=zeros(shape+3*sigma,classname);
    if numel(size(img))==3
        img_(1:shape(1),1:shape(2),1:shape(3))=img;
    else
        img_(1:shape(1),1:shape(2))=img;
    end;
    img=img_;
end;

if pad
    shape=size(img);
    %img=padarray(img,shape+3*sigma,'symmetric','post');
    img=padarray(img,ceil(3*sigma),pad_style,'post');
end;



if ~exist('gaussin_ft')
    imgsz = size(img);
    dim=numel(imgsz);
    
    if (numel(sigma)==1)
        sigma=repmat(sigma,1,dim);
    end;
    
    switch dim
        case 2
        [X Y] = ndgrid(0:(imgsz(1)-1),0:(imgsz(2)-1));
        X = X - ceil(imgsz(1)/2);
        Y = Y - ceil(imgsz(2)/2);
        R2 = (X.^2/(2*sigma(1)^2) + Y.^2/(2*sigma(2)^2));
        case 3
        [X Y Z] = ndgrid(0:(imgsz(1)-1),0:(imgsz(2)-1),0:(imgsz(3)-1));
        X = X - ceil(imgsz(1)/2);
        Y = Y - ceil(imgsz(2)/2);
        Z = Z - ceil(imgsz(3)/2);
        if ~supergauss
            R2 = cast((X.^2/(2*sigma(1)^2) + Y.^2/(2*sigma(2)^2) + Z.^2/(2*sigma(3)^2)),classname);
        else
            R2 = cast((X.^4/(2*sigma(1)^2) + Y.^4/(2*sigma(2)^2) + Z.^4/(2*sigma(3)^2)),classname);
        end;
    end;
    clear X Y Z;
    
    gaussin = (fftshift(exp(-R2)));
    if normalize
        %gaussin=gaussin/cast((2*pi*Simga^2)^(dim/2),classname);
        gaussin=gaussin/cast(sqrt(((2*pi)^dim)*prod(sigma.^2)),classname);
    end;
    %gaussin=gaussin./sum(gaussin(:));
    gaussin_ft= fftn(gaussin);
    clear gaussin;
end;
if imagereal
    if verbose>0
        display('warning: assuming input is real valued!')
    end;
    %img=real(ifftn(fftn(img).*gaussin_ft,'symmetric'));    
    img=ifftn(fftn(img).*gaussin_ft,'symmetric');    
else
    img=ifftn(fftn(img).*gaussin_ft);    
end;
    
    if ~normalize
        if verbose>0
            display('warning: Gaussian not normalized!')
        end;
    end
    
if zeropad || pad
    if numel(size(img))==3
        img=img(1:shape(1),1:shape(2),1:shape(3));
    else
        img=img(1:shape(1),1:shape(2));
    end;
end;

