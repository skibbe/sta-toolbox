%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3D4(img,varargin)



crop=[10,10,10];
epsilon=0.01;
threshold=0.1;



Scales_fac=1.5;
trueSF=false;

MultiscaleMedialness=false;
MultiscaleMedialnessValue=false;

sigma_r=0.5;

Hough=false;
hough_gamma=-1;
tube_steps=3;
tube_length=2;

hough_std=false;
hough_sigman=3;
hough_epsilon=0.1;

poldeg=4;
ndensescales=5;
sigman=-1;

%normalization='NCC';
normalization='STD';
%normalization='STDM';
%normalization='NONE';

%normalization_saliency='NCC';
normalization_saliency='STD';


secondary_ev_system=false;
secondary_ev_system_full=false;
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    
if ~exist('poldeg_vdir')
    poldeg_vdir=poldeg
end

    
    %Scales=[Scales/Scales_fac,Scales];
    
    classid=class(img);
    threshold=cast(threshold,classid);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);


    nscales=numel(Scales);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

    sphere_min_max_res=[16,66];
    
    
    ndirs=0;
    for sindx=1:nscales
        %smpts=16*sindx;
        %smpts=16*ceil(Scales(sindx));
        smpts=ceil(sphere_min_max_res(1)+(sphere_min_max_res(2)-sphere_min_max_res(1))*(sindx-1)/(nscales-1));
        fprintf('nupts %d\n',smpts);
        %smpts=66;
        dirs=cast(sphere2pts(smpts,true),classid);
        FeatureData.ofield_dirs{sindx}=dirs;   
        ndirs=ndirs+smpts;
    end;


    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    FeatureData.scales=Scales;

    FeatureData.saliency_map=zeros(cshape,classid);
    
    FeatureData.saliency_map_acc=zeros([nscales,cshape],classid);
    
  

    
    % ofilter normalization
    if sigman==-1
        sigman=max(Scales);
    end;
    
    mean=mhs_smooth_img(img,sigman,'normalize',true);
    mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
    std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
    std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
    clear mean2
    if ~strcmp(normalization,'STDM')
        clear mean
    else
        mean=mean(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));    
    end;
    %if strcmp(normalization,'NONE')
    %    std_dev(:)=1;
    %end;
    
    
    
% COMPUTING HESSIAN
    for sindx=1:nscales
        scale=Scales(sindx);

        HField=Hessian(img,scale,'crop',crop);
        
        if ~Hough
            HRawField_back{sindx}=HField;
        end;
        
        Lap=(sum(HField(1:3,:,:,:),1));
        

        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;
        HField_back{sindx}=HField;
    end;
        
    
% COMPUTING EIGENVECTORS
    
    clear Lap
    for sindx=1:nscales
        scale=Scales(sindx);

        if strcmp(normalization,'STD')
            tmp=reshape(repmat(std_dev(:)',6,1),[6,cshape]);
            HField=HField_back{sindx}.*tmp;        
        else
            HField=HField_back{sindx};        
        end
        
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        
        myevec{sindx}=myevec_main;
        myev{sindx}=myev_main;
    end;
    
clear HField tmp myevec_main myevec_second myev_main myev_second 
    
    
    %
    %
    % COMPUTING DATA TERM
    %
    %  
    FeatureData.vesselimgD=zeros([ndirs,cshape],classid);
    
        if Hough
            
            
             offset=0;
             for sindx=1:nscales  
                scale=Scales(sindx);
                
%                 if sindx==1
%                     if hough_std
%                         if (hough_sigman>0)
%                             mean_h=mhs_smooth_img(img,hough_sigman,'normalize',true);
%                             mean2_h=mhs_smooth_img(img.^2,hough_sigman,'normalize',true);
%                             std_dev_h=cast(real(1./(sqrt(mean2_h-mean_h.^2)+(hough_epsilon+eps))),classid);
%                             std_dev_h=std_dev_h(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%                             GField=reshape(repmat(std_dev_h(:)',3,1),[3,cshape]).*Grad(img,-1,'crop',crop);
%                             clear mean_h mean2_h std_dev_h
%                         else
%                             GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,-1,'crop',crop);
%                         end;
%                     else
%                         GField=Grad(img,-1,'crop',crop);
%                     end;
%                     GM=squeeze(sqrt(sum(GField.^2,1))); 
%                 end;
                
                GField=scale*Grad(img,scale,'crop',crop,'normalize',true);
                DirImg=myevec{sindx}([1,2,3],:,:,:);
                DirImg=reshape(DirImg,[3,1,cshape]);
                tube_steps=5;
                tube_length=5;
                
                hough_dis_angle=70;
                hough_dis_angle_rad=2*pi*hough_dis_angle/360;
                accu_smooth_sigma=sqrt(2*scale^2*(1-cos(hough_dis_angle_rad)));
                accu_smooth_sigma=accu_smooth_sigma*1/(2*sqrt(2*log(2)));
                fprintf('angle %3.2f: sigma: %f\n',hough_dis_angle,accu_smooth_sigma);
                
                %accu_smooth_sigma=scale;
                %accu_smooth_sigma=1;
                hough_gamma=0.5;
                accu_smooth_sigma=1;
                
                params={'radius',scale,...
                    'sigma_v',accu_smooth_sigma,...%'sigma_v',sigma_v,...%'sigma_v',sqrt(scale/2),...%'sigma_v',sqrt(scale),...%'sigma_v',sigma_v,...%'sigma_v',(scale/2),...%sqrt(scale)/2,...%'sigma_v',sqrt(scale),...%sqrt(scale)/2,...
                    'sigma_v2',-1,...
                    'sigma_r',hough_gamma,...%'sigma_r',-1,...%'sigma_r',sigma_r,...
                    'mode',1,...
                    'tube_steps',tube_steps,...
                    'tube_length',tube_length,...%'tube_length',sqrt(scale)*tube_length,...'tube_length',tube_length,...%'tube_length',scale*tube_length,...%
                    'circle_mode',false,...
                    'min_mode',false};
        
                
                nHessian=cast(scale^2/(2*pi*scale^2)^(3/2),classid);
                
                
                
                vote=squeeze(mhs_medialness(GField,DirImg,params));
                
                [forgetme,hough_weights]=(sta_EVGSL(HField_back{sindx}*nHessian));
                myev{sindx}(1,:,:,:)=vote.*squeeze((1-exp(-hough_weights(1,:,:,:).^2/(2*hough_c^2))));
                clear forgetme hough_weights;

                GaussNfact=1;

                ev_offset=0; 
                OT=cat(1,cat(1,myevec{sindx}(1+ev_offset,:).^2,myevec{sindx}(2+ev_offset,:).^2,myevec{sindx}(3+ev_offset,:).^2),myevec{sindx}(1+ev_offset,:).*myevec{sindx}(2+ev_offset,:),myevec{sindx}(1+ev_offset,:).*myevec{sindx}(3+ev_offset,:),myevec{sindx}(2+ev_offset,:).*myevec{sindx}(3+ev_offset,:));
                OT=reshape(OT,[6,cshape]).*reshape(repmat(myev{sindx}(1,:)*(GaussNfact),6,1),[6,cshape]);
                

                dirs=FeatureData.ofield_dirs{sindx};
                FeatureData.vesselimgD(offset+1:offset+size(dirs,2),:,:,:)=sta_orientdist(OT,{'dirs',dirs});
                offset=offset+size(dirs,2);
             end;  
             
             clear GField DirImg vote GM forgetme hough_weights;
              
%              offset=0;
%              for sindx=1:nscales  
%                 scale=Scales(sindx);
%                 
%                 if strcmp(normalization,'STD')
%                     std_dev_local=std_dev;
%                     hough_g_scale=1.5;
%                     GField=reshape(repmat(std_dev_local(:)',3,1),[3,cshape]).*Grad(img,hough_g_scale,'crop',crop,'normalize',true);
%                 else
%                     hough_g_scale=1.5;
%                     GField=Grad(img,hough_g_scale,'crop',crop,'normalize',true);
%                 end;     
%                 
%                 DirImg=myevec{sindx}([1,2,3],:,:,:);
%                 DirImg=reshape(DirImg,[3,1,cshape]);
%                 params={'radius',scale,...
%                         'sigma_v',sqrt(scale),...%sqrt(scale)/2,...%'sigma_v',sqrt(scale),...%sqrt(scale)/2,...
%                         'sigma_r',hough_gamma,...
%                         'mode',1,...
%                         'tube_steps',tube_steps,...
%                         'tube_length',tube_length,...%'tube_length',scale*tube_length,...%
%                         'circle_mode',false,...
%                         'min_mode',false};
% 
%                 vote=mhs_medialness(GField,DirImg,params); 
%                 myev{sindx}(1,:)=vote(:);
% 
%                 GaussNfact=1;
% 
%                 ev_offset=0; 
%                 OT=cat(1,cat(1,myevec{sindx}(1+ev_offset,:).^2,myevec{sindx}(2+ev_offset,:).^2,myevec{sindx}(3+ev_offset,:).^2),myevec{sindx}(1+ev_offset,:).*myevec{sindx}(2+ev_offset,:),myevec{sindx}(1+ev_offset,:).*myevec{sindx}(3+ev_offset,:),myevec{sindx}(2+ev_offset,:).*myevec{sindx}(3+ev_offset,:));
%                 OT=reshape(OT,[6,cshape]).*reshape(repmat(myev{sindx}(1,:)*(GaussNfact),6,1),[6,cshape]);
%                 
% 
%                 dirs=FeatureData.ofield_dirs{sindx};
%                 FeatureData.vesselimgD(offset+1:offset+size(dirs,2),:,:,:)=sta_orientdist(OT,{'dirs',dirs});
%                 offset=offset+size(dirs,2);
%              end;            
           
            
    
    
        elseif MultiscaleMedialness
            offset=0;
            for sindx=1:nscales
                scale=Scales(sindx);

                GaussNfact=1/scale;
                if strcmp(normalization,'STD')
                    GField=reshape(repmat(std_dev(:)'*(GaussNfact),3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);       
                else
                    GField=Grad(img,scale,'crop',crop,'normalize',true)*(GaussNfact);       
                end
                
                dirs=FeatureData.ofield_dirs{sindx};
                GMField=squeeze(sqrt(sum(GField.^2,1)));
                VM=mhs_medialness(GMField,dirs,{'radius',scale,'sigma_r',sigma_r});
                for t=1:size(VM,1)
                 VM(t,:,:,:)=scale*(squeeze(VM(t,:,:,:))-GMField);
                end;
                %VM2=scale*VM2.*(VM2>0);
                
                FeatureData.vesselimgD(offset+1:offset+size(dirs,2),:,:,:)=VM;
                offset=offset+size(dirs,2);
            end;

        else
            %
            %
            % COMPUTING 1. EIGENVECTOR POLYNOM  
            %
            %
            offset=0;
             for sindx=1:nscales  
                scale=Scales(sindx);
                
                if MultiscaleMedialnessValue
                    if strcmp(normalization,'STD')
                        GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);       
                    else
                        GField=Grad(img,scale,'crop',crop,'normalize',true);       
                    end
                    GMField=squeeze(sqrt(sum(GField.^2,1)));
                    DirImg=myevec{sindx}([1,2,3],:,:,:);
                    %DirImg=myevec{sindx}([2,1,3],:,:,:);
                    DirImg=reshape(DirImg,[3,1,cshape]);
                    VM=squeeze(mhs_medialness(GMField,DirImg,{'radius',scale,'sigma_r',sigma_r}));
                    myev{sindx}(1,:)=sqrt(2)*scale*(VM(:)-GMField(:));
                else
                    GaussNfact=1/scale;
                end;                
                

                %GaussNfact=1/((2*pi)^(3/2));GaussNfact=GaussNfact/scale;
                

                ev_offset=0; 

                if trueSF
                    if strcmp(normalization,'STD')
                        tmp=reshape(repmat(std_dev(:)'*(GaussNfact),6,1),[6,cshape]);
                        OT=HField_back{sindx}([1,2,3,4,6,5],:,:,:).*tmp;      
%    display(max(OT(:)));                        
                    else
                        OT=HField_back{sindx}*(GaussNfact);        
                     
                    end
                else
                    OT=cat(1,cat(1,myevec{sindx}(1+ev_offset,:).^2,myevec{sindx}(2+ev_offset,:).^2,myevec{sindx}(3+ev_offset,:).^2),myevec{sindx}(1+ev_offset,:).*myevec{sindx}(2+ev_offset,:),myevec{sindx}(1+ev_offset,:).*myevec{sindx}(3+ev_offset,:),myevec{sindx}(2+ev_offset,:).*myevec{sindx}(3+ev_offset,:));
                    OT=reshape(OT,[6,cshape]).*reshape(repmat(myev{sindx}(1,:)*(GaussNfact),6,1),[6,cshape]);
                end;

                dirs=FeatureData.ofield_dirs{sindx};
                FeatureData.vesselimgD(offset+1:offset+size(dirs,2),:,:,:)=sta_orientdist(OT,{'dirs',dirs});
                offset=offset+size(dirs,2);
             end;
        end;    
     
    
        
    fprintf('polfit\n');
    %radfunc=@(x)sqrt(x);
    %radfunc=@(x)log(x)/log(Scales_fac);
    radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    FeatureData.Alphas=mhs_polfit(FeatureData,'mondeg',[8,8,8,0],'poldeg',poldeg,'radfunc',radfunc,'even',true);
    fprintf('done\n');
    
  

clear myevec  OT
if ~Hough
    clear myev
end;

FeatureData = rmfield(FeatureData,'vesselimgD');
clear myev_full myevec_full OT
    


% COMPUTING SALIENCY MAP

     for sindx=1:nscales        
        scale=Scales(sindx);           
            
        if Hough
            FH=squeeze(myev{sindx}(1,:,:,:));
        else

            if strcmp(normalization_saliency,'NCC')
                Imean=mhs_smooth_img(img,scale*sqrt(2),'normalize',true);
                ImgM=img-Imean;    
                tmp=mhs_smooth_img(ImgM.^2,scale,'normalize',false);
                tmp=tmp(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
                NI=1./(sqrt(tmp.*(tmp>0))+epsilon+eps);
                NH=cast(2*scale^2/sqrt(32*sqrt(2)*pi^(3/2)*scale^3),class(img));
                clear ImgM Imean tmp  
                HField=HField_back{sindx}.*reshape(repmat((NI(:).*NH(:))',6,1),[6,cshape]);    

             elseif strcmp(normalization_saliency,'STD')
                N=scale^2/((2*pi*scale^2)^(3/2));
                HField=N*HField_back{sindx}.*reshape(repmat(std_dev(:)',6,1),[6,cshape]);   
            else
                N=1/((2*pi*scale^2)^(3/2));
                HField=N*HField_back{sindx};   
            end;

            [myevec,myev]=(sta_EVGSL(HField));

            HField=HRawField_back{sindx};
            [myevec_second,myev_second]=(sta_EVGSL(HField));

            myev(1,:)=myev(1,:).*(myev(1,:)>0).*((myev_second(1,:)<=-eps)&(myev_second(2,:)<=-eps)).*(myev_second(2,:)./(eps+myev_second(1,:))>0.25);
            
            FH=squeeze(myev(1,:,:,:)); 
        end;
        
            
        
        
            if strcmp(normalization_saliency,'NCC')
                FH=1/(1-threshold)*(FH-threshold);
                FH=abs(FH.*(FH>0));
            end;
        
        FeatureData.saliency_map=max(FH,FeatureData.saliency_map);
    end;
    
clear FH NI NH accimg mean mean2 ImgM Imean tmp     myev
clear myevec myev HField HField_back    HRawField_back myevec_second myev_second
    
    DScales=Scales(1)+[0:1:ndensescales-1]*(Scales(end)-Scales(1))/(ndensescales-1);
    
    
    prop_min=0.001;
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    accimg=FeatureData.saliency_map;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    for a=2:numel(accimg),
        accimg(a)=accimg(a)+accimg(a-1);
    end;

    accimg=accimg./((accimg(end)));
    FeatureData.saliency_map_acc=accimg;
    


    
% COMPUTING GRADIENT IN PLANE

 FeatureData.curvatureVessel=zeros([ndirs,cshape],classid);
 offset=0;
 for sindx=1:nscales
        scale=Scales(sindx);
        GaussNfact=1/((2*pi)^(3/2));
        %GaussNfact=GaussNfact/scale;
        GaussNfact=GaussNfact/scale^2;
        
        GField=Grad(img,scale,'crop',crop,'normalize',false);
        GField=GField.*reshape(repmat(std_dev(:)'*GaussNfact,3,1),[3,cshape]);        
        dirs=FeatureData.ofield_dirs{sindx};
        FeatureData.curvatureVessel(offset+1:offset+size(dirs,2),:,:,:)=sta_gradorientdist(GField,{'dirs',dirs});              
        offset=offset+size(dirs,2);
 end;

    
    fprintf('polfit\n');
    %radfunc=@(x)sqrt(x);%
    %radfunc=@(x)log(x)/log(Scales_fac);
    radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    FeatureData.AlphasCurvature=mhs_polfit(FeatureData,'mondeg',[8,8,8,0],'poldeg',poldeg,'radfunc',radfunc,'even',true,'field','curvatureVessel');
    fprintf('done\n');
        
    

FeatureData = rmfield(FeatureData,'curvatureVessel');
    
    
% COMPUTING GRADIENT IN VESSEL DIR    

        GField=Grad(img,1.5,'crop',crop,'normalize',true);
        
% FeatureData.gradient_img=1.5*GField;
        
        GField=GField.*reshape(repmat(std_dev(:)',3,1),[3,cshape]);        
        
        GFieldN=sqrt(sum(GField(:,:).^2,1));
        GField=GField./(reshape(repmat(GFieldN,3,1),[3,cshape])+eps);
        
        OT2=cat(1,cat(1,GField(1,:).^2,GField(2,:).^2,GField(3,:).^2),GField(1,:).*GField(2,:),GField(1,:).*GField(3,:),GField(2,:).*GField(3,:));
        OT2=reshape(OT2,[6,cshape]).*reshape(repmat(GFieldN(:,:),6,1),[6,cshape]);
        
        dirs=FeatureData.ofield_dirs{end};
        FeatureData.curvatureVesselDir=sta_orientdist(OT2,{'dirs',dirs});           
        
        fprintf('polfit\n');
        radfunc=@(x)x;
        FeatureData.AlphasCurvatureVDir=mhs_polfit(FeatureData,'mondeg',[8,8,8,0],'poldeg',poldeg_vdir,'radfunc',radfunc,'even',true,'field','curvatureVesselDir','unitsphere',true);
        fprintf('done\n');
    
FeatureData = rmfield(FeatureData,'curvatureVesselDir');    
    


    
    FeatureData.scales=DScales;
    
    FeatureData.vesselness=single(0);
    
FeatureData.datafunc=single(1);
    FeatureData.cshape=single(cshape);





 
    
    
    
    
    
    
    
 
    