
function tensor = Hessian(inputimg,sigma,varargin)

supergauss=false;
normalize=false;
crop=[0,0,0];
psf=[1,1,1];
element_size=[1,1,1];

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

shape=size(inputimg);

element_size=element_size./psf;


if numel(shape)==3,
    cshape=shape-2*crop;

%     if exist('element_size','var')
%         sigma=sigma./element_size;
%     else
%         element_size=[1,1,1];
%     end;

    if (nargin>1) && (sum(abs(sigma))>0)
        inputimg=mhs_smooth_img(inputimg,sigma,'normalize',normalize,'supergauss',supergauss,'element_size',element_size);
    end;

    wxx=1/element_size(1)^2;
    wyy=1/element_size(2)^2;
    wzz=1/element_size(3)^2;
    wxy=1/(element_size(1)*element_size(2));
    wxz=1/(element_size(1)*element_size(3));
    wyz=1/(element_size(2)*element_size(3));

    tensor_=zeros([6,shape],class(inputimg));

    kernel = zeros(3,3,3);
    kernel(2,2,2) = -2; kernel(1,2,2) = 1; kernel(3,2,2) = 1;
    tensor_(1,:,:,:) = imfilter(inputimg,kernel*wxx,'circular');
    tensor_(2,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wyy,'circular');
    tensor_(3,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wzz,'circular');
    kernel = zeros(3,3,3);
    kernel(1,1,2) = 0.25; kernel(3,3,2) = 0.25; kernel(1,3,2) = -0.25; kernel (3,1,2) = -0.25; 
    tensor_(4,:,:,:) = imfilter(inputimg,kernel*wxy,'circular');
    tensor_(5,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wyz,'circular');
    tensor_(6,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wxz,'circular');
    
  
    
    tensor=tensor_(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
    %tensor(5,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wxz,'circular');
    %tensor(6,:,:,:) = imfilter(inputimg,permute(kernel,[2 3 1])*wyz,'circular');


elseif numel(shape)==2,
    cshape=shape-2*crop(1:2);

%     if exist('element_size','var')
%         sigma=sigma./element_size;
%     else
%         element_size=[1,1];
%     end;

    if (nargin>1) && (sum(abs(sigma))>0)
        inputimg=mhs_smooth_img(inputimg,sigma,'normalize',normalize);
    end;

    wxx=1/element_size(1)^2;
    wyy=1/element_size(2)^2;
    wxy=1/(element_size(1)*element_size(2));

    tensor_=zeros([3,shape],class(inputimg));
    
    kernel = zeros(3,3);
    kernel(2,2) = -2; kernel(1,2) = 1; kernel(3,2) = 1;
    tensor_(1,:,:) = imfilter(inputimg,kernel*wxx,'circular');
    tensor_(2,:,:) = imfilter(inputimg,permute(kernel,[2 1])*wyy,'circular');
    kernel = zeros(3,3);
    kernel(1,1) = 0.25; kernel(3,3) = 0.25; kernel(1,3) = -0.25; kernel (3,1) = -0.25; 
    tensor_(3,:,:) = imfilter(inputimg,kernel*wxy,'circular');    
    
    tensor=tensor_(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2));
%     
%      kernel = zeros(3,3);
%      kernel(2,2,2) = -2; kernel(1,2,2) = 1; kernel(3,2,2) = 1;
%      tensor(1,:,:,:) = imfilter(inputimg,kernel*wxx,'circular');
%      tensor(2,:,:,:) = imfilter(inputimg,permute(kernel,[3 1 2])*wyy,'circular');
%      kernel = zeros(3,3,3);
%      kernel(1,1,2) = 0.25; kernel(3,3,2) = 0.25; kernel(1,3,2) = -0.25; kernel (3,1,2) = -0.25; 
%      tensor(3,:,:,:) = imfilter(inputimg,kernel*wxy,'circular');
end