function R = mhs_vote_filter(ImgG,scale,gamma)


if nargin<3
    gamma=1;
end;
scalefact=1.15;
        %ImgGN=reshape(repmat(sqrt( sum(ImgG(:,:).^2,1) ) ,3,1),size(ImgG))+0.01;
        %ImgG=ImgG./ImgGN;
        
        classid=class(ImgG);
        
        
        if (gamma==1)
            
        elseif (gamma==0.5)
            ImgGN=reshape(repmat(sqrt( sum(ImgG(:,:).^2,1) )+eps ,3,1),size(ImgG));
            ImgG=sqrt(ImgGN).*(ImgG./ImgGN);
        else
            ImgGN=reshape(repmat(sqrt( sum(ImgG(:,:).^2,1) )+eps ,3,1),size(ImgG));
            ImgG=(ImgGN.^gamma).*(ImgG./ImgGN);
        end;
        %ImgG=(ImgGN.^0.1).*(ImgG./ImgGN);
        clear ImgGN;
fprintf('.');
        dim=3;
        sigma=[scale,scale,scale]*scalefact;

        imgsz = size(ImgG);
        imgsz=imgsz([2,3,4]);
            
        [X Y Z] = ndgrid(0:(imgsz(1)-1),0:(imgsz(2)-1),0:(imgsz(3)-1));
        X = X - ceil(imgsz(1)/2);
        Y = Y - ceil(imgsz(2)/2);
        Z = Z - ceil(imgsz(3)/2);
        
        R2 = (X.^2/(2*sigma(1)^2) + Y.^2/(2*sigma(2)^2) + Z.^2/(2*sigma(3)^2));
        
        gaussin = (exp(-R2));
        
        clear R2;

        gaussin=scale*gaussin/sqrt(((2*pi)^dim)*prod(sigma.^2));
fprintf('.');        
        
        VKernel=[X(:),Y(:),Z(:)];
        clear X Y Z;
        VKernelN=reshape(repmat(gaussin(:)./(sqrt(sum(VKernel.^2,2))+eps),1,3),size(VKernel));
        clear gaussin;
        VKernel=cast(reshape((VKernel.*VKernelN).',size(ImgG)),classid);
        
        
fprintf('.');        
        R=zeros(imgsz,classid);
        
        for d=1:dim    
            kernel_ft= fftn(squeeze(fftshift(VKernel(d,:,:,:))));
            imgg_ft= fftn(squeeze(ImgG(d,:,:,:)));

            R=R+ifftn(imgg_ft.*kernel_ft,'symmetric');    
        end;
        
fprintf('.');        