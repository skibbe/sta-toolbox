function [FeatureData]=init_mhs_SD(img,varargin)

    crop=[10,10,10];
    epsilon=0.00000001;
    Scales_fac=1;
    poldeg=3;
    L=4;
    maxit=50;
    
    

    
    scale_range=[1.15,8];
    nscales=4;

    saliancy_offset=-1;
    
    element_size=[1,1,1];
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    if ~exist('sd_element_size','var')
        sd_element_size=element_size;
    end;
    
    nscales_pol=nscales;
       
    element_size=element_size/min(element_size);
    
    pad=max(crop)<0;
    
    if pad
        border=-crop;
        img=padarray(img,border,'symmetric');
        crop=border;
    end;
    
    scale_stepsize=(scale_range(2)-scale_range(1))/(nscales-1);
    Scales=scale_range(1):scale_stepsize:scale_range(2);
    
    
    scale_stepsize_pol=(scale_range(2)-scale_range(1))/(nscales_pol-1);
    Scales_pol=scale_range(1):scale_stepsize_pol:scale_range(2);
    
    classid=class(img);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);

    nscales=numel(Scales);
    nscales_pol=numel(Scales_pol);
    
    poldeg=min(poldeg,nscales_pol);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

    if pad
        FeatureData.shape_org=cshape;
        FeatureData.shape_new_offset=[0,0,0];
        FeatureData.shape_boundary=single(crop);
    else
        FeatureData.shape_org=shape;
        FeatureData.shape_new_offset=crop;
    end;
    
   
    FeatureData.scales=Scales;
    FeatureData.saliency_map=zeros(cshape,classid);
   
    
    
    img_slocal_sdv=zeros([nscales_pol,cshape],classid);
    FeatureData.sd_data=zeros([3,nscales,cshape],classid);  
    FeatureData.saliency_map=zeros(cshape,classid);  
    
    fprintf('computing saliency map: ');
            
    

    if exist('workers','var')
        [ROIS,workers]=rois_from_shape_q(cshape,workers);

    else
        workers=1;
    end;
       
    for w=1:workers
        
        if workers>1
            R0=[ROIS(w,1:3),ROIS(w,1:3)+ROIS(w,4:6)-1];
            R=R0+[crop,crop];
            
            image=stafieldStruct(img(R(1)-crop(1):R(4)+crop(1),  R(2)-crop(2):R(5)+crop(2),  R(3)-crop(3):R(6)+crop(3)));     
            sub_shape=R0(4:6)-R0(1:3)+1;
        else
            image=stafieldStruct(img);
            R=[1,1,1,cshape];
            R0=R;
            sub_shape=cshape;
        end;
        %tmp=squeeze(image.data(1,1,:,:,:));
        %FeatureData.saliency_map(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=tmp(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    
    
        for sindx=1:nscales
                scale=Scales(sindx);


                res = sta_steerdeconv(image,'tolerance',0.001,'maxit',maxit,'L',L,'element_size',sd_element_size,'sigma',scale);
                n = getLocalMax(res.data,res.L,1,true,res.type,res.storage);

                m=squeeze(sqrt(sum(n.^2,1)));

                sigmanorm=max(scale*sqrt(2),2);
                std_dev=mhs_compute_std_mask(m,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',true,'element_size',element_size);
                %std_dev=squeeze(img_slocal_sdv(sindx,:,:,:));
                
                img_slocal_sdv(sindx,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=std_dev;

                HField=Hessian(m,scale,'crop',crop,'normalize',false,'element_size',element_size);    
                Lap=(sum(HField(1:3,:,:,:),1));
                HField(1,:,:,:)=HField(1,:,:,:)-Lap;
                HField(2,:,:,:)=HField(2,:,:,:)-Lap;
                HField(3,:,:,:)=HField(3,:,:,:)-Lap;

                m=m(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
                n=n(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));

                n=n./reshape(repmat(m(:).'+eps,3,1),size(n));


                N=scale^2/((2*pi*scale^2)^(3/2));
                H=N*(sum((n(1:3,:,:,:).^2).* (HField(1:3,:,:,:)),1) ... 
                    +2 * n(1,:,:,:).*(n(2,:,:,:).*HField(4,:,:,:) + n(3,:,:,:).*HField(6,:,:,:)) ...
                    +2 * prod(n(2:3,:,:,:)).*HField(5,:,:,:));



                FeatureData.sd_data(:,sindx,R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=n.*reshape(repmat(sqrt(abs(H(:,:))),3,1),size(n));

                FeatureData.saliency_map(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6))=max(reshape(squeeze(H).*std_dev,sub_shape),FeatureData.saliency_map(R0(1):R0(4),R0(2):R0(5),R0(3):R0(6)));
        end;      
    end;
    
%     for sindx=1:nscales
%         scale=Scales(sindx);
%         
%         
%         res = sta_steerdeconv(image,'tolerance',0.001,'maxit',maxit,'L',L,'element_size',sd_element_size,'sigma',scale);
%         n = getLocalMax(res.data,res.L,1,true,res.type,res.storage);
% 
%         m=squeeze(sqrt(sum(n.^2,1)));
%         
%         sigmanorm=max(scale*sqrt(2),2);
%         img_slocal_sdv(sindx,:,:,:)=mhs_compute_std_mask(m,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',true,'element_size',element_size);
%         std_dev=squeeze(img_slocal_sdv(sindx,:,:,:));
%         
%         HField=Hessian(m,scale,'crop',crop,'normalize',false,'element_size',element_size);    
%         Lap=(sum(HField(1:3,:,:,:),1));
%         HField(1,:,:,:)=HField(1,:,:,:)-Lap;
%         HField(2,:,:,:)=HField(2,:,:,:)-Lap;
%         HField(3,:,:,:)=HField(3,:,:,:)-Lap;
%         
%         m=m(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%         n=n(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%         
%         n=n./reshape(repmat(m(:).'+eps,3,1),size(n));
%       
%         
%         N=scale^2/((2*pi*scale^2)^(3/2));
%         H=N*(sum((n(1:3,:,:,:).^2).* (HField(1:3,:,:,:)),1) ... 
%             +2 * n(1,:,:,:).*(n(2,:,:,:).*HField(4,:,:,:) + n(3,:,:,:).*HField(6,:,:,:)) ...
%             +2 * prod(n(2:3,:,:,:)).*HField(5,:,:,:));
%         
%      
%         
%         FeatureData.sd_data(:,sindx,:,:,:)=n.*reshape(repmat(sqrt(abs(H(:,:))),3,1),size(n));
%       
%         FeatureData.saliency_map=max(reshape(squeeze(H).*std_dev,cshape),FeatureData.saliency_map);
%     end;  
    
    
    clear FH;
    
    clear SD FH res 
        
    mondeg=[poldeg,0,0,0];
    even=false;
    pm=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);


    
    prop_min=0.001;
    if (saliancy_offset>-1)
        assert(~(saliancy_offset>1));
        prop_min=saliancy_offset;
    end;
    
    
FeatureData.debug_saliency_map=FeatureData.saliency_map;
    
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    
    %FeatureData.saliency_map(:)=0;
    %FeatureData.saliency_map(48,48,48)=1;
   
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));

    radfunc=@(x)(x);
    positions=radfunc(Scales_pol);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);
    
    FeatureData.alphas_sdv=reshape(sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat),[poldeg+1,cshape]);
      
FeatureData.debug_img_slocal_sdv=img_slocal_sdv;
   
    

    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);
    
    if exist('override_scales')
        assert(numel(override_scales)==2);
        assert(override_scales(1)<override_scales(2));
        FeatureData.override_scales=cast(override_scales,classid);
    end;

    FeatureData.datafunc=cast(8,classid);
    
    if max(abs(element_size-1))<eps
        FeatureData.cshape=cast(cshape,classid);
        FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    else
        
        cshape=ceil(cshape.*element_size);
        
        FeatureData.cshape=class(cshape,classid);
        FeatureData.img=scaleimage3D(img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),cshape,true);
    end;
        

 
    
function [ROIS,workers]=rois_from_shape_q(shape,workers)

    q=floor((workers^(1/3)));

    
    for dim=1:3
        stepsize=floor((shape(dim)/q));
        R=[];
        for w=1:q
            R=[R;[(w-1)*stepsize,stepsize]];
        end;
        %filling last gab
        R(end,2)=shape(dim)-R(end,1);
        Rq(dim).R=R;
    end;
    ROIS=[];
    for qx=1:q
        for qy=1:q
            for qz=1:q
                ROIS=[ROIS;[Rq(1).R(qx,1),Rq(2).R(qy,1),Rq(3).R(qz,1),Rq(1).R(qx,2),Rq(2).R(qy,2),Rq(3).R(qz,2)]];
            end;
        end;
    end;    
    
    ROIS(:,1:3)=ROIS(:,1:3)+1;
    workers=q^3;
    
    
    
 
    