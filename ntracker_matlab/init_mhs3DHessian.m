%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3DHessian(img,varargin)



crop=[10,10,10];
epsilon=0.01;
Scales_fac=1.5;
sigman=-1;
poldeg=3;
%poldeg=numel(Scales);

trueSF=true;

normalization='STD';
%normalization='NONE';
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
% if ~exist('poldeg')
%     poldeg=numel(Scales);
% end;
    
    classid=class(img);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);


    nscales=numel(Scales);
    
    poldeg=min(poldeg,nscales);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);


    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    FeatureData.scales=Scales;

    FeatureData.saliency_map=zeros(cshape,classid);
    
    FeatureData.saliency_map_acc=zeros([nscales,cshape],classid);
    
  
    if sigman==-1
        sigman=max(Scales);
    end;
    
    
    if strcmp(normalization,'STD')
        
        if exist('Mask')
            %std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon,'crop',crop,'Mask',Mask);   
            std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon,'Mask',Mask);   
            %std_dev=mhs_compute_std_scaledependent(img,'Scales',Scales,'epsilon',epsilon,'Mask',Mask);
        else
            %std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon,'crop',crop);
            std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon);
            %std_dev=mhs_compute_std_scaledependent(img,'Scales',Scales,'epsilon',epsilon);
        end;
    end;
    if exist('Mask')
        Mask=cast(Mask(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),classid);
    end;
    
     %if strcmp(normalization,'STD')
     %   mean=mhs_smooth_img(img,sigman,'normalize',true);
     %   mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
     %   std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
     %   std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
     %end;
     
     clear mean mean2 
    
     
     
normalize_derivatives=false;     
    
%COMPUTING SALIENCY MAP
  
        Hessian_img=zeros([6,nscales,cshape],classid);

    %GGG=GVF(img,'iteration',100);    
        
    for sindx=1:nscales
        scale=Scales(sindx);

        
        
        
        
        if strcmp(normalization,'STD')
%             G=Grad(img,scale,'crop',crop,'normalize',normalize_derivatives).*reshape(repmat(std_dev(:)',3,1),[3,cshape]);
%             GX=Grad(squeeze(G(1,:,:,:)),'normalize',normalize_derivatives);
%             GY=Grad(squeeze(G(2,:,:,:)),'normalize',normalize_derivatives);
%             GZ=Grad(squeeze(G(3,:,:,:)),'normalize',normalize_derivatives);

            %G=GGG.*reshape(repmat(std_dev(:)',3,1),[3,shape]);
            %GX=Grad(squeeze(G(1,:,:,:)),scale,'normalize',normalize_derivatives,'crop',crop);
            %GY=Grad(squeeze(G(2,:,:,:)),scale,'normalize',normalize_derivatives,'crop',crop);
            %GZ=Grad(squeeze(G(3,:,:,:)),scale,'normalize',normalize_derivatives,'crop',crop);

if 0
             G=Grad(img,scale,'normalize',normalize_derivatives).*reshape(repmat(std_dev(:)',3,1),[3,shape]);
             GX=Grad(squeeze(G(1,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
             GY=Grad(squeeze(G(2,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
             GZ=Grad(squeeze(G(3,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
            HField=[GX(1,:,:,:);GY(2,:,:,:);GZ(3,:,:,:);GX(2,:,:,:);GY(3,:,:,:);GX(3,:,:,:)];
            clear GX GY GZ G;
else            
            HField=reshape(repmat(std_dev(:)',6,1),[6,shape]).*(Hessian(img,scale,'normalize',normalize_derivatives));
            HField=HField(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
end;            
        else
            %HField=scale^2*Hessian(img,scale,'crop',crop,'normalize',normalize_derivatives);
            HField=Hessian(img,scale,'crop',crop,'normalize',normalize_derivatives);
        end
        
        
        Lap=(sum(HField(1:3,:,:,:),1));

        [myevec_second,myev_second]=(sta_EVGSL(HField));
        
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;
        
        [myevec,myev]=(sta_EVGSL(HField));
        N=scale^2/((2*pi*scale^2)^(3/2));
        FH=N*myev(1,:).*(myev(1,:)>0).*((myev_second(1,:)<=-eps)&(myev_second(2,:)<=-eps)).*(myev_second(2,:)./(eps+myev_second(1,:))>0.25);
        
        
        %FH=squeeze(myev(1,:,:,:)); 
        
        FeatureData.saliency_map=max(reshape(FH,cshape),FeatureData.saliency_map);
        
        N=1/scale;
        
        if trueSF
            Hessian_img(:,sindx,:)=N*HField(:,:);
        else
            %OT=cat(1,cat(1,myevec(1,:).^2,myevec(2,:).^2,myevec(3,:).^2),myevec(1,:).*myevec(2,:),myevec(1,:).*myevec(3,:),myevec(2,:).*myevec(3,:));
            OT=cat(1,cat(1,myevec(1,:).^2,myevec(2,:).^2,myevec(3,:).^2),myevec(1,:).*myevec(2,:),myevec(2,:).*myevec(3,:),myevec(1,:).*myevec(3,:));
            Hessian_img(:,sindx,:,:,:)=reshape(OT,[6,cshape]).*reshape(repmat(myev(1,:)*N,6,1),[6,cshape]);
        end;
        
        %display(max(N*HField(:)));
    end;
    
    if strcmp(normalization,'STD')
        std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    end;

        

    
%     display(max(Hessian_img(:)));
    
    clear HField myev_second myevec_second myevec myev Lap OT FH  GX GY GZ G
        
    mondeg=[poldeg,0,0,0];
    even=false;
    [pm,p]=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);

    %radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    radfunc=@(x)sqrt(log(1+x)/log(Scales_fac));
    %radfunc=@(x)sqrt(x);
    %radfunc=@(x)(x);
    positions=radfunc(Scales);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);
    
    FeatureData.alphas_hessian=zeros([size(polmat,2),6,cshape],classid);
    for a=1:size(Hessian_img,1)
        FeatureData.alphas_hessian(:,a,:)=sta_matmult(squeeze(Hessian_img(a,:,:)),pimat);
    end;
    
%FeatureData.Hessian_img=Hessian_img;    
    
    clear Hessian_img
    

    prop_min=0.001;
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    if exist('Mask')
        FeatureData.saliency_map=FeatureData.saliency_map.*Mask;
    end;
    accimg=FeatureData.saliency_map;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    for a=2:numel(accimg),
        accimg(a)=accimg(a)+accimg(a-1);
    end;

    accimg=accimg./((accimg(end)));
    FeatureData.saliency_map_acc=accimg;
    
    gradient_img=zeros([3,nscales,cshape],classid);
    for sindx=1:nscales
        scale=Scales(sindx);
        
        if strcmp(normalization,'STD')
            GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',normalize_derivatives);
        else
            GField=Grad(img,scale,'crop',crop,'normalize',normalize_derivatives);
        end;    
        N=1/((2*pi)^(3/2));
        N=N/scale^2;
        %N=1/scale^2;
        gradient_img(:,sindx,:)=N*GField(:,:);
        %gradient_img(sindx,:)=squeeze(sqrt(sum(GField(:,:).^2,1)));
    end;
    %FeatureData.gradient_img=gradient_img;
    
    
    clear GField

    %mondeg=[poldeg,0,0,0];
    %even=false;
    %[pm,p]=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);


    %radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    %positions=radfunc(Scales);
    %nvalues=size(positions,2);
    %polmat=[ones(nvalues,1),pm(positions(1,:)')];
    %pimat=pinv(polmat);

    FeatureData.alphas_gradient=zeros([size(polmat,2),3,cshape],classid);

    for a=1:3
        FeatureData.alphas_gradient(:,a,:)=sta_matmult(squeeze(gradient_img(a,:,:)),pimat);
    end;
    
    clear gradient_img
    %values=gradient_img(:,:);
    %alphas=sta_matmult(values,pimat);
    %FeatureData.alphas(:,:)=alphas;
    





    scale=1.5;
    if strcmp(normalization,'STD')
        FeatureData.gradient_vessel=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
    else
        FeatureData.gradient_vessel=Grad(img,scale,'crop',crop,'normalize',true);
    end;
    

    
    
    



    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);
    
    if exist('override_scales')
        assert(numel(override_scales)==2);
        assert(override_scales(1)<override_scales(2));
        FeatureData.override_scales=cast(override_scales,classid);
    end;


    FeatureData.datafunc=single(4);
    FeatureData.cshape=single(cshape);

    if exist('Mask')
        FeatureData.alphas_gradient(:,:,Mask(:)<1)=0;
        FeatureData.alphas_hessian(:,:,Mask(:)<1)=0;
        FeatureData.gradient_vessel(:,Mask(:)<1)=0;
        
    end;
    
    FeatureData.trueSF=cast(trueSF,classid);
    
%     if exist('Mask')
%         FeatureData.Mask=Mask>0;
%     end;



 
    
    
    
    
    
    
    
 
    