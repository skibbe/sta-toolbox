
function A=multi2single(B)

%org_shape=max([B.W(:).shape]+[B.W(:).offset],[],2);

numw=numel(B.W);
A=B.W(1).A;
A.data=[];
A.connections=[];
A.opt_temp=100000000;
offset=0;
path_id=0;

for a=1:numw
    data=B.W(a).A.data;
    idx=data(21,:)>-1;
    
    data(21,idx)=data(21,idx)+path_id;
    
    A.data=[A.data,data];
    conn=B.W(a).A.connections;
    conn(1:2,:)=conn(1:2,:)+offset;
    A.connections=[A.connections,conn];
    A.opt_temp=min(A.opt_temp,B.W(a).A.opt_temp);
    offset=offset+size(B.W(a).A.data,2);
    if ~isempty(idx)
    path_id=max(A.data(21,:));
    end;
    %path_id=path_id+max(B.W(a).A.data(21,:));
end;

