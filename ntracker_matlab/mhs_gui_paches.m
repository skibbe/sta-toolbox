%quiver3(p.v(1,:),p.v(2,:),p.v(3,:),p.n(1,:),p.n(2,:),p.n(3,:))
function vert_data=mhs_gui_paches(mesh_id,resolution,reinit)

if nargin<3
    reinit=false;
end;    
    

                found_valid_data=false;
                if (exist(['renderer_meshes',num2str(resolution),'.mat'])&&(nargin>0)) && (~reinit)
                    if ((mesh_id<0)||(mesh_id>6))
                        vert_data=-1;
                        return;
                    end;
                    load(['renderer_meshes',num2str(resolution),'.mat']);
                    if (mesh_id<numel(renderer_meshes))
                        vert_data=renderer_meshes(mesh_id+1).vert_data;
                        found_valid_data=true;
                    end;
                end;
                
                if (~found_valid_data)
                    
                    
                    for mesh_id=0:6
                        mode=mesh_id;
                        %resolution_disk=32;
                        %resolution_cylinder=6;
                        flat=true;
                        
flat=false;                        
                        normal_mode=1;
                        reduce_sphere=0.1;


                        %resolution_disk=32;
                        %resolution_cylinder=6;
                        
                        switch(resolution)
                            case 3
                                    %resolution_disk=72;
                                    reduce_sphere=0.2;
                                    resolution_disk=64;
                                    resolution_cylinder=32;
                            case 2
                                    resolution_disk=64;
                                    resolution_cylinder=16;
                                    
                            case 1
                                    resolution_disk=32;
                                    resolution_cylinder=16;
                                    
                            case 0
                                    resolution_disk=8;
                                    resolution_cylinder=8;        
                                    
                                    %resolution_disk=8;
                                    %resolution_cylinder=3;   
                        
                        end;
                        generate_from_coordinates=true;
                        
                        %resolution_disk=32;
                        %resolution_cylinder=8;
                        
                        %resolution_cylinder=3+(mode);
                        %mode=2;

                        switch mode
                            case 0 % sphere
                                fprintf('creating sphere\n');
                                [x y z] = sphere(resolution_disk); 
                                z=z./max(abs(z(:)));     
                                flat=false;
                            case 1 % flat disk
                                fprintf('creating disk\n');
                                [x y z] = sphere(resolution_disk); 
                                z=z./max(abs(z(:)));         
                            case 2 % cylinder
                                fprintf('creating cylinder (crashes when called from within mex file!?)\n');
                                [x,y,z] = cylinder(1,resolution_cylinder);
                                z=2*(z-0.5);
                                normal_mode=2;
                            case 3 % cube
                                fprintf('creating cube\n');
                                p.vertices = ([0 0 0;1 0 0;1 1 0;0 1 0;0 0 1;1 0 1;1 1 1;0 1 1]-0.5)*2;
                                %p.vertices = ([0 0 0;1 0 0;1 1 0;0 1 0;0 0 1;1 0 1;1 1 1;0 1 1]);
                                %p.faces = [1 2 6 5;2 3 7 6;3 4 8 7;4 1 5 8;1 2 3 4;5 6 7 8];
                                 p.faces = [1 2 6 5;2 3 7 6;3 4 8 7;4 1 5 8;2 1 4 3;5 6 7 8];
                                
                                generate_from_coordinates=false;
                                 normal_mode=2;
                                flat=false;
                             case 4 % flat disk
                                fprintf('creating disk 2\n');
                                [x y z] = sphere(resolution_disk); 
                                z=z./max(abs(z(:)));   
                                r=sqrt(x.^2+y.^2);
                                z=(r<0.5).*(0.65*z-0.35*z.*cos(2*r*pi))+(~(r<0.5)).*z;
                                %z=0.65*z-0.35*z.*cos(r*pi);
                                %z=z+sign(z).*(0.25-0.25*cos(r*pi));
                              case 5 % flat disk
                                fprintf('creating spine\n');
                                [x y z] = sphere(resolution_disk); 
                                d=z./max(abs(z(:)));         
                                %d=(z./(max(abs(z(:))))+1)/2;  
                                flat=false;
                                %(1.5 + Cos[x])/2.5
                                %w=(1.5 + cos(pi*(d+1)/2))/2.5;
                                z=pi*(d+1)/2;
                                w=(((cos(z* 1.5) + 1.5)/1.5 .* exp(-z.^2/20)));
                                x=x.*w(end:-1:1,:);
                                y=y.*w(end:-1:1,:);
                             case 6 % cube
                                fprintf('creating flat cube\n');
                                p.vertices = ([0 0 0; 1 0 0; 1 1 0; 0 1 0; 0 0 1; 1 0 1; 1 1 1; 0 1 1]-0.5)*2;
                                p.faces = [1 2 6 5;2 3 7 6;3 4 8 7;4 1 5 8;2 1 4 3;5 6 7 8];
                                
                                t=[];
                                for a=1:6,t=[t;p.vertices(p.faces(a,:),:)];end;
                                
                                p.vertices=t;
                                
                                f=reshape([1:size(t,1)],[4,size(t,1)/4])';
                                p.faces=f;
                                
                                generate_from_coordinates=false;
                                 normal_mode=2;
                                flat=false;  
                            otherwise
                                [x y z] = sphere(resolution_disk); 
                                z=z./max(abs(z(:)));     
                                flat=false;

                        end;


                        if (generate_from_coordinates)
                            p=surf2patch(x,y,z,z);
                            
                            if mode==2
                                p=mergeverts(p);
                            end;
                        end;
                        
                        if normal_mode==1
                            if (resolution_disk>16)
                                
                                [p.faces,p.vertices]=reducepatch(p.faces,p.vertices,reduce_sphere);
                                
                                %vert_data.n=single(-compute_vnormals2(p,flat))';
                                [vert_data.n,vert_data.ni]=compute_vnormals2(p,flat);
                                vert_data.n=single(-vert_data.n)';
                                vert_data.ni=uint32(vert_data.ni);
                                
                                %data=patchQuad2Tri(p);
                                vert_data.v=single(p.vertices)';
                                vert_data.f=uint32(p.faces)'-1;
                            else
                                %vert_data.n=single(-compute_vnormals2(p,flat))';
                                
                                [vert_data.n,vert_data.ni]=compute_vnormals2(p,flat);
                                vert_data.n=single(-vert_data.n)';
                                vert_data.ni=uint32(vert_data.ni);
                                
                                
                                data=patchQuad2Tri(p);
                                vert_data.v=single(data.vertices)';
                                vert_data.f=uint32(data.faces)'-1;
                            end;
                        else
                                %vert_data.n=single(-compute_vnormals(p))';
                                [vert_data.n,vert_data.ni]=compute_vnormals(p);
                                vert_data.n=single(-vert_data.n)';
                                vert_data.ni=uint32(vert_data.ni);
                                
                                data=patchQuad2Tri(p);
                                vert_data.v=single(data.vertices)';
                                vert_data.f=uint32(data.faces)'-1;
                        end;
                        
                        
                       % vert_data.n=-vert_data.n;
                        renderer_meshes(mesh_id+1).vert_data=vert_data;
                    end;
                     %save('renderer_meshes.mat','renderer_meshes');
                     save(['renderer_meshes',num2str(resolution),'.mat'],'renderer_meshes');
                     
                     vert_data=-1;
                end;
                
                
function mesh=mergeverts(mesh)                

                
D=distmat(mesh.vertices,mesh.vertices)+1000*eye(size(mesh.vertices,1));
pos=[1:size(mesh.vertices,1)];
[v,indx]=min(D);
indx2=(indx>pos);
indx3=indx2&(v==0);
keepme=pos(indx3);
replaceme=indx(indx3);

for v=1:numel(keepme)
    mesh.faces(mesh.faces(:)==replaceme(v))=keepme(v);
end;


pos_valid=pos;
pos_valid(:,replaceme)=[];
pos_new=[1:numel(pos_valid)];

if pos_valid(end)>numel(pos_valid)
    mesh.faces=mesh.faces(:,pos_valid);
    for v=1:numel(pos_new) 
           mesh.faces(mesh.faces(:)==pos_valid(v))= pos_new(v);

    end;
end;
                
function [vnormals,nmap]=compute_vnormals(pcylinder)

               % fprintf('precomputing normals ...');
                
                vnormals=zeros(size(pcylinder.vertices));
                
                nmap_tmp=cell(1,size(pcylinder.vertices,1));
                for a=1:size(pcylinder.faces,1)
                    %idx=pcylinder.face()
                    %snormal=cross(pcylinder.vertices(pcylinder.faces(a,1),:),pcylinder.vertices(pcylinder.faces(a,2),:));
                    n1=pcylinder.vertices(pcylinder.faces(a,2),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n1=n1/norm(n1);
                    n2=pcylinder.vertices(pcylinder.faces(a,4),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n2=n2/norm(n2);
                    snormal=cross(n1,n2);
                    if sum(isnan(snormal(:)))==0
                    for b=1:4
                        vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+snormal;
                        nmap_tmp{pcylinder.faces(a,b)}=[nmap_tmp{pcylinder.faces(a,b)},a];
                    end;
                    
                    end;
                end;
                max_neighbors=max(cellfun(@(x)numel(x),nmap_tmp));
                tmp=cellfun(@(x)([x,0*ones(1,max_neighbors-numel(x))]),nmap_tmp,'UniformOutput',false);
                nmap=reshape([tmp{:}],max_neighbors,numel(tmp));
                
                vnormals=vnormals./repmat(eps+sqrt(sum(vnormals.^2,2)),1,3);
           
                                
                

function [vnormals,nmap]=compute_vnormals2(pcylinder,makeflat)

               % fprintf('precomputing normals ...');
               if size(pcylinder,2)==4 
               
                vnormals=zeros(size(pcylinder.vertices));
                for a=1:size(pcylinder.faces,1)
                    %idx=pcylinder.face()
                    %snormal=cross(pcylinder.vertices(pcylinder.faces(a,1),:),pcylinder.vertices(pcylinder.faces(a,2),:));
                    n1=pcylinder.vertices(pcylinder.faces(a,2),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n1=n1/norm(n1);
                    n2=pcylinder.vertices(pcylinder.faces(a,4),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n2=n2/norm(n2);
                    snormal=cross(n1,n2);
                    if sum(isnan(snormal(:)))==0
                        for b=1:4
                            vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+snormal;
                        end;
                    else
                        for b=1:4
                            vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+[0,0,-1];
                        end;
                    end;
                end;
               else
                   vnormals=zeros(size(pcylinder.vertices));
                   nmap_tmp=cell(1,size(pcylinder.vertices,1));
                for a=1:size(pcylinder.faces,1)
                    %idx=pcylinder.face()
                    %snormal=cross(pcylinder.vertices(pcylinder.faces(a,1),:),pcylinder.vertices(pcylinder.faces(a,2),:));
                    n1=pcylinder.vertices(pcylinder.faces(a,2),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n1=n1/norm(n1);
                    n2=pcylinder.vertices(pcylinder.faces(a,3),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n2=n2/norm(n2);
                    snormal=cross(n1,n2);
                    if sum(isnan(snormal(:)))==0
                        for b=1:3
                            vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+snormal;
                            nmap_tmp{pcylinder.faces(a,b)}=[nmap_tmp{pcylinder.faces(a,b)},a];
                        end;
                    else
                        for b=1:3
                            vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+[0,0,-1];
                        end;
                    end;
                end;
               end;
                max_neighbors=max(cellfun(@(x)numel(x),nmap_tmp));
                tmp=cellfun(@(x)([x,0*ones(1,max_neighbors-numel(x))]),nmap_tmp,'UniformOutput',false);
                nmap=reshape([tmp{:}],max_neighbors,numel(tmp)); 
               
                vnormals=vnormals./repmat(eps+sqrt(sum(vnormals.^2,2)),1,3);                
                
                 DM=sqrt(distmat(pcylinder.vertices,pcylinder.vertices));
                 
                 
                    checked=zeros(1,size(DM,1));
                     for a=1:size(DM,1)
                         if ~checked(a)
                                 sel=find(DM(a,:)<0.0001);
                                 if numel(sel)>1
                                     newn=mean(vnormals(sel,:));
                                     %checked(sel)=true;
                                     newn=newn./(norm(newn)+eps);
                                     vnormals(sel,:)=repmat(newn,numel(sel),1);
                                 end;
                         end;
                     end;   
                     
if makeflat                     
                     for n=1:size(vnormals,1)
                         vnormals(n,:)=vnormals(n,:)+5*dot(vnormals(n,:),[0,0,1])*[0,0,1];
                         vnormals(n,:)=vnormals(n,:)./norm(vnormals(n,:)+eps);
                     end;
end;                                