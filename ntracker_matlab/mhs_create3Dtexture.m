function [timg,tlight]=mhs_create3Dtexture(img,s,t)


% timg=zeros(128,128,128);
% timg(1:10,1:10,1:10)=1;
% timg(end-10:end,end-10:end,end-10:end)=1;
% timg(60:68,60:68,60:68)=1;
% timg=uint8(255*timg./max(timg(:)));
% return;
if nargin<2
   s=8; 
end
if nargin<3
   %t=0.25; 
   t=0; 
end

assert(s>1);

Img=img;
imgM=mean(Img(:));
imgV=sqrt(var(Img(:)));
Img=Img-imgM;
Img=Img./(imgV);
Img(:)=max(Img(:),0);
Img(:)=Img(:)./max(Img(:));
img=Img;

shape=size(img);
texture_dim_max=[2,2,2].^s;
texture_dim=min([2,2,2].^ceil(log(shape)/log(2)),texture_dim_max);


if nargout>1
    timg=scaleimage3D(img,texture_dim,true);
    tlight=Grad(timg);
    tlight=uint8(127*tlight([3,2,1],:,:,:)./reshape(repmat(eps+sqrt(sum(tlight(:,:).^2,1)),3,1),size(tlight))+128);
    tlight=permute(tlight,[1,4,3,2]);
end;

img=img-min(img(:));
img(img(:)<t*max(img(:)))=0;
timg=scaleimage3D(img,texture_dim,true);
timg=timg-min(timg(:));
timg=uint8(255*timg./max(timg(:)));
timg=permute(timg,[3,2,1]);
%timg=uint8(255*timg./max(timg(:)));