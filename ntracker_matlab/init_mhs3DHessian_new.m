%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3DHessian_new(img,varargin)

    crop=[10,10,10];
    epsilon=0.001;
    Scales_fac=1;
    sigman=-1;
    poldeg=3;
    psf=[1,1,1];
    
    preprocess_voting=-1;
    preprocess_voting_gamma=1;
    
    expand_hessian=false;
    
    scale_range=[1.15,8];
    nscales=4;
    %nscales_pol=4;

    trueSF=true;
    saliancy_offset=-1;
    normalization='STD';
    
    element_size=[1,1,1];
    
    pad=false;
    
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    if ~exist('nscales_pol','var')
        nscales_pol=nscales;
    end;
    
    element_size=element_size/min(element_size);
    
    pad=max(crop)<0;
    
    if pad
        %shape=size(img);
        border=-crop;
        %img=padarray(img,border,'replicate');
        img=padarray(img,border,'symmetric');
        %img_new=zeros(shape+2*border,'single');
        %img_new(border(1)+1:border(1)+shape(1),border(2)+1:border(2)+shape(2),border(3)+1:border(3)+shape(3))=img;
        %img=img_new;
        crop=border;
    end;
    
    

    scale_stepsize=(scale_range(2)-scale_range(1))/(nscales-1);
    Scales=scale_range(1):scale_stepsize:scale_range(2);
    
    
    scale_stepsize_pol=(scale_range(2)-scale_range(1))/(nscales_pol-1);
    Scales_pol=scale_range(1):scale_stepsize_pol:scale_range(2);
    
    classid=class(img);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);


    nscales=numel(Scales);
    nscales_pol=numel(Scales_pol);
    
    poldeg=min(poldeg,nscales_pol);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

    if pad
        FeatureData.shape_org=cshape;
        FeatureData.shape_new_offset=[0,0,0];
        FeatureData.shape_boundary=cast(crop,classid);
    else
        FeatureData.shape_org=shape;
        FeatureData.shape_new_offset=crop;
    end;
    
   
    FeatureData.scales=Scales;

    FeatureData.saliency_map=zeros(cshape,classid);
   
    
  
    if sigman==-1
        sigman=max(Scales);
    end;
    
    if preprocess_voting>0
        imgG=Grad(img);
    end;
    
    
    fprintf('computing local normalizations: ');
    img_slocal_sdv=zeros([nscales_pol,cshape],classid);
    for sindx=1:nscales_pol
        scale=Scales_pol(sindx);
        %sigmanorm=scale*1.5;
        % we don't trust an estimate based on too few voxels
        sigmanorm=max(scale*sqrt(2),2);
        %sigmanorm=max(scale,2);
        %img_slocal_sdv(sindx,:,:,:)=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',false,'element_size',element_size);
        
        if scale<preprocess_voting
            voting_map{sindx}=mhs_vote_filter(imgG,scale,preprocess_voting_gamma);
            img_slocal_sdv(sindx,:,:,:)=mhs_compute_std_mask(voting_map{sindx},'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',true,'element_size',element_size,'psf',psf);
        else
            img_slocal_sdv(sindx,:,:,:)=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',true,'element_size',element_size,'psf',psf);
        end;
        
        
        fprintf('.');
    end;
    fprintf('\n');
    
    clear mean mean2 
    
     
     
    normalize_derivatives=false;     
    
    if (expand_hessian)
        Hessian_img=zeros([6,nscales_pol,cshape],classid);  
    end;

    fprintf('computing saliency map: ');
    for sindx=1:nscales_pol
        scale=Scales_pol(sindx);
       
        %std_dev=1./(img_slocal_sdv(sindx,:)+epsilon);
        std_dev=img_slocal_sdv(sindx,:);
        
        if scale<preprocess_voting
            HField=Hessian(voting_map{sindx},scale,'crop',crop,'normalize',normalize_derivatives,'psf',psf,'element_size',element_size);
        else
            HField=Hessian(img,scale,'crop',crop,'normalize',normalize_derivatives,'psf',psf,'element_size',element_size);    
        end;
        
        
        Lap=(sum(HField(1:3,:,:,:),1));

        %[myevec_second,myev_second]=(sta_EVGSL(HField));
        
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;
        
        [myevec,myev]=(sta_EVGSL(HField));
        N=scale^2/((2*pi*scale^2)^(3/2));
        
        
        
        %N=1/scale;
        %FH=N*myev(1,:).*(myev(1,:)>0).*((myev_second(1,:)<=-eps)&(myev_second(2,:)<=-eps)).*(myev_second(2,:)./(eps+myev_second(1,:))>0.25);
        FH=N*std_dev.*myev(1,:).*(myev(1,:)>0);
        
        FeatureData.saliency_map=max(reshape(FH,cshape),FeatureData.saliency_map);
        
        if (expand_hessian)
            if trueSF
                Hessian_img(:,sindx,:)=N*HField(:,:);
            else
                OT=cat(1,cat(1,myevec(1,:).^2,myevec(2,:).^2,myevec(3,:).^2),myevec(1,:).*myevec(2,:),myevec(2,:).*myevec(3,:),myevec(1,:).*myevec(3,:));
                Hessian_img(:,sindx,:,:,:)=reshape(OT,[6,cshape]).*reshape(repmat(myev(1,:)*N,6,1),[6,cshape]);
            end;
        end;
        fprintf('.');
    end;
     fprintf('\n');
%     if strcmp(normalization,'STD')
%          for sindx=1:nscales
%             std_dev{sindx}=std_dev{sindx}(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%          end;
%     end;

    clear HField myev_second myevec_second myevec myev Lap OT FH  GX GY GZ G std_dev voting_map
        
    mondeg=[poldeg,0,0,0];
    even=false;
    pm=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);

    radfunc=@(x)(x);
    %radfunc=@(x)sqrt(x);
    positions=radfunc(Scales_pol);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);
    
    if (expand_hessian)
         fprintf('computing hessian coefficients: ');
        FeatureData.alphas_hessian=zeros([size(polmat,2),6,cshape],classid);
        for a=1:size(Hessian_img,1)
            FeatureData.alphas_hessian(:,a,:)=sta_matmult(squeeze(Hessian_img(a,:,:)),pimat);
            fprintf('.');
        end;
        clear Hessian_img
          fprintf('\n');
    end;
    

    prop_min=0.001;
    if (saliancy_offset>-1)
        assert(~(saliancy_offset>1));
        %prop_min=max(FeatureData.saliency_map(:))*saliancy_offset;
        prop_min=saliancy_offset;
    end;
    
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    
 
   
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    
%     gradient_img=zeros([3,nscales,cshape],classid);
%     for sindx=1:nscales
%         scale=Scales(sindx);
%         
%         if strcmp(normalization,'STD')
%             GField=reshape(repmat(std_dev{sindx}(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',normalize_derivatives);
%         else
%             GField=Grad(img,scale,'crop',crop,'normalize',normalize_derivatives);
%         end;    
%         N=scale/((2*pi*scale^2)^(3/2));
%         gradient_img(:,sindx,:)=N*GField(:,:);
%     end;
%     clear GField
% 
% 
%     FeatureData.alphas_gradient=zeros([size(polmat,2),3,cshape],classid);
% 
%     for a=1:3
%         FeatureData.alphas_gradient(:,a,:)=sta_matmult(squeeze(gradient_img(a,:,:)),pimat);
%     end;
%     
%     clear gradient_img

%     scale=1.5;
%     if strcmp(normalization,'STD')
%         sigmanorm=1.5*1.5;
%         if exist('Mask')
%             %std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon,'crop',crop,'Mask',Mask);   
%             std_dev=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon,'Mask',Mask);   
%         else
%             %std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon,'crop',crop);
%             std_dev=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon);
%         end;
%         std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%         FeatureData.gradient_vessel=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
%     else
%         FeatureData.gradient_vessel=Grad(img,scale,'crop',crop,'normalize',true);
%     end;


    FeatureData.alphas_sdv=reshape(sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat),[poldeg+1,cshape]);
      
    fprintf('computing multi-scale coefficients: ');
    
    if (nscales_pol~=nscales)
        img_slocal_sdv=zeros([nscales,cshape],classid);
    end;
    
        for sindx=1:nscales
            scale=Scales(sindx);
            %N=1/scale^3;
            %N=1/((2*pi*scale^2)^(3/2));
            %tmp=N*mhs_smooth_img(img,scale,'normalize',false,'element_size',element_size);
             
            if scale<preprocess_voting
                tmp=mhs_smooth_img(mhs_vote_filter(imgG,scale,preprocess_voting_gamma),scale,'normalize',true,'element_size',element_size,'psf',psf);
            else
                tmp=mhs_smooth_img(img,scale,'normalize',true,'element_size',element_size,'psf',psf);
            end;
            img_slocal_sdv(sindx,:,:,:)=tmp(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
            fprintf('.');
        end;
        
    if true
        FeatureData.img_smooth_is_pol=cast(0,classid);
        FeatureData.alphas_img=img_slocal_sdv; 
    %else 
    %    FeatureData.img_smooth_is_pol=single(1);
    %    FeatureData.alphas_img=reshape(sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat),[poldeg+1,cshape]);    
    end;
%      radfunc2=@(x)sqrt(x);
%     positions2=radfunc2(Scales);
%     poldeg2=nscales;
%      mondeg2=[poldeg2,0,0,0];
%      [pm2,p2]=mhs_makpolynom('mondeg',mondeg2,'poldeg',poldeg2,'even',even);
%      polmat2=[ones(nvalues,1),pm2(positions2(1,:)')];
%      pimat2=pinv(polmat2);
%      FeatureData.alphas_img2=reshape(sta_matmult(squeeze((max(img_slocal_sdv(:,:),eps))),pimat2),[poldeg2+1,cshape]);
%      FeatureData.alphas_img3=img_slocal_sdv(:,:);
    %FeatureData.alphas_img2=reshape(sta_matmult(squeeze(log(max(img_slocal_sdv(:,:),eps))),pimat2),[poldeg2+1,cshape]);
    %FeatureData.alphas_img2=reshape(sta_matmult(squeeze(log(max(img_slocal_sdv(:,:),eps))),pimat),[poldeg+1,cshape]);
    
    clear img_slocal_sdv; 
    fprintf('\n');

    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);
    
    if exist('override_scales')
        assert(numel(override_scales)==2);
        assert(override_scales(1)<override_scales(2));
        FeatureData.override_scales=cast(override_scales,classid);
    end;


    if sum(psf~=1)>0
        FeatureData.psf=cast(psf,classid);
    end;
    
    FeatureData.datafunc=cast(7,classid);
    
    if max(abs(element_size-1))<eps
        FeatureData.cshape=cast(cshape,classid);
        FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    else
        
        cshape=ceil(cshape.*element_size);
        
        FeatureData.cshape=cast(cshape,classid);
        FeatureData.img=scaleimage3D(img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3)),cshape,true);
    end;

    if exist('Mask')
        FeatureData.alphas_gradient(:,:,Mask(:)<1)=0;
        FeatureData.alphas_hessian(:,:,Mask(:)<1)=0;
        FeatureData.gradient_vessel(:,Mask(:)<1)=0;
    end;
    
    FeatureData.trueSF=cast(trueSF,classid);

 
    
    
    
    
    
    
    
 
    