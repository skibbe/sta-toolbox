function pm=mhs_makpolynom(varargin)

mondeg=[8,8,0,0];
poldeg=8;
even=true;
recreate=false;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

fname=['poldata_',num2str(mondeg(1)),'_',num2str(mondeg(2)),'_',num2str(mondeg(3)),'_',num2str(mondeg(4)),'_',num2str(poldeg(1)),'_',num2str(poldeg(1)),'_',num2str(even),'.mat'];

if (~exist(fname))||recreate
    fprintf('creating %s (symbolic toolbox required)\n',fname);
else
   fprintf('loading %s\n',fname);
   load(fname);
   return;
end



numvars=sum(mondeg~=0);
switch numvars
    case 1
        syms p(x);
    case 2
        syms p(x,y);
    case 3
        syms p(x,y,z);
    case 4
        syms p(x,y,z,w);        
end;

P='[';
count=1;
for d4=0:mondeg(4)
   for d3=0:mondeg(3)
        for d2=0:mondeg(2)
           for d1=0:mondeg(1)
                if d1+d2+d3+d4<=poldeg
                    if d1+d2+d3+d4>0
                         if mod(d1+d2+d3+d4,2)==0 || (~even)
                         %if (mod(d1,2)==0 &&  mod(d2,2)==0 && mod(d3,2)==0 && mod(d4,2)==0) || (~even)
                             M=['x^',num2str(d1),'*y^',num2str(d2),'*z^',num2str(d3),'*w^',num2str(d4)];
                             if count==1
                                P=[P,M];
                             else
                                P=[P,',',M];
                             end;
                             count=count+1;
                         end;
                    end;
                end;
            end;
        end;
    end;
end;
P=[P,']'];



switch numvars
    case 1
        p(x)=P;
    case 2
        p(x,y)=P;
    case 3
        p(x,y,z)=P;
    case 4
        p(x,y,z,w)=P;
end;


pm = matlabFunction(p+0);






fprintf('polynom: %s\n\n',char(p));
fprintf('values - fit = diff:\n');


    
fprintf('\n');
fprintf('\n');
fprintf('const   T * & a=alphas;\n');

fprintf('const T & x1=vector.v[0];\n');

if numvars>1
    fprintf('const T & y1=vector.v[1];\n');
end;
if numvars>2
    fprintf('const T & z1=vector.v[2];\n');
end;
if numvars>3
    fprintf('const T & w1=vector.v[3];\n');
end;

fprintf('\n');

Sym=['x','y','z','w'];
for a=1:4
    v=Sym(a);
    for d=2:min(mondeg(a),poldeg)
        fprintf(['T ',v,num2str(d),'=',v,num2str(d-1),'*',v,'1;\n']);
    end;
end;

fprintf('\n');


switch numvars
    case 1
        ptmp=p(x);
    case 2
        ptmp=p(x,y);
    case 3
        ptmp=p(x,y,z);
    case 4
        ptmp=p(x,y,z,w);
end;


codep='r=a[0]';
for m=1:numel(ptmp)
    mo=char(ptmp(m));
    
    for a=1:4
        v=Sym(a);
        for d=2:min(mondeg(a),poldeg)
            repalce_me=[v,'^',num2str(d)];
            with=[v,num2str(d)];
            mo=strrep(mo,repalce_me,with);            
        end;
    end;
     for a=1:4
         v=Sym(a);
         if strcmp(mo,v)
             mo=[mo,'1'];
         end;
         
            repalce_me=[v,'*'];
            with=[v,'1*'];
            mo=strrep(mo,repalce_me,with);            
            
            if mo(end)==v
                mo=[mo,'1'];
            end;

     end;
     %if m>1
        codep=[codep,'+a[',num2str(m),']*',mo];
     %else
     %   codep=mo;
     %end;
    %fprintf('%s\n',mo);
end;

     

fprintf('%s;\n\n\n',codep);



codep='r=*a++';
fprintf('%s;\n',codep);
for m=1:numel(ptmp)
    mo=char(ptmp(m));
    
    for a=1:4
        v=Sym(a);
        for d=2:min(mondeg(a),poldeg)
            repalce_me=[v,'^',num2str(d)];
            with=[v,num2str(d)];
            mo=strrep(mo,repalce_me,with);            
        end;
    end;
     for a=1:4
         v=Sym(a);
         if strcmp(mo,v)
             mo=[mo,'1'];
         end;
         
            repalce_me=[v,'*'];
            with=[v,'1*'];
            mo=strrep(mo,repalce_me,with);            
            
            if mo(end)==v
                mo=[mo,'1'];
            end;

     end;
     %if m>1
        codep=['r+=(*a++)*',mo];
        fprintf('%s;\n',codep);
     %else
     %   codep=mo;
     %end;
    %fprintf('%s\n',mo);
end;

     
save(fname,'pm');

