%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs_Simple(img,varargin)

    epsilon=0.01;

    crop=[10,10,10];
    poldeg=3;
    % sigman=-1;
    saliancy_offset=-1;
    Scales_fac=1;
    
    scale_range=[1.15,8];
    nscales=4;
 
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;

    scale_stepsize=(scale_range(2)-scale_range(1))/(nscales-1);
    Scales=scale_range(1):scale_stepsize:scale_range(2);
    
    classid=class(img);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);

    nscales=numel(Scales);
    
    poldeg=min(poldeg,nscales);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);


    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    FeatureData.scales=Scales;

    FeatureData.saliency_map=zeros(cshape,classid);
   
    
    
    fprintf('computing local normalizations: ');
    img_slocal_sdv=zeros([nscales,cshape],classid);
    for sindx=1:nscales
        scale=Scales(sindx);
        sigmanorm=scale*1.5;
        img_slocal_sdv(sindx,:,:,:)=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',false);
        fprintf('.');
    end;
    fprintf('\n');
    
    normalize_derivatives=false;
    
   clear mean mean2 
     
    fprintf('computing saliency map: ');
    for sindx=1:nscales
        scale=Scales(sindx);
        std_dev=1./(img_slocal_sdv(sindx,:)+epsilon);
        
        HField=reshape(repmat(std_dev(:)',6,1),[6,cshape]).*(Hessian(img,scale,'normalize',normalize_derivatives,'crop',crop));
        %HField=HField(:,crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
        
        Lap=(sum(HField(1:3,:,:,:),1));

        %[myevec_second,myev_second]=(sta_EVGSL(HField));
        
        HField(1,:,:,:)=HField(1,:,:,:)-Lap;
        HField(2,:,:,:)=HField(2,:,:,:)-Lap;
        HField(3,:,:,:)=HField(3,:,:,:)-Lap;
        
        [myevec,myev]=(sta_EVGSL(HField));
        N=scale^2/((2*pi*scale^2)^(3/2));
        %FH=N*myev(1,:).*(myev(1,:)>0).*((myev_second(1,:)<=-eps)&(myev_second(2,:)<=-eps)).*(myev_second(2,:)./(eps+myev_second(1,:))>0.25);
        FH=N*myev(1,:).*(myev(1,:)>0);
        FeatureData.saliency_map=max(reshape(FH,cshape),FeatureData.saliency_map);
    end;
    
    clear HField myev_second myevec_second myevec myev Lap OT FH  GX GY GZ G
        
    mondeg=[poldeg,0,0,0];
    even=false;
    [pm,p]=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);

    radfunc=@(x)sqrt(x);
    
    positions=radfunc(Scales);
    nvalues=size(positions,2);
    polmat=[ones(nvalues,1),pm(positions(1,:)')];
    pimat=pinv(polmat);
    
    clear Hessian_img
    

    prop_min=0.001;
    if (saliancy_offset>0)
        assert(~(saliancy_offset>1));
        prop_min=max(FeatureData.saliency_map(:))*saliancy_offset;
    end;
    
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    
    
   %FeatureData.debug_sdv=img_slocal_sdv;
    FeatureData.alphas_sdv=sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat);
    
    
      
    for sindx=1:nscales
        scale=Scales(sindx);
        tmp=mhs_smooth_img(img,scale,'normalize',true);
        img_slocal_sdv(sindx,:,:,:)=tmp(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    end;
    FeatureData.alphas_img=sta_matmult(squeeze(img_slocal_sdv(:,:)),pimat);
    clear img_slocal_sdv; 


%      scale=1.5;
%      sigmanorm=scale*1.5;
%      img_smoothed_normed=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon).*...
%          mhs_smooth_img(img,scale,'normalize',true);
%      img_smoothed_normed=img_smoothed_normed(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%      FeatureData.img_smoothed=img_smoothed_normed;
     
%     scale=1.5;
%     sigmanorm=scale*1.5;
%     std_dev=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon);
%     std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
%     FeatureData.gradient_vessel=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);


    FeatureData.scales=cast(Scales,classid);
    FeatureData.Scales_fac=cast(Scales_fac,classid);
    
    if exist('override_scales')
        assert(numel(override_scales)==2);
        assert(override_scales(1)<override_scales(2));
        FeatureData.override_scales=cast(override_scales,classid);
    end;


    FeatureData.datafunc=single(10);
    FeatureData.cshape=single(cshape);

 
    
    
    
    
    
    
    
 
    