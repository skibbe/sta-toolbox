function [alphas,positions,values,debug]=mhs_polfit(FeatureData,varargin)


unitsphere=false;
mondeg=[8,8,0,0];
poldeg=8;
even=true;
radfunc=@(x)x;
field='vesselimgD';


for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


[pm,p]=mhs_makpolynom('mondeg',mondeg,'poldeg',poldeg,'even',even);




% 
% numvars=sum(mondeg~=0);
% switch numvars
%     case 1
%         syms p(x);
%     case 2
%         syms p(x,y);
%     case 3
%         syms p(x,y,z);
%     case 4
%         syms p(x,y,z,w);        
% end;
% 
% P='[';
% count=1;
% for d4=0:mondeg(4)
%    for d3=0:mondeg(3)
%         for d2=0:mondeg(2)
%            for d1=0:mondeg(1)
%                 if d1+d2+d3+d4<=poldeg
%                     if d1+d2+d3+d4>0
%                          if mod(d1+d2+d3+d4,2)==0 || (~even)
%                          %if (mod(d1,2)==0 &&  mod(d2,2)==0 && mod(d3,2)==0 && mod(d4,2)==0) || (~even)
%                              M=['x^',num2str(d1),'*y^',num2str(d2),'*z^',num2str(d3),'*w^',num2str(d4)];
%                              if count==1
%                                 P=[P,M];
%                              else
%                                 P=[P,',',M];
%                              end;
%                              count=count+1;
%                          end;
%                     end;
%                 end;
%             end;
%         end;
%     end;
% end;
% P=[P,']'];
% 
% p(x,y)=P;
% pm = matlabFunction(p);





%Data=getfield(FeatureData,field);
values=getfield(FeatureData,field);

% if dims==2
%    % values=reshape(Data,[size(Data,1)*size(Data,2),size(Data,3),size(Data,4)]);
% elseif dims==3
% %     if numel(size(Data))==4
% %         values=reshape(Data,[size(Data,1)*size(Data,2),size(Data,3),size(Data,4),size(Data,5)]);
% %     elseif numel(size(Data))==4
% %         values=reshape(Data,[size(Data,1),size(Data,2),size(Data,3),size(Data,4)]);    
% %    elseif numel(size(Data))==3
%        % values=Data;
% %     end
% else
%     error('asass');
% end


positions=[];
if unitsphere
        %ndirs=size(FeatureData.ofield_dirs,2);
        positions=[positions,FeatureData.ofield_dirs{end}];
else
    for s=1:numel(FeatureData.scales)
        scale=radfunc(FeatureData.scales(s));
        ndirs=size(FeatureData.ofield_dirs{s},2);
        positions=[positions,FeatureData.ofield_dirs{s}.*repmat(scale,3,ndirs)];
    end;
end;


nvalues=size(positions,2);
% if dims==2
%     polmat=[ones(nvalues,1),pm(positions(1,:)',positions(2,:)')];
% elseif dims==3
    polmat=[ones(nvalues,1),pm(positions(1,:)',positions(2,:)',positions(3,:)')];
% else
%     error('asass');
% end
pimat=pinv(polmat);






%    values(:,:)=values(:,:)-repmat(FeatureData.vessel_min(:)',size(values,1),1);
%    values(:,:)=values(:,:).*(repmat(FeatureData.vessel_max(:)'./(0.0001+FeatureData.vessel_max(:)'-FeatureData.vessel_min(:)'),size(values,1),1));
    
% values(:,:)=values-repmat(FeatureData.vessel_min(:),size(values,1),1);
% values(:,:)=values.*(repmat(mmaxv(:,:)./(eps+mmaxv(:,:)-mminv(:,:)),size(values,1),1));

%     vmin=min(values,[],1);%vmin=vmin.*(vmin>0);
%     vmax=max(values,[],1);
%     values(:,:)=values(:,:)-repmat(vmin(:,:),size(values,1),1);
%     values(:,:)=values(:,:).*repmat(vmax(:,:),size(values,1),1)./repmat(eps+vmax(:,:)-vmin(:,:),size(values,1),1);

alphas=sta_matmult(values(:,:),pimat(:,:));
alphas=reshape(alphas,[size(alphas,1),size(FeatureData.img)]);




fprintf('num monoms: %d\n\n',size(alphas,1));

if nargout>3
    numtests=size(positions,2);
    debug=zeros(size(values));
    for a=1:numtests,
        debug(a,:)=sta_evalpol(alphas,{'vec',positions(:,a)});
    end;
    
end;


% figure(24);
% clf;
% hold on;
% plot(values(:,84,61,90));plot(debug(:,84,61,90),'r');plot(abs(values(:,84,61,90)-debug(:,84,61,90)),'g');
% 
% numtests=size(positions,2);
% for a=1:numtests,
%     gt=reshape(values(a,:),size(FeatureData.img));
%     %test=reshape(sta_evalpol(alphas,{'vec',positions(:,a)}),size(FeatureData.img));
%     test=reshape(debug(a,:),size(FeatureData.img));
%     simul(a)=sqrt(sum((gt(:)-test(:)).^2))/(sqrt(sum(gt(:).^2)));
%     simul2(a)=abs(max(gt(:))-max(test(:)))/abs(max(gt(:)));
%     figure(12314);
%     subplot(2,1,1);
%     imagesc(squeeze(gt(:,:,ceil(end/2))))
%     colorbar
%     subplot(2,1,2);
%     imagesc(squeeze(test(:,:,ceil(end/2))))
%     colorbar
%    % pause(0.5);
% end;
% figure(12315);plot(simul)
% figure(12316);plot(simul2)
% fprintf('end test\n')

%%






