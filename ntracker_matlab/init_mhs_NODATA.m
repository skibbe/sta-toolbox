function [FeatureData,A]=init_mhs_NODATA(A,shape)

if nargin<2
    shape=A.cshape;
end;

assert(nargout==2);

FeatureData.Scales_fac=single(1);
FeatureData.scales=single(A.scales);
FeatureData.datafunc=single(99);
FeatureData.cshape=single(shape);
FeatureData.shape_new_offset=single([0,0,0]);


if isfield(A,'W')
    for w=1:numel(A.W)
        A.W(w).A.data(15,:)=0;
        A.W(w).A.data(16,:)=1;
    end;
else
    A.data(15,:)=0;
    A.data(16,:)=1;
    A=rmfield(A,'options');
end;