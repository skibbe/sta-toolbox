%
%
%   FeatureData.vesselimg      : oriented vesselness img    [OMAT x Scales x W x H]
%   FeatureData.saliency_map    : maximum vesselness img    [W x H]
%   FeatureData.saliency_map_acc: vesselness   (bin search) [W x H]
%   FeatureData.scales          : scales                    [scales]
%
%
%
function [FeatureData]=init_mhs3Dvessel(img,varargin)


crop=[10,10,10];
epsilon=0.01;
%Scales=[3];
threshold=0.1;

a=0.5;
b=0.5;
c=0.5;

Scales_fac=1.5;
trueSF=false;
%ndirs=32;
poldeg=4;
ndensescales=5;
sigman=-1;
%normalization='STD';
normalization='NONE';
keep_raw_data=false;


 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    

    if ~exist('poldeg_vdir')
        poldeg_vdir=poldeg;
    end

    
    
    classid=class(img);
    threshold=cast(threshold,classid);
    Scales=cast(Scales,classid);

    FeatureData.Scales_fac=cast(Scales_fac,classid);

    nscales=numel(Scales);

    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

    sphere_min_max_res=[16,66];
    
    
    ndirs=0;
    for sindx=1:nscales
        smpts=ceil(sphere_min_max_res(1)+(sphere_min_max_res(2)-sphere_min_max_res(1))*(sindx-1)/(nscales-1));
        fprintf('nupts %d\n',smpts);
        dirs=cast(sphere2pts(smpts,true),classid);
        FeatureData.ofield_dirs{sindx}=dirs;   
        ndirs=ndirs+smpts;
    end;


    FeatureData.shape_org=shape;
    FeatureData.shape_new_offset=crop;
    
    FeatureData.img=img(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    FeatureData.scales=Scales;

    FeatureData.saliency_map=zeros(cshape,classid);
    FeatureData.saliency_map_acc=zeros([nscales,cshape],classid);
    
    if sigman==-1
        sigman=max(Scales);
    end;
    
    %if strcmp(normalization,'STD')
        mean=mhs_smooth_img(img,sigman,'normalize',true);
        mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
        std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
        std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    %end;
    clear mean2 mean
    
    
% COMPUTING HESSIAN
    for sindx=1:nscales
        scale=Scales(sindx);
        if strcmp(normalization,'STD')
                
            HField=scale^2*reshape(repmat(std_dev(:)',6,1),[6,cshape]).*Hessian(img,scale,'crop',crop,'normalize',true);
        else
            HField=scale^2*Hessian(img,scale,'crop',crop,'normalize',true);
        end;        
                
        
        HField_back{sindx}=HField;
    end;
    clear HField 
    
% COMPUTING VESSELNESS
    
    V=zeros([nscales,cshape],'single');
    

    for sindx=1:nscales
        scale=Scales(sindx);
        HField=HField_back{sindx};        
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        
        S=squeeze(sqrt(sum(myev_main.^2,1)));
        Ra=squeeze(abs(myev_main(2,:,:,:))./(eps+abs(myev_main(1,:,:,:))));
        Rb=squeeze(abs(myev_main(3,:,:,:))./sqrt((eps+abs(myev_main(1,:,:,:).*abs(myev_main(2,:,:,:))))));
        V(sindx,:,:,:)=(1-exp(-Ra.^2/(2*a^2))).*exp(-Rb.^2/(2*b^2)).*(1-exp(-S.^2/(2*c^2))).*squeeze(((myev_main(1,:,:,:)<0)&(myev_main(2,:,:,:)<0)));
        %myevec{sindx}=myevec_main(1:3,:,:,:);
        myevec{sindx}=myevec_main(7:9,:,:,:);
    end;
    
    clear HField_back tmp S Ra Rb myevec_main myevec_second myev_main myev_second 
    
    
% COMPUTING 1. EIGENVECTOR POLYNOM    
    
    FeatureData.vesselimgD=zeros([ndirs,cshape],classid);
    offset=0;
     for sindx=1:nscales  
        scale=Scales(sindx);
        
        %GaussNfact=1/scale;
        GaussNfact=scale^2;
        %GaussNfact=1;
        OT=cat(1,cat(1,myevec{sindx}(1,:).^2,myevec{sindx}(2,:).^2,myevec{sindx}(3,:).^2),myevec{sindx}(1,:).*myevec{sindx}(2,:),myevec{sindx}(1,:).*myevec{sindx}(3,:),myevec{sindx}(2,:).*myevec{sindx}(3,:));
        OT=reshape(OT,[6,cshape]).*reshape(repmat(V(sindx,:)*(GaussNfact),6,1),[6,cshape]);
        
        if (keep_raw_data)
            FeatureData.OT{sindx}=OT;
        end;
        
        dirs=FeatureData.ofield_dirs{sindx};
        FeatureData.vesselimgD(offset+1:offset+size(dirs,2),:,:,:)=sta_orientdist(OT,{'dirs',dirs});
        offset=offset+size(dirs,2);
     end;
    
    fprintf('polfit\n');
    radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    FeatureData.radfunc_vesselness=radfunc;
    FeatureData.Alphas=mhs_polfit(FeatureData,'mondeg',[8,8,8,0],'poldeg',poldeg,'radfunc',radfunc,'even',true);
    fprintf('done\n');
    
clear myevec OT


FeatureData = rmfield(FeatureData,'vesselimgD');


    
    
% COMPUTING SALIENCY MAP

FeatureData.saliency_map=squeeze(max(V,[],1));


    
clear FH NI NH accimg mean mean2 ImgM Imean tmp    
clear myevec myev HField HField_back    HRawField_back myevec_second myev_second
    
    DScales=Scales(1)+[0:1:ndensescales-1]*(Scales(end)-Scales(1))/(ndensescales-1);
    
    
    prop_min=0.001;
    FeatureData.saliency_map=FeatureData.saliency_map+prop_min;
    accimg=FeatureData.saliency_map;
    FeatureData.saliency_map=FeatureData.saliency_map./sum(FeatureData.saliency_map(:));
    %prop_min=0.0001;
    %accimg(accimg<prop_min)=min(accimg(accimg>=prop_min));
    
    for a=2:numel(accimg),
        accimg(a)=accimg(a)+accimg(a-1);
    end;

    accimg=accimg./((accimg(end)));
    FeatureData.saliency_map_acc=accimg;
    


    
% COMPUTING GRADIENT IN PLANE

 FeatureData.curvatureVessel=zeros([ndirs,cshape],classid);
 offset=0;
 for sindx=1:nscales
        scale=Scales(sindx);
        %GaussNfact=sindx^2;
        GaussNfact=1/((2*pi)^(3/2));
        GaussNfact=GaussNfact/scale;
        
        %GField=Grad(img,scale,'crop',crop,'normalize',true);
        GField=Grad(img,scale,'crop',crop,'normalize',false);
        GField=GField.*reshape(repmat(std_dev(:)'*GaussNfact,3,1),[3,cshape]);        
        dirs=FeatureData.ofield_dirs{sindx};
        FeatureData.curvatureVessel(offset+1:offset+size(dirs,2),:,:,:)=sta_gradorientdist(GField,{'dirs',dirs});              
        %FeatureData.vesselimgD(offset:offset+size(dirs,2),:,:,:)=sta_orientdist(OT,{'dirs',dirs});
        offset=offset+size(dirs,2);
 end;

    
    fprintf('polfit\n');
    %radfunc=@(x)sqrt(x);%
    %radfunc=@(x)log(x)/log(Scales_fac);
    radfunc=@(x)sqrt(log(x)/log(Scales_fac));
    FeatureData.AlphasCurvature=mhs_polfit(FeatureData,'mondeg',[8,8,8,0],'poldeg',poldeg,'radfunc',radfunc,'even',true,'field','curvatureVessel');
    fprintf('done\n');
    
        
    

FeatureData = rmfield(FeatureData,'curvatureVessel');
    
    
% COMPUTING GRADIENT IN VESSEL DIR    

        %FeatureData.curvatureVesselDir=zeros(cshape,classid);
        GField=Grad(img,1.5,'crop',crop,'normalize',true);
        %GField=Grad(img,1.5,'crop',crop,'normalize',true);
        GField=GField.*reshape(repmat(std_dev(:)',3,1),[3,cshape]);        
        
        %GField = GVF3D(GField, 0.1, 10);
        %GField = GVF3D(GField, 0.1, 100);
        
        GFieldN=sqrt(sum(GField(:,:).^2,1));
        GField=GField./(reshape(repmat(GFieldN,3,1),[3,cshape])+eps);
        
        OT2=cat(1,cat(1,GField(1,:).^2,GField(2,:).^2,GField(3,:).^2),GField(1,:).*GField(2,:),GField(1,:).*GField(3,:),GField(2,:).*GField(3,:));
        OT2=reshape(OT2,[6,cshape]).*reshape(repmat(GFieldN(:,:),6,1),[6,cshape]);
        
        dirs=FeatureData.ofield_dirs{end};
        FeatureData.curvatureVesselDir=sta_orientdist(OT2,{'dirs',dirs});           
        
        fprintf('polfit\n');
        radfunc=@(x)x;
        FeatureData.AlphasCurvatureVDir=mhs_polfit(FeatureData,'mondeg',[8,8,8,0],'poldeg',poldeg_vdir,'radfunc',radfunc,'even',true,'field','curvatureVesselDir','unitsphere',true);
        fprintf('done\n');
    
FeatureData = rmfield(FeatureData,'curvatureVesselDir');    
    


    
    FeatureData.scales=DScales;
    FeatureData.vesselness=single(1);




 
    
    
    
    
    
    
    
 
    