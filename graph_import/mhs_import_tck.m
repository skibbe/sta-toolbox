function A=mhs_import_tck(fname,varargin)

if 0
   %%
   T=R;
   T=ntrack_graph_tools(R,{'tool',12,'dist',10,'params',{'connection_candidate_searchrad',200}});
   T=ntrack_graph_tools(T,{'tool',11,'iter',200,'weight',0.000000001,'params',{'connection_candidate_searchrad',200}});
   
   %--> R --> T? T=ntrack_graph_tools(R,{'tool',12,'dist',5,'params',{'connection_candidate_searchrad',200}});
   % ich denke so ist richtig:
   T=ntrack_graph_tools(T,{'tool',12,'dist',5,'params',{'connection_candidate_searchrad',200}});
    
    
   [D_,pathlengts]=ntrack_graph_tools(T,{'tool',3,'only_mark_paths',true,'params',{'connection_candidate_searchrad',200}});
   %pathlengts_m=min(pathlengts,5000);
   %%
   %pathlengts_m=min(pathlengts,5000);
   pathlengts_m=pathlengts;
   D3=T;
   D3.data(8,:)=0.1*((pathlengts_m/max(pathlengts_m)).^0.5*2);
   D4=D3;D4.data(8,:)=D4.data(8,:)*2; 
   
   %%
   addpath  /disk/k_raid/SOFTWARE/KAKUSHI-NOU/nifti_tool/
   img=load_untouch_nii('/disk/k_raid/skibbe-h/kn_paper_data/T_DTI/R01_0054_CM1060F/mod-dir-t2_org.nii');
   %p=inv([img.original.hdr.hist.srow_x;img.original.hdr.hist.srow_y;img.original.hdr.hist.srow_z;0,0,0,1]);
   %R=mhs_import_tck('dense_FB_FTR.tck',-1);
   R=mhs_import_tck('dense_FB_FTR.tck','resize',-1);
   p=inv([img.hdr.hist.srow_x;img.hdr.hist.srow_y;img.hdr.hist.srow_z;0,0,0,1]);
   R2=R;Rp=p*[R.data(1:3,:);ones(1,size(R.data,2))];R2.data(1:3,:)=Rp(1:3,:);
   imgR3=mhs_distmap(R2,size(squeeze(img.img)),'mode',1,'weight',true,'sigma',1,'lininterp',false,'maxdist',3);
   
   %%
    indx=sub2ind(size(img.img),round(R2.data(1,:)),round(R2.data(2,:)),round(R2.data(3,:)));
    img3=load_untouch_nii('/disk/k_raid/skibbe-h/kn_paper_data/T_DTI/R01_0054_CM1060F/tc_C03_t2.nii');
    inside=mhs_smooth_img(single(img3.img>10000),3,'normalize',true);
    mask=((inside./max(inside(:)))>0.25);
    pathids=R2.data(21,mask(indx));
    pathids=unique(R2.data(21,mask(indx)));R3=R2;for p=pathids,R3.data(15,R3.data(21,:)==p)=-1;end;
    R3.cshape=single(ceil(max(R3.data(1:3,:),[],2))+1);
    %R4=ntrack_graph_tools(R3,{'tool',5,'params',{'connection_candidate_searchrad',5,'opt_particles_per_voxel',100,'del_id',0}});
    R4=ntrack_graph_tools(R3_,{'tool',5,'del_id',0,'params',{'connection_candidate_searchrad',5,'opt_particles_per_voxel',100}});
    %%
    R4_=R4;R4_.data(3,:)=R4_.data(3,:)*2;
    %imgR4=mhs_distmap(R4_,size(squeeze(img.img)).*[1,1,2],'mode',1,'weight',true,'sigma',1.5,'lininterp',false,'maxdist',3,'gamma',1);
    imgR4=mhs_distmap(R4_,size(squeeze(img.img)).*[1,1,2],'mode',1,'weight',true,'sigma',1,'lininterp',false,'maxdist',3,'gamma',1,'normalize',false);
    %imgR4=mhs_distmap(R4_,size(squeeze(img.img)).*[1,1,2],'mode',1,'weight',false,'sigma',1.5,'lininterp',false,'maxdist',3,'gamma',1);
    imgR4=min(imgR4,15);
    %%
    img_out=imgR4;
    voxel_size=[0.1,0.1,0.1];
    nifti = make_nii(single(img_out),voxel_size, [0,0,0], 16);
    M=([img.hdr.hist.srow_x;img.hdr.hist.srow_y;img.hdr.hist.srow_z;0,0,0,1]);Ms=eye(4);Ms(3,3)=0.5;
    nifti.hdr.hist.sform_code = 1;
    Mo=M*Ms;
    nifti.hdr.hist.srow_x(1:4)=Mo(1,:);
    nifti.hdr.hist.srow_y(1:4)=Mo(2,:);
    nifti.hdr.hist.srow_z(1:4)=Mo(3,:);
    save_nii(nifti,'DTI_track.nii');
    
end


flipdims=true;
resize=500;
scale=0.1;
min_p_length = 0;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


fid = fopen(fname);

offset=-1;
tline = fgetl(fid);
while (ischar(tline)) && (strcmp(tline,'END')==0)
    %disp(tline)
    %offset=tline;
    tline=strsplit(tline,' ');
    if strcmp(tline{1},'file:')
        offset=str2num(tline{3});
    end
    
    tline = fgetl(fid);
end
%offset=strsplit(offset,' ');

%offset=str2num(offset{end});
disp(['[',num2str(offset),']'])

fseek(fid,offset,'bof');
raw = fread(fid, 'single', 'l');
raw=reshape(raw,[3,numel(raw)/3]);

raw=raw([3,2,1],:);

%A=raw;



pmask=(isnan(raw(1,:)));

term_indx=(cumsum(pmask(end:-1:1))==0);
term_indx=find(term_indx(end:-1:1));
if numel(term_indx)>0
    term_indx=term_indx(1)-1;
    raw=raw(:,1:term_indx);
    pmask=(isnan(raw(1,:)));
end


%pmask(end)=true;
path_end=find(pmask);

    %raw=raw(:,1:path_end(2));
    %pmask=(isnan(raw(1,:)));
    %path_end=find(pmask);

    
if min_p_length>0    
    path_lengts = path_end-[0,path_end(1:end-1)];  
    raw_size = path_end(end);
    mask_raw=ones(1,raw_size) > 0;
    %mask_path_ends=ones(1,numel(path_end)) > 0;
    path_start = [1,path_end(1:end-1)+1];
    deleted_paths = 0;
    for p=1:numel(path_end)
        is_valid = path_lengts(p) > min_p_length;
        deleted_paths = deleted_paths + ~is_valid;
        mask_raw(path_start(p):path_end(p)) = is_valid;
        %mask_path_ends(p) = is_valid;
    end
    %path_end = path_end(mask_path_ends);
    raw = raw(:,mask_raw);
    pmask=(isnan(raw(1,:)));
    path_end=find(pmask);
    fprintf('deleted %d tracks\n',deleted_paths);
end

assert(min(path_end(1:end-1)-path_end(2:end))<-2)

paths_l=path_end-1;

raw_filled=raw;
raw_filled(:,path_end)=raw_filled(:,paths_l);
n=raw_filled(:,1:end-1)-raw_filled(:,2:end);

n=n./(repmat(sqrt(sum(n.^2,1)),3,1)+eps);

    n2=raw_filled(:,1:end-1)-raw_filled(:,[1,1:end-2]);
    n2=-n2./(repmat(sqrt(sum(n2.^2,1)),3,1)+eps);
    n=(n2+n)/2;
    n=n./(repmat(sqrt(sum(n.^2,1)),3,1)+eps);



n(:,path_end-1)=n(:,path_end-2);
%n=[n,n(:,end)];


s=size(raw);

num_pts=s(2)-numel(path_end);


path_labels=cumsum(pmask)+1;

A.data=zeros(26,num_pts);
A.data(1:3,:)=raw(:,~pmask);
A.data(4:6,:)=-n(:,~pmask);
A.data(21,:)=path_labels(~pmask);

term_pts=zeros(1,numel(path_end));
term_pts(paths_l)=1;
term_pts=term_pts(~pmask);

%A.data(1:6,:)=B(1:6,:);
    
if resize>1
    A.data(1,:)=A.data(1,:)-min(A.data(1,:));
    A.data(2,:)=A.data(2,:)-min(A.data(2,:));
    A.data(3,:)=A.data(3,:)-min(A.data(3,:));
    shape=max(A.data(1:3,:),[],2);

    standard_size=resize;

    A.data(1,:)=standard_size*A.data(1,:)./max(shape);
    A.data(2,:)=standard_size*A.data(2,:)./max(shape);
    A.data(3,:)=standard_size*A.data(3,:)./max(shape);
end;

  A.data(8,:)=scale;
  A.data(17,:)=0;
  A.data(22,:)=1;
  
  %A.data(7:9,:)=A.data(1:3,:)-A.data(1:3,:)
  
  
  
  term_pts=find(term_pts);
  %term_pts(1)=[];
  %term_pts(end)=[];
  
  con_ptids=[1:num_pts+1];
  con_ptids_A=con_ptids(1:end-1);
  con_ptids_B=con_ptids(2:end);
  %con_ptids_A((term_pts)-1)=[];
  %con_ptids_B((term_pts)-1)=[];
  con_ptids_A((term_pts))=[];
  con_ptids_B((term_pts))=[];
  
  num_con=num_pts-numel(path_end);
  
  A.connections=zeros(6,num_con);
  A.connections(1,:)=con_ptids_A-1;
  A.connections(2,:)=con_ptids_B-1;
  A.connections(3,:)=0;
  A.connections(4,:)=1;
  
  A.data(20,A.connections(1,:)+1)=1;
  A.data(19,A.connections(2,:)+1)=1;
  
  
  
  
  
  
  

  if flipdims
     A.data(1:3,:)  = A.data([3,2,1],:);
  end
  
  A.cshape=single(ceil(max(A.data(1:3,:),[],2))+1)';
  A.scales=single([-eps,0,eps]+scale);
  
fclose(fid);