function [BB,B] = parseDiademFile(filename,fast)


if ~exist(filename,'file')
   BB=[];
   warning(['file ',filename,' not exists']);
   return;
end

%filename='superell/brainvessel/vesselimg.swc';
fid = fopen(filename);
s = textscan(fid,'%s','Delimiter','\n');
s = s{1};
%s{1}=[s{1},'<root>'];
%s{end+1}='</root>';
fclose(fid);
clear neuro_data B
count =1;
pathid=0;


for a=1:numel(s)
    if numel(s{a})>0
        if s{a}(1)~='#'
             %vec=str2num(s{a});
             stmp=s{a};spos=strfind(stmp,'#');if ~isempty(spos), stmp=stmp(1:spos-1);end;
             vec=str2num(stmp);
             if vec(end)<0
                 pathid=pathid+1;
             end;
             ID=vec(1);
             edge=[ID,vec(end)];
             pos=vec(3:5);
             pos=pos([2,1,3]);
             scale=vec(6);
             
             if any(pos<0)
               warning('point pos <0; correcting boundary');                 
               pos(pos<0)=0; 
             end;           
             
             neuro_data(count).ID=ID;
             neuro_data(count).edge=edge;
             neuro_data(count).pos=pos;
                   
          
             neuro_data(count).scale=scale;
             neuro_data(count).pathid=pathid;
             count=count+1;
        end;
    end;
end;

if ~exist('neuro_data','var')
    BB=[];
   warning(['file ',filename,' has no track info']);
   return;
end;


clear B
B.data=zeros(25,numel(neuro_data));

edges=0;
for a=1:numel(neuro_data)
    ID=neuro_data(a).ID+1;
    pos=neuro_data(a).pos;
    %pos=pos([2,1,3]);
    B.data(1:3,ID)=pos;
   
    
    B.data(4:6,ID)=[0,0,1];
    B.data(7,ID)=0;
    B.data(8,ID)=max(neuro_data(a).scale,0.01);
    
    B.data(9:11,ID)=pos;
    B.data(12:14,ID)=pos;
    
    B.data(21,ID)=neuro_data(a).pathid;
    
    edges=edges+min(neuro_data(a).edge>0);
    
end;


B.connections=zeros(6,edges);

count=1;
for a=1:numel(neuro_data)
        if min(neuro_data(a).edge>0)
        B.connections(1,count)=neuro_data(a).edge(1);
        B.connections(2,count)=neuro_data(a).edge(2);
        %B.data(19:20,B.connections(2,count)+1)=1;
        B.data(22,B.connections(2,count)+1)=1;
        B.data(22,B.connections(1,count)+1)=1;
        count=count+1;
        end;
end;

for b=1:size(B.connections,2)
    %idA= B.connections(1,b);
    %idB= B.connections(2,b);
    
    idA= B.connections(1,b)+1;
    idB= B.connections(2,b)+1;
    
    
    N=B.data(1:3,idA)-B.data(1:3,idB);
    N=N./norm(N);
    B.data(4:6,idA)=N;
    B.data(4:6,idB)=N;
    pos=B.data(1:3,idA);
    B.data(9:11,idA)=pos+N;
    B.data(12:14,idA)=pos-N;
    pos=B.data(1:3,idB);
    B.data(9:11,idB)=pos+N;
    B.data(12:14,idB)=pos-N;    

end;

if (nargin > 1) && ~fast
    
    [paths,BB]=extract_paths(B,'spacing',1.5);
    B=mhs_assign_path_ids(B);
    BB=mhs_assign_path_ids(BB);
else
    %if size(B.data,2)<10000
        C=mhs_assign_path_ids(B);
        if ~(isempty(C))
            B=C;
        end;
    %end;
    BB=B;
end;




