function B = parseSuperMHTXML(filename,mode)
% PARSEXML Convert XML file to a MATLAB structure.
set(0,'RecursionLimit',1000)
if 0
    fid = fopen(filename);
    s = textscan(fid,'%s','Delimiter','\n');
    s = s{1};
    s{1}=[s{1},'<root>'];
    s{end+1}='</root>';
    fclose(fid);

    fid = fopen('tmp_xml.xml', 'w');
    for a=1:numel(s)
        %fwrite(fid, magic(5), 'integer*4');
        fprintf(fid,'%s\n',s{a});
    end;
    fclose(fid);

    filename='tmp_xml.xml';
end;

try
   tree = xmlread(filename);
catch
   error('Failed to read XML file %s.',filename);
end

% Recurse over child nodes. This could run into problems 
% with very deeply nested trees.
try
   theStruct = parseChildNodes(tree);
catch
   error('Unable to parse XML file %s.',filename);
end
%load('theStruct.mat','theStruct');


fprintf('conveting data\n');

clear traces
count =1;

theStruct=theStruct.Children(2).Children(6);

for c=2:2:numel(theStruct.Children)
    fprintf('%d %d\n',c,numel(theStruct.Children));
    if strcmp(theStruct.Children(c).Name,'Item')
        %fprintf('%d\n',c);
        ID=str2num(theStruct.Children(c).Children(2).Children(2).Children(1).Data);
        
        pos=str2num(theStruct.Children(c).Children(4).Children.Data);
        vec=str2num(theStruct.Children(c).Children(6).Children.Data);
        
         NID=[];
%         for b=2:2:numel(theStruct.Children(c).Children)
%             if strcmp(theStruct.Children(c).Children(b).Name,'Neighbors')
%                 NID=[NID,str2num(theStruct.Children(c).Children(b).Attributes(1).Value)];
%                 
%             end;
%            
%         end;
        traces(count).ID=ID;
        traces(count).pos=pos;
        traces(count).NID=NID;
        traces(count).timestamp=-1;
        traces(count).PATHID=-1;
        traces(count).vec=vec/norm(vec);
        traces(count).scale=norm(vec);
        count=count+1;
    end;
end;


tmp=traces;

% build connections (temporarily)
for a=1:numel(traces)-1
     posA=traces(a).pos(1:3);
     posB=traces(a+1).pos(1:3);
     if norm(posA-posB)<25
         traces(a).NID=a;
         %fprintf('/');
     end;
end;

traces=assignpathid2(traces);


% identify & remove overlaps
overlap_dist=2.5;

pathids=unique([traces(:).PATHID]);
for p=1:numel(pathids)
    select=[traces(:).PATHID]==pathids(p);
    nonselect=[traces(:).PATHID]>pathids(p);
    %nonselect=~select;
    pA=cat(1,traces(select).pos);
    pB=cat(1,traces(nonselect).pos);
    if (~isempty(pA))&&(~isempty(pB))
        D=distmat(pA,pB);
    
        
        candidates=find(nonselect);
        candidates=candidates((max(D<overlap_dist*overlap_dist,[],2)));
        traces(candidates)=[];
    end;
end;

for a=1:numel(traces)
    traces(a).ID=a;
    traces(a).NID=[];
end;


%build connections 
for a=1:numel(traces)-1
     posA=traces(a).pos(1:3);
     posB=traces(a+1).pos(1:3);
     if norm(posA-posB)<15
         traces(a).NID=a;
         %fprintf('/');
     end;
end;

% all_ids=[1:numel(traces)];
% numN=zeros(1,numel(traces));
% %reassign conectivity
% for a=1:numel(traces)
%     if numel(traces(a).NID)==0
%          posA=traces(a).pos(1:3);
%          check_ids=all_ids((all_ids~=a)&(numN~=(a-1)));
%          
%          if numel(check_ids)>0
%              [min_v,min_indx]=min(distmat([posA,0,0,0],cat(1,traces(check_ids).pos)));
%              %posB=traces(a+1).pos(1:3);
%              if sqrt(min_v)<45
%                  traces(a).NID=traces(check_ids(min_indx)).ID-1;
%                  numN(a)=traces(a).NID;
%              end;
%          end;
%     end;
% end;


% all_ids=[1:numel(traces)];
% numN=zeros(1,numel(traces));
% %reassign conectivity
% for a=1:numel(traces)-1
%     if numel(traces(a).NID)==0
%          posA=traces(a).pos(1:3);
%          check_ids=all_ids((all_ids>a)&(numN<1));
%          
%          [min_v,min_indx]=min(distmat([posA,0,0,0],cat(1,traces(check_ids).pos)));
%          %posB=traces(a+1).pos(1:3);
%          if sqrt(min_v)<15
%              traces(a).NID=traces(check_ids(min_indx)).ID;
%              numN(a)=numN(a)+1;
%              fprintf('/');
%          end;
%     end;
% end;

traces=assignpathid2(traces);

 
clear B
B.data=zeros(22,numel(traces));

edges=0;
for a=1:numel(traces)
    
    ID=traces(a).ID;
    pos=traces(a).pos([2,1,3]);
    vec=traces(a).vec([2,1,3]);
    %pos=pos([2,1,3]);
    %pos(3)=shape(3)-pos(3)-1;
    %pos(1:2)=pos(1:2)+1;
    B.data(1:3,ID)=pos;
    
    B.data(4:6,ID)=vec;
    B.data(7,ID)=0;
    
    B.data(8,ID)=(traces(a).scale);
    
    B.data(9:11,ID)=pos+B.data(4:6,ID)';
    B.data(12:14,ID)=pos-B.data(4:6,ID)';
    
    
    B.data(21,ID)=traces(a).PATHID;
    
    B.data(end,ID)=1;
    
    edges=edges+sum(traces(a).NID>traces(a).ID);
    fprintf('%d %d (%.2f)\n',a,numel(traces),traces(a).scale);
end;


    
B.connections=zeros(4,edges);

count=1;
for a=1:numel(traces)
    for b=1:numel(traces(a).NID)
        if traces(a).NID(b)>traces(a).ID-1
        B.connections(1,count)=traces(a).ID-1;
        B.connections(2,count)=traces(a).NID(b);
        B.data(19:20,B.connections(2,count)+1)=1;
        count=count+1;
        end;
    end;
end;



%[paths,B]=extract_paths(B,'spacing',1,'smoothing',1,'smoothing_scale',1);
A=B;
connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          

terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
%terminalsTERMPos=A.data(1:3,terminalsTERM);



terminal_flag=(1==cellfun(@(x)numel(x),connection_listGT));
%assert(sum((cellfun(@(x)numel(x),connection_listGT))>2)==0);

for t=1:numel(terminalsTERM)
    
        tid=terminalsTERM(t);
    if terminal_flag(tid)%numel(connection_listGT{tid}==1)        
        if numel(connection_listGT{connection_listGT{tid}})==2
            cid=connection_listGT{tid};
            cid2=connection_listGT{cid};
            if numel(cid2)>1
                cid=cid2(cid2~=tid);
            end;
            
            pid1=B.data(21,tid);
            

            n=A.data(1:3,tid)-A.data(1:3,cid);
            assert(norm(n)>eps);
            n=n./(eps+norm(n));
            dvec=(A.data(1:3,:)-repmat(A.data(1:3,tid),1,size(A.data,2)));
            dvec=dvec./repmat(sqrt(sum(dvec.^2,1)),3,1);
            bif_candidates1=(sum(dvec.*repmat(n,1,size(A.data,2)),1)>0.25);
            bif_candidates2=sum((A.data(1:3,:)-repmat(A.data(1:3,tid),1,size(A.data,2))).^2,1);
            
            bif_candidates3=terminal_flag;%(terminal_flag|(A.data(21,:)~=pid1));

            bif_candidates2(~bif_candidates1)=inf;
            bif_candidates2(~bif_candidates3)=inf;
            [v,indx]=min(bif_candidates2);
            if v<15^2
                terminal_flag(tid)=false;
                terminal_flag(indx)=false;
                fprintf('adding %d<->%d\n',tid,indx);
                B.connections=[B.connections,[tid-1,indx-1,0,0]'];
                
                pid2=B.data(21,indx);
                B.data(21,B.data(21,:)==pid2)=pid1;
                connection_listGT{tid}=[connection_listGT{tid},indx];
                connection_listGT{indx}=[connection_listGT{indx},tid];
                
            else
               % fprintf('meep\n');
            end;

        else

        end;
    end;
end;



A=B;
connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          

for d=1:size(A.data,2)
    pid=B.data(21,d);
    for e=1:numel(connection_listGT{d})
        pid2=B.data(21,connection_listGT{d}(e));
       if  pid2~=pid
           B.data(21,B.data(21,:)==pid2)=pid1;
           fprintf('update\n');
       end;
    end;
end;




%assert(sum((cellfun(@(x)numel(x),connection_listGT))>2)==0);
terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));


path_ids=unique(B.data(21,:));

for b=1:numel(path_ids)
    pid1=path_ids(b);
    group1=find(B.data(21,:)==pid1);
    for c=b+1:numel(path_ids)
        pid2=path_ids(c);
        group2=find(B.data(21,:)==pid2);
        
        
        D=distmat(B.data(1:3,group1)',B.data(1:3,group2)');
        
        [min_v,min_G2]=min(D,[],1);
        [min_v,min_G1]=min(min_v,[],2);
        min_G2=min_G2(min_G1);
        %fprintf('%d %d %f\n',group1(min_G1),group2(min_G2),min_v);
        if min_v<5^2
         B.connections=[B.connections,[group1(min_G1)-1,group2(min_G2)-1,0,0]'];
            pid1=B.data(21,group1(min_G1));
            pid2=B.data(21,group2(min_G2));
            B.data(21,B.data(21,:)==pid2)=pid1;
        end;
    end;
end;
% 
% A=B;
% connection_listGT={};
% for d=1:size(A.data,2)
%     connection_listGT{d}=[];
% end;
% for d=1:size(A.connections,2)
%     pointIDA=A.connections(1,d)+1;
%     pointIDB=A.connections(2,d)+1;
%     connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
%     connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
% end;    
% 
% for d=1:size(A.data,2)
%     pid=B.data(21,d);
%     for e=1:numel(connection_listGT{d})
%         pid2=B.data(21,connection_listGT{d}(e));
%        if  pid2~=pid
%            B.data(21,B.data(21,:)==pid2)=pid1;
%            fprintf('update\n');
%        end;
%     end;
% end;

%B.data(21,terminalsTERM)=0;



% 
% 
% for t=1:numel(terminalsTERM)
%     tid=terminalsTERM(t);
%         if numel(connection_listGT{connection_listGT{tid}})==2
%             cid=connection_listGT{tid};
%             cid2=connection_listGT{cid};
%             if numel(cid2)>1
%                 cid=cid2(cid2~=tid);
%             end;
%             
%             pid1=B.data(21,tid);
%             
% 
%             n=A.data(1:3,tid)-A.data(1:3,cid);
%             assert(norm(n)>eps);
%             n=n./(eps+norm(n));
%             dvec=(A.data(1:3,:)-repmat(A.data(1:3,tid),1,size(A.data,2)));
%             dvec=dvec./repmat(sqrt(sum(dvec.^2,1)),3,1);
%             bif_candidates1=(sum(dvec.*repmat(n,1,size(A.data,2)),1)>0.25);
%             bif_candidates2=sum((A.data(1:3,:)-repmat(A.data(1:3,tid),1,size(A.data,2))).^2,1);
%             
% 
%             bif_candidates2(~bif_candidates1)=inf;
%             [v,indx]=min(bif_candidates2);
%             if v<15^2
%                 fprintf('adding %d<->%d\n',tid,indx);
%                 B.connections=[B.connections,[tid-1,indx-1,0,0]'];
%                 pid2=B.data(21,indx);
%                 B.data(21,B.data(21,:)==pid2)=pid1;
%             else
%                % fprintf('meep\n');
%             end;
% 
%         else
% 
%         end;
% end;





function traces=assignpathid2(traces)



pathid=2;


for a=1:numel(traces)
    if traces(a).PATHID==-1
        traces=assignid(traces,a,pathid,0);
        pathid=pathid+1;
    end;
end;


function traces=assignid(traces,a,pathid,d)

if traces(a).PATHID~=-1
    return;
end;


traces(a).PATHID=pathid;
for b=1:numel(traces(a).NID)
    traces=assignid(traces,traces(a).NID(b)+1,pathid,d+1);
end;



% ----- Local function PARSECHILDNODES -----
function children = parseChildNodes(theNode)
% Recurse over node children.
children = [];
if theNode.hasChildNodes
   childNodes = theNode.getChildNodes;
   numChildNodes = childNodes.getLength;
   allocCell = cell(1, numChildNodes);

   children = struct(             ...
      'Name', allocCell, 'Attributes', allocCell,    ...
      'Data', allocCell, 'Children', allocCell);

    for count = 1:numChildNodes
        if numChildNodes>100
            fprintf('%d %d\n',count,numChildNodes);
        end;
        theChild = childNodes.item(count-1);
        children(count) = makeStructFromNode(theChild);
    end
end

% ----- Local function MAKESTRUCTFROMNODE -----
function nodeStruct = makeStructFromNode(theNode)
% Create structure of node info.

nodeStruct = struct(                        ...
   'Name', char(theNode.getNodeName),       ...
   'Attributes', parseAttributes(theNode),  ...
   'Data', '',                              ...
   'Children', parseChildNodes(theNode));

if any(strcmp(methods(theNode), 'getData'))
   nodeStruct.Data = char(theNode.getData); 
else
   nodeStruct.Data = '';
end

% ----- Local function PARSEATTRIBUTES -----
function attributes = parseAttributes(theNode)
% Create attributes structure.

attributes = [];
if theNode.hasAttributes
   theAttributes = theNode.getAttributes;
   numAttributes = theAttributes.getLength;
   allocCell = cell(1, numAttributes);
   attributes = struct('Name', allocCell, 'Value', ...
                       allocCell);

   for count = 1:numAttributes
      attrib = theAttributes.item(count-1);
      attributes(count).Name = char(attrib.getName);
      attributes(count).Value = char(attrib.getValue);
   end
end