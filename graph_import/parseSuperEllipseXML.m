function [traces,B] = parseSuperEllipseXML(filename,mode)
% PARSEXML Convert XML file to a MATLAB structure.
set(0,'RecursionLimit',1000)
fid = fopen(filename);
s = textscan(fid,'%s','Delimiter','\n');
s = s{1};
s{1}=[s{1},'<root>'];
s{end+1}='</root>';
fclose(fid);

fid = fopen('tmp_xml.xml', 'w');
for a=1:numel(s)
    %fwrite(fid, magic(5), 'integer*4');
    fprintf(fid,'%s\n',s{a});
end;
fclose(fid);

filename='tmp_xml.xml';
try
   tree = xmlread(filename);
catch
   error('Failed to read XML file %s.',filename);
end

% Recurse over child nodes. This could run into problems 
% with very deeply nested trees.
try
   theStruct = parseChildNodes(tree);
catch
   error('Unable to parse XML file %s.',filename);
end

fprintf('conveting data\n');

clear traces
count =1;
for c=2:2:numel(theStruct.Children)
    fprintf('%d %d\n',c,numel(theStruct.Children));
    if strcmp(theStruct.Children(c).Name,'Superellipse')
        %fprintf('%d\n',c);
        ID=str2num(theStruct.Children(c).Attributes(3).Value);
        x=str2num(theStruct.Children(c).Attributes(end-2).Value);
        y=str2num(theStruct.Children(c).Attributes(end-1).Value);
        z=str2num(theStruct.Children(c).Attributes(end).Value);
        R=[1,0,0;0,1,0;0,0,1];
        for v=1:9
            R(v)=str2num(theStruct.Children(c).Attributes(v+5).Value);
        end;
        R=R([2,1,3],:);
        scale(1)=str2num(theStruct.Children(c).Attributes(16).Value);
        scale(2)=str2num(theStruct.Children(c).Attributes(17).Value);
        scale(3)=str2num(theStruct.Children(c).Attributes(18).Value);
        
        NID=[];
        for b=2:2:numel(theStruct.Children(c).Children)
            if strcmp(theStruct.Children(c).Children(b).Name,'Neighbors')
                NID=[NID,str2num(theStruct.Children(c).Children(b).Attributes(1).Value)];
                
            end;
           
        end;
        traces(count).ID=ID;
        traces(count).pos=[x,y,z];
        traces(count).NID=NID;
        traces(count).timestamp=-1;
        traces(count).PATHID=-1;
        traces(count).R=R;
        traces(count).scale=scale;
        count=count+1;
    end;
end;

traces=assignpathid2(traces);
 
 
clear B
B.data=zeros(22,numel(traces));

edges=0;
for a=1:numel(traces)
    
    ID=traces(a).ID+1;
    pos=traces(a).pos;
    pos=pos([2,1,3]);
    %pos(3)=shape(3)-pos(3)-1;
    %pos(1:2)=pos(1:2)+1;
    B.data(1:3,ID)=pos;
    
    B.data(4:6,ID)=traces(a).R*[0,0,1]';
    B.data(7,ID)=0;
    
    if nargin<2
        B.data(8,ID)=(traces(a).scale(1)+traces(a).scale(2))/2;
    else
        B.data(8,ID)=(traces(a).scale(mode));
        
    end;
    
    B.data(9:11,ID)=pos+B.data(4:6,ID)';
    B.data(12:14,ID)=pos-B.data(4:6,ID)';
    
    
    B.data(21,ID)=traces(a).PATHID;
    
    B.data(end,ID)=1;
    
    edges=edges+sum(traces(a).NID>traces(a).ID);
    fprintf('%d %d (%.2f)\n',a,numel(traces),traces(a).scale(1)/traces(a).scale(2));
end;


B.connections=zeros(4,edges);

count=1;
for a=1:numel(traces)
    for b=1:numel(traces(a).NID)
        if traces(a).NID(b)>traces(a).ID
        B.connections(1,count)=traces(a).ID;
        B.connections(2,count)=traces(a).NID(b);
        B.data(19:20,B.connections(2,count)+1)=1;
        count=count+1;
        end;
    end;
end;




function traces=assignpathid2(traces)



pathid=2;


for a=1:numel(traces)
    if traces(a).PATHID==-1
        traces=assignid(traces,a,pathid,0);
        pathid=pathid+1;
    end;
end;


function traces=assignid(traces,a,pathid,d)

if traces(a).PATHID~=-1
    return;
end;


traces(a).PATHID=pathid;
for b=1:numel(traces(a).NID)
    traces=assignid(traces,traces(a).NID(b)+1,pathid,d+1);
end;



% ----- Local function PARSECHILDNODES -----
function children = parseChildNodes(theNode)
% Recurse over node children.
children = [];
if theNode.hasChildNodes
   childNodes = theNode.getChildNodes;
   numChildNodes = childNodes.getLength;
   allocCell = cell(1, numChildNodes);

   children = struct(             ...
      'Name', allocCell, 'Attributes', allocCell,    ...
      'Data', allocCell, 'Children', allocCell);

    for count = 1:numChildNodes
        if numChildNodes>100
            fprintf('%d %d\n',count,numChildNodes);
        end;
        theChild = childNodes.item(count-1);
        children(count) = makeStructFromNode(theChild);
    end
end

% ----- Local function MAKESTRUCTFROMNODE -----
function nodeStruct = makeStructFromNode(theNode)
% Create structure of node info.

nodeStruct = struct(                        ...
   'Name', char(theNode.getNodeName),       ...
   'Attributes', parseAttributes(theNode),  ...
   'Data', '',                              ...
   'Children', parseChildNodes(theNode));

if any(strcmp(methods(theNode), 'getData'))
   nodeStruct.Data = char(theNode.getData); 
else
   nodeStruct.Data = '';
end

% ----- Local function PARSEATTRIBUTES -----
function attributes = parseAttributes(theNode)
% Create attributes structure.

attributes = [];
if theNode.hasAttributes
   theAttributes = theNode.getAttributes;
   numAttributes = theAttributes.getLength;
   allocCell = cell(1, numAttributes);
   attributes = struct('Name', allocCell, 'Value', ...
                       allocCell);

   for count = 1:numAttributes
      attrib = theAttributes.item(count-1);
      attributes(count).Name = char(attrib.getName);
      attributes(count).Value = char(attrib.getValue);
   end
end