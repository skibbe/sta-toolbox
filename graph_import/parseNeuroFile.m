function B = parseNeuroFile(filename)

%filename='superell/brainvessel/vesselimg.swc';
fid = fopen(filename);
s = textscan(fid,'%s','Delimiter','\n');
s = s{1};
%s{1}=[s{1},'<root>'];
%s{end+1}='</root>';
fclose(fid);
clear neuro_data B
count =1;
for a=1:numel(s)
    if numel(s{a})>0
        if s{a}(1)~='#'
             %vec=str2num(s{a});
             stmp=s{a};spos=strfind(stmp,'#');if ~isempty(spos), stmp=stmp(1:spos-1);end;
             vec=str2num(stmp);
             if numel(vec)<1
                 1
             end;
             ID=vec(1);
             edge=[ID,vec(end)];
             pos=vec(3:5)*10;
             pos=pos([2,1,3]);
             scale=vec(3:6);
             neuro_data(count).ID=ID;
             neuro_data(count).edge=edge;
             neuro_data(count).pos=pos;
             neuro_data(count).scale=scale;
             count=count+1;
        end;
    end;
end;


clear B
B.data=zeros(25,numel(neuro_data));

edges=0;
for a=1:numel(neuro_data)
    ID=neuro_data(a).ID+1;
    pos=neuro_data(a).pos;
    %pos=pos([2,1,3]);
    B.data(1:3,ID)=pos;
    
    B.data(4:6,ID)=[0,0,1];
    B.data(7,ID)=0;
    B.data(8,ID)=2;
    
    B.data(9:11,ID)=pos;
    B.data(12:14,ID)=pos;
    
    edges=edges+min(neuro_data(a).edge>0);
end;


B.connections=zeros(5,edges);

count=1;
for a=1:numel(neuro_data)
        if min(neuro_data(a).edge>0)
        B.connections(1,count)=neuro_data(a).edge(1);
        B.connections(2,count)=neuro_data(a).edge(2);
        B.data(19:20,B.connections(2,count)+1)=1;
        count=count+1;
        end;
end;
