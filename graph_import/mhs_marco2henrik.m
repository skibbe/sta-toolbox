function A=mhs_marco2henrik(FTR,scale,minlength,resize)

        if nargin<3
            minlength=-1;
        end;
        if nargin<2
            scale=0.25;
        end;
        
        if nargin<3
            resize=-1;
        end;

    B=FTR.user;
    
    num_pts=size(B,2);
    
    A.data=zeros(26,num_pts);
    A.data(1:6,:)=B(1:6,:);
    
    if resize>1
        A.data(1,:)=A.data(1,:)-min(A.data(1,:));
        A.data(2,:)=A.data(2,:)-min(A.data(2,:));
        A.data(3,:)=A.data(3,:)-min(A.data(3,:));
        shape=max(A.data(1:3,:),[],2);
    
        standard_size=resize;
    
        A.data(1,:)=standard_size*A.data(1,:)./max(shape);
        A.data(2,:)=standard_size*A.data(2,:)./max(shape);
        A.data(3,:)=standard_size*A.data(3,:)./max(shape);
    end;
    
    A.data(8,:)=scale;
    A.data(17,:)=0;
    
    connectionsA=B(9,:)+1;
    connectionsB=B(10,:)+1;
    
    
    pathids=zeros(1,num_pts);
    pathids(:)=-1;
    pathids_valid=zeros(1,num_pts);
    pathids_valid(:)=true;
  
    
    id_current=zeros(1,num_pts);
  
    
    current_id=1;
    for p=1:num_pts
        if (pathids(p)==-1)
            l=0;
            
            [pathids,l,id_current]=assign_id(connectionsA,connectionsB,current_id,pathids,p,l,id_current);
            
            if (minlength<0)||(l<minlength)
               pathids_valid(id_current(1:l))=false;
            end;
            current_id=current_id+1;
        end;
    end;
    
    A.data(21,:)=pathids;
    
    if minlength>0
     connectionsA(~pathids_valid)=-1;
     connectionsB(~pathids_valid)=-1;
    end;
    
    A.data(22,:)=(connectionsA>0)|(connectionsB>0);
    %A.data(22,:)=(connectionsA>-1)|(connectionsB>-1);
    %A.data(19,:)=(connectionsA>0);
    %A.data(20,:)=(connectionsB>0);
    

    indx=[1:num_pts];
    valid=(connectionsA>indx);
    connA=([indx(valid);connectionsA(valid)]);    
    
    valid=(connectionsB>indx);
    connB=([indx(valid);connectionsB(valid)]);
    
    
    num_connections=size(connB,2)+size(connA,2);
    A.connections=zeros(6,num_connections);
    
    
    A.connections(1:2,:)=[connA,connB]-1;
    
    shape=max(A.data(1:3,:),[],2);
    A.cshape=single(shape);
    A.scales=single(ceil([scale-0.0001*scale,scale,scale+0.0001*scale])+1);
    
    %set(0,'RecursionLimit',1500) 
    
    
    
    
    function [pathids,l,id_current]=assign_id(connectionsA,connectionsB,current_id,pathids,p,l,id_current)
        if (connectionsA(p)==0)&& (connectionsB(p)==0)
            pathids(p)=0;
              return;  
        end;
        if (pathids(p)>-1)
            return;
        end
        pathids(p)=current_id;
        l=l+1;
        id_current(l)=p;
            
        if connectionsA(p)>0
        if pathids(connectionsA(p))<0
            pnew=connectionsA(p);
            %pathids(connectionsA(p))=current_id;
            [pathids,l,id_current]=assign_id(connectionsA,connectionsB,current_id,pathids,pnew,l,id_current);
        end;
        end;
        if connectionsB(p)>0
        if pathids(connectionsB(p))<0
            pnew=connectionsB(p);
            %pathids(connectionsA(p))=current_id;
            [pathids,l,id_current]=assign_id(connectionsA,connectionsB,current_id,pathids,pnew,l,id_current);
        end;
        end;
        
        
        