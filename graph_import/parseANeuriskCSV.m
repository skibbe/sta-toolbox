function [Img,B,C] = parseANeuriskCSV(filename)


fid = fopen([filename,'/morphology/centerlines.csv']);

tline = fgetl(fid);
disp(tline);
P={};
count=1;
while ischar(tline)
    %disp(tline);
    tline = fgetl(fid);
    if ischar(tline)
        %A = fscanf(tline, '%f,%f,%f,%f');
        sep=find(tline==','); 
        A=[str2num(tline(1:sep(1)-1)),str2num(tline(sep(1)+1:sep(2)-1)),str2num(tline(sep(2)+1:sep(3)-1)),str2num(tline(sep(3)+1:end))];
        %P={P{:},A};
        P{count}=A;
        count=count+1;
        %disp(A);
    end;
end

fclose(fid);


tmp=reshape([P{:}],[4,numel(P)]);
tmp=tmp(1:3,:);
DM=distmat(tmp',tmp');

remove(1:numel(P))=false;
for p=1:numel(P)
    if sum(DM(p,1:p)<0.01)>1
      remove(p)=true;
    end;
end;


fprintf('removing %d points\n',sum(remove));

fi=dir([filename,'/dicom/IM_*']);
X = dicominfo([filename,'/dicom/',fi(1).name]);

Y = dicomread([filename,'/dicom/',fi(1).name]);

Img=zeros([size(Y),numel(fi)],'single');

for a=1:numel(fi)
    if mod(a,5)==0
        fprintf('.');
    end;
    Img(:,:,a)=single(dicomread([filename,'/dicom/',fi(a).name]));
end;
fprintf('\n');

Img=Img./2^16;
fprintf('image min/max: %f\n',min(Img(:)),max(Img(:)));
%Img=permute(Img,[1,3,2]);
Dim=size(Img);

if nargout>2
    C=todata(P,X,Dim);
end;
P={P{find(~remove)}};
B=todata(P,X,Dim);


function B=todata(P,X,Dim)


PA={};
count=1;
pathid=1;
for n=1:numel(P)
  if n>1
      if max(abs(P{n}(1:3)-P{n-1}(1:3)))>1
         pathid=pathid+1; 
      end;
  end;
    PA{n}=pathid;
end;


numpts=numel(P);
numconn=numpts-numel(unique([PA{:}]));

B.data=zeros(22,numconn);

for n=1:numpts
    %B.data(1:3,n)=2.825*P{n}([2,1,3]);
    %B.data(1:3,n)=2.825*P{n}([2,1,3]);
    %B.data(1:3,n)=2.82*P{n}([2,1,3]);
    B.data(1:3,n)=P{n}([2,1,3])/X.SliceThickness;
    
    %B.data(1:3,n)=2.85*P{n}([2,1,3]);
    B.data(1,n)=Dim(1)-B.data(1,n)-1;
    B.data(2,n)=Dim(2)-B.data(2,n)-1;
    B.data(3,n)=Dim(3)-B.data(3,n)-1;
    
    
    %B.data(3,n)=B.data(3,n)-1;
    %B.data([1:3],n)=Dim([1:3])-B.data(1:3,n)';
    
    B.data(8,n)=P{n}(4)/X.SliceThickness;
    B.data(21,n)=PA{n};
end;


for n=2:numpts
    if PA{n}==PA{n-1}
        N=B.data(1:3,n)-B.data(1:3,n-1);
        N=N/(norm(N)+eps);
        B.data(4:6,n)=N;
    else
        B.data(4:6,n-1)=N;
    end;
    if (n==2)
        B.data(4:6,n-1)=N;
    end;
end;

B.connections=zeros(4,numconn);

count=1;
for n=2:numpts
   if PA{n}==PA{n-1}
         B.connections(1,count)=n-2;
         B.connections(2,count)=n-1;
         B.data(19:20,n)=1;
         B.data(19:20,n-1)=1;
        count=count+1;
   end;
end;

assert(count-1==numconn)

