function [Img,B,spointsA,spointsB,esize_correct,Img_org,Pos_orgs] = parseRott(foldername,varargin)

skip_pts=5;
evalonly=false;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


spointsA=[];
spointsB=[];

imgname=dir([foldername,'/*.raw']);
imgname=imgname(1).name;

mhdname=dir([foldername,'/*.mhd']);
mhdname=mhdname(1).name;

fid = fopen([foldername,mhdname]);
tline = fgetl(fid);
%disp(tline);
while ischar(tline)
    %disp(tline);
    tline = fgetl(fid);
    if ischar(tline)
        spattern='DimSize = ';
        offset=strfind(tline,spattern);
        if numel(offset)==1, 
            shape=str2num(tline(offset+numel(spattern):end));
        end;
        spattern='ElementSpacing = ';
        offset=strfind(tline,spattern);
        if numel(offset)==1, 
            esize=str2num(tline(offset+numel(spattern):end));
        end;
        
    end;
end

fclose(fid);

fid = fopen([foldername,imgname]);
Img=single(reshape(fread(fid,'uint16'),shape));
if nargout>5
    Img_org=Img;
end;
%Img=Img./max(Img(:));
fclose(fid);

%Img=scaleimage3D(image,dim,smoothed,method,cyclic,gamma);
scale=esize./max(esize);
if ~evalonly
    Img=scaleimage3D(Img,floor(size(Img).*scale),true);
else
    Img=1;
end;
esize=[max(esize),max(esize),max(esize)];


clear neuro_data B
count =1;

for vesselid=0:3
vcount=0;
fid = fopen([foldername,'/vessel',num2str(vesselid),'/reference.txt']);
s = textscan(fid,'%s','Delimiter','\n');
s = s{1};
%s{1}=[s{1},'<root>'];
%s{end+1}='</root>';
fclose(fid);
Pos_org=[];
for a=1:numel(s)
    if numel(s{a})>0
        %if s{a}(1)~='#'
        vec=str2num(s{a});
        Pos_org=[Pos_org;vec(1:3)];
        if (mod(vcount,skip_pts)==0)||(vcount==numel(s)-1)
             %vec=str2num(s{a});
             %display(vec);
             ID=count-1;
             if (vcount>0)
                edge=[ID-1,ID];
             else
                    edge=[-1,-1];
             end
             pos=vec(1:3);
             pmindx=[1,2,3];
             pos=pos(pmindx);
             scale=vec(4);
             neuro_data(count).ID=ID;
             neuro_data(count).edge=edge;
             neuro_data(count).pos=pos./esize(pmindx);
             neuro_data(count).scale=scale/min(esize);
             neuro_data(count).pid=vesselid+1+1;%otherwise "single table flag"
             count=count+1;
        end;
             vcount=vcount+1;        
    end;
end;


esize_correct=esize(pmindx);

fid = fopen([foldername,'/vessel',num2str(vesselid),'/pointA.txt']);
tline = fgetl(fid);
tmp_v=str2num(tline);
spointsA=[spointsA;tmp_v(pmindx)./esize(pmindx)];
fclose(fid);

fid = fopen([foldername,'/vessel',num2str(vesselid),'/pointB.txt']);
tline = fgetl(fid);
tmp_v=str2num(tline);
spointsB=[spointsB;tmp_v(pmindx)./esize(pmindx)];
fclose(fid);

Pos_orgs{vesselid+1}=Pos_org;
end;



clear B
B.data=zeros(22,numel(neuro_data));

edges=0;
for a=1:numel(neuro_data)
    ID=neuro_data(a).ID+1;
    pos=neuro_data(a).pos;
    %pos=pos([2,1,3]);
    B.data(1:3,ID)=pos;
    
    B.data(4:6,ID)=[0,0,1];
    B.data(7,ID)=0;
    B.data(8,ID)=neuro_data(a).scale;
    
    B.data(21,ID)=neuro_data(a).pid;
    
    B.data(9:11,ID)=pos;
    B.data(12:14,ID)=pos;
    B.data(22,ID)=1; % connection flag
    edges=edges+min(neuro_data(a).edge>0);
end;


B.connections=zeros(4,edges);

count=1;
for a=1:numel(neuro_data)
        if min(neuro_data(a).edge>0)
        B.connections(1,count)=neuro_data(a).edge(1);
        B.connections(2,count)=neuro_data(a).edge(2);
        
        pA=B.connections(1,count)+1;
        pB=B.connections(2,count)+1;
        vdir=B.data(1:3,pA)-B.data(1:3,pB);
        vdir=vdir./norm(vdir);
        B.data(4:6,pA)=vdir;
        B.data(4:6,pB)=vdir;
        
        
        B.data(19:20,B.connections(2,count)+1)=1;
        
        count=count+1;
        end;
end;

for a=1:numel(neuro_data)
    
        B.data(9:11,a)=B.data(1:3,a)+B.data(4:6,a);
        B.data(12:14,a)=B.data(1:3,a)-B.data(4:6,a);
end;


 [paths,B]=extract_paths(B,'spacing',3);



