
%for d=[1:10],data=mhs_simualte_axions('shape',[512,512,512],'mode','chaos','density',d,'thick_noise_fact',0.25);;save(['data_chaos_',num2str(d),'.mat'],'data');end;

%bundles
%for a=[5,10,50,100,200],data=mhs_simualte_axions('shape',[128,128,256],'mode','bundle','density',a,'dir_noise_bias',0.02);save(['data_bundle_',num2str(a),'.mat'],'data');end;


%for a=[5,10,50,100,200],data=mhs_simualte_axions('shape',[128,128,256],'mode','bundle','density',a,'dir_noise_bias',0.02);save(['data_bundle_',num2str(a),'.mat'],'data');end;
%for a=[5,10,50,100,200],load(['data_bundle_',num2str(a),'.mat'],'data');img_s=data.ImgD2.^0.75;FeatureData=init_mhs_SD_SHorg(single(img_s),'element_size',[1,1,1],'scale_range',[1.15,2],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',50,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1,'adaptive_normalization',false);options=SD_testsettings_new_SIMax('the_merge_make',false,'the_bif_make',false,'FeatureData',FeatureData);A=ntrack(FeatureData,{'opt_numiterations',1000000000,options{:},'movie_folder','./movie/','pausing',false});save(['data_bundle_',num2str(a),'_results.mat'],'A');end;
%for a=[5,10,50,100,200],load(['data_bundle_',num2str(a),'.mat']);img=data.ImgD2;img=mhs_smooth_img_aniso(mhs_smooth_img(img,1.0,'normalize',true),1.5,0.1,200,0.1);save(['data_bundle_',num2str(a),'_iso_plus_aniso_smoothed.mat'],'img');end;
%for a=[5,10,50,100,200], load(['data_bundle_',num2str(a),'.mat']);mywritetif(['./tif/img',num2str(a),'_img.tif'],data.ImgD2);  end;
%for a=[5,10,50,100,200], load(['data_bundle_',num2str(a),'_iso_plus_aniso_smoothed.mat']);mywritetif(['./tif/img',num2str(a),'_img.tif'],img);  end;


%for a=[10,20,40,80,160,320],data=mhs_simualte_axions('shape',[128,128,256],'mode','bundle','density',a,'dir_noise_bias',0.005);save(['data_bundle_',num2str(a),'.mat'],'data');end;
%for a=[10,20,40,80,160,320],load(['data_bundle_',num2str(a),'.mat'],'data');img_s=data.ImgD2.^0.75;FeatureData=init_mhs_SD_SHorg(single(img_s),'element_size',[1,1,1],'scale_range',[1.15,2],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',50,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1,'adaptive_normalization',false);options=SD_testsettings_new_SIMax('the_merge_make',false,'the_bif_make',false,'FeatureData',FeatureData);A=ntrack(FeatureData,{'opt_numiterations',1000000000,options{:},'movie_folder','./movie/','pausing',false});save(['data_bundle_',num2str(a),'_results.mat'],'A');end;
%for a=[10,20,40,80,160,320],load(['data_bundle_',num2str(a),'.mat']);img=data.ImgD2;img=mhs_smooth_img_aniso(mhs_smooth_img(img,1.0,'normalize',true),1.5,0.1,200,0.1);save(['data_bundle_',num2str(a),'_iso_plus_aniso_smoothed.mat'],'img');end;
%for a=[10,20,40,80,160,320], load(['data_bundle_',num2str(a),'.mat']);mywritetif(['./tif_raw/img',num2str(a),'_img.tif'],data.ImgD2);  end;
%for a=[10,20,40,80,160,320], load(['data_bundle_',num2str(a),'_iso_plus_aniso_smoothed.mat']);mywritetif(['./tif/img',num2str(a),'_img.tif'],img);  end;

%data=mhs_simualte_axions('shape',[128,128,256],'mode','simple','density',20,'bundle_rad',0.8);
%img=data.ImgD2;img=mhs_smooth_img_aniso(mhs_smooth_img(img,1.0,'normalize',true),1.5,0.1,200,0.1);

%NEUNEUNEU:

%data=mhs_simualte_axions('shape',[256,256,512],'mode','bundle','density',10,'dir_noise_bias',0.01,'bundle_rad',0.75);
    %for d=[1:10],data=mhs_simualte_axions('shape',[256,256,512],'mode','bundle','density',d,'dir_noise_bias',0.01,'bundle_rad',0.75);;save(['data_chaos_',num2str(d),'.mat'],'data');end;
    %for d=[1:10],data=mhs_simualte_axions('shape',[256,256,512],'mode','bundle','density',4+6*(d-1)/9,'dir_noise_bias',0.01,'bundle_rad',0.75);;save(['data_bundle_',num2str(d),'.mat'],'data');end;

%sim7_bundle    
%for d=[1:10],data=mhs_simualte_axions('shape',[256,256,512],'mode','bundle','density',5+15*(d-1)/9,'dir_noise_bias',0.01,'bundle_rad',0.75);;save(['data_bundle_',num2str(d),'.mat'],'data');end;
%sim7_bundle_v2    
%for d=[1:10],data=mhs_simualte_axions('shape',[256,256,512],'mode','bundle','density',5+15*(d-1)/9,'dir_noise_bias',0.005,'bundle_rad',0.75);;save(['data_bundle_',num2str(d),'.mat'],'data');end;

%sim7
%for d=[1:10],data=mhs_simualte_axions('shape',[512,512,512],'mode','chaos','density',d/2);;save(['data_chaos_',num2str(d),'.mat'],'data');end;
%sim7_v2
%for d=[1:10],data=mhs_simualte_axions('shape',[512,512,512],'mode','chaos','density',d/2,'max_b',1);save(['data_chaos_',num2str(d),'.mat'],'data');end;


%ommands={};for d=1:10, commands{d}=['p=pwd;cd ~/projects/neuro_tracker;mhs_setPaths;cd(p);load([''data_chaos_',num2str(d),'.mat'']);img=mhs_smooth_img_aniso(mhs_smooth_img(data.ImgD,1.0,''normalize'',true),1.5,0.1,200,0.1);save([''data_chaos_aniso',num2str(d),'.mat''],''img'');;'];end;mhs_create_bash_commands(commands,'nthreads',8,'ismatlab',true,'sta_threads',4);

%sim7_v4
%for d=[1:10],data=mhs_simualte_axions('shape',[512,512,512],'mode','chaos','density',d,'max_b',1,'axon_thickness',[2,3],'thick_min',1.5,'flatty_axions',true);save(['data_chaos_',num2str(d),'.mat'],'data');end;


%for sim8 nobif
%for d=[1:10],data=mhs_simualte_axions('shape',[512,512,512],'mode','chaos','density',d,'max_b',0,'axon_thickness',[2,3],'thick_min',1.5,'flatty_axions',true,'psf_z',8);save(['data_chaos_psf8_',num2str(d),'.mat'],'data');end;




function [GT,Img,ImgD,ImgD2,ImgW]=mhs_simualte_axions(varargin)

if false
    %%
    prepath='~/data_ext';
    %prepath='~/data';
    %%
    for d=1:50
        [GT,Img,ImgD,ImgD2,ImgW]=mhs_simualte_axions('shape',[256,256,64],'density',d);
        data.GT=GT;
        data.Img=single(Img);
        data.ImgD=single(ImgD);
        data.ImgD2=single(ImgD2);
        data.ImgW=single(ImgW);
        save([prepath,'/nopro_paper/sim4/data',num2str(d),'.mat'],'data');
    end;
    %%
   % for d=1:10
   for d=1:50
        load([prepath,'/nopro_paper/sim4/data',num2str(d),'.mat'],'data');
        %img_s=data.ImgD2.^0.75;
        img_s=data.ImgD.^0.75;
        clear FeatureData
        %FeatureData=init_mhs_SD_SHorg(single(img_s),'element_size',[1,1,1],'scale_range',[1,2],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.05,0.05,0.05,0.05],'maxit',20,'L',4,'Ldata',4,'debug',true,'steerable_conv_fact',1,'adaptive_normalization',false);
        FeatureData=init_mhs_SD_SHorg(single(img_s),'element_size',[1,1,1],'scale_range',[1.15,2.0],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',50,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1);
        SD_testsettings_new_SIMax
        A=ntrack(FeatureData,{'opt_numiterations',1000000000,options{:},'movie_folder','./movie/','pausing',false});
        save([prepath,'/nopro_paper/sim4/data',num2str(d),'_results.mat'],'A');
    end;
    %%
    for d=11:50
        load([prepath,'/nopro_paper/sim4/data',num2str(d),'.mat'],'data');
        load([prepath,'/nopro_paper/sim4/data',num2str(d),'_results.mat'],'A');
        scoreA=mhs_eval_graph(data.GT,A,1);
        save([prepath,'/nopro_paper/sim4/data',num2str(d),'_score.mat'],'scoreA');
    end;
end;


rng('shuffle') 
shape=[512,512,128];
max_d=500;
noise_offset=0.1;
min_particles=30;

create_images=true;

%axon_thickness=[1,2.5];
axon_thickness=[2,3];
%thick_min=0.5;   
thick_min=1;   
thick_noise_fact=0.25;
posnoise=1;
var_intensity=false;

psf_z=-1;

pemute_snratio=true;

density=5;

two_dirs=true;

bif_prob=0.1;
 min_b_dist=10;
max_b=2;
 
bundle_rad=0.5;
mode='chaos';
oriented_rad=[0.5,2];
flatty_axions=false;

background_noise=0.1;
max_photon_count_exp=4;

 fprop=[0.5,1,2];

%dir_noise_bias=[1,1,0.01];
dir_noise_bias=[1,1,1];

directions=[1,1,1];

for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


if ~exist('GT','var')


 
    
switch mode
    case 'chaos'
        
        min_particles=min_particles.*shape/max(shape);
        count=1;

        if directions(1)   
            for x=density:density:shape(1)-density
                
                  thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);

                 if (rand>0.5) || (~two_dirs)
                    ndir=[0,1,0];
                    start_pos=[x,0,rand*shape(3)];
                else
                    ndir=[0,-1,0];
                    start_pos=[x,shape(2),rand*shape(3)];
                end;


                %start_pos=[200,0,shape(3)/2];

                
                if flatty_axions
                    dir_noise_bias_=dir_noise_bias;
                    dir_noise_bias_(randi(3))=0.01;
                    ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias_;
                else
                    ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias;
                end;
                
                
                
                A=mhs_treegen('shape',shape,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'fprop',fprop,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                %max(A.data(1:3,:),[],2)
                %A.data(1:3,1)

                if size(A.data,2)>min_particles(1)
                    if (count==1)
                         A.data(21,:)=1;
                         C=A;
                    else
                         C=append(C,A);
                    end;
                    count=count+1;
                end;
            end;
        end;

        if directions(2)   
            for y=density:density:shape(2)-density

                thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);
                if (rand>0.5) || (~two_dirs)
                    ndir=[1,0,0];
                    start_pos=[0,y,rand*shape(3)];
                else
                    ndir=[-1,0,0];
                    start_pos=[shape(1),y,rand*shape(3)];
                end;


                %start_pos=[200,0,shape(3)/2];

                %ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias;
                if flatty_axions
                    dir_noise_bias_=dir_noise_bias;
                    dir_noise_bias_(randi(3))=0.01;
                    ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias_;
                else
                    ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias;
                end;

                A=mhs_treegen('shape',shape,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                %max(A.data(1:3,:),[],2)
                %A.data(1:3,1)

                if size(A.data,2)>min_particles(2)
                    if (count==1)
                         A.data(21,:)=1;
                         C=A;
                    else
                         C=append(C,A);
                    end;
                    count=count+1;
                end;
            end;
        end;


        if directions(3)   
            %for z=density:density:shape(3)-density
            for x=density:density:shape(1)-density

                thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);
                if (rand>0.5) || (~two_dirs)
                    ndir=[0,0,1];
                    start_pos=[x,rand*shape(2),0];
                else
                    ndir=[0,0,-1];
                    start_pos=[x,rand*shape(2),shape(3)];
                end;


                %start_pos=[200,0,shape(3)/2];

               % ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias;
                if flatty_axions
                    dir_noise_bias_=dir_noise_bias;
                    dir_noise_bias_(randi(3))=0.01;
                    ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias_;
                else
                    ndir_noise=0.01+(1-ndir).*0.1.*dir_noise_bias;
                end;
                
                
                %ndir_noise=0.01+(1-ndir).*0.1.*[1,1,1];

                A=mhs_treegen('shape',shape,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                %max(A.data(1:3,:),[],2)
                %A.data(1:3,1)

                if size(A.data,2)>min_particles(3)
                    if (count==1)
                         A.data(21,:)=1;
                         C=A;
                    else
                         C=append(C,A);
                    end;
                    count=count+1;
                end;
            end;
        end;
        
         case 'bundle'
        
        min_particles=min_particles.*shape/max(shape)/5;
        count=1;
        
        [X Y ] = ndgrid(0:(shape(1)-1),0:(shape(2)-1));
        mask=(mod(X,round(density))==0) & (mod(Y,round(density))==0);
        
        X2 = 2*(X - ceil(shape(1)/2))/shape(1);
        Y2 = 2*(Y - ceil(shape(2)/2))/shape(2);
        
        R2=X2.^2+Y2.^2;
        mask=(R2<bundle_rad^2) & mask;%(rand(size(X))<(1/density));
        
        totalcand=sum((R2(:)<bundle_rad^2));
        
        
        
        
        start_poss=[X(mask),Y(mask)];
        start_poss=start_poss+randn(size(start_poss))*density*0.25;
        
        fprintf('%d startpos candidates (of %d, %d percent)\n',size(start_poss,1),totalcand,ceil(100*size(start_poss,1)/totalcand));
        
        for s=1:size(start_poss,1)
            
             thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);
             
            ndir=[0,0,1];
            %start_pos=([start_poss(s,:),0]/2+0.5).*[shape(1:2),0];
            start_pos=([start_poss(s,:),0]);
            
            
             ndir=ndir+dir_noise_bias.*randn(size(ndir));
             ndir=ndir./norm(ndir);
             
             ndir_noise=dir_noise_bias;%0.01+(1-ndir).*0.1.*dir_noise_bias;
             %ndir_noise=[0.001,0.001,1];
             max_b=0;
            
             A=mhs_treegen('shape',shape,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                %max(A.data(1:3,:),[],2)
                %A.data(1:3,1)

            if size(A.data,2)>min_particles(1)
                if (count==1)
                     A.data(21,:)=1;
                     C=A;
                else
                     C=append(C,A);
                end;
                count=count+1;
            end;
        end;
        
        
        case 'oriented'
        
        assert(exist('direction','var')==1);    
        direction=direction/norm(direction);
        
        assert(min(shape)==max(shape));
        
        shape_outer=shape;
        
        
        %shape_outer=sqrt((shape(1)/2)^2*2)*2;
        %shape_outer=ceil([shape_outer,shape_outer,shape_outer]);
        shape=sqrt(shape_outer(1)^2/2);
        shape=ceil([shape,shape,shape]);
        
        center_box=shape_outer/2;
        
        %center=shape/2;
        center=shape/2;
        
        %rad=center(1)/2-10;
        rad=center(1)-10;
        
        
        [X,Y]=ndgrid([1:density:ceil(oriented_rad(1)*rad)],[1:density:ceil(oriented_rad(2)*rad)]);
        X=X-rad*oriented_rad(1)/2;
        Y=Y-rad*oriented_rad(2)/2;
        
        
        [n1,n2]=createOrths(direction);
         count=1;
          shape=shape_outer;
        
        for ind=1:numel(X(:))
            thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);
             
            ndir=direction;
            
            pnoise=posnoise*max(randn(1,2)*(density-axon_thickness(2)),0);
            
            %start_pos=n1*(X(ind)+pnoise(1))+n2*(Y(ind)+pnoise(2))-direction*rad+center_box;
            
            
            %ndir=ndir+dir_noise_bias.*randn(size(ndir));
            ndir=ndir./norm(ndir);
            ndir2=ndir;
            
            if exist('disper','var')
                ndir=ndir+randn*[disper,0,0];
                ndir=ndir./norm(ndir);
            end;
            
            w=rand();
            ndir2=ndir*w+(1-w)*ndir2;
            ndir2=ndir2/norm(ndir2);
            
            
            %start_pos=n1*(X(ind)+pnoise(1))+n2*(Y(ind)+pnoise(2))-ndir2*rad+center_box;
            
            start_pos=n1*(X(ind)+pnoise(1))+n2*(Y(ind)+pnoise(2))-ndir2*rad;
            if rand>0.5
                start_pos=center_box+start_pos;
                
            else
                start_pos=center_box-start_pos;
                ndir=-ndir;
            end
             
            %ndir_noise=dir_noise_bias;%0.01+(1-ndir).*0.1.*dir_noise_bias;
            %ndir_noise=[0.001,0.001,1];
            ndir_noise=[0,0,0];
            %if exist('disper','var')
            %    ndir_noise=randn*[disper,0,0];
            %end;
            max_b=0;
            
             A=mhs_treegen('max_depth',2*rad,'shape',shape_outer,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                %max(A.data(1:3,:),[],2)
                %A.data(1:3,1)

            if size(A.data,2)>1
                if (count==1)
                     A.data(21,:)=1;
                     C=A;
                else
                     C=append(C,A);
                end;
                count=count+1;
            end;
            
        end;
        
      
            
        
       %for d=1:3 
       % C.data(d,:)= C.data(d,:)-(shape_outer(1)-shape(1))/2;
      %end
      % C.cshape=shape;
        
         case 'simple'
             pemute_snratio=false;
             
       min_particles=min_particles.*shape/max(shape)/5;
        count=1;
        
        [X Y ] = ndgrid(0:(shape(1)-1),0:(shape(2)-1));
        X = 2*(X - ceil(shape(1)/2))/shape(1);
        Y = 2*(Y - ceil(shape(2)/2))/shape(2);
        
        num_samples=density;
        S=[0:num_samples-1];
        S=exp(-2*pi*1i*S/num_samples)*bundle_rad*min(shape(1:2))/2;
        Sx=round(real(S)+shape(1)/2);
        Sy=round(imag(S)+shape(2)/2);
        
        
        mask=zeros(shape(1:2))>0;
        
        indx=sub2ind(shape(1:2),Sx,Sy);
        mask(indx)=true;
        
        
        
        
        start_poss=[X(mask),Y(mask)];
        
        %fprintf('%d startpos candidates (of %d, %d percent)\n',size(start_poss,1),totalcand,ceil(100*size(start_poss,1)/totalcand));
        
        for s=1:size(start_poss,1)
            ndir=[0,0,1];
            start_pos=([start_poss(s,:),0]/2+0.5).*[shape(1:2),0];
            thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);
            
             ndir_noise=[0,0,0];
             max_b=0;
            
             A=mhs_treegen('shape',shape,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                %max(A.data(1:3,:),[],2)
                %A.data(1:3,1)

            if size(A.data,2)>min_particles(1)
                if (count==1)
                     A.data(21,:)=1;
                     C=A;
                else
                     C=append(C,A);
                end;
                count=count+1;
            end;
        end;        
             
        case 'simplebar'
            count=1;
         pemute_snratio=false;
             
            for y=density:density:shape(2)-density
                thick_start=max(rand*(axon_thickness(2)-axon_thickness(1)))+axon_thickness(1);
                ndir=[1,0,0];
                start_pos=[0,y,shape(3)/2];


                ndir_noise=[0,0,0];
                max_b=0;

                A=mhs_treegen('shape',shape,'start_pos',start_pos,'ndir',ndir,'max_d',max_d,'ndir_noise',ndir_noise,'bif_prob',bif_prob,'min_b_dist',min_b_dist,'max_b',max_b,'thick_start',thick_start,'thick_min',thick_min,'thick_noise_fact',thick_noise_fact);
                
                if (count==1)
                     A.data(21,:)=1;
                     C=A;
                else
                     C=append(C,A);
                end;
                count=count+1;
            end;
            
    case 'example'   
        shape=[64,64,32];
        %C=mhs_treegen('shape',shape,'start_pos',[0,32,16],'ndir',[1,0,0],'max_d',50,'ndir_noise',[0.01,0.2,0],'bif_prob',1,'min_b_dist',1,'max_b',2,'thick_start',2,'thick_min',1.5,'thick_noise_fact',0.25);
        C=mhs_treegen('shape',shape,'start_pos',[0,20,16],'ndir',[1,0,0],'max_d',60,'ndir_noise',[0.001,0.1,0],'bif_prob',0,'min_b_dist',1,'max_b',2,'thick_start',2,'thick_min',1.5,'thick_noise_fact',0.25,'step',3);
        
             
end;        

        GT=C;
end;        

if create_images

    
    if psf_z>0
        %Img=smoothZ(Img,psf_z);
        D=mhs_distmap(GT,shape,'maxdist',5,'vscale',[1,1,psf_z]);
    else
        D=mhs_distmap(GT,shape,'maxdist',5);
    end;
    Img=single(exp(-D.^4));
    %Img=single(exp(-D.^2));
    %Img=single(D<1.1);

    %if psf_z>0
    %    Img=smoothZ(Img,psf_z);
    %end;

    ImgD=single(mhs_poisson_noise(max(Img+background_noise+background_noise*randn(size(Img)),0.0),-1,max_photon_count_exp));
    %ImgD=single(mhs_poisson_noise(max(Img+background_noise+background_noise*randn(size(Img)),0.0),12,2^2));
    %ImgD=single(mhs_poisson_noise(Img,12,2^2));

    path_ids=unique(GT.data(21,:));

    if pemute_snratio
    rindx=randperm(numel(path_ids));
    path_ids=path_ids(rindx);
    else
        rindx=[1:numel(path_ids)];
    end;
end;

if var_intensity
    
    ImgD2=zeros(size(Img),'single');


    ttime=0;
    count=1;
    tic
    for p=path_ids

        D=mhs_distmap(GT,shape,5,true,p);
        Img_=exp(-D.^2);
        if psf_z>0
            Img_=smoothZ(Img_,psf_z);
        end;

        w=count/numel(path_ids);
        ImgD2=max(((1-noise_offset)*w+noise_offset)*Img_,ImgD2);
        ttime=ttime+toc;
        fprintf('%d %d (%2.2f min)\n',count,numel(path_ids),(numel(path_ids)-count)*(ttime/(60*count)));
       count=count+1;

    end;
    toc

    ImgW=single(ImgD2);
    %ImgD2=single(mhs_poisson_noise(max(ImgD2+0.1+0.1*randn(size(Img)),0.0),12,2^2));
    ImgD2=single(mhs_poisson_noise(max(ImgD2+background_noise+background_noise*randn(size(Img)),0.0),-1,max_photon_count_exp));
end;

if nargout==1
    data.GT=GT;
    if create_images
        data.Img=Img;
        data.ImgD=ImgD;
        if var_intensity
            data.ImgD2=ImgD2;
            data.ImgW=ImgW;
        end;
        data.rindx=rindx;
        data.params=varargin;
        GT=data;
    end;
    
end;

function C=append(C,B)

pid=max(C.data(21,:));


offset=size(C.data,2);

tmp=B.data;
tmp(21,:)=pid+1;
C.data=cat(2,C.data,tmp);
tmp=B.connections;
tmp(1:2,:)=tmp(1:2,:)+offset;
C.connections=cat(2,C.connections,tmp);

%if nargout==1
%minp=min(C.data(1:3,:),[],2);
%C.data(1:3,:)=C.data(1:3,:)-repmat(minp,1,size(C.data,2))+0.0001;
%end;


function Img=smoothZ(Img,sigma,n)
        if nargin<3
            n=true;
        end;

        shape=size(Img);
        Img=padarray(Img,ceil(3*sigma),'symmetric','post');

        imgsz=size(Img);
        [X Y Z] = ndgrid(0:(imgsz(1)-1),0:(imgsz(2)-1),0:(imgsz(3)-1));
        X = X - ceil(imgsz(1)/2);
        Y = Y - ceil(imgsz(2)/2);
        Z = Z - ceil(imgsz(3)/2);
        R2 = Z.^2/(2*sigma^2);
    
        gaussin = (fftshift(exp(-R2) .* ( X==0) .* (Y==0)));
        if n
        gaussin =gaussin ./sum(gaussin (:));
        end
        gaussin_ft= fftn(gaussin);
        Img=ifftn(fftn(Img).*gaussin_ft,'symmetric');    
        
       Img=Img(1:shape(1),1:shape(2),1:shape(3));
       Img=Img./max(Img(:));


function [vn1,vn2]=createOrths(v)
        if ((v(1))>abs(v(2)))
            if (abs(v(2))>abs(v(3)))
              vn1=cross(v,[0,0,1]);
            else
              vn1=cross(v,[0,1,0]);
            end;
        else
            if (abs(v(1))>abs(v(3)))
              vn1=cross(v,[0,0,1]);
            else
              vn1=cross(v,[1,0,0]);
            end;      
        end;
      
      vn1=vn1/norm(vn1);
      vn2=cross(vn1,v);
    
