#include <math.h>
#include "mex.h"
#include "matrix.h"
#define REAL float


#include "MersenneTwister.h"
MTRand mtrand;


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"


    class Bifu
    {
      public: 
	Vector<REAL,3> pos;
	Vector<REAL,3> dir;
	REAL scale;
    };
    
    class Edge
    {
      public:
      int a;
      int b;
    };


class TreeProd
{
public:
    REAL *accu;
    REAL dens;
    REAL dt;
    REAL nz;
    REAL mean_len;
    REAL dev_len;
    REAL bra;
    REAL bra_max;
    REAL mind;
    REAL tnoise;
    REAL shortfact;
    int crop;
    int mode;
    bool exact_angle;
    int wid,hei,dep;
    int maxdepth;

    std::list<Bifu> bifurcations;
    std::list<Bifu> pts;
    std::list<Edge> cons;
    
     TreeProd(REAL *a, int w, int h, int d,REAL dens, REAL dt, REAL nz, REAL mean_len, REAL dev_len, REAL bra, REAL bra_max, REAL mind, REAL tnoise, int crop,int mode,bool exact_angle,REAL shortfact,int maxdepth)
    {
        accu = a;
        this->dens = dens;              // density of points 
        this->dt = dt;                  // time step 
        this->nz = nz;                  // velocity distortion
        this->mean_len = mean_len;      // mean length of branch
        this->dev_len = dev_len;        
        this->bra =  bra;               // branching angle
        this->bra_max = bra_max;
        this->mind = mind;              // minimum thickness
        this->wid = w;
        this->hei = h;
        this->dep = d;
	this->tnoise =tnoise;
	this->crop =crop;
	this->mode =mode;
	this->exact_angle=exact_angle;
	this->shortfact=shortfact;
	this->maxdepth=maxdepth;
	switch (mode)
	{
	  case 0:
	    printf("org\n");
	    break;
	    case 1:
	      printf("new\n");
	      break;
	      case 2:
		printf("new gaussian\n");
		break;
	  
	  
	}
     }
    
    void createOrths(REAL *v,REAL *vn1, REAL *vn2)
    {
	    if (std::abs(v[2])+std::numeric_limits<REAL>::epsilon()>1)
            {
                REAL vnorm = sqrt(2);
                vn1[2] = 0;
                vn1[0] = -1/vnorm;
                vn1[1] = 1/vnorm;
            }
            else
            if (v[1] == 0)
            {
                REAL vnorm = sqrt(v[1]*v[1]+v[2]*v[2]);
                vn1[0] = 0;
                vn1[1] = -v[1]/vnorm;
                vn1[2] = v[2]/vnorm;
            }
            else
            {
                REAL vnorm = sqrt(v[0]*v[0]+v[2]*v[2]);
                vn1[0] = v[2];
                vn1[1] = 0;
                vn1[2] = -v[0]/vnorm;                
            }
                
            vn2[0] = vn1[1]*v[2] - vn1[2]*v[1];
            vn2[1] = vn1[2]*v[0] - vn1[0]*v[2];
            vn2[2] = vn1[0]*v[1] - vn1[1]*v[0];
            REAL vnorm2 = sqrt(vn2[0]*vn2[0]+vn2[1]*vn2[1]+vn2[2]*vn2[2]);
            vn2[0] /= vnorm2;
            vn2[1] /= vnorm2;
            vn2[2] /= vnorm2;
    }
    
    
    void uniCircRand(REAL *r)
    {
        for(;;)
        {
            r[0] = 2*mtrand.frand()-1;
            r[1] = 2*mtrand.frand()-1;
            if (r[0]*r[0] + r[1]*r[1] <= 1)
                return;
        }
                
    }
    
    int renderPlane(REAL *x, REAL *v, REAL d)
    {
        int NumPts = int(d*dens)+1;
        REAL vn1[3];
        REAL vn2[3];
        REAL r[2];
        for (int k = 0; k< NumPts; k++)
        {
          
            createOrths(v,vn1,vn2);
            
            uniCircRand(r);
            
            REAL ptsx = x[0] + (vn1[0]*r[0] + vn2[0]*r[1])*d/2;
            REAL ptsy = x[1] + (vn1[1]*r[0] + vn2[1]*r[1])*d/2;
            REAL ptsz = x[2] + (vn1[2]*r[0] + vn2[2]*r[1])*d/2;
        
            int px = (int) (ptsx);
            if (px < 0 || px >= wid-1)
                continue;
            int py = (int) (ptsy);
            if (py < 0 || py >= hei-1)
                continue;
            int pz = (int) (ptsz);
            if (pz < 0 || pz >= dep-1)
                continue;
            float frac_x = ptsx - px;
            float frac_y = ptsy - py;
            float frac_z = ptsz - pz;

            REAL maxdens = dens*0.8;
            if ( accu[px + wid*(py+hei*pz)] < maxdens)
                accu[px + wid*(py+hei*pz)] += (1-frac_x)*(1-frac_y)*(1-frac_z);
            if ( accu[px+1 + wid*(py+hei*pz)]  < maxdens)
                accu[px+1 + wid*(py+hei*pz)] += (frac_x)*(1-frac_y)*(1-frac_z);            
            if (  accu[px + wid*(py+1+hei*pz)] < maxdens)
                accu[px + wid*(py+1+hei*pz)] += (1-frac_x)*(frac_y)*(1-frac_z);
            if ( accu[px + wid*(py+hei*pz+hei)] < maxdens)
                accu[px + wid*(py+hei*pz+hei)] += (1-frac_x)*(1-frac_y)*(frac_z);
            if ( accu[px + wid*(py+1+hei*pz+hei)]< maxdens)
                accu[px + wid*(py+1+hei*pz+hei)] += (1-frac_x)*(frac_y)*(frac_z);
            if ( accu[px+1 + wid*(py+hei*pz+hei)]< maxdens)
                accu[px+1 + wid*(py+hei*pz+hei)] += (frac_x)*(1-frac_y)*(frac_z);
            if ( accu[px+1 + wid*(py+1+hei*pz)]< maxdens)
                accu[px+1 + wid*(py+1+hei*pz)] += (frac_x)*(frac_y)*(1-frac_z);
            if (accu[px+1 + wid*(py+1+hei*pz+hei)]< maxdens)
                accu[px+1 + wid*(py+1+hei*pz+hei)] += (frac_x)*(frac_y)*(frac_z);

        }
        return 0;
    }


 int renderPlane2(REAL *x_, REAL *v, REAL d,bool gauss=false)
    {

       d/=2;
//        d=std::max(d,REAL(1));
      
	
//         REAL vn1[3];
//         REAL vn2[3];
        REAL r[2];
 	Vector<REAL,3> vn1;
        Vector<REAL,3> vn2;
	createOrths(v,vn1.v,vn2.v);

	Vector<int,3> bbl;
	Vector<int,3> bbu;

	Vector<int,3> shape;		
	shape[0]=wid;
	shape[1]=dep;
	shape[2]=hei;
	
	Vector<REAL,3> X(x_[0],x_[1],x_[2]);
	Vector<REAL,3> V(v[2],v[1],v[0]);
	//V.normalize();
	
//         createOrths(v,vn1.v,vn2.v);
	
	int boffset=2;
	if (gauss)
	  boffset=2*d+1;
	for (int e=0;e<3;e++)
	{
	  REAL low=X[e]-d-boffset;
	  REAL up=X[e]+d+boffset;
	  bbl[e]=std::max(std::floor(low),REAL(0));
	  bbu[e]=std::min(std::ceil(up),REAL(shape[e]));
	}
	
	for (int x=bbl[0];x<bbu[0];x++)
	{
	for (int y=bbl[1];y<bbu[1];y++)
	{
	  for (int z=bbl[2];z<bbu[2];z++)
	  {
	    Vector<REAL,3> pos(x,y,z);
	    
	    Vector<REAL,3> localx=pos-X;
	    
	    REAL pdist=V.dot(localx);
	    
	    if (std::abs(pdist)>0.5)
	      continue;
	    
	    //Vector<REAL,3> localr=localx-V*pdist;
	    //REAL rad2=localr.norm2();
	    
	    REAL rad2=(localx-V*pdist).norm2();
	    
	    
	    
	    
	    REAL v=1;
	    if (gauss)
	    {
	      if (!(rad2<4*d*d))
		continue;
	      v=std::exp(-rad2/(d*d));
	    }
	    else
	    {
// 		if (!(rad2<d*d))
// 		  continue;
	      if (!(rad2<(d+2)*(d+2)))
		  continue;
	      
	      REAL dists=std::sqrt(rad2);
	      REAL dist=dists-d;
	      if (dist<0)
		v=1;
	      else
		v=1-std::min(dist/REAL(1.25),REAL(1));
	    }
	    
	    //int index=(z*shape[1]+y)*shape[2]+x;
	    int index=(z*wid+y)*hei+x;
	    if ((index<shape[0]*shape[1]*shape[2])&&(index>-1))
	    {
	      accu[index]=std::max(accu[index],v);
	    }
	  }}}
        return 0;
    }
    
    
    int track(REAL *x, REAL *v, REAL d, REAL dold, int numPts,int ptdid)
    {
     
	REAL collision[3];
	REAL rad=std::min(REAL(wid),REAL(hei));
	      rad=std::min(REAL(rad),REAL(dep))/2;
	      rad*=rad;

	 
	      
        for (int k = 0; k < numPts;k++)
        {
	    Vector<REAL,3> Xold(x[0],x[1],x[2]);
	  
            x[0] += dt*v[0];
            x[1] += dt*v[1];
            x[2] += dt*v[2];
	    
	    
// 	    if (k>std::ceil((REAL)numPts/2))
// 	    {
// 	      Vector<int,3> shape;		
// 	      shape[0]=wid;
// 	      shape[1]=hei;
// 	      shape[2]=dep;	      
// 	      int index=(x[0]*shape[1]+x[1])*shape[2]+x[2];
// 	      if (accu[index]>0.01)
// 		return -1;
// 	    }
	    
	    collision[0]=x[0] +crop*v[0];
	    collision[1]=x[1] +crop*v[1];
	    collision[2]=x[2] +crop*v[2];
	    
	    REAL tmp=(wid-1)/2-collision[0];
	    REAL dist2=tmp*tmp;
	    tmp=(hei-1)/2-collision[1];
	    dist2+=tmp*tmp;
	    tmp=(dep-1)/2-collision[2];
	    dist2+=tmp*tmp;
	    
	    if (dist2>rad)
	        return -1;
	    
	    

	    
// 	    if (collision[0] < 0 || collision[0] >= wid-1)
//                 return -1;
//             if (collision[1] < 0 || collision[1] >= hei-1)
//                 return -1;
//             if (collision[2] < 0 || collision[2] >= dep-1)
//                 return -1;
	    
	    
//             if (x[0] < 0 || x[0] >= wid-1)
//                 return -1;
//             if (x[1] < 0 || x[1] >= hei-1)
//                 return -1;
//             if (x[2] < 0 || x[2] >= dep-1)
//                 return -1;

           
	      //NOTE "2D tree"
	      //v[0] += nz*mtrand.frandn();
	      v[1] += nz*mtrand.frandn();
	      v[2] += nz*mtrand.frandn();
	    
	    
	   
            
            REAL vnorm = sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
            v[0] /= vnorm;
            v[1] /= vnorm;
            v[2] /= vnorm;
            
            int px = int(x[0]);
            int py = int(x[1]);
            int pz = int(x[2]);
            
            
            REAL w=REAL(k)/(numPts-1);
	    w+=tnoise*mtrand.frandn();
	    w=std::min(w,REAL(1));
	    w=std::max(w,REAL(0));
	    
	    switch (mode)
	    {
	      case 0:
		renderPlane(x,v,w*d+(1-w)*dold);
		break;
		case 1:
		  renderPlane2(x,v,w*d+(1-w)*dold);
		  break;
		  case 2:
		    renderPlane2(x,v,w*d+(1-w)*dold,true);
		    break;
	    }
	
	
	  if (k%30==0)
	  {
	      Bifu bifu;
	      Vector<REAL,3> X(x[0],x[1],x[2]);
	      bifu.pos=X;
	      Vector<REAL,3> D(v[0],v[1],v[2]);
	      bifu.dir=D;
// 	      Vector<REAL,3> D=Xold-X;
// 	      D.normalize();
// 	       bifu.dir=D;
// 	       D.print();
	      bifu.scale=w*d+(1-w)*dold;
	      pts.push_back(bifu);
	      
	      
	      if (pts.size()>1)
	      {
		Edge edge;
		edge.a=ptdid;
		edge.b=pts.size()-1;
		cons.push_back(edge);
		ptdid=pts.size()-1;
	      }
	  }

        }
        return 0;
        
    }


    void createTree(REAL *x, REAL *v,REAL d,REAL oldd,int len,int ptdid=0,int depth=0)
    {

	if (len<=0)
	{
	      REAL fact=1;
	      
	      if (shortfact>0)
		fact=shortfact/(depth+shortfact);
	      
	      
	      len = int(fact*(mean_len + dev_len*(mtrand.frand()-0.5)));
	      
	      
	      
	      //len = int(mean_len + dev_len*(mtrand.frand()-0.5));
	  
	  
// 	  for (int a=1;a<100;a++)
// 	    printf("%f\n",0.25*mtrand.randNorm(0,1));
	}
	
// 	if (ptdid==-1)
// 	  ptdid=0;
	
	
        if (track(x, v, d,oldd, len,ptdid) == -1)
            return;
	
// 	printf("-> start: %d %d\n",depth,maxdepth);
	if ((depth>maxdepth)&&(maxdepth!=-1))
	{
	  return;
	}
	
        /// NOTE fix diameter
        /*REAL newd1 = d*(0.85-mtrand.frand()*0.2);     
        REAL newd2 = d*(0.85-mtrand.frand()*0.2);*/     
	REAL newd1 = d;//*(0.85-mtrand.frand()*0.2);     
        REAL newd2 = d;//*(0.85-mtrand.frand()*0.2);
        
        if (newd1 < mind || newd2 < mind)
            return;
        
        
        REAL vn1[3];
        REAL vn2[3];
        REAL v1[3];
        REAL v2[3];
        REAL delv[3];
        REAL x1[3];
        REAL x2[3];
	
        createOrths(v,vn1,vn2);  
// 	printf("1 %f %f %f\n",v[0],v[1],v[2]);	
// 	printf("2 %f %f %f\n",vn1[0],vn1[1],vn1[2]);
// 	printf("3 %f %f %f\n",vn2[0],vn2[1],vn2[2]);
	
        REAL r1 = mtrand.frandn();
        REAL r2 = mtrand.frandn();            
        
        delv[0] = (vn1[0]*r1 + vn2[0]*r2);
        delv[1] = (vn1[1]*r1 + vn2[1]*r2);
        delv[2] = (vn1[2]*r1 + vn2[2]*r2);
	
	
	if (exact_angle)
	{
	  REAL dnorm = sqrt(delv[0]*delv[0] + delv[1]*delv[1] + delv[2]*delv[2]);
	  delv[0] /= dnorm;
	  delv[1] /= dnorm;
	  delv[2] /= dnorm;	
	}
	
	REAL cbra=bra+(mtrand.frand()*(bra_max-bra));
	//printf("%f (%f %f) %f %f\n",cbra,bra,bra_max,(bra_max-bra),mtrand.frandn());
	REAL c=1/(std::cos(2*M_PI*cbra/360.0)+std::numeric_limits<REAL>::epsilon());
	cbra=std::sqrt(std::max(c*c-1,REAL(0)));
// 	printf("%f\n",cbra);
        
	//NOTE 2D
        v1[0] = v[0];// + delv[0]*cbra;
        v1[1] = v[1] + delv[1]*cbra;
        v1[2] = v[2] + delv[2]*cbra;
        REAL v1norm = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        v1[0] /= v1norm;
        v1[1] /= v1norm;
        v1[2] /= v1norm;
        
//         cbra=bra+mtrand.frandn()*(bra_max-bra);
// 	c=1/(std::cos(2*M_PI*cbra/360.0)+std::numeric_limits<REAL>::epsilon());
// 	cbra=c*c-1;
	
	///NOTE 2D
        v2[0] = v[0];// - delv[0]*cbra;
        v2[1] = v[1] - delv[1]*cbra;
        v2[2] = v[2] - delv[2]*cbra;
        REAL v2norm = sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]);
        v2[0] /= v2norm;
        v2[1] /= v2norm;
        v2[2] /= v2norm;
        
        x1[0] = x[0];
        x1[1] = x[1];
        x1[2] = x[2];
        
        x2[0] = x[0];
        x2[1] = x[1];
        x2[2] = x[2];
	        
	Bifu bifu;
	Vector<REAL,3> X(x[0],x[1],x[2]);
	Vector<REAL,3> X1(x1[0],x1[1],x1[2]);
	Vector<REAL,3> X2(x2[0],x2[1],x2[2]);
	bifu.pos=(X+(X1+v1)+(X2+v2))/REAL(3);
	bifu.scale=d;
	bifurcations.push_back(bifu);
	
	
	ptdid=pts.size()-1;
	
// 	printf("4 %f %f %f\n",v[0],v[1],v[2]);	
// 	printf("4 %f %f %f\n",v1[0],v1[1],v1[2]);
// 	printf("4 %f %f %f\n",v2[0],v2[1],v2[2]);
	 
// 	if (std::rand()%100>20)
	
// 	depth++;
// 	printf("-> split: %d %d\n",depth,maxdepth);
	createTree(x1, v1,newd1,d,-1,ptdid,++depth);
        createTree(x2, v2,newd2,d,-1,ptdid,++depth);
//           createTree(x1, v1,newd1,d,-1,ptdid,depth++);
//           createTree(x2, v2,newd2,d,-1,ptdid,depth++);
	
//           createTree(x1, v1,newd1,d,(-len),ptdid);
//           createTree(x2, v2,newd2,d,(-len),ptdid);
        
        
    }
    
    
    
};






void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
 	
    if(nrhs>1) {
	mexPrintf("wrong usage!!\n",nrhs);
    return;
	} else if(nlhs>4) {
	printf("Too many output arguments\n");
    return;
	}

    int pcnt = 0;
//     const mxArray *Dim;
//     Dim = prhs[pcnt++];       
//     REAL *dim = (REAL*) mxGetData(Dim);
//     int w = (int) dim[0];
//     int h = (int) dim[1];
//     int d = (int) dim[2];    
    REAL dens = 150;
    REAL dt = 0.05;
    REAL nz = 0.005;
    REAL mean_len = 350;
    REAL dev_len = 50;
    REAL bra = 0.5;
    REAL bra_max = 0.5;
    REAL startd = 20;
    REAL mind = 1;
    REAL tnoise = 0;
    int rootl=10;
    int crop=20;
    int mode=0;
    bool exact_angle=false;
    REAL shortfact=-1;
    int maxdepth=-1;
    
    
    std::vector<REAL> dim;
    dim.resize(3);
    dim[0]=256;
    dim[1]=256;
    dim[2]=256;    
    
    if (nrhs==1)
    {
        const mxArray * params=prhs[nrhs-1] ;

         if (mhs::mex_hasParam(params,"shape")!=-1)
             dim=mhs::mex_getParam<REAL>(params,"shape",3);
	 
         if (mhs::mex_hasParam(params,"shortfact")!=-1)
             shortfact=mhs::mex_getParam<REAL>(params,"shortfact",1)[0];
	 
	 
         if (mhs::mex_hasParam(params,"exact_angle")!=-1)
             exact_angle=mhs::mex_getParam<bool>(params,"exact_angle",1)[0];	 
	 
         if (mhs::mex_hasParam(params,"dens")!=-1)
             dens=mhs::mex_getParam<REAL>(params,"dens",1)[0];	 
	 if (mhs::mex_hasParam(params,"dt")!=-1)
             dt=mhs::mex_getParam<REAL>(params,"dt",1)[0];	 
	 if (mhs::mex_hasParam(params,"nz")!=-1)
             nz=mhs::mex_getParam<REAL>(params,"nz",1)[0];	
	 if (mhs::mex_hasParam(params,"mean_len")!=-1)
             mean_len=mhs::mex_getParam<REAL>(params,"mean_len",1)[0];	
	 if (mhs::mex_hasParam(params,"dev_len")!=-1)
             dev_len=mhs::mex_getParam<REAL>(params,"dev_len",1)[0];	
	 if (mhs::mex_hasParam(params,"bra")!=-1)
             bra=mhs::mex_getParam<REAL>(params,"bra",1)[0];	
	 
	 bra_max=bra;
	 
	 if (mhs::mex_hasParam(params,"bra_max")!=-1)
             bra_max=mhs::mex_getParam<REAL>(params,"bra_max",1)[0];	
	 
	 if (mhs::mex_hasParam(params,"startd")!=-1)
             startd=mhs::mex_getParam<REAL>(params,"startd",1)[0];	
	 if (mhs::mex_hasParam(params,"mind")!=-1)
             mind=mhs::mex_getParam<REAL>(params,"mind",1)[0];		 
	 if (mhs::mex_hasParam(params,"tnoise")!=-1)
             tnoise=mhs::mex_getParam<REAL>(params,"tnoise",1)[0];		 
	 if (mhs::mex_hasParam(params,"rootl")!=-1)
             rootl=mhs::mex_getParam<int>(params,"rootl",1)[0];
	 if (mhs::mex_hasParam(params,"crop")!=-1)
             crop=mhs::mex_getParam<int>(params,"crop",1)[0];	 
	 if (mhs::mex_hasParam(params,"mode")!=-1)
             mode=mhs::mex_getParam<int>(params,"mode",1)[0];	
	 if (mhs::mex_hasParam(params,"maxdepth")!=-1)
             maxdepth=mhs::mex_getParam<int>(params,"maxdepth",1)[0];		 
    }    
    printf("maxdepth: %d\n",maxdepth);
    
//     if (bra_max<bra)
//       std::swap(bra_max,bra);
    if((bra_max<bra))
    {
     printf("error"); 
     return;
    }
    
   
   
    int w = (int) dim[0];
    int h = (int) dim[1];
    int d = (int) dim[2];        
    
    
    
    
    
    int dims[3];
    dims[0] = w;
    dims[1] = h;
    dims[2] = d;
    plhs[0] = mxCreateNumericArray(3,dims,mhs::mex_getClassId<REAL>(),mxREAL);
    REAL *accu = (REAL*) mxGetData(plhs[0]);

    TreeProd myTree(accu,w,h,d, dens, dt, nz, mean_len, dev_len, bra,bra_max, mind,tnoise,crop,mode,exact_angle,shortfact,maxdepth);
    
    REAL x[3]; x[0] = REAL(dim[0]/2.0); x[1] =  REAL(dim[1]/2); x[2]=  1.0;
    REAL v[3]; v[0] = 0; v[1] = 0; v[2]=1;
    
   
    //myTree.createTree(x,v,0.9*startd,startd,std::ceil(REAL(rootl)/dt));
    myTree.createTree(x,v,startd,startd,std::ceil(REAL(rootl)/dt));
    
    
    if (nlhs>1)
    {
    dims[0] = 4;
    dims[1] = myTree.bifurcations.size();
    plhs[1] = mxCreateNumericArray(2,dims,mhs::mex_getClassId<REAL>(),mxREAL);
    REAL *bif = (REAL*) mxGetData(plhs[1]);	
    
    for (std::list<Bifu>::iterator iter=myTree.bifurcations.begin();
	iter!=myTree.bifurcations.end();iter++)
	 {
	  Bifu & bifu=*	 iter;
	  bif[0]=bifu.pos[0];
	  bif[1]=bifu.pos[1];
	  bif[2]=bifu.pos[2];
	  bif[3]=bifu.scale;
	  bif+=4;
	  //bifu.pos.print(); 
	}
    }
    
     if (nlhs>3)
    {
      printf("%d %d\n",myTree.pts.size(),myTree.cons.size());
      {
	dims[0] = 7;
	dims[1] = myTree.pts.size();
	plhs[2] = mxCreateNumericArray(2,dims,mhs::mex_getClassId<REAL>(),mxREAL);
	REAL *bif = (REAL*) mxGetData(plhs[2]);	
	
	for (std::list<Bifu>::iterator iter=myTree.pts.begin();
	    iter!=myTree.pts.end();iter++)
	    {
	      Bifu & bifu=*	 iter;
	      bif[0]=bifu.pos[0];
	      bif[1]=bifu.pos[1];
	      bif[2]=bifu.pos[2];
	      bif[3]=bifu.dir[0];
	      bif[4]=bifu.dir[1];
	      bif[5]=bifu.dir[2];
	      bif[6]=bifu.scale;
	      bif+=7;
	    }
      }
      {
	dims[0] = 2;
	dims[1] = myTree.cons.size();
	plhs[3] = mxCreateNumericArray(2,dims,mhs::mex_getClassId<REAL>(),mxREAL);
	REAL *bif = (REAL*) mxGetData(plhs[3]);	
	
	for (std::list<Edge>::iterator iter=myTree.cons.begin();
	    iter!=myTree.cons.end();iter++)
	    {
	      Edge & bifu=*	 iter;
	      bif[0]=bifu.a;
	      bif[1]=bifu.b;
	      bif+=2;
	    }
      }
    }
    
    
    
    //myTree.createTree(x,v,startd,startd,std::ceil(REAL(rootl)/dt));
}

// v = TreeProd(single([64 64 64]*2)); clf; isosurface(v,50); axis equal;
// mr = mrstruct_init('volume',v +randn(size(v))*20);   mr.vox = [1 1 1];
// mrstruct_write(mr,'/mymnt/Beast/matlab.work/tensorGT.V3/phantom9.mat');


