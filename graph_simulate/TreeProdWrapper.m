%
%[Img,ImgBif,ABif,Agt]=TreeProdWrapper('mode',1,'rootl',30,'maxdepth',2,'startd',8,'mean_len',500,'bra',25,'crop',20,'shape',2*[128,128,128]);figure(13);subplot(2,2,1);imagesc(squeeze(max(Img,[],1)));subplot(2,2,2);imagesc(squeeze(max(Img,[],2)));subplot(2,2,3);imagesc(squeeze(max(Img,[],3)));
%Scale=0.5;[Img,ImgBif,ABif,Agt]=TreeProdWrapper('mode',1,'rootl',Scale*60,'crop',20,'shape',Scale*[512,512,512],'mean_len',Scale*700,'mind',Scale*5,'startd',Scale*40);hdf5write('test.h5','Img',single(Img>0.25));
%Scale=0.5;[Img,ImgBif,ABif,Agt]=TreeProdWrapper('mode',1,'bra',0.55,'rootl',Scale*60,'crop',20,'shape',Scale*[512,512,512],'mean_len',Scale*700,'mind',Scale*2,'startd',Scale*32);hdf5write('test.h5','Img',single(Img>0.25));
%
function [Img,ImgBif,ABif,Agt]=TreeProdWrapper(varargin)

crop=0;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


[Img,ImgBif,Pts,Cons]=TreeProd({varargin{:}});


Cropme=[];
for d=1:3
    dims=[1,2,3];
    dims(d)=[];
    pimg=Img;
    for c=1:2
        pimg=max(pimg,[],dims(c));
    end;
    indx=find(pimg(:)>0);
%     [shape(d),numel(indx),indx(end)-indx(1)]
    Cropme=[Cropme;[indx(1),indx(end)]];
end;


%Cropme=Cropme+[-repmat(crop,3,1),+repmat(crop,3,1)];



Img2=Img(Cropme(1,1):Cropme(1,2),Cropme(2,1):Cropme(2,2),Cropme(3,1):Cropme(3,2));



shape=size(Img2);
%Img=zeros(shape+2*[crop,crop,crop]);
Img=zeros(shape+2*[crop,crop,crop/2]);

%Img(crop+1:end-crop,crop+1:end-crop,crop+1:end-crop)=Img2;
Img(crop+1:end-crop,crop+1:end-crop,1:end-crop)=Img2;


%ImgBif=ImgBif-repmat([Cropme(:,1);1],1,size(ImgBif,2))+crop+1;
ImgBif=ImgBif-repmat([Cropme(:,1);1],1,size(ImgBif,2))+crop+1;
ImgBif(3,:)=ImgBif(3,:)-crop;


ABif.data=zeros(21,size(ImgBif,2));
ABif.data(1:3,:)=ImgBif(1:3,:);
%ABif.data(8,:)=ImgBif(4,:);
ABif.data(8,:)=1;

ABif.data(21,:)=1;
ABif.connections=zeros(7,0);



Pts=Pts-repmat([Cropme(:,1);0;0;0;0],1,size(Pts,2));
Pts(1,:)=Pts(1,:)+crop+1;
Pts(2,:)=Pts(2,:)+crop+1;
%Pts(3,:)=Pts(3,:);

Agt.data=zeros(21,size(Pts,2));
Agt.data(1:3,:)=Pts(1:3,:);

Agt.data(4:6,:)=Pts([4,5,6],:);
%Agt.data(4:6,:)=Pts([4,5,6],:);
%Agt.data(6,:)=-Agt.data(6,:);


%ABif.data(8,:)=ImgBif(4,:);
Agt.data(8,:)=Pts(7,:)/2;

for ID=1:size(Agt.data,2)
    pos=Agt.data(1:3,ID);
    Agt.data(9:11,ID)=pos+Agt.data(4:6,ID);
    Agt.data(12:14,ID)=pos-Agt.data(4:6,ID);
end;

Agt.data(21,:)=1;
Agt.connections=zeros(7,size(Cons,2));
Agt.connections(end,:)=1;
Agt.connections(1:2,:)=Cons(1:2,:);


