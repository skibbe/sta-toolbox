#ifndef MY_OCTREE_H
#define MY_OCTREE_H

#include <list>
#include <string>
#include <cstddef>
#include <complex>
#include <cmath>
#include <sstream>
#include <cstddef>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <string>
#include <limits>

#include "mhs_error.h"
#include "mhs_vector.h"



int verbose=0;
unsigned int pointid=1;
const unsigned int mhs_maxpoints=10000000;

//#define _POINT_PROB_

template<typename T,int Dim> class OctTreeNode;
template<typename T,int Dim> class Cell;
template<typename T,int Dim> class volume_lock;
template<class C> class pool;
template<class C> class pool_element;


template<typename T,int Dim>
class OctPoints: public pool_element<class OctPoints<T,Dim> >
{
    friend class Cell<T,Dim>;
    friend class OctTreeNode<T,Dim>;
    friend pool<OctPoints>;

protected:

    unsigned int id;
    Cell<T,Dim> * owner;

    unsigned int index_id;
    unsigned int cell_index_sub_id;
    

    virtual void clear()
    {
	pool_element<class OctPoints<T,Dim> >::clear();
	id=pointid++;
	if (owner!=NULL)
        {
            owner->removepoint(*this);
            owner=NULL;
        }
        index_id=0;
    }
    
public:
  
    Vector<T,Dim> position;
    
    OctPoints(const OctPoints& copy)
    {
      position=copy.position;
      index_id=0;
      id=copy.id;
      owner=NULL;  // owner is NOT copied!!
    }
    
  OctPoints& operator=( const OctPoints& rhs )
  {
      position=rhs.position;
      //index_id=0;  // owner is NOT copied!!
      id=rhs.id;
      //owner=NULL;  // owner is NOT copied!!
      return *this;
  }
  
  bool has_owner(){return (owner!=NULL);}

    unsigned int getid() {
        return id;
    };
    
    T * getposvec() {
        return position.v;
    };

    void print()
    {
            printf("(%f %f %f %d %d)\n",position[0],position[1],position[2],(owner!=NULL), id);
    }

    bool operator >(const OctPoints & a) const
    {
        return (id>a.id);
    }
    bool operator <(const OctPoints & a) const
    {
        return (id<a.id);
    }

    OctPoints()
    {
        owner=NULL;
	clear();
        setpoint(-1,-1,-1);
    }


    void setpoint(const T point[])
    {
	position=point;
    }

    void setpoint(const Vector<T,Dim> & point)
    {
	position=point;
    }    
    
    
    void setpoint(T x, T y, T z=0)
    {
        position[0]=x;
        position[1]=y;
        position[2]=z;
    }

    void  clear_owner()
    {
        this->owner=NULL;
    }


    virtual ~OctPoints()
    {
        if (owner!=NULL)
        {
            if (verbose>1)
                printf("unregistering %d\n",id);
            owner->removepoint(*this);
            owner=NULL;
        }
    }
    
    void update_pos()
    {
     //#pragma omp critical (GRID_POINTER_UPDATE)
     try{
       sta_assert(owner!=NULL);
	if (owner!=NULL)
	{
	  OctTreeNode<T,Dim> * root=owner->parent;
// 	  int old_v=owner->num_pts;
//  	  printf(":-(  ");
	    owner->removepoint(*this);
//  	    printf(" . ");
	    if (!root->insert(*this))
	    {
		print();
 		printf("error updating point position (re-insering point)\n");
	    }
// 	    sta_assert(root==owner->parent);
// 	    sta_assert(old_v==owner->num_pts);
//  	    printf(" ! ");
	/*      printf(":-)\n");	 */   
	  }
       
    }  catch (mhs::STAError & error)
    {
        throw error;
    }
//     printf(" done\n");
    }
      
};






template<typename T,int Dim>
class OctTreeNode
{
  protected:
    friend class Cell<T,Dim> ;
    friend class OctPoints<T,Dim> ;
  
    Cell<T,Dim> *** grid;  
    Cell<T,Dim> **  grid_;
    Cell<T,Dim> *   grid__;
    
    class OctPoints<T,Dim> ** point_index;
//     bool * empty_indx;
    
    T cell_size;
    std::size_t shape[3];
    std::size_t grid_shape[3];
    std::size_t num_cells;
    std::size_t num_voxel;
    
    bool delete_data;
    unsigned int  numpoints;    
  
    bool remove( OctPoints<T,Dim>  & p)
    {
#ifdef _MCPU           
 	#pragma omp critical (TREE_POINTER_UPDATE)
#endif      
	{
	  numpoints--;
	  point_index[numpoints]->index_id=p.index_id;
	  point_index[p.index_id]=point_index[numpoints]; 
	}
    };
    
    void init()
    {
	int capacity=std::ceil(cell_size*cell_size*cell_size);
	
	num_cells=1;
	for (int i=0;i<3;i++)
	{
	  grid_shape[i]=ceil(shape[i]/cell_size);
	  num_cells*=grid_shape[i];
	}

	  grid=new Cell< T,Dim >** [grid_shape[0]];
	  grid_=new Cell< T,Dim >* [grid_shape[0]*grid_shape[1]];
	  grid__=new Cell< T,Dim >[grid_shape[0]*grid_shape[1]*grid_shape[2]];
	  
	  for (std::size_t z=0;z<grid_shape[0];z++)
	    grid[z]=grid_+z*grid_shape[1];
	  
	  for (std::size_t y=0;y<grid_shape[0]*grid_shape[1];y++)
	    grid_[y]=grid__+y*grid_shape[2];
	  
	  for (std::size_t x=0;x<num_cells;x++)
	    grid__[x].init(capacity,this);
	  
	  point_index=new class OctPoints<T,Dim> *[mhs_maxpoints];
    };
    
    void cleanup()
    {
	{
	  if (delete_data)
	  {
	    printf("cleaning up all points\n");
	   for (std::size_t i=0; i<numpoints; i++)
	   {
                    delete point_index[i];
	   }
 	    printf("done\n");	   
	  }

	  if (grid__!=NULL)
	    delete [] grid__;
	  if (grid_!=NULL)
	    delete [] grid_;
	  if (grid!=NULL)
	    delete [] grid;	  
	  
	  if (point_index!=NULL)
	  {
	    delete [] point_index;
	  }
	}
    }
  public:    
    
    void print(){
      unsigned int min_pts=100000000;
      unsigned int max_pts=0;
      for (int z=0;z<grid_shape[0];z++)
	for (int y=0;y<grid_shape[1];y++)
	  for (int x=0;x<grid_shape[2];x++)
	  {
	    min_pts=std::min(grid[z][y][x].num_pts,min_pts);
	    max_pts=std::max(grid[z][y][x].num_pts,max_pts);
	  }
	printf("%d %d\n",min_pts,max_pts)  ;
      
	printf("total points: %d \n",numpoints);
    };
    
    void get_pointlist(
      class OctPoints<T,Dim> ** & index,
      unsigned int & npoints)
    {
	index=point_index;
	npoints=numpoints; 
    }
    
    int get_numpts(){return numpoints;};

    
    OctPoints<T,Dim> * get_rand_point(
      class volume_lock<T,Dim> & vlock,
      T lock_radius,
      bool & empty)
    {
	empty=false;
	OctPoints<T,Dim> * point_ptr=NULL;
#ifdef _MCPU     	
	#pragma omp critical (TREE_POINTER_UPDATE)
#endif	
	{
	  
	  if (numpoints>0)
	  {
	    point_ptr=((point_index)[std::rand()%numpoints]);
	    bool can_lock=vlock.lock(point_ptr->position,lock_radius);
	    if (!can_lock)
		point_ptr=NULL;
	  }else
	    empty=true;
	}
        return point_ptr;
    }
    
    OctPoints<T,Dim> * get_rand_point(bool & empty)
    {
	empty=false;
	OctPoints<T,Dim> * point_ptr=NULL;
#ifdef _MCPU     	
	#pragma omp critical (TREE_POINTER_UPDATE)
#endif	
	{
	  
	  if (numpoints>0)
	  {
	    point_ptr=((point_index)[std::rand()%numpoints]);
	  }else
	    empty=true;
	}
        return point_ptr;
    }
    
    
    bool insert(OctPoints<T,Dim> & p)
    {
	sta_assert(numpoints<mhs_maxpoints);
	std::size_t gridpos[3];
	gridpos[0]=std::floor(p.position.v[0]/cell_size);
	gridpos[1]=std::floor(p.position.v[1]/cell_size);
	gridpos[2]=std::floor(p.position.v[2]/cell_size);
	
	if (
	  (gridpos[0]<0)||
	  (gridpos[1]<0)||
	  (gridpos[2]<0)||
	  (gridpos[0]>grid_shape[0]-1)||
	  (gridpos[1]>grid_shape[1]-1)||
	  (gridpos[2]>grid_shape[2]-1)
	) 
	{
	  printf("\n%d %d %d / %d %d %d\n",gridpos[0],gridpos[1],gridpos[2],
 	  grid_shape[0],grid_shape[1],grid_shape[2]);
	  p.print();
	  printf("???????????????????????????????\n");
	  sta_assert(false);
	  return false;
	}
	
	if (!grid[gridpos[0]][gridpos[1]][gridpos[2]].addpoint(p))
	  return false;
#ifdef _MCPU     	
	#pragma omp critical (TREE_POINTER_UPDATE)
#endif	
	{
	  p.index_id=numpoints++;
// 	  empty_indx[p.index_id]=false;
	  point_index[p.index_id]=&p;
	}
	return true;
    }
    
    
    
 void queryRange(
      Vector<T,Dim> &  position,
      T radius,
      class OctPoints<T,Dim>**  & query_buffer,
      std::size_t buffer_size,
      std::size_t & found)
 {
	int gridpos_lower[3];
	int gridpos_upper[3];
	T * pos=position.v;
	
	
	gridpos_lower[0]=std::floor((*pos-radius)/cell_size);
	if (gridpos_lower[0]<0) 
	  gridpos_lower[0]=0;
	
	gridpos_upper[0]=std::ceil((*pos+radius)/cell_size);
	if (gridpos_upper[0]>grid_shape[0]-1)
	  gridpos_upper[0]=grid_shape[0]-1;
	
	pos++;
	
	gridpos_lower[1]=std::floor((*pos-radius)/cell_size);
	if (gridpos_lower[1]<0) 
	  gridpos_lower[1]=0;
	
	gridpos_upper[1]=std::ceil((*pos+radius)/cell_size);
	if (gridpos_upper[1]>grid_shape[1]-1)
	  gridpos_upper[1]=grid_shape[1]-1;
	
	pos++;

	gridpos_lower[2]=std::floor((*pos-radius)/cell_size);
	if (gridpos_lower[2]<0) 
	  gridpos_lower[2]=0;
	
	gridpos_upper[2]=std::ceil((*pos+radius)/cell_size);
	if (gridpos_upper[2]>grid_shape[2]-1)
	  gridpos_upper[2]=grid_shape[2]-1;
	
	

	class OctPoints<T,Dim>**  query_buffer_ptr=query_buffer;
	found=0;
	radius*=radius;
	for (int z=gridpos_lower[0];z<=gridpos_upper[0];z++)
	  for (int y=gridpos_lower[1];y<=gridpos_upper[1];y++)
	    for (int x=gridpos_lower[2];x<=gridpos_upper[2];x++)
	    {
	      //grid[z][y][x].grap_points(query_buffer_ptr,buffer_size,found,position,radius);
	      grid[z][y][x].grap_pointsL2(query_buffer_ptr,buffer_size,found,position,radius);
	    }
	
 }
    
    void deletedata(bool k) {
        delete_data=k;
    }   
    
    OctTreeNode(std::size_t shape[], T cell_size=10)
    :grid(NULL),grid_(NULL),grid__(NULL),point_index(NULL),numpoints(0)
    {
      
	printf("using a voxegrid\n");
	this->shape[0]=shape[0];
	this->shape[1]=shape[1];
	this->shape[2]=shape[2];
	this->cell_size=cell_size;
	init();
	delete_data=false;
    }

    ~OctTreeNode()
    {
	cleanup(); 
    }
    
    OctTreeNode()
    {sta_assert(1==0)};
};





template<typename T,int Dim>
class Cell
{
  protected:
    friend class OctTreeNode<T,3>;
    friend class OctPoints<T,Dim> ;
    unsigned int capacity;
    unsigned int num_pts;
    OctPoints<T,Dim> ** points;
    OctTreeNode<T,Dim> * parent; 
    
    void init(unsigned int capacity,OctTreeNode<T,Dim> *  parent)
    {
	this->capacity=capacity;
	points=new OctPoints<T,Dim> *[capacity];
	this->parent=parent;
    };
    
    bool addpoint(OctPoints<T,Dim>  & point)
    {
      if (num_pts<capacity)
      {
	point.cell_index_sub_id=num_pts;
	points[num_pts++]=&point;
	point.owner=this;
// 	bool ok=true;
// 	  for (int i=0;i<num_pts;i++)
// 	  {
// 	    if (i!=points[i]->cell_index_sub_id)
// 	    ok=false;
// 	  }
// 	  if (!ok)
// 	for (int i=0;i<num_pts;i++)
// 	  {
// 	    printf ("%u %u",i,points[i]->cell_index_sub_id);
// 	  }
// 	
	return true;
      }
      return false;
    }
    
    void removepoint(OctPoints<T,Dim>  & point)
    {
	{      
	sta_assert_error(num_pts>0);  
	num_pts--;
	points[point.cell_index_sub_id]=points[num_pts];
	points[point.cell_index_sub_id]->cell_index_sub_id=point.cell_index_sub_id;
	parent->remove(point);
	point.owner=NULL;
      }
    }
    
    void grap_pointsL2(
      class OctPoints<T,Dim>**  & query_buffer,
      const std::size_t & buffer_size,
      std::size_t & found,
      const Vector<T,Dim> pos,
      const T & radius2)
    {
	class OctPoints<T,Dim>** pts=points;
	const T * pt_data1;
	const T * pt_data2;
	T dist2;
	T tmp;
	for (int i=0;i<num_pts;i++,pts++)
	{
 	  pt_data1=pos.v;
 	  pt_data2=(*pts)->position.v;
 	  tmp=(*pt_data1++)-(*pt_data2++);
 	  dist2=tmp*tmp;
 	  if (dist2>radius2) {continue;}
 	  tmp=(*pt_data1++)-(*pt_data2++);
 	  dist2+=tmp*tmp;
 	  if (dist2>radius2) {continue;}
 	  tmp=(*pt_data1++)-(*pt_data2++);
 	  dist2+=tmp*tmp;
 	  if (dist2>radius2) {continue;}
	  {
	    (*query_buffer++)=*pts;
	    if (!(found<buffer_size))
	    {
	      printf("search buffer full!\n");
	      sta_assert(found<buffer_size);
	    }
	    found++;
	  }
// 	  pts++;
	}
    }
    
public:
  
  Cell():points(NULL),capacity(0),num_pts(0),parent(NULL){};
  ~Cell()
  {
    if (points!=NULL)
      delete [] points;
  };
  
};










#endif




