#ifndef MHS_GRAPHICS_H
#define MHS_GRAPHICS_H

#include "mhs_vector.h"

template<typename T,int Dim> class Vector;

namespace mhs_graphics
{

  template<typename T>
  void createOrths(const Vector<T,3> & v,Vector<T,3> & vn1, Vector<T,3> &vn2)
  {
      if (std::abs(v[0])>std::abs(v[1]))
      {
	if (std::abs(v[1])>std::abs(v[2]))
	{
	  vn1=v.cross(Vector<T,3>(0,0,1));
	}else
	{
	  vn1=v.cross(Vector<T,3>(0,1,0));
	}
      }else
      {
	if (std::abs(v[0])>std::abs(v[2]))
	{
	  vn1=v.cross(Vector<T,3>(0,0,1));
	}else
	{
	  vn1=v.cross(Vector<T,3>(1,0,0));
	}      
      }
      vn1.normalize();
      vn2=vn1.cross(v);
  }


  template<typename T>  
    int renderPlane( Vector<T,3> & X,
		     Vector<T,3> & V,
		     T d,
		     T weight,
		     int shape[],
		     T * img,
		     int paint_mode=0)
    {

        d/=2;

	Vector<int,3> bbl;
	Vector<int,3> bbu;
	V.normalize();
	
	
	int boffset=2;
	if (paint_mode==2)
	  boffset=2*d+1;
	for (int e=0;e<3;e++)
	{
	  T low=X[e]-d-boffset;
	  T up=X[e]+d+boffset;
	  bbl[e]=std::max(std::floor(low),T(0));
	  bbu[e]=std::min(std::ceil(up),T(shape[e]));
	}
	
	for (int z=bbl[0];z<bbu[0];z++)
	for (int y=bbl[1];y<bbu[1];y++)
	  for (int x=bbl[2];x<bbu[2];x++)
	  {
	    Vector<T,3> pos(z,y,x);
	    

// 	    if ((pos-X).norm2()>d*d)
// 	      continue;
// 	    {
// 	     int index=(z*shape[1]+y)*shape[2]+x;
// 	      img[index]=std::max(img[index],T(1)); 
// 	      continue;
// 	    } 
	    
	    Vector<T,3> localx=pos-X;
	    
	    T pdist=V.dot(localx);
	    
	    if (std::abs(pdist)>0.5)
	      continue;
	    
	    //Vector<T,3> localr=localx-V*pdist;
	    //T rad2=localr.norm2();
	    
	    T rad2=(localx-V*pdist).norm2();
	    
	    
	    
	    
	    T v;
	    switch (paint_mode)
	    {
	      case 0:
	      {
		if (!(rad2<(d+2)*(d+2)))
		  continue;
	          T dists=std::sqrt(rad2);
		  T dist=dists-d;
		  if (dist>0)
		    continue;
		   v=1; 
	      }break;
	      case 1:
	      {
		 if (!(rad2<(d+2)*(d+2)))
		  continue;
	      
		  T dists=std::sqrt(rad2);
		  T dist=dists-d;
		  if (dist<0)
		    v=1;
		  else
		    v=1-std::min(dist/T(1.25),T(1));
	      }break;
	      case 2:
	      {
		 if (!(rad2<4*d*d))
		continue;
	      v=std::exp(-rad2/(d*d));
	      }break;	      
	    }
	   
	    
	    v*=weight;
	    int index=(z*shape[1]+y)*shape[2]+x;
	      img[index]=std::max(img[index],v); 
	  }
        return 0;
    }
}



#endif
