//#define _POOL_TEST
//#define _NUM_THREADS_ 4

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"


#include "graph_rep.h"
#include "specialfunc.h"

#include "mhs_data_sfilter.h"
#include "mhs_data_hough.h"
#include "mhs_data_medialness.h"
#include "mhs_data_hessian.h"
#include "mhs_data_rayburst.h"



const int Dim=3;


#define EPS 0.00000000001
#define EPS_R 0.0001




enum PROPOSAL_TYPES {
    PROPOSAL_BIRTH=0,
    PROPOSAL_DEATH=1,
    PROPOSAL_ROTATE=2,
    PROPOSAL_CONNECT=3,
    PROPOSAL_MOVE=4,
    PROPOSAL_SCALE=5,
    PROPOSAL_CONNECT_BIRTH=6,
    PROPOSAL_CONNECT_DEATH=7,
    PROPOSAL_INSERT_BIRTH=8,
    PROPOSAL_INSERT_DEATH=9,    
};  




std::string enum2string(int type)
{
 switch (type)
 {
   case PROPOSAL_BIRTH:
     return "BIRTH  ";
     break;
   case PROPOSAL_DEATH:
     return "DEATH  ";
     break;     
   case PROPOSAL_ROTATE:
     return "ROTATE ";
     break;
   case PROPOSAL_CONNECT:
     return "CONNECT";
   case PROPOSAL_MOVE:
     return "MOVE   ";
     break;
   case PROPOSAL_SCALE:
     return "SCALE  ";
     break;     
   case PROPOSAL_CONNECT_BIRTH:
     return "C_BIRTH";
     break;
   case PROPOSAL_CONNECT_DEATH:
     return "C_DEATH";
     break;         
   case PROPOSAL_INSERT_BIRTH:
     return "I_BIRTH";
     break;
   case PROPOSAL_INSERT_DEATH:
     return "I_DEATH";
     break;    
   default:
     return "unknown";
 }
}


// std::string enum2stringS(int type)
// {
//  switch (type)
//  {
//    case PROPOSAL_BIRTH:
//      return "B ";
//      break;
//    case PROPOSAL_DEATH:
//      return "D ";
//      break;     
//    case PROPOSAL_ROTATE:
//      return "R ";
//      break;
//    case PROPOSAL_CONNECT:
//      return "C ";
//    case PROPOSAL_MOVE:
//      return "M ";
//      break;
//    case PROPOSAL_SCALE:
//      return "S ";
//      break;     
//    case PROPOSAL_CONNECT_BIRTH:
//      return "CB";
//      break;
//    case PROPOSAL_CONNECT_DEATH:
//      return "CD";
//      break;         
//    case PROPOSAL_INSERT_BIRTH:
//      return "IB";
//      break;
//    case PROPOSAL_INSERT_DEATH:
//      return "ID";
//      break;    
//    default:
//      return "unknown";
//  }
// }




/*!
 *
 *  MAIN FUNCTION
 *
 * */
template <typename T,typename TData>
//template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  

  
  printf("\n\n---------------START----------------\n\n");
  printf("data precision : %s\n",mhs::mex_getPrecisionFlag<TData>().c_str());
  printf("comp precision : %s\n",mhs::mex_getPrecisionFlag<T>().c_str());
  
  
  track_id=0;
  classid=0;
  pointid=1;
//   normal_scale=true;
  
 
    std::srand (std::time(NULL));
  
    int paramindx=0;
    if (nrhs==0)
        mexErrMsgTxt ( "error: expecting image\n" );

    std::size_t shape[3];
    std::size_t numvoxel=0;

    std::vector<T> proposal_prop;  
    
    
//     int scale_steps=5;
    int numiterations=10;
    T connection_candidate_searchrad=5;
    T Temp=100;
    T lambda=100;
    T imgenergy=10000000;
    T ConnTemp=1;
    T ConnCostTemp=1;
    T DataTemp=1;
    T PointTemp=1;
    bool randwalk=true;
//     bool edge_length_energy=false;
//     bool max_hard_edge_length=true;
    double iteration_time=0;
    double iteration_time_total=0;
    
    
    bool constraint_isdirectedge=true;
    double iteration_total_num=0;
//     int multi_egde_check=-1;
    bool use_saliency_map=true;
    //bool useing_vesselness=false;
    T energy_grad=0;
    T energy_grad_old=0;
//     T old_energy=0;
    T opt_alpha=0.9;
    T gradient_data_[7];
    std::vector<T> proposals;
//     T PointScaleCost=0;
    
    T custom_grid_spacing=-1;
    
    
     CData<T,TData,Dim> * Dataterm=NULL;
     CEdgecost<T,Dim> * edgecost_fun=new CNTrackerEdgecost<T,Dim>();
    
//     int scale_power=1;
    
    
    
//      std::vector<T> tracking_sigma_scales;
//      tracking_sigma_scales.resize(3);
//      tracking_sigma_scales[0]=1;
//      tracking_sigma_scales[1]=1;
//      tracking_sigma_scales[2]=1;
    
//     T DataScale=1;
//     T DataScaleGrad=1;
//     T DataScaleGradVessel=1;
//     T DataThreshold=1;
//     T ConnectionScale=1;
//     T ThicknessScale=1;
//     T AngleScale=1;
//     bool noedgescales=false;
    
    std::vector<T> particle_connection_state_cost_scale;particle_connection_state_cost_scale.resize(3);
    particle_connection_state_cost_scale[0]=particle_connection_state_cost_scale[1]=particle_connection_state_cost_scale[2]=1;
  
    std::vector<T> particle_connection_state_cost_offset;particle_connection_state_cost_offset.resize(3);
    particle_connection_state_cost_offset[0]=particle_connection_state_cost_offset[1]=particle_connection_state_cost_offset[2]=0;
    
//     T Connectionparticle_connection_state_cost_scale=1;
    
    
    
    T L=-2;
    T Lprior=0;

//     bool searchrad_scales=false;
    int loop_depth=-1;
    Points<T,Dim>::maxconnections=3;
    int opt_eval_state=1;
//     int opt_eval_state2=1;
//     int energy_ratio[2];
//     energy_ratio[0]=energy_ratio[1]=1;
//     
    
    int numproposals=10;
    
    int proposal_activity_reg=0;
    int proposal_activity[numproposals][2];
    for (int i=0; i<numproposals; i++)
      proposal_activity[i][0]=proposal_activity[i][1]=proposal_activity_reg;    
    
    bool return_statistic=false;
    
//     bool subpixel=false;
    
    
    /*!
     *
     * 	Reading Data
     *
     * */

//     int num_scales=0;
//     int num_alphas=0;
//     int num_alphas_curve=0;
//     int num_alphas_vessel=0;
    
    int num_sphere_pts=0;
    
    const mxArray *feature_struct;
    feature_struct = prhs[paramindx++];
    sta_assert(mxIsStruct(feature_struct));

    //T *simg=NULL; // smoothed image
//     mhs::dataArray<TData> scales;
    
    
    
    
    mhs::dataArray<TData>  saliency_map;
    mhs::dataArray<TData>  saliency_map_acc;

    saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
    saliency_map_acc=mhs::dataArray<TData>(feature_struct,"saliency_map_acc");

    
//     scales=mhs::dataArray<TData>(feature_struct,"scales");
    
/*    for (int i=0;i<Dim;i++)    
      shape[i]=saliency_map.dim[i]; */    
    
    

//     template<typename T,int Dim, class DATA,class EDGECOST>
    


    int datafunc=mhs::dataArray<TData>(feature_struct,"datafunc").data[0];

    switch(datafunc)
    {
      case 1:
      {
	Dataterm=new CDataSFilter<T,TData,Dim>();
	printf("dataterm: SFilter\n");
      }break;
      case 2:
      {
	Dataterm=new CDataHough<T,TData,Dim>();
	printf("dataterm: Hough\n");
      }break;
      case 3:
      {
	Dataterm=new CDataMedial<T,TData,Dim>();
	printf("dataterm: Medialness\n");
      }break;
      case 4:
      {
	Dataterm=new CDataHessian<T,TData,Dim>();
	printf("dataterm: Derivatives\n");
      }break;
      default:
      {
	printf("dataterm: ?? %d\n",datafunc);
	sta_assert(0==1);
      }
    }
    
//     printf("init ");
    Dataterm->init(feature_struct);
//     printf("done\n ");
    Dataterm->getshape(shape);
    
    T minscale=Dataterm->get_minscale();
    T maxscale=Dataterm->get_maxscale();
    
    
    
    CTracker<TData,T,Dim> tracker(*Dataterm,*edgecost_fun);
    
    
//     num_scales=scales.dim[0];
//     sta_assert(num_scales>0);
    
//     printf("numscales %d\n",num_scales);
    
//     T maxscale=0;
//     T minscale=100000000000;
//     for (int i=0;i<num_scales;i++)
//     {
//       maxscale=std::max((T)scales.data[i],maxscale);
//       minscale=std::min((T)scales.data[i],minscale);
//     } 

    printf("scale: [%f %f]\n",minscale,maxscale);
    
    
    /*!
     *
     * 	Existing Data 
     *
     *
     * */
    
    mhs::dataArray<T> treedata;
    mhs::dataArray<T> conncection_data;
    
   
    
    
    if ((nrhs>1)&&(!mxIsCell(prhs[paramindx]))&&(mxIsStruct(prhs[paramindx])))
    {
        const mxArray *data;
        data = prhs[paramindx++];
	
	treedata=mhs::dataArray<T>(data,"data"); 
	conncection_data=mhs::dataArray<T>(data,"connections");
	

	
	const mxArray * mx_enegy= mxGetField(data,0,(char *)"opt_eval_state");
        sta_assert(mx_enegy!=NULL);
        opt_eval_state=*(( T * ) ( mxGetData ( mx_enegy ) ));
	
// 	mx_enegy= mxGetField(data,0,(char *)"opt_eval_state2");
//         sta_assert(mx_enegy!=NULL);
//         opt_eval_state2=*(( T * ) ( mxGetData ( mx_enegy ) ));
	
// 	mx_enegy= mxGetField(data,0,(char *)"ratio_up");
//         sta_assert(mx_enegy!=NULL);
//         energy_ratio[0]=*(( T * ) ( mxGetData ( mx_enegy ) ));
// 	mx_enegy= mxGetField(data,0,(char *)"ratio_down");
//         sta_assert(mx_enegy!=NULL);
//         energy_ratio[1]=*(( T * ) ( mxGetData ( mx_enegy ) ));
	
	mx_enegy= mxGetField(data,0,(char *)"iteration_time_total");
        sta_assert(mx_enegy!=NULL);
        iteration_time_total=*(( T * ) ( mxGetData ( mx_enegy ) ));
	
	mx_enegy= mxGetField(data,0,(char *)"iteration_total_num");
        sta_assert(mx_enegy!=NULL);
        iteration_total_num=*(( T * ) ( mxGetData ( mx_enegy ) ));	
	
	mx_enegy= mxGetField(data,0,(char *)"energy_grad");
        sta_assert(mx_enegy!=NULL);
        energy_grad=*(( T * ) ( mxGetData ( mx_enegy ) ));	
	
	mx_enegy= mxGetField(data,0,(char *)"energy_grad_old");
        sta_assert(mx_enegy!=NULL);
        energy_grad_old=*(( T * ) ( mxGetData ( mx_enegy ) ));	
	
	
// 	mx_enegy= mxGetField(data,0,(char *)"old_energy");
//         sta_assert(mx_enegy!=NULL);
//         old_energy=*(( T * ) ( mxGetData ( mx_enegy ) ));	
	
	
	
	mx_enegy = mxGetField(data,0,(char *)"activity");
	sta_assert(mx_enegy!=NULL);
	double * tmp_ptr=(double *)mxGetData(mx_enegy);
	for (int i=0; i<numproposals; i++)
	  for (int j=0; j<2; j++)  
	    proposal_activity[i][j]=(*tmp_ptr++);
	
	
// 	for (int i=0; i<numproposals; i++)
// 	{
// 	    proposal_activity[i][0]=proposal_activity[i][1]=0; 	
// 	}
	
	
// 	printf("opt_eval_state %d, E-ratio %d/%d\n",opt_eval_state,energy_ratio[0],energy_ratio[1]);
//         printf("found existing data\n");
        mhs::mex_dumpStringNOW();
    }

    
    printf("nex step\n");

    if (nrhs>paramindx)
    {
        const mxArray * params=prhs[nrhs-1] ;
	
	Dataterm->set_params(params);
	edgecost_fun->set_params(params);


        if (mhs::mex_hasParam(params,"return_statistic")!=-1)
            return_statistic=mhs::mex_getParam<bool>(params,"return_statistic",1)[0];
/*
     if (mhs::mex_hasParam(params,"normal_scale")!=-1)
            normal_scale=mhs::mex_getParam<bool>(params,"normal_scale",1)[0];*/
     
//      if (mhs::mex_hasParam(params,"searchrad_scales")!=-1)
//             searchrad_scales=mhs::mex_getParam<bool>(params,"searchrad_scales",1)[0];

	
        if (mhs::mex_hasParam(params,"randwalk")!=-1)
            randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];	
	
	
        if (mhs::mex_hasParam(params,"iterations")!=-1)
            numiterations=mhs::mex_getParam<int>(params,"iterations",1)[0];
	

        if (mhs::mex_hasParam(params,"loop_depth")!=-1)
            loop_depth=mhs::mex_getParam<int>(params,"loop_depth",1)[0];
	
        if (mhs::mex_hasParam(params,"ccT")!=-1)
            ConnCostTemp=mhs::mex_getParam<T>(params,"ccT",1)[0];
/*	
        if (mhs::mex_hasParam(params,"max_hard_edge_length")!=-1)
            max_hard_edge_length=mhs::mex_getParam<bool>(params,"max_hard_edge_length",1)[0];		*/
	
	if (mhs::mex_hasParam(params,"constraint_isdirectedge")!=-1)
            constraint_isdirectedge=mhs::mex_getParam<bool>(params,"constraint_isdirectedge",1)[0];		
	
	if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];	
	
// 	if (mhs::mex_hasParam(params,"PointScaleCost")!=-1)
//             PointScaleCost=mhs::mex_getParam<T>(params,"PointScaleCost",1)[0];
	
	
        if (mhs::mex_hasParam(params,"cT")!=-1)
            ConnTemp=mhs::mex_getParam<T>(params,"cT",1)[0];
	
        if (mhs::mex_hasParam(params,"T")!=-1)
            Temp=mhs::mex_getParam<T>(params,"T",1)[0];

        if (mhs::mex_hasParam(params,"dT")!=-1)
            DataTemp=mhs::mex_getParam<T>(params,"dT",1)[0];
	
	
	if (mhs::mex_hasParam(params,"pT")!=-1)
            PointTemp=mhs::mex_getParam<T>(params,"pT",1)[0];
	
	
	if (mhs::mex_hasParam(params,"endpoint_offset")!=-1)
            Points<T,Dim>::endpoint_nubble_scale=mhs::mex_getParam<T>(params,"endpoint_offset",1)[0];
/*	
	if (mhs::mex_hasParam(params,"ConnectionScale")!=-1)
            ConnectionScale=mhs::mex_getParam<T>(params,"ConnectionScale",1)[0];*/
	
	
// 	if (mhs::mex_hasParam(params,"ThicknessScale")!=-1)
//             ThicknessScale=mhs::mex_getParam<T>(params,"ThicknessScale",1)[0];
	
	if (mhs::mex_hasParam(params,"particle_connection_state_cost_scale")!=-1)
            particle_connection_state_cost_scale=mhs::mex_getParam<T>(params,"particle_connection_state_cost_scale",3);

	if (mhs::mex_hasParam(params,"particle_connection_state_cost_offset")!=-1)
            particle_connection_state_cost_offset=mhs::mex_getParam<T>(params,"particle_connection_state_cost_offset",3);		
	
	if (mhs::mex_hasParam(params,"custom_grid_spacing")!=-1)
            custom_grid_spacing=mhs::mex_getParam<T>(params,"custom_grid_spacing",1)[0];		
	
	if (mhs::mex_hasParam(params,"opt_alpha")!=-1)
            opt_alpha=mhs::mex_getParam<T>(params,"opt_alpha",1)[0];
	
// 	if (mhs::mex_hasParam(params,"Connectionparticle_connection_state_cost_scale")!=-1)
//             Connectionparticle_connection_state_cost_scale=mhs::mex_getParam<T>(params,"Connectionparticle_connection_state_cost_scale",1)[0];	

/*	if (mhs::mex_hasParam(params,"AngleScale")!=-1)
            AngleScale=mhs::mex_getParam<T>(params,"AngleScale",1)[0];	*/	
	
        if (mhs::mex_hasParam(params,"connection_candidate_searchrad")!=-1)
            connection_candidate_searchrad=mhs::mex_getParam<T>(params,"connection_candidate_searchrad",1)[0];		
	
        if (mhs::mex_hasParam(params,"L")!=-1)
            L=mhs::mex_getParam<T>(params,"L",1)[0];	

	if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];	
	
	if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	
	if (mhs::mex_hasParam(params,"propprop")!=-1)
            proposal_prop=mhs::mex_getParam<T>(params,"propprop");
/*	
        if (mhs::mex_hasParam(params,"scale_steps")!=-1)
            numiterations=mhs::mex_getParam<int>(params,"scale_steps",1)[0];	*/
	
        if (mhs::mex_hasParam(params,"max_connections")!=-1)
            Points<T,Dim>::maxconnections=mhs::mex_getParam<int>(params,"max_connections",1)[0];

	if (mhs::mex_hasParam(params,"point_weight")!=-1)
            Points<T,Dim>::point_weight=mhs::mex_getParam<int>(params,"point_weight",1)[0];		
	
	if (mhs::mex_hasParam(params,"thickness")!=-1)
            Points<T,Dim>::thickness=mhs::mex_getParam<T>(params,"thickness",1)[0];		

	if (mhs::mex_hasParam(params,"min_thickness")!=-1)
            Points<T,Dim>::min_thickness=mhs::mex_getParam<T>(params,"min_thickness",1)[0];	
    }
    
    
    
    
    sta_assert_error(!(opt_alpha<0));
    sta_assert_error(!(opt_alpha>1));
    printf("opt_alpha: %f\n",opt_alpha);
    
    printf("point cost = scale^%d\n",Points<T,Dim>::point_weight);
   
    
    
    printf("using saliency map: %d\n",use_saliency_map);
    
//     printf("non connected edge proposal scales: %d\n",noedgescales);
    
    
//     printf("multi_egde_check %d\n",multi_egde_check);
    
    
    printf("point weight offset %f %f %f\n",particle_connection_state_cost_offset[0],particle_connection_state_cost_offset[1],particle_connection_state_cost_offset[2]);
    
/*    
    printf("point scale cost: %f\n",PointScaleCost);*/
    
    
    printf("point thickness: %f\n",Points<T,Dim>::thickness);
    printf("point min_thickness: %f\n",Points<T,Dim>::min_thickness);
    
   
    printf("prevent naked edges: %d\n",constraint_isdirectedge);
    
    T energy_searchrad=maxscale*2+0.1;
    if (std::abs(Points<T,Dim>::thickness)>1)
      energy_searchrad=std::abs(Points<T,Dim>::thickness)*maxscale*2+0.1;
    
    printf("energy_searchrad %f\n",energy_searchrad);
    T search_connection_point_center_candidate=std::max(energy_searchrad,T(3)*maxscale+connection_candidate_searchrad);
    T search_connection_point_candidate=connection_candidate_searchrad;

    printf("connection searchrad %f\n",search_connection_point_candidate);
    T maxendpointdist=connection_candidate_searchrad*connection_candidate_searchrad;
//     if (!max_hard_edge_length)
// 	maxendpointdist=-1;
//     else
      search_connection_point_center_candidate=T(2)*maxscale+connection_candidate_searchrad+0.1;
    
    
    T lock_radius=search_connection_point_center_candidate*2;
  
    printf("connection point center searchrad %f\n",search_connection_point_center_candidate);

//     printf("searchrad_scales : %f\n",searchrad_scales);
    
    printf("image shape (%d x %d x %d)\n",shape[0],shape[1],shape[2]);
    numvoxel=shape[0]*shape[1]*shape[2];
    
    
    printf("randwalk :%d\n",randwalk);
    lambda=numvoxel;
    printf("lambda :%.0f , numvoxel:%d\n",lambda,numvoxel);    

    
    if (custom_grid_spacing<0)
      custom_grid_spacing=search_connection_point_center_candidate/2;
    printf("custom_grid_spacing %f\n",custom_grid_spacing);
    OctTreeNode<T,Dim> tree(shape,custom_grid_spacing);
    
    volume_lock<T,Dim> vlock;
    pool<class Points<T,Dim> > mypool(pointlimit,"points");
//     pool<class Connection<T,Dim> > my_connection_pool(100000);
    
    
    Connections<T,Dim>  connections;
   

    Points<T,Dim> * loaded_points=NULL;
    try
    {
      
        /*!
        *
        * 	init existing state
        *
        *
        * */
        if (treedata.data!=NULL)
        {
            T * ptreedata=treedata.data;
            unsigned int numpoints=treedata.dim[0];//]mx_treedata_dims[1];
            //sta_assert_error(mx_treedata_dims[0]==17);
	    sta_assert_error(treedata.dim[1]==22);
	    printf("initializing points ... ");
            for (int i=0; i<numpoints; i++)
            {
                //loaded_points=new Points<T,Dim>;
		loaded_points=mypool.create_obj();
		
                loaded_points->setpoint(ptreedata[2],ptreedata[1],ptreedata[0]);
		loaded_points->direction[0]=(ptreedata[5]);
		loaded_points->direction[1]=(ptreedata[4]);
		loaded_points->direction[2]=(ptreedata[3]);
		loaded_points->tracker_birth=ptreedata[6];
		loaded_points->scale=(ptreedata[7]);
		loaded_points->point_cost=(ptreedata[14]);
		loaded_points->saliency=(ptreedata[15]);
// 		loaded_points->ofield_indx=(ptreedata[15]);
// 		loaded_points->sindx=(ptreedata[16]);
		//loaded_points->vesselness=(ptreedata[17]);
// 		loaded_points->oindx=(ptreedata[20]);
		
		
		
		
		
		ptreedata+=treedata.dim[1];

                OctPoints<T,Dim> * opoint=(OctPoints<T,Dim> *)loaded_points;
		
		
// 		sta_assert(loaded_points->free_points_indx==mhs_maxpoints+1);
		
		
		
                if (!tree.insert(*opoint))
                {
                    loaded_points->print();
                    throw mhs::STAError("error insering point\n");
		    
                }else
		{
		 loaded_points->init_pointer(); 
		 loaded_points->path_id=i;
		}
            }
            printf("done\n");
	    
             T * pconncection_data=conncection_data.data;
	    
	     
	     

	     class OctPoints<T,Dim> ** index;
	     unsigned int  npoints;
	     tree.get_pointlist( index,npoints);
	     sta_assert_error(numpoints==npoints);
	    
	     unsigned int num_connections=conncection_data.dim[0];
             sta_assert_error(conncection_data.dim[1]==7);

	     printf("initializing edges ... ");
	     for (int i=0; i<num_connections; i++)
	     {
	       int pointidxA=pconncection_data[0];
	       int point_sideA=pconncection_data[2];
	       int pointidxB=pconncection_data[1];
	       int point_sideB=pconncection_data[3];
	       
	      sta_assert_error(pointidxA>=0);
	      sta_assert_error(pointidxA<npoints);
	      sta_assert_error(pointidxB>=0);
	      sta_assert_error(pointidxB<npoints);
	       class Connection<T,Dim> connection_new;
	       class Points<T,Dim> * pointA=(class Points<T,Dim> *)(index[pointidxA]);
	       class Points<T,Dim> * pointB=(class Points<T,Dim> *)(index[pointidxB]);
	       connection_new.pointA=pointA->getfreehub(point_sideA);
	       connection_new.pointB=pointB->getfreehub(point_sideB);
       
	       
	       if (maxendpointdist>-1)
		{
		  pointA->update_endpoint(connection_new.pointA->side);
		  pointB->update_endpoint(connection_new.pointB->side);
		  sta_assert_error(maxendpointdist>(*connection_new.pointA->position-*connection_new.pointB->position).norm2()); 
// 		  if (pointA->free_points_indx!=mhs_maxpoints+1)
// 		    printf("%d \n",pointA->free_points_indx);
// 			sta_assert(pointA->free_points_indx==mhs_maxpoints+1);
// 			sta_assert(pointB->free_points_indx==mhs_maxpoints+1);
// 		if (pointB->free_points_indx!=mhs_maxpoints+1)
// 		    printf("%d \n",pointB->free_points_indx);	    
		}
	       
	       
	       sta_assert_error(connection_new.pointA!=NULL);
	       sta_assert_error(connection_new.pointB!=NULL);
	       connections.add_connection(connection_new);
	       pconncection_data+=conncection_data.dim[1];
	     }
	     printf("done\n");
	    tree.print();
	    
// 	    {
// 	    class Points<T,Dim> ** index=mypool.getMemW();
// 	    for (unsigned int i=0; i<mypool.get_numpts(); i++)
// 	    {
// 		//Points<T,Dim> & point=*((Points<T,Dim> *) (index[i]));
// 	      //Points<T,Dim> & point=*(index[i]);
// 		if (!(index[i]->isconnected()))
// 		{
// 		  
// 		  free_points[free_points_num]=index[i];
// 		  index[i]->free_points_indx=free_points_num;
// 		  free_points_num++;
// 		}
// 	    }
// 	    }
        }




        /*!
         *
         * 	Iter
         *
         *
         * */

	
      std::size_t proposal_calls[numproposals];
      std::size_t proposal_accept[numproposals];    
        
        //T proposals[numproposals];
      
        proposals.resize(numproposals);
        proposals[PROPOSAL_BIRTH]=1; 
        proposals[PROPOSAL_DEATH]=1; 
        proposals[PROPOSAL_ROTATE]=1;
        proposals[PROPOSAL_CONNECT]=1;
        proposals[PROPOSAL_MOVE]=1; 
        proposals[PROPOSAL_SCALE]=1;
	proposals[PROPOSAL_CONNECT_BIRTH]=1;
	proposals[PROPOSAL_CONNECT_DEATH]=1;
	proposals[PROPOSAL_INSERT_BIRTH]=0;
	proposals[PROPOSAL_INSERT_DEATH]=0;/*
	proposals[PROPOSAL_BIF_BIRTH]=0;
	proposals[PROPOSAL_BIF_DEATH]=0;*/
	for (int i=0;i<proposal_prop.size();i++)
	{
	  if (i<numproposals)
	  {
	    proposals[i]=proposal_prop[i];
	  }else
	  {
	    throw mhs::STAError("more proposal parameters than proposals!");
	  }
	}
	

	
//         proposals[PROPOSAL_BIRTH]=1; 
//         proposals[PROPOSAL_DEATH]=1; 
//         proposals[PROPOSAL_ROTATE]=0;
//         proposals[PROPOSAL_CONNECT]=0;
//         proposals[PROPOSAL_MOVE]=0; 
//         proposals[PROPOSAL_SCALE]=0;
// 	proposals[PROPOSAL_CONNECT_BIRTH]=0;
// 	proposals[PROPOSAL_CONNECT_DEATH]=0;
        

	
        
	
	 TData *static_data =NULL;
    if (return_statistic && (nlhs>1))
    {
	int ndims[4];
	ndims[0]=numproposals*2;
	ndims[1]=shape[2];
	ndims[2]=shape[1];
	ndims[3]=shape[0];
	plhs[1] = mxCreateNumericArray(4,ndims,mhs::mex_getClassId<TData>(),mxREAL);
// 	int ndims[3];
// 	ndims[0]=shape[2];
// 	ndims[1]=shape[1];
// 	ndims[2]=shape[0];
// 	plhs[1] = mxCreateNumericArray(3,ndims,mhs::mex_getClassId<TData>(),mxREAL);      
 	static_data= (TData*) mxGetData(plhs[1]); 
      
    }else 
      return_statistic=false;
        
	
	bool debugpoints=false;
	    
	
	T proposals_map[numproposals];

	for (int i=0; i<numproposals; i++)
	  proposals_map[i]=proposals[i];
	
	for (int i=1; i<numproposals; i++)
	{
            proposals_map[i]=proposals_map[i]+proposals_map[i-1];
	}
        for (int i=0; i<numproposals; i++)
        {
	    proposals[i]/=proposals_map[numproposals-1];
            proposals_map[i]/=proposals_map[numproposals-1];
            proposal_calls[i]=0;
	    proposal_accept[i]=0;
        }
        
        printf("------------------------------------\n");
	//printf("proposal %s, prob %1.3f\n",enum2string(0).c_str(),proposals[0]);
        for (int i=0;i<numproposals;i++)
	//printf("proposal %s, prob %1.3f\n",enum2string(i).c_str(),proposals[i]-proposals[i-1]);
	  printf("proposal %s, prob %1.3f\n",enum2string(i).c_str(),proposals[i]);
	printf("------------------------------------\n");
        
   
        printf("iterations : %d\n",numiterations);
	printf("------------------------------------\n");
        printf("T        : %2.3f\n",Temp);
	printf("ccT      : %2.3f\n",ConnCostTemp);
	printf("cT       : %2.3f\n",ConnTemp);
	printf("dT       : %2.3f\n",DataTemp);
	printf("pT       : %2.3f\n",PointTemp);
	printf("------------------------------------\n");
	
	//energy_ratio[0]=energy_ratio[1]=1;
	
	
	
	int energy_ratio_up=1;
	
	int missed=0;
	 

int debuginfosize=0;
T debug_info[debuginfosize];
for (int i=0;i<debuginfosize;i++)
  debug_info[i]=0;

int num_decrease=0;



	
mhs::CtimeStopper timer;
	
	//#pragma omp parallel for num_threads(omp_get_num_procs())
        for (int i=1; i<=numiterations; i++)
        {
   
	  if (Temp<0.001)
	    break;
	  
	  T tmp_sqrt=std::sqrt(Temp);
	  T temp_fact_rot=tmp_sqrt;
	  T temp_fact_pos=tmp_sqrt;
	  T temp_fact_scale=tmp_sqrt+1;

// 	  if (opt_eval_state%10000==0)
// 	    printf("eratio: %f\n",energy_grad);
	  
	  
	  
	  
	  if ((energy_grad>0)&&(!(energy_grad_old>0)))
	  {
	    num_decrease++;
	    T dfact=0.98;
	    T cdfact=0.98;
	    Temp*=dfact;
	    DataTemp*=dfact;
	    PointTemp*=dfact;
	    ConnCostTemp*=cdfact;
	  }
	  energy_grad_old=energy_grad;
	  //opt_eval_state2++;
	  
	  
	    if (opt_eval_state%100000==0)
	    {

      
// 	      if (energy_grad>0)
// 	      {
// 		T dfact=0.98;
// 		T cdfact=0.98;
// 		Temp*=dfact;
// 		DataTemp*=dfact;
// 		PointTemp*=dfact;
// 		ConnCostTemp*=cdfact;
// 		printf("v");
// 
// 	      }else
// 		 printf(".");
	      if (num_decrease>0)
	      {
		printf("%d",num_decrease);
		num_decrease=0;
	      }else
		 printf(".");
	      
// 	      energy_ratio[0]=energy_ratio[1]=1;
	      
/*	      for (int r=0; r<numproposals; r++)
	      proposal_activity[r][0]=proposal_activity[r][1]=proposal_activity_reg; */   
	      
		if (opt_eval_state%1000000==0)
		{
		  printf(" (grad %1.2f, T %1.2f, [%d/%d])\n",energy_grad,Temp,i/1000000,numiterations/1000000);
		}
		    mhs::mex_dumpStringNOW();
	    }
	    else
	    {
	        if (opt_eval_state%10000==0)
		{
		    printf(".");
		    mhs::mex_dumpStringNOW();
		}
	    }

	    
	    {
	        if (opt_eval_state%10000==0)
		{
		    printf(".");
		    mhs::mex_dumpStringNOW();
		}
	    }
	    opt_eval_state++;
	    
	    
	    
	    /// uniformly pick proposal
            T choice=myrand(1);

            int proposal=-1;
            for (int a=0; a<numproposals; a++)
            {
                if (proposals_map[a]>=choice)
                {
                    proposal=a;
                    break;
                }
            }
            sta_assert_error(proposal<numproposals);
	    

	    


            switch (proposal)
            {
	      
/*##################################################################  
####################################################################
#  
#		PROPOSAL_INSERT_BIRTH
#  
####################################################################
##################################################################*/

	    case PROPOSAL_INSERT_BIRTH:
            {
	      bool empty;
	      class Connection<T,Dim> * edge;
	      
	      
	      /// uniformly pic edge 
	      edge=(connections.get_rand_edge(vlock,lock_radius,empty));
	      if (edge==NULL)
	      {
		if (!empty)
		  missed++;
		      continue;
	      }
	      sta_assert_error(!empty);
#ifdef 	_POOL_TEST      
	      sta_assert_error(edge->exist);
#endif	      
	      
	      proposal_calls[PROPOSAL_INSERT_BIRTH]++;
	      
	      
	      /// uniformly one endpoint as reference (with id 0)
	      class Points< T, Dim >::CEndpoint * epoints[2];
	      int choice=std::rand()%2;
	      epoints[choice]=edge->pointA;
	      epoints[1-choice]=edge->pointB;
	      
	      /// we only select edges not connected to bifucations
	      if ((epoints[0]->point->get_num_connections()>2)&&(epoints[1]->point->get_num_connections()>2))
	      {
		vlock.free(); 
		continue;
	      }
	      
	      /// update endpoint positions
	      epoints[0]->point->update_endpoint(epoints[0]->side);
	      epoints[1]->point->update_endpoint(epoints[1]->side);
	      
	      
	      /// optimnal pos: in between
	      Vector<T,Dim> optimal_pos=(*(epoints[0]->position)+*(epoints[1]->position))/2;
	      
	      /// optimnal direction: direction of reference point pointing in edge direction
	      Vector<T,Dim> optimal_direction=epoints[0]->point->direction*Points<T,Dim>::side_sign[epoints[0]->side];
	      
	      /// optimnal scale: mean scale
	      T optimal_scale=((epoints[0]->point->scale)+(epoints[1]->point->scale))/2;
	      
	      /// create new point
	      Points<T,Dim> * p_point=mypool.create_obj();
	      Points<T,Dim> & point=*p_point;
	      point.tracker_birth=2;
	      

		/// likelyness for point the parameter of the new point
		T connection_probability=1;
	      
	      
		/// valid scale interval
	      	T scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
		scale_lower=std::max(scale_lower,std::min(epoints[0]->point->scale,epoints[1]->point->scale));
		T scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale);
		/// pic scale (uniformly)
		point.scale=myrand(scale_lower,scale_upper);
		connection_probability*=1.0/(scale_upper-scale_lower+EPS);

		
	        /// pic position (normal)
		T agility=temp_fact_pos*std::sqrt((*(epoints[0]->position)-*(epoints[1]->position)).norm2())/4;
	        Vector<T,Dim> randv;
		randv.rand_normal(agility);
		point.position=optimal_pos+randv;
		
		if ((point.position[0]<0)||
		  (point.position[1]<0)||
		(point.position[2]<0)||
		  (point.position[0]-1>shape[0])||
		  (point.position[1]-1>shape[1])||
		  (point.position[2]-1>shape[2])
		){
		  mypool.delete_obj(p_point);
		  vlock.free(); 
		  continue;
		}
		connection_probability*=normal_dist<T,3>(randv.v,agility);
		
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
 		  static_data[center*2*numproposals+16]+=1;
		}	
		
		/// pic direction (normal)		
		random_pic_direction<T,Dim>(
		optimal_direction,
 		  point.direction,temp_fact_rot);		
		connection_probability*=normal_dist_sphere(optimal_direction,point.direction,temp_fact_rot);

		/// we consider the new particel as directed (just because I'm lazy)
		/// -> point must have same orientation as reference point
		if (optimal_direction.dot(point.direction)<0)
		{
		  mypool.delete_obj(p_point);
		  vlock.free(); 
		   continue;
		}
		
		/// point collides?
		if (colliding( tree,point,energy_searchrad))
		{
		  mypool.delete_obj(p_point);
		vlock.free(); 
		  continue;
		}
		
		/// setting new and old edges
		TConnecTProposal<T,Dim> newconnections[2];
		newconnections[0].old_connection_exists=false;  
		newconnections[0].connection_new.pointA=epoints[0];
		newconnections[0].connection_new.pointB=point.getfreehub(1-epoints[0]->side);
		newconnections[0].new_prob=1;
		newconnections[0].new_connection_exists=true;		
		sta_assert_error(newconnections[0].connection_new.pointA!=NULL);
		sta_assert_error(newconnections[0].connection_new.pointB!=NULL);
		
		newconnections[1].old_connection_exists=true;
		newconnections[1].connection_old=edge;
		newconnections[1].connection_new.pointA=epoints[1];
		newconnections[1].connection_new.pointB=point.getfreehub(epoints[0]->side);
		newconnections[1].new_prob=1;
		newconnections[1].new_connection_exists=true;		
		
		sta_assert_error(newconnections[1].connection_new.pointA!=NULL);
		sta_assert_error(newconnections[1].connection_new.pointB!=NULL);
		

		
		T newpointsaliency;
		if (!interp3(newpointsaliency,
		saliency_map.data, 
		point.position,
		shape))
		{
		   mypool.delete_obj(p_point);
			    vlock.free(); 
			      continue;
		}
		
		T newenergy_data;
		
		if (!Dataterm->eval_data(
		      newenergy_data,
// 		      newpointsaliency,
		      point.direction,
		      point.position,
		       point.scale))
			  {
			    mypool.delete_obj(p_point);
			    vlock.free(); 
			      continue;
			    }

			    


		    point.point_cost=newenergy_data;
 		    point.saliency=newpointsaliency;
		
		 /// computing  new edge costs
		 int inrange;
		 for (int e=0;e<2;e++)
		 {
		    newconnections[e].new_cost=newconnections[e].connection_new.compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
		    if (inrange<0)
		    {
		      break;
		    }		
		 }
		if (inrange<0)
		{
		  mypool.delete_obj(p_point);
		  vlock.free(); 		      
		  continue;
		}	
		 
		 /// computing  old edge costs
		 newconnections[1].old_cost=newconnections[1].connection_old->compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
		 sta_assert_error(!(inrange<0));

		 
		  /// Energy Data term
		  T E=newenergy_data/DataTemp;
		    
		  /// Point Costs
		  T pointcost=particle_connection_state_cost_scale[2]*point.compute_cost()+(particle_connection_state_cost_offset[2]);////+PointScaleCost*point.compute_cost_scale(); // 0-> 2 connections
		  E+=(pointcost)/PointTemp;
		  
		  
		  /// Edge Costs (+new - old)
		  T edge_cost=newconnections[0].new_cost+newconnections[1].new_cost-newconnections[1].old_cost;
		  E+=(edge_cost)/ConnCostTemp;
		
	      /// Energy
	      T R=mexp(E);
	     
	      energy_ratio_up=0;
	      if (R>1)
	      {
		energy_ratio_up=1;
	      }
	      else if (R<1)
		energy_ratio_up=-1;		
	      
	      
	      /// Proposal probability
		    R*=proposals[PROPOSAL_INSERT_DEATH]
			//*lambda
			*2*(T)(connections.pool->get_numpts());
		    R/=EPS+proposals[PROPOSAL_INSERT_BIRTH]
			*connection_probability
			*(T)(tree.get_numpts()+1)
			*(maxscale-minscale)
			;//*numvoxel;
		
	      
	      
	      
	      if (R>=myrand(1)+EPS_R)
	      {
			sta_assert_error(!p_point->has_owner());
		
			if (!tree.insert(*p_point))
			{
			    mypool.delete_obj(p_point);
			    vlock.free();  			  
			    p_point->print();
			    printf("error insering point\n");
			    continue;
			}else
			{
			    
			}	
			sta_assert_error(p_point->has_owner());
			
			connections.remove_connection(newconnections[1].connection_old);
 			connections.add_connection(newconnections[0].connection_new,false);
 			connections.add_connection(newconnections[1].connection_new,false);
			
			energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
		
			 if (energy_ratio_up==1)
			  {
// 			    energy_ratio[0]++;
			    proposal_activity[PROPOSAL_INSERT_BIRTH][0]++;
			  }
			  else if (energy_ratio_up==-1)
			  {
			    proposal_activity[PROPOSAL_INSERT_BIRTH][1]++;
// 			    energy_ratio[1]++;
			  }
			  
			  proposal_accept[PROPOSAL_INSERT_BIRTH]++;
			  
			    if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
 			    static_data[center*2*numproposals+17]+=1;
			  }
	      }else
	      {
		mypool.delete_obj(p_point);
	      }
	        vlock.free();
	    } break;
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_INSERT_DEATH
#  
####################################################################
##################################################################*/
	    
	    
	    case PROPOSAL_INSERT_DEATH:
            {
	      bool empty;
	      OctPoints<T,Dim> *  cpoint;
	      
	      ///randomly choose a point
	      cpoint=(tree.get_rand_point(vlock,lock_radius,empty));
	      
	      if (cpoint==NULL)
	      {
		if (!empty)
		  missed++;
		      continue;
	      }
	      sta_assert_error(!empty);
	      
	      proposal_calls[PROPOSAL_INSERT_DEATH]++;
	      
	      Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	      
	      

	      /// check if point has exactly one connection / side
	      if ((point.endpoint_connections[0]!=1)||(point.endpoint_connections[1]!=1))
	      {
		vlock.free(); 
		continue;
	      }		

	      
	      /// the two connected endpoints connecting to point
	      class Points< T, Dim >::CEndpoint * epoints[2];
	      /// the two connected endpoints of point
	      class Points< T, Dim >::CEndpoint * startpoints[2];
	      epoints[0]=epoints[1]=NULL;
	      
	      for (int t2=0;t2<2;t2++)
	      {
		for (int t=0;t<Points<T,Dim>::maxconnections;t++)
		{
		  if (point.endpoints[t2][t].connected!=NULL)
		    startpoints[t2]=&(point.endpoints[t2][t]);
		}
		epoints[t2]=startpoints[t2]->connected;
		sta_assert_error(epoints[t2]!=NULL);
	      }

	      	      
	      /// we only select poins not connected to points which are bifucations
	      if ((epoints[0]->point->get_num_connections()>2)&&(epoints[1]->point->get_num_connections()>2))
	      {
		vlock.free(); 
		continue;
	      }	      
	      
	      /// uniformly pick reference point
	      int choice=std::rand()%2;
	      
	      /// update endpoint pos (not necessary here??)
	      epoints[0]->point->update_endpoint(epoints[0]->side);
	      epoints[1]->point->update_endpoint(epoints[1]->side);
	      

	      /// optimal position
	      Vector<T,Dim> optimal_pos=(*(epoints[0]->position)+*(epoints[1]->position))/2;
	      /// optimal direction
	      Vector<T,Dim> optimal_direction=epoints[choice]->point->direction*Points<T,Dim>::side_sign[epoints[choice]->side];
	      /// optimal scale
	      T optimal_scale=((epoints[0]->point->scale)+(epoints[1]->point->scale))/2;

	      	      
	      //check if point has same direction as reference point
	      if (optimal_direction.dot(point.direction)<0)
		{
		  vlock.free(); 
		   continue;
		}
	      
	      //probability for point parameters
	      T connection_probability=1;
	      
	        T agility=temp_fact_pos*std::sqrt((*(epoints[0]->position)-*(epoints[1]->position)).norm2())/4;
	      
		Vector<T,3> displacement;
		displacement=(point.position-optimal_pos);
	      
		connection_probability*=normal_dist<T,3>(displacement.v,agility);
		
		connection_probability*=normal_dist_sphere(optimal_direction,point.direction,temp_fact_rot);
		
		T scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
		scale_lower=std::max(scale_lower,std::min(epoints[0]->point->scale,epoints[1]->point->scale));
		T scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale);
		
	      		
		if ((point.scale<scale_lower)||(point.scale>scale_upper))
		{
		  vlock.free(); 
		    continue;
		}		
		
		connection_probability*=1.0/(scale_upper-scale_lower+EPS);
		
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
 		  static_data[center*2*numproposals+18]+=1;
		}	
		
		// setting up connections (2 for removal, 1 new)
		TConnecTProposal<T,Dim> newconnections[2];
		newconnections[0].old_connection_exists=true;  
		newconnections[0].connection_old=startpoints[0]->connection;
		newconnections[0].new_connection_exists=false;		
		
		
		newconnections[1].old_connection_exists=true;
		newconnections[1].connection_old=startpoints[1]->connection;
		newconnections[1].new_connection_exists=true;
		newconnections[1].connection_new.pointA=epoints[1];
		newconnections[1].connection_new.pointB=epoints[0];
		newconnections[1].new_prob=1;

		
	      		
		// computing new edge cost
		T edge_cost=0;
		int inrange;
		edge_cost+=newconnections[1].connection_new.compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
		if (inrange<0)
		{
		  vlock.free(); 		      
		  continue;
		}	
	      
	      edge_cost-=newconnections[0].connection_old->compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
	      sta_assert_error(!(inrange<0));
	      edge_cost-=newconnections[1].connection_old->compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
	      sta_assert_error(!(inrange<0));
	      
		// computing new edge cost
	      T E=edge_cost/ConnCostTemp;
	    
	      // computing data Derm
	      T newenergy=point.point_cost;
	      E-=newenergy/Temp;
	      
	      // computing point cost
	      T pointcost=particle_connection_state_cost_scale[2]*point.compute_cost()+(particle_connection_state_cost_offset[2]);////+PointScaleCost*point.compute_cost_scale(); //2 -> 0 connection
 	      E-=pointcost/PointTemp;		    
		   
	    
	      T R=mexp(E);
	      
	      energy_ratio_up=0;
	      if (R>1)
	      {
		energy_ratio_up=1;
	      }
	      else if (R<1)
		energy_ratio_up=-1;
	      
	      R*=proposals[PROPOSAL_INSERT_BIRTH]*
	      connection_probability*
	      (T)(tree.get_numpts())*
	      (maxscale-minscale)
	      ;//*numvoxel;
	      
	      R/=EPS+proposals[PROPOSAL_INSERT_DEATH]*
	      //lambda*
	      2*(T)(connections.pool->get_numpts()-1);
	      
	  	      
	      if (R>=myrand(1)+EPS_R)
	      {      		
			if (energy_ratio_up==1)
			{
			  proposal_activity[PROPOSAL_INSERT_DEATH][0]++;
// 			  energy_ratio[0]++;
			}
			else if (energy_ratio_up==-1)
			{
			  proposal_activity[PROPOSAL_INSERT_DEATH][1]++;
			/*  energy_ratio[1]++;	*/	     
			}
		
			connections.remove_connection(newconnections[0].connection_old);
			connections.remove_connection(newconnections[1].connection_old);
			connections.add_connection(newconnections[1].connection_new,false);
			
			  energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
			  
			  proposal_accept[PROPOSAL_INSERT_DEATH]++;
			  
			  mypool.delete_obj(&point);
			  
			    if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
 			    static_data[center*2*numproposals+19]+=1;
			  }
			  
			  
	      }     	      
	        vlock.free();
	    } break;
	    
	    
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_CONNECT_BIRTH
#  
####################################################################
##################################################################*/
	    
	      
	    case PROPOSAL_CONNECT_BIRTH:
            {
	      
	      /// randomly pic a terminal node
	      if (connections.get_num_terminals()>0)
              {
		Points<T,Dim> * p_point=mypool.create_obj();
		
		proposal_calls[PROPOSAL_CONNECT_BIRTH]++;	
		  
		Points<T,Dim> & point=*p_point;
		point.tracker_birth=1;
		
		Points<T,Dim> &  track_point=connections.get_rand_point();
		
		if (!vlock.lock(track_point.position,lock_radius))
		{
		  mypool.delete_obj(p_point);
		  missed++;
		  continue;
		}
		
		sta_assert_error(track_point.get_num_connections()==1);
 		
	
		
	        // compute optimal pos, orientation and scale
		Vector<T,Dim> optimal_pos;
		Vector<T,Dim> optimal_direction;
		T & optimal_scale=track_point.scale;
		
		int optimal_sideA;
		int optimal_sideB;
		track_point.optimal_sucessor(optimal_pos,optimal_direction,optimal_sideA,optimal_sideB);
		optimal_sideB=1;
		
		/// DEBUG
		{
		  track_point.update_endpoint(optimal_sideA);
		  
		  Vector<T,Dim> tmp;
		  tmp=track_point.endpoints_pos[optimal_sideA]-track_point.position;
		  tmp.normalize();
		  sta_assert_error(optimal_direction.dot(tmp)>0.999);
		  sta_assert_error(track_point.endpoint_connections[optimal_sideA]==0);
		  sta_assert_error(track_point.endpoint_connections[1-optimal_sideA]==1);
		}
		
		T connection_probability=1;
		
		
		T position_sigma=temp_fact_pos*(optimal_scale/2);
		/// NEW POSITION
		Vector<T,Dim> randv;
		randv.rand_normal(position_sigma);
		point.position=optimal_pos+randv;
		
		/// only accept point if before terminal node
		Vector<T,Dim> tmp;
		tmp=randv;
		tmp.normalize();
		if ((optimal_direction.dot(tmp)<0))
		{
		  mypool.delete_obj(p_point);
		  vlock.free(); 
		  continue;		  
		}
		
		if ((point.position[0]<0)||
		  (point.position[1]<0)||
		(point.position[2]<0)||
		  (point.position[0]-1>shape[0])||
		  (point.position[1]-1>shape[1])||
		  (point.position[2]-1>shape[2])
		){
		  mypool.delete_obj(p_point);
		  vlock.free(); 
		  continue;
		}
		connection_probability*=normal_dist<T,3>(randv.v,position_sigma);
		
		
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
		  static_data[center*2*numproposals]+=1;
		}	
		
		/// NEW ORIENTATION
 		random_pic_direction<T,Dim>(
		  optimal_direction,
 		  point.direction,temp_fact_rot);
		/*
		  connection_probability*=normal_dist_sphere(optimal_direction,point.direction,temp_fact_rot);
		  if ((optimal_direction.dot(point.direction)<0))
		  {
		    mypool.delete_obj(p_point);
		    vlock.free(); 
		    continue;		  
		  }
		*/
		connection_probability*=normal_dist_sphere_full(optimal_direction,point.direction,temp_fact_rot);
		// sign same as terminal node
		if ((optimal_direction.dot(point.direction)<0))
		  point.direction*=-1;
		
		/// NEW SCALE
		T scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
		T scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale);
		point.scale=myrand(scale_lower,scale_upper);
		connection_probability*=1/(scale_upper-scale_lower+EPS);
		
		/// Collision check
		if (colliding( tree,point,energy_searchrad))
		{
		  mypool.delete_obj(p_point);
		  vlock.free(); 
		  continue;
		}
		
		/// new connection
		TConnecTProposal<T,Dim> connection;
		connection.old_connection_exists=false;  
		connection.connection_new.pointA=track_point.getfreehub(optimal_sideA);
		connection.connection_new.pointB=point.getfreehub(optimal_sideB);
		sta_assert_error(connection.connection_new.pointA!=NULL);
		sta_assert_error(connection.connection_new.pointB!=NULL);

		connection.new_prob=1;
		connection.new_connection_exists=true;
		
		sta_assert_error(connection.connection_new.pointA!=NULL);
		sta_assert_error(connection.connection_new.pointB!=NULL);
		
		
		T newpointsaliency;
		if (!interp3(newpointsaliency,
		saliency_map.data, 
		point.position,
		shape))
		{
		   mypool.delete_obj(p_point);
			    vlock.free(); 
			      continue;
		}
		
		/// evaluating data term
		T newenergy_data;	
		if (!Dataterm->eval_data(
		      newenergy_data,
// 		      newpointsaliency,
		      point.direction,
		      point.position,
		       point.scale))
			  {
			    mypool.delete_obj(p_point);
			    vlock.free(); 
			      continue;
			    }		
		
		    point.point_cost=newenergy_data;
		    point.saliency=newpointsaliency;

		    /// computing edge cost
		    int inrange;
		    connection.new_cost=connection.connection_new.compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
		    if (inrange<0)
		    {
		      mypool.delete_obj(p_point);
		      vlock.free(); 		      
		      continue;
		    }		    
		    
		    /// edge energy
		    T E=newenergy_data/DataTemp;
		    
		    /// updating point costs
		    T pointcost=particle_connection_state_cost_scale[1]*point.compute_cost()+(particle_connection_state_cost_offset[1])//+PointScaleCost*point.compute_cost_scale() //0 -> 1 connection
				+(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])*track_point.compute_cost()+(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]); //1 -> 2 connection
		    
		    E+=(pointcost)/PointTemp;
		    
		    /// edge costs
		    T edge_cost=connection.new_cost;
		    E+=(edge_cost)/ConnCostTemp;
		    
		    
		    T R=mexp(E);
		    
		    energy_ratio_up=0;
		    if (R>1)
		    {
		      energy_ratio_up=1;
		    }
		    else if (R<1)
		      energy_ratio_up=-1;		    
		    


		    
		    
// 		    int num_undo_candidates=connections.get_num_terminals(1);
// 		    if (track_point.terminal_indx[1]<0)
// 		      num_undo_candidates++;
		    
		    R*=proposals[PROPOSAL_CONNECT_DEATH];
			//*lambda
// 			*(T)connections.get_num_terminals(0);
		    R/=EPS+proposals[PROPOSAL_CONNECT_BIRTH]
			*connection_probability
// 			*(T)num_undo_candidates
			*(maxscale-minscale)
			;//*numvoxel;
		    

		    

 
		    if (R>=myrand(1)+EPS_R)
                    {
      
		      


			sta_assert_error(!p_point->has_owner());
		
			if (!tree.insert(*p_point))
			{
			    mypool.delete_obj(p_point);
			    vlock.free();  			  
			    p_point->print();
			    printf("error insering point\n");
			    continue;
			}else
			{
			    
			}	
			sta_assert_error(p_point->has_owner());
		      
		      sta_assert_error((connection.new_connection_exists));
		      
  		      if (connection.new_connection_exists)
  			sta_assert_error(connections.add_connection(connection.connection_new,false));
			
		      energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
	      
		      if (energy_ratio_up==1)
		      {
// 			energy_ratio[0]++;
			proposal_activity[PROPOSAL_CONNECT_BIRTH][0]++;
		      }
		      else if (energy_ratio_up==-1)
		      {
			proposal_activity[PROPOSAL_CONNECT_BIRTH][1]++;
// 			energy_ratio[1]++;
		      }
		      
		      proposal_accept[PROPOSAL_CONNECT_BIRTH]++;
		      
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+1]+=1;
			  }
			  
/*		      sta_assert_error(!(optimal_direction.dot(point.direction)<0));
		      sta_assert_error(track_point.endpoint_connections[0]==1);
		      sta_assert_error(track_point.endpoint_connections[1]==1);	*/		  
			  
                    }
                    else
		    {
		     mypool.delete_obj(p_point);
		
		    }
		      //delete p_point;
		
		    vlock.free();
		}
		 
	    } break;	      
	      
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_CONNECT_DEATH
#  
####################################################################
##################################################################*/
	    
	      
	    case PROPOSAL_CONNECT_DEATH:
            {
	      
	      /// uniformly select terminal, where after removal remains a terminal
	      if (connections.get_num_terminals()>0)
              {
		
		Points<T,Dim> &  point=connections.get_rand_point();
		
		if (!vlock.lock(point.position,lock_radius))
		{
		  missed++;
		  continue;
		}
		
		proposal_calls[PROPOSAL_CONNECT_DEATH]++;
				    
 		sta_assert_error(point.get_num_connections()==1);
		sta_assert(point.get_num_connections()==1);
		
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
		  static_data[center*2*numproposals+2]+=1;
		}		
		
		
		/// find the connected edge
		class Connection<T,Dim> * connection;
		int found=-1;
		
		Points<T,Dim> *  new_terminal=NULL;
		
		for (int s=0;s<2;s++)
		for (int c=0;c<Points<T,Dim>::maxconnections;c++)
		{
		  if (point.endpoints[s][c].connected!=NULL)
		  {
		    connection=point.endpoints[s][c].connection;
		    new_terminal=point.endpoints[s][c].connected->point;
		    found=point.endpoints[s][c].connected->side;
		    break;
		  }
		}
		if ((new_terminal->endpoint_connections[0]!=1)||
		  (new_terminal->endpoint_connections[1]!=1))
		{
		  vlock.free(); 
		  continue;
		}
		

		
	      
	      sta_assert_error(found!=-1);

	      /// computing edge cost
	      int inrange;
	      T edge_cost=connection->compute_cost(*edgecost_fun,L,maxendpointdist,inrange);
	      sta_assert_error(inrange>0);
		
		
	      /// computing optimal pos, scale, orientation
	      Vector<T,Dim> optimal_pos;
	      Vector<T,Dim> optimal_direction;
	      int optimal_sideA;
	      int optimal_sideB;
	      new_terminal->optimal_sucessor(
		optimal_pos,
		optimal_direction,
		optimal_sideA,
		optimal_sideB,
		found);
	      T optimal_scale=new_terminal->scale;

		
	      
	      T position_sigma=temp_fact_pos*optimal_scale/2;
	      
	      T connection_probability=1;
	      
	      Vector<T,3> displacement;
	      displacement=(point.position-optimal_pos);
	      connection_probability*=normal_dist<T,3>(displacement.v,position_sigma);
	      
		/// check if old terminal is in front of new terminal
		Vector<T,3> tmp=(point.position-new_terminal->position);
		tmp.normalize();
		if (optimal_direction.dot(tmp)<0)
		{
		  vlock.free(); 
		  continue;		  
		}	      

		/// make sure that direction vector points in direction of not-connected endpoint
	      Vector<T,3> direction=point.direction;
	      if (point.endpoint_connections[1]==0)
		direction*=-1;
	      	
	      /// this could not have been created via birth-track
	      if ((optimal_direction.dot(direction)<0))
		{
		  vlock.free(); 
		  continue;		  
		}	
	      
	      connection_probability*=normal_dist_sphere_full(optimal_direction,direction,temp_fact_rot);
	      //connection_probability*=normal_dist_sphere(optimal_direction,direction,temp_fact_rot);
	      
	      
	      T scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
	      T scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale);
	      
	      if ((point.scale<scale_lower)||(point.scale>scale_upper))
	      {
		vlock.free(); 
		  continue;
	      }
	      
	      connection_probability*=1.0/(scale_upper-scale_lower+EPS);
	      
	      
	      
		  
	      T E=-edge_cost/ConnCostTemp;
	    
	      T newenergy=point.point_cost;
	      E-=newenergy/Temp;
	      
	      
	       T pointcost=particle_connection_state_cost_scale[1]*point.compute_cost()+(particle_connection_state_cost_offset[1])//+PointScaleCost*point.compute_cost_scale() //0 -> 1 connection
				+(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])*new_terminal->compute_cost()+(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]); //1 -> 2
	      E-=pointcost/PointTemp;		    
		   
		    
	      T R=mexp(E);
	      
		energy_ratio_up=0;
	      if (R>1)
		energy_ratio_up=1;
	      else if (R<1)
		energy_ratio_up=-1;			    


		    R*=proposals[PROPOSAL_CONNECT_BIRTH]
			*connection_probability
// 			*(T)connections.get_num_terminals(1)
			*(maxscale-minscale)
			;//*numvoxel;
		    R/=EPS+proposals[PROPOSAL_CONNECT_DEATH];
		    //*lambda
// 		    *(T)connections.get_num_terminals(0);
	    
	      
		if (R>=myrand(1)+EPS_R)
		{
		  
		  energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
		  
		    if (energy_ratio_up==1)
		    {
		      proposal_activity[PROPOSAL_CONNECT_DEATH][0]++;
// 		      energy_ratio[0]++;
		    }
		    else if (energy_ratio_up==-1)
		    {
		      proposal_activity[PROPOSAL_CONNECT_DEATH][1]++;
		    /*  energy_ratio[1]++;*/		     
		    }
		     connections.remove_connection(connection);		    
		    
		    proposal_accept[PROPOSAL_CONNECT_DEATH]++;
		    //delete &point;
		     mypool.delete_obj(&point);
		     
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+3]+=1;
			  }		     
		}
		
		      vlock.free();
		}
		 
	    } break;	  
	      
	      
/*##################################################################  
####################################################################
#  
#		PROPOSAL_BIRTH
#  
####################################################################
##################################################################*/	      
	      
	      
	    case PROPOSAL_BIRTH:
            {
	     
	        
	        
                T pos[3];
                std::size_t center;

		Points<T,Dim> * point=mypool.create_obj();
		Points<T,Dim> & cpoint=*point;
		
if (use_saliency_map)				
{
		T v= rand_pic_position(saliency_map_acc.data,shape,pos,center,1,0,true);
                cpoint.setpoint(pos[0],pos[1],pos[2]);
		
}		
else
{
		Vector<T,Dim> randv;
		T maxect[3];
		maxect[0]=shape[0]-1;
		maxect[1]=shape[1]-1;
		maxect[2]=shape[2]-1;
		T maxect_min[3];
		maxect_min[0]=maxect_min[1]=maxect_min[2]=0;
		randv.rand(maxect_min,maxect);
		cpoint.setpoint(randv[0],randv[1],randv[2]);
		center=(std::floor(cpoint.position[0])*shape[1]
 				+std::floor(cpoint.position[1]))*shape[2]
 				+std::floor(cpoint.position[2]);
}				
				
		

		if (!vlock.lock(cpoint.position,lock_radius))
		{
		  mypool.delete_obj(point);
		  missed++;
		  continue;
		}		
		
		if (return_statistic)	
		{
		  static_data[center*2*numproposals+4]+=1;
		}		
		
		proposal_calls[PROPOSAL_BIRTH]++;

		// uniformly pick scale
		T new_scale=myrand(maxscale-minscale)+minscale;
		cpoint.scale=new_scale;
		
		
		// orientation
		do {
		cpoint.direction=T(0);
		random_pic_direction<T,Dim>(
		  cpoint.direction,
		  cpoint.direction,
		   T(1));
		} while (cpoint.direction[0]+cpoint.direction[1]+cpoint.direction[2]==0);

		if (colliding( tree,cpoint,energy_searchrad))
		{
		  mypool.delete_obj(point);
		  vlock.free(); 
		  continue;
		}
		
		
		T newpointsaliency;
		if (!interp3(newpointsaliency,
		saliency_map.data, 
		cpoint.position,
		shape))
		{
		   mypool.delete_obj(point);
			    vlock.free(); 
			      continue;
		}
		// data term
		T newenergy_data;
// 		T newpointsaliency;
		if (!Dataterm->eval_data(
		      newenergy_data,
// 		      newpointsaliency,
		      cpoint.direction,
		      cpoint.position,
		       cpoint.scale))
			  {
			    mypool.delete_obj(point);
			    vlock.free(); 
			      continue;
			    }		
		
		cpoint.point_cost=newenergy_data;
		cpoint.saliency=newpointsaliency;

		
		T E=newenergy_data/DataTemp;
		
		
		// cost for non-connected point
		T pointcost=particle_connection_state_cost_scale[0]*cpoint.compute_cost()+(particle_connection_state_cost_offset[0]);//+PointScaleCost*cpoint.compute_cost_scale();
		E+=(pointcost)/PointTemp;
		
		
                T R=mexp(E);

		if (R>1)
		  energy_ratio_up=1;
		else
		  energy_ratio_up=-1;
// T debugR=R;
/*R=1;*/		
		
		if (randwalk)
		{
		  R*=proposals[PROPOSAL_DEATH]/(EPS+proposals[PROPOSAL_BIRTH]);
		}else
		{
		  //lambda=bin/(1/V)  bin=lambda/V  /// uniform case
		  //bin/prob  /// saliency map case
		  if (use_saliency_map)
		  {
		    R*=proposals[PROPOSAL_DEATH]*(lambda);
		    R/=EPS+proposals[PROPOSAL_BIRTH]*(tree.get_numpts()+1)*(numvoxel*cpoint.saliency);//*cpoint.saliency;
		  }else
		  {
		    R*=proposals[PROPOSAL_DEATH]*(lambda);
		    R/=EPS+proposals[PROPOSAL_BIRTH]*(tree.get_numpts()+1);//*cpoint.saliency;
		  }		    
		}
		
		
		

		
                if (R>=myrand(1)+EPS_R)
                {
		  energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
	  
		
// 		  printf("R: %f, %f\n",debugR,R);
		  
		    if (energy_ratio_up==1)
		    {
		      proposal_activity[PROPOSAL_BIRTH][0]++;
// 		      energy_ratio[0]++;
		    }
		    else
		    {
		      proposal_activity[PROPOSAL_BIRTH][1]++;
// 		      energy_ratio[1]++;
		    }
                    proposal_accept[PROPOSAL_BIRTH]++;
		     
                    if (!tree.insert(*point))
                    {
		      mypool.delete_obj(point);
                        point->print();
                        printf("error insering point\n");
                    }else
		    {
		      
		    }
			  if (return_statistic)	
			  {
			    static_data[center*2*numproposals+5]+=1;
			  }
                }else
		{
		      mypool.delete_obj(point);  
		}
                vlock.free(); 
		  
            }break;
	    
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_DEATH
#  
####################################################################
##################################################################*/	      
	    
	    
	    
            case PROPOSAL_DEATH:
            {
                //if (tree.get_numpts()>0)
                {
		    		  

		    
		    OctPoints<T,Dim> *  cpoint;
		    bool empty;
// 		    if (free_points_num<1)
// 		      continue;
		 /*   
		    if (false)
		    {*/
		      cpoint=(tree.get_rand_point(vlock,lock_radius,empty));
// 		    }else
// 		    { 
// 		      cpoint=(OctPoints<T,Dim> *)free_points[std::rand()%free_points_num];
// 		      empty=false;
// 		    }
		    
		    if (cpoint==NULL)
		    {
		      if (!empty)
		       missed++;
			    continue;
		    }
		    sta_assert_error(!empty);
// 		    sta_assert_error(cpoint->exist);
		    
                    proposal_calls[PROPOSAL_DEATH]++;
		    
		    Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
		    

		    
		    
		    if (point.isconnected())
		    {
		      vlock.free(); 
		      continue;
		    }
		    
// 		    sta_assert_error(mhs_maxpoints+1!=point.free_points_indx);
		    
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+6]+=1;
			  }		    

		    T newenergy=point.point_cost;

		    T E=-newenergy/Temp;
		    
		    T pointcost=particle_connection_state_cost_scale[0]*point.compute_cost()+(particle_connection_state_cost_offset[0]);////+PointScaleCost*point.compute_cost_scale();
		    E-=pointcost/PointTemp;		    
		    
		    T R=mexp(E);
		    
		    energy_ratio_up=0;
		    if (R>1)
		      energy_ratio_up=1;
		    else if (R<1)
		      energy_ratio_up=-1;	


/*R=1;	*/	
//  R=1;
//  std::size_t center=(std::floor(point.position[0])*shape[1]
// 				+std::floor(point.position[1]))*shape[2]
// 				+std::floor(point.position[2]);
//  point.saliency=saliency_map.data[center];
		    		
/*R=1;	*/	    		
		    		
		    if (randwalk)
		    {
		      energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
		      
		      R*=proposals[PROPOSAL_BIRTH]/(EPS+proposals[PROPOSAL_DEATH]);
		      
// 		      R*=proposals[PROPOSAL_BIRTH];
// 		      R/=EPS+proposals[PROPOSAL_DEATH]/(free_points_num+1);
		    }else
		    {
// 		      R*=proposals[PROPOSAL_BIRTH]/(lambda);
// 		      R/=EPS+proposals[PROPOSAL_DEATH]/(tree.get_numpts());
		      
		      if (use_saliency_map)
		      {
			R*=proposals[PROPOSAL_BIRTH]*(tree.get_numpts())*(numvoxel*point.saliency);
			R/=EPS+proposals[PROPOSAL_DEATH]*(lambda);//*(1-point.saliency);		      
		      }else
		      {
			R*=proposals[PROPOSAL_BIRTH]*(tree.get_numpts());//*point.saliency;;//
			R/=EPS+proposals[PROPOSAL_DEATH]*(lambda);//*(1-point.saliency);		      
		      }
		      
		      
		      
		      
		    }		    

                    if (R>=myrand(1)+EPS_R)
                    {

			if (energy_ratio_up==1)
			{
			  proposal_activity[PROPOSAL_DEATH][0]++;
// 			  energy_ratio[0]++;
			}
			else if (energy_ratio_up==-1)
			{
			  proposal_activity[PROPOSAL_DEATH][1]++;
		/*	  energy_ratio[1]++;	*/	     
			}
			 
                        proposal_accept[PROPOSAL_DEATH]++;


			mypool.delete_obj((Points<T,Dim> *)cpoint);
			
// 			printf("adter: %d\n",mypool.get_numpts());
			
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+7]+=1;
			  }
			  
 			
			
                    }
                }
                vlock.free(); 
            }
            break;
	    
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_SCALE
#  
####################################################################
##################################################################*/	      

	    
	    
	     case PROPOSAL_SCALE:
            {
                //if (tree.get_numpts()>0)
                {
		    OctPoints<T,Dim> *  cpoint;
		    bool empty;
		    
   		    cpoint=(tree.get_rand_point(vlock,lock_radius,empty));
		    if (cpoint==NULL)
		    {
		      if (!empty)
		       missed++;
			    continue;
		    }
		    sta_assert_error(!empty);
		  
	  
		  
		    proposal_calls[PROPOSAL_SCALE]++;
		  
                    //OctPoints<T,Dim> &  cpoint=tree.get_rand_point();
		    Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
		    		    
		    std::size_t center=(std::floor(point.position[0])*shape[1]
				      +std::floor(point.position[1]))*shape[2]
				      +std::floor(point.position[2]);
		    
			  if (return_statistic)	
			  {
			    static_data[center*2*numproposals+8]+=1;
			  }					      
				      
		     // edge cost
		    int inrange;
		    
		    
		    T old_cost=edge_energy(*edgecost_fun,point,L,maxendpointdist,inrange);
		    //T old_cost=edge_energy(point,L,ConnectionScale,Connectionparticle_connection_state_cost_scale,ThicknessScale,AngleScale,maxendpointdist,inrange);
		    sta_assert_error(inrange>0);
		    
// 		    if (edge_length_energy)
// 			old_cost+=edge_point_energy(point);
		    
		    // collision cost (currently alwasys zeros since we have a hard collision func)
		    //T oldenergy=get_energy( tree,cpoint,energy_searchrad);
		    // collision cost
		    T oldenergy_data=point.point_cost;
		    
		    T old_scale=point.scale;
		    

		    int connection_state=0;
		    {
		      bool check_connect[2];
		      check_connect[0]=(point.endpoint_connections[0]>0);
		      check_connect[1]=(point.endpoint_connections[1]>0);
		      if (check_connect[0]&&check_connect[1])
			connection_state=2;
		      else if (check_connect[0]||check_connect[1])
			connection_state=1;
		    }
		    
		    //TODO hier ist was faul!! vermisse weights!!!
		    T old_pointcost=point.compute_cost();////+PointScaleCost*point.compute_cost_scale();
		    
    
		    
   		      T scale_lower=std::max(minscale,old_scale/temp_fact_scale);
   		      T scale_upper=std::min(maxscale,old_scale*temp_fact_scale);
 		      T new_scale=myrand(scale_lower,scale_upper);
		      
		      T scale_lower_new=std::max(minscale,new_scale/temp_fact_scale);
  		      T scale_upper_new=std::min(maxscale,new_scale*temp_fact_scale);
  		      T probability=(scale_upper-scale_lower)/(scale_upper_new-scale_lower_new);
		      //probability=1/probability;
		      sta_assert_error(probability>=0);
		      
		      
		      
		      if (std::abs(new_scale-point.scale)<0.01) 
		      {
			vlock.free(); 
			continue;
		      }
		      point.scale=new_scale;
		    

		    // collision cost
		    if (colliding( tree,point,energy_searchrad))
		    {
		       point.scale=old_scale;
		       vlock.free(); 
		      continue;
		    }

		    // edge cost
		    T new_cost=edge_energy(*edgecost_fun,point,L,maxendpointdist,inrange);
 		    //T new_cost=edge_energy(point,L,ConnectionScale,Connectionparticle_connection_state_cost_scale,ThicknessScale,AngleScale,maxendpointdist,inrange);
		    
		    if (inrange<0)
		    {
		       point.scale=old_scale;
		       vlock.free(); 
		      continue;
		    }
		    
/*		    if (edge_length_energy)
			new_cost+=edge_point_energy(point);	*/	    
		
		    
		T newenergy_data;
// 		T newpointsaliency;
		if (!Dataterm->eval_data(
		      newenergy_data,
// 		      newpointsaliency,
		      point.direction,
		      point.position,
		       point.scale))
			  {
			     point.scale=old_scale; 
			    vlock.free(); 
			      continue;
			    }	
			    
		    T E=(new_cost-old_cost)/ConnCostTemp;	
		    E+=(newenergy_data-oldenergy_data)/DataTemp;	

		    
		    //TODO hier ist was faul!! vermisse weights!!!
		    T new_pointcost=point.compute_cost();////+PointScaleCost*point.compute_cost_scale();
		    
		    E+=particle_connection_state_cost_scale[connection_state]*(new_pointcost-old_pointcost)/PointTemp;		  
		    
		    
		    T R=mexp(E);
		    
		    energy_ratio_up=0;
		    if (R>1)
		    {
		      energy_ratio_up=1;
		    }
		    else
		    {
		      energy_ratio_up=-1;		    
		    }
		    
		    R*=probability;
		    
                    if (R>=myrand(1)+EPS_R)
                    {
		      
		      energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
	      
		    if (energy_ratio_up==1)
		    {
		      proposal_activity[PROPOSAL_SCALE][0]++;
// 		      energy_ratio[0]++;
		    }
		    else if (energy_ratio_up==-1)
		    {
		      proposal_activity[PROPOSAL_SCALE][1]++;
		   /*   energy_ratio[1]++;*/		      
		    }
		      
			point.point_cost=newenergy_data;
                        proposal_accept[PROPOSAL_SCALE]++;
			
			  if (return_statistic)	
			  {
			    static_data[center*2*numproposals+9]+=1;
			  }				
                    }else
		    {
		       point.scale=old_scale;
		    }
                }
                vlock.free(); 
            }
            break;
	    
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_ROTATE
#  
####################################################################
##################################################################*/	      
	    
	    
	    case PROPOSAL_ROTATE:
            {
                //if (tree.get_numpts()>0)
                {
		   OctPoints<T,Dim> *  cpoint;
		    bool empty;
		    
   		    cpoint=(tree.get_rand_point(vlock,lock_radius,empty));
		    if (cpoint==NULL)
		    {
		      if (!empty)
		       missed++;
			    continue;
		    }
		    sta_assert_error(!empty);
		  
		  
		  
		    proposal_calls[PROPOSAL_ROTATE]++;		  

                    //OctPoints<T,Dim> &  cpoint=tree.get_rand_point();
		    Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
		    
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+10]+=1;
			  }			    
		    
		    int inrange;
		    T old_cost=edge_energy(*edgecost_fun,point,L,maxendpointdist,inrange);
		    //T old_cost=edge_energy(point,L,ConnectionScale,Connectionparticle_connection_state_cost_scale,ThicknessScale,AngleScale,maxendpointdist,inrange);
		    sta_assert_error(inrange>0);
		    
// 		    if (edge_length_energy)
// 			old_cost+=edge_point_energy(point);
		    
		    
		    T oldenergy_data=point.point_cost;
		    
		    
		    Vector<T,Dim> old_dir=point.direction;
		    
		    random_pic_direction<T,Dim>(
		      point.direction,
		      point.direction,
		      temp_fact_rot);
		      //std::max(2*Temp,T(0.25)));
		    
		  if (colliding( tree,point,energy_searchrad))
		  {
		      point.direction=old_dir; 
		      vlock.free(); 
		    continue;
		  }  
		    
		  T new_cost=edge_energy(*edgecost_fun,point,L,maxendpointdist,inrange);  
 		  //T new_cost=edge_energy(point,L,ConnectionScale,Connectionparticle_connection_state_cost_scale,ThicknessScale,AngleScale,maxendpointdist,inrange);
		  if (inrange<0)
		  {
		     point.direction=old_dir; 
		     vlock.free(); 
				continue;
		  }
		    
		    
/*		  if (edge_length_energy)
			new_cost+=edge_point_energy(point);*/		    

		  
		T newenergy_data;
// 		T newpointsaliency;
		if (!Dataterm->eval_data(
		      newenergy_data,
// 		      newpointsaliency,
		      point.direction,
		      point.position,
		       point.scale))
			  {
			     point.direction=old_dir;
			    vlock.free(); 
			      continue;
			    }			  
		    
		    T E=(new_cost-old_cost)/ConnCostTemp;	
		    E+=(newenergy_data-oldenergy_data)/DataTemp;
		    
		    T R=mexp(E);
		    	    
		    
		    energy_ratio_up=0;
		    if (R>1)
		      energy_ratio_up=1;
		    else if (R<1)
		      energy_ratio_up=-1;		
		    
                    if (R>=myrand(1)+EPS_R)
                    {
		      energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
	      
		      if (energy_ratio_up==1)
		      {
			proposal_activity[PROPOSAL_ROTATE][0]++;
// 			  energy_ratio[0]++;
		      }
			else if (energy_ratio_up==-1)
			{
			  proposal_activity[PROPOSAL_ROTATE][1]++;
			 /* energy_ratio[1]++;*/			      
			}

			point.point_cost=newenergy_data;
// 			point.saliency=newpointsaliency;
		      
                        proposal_accept[PROPOSAL_ROTATE]++;
		      
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+11]+=1;
			  }			      
                    }else
		    {
		     point.direction=old_dir; 
		    }
                }
                vlock.free(); 
            }
            break;
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_MOVE
#  
####################################################################
##################################################################*/	      
	    
	    
	    
	    case PROPOSAL_MOVE:
            {
                //if (tree.get_numpts()>0)
                {
		   OctPoints<T,Dim> *  cpoint;
		    bool empty;
		    
   		    cpoint=(tree.get_rand_point(vlock,lock_radius,empty));
		    if (cpoint==NULL)
		    {
		      if (!empty)
		       missed++;
			    continue;
		    }
		    sta_assert_error(!empty);
		    
		    proposal_calls[PROPOSAL_MOVE]++;		  
		  
                    //OctPoints<T,Dim> &  cpoint=tree.get_rand_point();
		    Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
		  
		    
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+12]+=1;
			  }			    
		    
		    Vector<T,Dim> new_pos;
		    
		    if (Points<T,Dim>::thickness<0)
		      new_pos.rand_normal(-temp_fact_pos*point.scale*Points<T,Dim>::thickness);
		    else 
		      new_pos.rand_normal(temp_fact_pos*Points<T,Dim>::thickness);		    
		    
		    new_pos+=point.position;//+Vector<T,Dim>(T(myrand(2)-1),T(myrand(2)-1),T(myrand(2)-1));		    
		    
		    
		   if ((new_pos[0]<0)||(new_pos[1]<0)||(new_pos[2]<0)||
		      (new_pos[0]>shape[0]-1)||(new_pos[1]>shape[1]-1)||(new_pos[2]>shape[2]-1))
		   {
		     vlock.free(); 
		     continue;
		   }
		      
		      
		  
		   if ((point.position-new_pos).norm2()<0.001)
		   {
		     vlock.free(); 
		      continue;
		   }
		   

		    int inrange;
		    // edge cost
		    T old_cost=edge_energy(*edgecost_fun,point,L,maxendpointdist,inrange);
		    //T old_cost=edge_energy(point,L,ConnectionScale,Connectionparticle_connection_state_cost_scale,ThicknessScale,AngleScale,maxendpointdist,inrange);
		   sta_assert_error(inrange>0);
		   
		    
		    T oldenergy_data=point.point_cost;

			Vector<T,Dim> old_pos=point.position;
			
			
			//T old_scale=point.scale;
			point.position=new_pos;
			point.update_pos();
			

		

		    // collision cost
		    if (colliding( tree,point,energy_searchrad))
		    {
		      point.position=old_pos;
		      point.update_pos();
		      vlock.free(); 
		      continue;
		    }
		   		    
		
		T newpointsaliency;
		if (!interp3(newpointsaliency,
		saliency_map.data, 
		point.position,
		shape))
		{
			      point.position=old_pos;
			      point.update_pos();
			      continue;
		}
		
		T newenergy_data;
// 		T newpointsaliency;
		if (!Dataterm->eval_data(
		      newenergy_data,
// 		      newpointsaliency,
		      point.direction,
		      point.position,
		       point.scale))
			  {
				  point.position=old_pos;
				  point.update_pos();
			      continue;
			    }

				
		    T old_saliency=point.saliency;
		    point.saliency=newpointsaliency;
		    // edge cost			
		    T new_cost=edge_energy(*edgecost_fun,point,L,maxendpointdist,inrange);
 		    //T new_cost=edge_energy(point,L,ConnectionScale,Connectionparticle_connection_state_cost_scale,ThicknessScale,AngleScale,maxendpointdist,inrange);
		    if (inrange<0)
		    {
		      point.saliency=old_saliency;
		      point.position=old_pos;
		      point.update_pos();
		      vlock.free(); 
		      continue;		      
		    }
		    
		    
		    T E=(new_cost-old_cost)/ConnCostTemp;	
		    E+=(newenergy_data-oldenergy_data)/DataTemp;	

		    T R=mexp(E);
		    
		    energy_ratio_up=0;
		    if (R>1)
		      energy_ratio_up=1;
		    else if (R<1)
		      energy_ratio_up=-1;
		
                    if (R>=myrand(1)+EPS_R)
                    {
		      
		      energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
		      
			if (energy_ratio_up==1)
			{
			  proposal_activity[PROPOSAL_MOVE][0]++;
// 			  energy_ratio[0]++;
			}
			else if (energy_ratio_up==-1)
			{
			  proposal_activity[PROPOSAL_MOVE][1]++;
// 			  energy_ratio[1]++;	
			}

			point.point_cost=newenergy_data;
// 			point.saliency=newpointsaliency;

			proposal_accept[PROPOSAL_MOVE]++;
			
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+13]+=1;
			  }				
                    }else
		    {
		     point.saliency=old_saliency;
		     point.position=old_pos; 
		     point.update_pos();
		    }
                }
                vlock.free(); 
            }
            break;		
	    
	    
/*##################################################################  
####################################################################
#  
#		PROPOSAL_CONNECT
#  
####################################################################
##################################################################*/	    
	    
	    case PROPOSAL_CONNECT:
            {
                //if (tree.get_numpts()>0)
                {
		  
		  OctPoints<T,Dim> *  cpoint;
		  bool empty;
		  
		  cpoint=(tree.get_rand_point(vlock,lock_radius,empty));
		  if (cpoint==NULL)
		  {
		    if (!empty)
		      missed++;
			  continue;
		  }
		  sta_assert_error(!empty); 
		    
		 proposal_calls[PROPOSAL_CONNECT]++;		  
		  
		 Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
		 
		     
		    
		 
		 
		 
		  if (return_statistic)	
		  {
		      std::size_t center=(std::floor(point.position[0])*shape[1]
			+std::floor(point.position[1]))*shape[2]
			+std::floor(point.position[2]);
		    static_data[center*2*numproposals+14]+=1;
		  }			 
		 
		 TConnecTProposal<T,Dim> connection;
		 
		 T search_radius=search_connection_point_candidate;
// 		 if (searchrad_scales)
// 		  search_radius*=maxscale/point.scale;
		 

		 if (!do_tracking3<T,Dim>(
				*edgecost_fun,
				connection,
				tree,
			        point,
				search_connection_point_center_candidate,
			        search_radius,
			        ConnTemp,
				Lprior,
				L
// 				,
// 				ConnectionScale,
// 				Connectionparticle_connection_state_cost_scale,
// 				ThicknessScale,
// 				AngleScale
					 ))
		 {
		   vlock.free(); 
		   continue;
		 }
		 
    
		  
		  T old_prob=connection.old_prob;
		  T new_prob=connection.new_prob;
		  
		  T old_cost=connection.old_cost;
		  T new_cost=connection.new_cost;

		  T E=(new_cost-old_cost)/ConnCostTemp;
		  
		 
		 int num_involved_pts=0;
		 class Points< T, Dim > * involved_pt_set[3];
		 Connection<T,Dim>    connection_old;
		 Connection<T,Dim>   *connection_new=NULL;
		 
		 involved_pt_set[num_involved_pts++]=&point;
		 
		 if (connection.old_connection_exists)
		 {
		   connection_old=*connection.connection_old;
		   if (connection_old.pointA->point!=&point)
		     std::swap(connection_old.pointA,connection_old.pointB);
		   sta_assert_error(connection_old.pointA->point==&point);
		   involved_pt_set[num_involved_pts++]=connection_old.pointB->point;
		 }
		 if (connection.new_connection_exists)
		  {
		   if  ((!connection.old_connection_exists)
		     ||(connection_old.pointB->point!=connection.connection_new.pointB->point))
		    involved_pt_set[num_involved_pts++]=connection.connection_new.pointB->point;  
		  }
		 
		 
		 T involved_pt_weights[3];
		 T involved_pt_offsets[3];
		 for (int i=0;i<num_involved_pts;i++)
		 {
		   int pindx=involved_pt_set[i]->get_parity();
		   involved_pt_weights[i]=-particle_connection_state_cost_scale[pindx];
		   involved_pt_offsets[i]=-particle_connection_state_cost_offset[pindx];
		 }

		  

		 if (connection.old_connection_exists)
		  connections.remove_connection(connection.connection_old);

		 if (connection.new_connection_exists) 
		 {
		    connection_new=connections.add_connection(connection.connection_new,false);
		    sta_assert_error(connection_new!=NULL);
		 }
	 
		 for (int i=0;i<num_involved_pts;i++)
		 {
		   int pindx=involved_pt_set[i]->get_parity();
		   E+=((involved_pt_weights[i]+particle_connection_state_cost_scale[pindx])*
		   involved_pt_set[i]->compute_cost()+
		     (involved_pt_offsets[i]+particle_connection_state_cost_offset[pindx])
		  )/PointTemp;
		 }		 
		 
		T R=exp(-E);
		    
		energy_ratio_up=0;
		if (R>1)
		  energy_ratio_up=1;
		else if (R<1)
		  energy_ratio_up=-1;		    
		    
		    
		R*=old_prob/((new_prob)+EPS);

		    if (R>=myrand(1)+EPS_R)
                    {
			// checking constraints
			if (connection.new_connection_exists)
			{
			  if (Constraints::constraint_hasloop(point,connection_new,loop_depth)||
			     (constraint_isdirectedge&&Constraints::constraint_isdirectedge(connection_new)))
			  {
			    connections.remove_connection(connection_new);
			    if (connection.old_connection_exists)
				connections.add_connection(connection_old,false);
			    vlock.free();
			    continue;	
			  }
			}
		      energy_grad=opt_alpha*energy_grad+(1-opt_alpha)*E;
	      
			  if (energy_ratio_up==1)
			  {
			    proposal_activity[PROPOSAL_CONNECT][0]++;
// 			    energy_ratio[0]++;
			  }
			  else if (energy_ratio_up==-1)
			  {
			    proposal_activity[PROPOSAL_CONNECT][1]++;
// 			    energy_ratio[1]++;	
			  }
			  proposal_accept[PROPOSAL_CONNECT]++;
		      
			  if (return_statistic)	
			  {
			      std::size_t center=(std::floor(point.position[0])*shape[1]
				+std::floor(point.position[1]))*shape[2]
				+std::floor(point.position[2]);
			    static_data[center*2*numproposals+15]+=1;
			  }			      
                    }else
		    {
			    if (connection.new_connection_exists) 
			    connections.remove_connection(connection_new);
			    if (connection.old_connection_exists)
				connections.add_connection(connection_old,false);
			    vlock.free();
			     continue;
		    }
		}
		vlock.free(); 
	    } break;


            default:
                printf("proposal does not exist\n");
            }



       
}

if (debuginfosize>0)
{
  printf("\n-------------\n");
for (int i=0;i<debuginfosize;i++)
  printf("(%d) %f \n",i,debug_info[i]);
printf("\n-------------\n");
}

	

	iteration_time=timer.get_total_runtime();
	
	printf("iteration time: %u seconds\n",(unsigned int)(std::ceil(iteration_time)));
        
	
	
	
	printf("\n\n------------------------------------\n");
        printf("proposal acceptance : \n");
	

/*	
  PROPOSAL_BIRTH=0,
    PROPOSAL_DEATH=1,
    PROPOSAL_ROTATE=2,
    PROPOSAL_CONNECT=3,
    PROPOSAL_MOVE=4,
    PROPOSAL_SCALE=5,
    PROPOSAL_CONNECT_BIRTH=6,
    PROPOSAL_CONNECT_DEATH=7,
    PROPOSAL_INSERT_BIRTH=8,
    PROPOSAL_INSERT_DEATH=9,    
    PROPOSAL_BIF_BIRTH=10,
    PROPOSAL_BIF_DEATH=11	*/
	
	
        for (int i=0; i<numproposals; i++)
        {
	 
            printf("proposal %s, calls %10d, accept %10d / %3d\%\n",
		   enum2string(i).c_str(),
		   proposal_calls[i],
		   proposal_accept[i],
		   (int)std::ceil(100*double(proposal_accept[i])/(std::numeric_limits<T>::epsilon()+double(proposal_calls[i])))
		  );
        }
	 
        printf("------------------------------------\n");

	
// 	for (std::size_t a=0;a<free_points_num;a++)
// 	{
// 	  free_points[a]->free_points_indx=mhs_maxpoints+10;
// 	}

    } catch (mhs::STAError & error)
    {
        if (error.getCode()==mhs::STA_RESULT_POINTLIMIT)
        {
            loaded_points->print();
            delete loaded_points;
        }
        mexErrMsgTxt(error.what());
    }
    catch (...)
    {
        mexErrMsgTxt("exeption ?");
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    






    /*!
     *
     * 	Returning Data To Matlab
     *
     *
     * */
  printf("writing data into mex arrays\n");


 /*   T Temp=100;
    T ConnTemp=1;
    T ConnCostTemp=1;
    T DataTemp=1;
 */ 
    {
        int dim = 1;
        const char *field_names[] = {"data","connections","Temp","ConnTemp","ConnCostTemp","DataTemp","PointTemp","opt_eval_state","ratio_up","ratio_down","iteration_time","iteration_time_total","iteration_total_num","thickness","activity","energy_grad","energy_grad_old","gradient_data","proposal_prob","scale_correction"};
        plhs[0]=mxCreateStructArray(1,&dim,21,field_names);
	mxSetField(plhs[0],0,"Temp",  mxCreateDoubleScalar(Temp));
	mxSetField(plhs[0],0,"ConnTemp",  mxCreateDoubleScalar(ConnTemp));
	mxSetField(plhs[0],0,"ConnCostTemp",  mxCreateDoubleScalar(ConnCostTemp));
	mxSetField(plhs[0],0,"DataTemp",  mxCreateDoubleScalar(DataTemp));
	mxSetField(plhs[0],0,"PointTemp",  mxCreateDoubleScalar(PointTemp));
	mxSetField(plhs[0],0,"opt_eval_state",  mxCreateDoubleScalar(opt_eval_state));
// 	mxSetField(plhs[0],0,"ratio_up",  mxCreateDoubleScalar(energy_ratio[0]));
// 	mxSetField(plhs[0],0,"ratio_down",  mxCreateDoubleScalar(energy_ratio[1]));
	mxSetField(plhs[0],0,"iteration_time",  mxCreateDoubleScalar(iteration_time));
	mxSetField(plhs[0],0,"iteration_time_total",  mxCreateDoubleScalar(iteration_time+iteration_time_total));
	mxSetField(plhs[0],0,"iteration_total_num",  mxCreateDoubleScalar(iteration_total_num+numiterations));
	mxSetField(plhs[0],0,"thickness",  mxCreateDoubleScalar(Points<T,Dim>::thickness));
	mxSetField(plhs[0],0,"energy_grad",  mxCreateDoubleScalar(energy_grad));
	mxSetField(plhs[0],0,"energy_grad_old",  mxCreateDoubleScalar(energy_grad_old));
	mxSetField(plhs[0],0,"scale_correction",  mxCreateDoubleScalar(Dataterm->get_scalecorrection()));
	
// 	mxSetField(plhs[0],0,"old_energy",  mxCreateDoubleScalar(energy_grad));
	
	mwSize dims[1];
	dims[0]=numproposals*2;
	mxArray * tmp =mxCreateNumericArray(1, dims,mxDOUBLE_CLASS,mxREAL);
	double * tmp_ptr=(double *)mxGetData(tmp);
	for (int i=0; i<numproposals; i++)
	  for (int j=0; j<2; j++)  
	    (*tmp_ptr++)=proposal_activity[i][j];
	mxSetField(plhs[0],0,"activity",  tmp);
	
	

	
	
    }

     
	    class OctPoints<T,Dim> ** index;
	    unsigned int  npoints;
	    tree.get_pointlist( index,npoints);
    
    
    
    unsigned int  npointproperties=22;

    int ndims[2];
    ndims[1]=npoints;
    ndims[0]=npointproperties;
    mxArray * pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
    mxSetField(plhs[0],0,"data", pdata);
    T *result = ( T * ) mxGetData (pdata );

    T * presult=result;

    printf("creating result array\n");
    mhs::mex_dumpStringNOW();
    
    
        
//     static const int maxconnections=3;
//     T endpoint_connections[2];
    
    
    
    
    
    
    int endpoint_statistic[Points< T, Dim >::maxconnections+1];
    for (int i=0;i<=Points< T, Dim >::maxconnections;i++)
      endpoint_statistic[i]=0;

    
    T min_max_saliency[2];
    min_max_saliency[0]=std::numeric_limits<T>::max();
    min_max_saliency[1]=std::numeric_limits<T>::min();
    
    int point_id=1;
    for (unsigned int i=0; i<npoints; i++)
    {
        //OctPoints<T,Dim> * point=(*index)[i];
	Points<T,Dim> & point=*((Points<T,Dim> *) (index[i]));
	
	do_label(point,point_id);
    
	
    }
    

      
      
      
    
    for (unsigned int i=0; i<npoints; i++)
    {
	Points<T,Dim> & point=*((Points<T,Dim> *) (index[i]));
	
 	//printf("%d %d\n",point.endpoint_connections[0],point.endpoint_connections[1]);
	endpoint_statistic[point.endpoint_connections[0]]++;
	endpoint_statistic[point.endpoint_connections[1]]++;
	
	
	point.path_id=i;
	presult[npointproperties-1]=-1; //connected or not


	
	presult[20]=point.point_id;
	presult[19]=point.endpoint_connections[0];
	presult[18]=point.endpoint_connections[1];
	presult[17]=-1000000;
	presult[16]=-1000000;
	presult[15]=point.saliency;
	min_max_saliency[0]=std::min(point.saliency,min_max_saliency[0]);
	min_max_saliency[1]=std::max(point.saliency,min_max_saliency[1]);
	
	presult[14]=point.point_cost;  
	point.update_endpoint(0);
	point.update_endpoint(1);
	
	presult[13]=point.endpoints_pos[Points<T,Dim>::b_side][0];  
	presult[12]=point.endpoints_pos[Points<T,Dim>::b_side][1];  
	presult[11]=point.endpoints_pos[Points<T,Dim>::b_side][2];  
	presult[10]=point.endpoints_pos[Points<T,Dim>::a_side][0];  
	presult[9]=point.endpoints_pos[Points<T,Dim>::a_side][1];  
	presult[8]=point.endpoints_pos[Points<T,Dim>::a_side][2];  
	
	presult[7]=point.scale;
// 	presult[6]=point.ofield_prob;
	
	presult[6]=point.tracker_birth;
	
	presult[5]=point.direction[0];
	presult[4]=point.direction[1];
	presult[3]=point.direction[2];
	
	sta_assert(point.position[0]>=0);
	sta_assert(point.position[0]<shape[0]);
	sta_assert(point.position[1]>=0);
	sta_assert(point.position[1]<shape[1]);
	sta_assert(point.position[2]>=0);
	sta_assert(point.position[2]<shape[2]);
	
	presult[2]=point.position[0];
        presult[1]=point.position[1];
        presult[0]=point.position[2];
// 	presult[2]=5;
//         presult[1]=40;
//         presult[0]=44;	
        presult+=ndims[0];
    }
    printf("saliency: min %f, max %f\n",min_max_saliency[0],min_max_saliency[1]);
    
    printf("------------------------------------\n");
    printf("multiple connection histogram:\n");
    for (int i=0;i<=Points< T, Dim >::maxconnections;i++)
      printf("%d connections: %d \n",i,endpoint_statistic[i]);
    printf("\n");


    printf("number of connections: %d\n",connections.pool->get_numpts());
    connections.print();
    connections.check_consistency();
    
    std::size_t numconnections=connections.pool->get_numpts();
    ndims[1]=numconnections;
    ndims[0]=7;
    pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
    mxSetField(plhs[0],0,"connections", pdata);
    T* result2 = ( T * ) mxGetData (pdata );
    presult=result2;
//     for (class std::list<class Connection<T,Dim> >::iterator iter=connections.connections.begin();
//       iter!=connections.connections.end();iter++)
	  const Connection<T,Dim> ** conptr=connections.pool->getMem();
	  for (std::size_t i=0;i<numconnections;i++)
	 {
	  const Connection<T,Dim> * con=*conptr; 
	   presult[0]=con->pointA->point->path_id;
	   presult[1]=con->pointB->point->path_id;

	   presult[2]=con->pointA->side;
	   presult[3]=con->pointB->side;
	   
	   result[int((presult[0]+1)*npointproperties)-1]=1;
	   result[int((presult[1]+1)*npointproperties)-1]=1;
	   
// 	   int inrange;
// 	   presult[4]=con->compute_cost(0,ConnectionScale,Connectionparticle_connection_state_cost_scale,0,0,maxendpointdist,inrange);
// 	   sta_assert(inrange>0);
// 	   presult[5]=con->compute_cost(0,0,Connectionparticle_connection_state_cost_scale,ThicknessScale,0,maxendpointdist,inrange);
// 	   sta_assert(inrange>0);
// 	   presult[6]=con->compute_cost(0,0,Connectionparticle_connection_state_cost_scale,0,AngleScale,maxendpointdist,inrange);
// 	   sta_assert(inrange>0);
	   
	   presult+=ndims[0];
	   conptr++;
	 }
    
    
    
    
	printf("max point search iter %d\n",maxpiter);
	tree.print();
	printf("------------------------------------\n");
        printf("T        : %2.3f\n",Temp);
	printf("ccT      : %2.3f\n",ConnCostTemp);
	printf("cT       : %2.3f\n",ConnTemp);
	printf("dT       : %2.3f\n",DataTemp);
	printf("pT       : %2.3f\n",PointTemp);
	printf("------------------------------------\n");

    
printf("\n\n---------------STOP-----------------\n\n");

if (Dataterm!=NULL)
  delete Dataterm;

if (edgecost_fun!=NULL)
  delete edgecost_fun;

}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  printf("mexcfunc #params: %d\n",nrhs);
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");
  if (!mxIsStruct(prhs[0]))
    mexErrMsgTxt("error: expecting feature struct\n");
  
  const mxArray *img=mxGetField(prhs[0],0,(char *)("img"));
  sta_assert(img!=NULL);

  
  if (mxGetClassID(img)==mxDOUBLE_CLASS)
   _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(img)==mxSINGLE_CLASS)
    _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
}







