#ifndef MHS_VECTOR_H
#define MHS_VECTOR_H


#include <stdio.h>
#include <string.h>
#include "rand_help.h"
#include <sstream>


template<typename T,int Dim>
class Vector
{
public:
    T v[Dim];
    Vector() {};

    void print()
    {
        std::stringstream s;
        s<<"(";

        for (int i=0; i<Dim; i++)
            if (i<Dim-1)
                s<<v[i]<<",";
            else
                s<<v[i]<<")";
        printf("%s\n",s.str().c_str());
    }

    const T &operator[](int i) const {
        return v[i];
    };
    T &operator[](int i)  {
        return v[i];
    };

    //const T &operator[](int i)  { return v[i]; };

    T norm2() const
    {
        T n=0;
        #pragma omp simd reduction(+:n)
        for (int i=0; i<Dim; i++)
            n+=v[i]*v[i];
        return n;
    }

    T norm1() const
    {
        T n=0;
        #pragma omp simd reduction(+:n)
        for (int i=0; i<Dim; i++)
            n+=std::abs(v[i]);
        return n;
    }

    void normalize()
    {
        (*this )/=(std::sqrt(this->norm2())+std::numeric_limits<T>::epsilon());
    }

    const Vector & rand(T min,T max)
    {
        for (int i=0; i<Dim; i++)
            v[i]=myrand(max-min)+min;
        return *this;
    }

    const Vector & rand(T* min,T* max)
    {
        for (int i=0; i<Dim; i++)
            v[i]=myrand(max[i]-min[i])+min[i];
        return *this;
    }

    const Vector & rand_normal(T sigma)
    {
        for (int i=0; i<Dim; i++)
            v[i]=mynormalrand(sigma);
        return *this;
    }

    Vector(const T rhs[]) {
        memcpy (v,rhs,Dim*sizeof(T));
    };



    Vector(const T z,const T y=0,const T x=0)
    {
        v[0]=z;
        if (Dim>1)
            v[1]=y;
        if (Dim>2)
            v[2]=x;
    };


    T dot(const Vector &rhs) const
    {
        T result=0;
        #pragma omp simd reduction(+:result)
        for (int i=0; i<Dim; i++)
            result+=v[i]*rhs.v[i];
        return result;
    }

    template<typename S>
    T dot(const S rhs[]) const
    {
        T result=0;
        #pragma omp simd reduction(+:result)
        for (int i=0; i<Dim; i++)
            result+=v[i]*rhs[i];
        return result;
    }

    const Vector cross(const Vector &rhs) const
    {
        sta_assert(Dim==3);
        Vector c;
        c.v[0]=v[1]*rhs.v[2]-v[2]*rhs.v[1];
        c.v[1]=v[2]*rhs.v[0]-v[0]*rhs.v[2];
        c.v[2]=v[0]*rhs.v[1]-v[1]*rhs.v[0];
        return c;
    }

    Vector & operator=(const Vector &rhs)
    {
        if (this != &rhs)
            memcpy (v,rhs.v,Dim*sizeof(T));
        return *this;
    }

    template<typename S>
    Vector & operator=(const std::vector<S> &rhs)
    {
        for (int i=0; i<Dim; i++)
        {
            if (i<rhs.size())
                v[i]=rhs[i];
            else
                v[i]=0;
        }
        return *this;
    }

    Vector & operator=(const T rhs[])
    {
        if (v != rhs)
            //    if (this != &rhs)
            memcpy (v,rhs,Dim*sizeof(T));
        return *this;
    }


//     template<typename S>
//     Vector & operator=(S f)
//     {
//         #pragma omp simd
//         for (int i=0; i<Dim; i++)
//             v[i]=f;
//         return *this;
//     }
    
    Vector & operator=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]=f;
        return *this;
    }

    Vector & operator+=(const Vector &rhs)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]+=rhs.v[i];
        return *this;
    }

    Vector & operator+=(const T rhs[])
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]+=rhs[i];
        return *this;
    }

    Vector & operator+=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]+=f;
        return *this;
    }

    Vector & operator-=(const Vector &rhs)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]-=rhs.v[i];
        return *this;
    }

    Vector & operator-=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]-=f;
        return *this;
    }

    Vector & operator*=(const Vector &rhs)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]*=rhs.v[i];
        return *this;
    }

    Vector & operator*=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]*=f;
        return *this;
    }

    Vector & operator/=(const Vector &rhs)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]/=rhs.v[i];
        return *this;
    }

    Vector & operator/=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim; i++)
            v[i]/=f;
        return *this;
    }

    const Vector operator+(const Vector &rhs) const
    {
        return Vector(*this) += rhs;
    }

    const Vector operator+(const T rhs[]) const
    {
        return Vector(*this) += rhs;
    }

    const Vector operator+(T f) const
    {
        return Vector(*this) += f;
    }

    const Vector operator-(const Vector &rhs) const
    {
        return Vector(*this) -= rhs;
    }

    const Vector operator-(T f) const
    {
        return Vector(*this) -= f;
    }

    const Vector operator/(const Vector &rhs) const
    {
        return Vector(*this) /= rhs;
    }

    const Vector operator/(T f) const
    {
        return Vector(*this) /= f;
    }

    const Vector operator*(const Vector &rhs) const
    {
        return Vector(*this) *= rhs;
    }

    const Vector operator*(T f) const
    {
        return Vector(*this) *= f;
    }

    bool operator==(const Vector &other) const {
        for (int i=0; i<Dim; i++)
            if (v[i]!=other.v[i])
                return false;
        return true;
    }

    bool operator!=(const Vector &other) const {
        return !(*this==other);
    }
};









#endif
