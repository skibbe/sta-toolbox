#ifndef GRAPH_REPRESENTATION_H
#define GRAPH_REPRESENTATION_H


#include "voxgrid.h"
#include "mcpu_volume.h"
#include "mhs_pool.h"
#include "mhs_vector.h"
#include "rand_help.h"
#include "energy.h"


const int pointlimit=2000000;

static const int only_one_connecting=0; // 0 or 1
static const int directed_connections=-1; // 0 or 1



template<typename T,int Dim> class Points;
template<typename T,int Dim> class Connection;



template<typename T,int Dim>
class Points: public OctPoints< T, Dim >
{
  
  friend pool<Points>;
private:
    void clear() //:clear()
    {	
     
//       printf("!\n");
//       free_points_indx=mhs_maxpoints+1;
      tracker_birth=0;
	OctPoints< T, Dim >::clear();
	
// 	tracker_birth=false;
// 	terminal_indx[0]=-1;
// 	terminal_indx[1]=-1;
// 	direction=T(0);
// 	direction[0]=T(1);
// 	scale=1;
//  	//init_pointer();
// 	for (int n=0;n<maxconnections;n++)
// 	{
// 	  endpoints[0][n].connected=NULL;
// 	  endpoints[1][n].connected=NULL;
// 	}
	
// 	path_id=-1;
	track_me_id=track_point_id;
	endpoint_connections[0]=0;
	endpoint_connections[1]=0;  
    }  
  
public:
  
    int track_me_id;
    class Vector<T,Dim> direction;
    int tracker_birth;
    
    T point_cost;
//     int ofield_indx;
    
    int terminal_indx[2];
    T scale;
    T saliency;
    
//     int sindx;
//     int oindx;
    int path_id;
    int point_id;
    
    std::size_t free_points_indx;
    
    static int maxconnections;
    
    
    static T thickness;
    static T min_thickness;
    static T   endpoint_nubble_scale;
    
    static const int a_side=0;
    static const int b_side=1;
    
    static const T side_sign[2];//={+1,-1};
    
    static int point_weight;
    
    class Vector<T,Dim>     endpoints_pos[2];
    int endpoint_connections[2];
    
    
    
    T compute_cost_scale()
    {
     return scale; 
    }
    
    T compute_cost()
    {
      //return 1;
      //return scale;
      //return scale*scale*scale;
      
      switch (point_weight)
      {
	case 1:
	  return scale;
	case 2:
	  return scale*scale;
	case 3:
	  return scale*scale*scale;
	default:
	  return 1;
      }
      //return scale*scale+scale;
      
      //return scale*scale*std::sqrt(scale);
      
//       if (Dim==2)
// 	return scale; //return std::sqrt(scale); // sigma^(2/2)
// 	else 
 	 
      //return scale*scale*scale;
//       //return std::sqrt(scale);
//       return std::sqrt(scale*scale*scale); ///sigma^(3/2)
    }
    
    class CEndpoint
    {
      public:
      int  side;
      Points * point;     	
       //class std::list<class Connection<T,Dim> >::iterator connection;
      class Connection<T,Dim> * connection;
      CEndpoint * connected;
      bool is_source;
      CEndpoint() : point(NULL),connected(NULL), is_source(false){};
      Vector<T,Dim> * position;
    };
    
    //std::vector<CEndpoint> endpoints[2];
    CEndpoint endpoints[2][4];
    
    
    Points<T,Dim> * get_connected_point()
    {
	sta_assert_error(get_num_connections()==1);
	Points<T,Dim> * connection=NULL;
		
		for (int s=0;s<2;s++)
		for (int c=0;c<Points<T,Dim>::maxconnections;c++)
		{
		  if (endpoints[s][c].connected!=NULL)
		  {
		    connection=endpoints[s][c].connected->point;
		    //found=point.endpoints[s][c].connected->side;
		    break;
		  }
		}
	sta_assert_error(connection!=NULL);
	return connection;
    }	
    
//     void optimal_sucessor_bif(
// 		Vector<T,Dim> &optimal_pos,
// 		Vector<T,Dim> &track_direction)
//     {
// 	Vector<T,Dim> randp;
// 	randp.rand_normal(1);
// 	randp=randp-direction.dot(randp);
// 	randp.normalize();
// 	
// 	
//       
//     }
    
    
    void optimal_sucessor(
		Vector<T,Dim> &optimal_pos,
		Vector<T,Dim> &track_direction,
		int& optimal_sideA,
		int& optimal_sideB,
		int endpoint=-1)
    {
		
		//if (get_num_connections()==1)
		if (endpoint==-1)
		{
		  //if (endpoint_connections[0]>0)
		  if (endpoint_connections[0]==1)
		    endpoint=1;
		  //else if (endpoint_connections[1]>0)
		  else if (endpoint_connections[1]==1) 
		    endpoint=0;
		  else 
		    sta_assert_error(1==0);
		}
     		
		//if (endpoint_connections[0]>0)
		if (endpoint==1)  
		{
		  track_direction=direction*Points<T,Dim>::side_sign[1];
		  optimal_sideB=0;
		  optimal_sideA=1;
		}else
		{
		  track_direction=direction*Points<T,Dim>::side_sign[0];
		  optimal_sideB=1;
		  optimal_sideA=0;
		} 
		if (thickness>0)
		optimal_pos=(this->position)
 		    +track_direction*(Points<T,Dim>::thickness*2+scale/T(2.0));     
		    else
#ifdef 		SCALENORMAL		    
 		      optimal_pos=(this->position)
		    //  +track_direction*((-Points<T,Dim>::thickness*scale)*2+10);     
  		    //+track_direction*((-Points<T,Dim>::thickness*scale)*2+scale/T(2.0));     
		    +track_direction*std::max(Points<T,Dim>::min_thickness,((-Points<T,Dim>::thickness*scale)))*2;     
#else
	        optimal_pos=(this->position)
 		    +track_direction*((-Points<T,Dim>::thickness*mysqrt(scale))*2+scale/T(2.0));     		    
#endif
    }
    
    
    CEndpoint * getfreehub(int i)
    { 
      for (int a=only_one_connecting;a<maxconnections;a++)
      {
	if (endpoints[i][a].connected==NULL)
	  return (&(endpoints[i][a]));
      }
      return NULL;
    }
    
    int get_num_sources(int i)
    { 
      int num_sources=0;
      for (int a=0;a<maxconnections;a++)
      {
	if ((endpoints[i][a].connected!=NULL)&&(endpoints[i][a].is_source))
	 num_sources++;
      }
      return num_sources;
    }
    
    
    void update_endpoint(int i)
    {
	  if (thickness>0)
	  {
	    //endpoints_pos[i]=direction*side_sign[i]*(endpoint_nubble_scale+thickness)+this->position;
	    T* ept= endpoints_pos[i].v;
	    const T * dir= direction.v;
	    T  fact=side_sign[i]*(endpoint_nubble_scale+thickness);
	    const T * pos=this->position.v;
	    *ept++=(*dir++)*fact+(*pos++);
	    *ept++=(*dir++)*fact+(*pos++);
	    *ept=(*dir)*fact+(*pos);
	  }
	  else
	  {
	    T* ept= endpoints_pos[i].v;
	    const T * dir= direction.v;
#ifdef 		SCALENORMAL		    
	    T  fact=side_sign[i]*(endpoint_nubble_scale+std::max(Points<T,Dim>::min_thickness,-thickness*scale));
#else
	    T  fact=side_sign[i]*(endpoint_nubble_scale-thickness*mysqrt(scale));
#endif
	    const T * pos=this->position.v;
	    *ept++=(*dir++)*fact+(*pos++);
	    *ept++=(*dir++)*fact+(*pos++);
	    *ept=(*dir)*fact+(*pos);
	  }  
	    //endpoints_pos[i]=direction*side_sign[i]*scale+this->position;
    }
    
    
    void update_endpoints()
    {
     update_endpoint(0); 
     update_endpoint(1); 
    }
    
    Points() : OctPoints< T, Dim >()
    {
      free_points_indx=mhs_maxpoints+1;
      tracker_birth=0;
	terminal_indx[0]=-1;
	terminal_indx[1]=-1;
	
	direction=T(0);
	direction[0]=T(1);
//         point_prob=1;
// 	ofield_prob=1;
	scale=1;
	
	sta_assert_error(maxconnections<5);
	//endpoints[0].resize(maxconnections);
	//endpoints[1].resize(maxconnections);
	
 	init_pointer();
	path_id=-1;
	point_id=-1;
// 	ofield_indx=-1;
	endpoint_connections[0]=0;
	endpoint_connections[1]=0;
// 	this->owner=NULL;
// 	this->clear();
	//track_me_id=0;
    }
    
    void init_pointer()
    {
      for (int n=0;n<maxconnections;n++)
	{
	  
	  endpoints[0][n].side=a_side;
	  endpoints[0][n].point=this;
	  endpoints[0][n].connected=NULL;
	  endpoints[0][n].position=&(endpoints_pos[0]);
	  endpoints[1][n].side=b_side;
	  endpoints[1][n].point=this;
	  endpoints[1][n].connected=NULL;
	  endpoints[1][n].position=&(endpoints_pos[1]);
	}
    }

    bool isconnected()
    {
	if (endpoint_connections[0]+endpoint_connections[1]==0)
	  return false;
	return true;
    }
    
    int get_num_connections()
    {
	return (endpoint_connections[0]+endpoint_connections[1]);
    }
    
    ~Points()
    {
// 	if (isconnected())
// 	    throw mhs::STAError("removing connected point!");
//       printf("how?");
//       sta_assert_error(false);
    }
};



template<typename T,int Dim>
T Points<T,Dim>::endpoint_nubble_scale=0;

template<typename T,int Dim>
const T Points<T,Dim>::side_sign[2]={+1,-1};

template<typename T,int Dim>
int Points<T,Dim>::maxconnections=3;

template<typename T,int Dim>
T Points<T,Dim>::thickness=-1;

template<typename T,int Dim>
T Points<T,Dim>::min_thickness=0;

template<typename T,int Dim>
int Points<T,Dim>::point_weight=2;



template<typename T,int Dim>
class Connection: public pool_element<class Connection<T,Dim> >
{
protected:
  
   friend pool<Connection>;
//    std::size_t poolid;
    

    virtual void clear()
    {
	pool_element<class Connection<T,Dim> >::clear();
/*#ifdef _POOL_TEST         
	exist=false;
#endif	    */  
	track_me_id=track_id;
	pointA=NULL;
	pointB=NULL;
    }
    
  public:
    
    
    
    
//   #ifdef _POOL_TEST       
//     bool exist;
// #endif       
    
    const static T no_connection_cost;
    class Points< T, Dim >::CEndpoint * pointA;
    class Points< T, Dim >::CEndpoint * pointB;
    int track_me_id;    
    Connection() : pointA(NULL),pointB(NULL),track_me_id(0) {};  
  
    T compute_cost(T L,T sL,T pS, T tS,T AngleScale,T max_endpoint_dist,int & inrange,bool debug=false) const
    {
		pointA->point->update_endpoint(pointA->side);
		pointB->point->update_endpoint(pointB->side);
		
// 		pointA->position->print();
// 		pointB->position->print();
		
		T u=ucon<T,Dim>(
		      *(pointA->point),
		      *(pointA->position),
		      *(pointB->point),
		      *(pointB->position),
// 		      pointA->point->position,
// 		      *(pointA->position),
// 		      pointA->point->scale,
// 		      pointB->point->position,
// 		      *(pointB->position),
// 		      pointB->point->scale,
		      L,sL,pS,tS,AngleScale,max_endpoint_dist,inrange,debug);
		return u;
    }
    

    
};

template<typename T,int Dim>
const  T Connection<T,Dim>::no_connection_cost=0;


template<typename T,int Dim>
class Connections
{
  public:  
    std::vector<Points<T,Dim> * > terminals[2];
    std::size_t num_terminals[2];
    
    
    
    
    
    
    //const unsigned int mhs_maxpoints
    //std::list<Connection<T,Dim> > connections;
    
//     Connection<T,Dim> ** connection_index;
//     std::size_t num_connections;
//     class pool<class Connection<T,Dim> >  * pool;
    typedef class pool<class Connection<T,Dim> > pclass;
    pclass * pool;
    
//     class pool<Connection<T,Dim> >  * pool2;
    //point_index=new class OctPoints<T,Dim> *[mhs_maxpoints];
    
//   Connections(connection_pool & pool )
    
    
    class Connection<T,Dim> * get_rand_edge(
      class volume_lock<T,Dim> & vlock,
      T lock_radius,
      bool & empty)
    {
	empty=false;
	Connection<T,Dim> * edge_ptr=NULL;
#ifdef _MCPU     	
	#pragma omp critical (EDGE_POINTER)
#endif	
	{
	  
	  if (pool->get_numpts()>0)
	  {
	    edge_ptr=(pool->getMemW()[std::rand()%pool->get_numpts()]);
	    bool can_lock=vlock.lock(*(edge_ptr->pointA->position),lock_radius);
	    if (!can_lock)
		edge_ptr=NULL;
	  }else
	    empty=true;
	}
        return edge_ptr;
	
	
	
	
	
    }
    
    
    
      Connections()
  {
    for (int a=0;a<2;a++)
    {
      terminals[a].resize(mhs_maxpoints);
      num_terminals[a]=0;
    }
//     num_connections=0;
//     connection_index=new Connection<T,Dim> *[mhs_maxpoints];
//     this->pool=&pool;
      pool=new pclass(pointlimit);
  }
  
  ~Connections()
  {
    delete pool;
//     delete [] connection_index;
  }
  
  int get_num_terminals(int listid){return num_terminals[listid];}
  
  Points<T,Dim> & get_rand_point(int listid)
  {
      sta_assert(num_terminals[listid]>0);
      return *(terminals[listid][std::rand()%num_terminals[listid]]);
  }
  
  
  void check_consistency()
  {
    printf("checking consistency (terminals) ..");
    int listid=0;
   for (std::size_t i=0;i<num_terminals[listid];i++) 
   {
     //sta_assert_error(terminals[i]->get_num_connections()==1);
     sta_assert_error((terminals[listid][i]->endpoint_connections[0]==0)||(terminals[listid][i]->endpoint_connections[1]==0));
     for (std::size_t j=i+1;j<num_terminals[listid];j++) 
       sta_assert_error(terminals[listid][i]!=terminals[listid][j]);
   }
   printf("done\n");
  }
    
    
  
  void print()
  {
    for (int listid=0;listid<2;listid++)
      printf("num terminals: %d\n",num_terminals[listid]);
  }
  
  void register_terminal_node(Points<T,Dim> * p,int listid)
  {
    
    sta_assert_error(p->terminal_indx[listid]<=-1);
    //sta_assert_error(p->get_num_connections()==1);
    sta_assert_error((p->endpoint_connections[0]==0)||(p->endpoint_connections[1]==0));
    terminals[listid][num_terminals[listid]]=p;
    p->terminal_indx[listid]=num_terminals[listid];
    num_terminals[listid]++;
    sta_assert_error(num_terminals[listid]<mhs_maxpoints);
  }
  
  void unregister_terminal_node(Points<T,Dim> * p,int listid)
  {
    sta_assert_error(p->terminal_indx[listid]>-1);
    sta_assert_error(p->get_num_connections()!=1);
    terminals[listid][p->terminal_indx[listid]]=terminals[listid][num_terminals[listid]-1];
    terminals[listid][p->terminal_indx[listid]]->terminal_indx[listid]=p->terminal_indx[listid];
    sta_assert(p->terminal_indx[listid]>-1);
    sta_assert_error(terminals[listid][p->terminal_indx[listid]]->terminal_indx[listid]>-1);
    p->terminal_indx[listid]=-2;
     sta_assert_error(num_terminals[listid]>0);
    num_terminals[listid]--;
    sta_assert_error(num_terminals[listid]>=0);
  }
  
  
  bool connection_exists(Connection<T,Dim> & connection)  
  {
   if (Points<T,Dim>::maxconnections>1)
    {
	for (int s=0;s<2;s++)
	{
	  //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=connection.pointA->point->endpoints[s];
	  
	  class Points<T,Dim>::CEndpoint * endpointsA=connection.pointA->point->endpoints[s];
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      if (connection.pointB->point==endpointsA[i].connected->point)
	      {
		return true;
	      }
	    }
	  }
	}
    }   
    return false;
  }
 
 
    bool same_pt(Connection<T,Dim> & a, Connection<T,Dim> * & b)
    {
//       if (((a.pointA->point==b.pointA->point)&&(a.pointB->point==b.pointB->point))||
// 	((a.pointA->point==b.pointB->point)&&(a.pointB->point==b.pointA->point)))
// 	return true;
      if (((a.pointA->point!=b->pointA->point)||(a.pointB->point!=b->pointB->point))&&
	((a.pointA->point!=b->pointB->point)||(a.pointB->point!=b->pointA->point)))
	return false;      
      else true;
    } 
  
  bool connection_exists2(Connection<T,Dim> & connection)  
  {
   
   if (Points<T,Dim>::maxconnections>1)
    {
	class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=connection.pointA->point->endpoints[connection.pointA->side];
	for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	{
	  if (endpointsA[i].connected!=NULL)
	  {
	    if ((endpointsA[i].connected->point==connection.pointB->point)&&((endpointsA[i].connected->side==connection.pointB->side)))
	    {

	      return true;
	    }
	  }
 	}
    }

    return false;
  }
    
  bool add_connection(Connection<T,Dim> & connection,
    bool avoid_double_connections=true)
  {
    if ((connection.pointA->connected!=NULL)&&(connection.pointB->connected!=NULL)&&
      (connection.pointA->connected->point==connection.pointB->point)&&
      (connection.pointA->connected->side==connection.pointB->side)
    )
    {
	return false;
    }
      
    sta_assert_error((connection.pointA->connected==NULL));
    sta_assert_error((connection.pointB->connected==NULL));	
    
    if ((avoid_double_connections) && (connection_exists(connection)))
      return false;


    int connectedA[2];
    connectedA[0]=connection.pointA->point->endpoint_connections[0]>0;
    connectedA[1]=connection.pointA->point->endpoint_connections[1]>0;
    
    
//     sta_assert_error(num_connections<mhs_maxpoints);
//     connection_index[num_connections++]
    
// printf("0\n");
    class Connection<T,Dim> * new_connection =pool->create_obj();
    
    
//     printf("adding %u\n",new_connection);
//     const Connection<T,Dim> ** poll_ptr=pool->getMem();
//     for (std::size_t i=0;i<pool->get_numpts();i++)
//     {
// 	printf("%u %u %d\n",i,*poll_ptr,(*poll_ptr)->exist);  
// 	poll_ptr++;
//     }
    *new_connection=connection;
    //connections.push_back(connection);
    //class std::list<class Connection<T,Dim> >::iterator  conn=connections.end();
    
    
    
    
//     conn--;
    connection.pointA->connection=new_connection;
    connection.pointA->is_source=true;
    connection.pointA->connected=connection.pointB;    
    connection.pointA->point->endpoint_connections[connection.pointA->side]++;
    connection.pointB->connection=new_connection;
    connection.pointB->is_source=false;
    connection.pointB->connected=connection.pointA;    
    connection.pointB->point->endpoint_connections[connection.pointB->side]++;
    


    
   bool candidateA=(connection.pointA->point->get_num_connections()==1);
   
   bool double_terminal=false;
 
    
    bool registered=connection.pointA->point->terminal_indx[0]>=0;
    
    if ((candidateA)&&(!registered))
    	register_terminal_node(connection.pointA->point,0);     
    if ((!candidateA)&&(registered))
	unregister_terminal_node(connection.pointA->point,0);     
    
    
  bool candidateB=(connection.pointB->point->get_num_connections()==1);
  
    registered=connection.pointB->point->terminal_indx[0]>=0;
    
    if ((candidateB)&&(!registered))
    	register_terminal_node(connection.pointB->point,0);     
    if ((!candidateB)&&(registered))
	unregister_terminal_node(connection.pointB->point,0);          
    
   
   candidateA=(candidateA && (connection.pointB->point->get_num_connections()<=2));
   registered=connection.pointA->point->terminal_indx[1]>=0;
   
    if ((candidateA)&&(!registered))
    	register_terminal_node(connection.pointA->point,1);     
    if ((!candidateA)&&(registered))
	unregister_terminal_node(connection.pointA->point,1);     
   
   
   candidateB=(candidateB && (connection.pointA->point->get_num_connections()<=2)); 
   registered=connection.pointB->point->terminal_indx[1]>=0;
   
    if ((candidateB)&&(!registered))
    	register_terminal_node(connection.pointB->point,1);     
    if ((!candidateB)&&(registered))
	unregister_terminal_node(connection.pointB->point,1);     
      
//    printf("1\n"); 
    
    return true;
  }

  //void remove_connection(class std::list<class Connection<T,Dim> >::iterator & connection)
  void remove_connection(class Connection<T,Dim> * & connection)
  {
    sta_assert_error((connection->pointA->connected!=NULL));
    sta_assert_error((connection->pointB->connected!=NULL));
    
//     printf("2\n");
    connection->pointA->point->endpoint_connections[connection->pointA->side]--;
    connection->pointB->point->endpoint_connections[connection->pointB->side]--;
    
    connection->pointA->connected=NULL;
    connection->pointB->connected=NULL;
    
    //connections.erase(connection);
    
    
    bool candidateA=(connection->pointA->point->get_num_connections()==1);
    
    bool registered=connection->pointA->point->terminal_indx[0]>=0;
    
    if ((candidateA)&&(!registered))
    	register_terminal_node(connection->pointA->point,0);     
    if ((!candidateA)&&(registered))
	unregister_terminal_node(connection->pointA->point,0);     
    
    
    bool candidateB=(connection->pointB->point->get_num_connections()==1);
	      
    registered=connection->pointB->point->terminal_indx[0]>=0;
    
    if ((candidateB)&&(!registered))
    	register_terminal_node(connection->pointB->point,0);     
    if ((!candidateB)&&(registered))
	unregister_terminal_node(connection->pointB->point,0);         
   
    
    candidateA=(candidateA && (connection->pointB->point->get_num_connections()<=2));
   registered=connection->pointA->point->terminal_indx[1]>=0;
   
    if ((candidateA)&&(!registered))
    	register_terminal_node(connection->pointA->point,1);     
    if ((!candidateA)&&(registered))
	unregister_terminal_node(connection->pointA->point,1);     
   
   
   candidateB=(candidateB && (connection->pointA->point->get_num_connections()<=2)); 
   registered=connection->pointB->point->terminal_indx[1]>=0;
   
    if ((candidateB)&&(!registered))
    	register_terminal_node(connection->pointB->point,1);     
    if ((!candidateB)&&(registered))
	unregister_terminal_node(connection->pointB->point,1);         
   
//      printf("3.1\n");
//     printf("remioving %u\n",connection);
//     for (std::size_t i=0;i<pool->get_numpts();i++)
//     {
// 	printf("%u %u\n",i,&(*pool->getMem()[i]));  
//     }
    
    pool->delete_obj(connection);
    
//     printf("3\n");
  }  
  
};




template<typename T,int Dim>
class   TConnecTProposal
{
public:
    class Connection<T,Dim>   connection_new;
    bool old_connection_exists;
    T old_prob;
    T new_prob;
    T old_cost;
    T new_cost;    
    //T new_prob_no_connection;   
    bool new_connection_exists;
    //class std::list<class Connection<T,Dim> >::iterator 
    class Connection<T,Dim>  * connection_old;
    
  void print()
  {
   //printf("pole: %d  port: %d, dest pole %d, cost %f, prob %f\n",source_pole_id,source_port_id,dest_pole_id,cost,prob); 
  }
};



template<typename T,int Dim>
bool has_loop(Points<T,Dim> & npoint,
	      TConnecTProposal<T,Dim> & connection,
	      int max_depths=1,
	      int depths=0,
	      Points<T,Dim> * startpoint=NULL)
{
  if (depths>max_depths)
      return false;
  
  //! first node should follow new connection
  if (depths==0)
  {
    if (connection.new_connection_exists)
    {
       
       // modification speedup dez 2013 (check if connection point pair is same)
       if (connection.old_connection_exists)
       {
	 Points<T,Dim> * test_point=connection.connection_new.pointB->point;
	 if ((test_point==connection.connection_old->pointA->point)||
	   (test_point==connection.connection_old->pointB->point))
	    return false;
       }
	track_id++;
	sta_assert_error(connection.connection_new.pointA->point==&npoint);
	return has_loop(*(connection.connection_new.pointB->point),
			connection,
			max_depths,
			depths+1,
			&npoint);
      
    }else
      return false; //! no new connection. so if there was no loop there is still no loop
  }else 
  {
    
    for (int end_id=0;end_id<2;end_id++)
    {
	  //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	  class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	  
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      //! check if edge is not the edge that will be deleted and edge has not been tracked so far
	      if (((!connection.old_connection_exists)||(endpointsA[i].connection!=connection.connection_old))
		&&(endpointsA[i].connection->track_me_id!=track_id))
	      {
		  endpointsA[i].connection->track_me_id=track_id;
		  
		  if (endpointsA[i].connected->point==startpoint)
		    return true;
		  
		  
		  if (has_loop(*(endpointsA[i].connected->point),
			connection,
			max_depths,
			depths+1,
			startpoint))
		  return true;
	      }
	    }
	  }
    }
  }
  return false;
    sta_assert_error(false);
}



template<typename T,int Dim>
bool check_constraint_path(
	      Points<T,Dim> & npoint,
	      TConnecTProposal<T,Dim> & connection,
	      int max_depths,
	      int depths=0)
{
  if (depths>max_depths) 
    false;  
  
 if (npoint.track_me_id==track_point_id)
    return true;
  npoint.track_me_id=track_point_id;

  
  int num_edges=npoint.get_num_connections();
  if (connection.new_connection_exists)
  {
      if (connection.connection_new.pointA->point==&npoint)
	num_edges+=1;
      if (connection.connection_new.pointB->point==&npoint)
	num_edges+=1;
  }
  if (connection.old_connection_exists)
  {
      if (connection.connection_old->pointA->point==&npoint)
	num_edges-=1;
      if (connection.connection_old->pointB->point==&npoint)
	num_edges-=1;
  }  
  
  if (num_edges!=2)
    return true;
  
  
  int not_violated=0;
  
    for (int end_id=0;end_id<2;end_id++)
    {
	  class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	  
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      if ((!connection.old_connection_exists)||(endpointsA[i].connection!=connection.connection_old))
	      {
		  if (!check_constraint_path(
			*(endpointsA[i].connected->point),		       
			connection,
			max_depths,
			depths+1))
		    not_violated++;
	      }
	    }
	  }
    }
    if (connection.new_connection_exists)
    {
	if (!check_constraint_path(
			*(connection.connection_new.pointB->point),		       
			connection,
			max_depths,
			depths+1))
	not_violated++;
    }
    
   if  (not_violated!=1)
     return true;
  
  return false;
}


template<typename T,int Dim>
bool check_constraint_point(
	      Points<T,Dim> & npoint,
	      TConnecTProposal<T,Dim> & connection,
	      int max_depths)
{

  track_point_id++;
  
  if (npoint.track_me_id==track_point_id)
    return false;
  npoint.track_me_id=track_point_id;
  
    for (int end_id=0;end_id<2;end_id++)
    {
	  class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	  
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      if ((!connection.old_connection_exists)||(endpointsA[i].connection!=connection.connection_old))
	      {
		  if (check_constraint_path(
			*(endpointsA[i].connected->point),		       
			connection,
			max_depths,
			1))
		    return true;
	      }
	    }
	  }
    }
    if (connection.new_connection_exists)
    {
	if (check_constraint_path(
			*(connection.connection_new.pointB->point),		       
			connection,
			max_depths,
			1))
	return true;
    }

  
  return false;
}


template<typename T,int Dim>
bool check_multi_edge_constraint(
	      Points<T,Dim> & npoint,
	      TConnecTProposal<T,Dim> & connection,
	      int max_depths)
{
      Points<T,Dim> * candidates[50];
      Points<T,Dim> ** candidatesp=&candidates[0];
      int numcandidates=0;
      multi_edge_constraint_candidates(
	      npoint,
	      candidatesp,
	      numcandidates,
	      connection,
	      max_depths);
      
      for (int i=0;i<numcandidates;i++)
      {
	if (check_constraint_point(
	      *(candidates[i]),
	      connection,
	      max_depths))
	  return true;
      }
      return false;
      
}



template<typename T,int Dim>
void multi_edge_constraint_candidates(
	      Points<T,Dim> & npoint,
	      Points<T,Dim> ** & candidates,
	      int & numcandidates,
	      TConnecTProposal<T,Dim> & connection,
	      int max_depths,
	      int depths=0)
{
  if (depths+1>max_depths) 
      return;
  
  
  int num_edges=0;
  
  if (depths==0)
  {
    track_point_id++;
  }
  
  if (npoint.track_me_id==track_point_id)
    return;
  npoint.track_me_id=track_point_id;
  
  
  num_edges+=npoint.get_num_connections();
  
  
  if (connection.new_connection_exists)
  {
      if (connection.connection_new.pointA->point==&npoint)
	num_edges+=1;
      if (connection.connection_new.pointB->point==&npoint)
	num_edges+=1;
  }
  if (connection.old_connection_exists)
  {
      if (connection.connection_old->pointA->point==&npoint)
	num_edges-=1;
      if (connection.connection_old->pointB->point==&npoint)
	num_edges-=1;
  }  
  
  if (num_edges>2)
  {
      sta_assert_error(numcandidates<50);
      candidates[numcandidates]=&npoint;
      numcandidates++;
  }
  
  
  
  if ((depths==0)&&(connection.new_connection_exists))
    multi_edge_constraint_candidates(
			  *(connection.connection_new.pointB->point),
			  candidates,
			  numcandidates,
			  connection,
			  max_depths,
			  depths+1);
    
  //follow all (also probably removed) connection
  for (int end_id=0;end_id<2;end_id++)
  {
	
	class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	
	for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	{
	  if (endpointsA[i].connected!=NULL)
	  {
		multi_edge_constraint_candidates(
		      *(endpointsA[i].connected->point),
		      candidates,
		      numcandidates,						       
		      connection,
		      max_depths,
		      depths+1);
	  }
	}
  }
}




template<typename T,int Dim>
void do_label(Points<T,Dim> & npoint,
	      int & pointid,
	      int max_depths=100000,
	      int depths=0,
	      Points<T,Dim> * startpoint=NULL)
{
  if (depths>max_depths)
      return;
  
  if ((depths==0)&&(npoint.get_num_connections()==0))
  {
    return;
  }
  
  if (npoint.point_id>-1)
      return;
  
  if (depths==0)
  {
    track_id++;
    pointid++;
  }
  
  npoint.point_id=pointid;
  
  {
    for (int end_id=0;end_id<2;end_id++)
    {
	  //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	  class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	  
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      if (endpointsA[i].connection->track_me_id!=track_id)
	      {
		  endpointsA[i].connection->track_me_id=track_id;
		  
		  (do_label(*(endpointsA[i].connected->point),
			pointid,
			max_depths,
			depths+1,
			startpoint));
	      }
	    }
	  }
    }
  }
}


/*
template<typename T,int Dim>
bool nerve_edge_check(Points<T,Dim> & npoint,
	      TConnecTProposal<T,Dim> & connection,
	      int max_depths=1,
	      int depths=0,
	      Points<T,Dim> * startpoint=NULL)
{
  if (depths>max_depths)
      return false;
  
  //! first node should follow new connection
  if (depths==0)
  {
    if (connection.new_connection_exists)
    {
	track_id++;
	sta_assert_error(connection.connection_new.pointA->point==&npoint);
	return has_loop(*(connection.connection_new.pointB->point),
			connection,
			max_depths,
			depths+1,
			&npoint);
      
    }else
      return false; //! no new connection. so if there was no loop there is still no loop
  }else 
  {
    
    for (int end_id=0;end_id<2;end_id++)
    {
	  class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      //! check if edge is not the edge that will be deleted and edge has not been tracked so far
	      if (((!connection.old_connection_exists)||(endpointsA[i].connection!=connection.connection_old))
		&&(endpointsA[i].connection->track_me_id!=track_id))
	      {
		  endpointsA[i].connection->track_me_id=track_id;
		  
		  if (endpointsA[i].connected->point==startpoint)
		    return true;
		  
		  
		  if (has_loop(*(endpointsA[i].connected->point),
			connection,
			max_depths,
			depths+1,
			startpoint))
		  return true;
	      }
	    }
	  }
    }
  }
  return false;
    sta_assert_error(false);
}*/


template<typename T,int Dim>
bool path_length_with_bi(Points<T,Dim> & npoint,
	      int max_depths=1,
	      bool update_track_id=true,
	      int depths=0,
	      Points<T,Dim> * startpoint=NULL)
{
    if (depths>max_depths)
      return false;
  
  if (depths==0)
  {
//     if ((npoint.endpoint_connections[0]+npoint.endpoint_connections[1])>0)
//       return true;
    if (update_track_id)
      track_id++;
  }
  
  
  if ((npoint.endpoint_connections[0]>1)||
    (npoint.endpoint_connections[1]>1))
  {
    return true;
  }
  
  
  
    
  
  //! first node should follow new connection

  {
    for (int end_id=0;end_id<2;end_id++)
    {
	  class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      //! check if edge has not been tracked so far
	      if ((endpointsA[i].connection->track_me_id!=track_id))
	      {
		  endpointsA[i].connection->track_me_id=track_id;
		  
		  if (endpointsA[i].connected->point==startpoint)
		    continue;
		  
		  //endpoint_connections
		  
		  if (path_length_with_bi(*(endpointsA[i].connected->point),
			max_depths,
			update_track_id,
			depths+1,
			startpoint))
		    return true;
	      }
	    }
	  }
    }
  }
  return false;
    sta_assert_error(false);
}


// searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
template<typename T,int Dim>
bool  do_tracking2(
	       TConnecTProposal<T,Dim> & proposed_connection,
	       OctTreeNode<T,Dim> & tree,
	       Points<T,Dim> & npoint,
	       T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
	       T searchrad, // TODO currently ignored. using point scale instead
// 	       T TempConnect=1,
// 	       T L=0,
// 	       T sL=1,
// 	       int side=Points< T, Dim >::a_side)
	       T TempConnect,
	       T Lprior,
	       T L,
	       T sL,
	       T pS,
	       T tS,
	       T AngleScale,
	       bool noedgescales,
	       int side)
	       
{
  

    bool debug=false;
    
	
    if (debug)
	  printf("do_tracking\n");
      
  
    //std::vector<typename Points<T,Dim>::CEndpoint>  & p_pole=npoint.endpoints[side];
    class Points<T,Dim>::CEndpoint * p_pole=npoint.endpoints[side];
    
    std::size_t port_id=std::rand()%Points< T, Dim >::maxconnections;
    if (only_one_connecting==1)
      port_id=0;//std::rand()%Points< T, Dim >::maxconnections;
    
    class Points<T,Dim>::CEndpoint * enpt=&p_pole[port_id];
    class Points< T, Dim >::CEndpoint * hub=NULL;        
    
    
    int num_existing_sources=0;
    if (directed_connections!=-1)
    {
      num_existing_sources=npoint.get_num_sources(side);
      sta_assert_error(num_existing_sources<=directed_connections);
      if ((num_existing_sources>=directed_connections)&&((p_pole[port_id].connected==NULL)||(!enpt->is_source)))
	return false;
    }    
    
    /*!
     collect all points in search rectangle
     */
    npoint.update_endpoint(side);
    
    Vector<T,Dim> endpoint=npoint.endpoints_pos[side]; // REF OK?
    

    
  std::size_t found;
  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
  class OctPoints<T,Dim> ** candidates;
  candidates=query_buffer;
  
  try{
  tree.queryRange(
	npoint.endpoints_pos[side],search_connection_point_center_candidate,candidates,query_buffer_size,found); 
  } catch (mhs::STAError & error)
    {
     throw error;
    }
       
    sta_assert_error(npoint.maxconnections>0);
    
    /// no points nearby. do nothing
    if (found==1)
    {
      if (debug)
	printf("no points\n",found);
      return false;
    }    
    
    
	if (debug)
	    printf("found %d initial candidates in radius %f\n",found,searchrad);    

   
    searchrad*=searchrad;    
    search_connection_point_center_candidate*=search_connection_point_center_candidate;
	
    
	if (debug)
	    printf("collecting endpoints in circle\n");
    
	
	std::size_t endpoint_search_candidate_buffer_size=query_buffer_size;
	//std::list<class Points< T, Dim >::CEndpoint *> connet_candidates;	
	class Points< T, Dim >::CEndpoint * connet_candidates_buffer[endpoint_search_candidate_buffer_size];
	class Points< T, Dim >::CEndpoint ** connet_candidates=connet_candidates_buffer;
	
	std::size_t num_connet_candidates=0;
	{
	 
	  {
	        /*!
		  check for all candidates in the rectangle 
		  if one of the endpoints lies within the circle 
		    with radius "searchrad"
		*/
		 
		 
	        //typename std::list< class OctPoints<T,Dim> * >::iterator  iter=candidates.begin();
		//while (iter!=candidates.end())
		for (std::size_t a=0;a<found;a++) 
		{
		    //Points<T,Dim> * point=(Points<T,Dim> *)(*iter);
		    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    if (point!=&npoint) // no self connections ;-)
		    {
		      
//  		      	T scale_candidate=point->scale;
			T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-npoint.position).norm2();
			
			if (search_connection_point_center_candidate>candidate_center_dist_sq)
			{
			    for (int pointside=0;pointside<=1;pointside++)
			    {
				///! updating endpoints (not done automatically)
				point->update_endpoint(pointside);
				Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
				T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
			    if (debug)
				printf("endpointdist: %f\n",std::sqrt(candidate_dist_sq));
				
				/*!
				  if not in radius remove
				    point from further consideration
				*/
				if (searchrad>candidate_dist_sq)
				//if (endpoint_searchrad>candidate_dist_sq)
				{
				    class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
				    //printf("side: %d\n",freehub->side);
				    /*!
				      if there are no free connections, remove
					endpoint from further consideration
				    */
				    if (freehub!=NULL)
				    {
				      sta_assert_error(num_connet_candidates<endpoint_search_candidate_buffer_size);
				      (*connet_candidates++)=freehub;
				      num_connet_candidates++;
				      
				      //connet_candidates.push_back(freehub);
				    }
				}
			    }
			}
				
		    }
		  /*  iter++;	*/		
		}
		

	

	//! add existing connection
	if (p_pole[port_id].connected!=NULL)
	{
	    hub=p_pole[port_id].connected;
	    hub->point->update_endpoints();
	    sta_assert_error(hub!=NULL);
	    
	      T candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
	      if (searchrad>candidate_dist_sq) 
	      {
		//connet_candidates.push_back(hub);
		*connet_candidates++=hub;
		num_connet_candidates++;	      
	      }
	      else
	      {
		printf("%f %f\n",searchrad,candidate_dist_sq);
		hub->point->update_endpoints();
		candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
		printf("%f %f\n",searchrad,candidate_dist_sq);
		npoint.update_endpoints();
		candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
		printf("%f %f\n",searchrad,candidate_dist_sq);
		sta_assert_error(false);
		return false; //! not reversable 
	      }
	}else
	/// no points nearby and not connected -> do nothing
	if ((num_connet_candidates==0))
	{
	if (debug)
	  printf("no endpoints\n",num_connet_candidates);
	  
	  return false;
	}

	T min_cost=std::numeric_limits<T>::max();
		
		
	if (debug)
	    printf("computing probabilities for %d points\n",num_connet_candidates);
	
  

		T connet_cost[endpoint_search_candidate_buffer_size];
		T connet_prop[endpoint_search_candidate_buffer_size];
		T connet_prop_acc[endpoint_search_candidate_buffer_size];
		 
		
		class Points< T, Dim >::CEndpoint * connet_candidates_array[endpoint_search_candidate_buffer_size];	
		
// for (int i=0;i<endpoint_search_candidate_buffer_size;i++)
// connet_candidates_array[i]=NULL;
		
		connet_candidates=connet_candidates_buffer;
		int candidates_count=0;		
		 for (std::size_t i=0;i<num_connet_candidates;i++)   
		 {
		   
		   
		      class Points< T, Dim >::CEndpoint * candidate_hub=connet_candidates[i];
		      
		      connet_candidates_array[candidates_count]=candidate_hub;
		      
		      Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
		      Points<T,Dim> * candidate_point=candidate_hub->point;
		  
		      Vector<T,Dim> & candidate_point_pos=candidate_point->position;
		      T   candidate_point_size=candidate_point->scale;

		      
		      
		      
// 		     
			
			int inrange;
		      T u=ucon<T,Dim>(
			    npoint,
			    endpoint,
			    *candidate_point,
			    *(candidate_hub->position),
			    L,sL,pS,tS,AngleScale,searchrad,inrange);
		      sta_assert_error(inrange>-2);
		      
		      
		      if (debug)
		      {
		      printf("u:  %f\n",u);
		      printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
		      printf("(%f %f)\n",endpoint[0],endpoint[1]);		      
		      printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
		      printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// 		      sta_assert_error(false);
		      }
		      
		      connet_cost[candidates_count]=u;
		      
		      min_cost=std::min(min_cost,u);
		      
		      if (inrange>0)
			connet_prop[candidates_count]=mexp((u+Lprior-L)/TempConnect);
		      else 
			connet_prop[candidates_count]=0;
		      
		      if (candidates_count==0)
		      {
			connet_prop_acc[candidates_count]=connet_prop[candidates_count];
		      }
		      else
		      {
			connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
		      }
		      candidates_count++;
		  }
		
// 		#pragma omp parallel for num_threads(4) 
// 		  for (std::size_t i=0;i<num_connet_candidates;i++)   
// 		 {
// 		   
// 		   
// 		      class Points< T, Dim >::CEndpoint * candidate_hub=connet_candidates[i];
// 		      
// 		      connet_candidates_array[i]=candidate_hub;
// 		      
// 		      Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
// 		      Points<T,Dim> * candidate_point=candidate_hub->point;
// 		  
// 		      Vector<T,Dim> & candidate_point_pos=candidate_point->position;
// 		      T   candidate_point_size=candidate_point->scale;
// 
// 		      
// 		      
// 		      T offset=L;
// // 		     
// 			
// 			bool inrange;
// 		      T u=ucon<T,Dim>(
// 			    npoint,
// 			    endpoint,
// 			    *candidate_point,
// 			    *(candidate_hub->position),
// 			    offset,searchrad,inrange);
// 		      sta_assert_error(inrange);
// 		      
// 		      
// 		      if (debug)
// 		      {
// 		      printf("u:  %f\n",u);
// 		      printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
// 		      printf("(%f %f)\n",endpoint[0],endpoint[1]);		      
// 		      printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
// 		      printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// // 		      sta_assert_error(false);
// 		      }
// 		      
// 		      connet_cost[i]=u;
// 		      connet_prop[i]=mexp(u/TempConnect);
// 		  }
// 		  
// 		  int candidates_count=0;
// 		  for (std::size_t i=0;i<num_connet_candidates;i++)   
// 		  {	    
// 		      if (candidates_count==0)
// 		      {
// 			connet_prop_acc[candidates_count]=connet_prop[candidates_count];
// 		      }
// 		      else
// 		      {
// 			connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
// 		      }
// 		      candidates_count++;
// 		    }
		

		//! since connect_prob=1 -> e^0
		connet_cost[candidates_count]=0;
		//connet_cost[candidates_count]=L*(1-npoint.vesselness);
		if (noedgescales)
		connet_cost[candidates_count]=min_cost+Lprior-L;
		
			
		T const_no_connection_weight=1;
  		if (connet_cost[candidates_count]!=0)
  		  const_no_connection_weight=mexp(connet_cost[candidates_count]/TempConnect);
		
		
		/// adding the "do not connect" proposal propability
 		if (candidates_count==0)
 		  connet_prop_acc[candidates_count]=const_no_connection_weight;
 		else
		{
 		  connet_prop_acc[candidates_count]=connet_prop_acc[candidates_count-1]+const_no_connection_weight;
		}
		connet_prop[candidates_count]=const_no_connection_weight;
		

		
	if (debug)
	    printf("creating proposal\n");
	
// for (int i=0;i<num_connet_candidates;i++)
// {
//   if (connet_candidates_array[i]==NULL)
//   {
//    printf("%d %d\n",i,num_connet_candidates); 
//   }
//   sta_assert_error(connet_candidates_array[i]!=NULL);	
// }
	
		
		/*!
		  now we choose a candidate with probability connet_prop
		  using connet_prop_acc
		  
		  and we randomliy (uniformly) choose one port from the current point
		  (if there is no port, we create one)
		*/		
		{
		  std::size_t connection_candidate=rand_pic_array(connet_prop_acc,1+num_connet_candidates,connet_prop_acc[candidates_count]);
		  
	  
		  

if (debug)
{
		  for (int g=0;g<candidates_count+1;g++)
		  {
		      printf("%f,",connet_prop_acc[g]);
		  }	
		  printf("\n");
		  for (int g=0;g<candidates_count;g++)
		  {
		      printf("%f,",connet_cost[g]);
		  }	
		  printf("\n");
		  for (int g=0;g<candidates_count;g++)
		  {
		      printf("%f,",connet_prop[g]);
		  }	
		  printf("\n");			  
}		  


	if (debug)
	    printf("choosing %d\n",connection_candidate);

			    
		  sta_assert_error(connection_candidate>=0);
		  sta_assert_error(connection_candidate<=num_connet_candidates);
	
		  proposed_connection.connection_old=(p_pole[port_id].connection);
		  proposed_connection.old_connection_exists=(p_pole[port_id].connected!=NULL);  
		  
		  
		  if (!proposed_connection.old_connection_exists)
		  {
		    //! same as old conection (no connection)
		    if (connection_candidate==num_connet_candidates)
		      return false;
		    
		    proposed_connection.old_prob=const_no_connection_weight/connet_prop_acc[candidates_count];
		    proposed_connection.old_cost=connet_cost[candidates_count];
		  }else
		  {
		    //! same as old conection
		    //if (connection_candidate==connet_candidates.size()-1)
//sta_assert_error((connet_candidates_array[connection_candidate]!=NULL)||(connection_candidate==num_connet_candidates));
		    
		     if ((connection_candidate<num_connet_candidates)&&(p_pole[port_id].connected==connet_candidates_array[connection_candidate]))
		      return false;
		    
		    proposed_connection.old_prob=connet_prop[candidates_count-1]/connet_prop_acc[candidates_count];
		    proposed_connection.old_cost=connet_cost[candidates_count-1];
		  }
		  
		  
		  
		  
		  
		  /*!
		   * the "no connection proposal"
		   */
		  if (connection_candidate==num_connet_candidates)
		  {
		    	if (debug)
			  printf("no connection proposal\n");
	    
		    proposed_connection.new_prob=const_no_connection_weight/connet_prop_acc[candidates_count];
		    proposed_connection.new_connection_exists=false;
		    proposed_connection.new_cost=connet_cost[candidates_count];
		    return true;
		  }else
		  /*!
		   * propose a connection
		   */
		  {
		    	if (debug)
			  printf("connection proposal\n");		    
		    
		      sta_assert_error(connection_candidate>=0);
		      sta_assert_error(connection_candidate<num_connet_candidates);
		    

			proposed_connection.connection_new.pointA=&(p_pole[port_id]);
		     
// 		      if (connection_candidate<connet_candidates.size()-currently_connected)
// 			proposed_connection.connection_new.pointA=&(p_pole[port_id]);
// 		      else
// 			proposed_connection.connection_new.pointA=&(p_pole[currently_connected_ports[connection_candidate+currently_connected-connet_candidates.size()]]);

		      
		      sta_assert_error(connet_candidates_array[connection_candidate]!=NULL);
		      proposed_connection.connection_new.pointB=connet_candidates_array[connection_candidate];
		      
		      if (debug)
		      printf("side %d with side %d\n",connet_candidates_array[connection_candidate]->side,p_pole[port_id].side);
		      
		      
		      proposed_connection.new_cost=connet_cost[connection_candidate];
		      proposed_connection.new_prob=connet_prop[connection_candidate]/connet_prop_acc[candidates_count];
		      proposed_connection.new_connection_exists=true;
		      //proposed_connection.new_prob_no_connection=const_no_connection_weight/connet_prop_acc[candidates_count];		      
		     
		      sta_assert_error(proposed_connection.connection_new.pointA!=NULL);
		      sta_assert_error(proposed_connection.connection_new.pointB!=NULL);	      
			//throw mhs::STAError("B is null");
		      
		      if (debug)
   		       printf("conn index %d\n",connection_candidate);		      
		      return true;
		  }
		  
		}
		
	  }
	}
	
	throw mhs::STAError("should not be reached!");
}
 
 


// searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
template<typename T,int Dim>
bool  do_tracking(
	       TConnecTProposal<T,Dim> & proposed_connection,
	       OctTreeNode<T,Dim> & tree,
	       Points<T,Dim> & npoint,
	       T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
	       T searchrad, // TODO currently ignored. using point scale instead
	       T TempConnect=1,
	       T L=0,
	       int side=Points< T, Dim >::a_side)
{
  

    bool debug=false;
    
	
    if (debug)
	  printf("do_tracking\n");
      
  
    std::vector<typename Points<T,Dim>::CEndpoint>  & p_pole=npoint.endpoints[side];
    
    std::size_t port_id=std::rand()%Points< T, Dim >::maxconnections;
    if (only_one_connecting==1)
      port_id=0;//std::rand()%Points< T, Dim >::maxconnections;
    
    class Points<T,Dim>::CEndpoint * enpt=&p_pole[port_id];
    class Points< T, Dim >::CEndpoint * hub=NULL;        
    
    
    int num_existing_sources=0;
    if (directed_connections!=-1)
    {
      num_existing_sources=npoint.get_num_sources(side);
      sta_assert_error(num_existing_sources<=directed_connections);
      if ((num_existing_sources>=directed_connections)&&((p_pole[port_id].connected==NULL)||(!enpt->is_source)))
	return false;
    }    
    
    /*!
     collect all points in search rectangle
     */
    npoint.update_endpoint(side);
    
    Vector<T,Dim> endpoint=npoint.endpoints_pos[side]; // REF OK?
    

    
  std::size_t found;
  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
  class OctPoints<T,Dim> ** candidates;
  candidates=query_buffer;
  
  try{
  tree.queryRange(
	npoint.endpoints_pos[side],search_connection_point_center_candidate,candidates,query_buffer_size,found); 
  } catch (mhs::STAError & error)
    {
     throw error;
    }
       
    sta_assert_error(npoint.maxconnections>0);
    
    /// no points nearby. do nothing
    if (found==1)
    {
      if (debug)
	printf("no points\n",found);
      return false;
    }    
    
    
	if (debug)
	    printf("found %d initial candidates in radius %f\n",found,searchrad);    

   
    searchrad*=searchrad;    
    search_connection_point_center_candidate*=search_connection_point_center_candidate;
	
    
	if (debug)
	    printf("collecting endpoints in circle\n");
    
	
	
	std::list<class Points< T, Dim >::CEndpoint *> connet_candidates;	
	
	{
	  int candidates_count=0;
	  {
	        /*!
		  check for all candidates in the rectangle 
		  if one of the endpoints lies within the circle 
		    with radius "searchrad"
		*/
		 
		 
	        //typename std::list< class OctPoints<T,Dim> * >::iterator  iter=candidates.begin();
		//while (iter!=candidates.end())
		for (std::size_t a=0;a<found;a++) 
		{
		    //Points<T,Dim> * point=(Points<T,Dim> *)(*iter);
		    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    if (point!=&npoint) // no self connections ;-)
		    {
		      
//  		      	T scale_candidate=point->scale;
			T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-npoint.position).norm2();
			
			if (search_connection_point_center_candidate>candidate_center_dist_sq)
			{
			    for (int pointside=0;pointside<=1;pointside++)
			    {
				///! updating endpoints (not done automatically)
				point->update_endpoint(pointside);
				Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
				T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
			    if (debug)
				printf("endpointdist: %f\n",std::sqrt(candidate_dist_sq));
				
				/*!
				  if not in radius remove
				    point from further consideration
				*/
				if (searchrad>candidate_dist_sq)
				//if (endpoint_searchrad>candidate_dist_sq)
				{
				    class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
				    //printf("side: %d\n",freehub->side);
				    /*!
				      if there are no free connections, remove
					endpoint from further consideration
				    */
				    if (freehub!=NULL)
				    {
				      connet_candidates.push_back(freehub);
				    }
				}
			    }
			}
				
		    }
		  /*  iter++;	*/		
		}
		
	
if (true)
	//! add existing connection
	if (p_pole[port_id].connected!=NULL)
	{
	    hub=p_pole[port_id].connected;
	    sta_assert_error(hub!=NULL);
	    
	      T candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
	      if (searchrad>candidate_dist_sq) 
		connet_candidates.push_back(hub);
	      else
	      {
		sta_assert_error(false);
		return false; //! not reversable 
	      }
	}else
	/// no points nearby and not connected -> do nothing
	if ((connet_candidates.size()==0))
	{
	if (debug)
	  printf("no endpoints\n",connet_candidates.size());
	  
	  return false;
	}
else // seems 2b incorrect
{
	if (p_pole[port_id].connected!=NULL)
	{
	    hub=p_pole[port_id].connected;
	    sta_assert_error(hub!=NULL);
	    
	      T candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
	      if (searchrad>candidate_dist_sq) 
	      {
		//connet_candidates.push_back(hub);
	      }
	      else
	      {
		return false; //! not reversable 
	      }
	}else
	/// no points nearby and not connected -> do nothing
	if ((connet_candidates.size()==0))
	{
	if (debug)
	  printf("no endpoints\n",connet_candidates.size());
	  
	  return false;
	}
}
	
		
		
	if (debug)
	    printf("computing probabilities for %d points\n",connet_candidates.size());
	
  

		std::vector<T> connet_cost;
		connet_cost.resize(connet_candidates.size()+1);
		std::vector<T> connet_prop;
		connet_prop.resize(connet_candidates.size()+1);
		std::vector<T> connet_prop_acc;
		connet_prop_acc.resize(connet_candidates.size()+1);
		candidates_count=0;
		std::vector<class Points< T, Dim >::CEndpoint *> connet_candidates_array;	
		connet_candidates_array.resize(connet_candidates.size());
// 		T const_no_connection_weight=0;
		
		
		for (typename std::list<class Points< T, Dim >::CEndpoint *>::iterator conn_iter=connet_candidates.begin();
		      conn_iter!=connet_candidates.end();
		      conn_iter++)
		 {
		      class Points< T, Dim >::CEndpoint * candidate_hub=*conn_iter;
		      
		      connet_candidates_array[candidates_count]=candidate_hub;
		      
		      Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
		      Points<T,Dim> * candidate_point=candidate_hub->point;
		  
		      Vector<T,Dim> & candidate_point_pos=candidate_point->position;
		      T   candidate_point_size=candidate_point->scale;

		      
		      
		      T offset=L;
// 		     
			
			int inrange;
		      T u=ucon<T,Dim>(
			    npoint,
			    endpoint,
			    *candidate_point,
			    *(candidate_hub->position),
			    offset,searchrad,inrange);
		      sta_assert_error(inrange>-2);
		      
		      
		      if (debug)
		      {
		      printf("u:  %f\n",u);
		      printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
		      printf("(%f %f)\n",endpoint[0],endpoint[1]);		      
		      printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
		      printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// 		      sta_assert_error(false);
		      }
		      
		      connet_cost[candidates_count]=u;
		      if (inrange>0)
			connet_prop[candidates_count]=mexp(u/TempConnect);
		      else 
			connet_prop[candidates_count]=0;
		      
		      if (candidates_count==0)
		      {
			connet_prop_acc[candidates_count]=connet_prop[candidates_count];
		      }
		      else
		      {
			connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
		      }
		      candidates_count++;
		  }
	
		//! since connect_prob=1 -> e^0
		connet_cost[candidates_count]=0;
		//connet_cost[candidates_count]=L*(1-npoint.vesselness);
		
			
		T const_no_connection_weight=1;
  		if (connet_cost[candidates_count]!=0)
  		  const_no_connection_weight=mexp(connet_cost[candidates_count]/TempConnect);
		
		/// adding the "do not connect" proposal propability
 		if (candidates_count==0)
 		  connet_prop_acc[candidates_count]=const_no_connection_weight;
 		else
		{
 		  connet_prop_acc[candidates_count]=connet_prop_acc[candidates_count-1]+const_no_connection_weight;
		}
		connet_prop[candidates_count]=const_no_connection_weight;
		

		
	if (debug)
	    printf("creating proposal\n");
	
		
		/*!
		  now we choose a candidate with probability connet_prop
		  using connet_prop_acc
		  
		  and we randomliy (uniformly) choose one port from the current point
		  (if there is no port, we create one)
		*/		
		{
		  std::size_t connection_candidate=rand_pic_array(connet_prop_acc.data(),1+connet_candidates.size(),connet_prop_acc[candidates_count]);
		  
	  
		  

if (debug)
{
		  for (int g=0;g<candidates_count+1;g++)
		  {
		      printf("%f,",connet_prop_acc[g]);
		  }	
		  printf("\n");
		  for (int g=0;g<candidates_count;g++)
		  {
		      printf("%f,",connet_cost[g]);
		  }	
		  printf("\n");
		  for (int g=0;g<candidates_count;g++)
		  {
		      printf("%f,",connet_prop[g]);
		  }	
		  printf("\n");			  
}		  


	if (debug)
	    printf("choosing %d\n",connection_candidate);

			    
		  sta_assert_error(connection_candidate>=0);
		  sta_assert_error(connection_candidate<=connet_candidates.size());
	
		  proposed_connection.connection_old=(p_pole[port_id].connection);
		  proposed_connection.old_connection_exists=(p_pole[port_id].connected!=NULL);  
		  
		  
		  if (!proposed_connection.old_connection_exists)
		  {
		    //! same as old conection (no connection)
		    if (connection_candidate==connet_candidates.size())
		      return false;
		    
		    proposed_connection.old_prob=const_no_connection_weight/connet_prop_acc[candidates_count];
		    proposed_connection.old_cost=connet_cost[candidates_count];
		  }else
		  {
		    //! same as old conection
		    //if (connection_candidate==connet_candidates.size()-1)
		     if (p_pole[port_id].connected==connet_candidates_array[connection_candidate])
		      return false;
		    
		    proposed_connection.old_prob=connet_prop[candidates_count-1]/connet_prop_acc[candidates_count];
		    proposed_connection.old_cost=connet_cost[candidates_count-1];
		  }
		  
		  
		  
		  
		  
		  /*!
		   * the "no connection proposal"
		   */
		  if (connection_candidate==connet_candidates.size())
		  {
		    	if (debug)
			  printf("no connection proposal\n");
	    
		    proposed_connection.new_prob=const_no_connection_weight/connet_prop_acc[candidates_count];
		    proposed_connection.new_connection_exists=false;
		    proposed_connection.new_cost=connet_cost[candidates_count];
		    return true;
		  }else
		  /*!
		   * propose a connection
		   */
		  {
		    	if (debug)
			  printf("connection proposal\n");		    
		    
		      sta_assert_error(connection_candidate>=0);
		      sta_assert_error(connection_candidate<connet_candidates.size());
		    

			proposed_connection.connection_new.pointA=&(p_pole[port_id]);
		     
// 		      if (connection_candidate<connet_candidates.size()-currently_connected)
// 			proposed_connection.connection_new.pointA=&(p_pole[port_id]);
// 		      else
// 			proposed_connection.connection_new.pointA=&(p_pole[currently_connected_ports[connection_candidate+currently_connected-connet_candidates.size()]]);

		      
		      
		      proposed_connection.connection_new.pointB=connet_candidates_array[connection_candidate];
		      
		      if (debug)
		      printf("side %d with side %d\n",connet_candidates_array[connection_candidate]->side,p_pole[port_id].side);
		      
		      
		      proposed_connection.new_cost=connet_cost[connection_candidate];
		      proposed_connection.new_prob=connet_prop[connection_candidate]/connet_prop_acc[candidates_count];
		      proposed_connection.new_connection_exists=true;
		      //proposed_connection.new_prob_no_connection=const_no_connection_weight/connet_prop_acc[candidates_count];		      
		     
		      sta_assert_error(proposed_connection.connection_new.pointA!=NULL);
		      sta_assert_error(proposed_connection.connection_new.pointB!=NULL);	      
			//throw mhs::STAError("B is null");
		      
		      if (debug)
   		       printf("conn index %d\n",connection_candidate);		      
		      return true;
		  }
		  
		}
		
	  }
	}
	
	throw mhs::STAError("should not be reached!");
}
 
 
 

// searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
template<typename T,int Dim>
bool  do_track_birth(
	       TConnecTProposal<T,Dim> & proposed_connection,
	       OctTreeNode<T,Dim> & tree,
	       Points<T,Dim> & npoint,
	       T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
	       T searchrad, // TODO currently ignored. using point scale instead
	       T TempConnect=1,
	       T L=0)
{
    bool debug=false;
    
	
    if (debug)
	  printf("do_tracking\n");
    
    
    std::list<class Points< T, Dim >::CEndpoint *> connet_candidates;	
    std::list<int> connet_side;	
    std::size_t port_id=0;
    
    /*!
     collect all points in search rectangle
    */
//     AABB<T,Dim>   range;
//     Vector<T,Dim> halfDimension;
//     halfDimension=search_connection_point_center_candidate;
    
    for (int side=0;side<2;side++)
    {
	npoint.update_endpoint(side);
// 	range.setAABB(npoint.endpoints_pos[side].v,halfDimension.v);
	
	Vector<T,Dim> endpoint=npoint.endpoints_pos[side]; // REF OK?
	
	std::list<class OctPoints< T, Dim > *>  candidates;
	//tree.queryRange(range,candidates);
	tree.queryRange(npoint.endpoints_pos[side],search_connection_point_center_candidate,candidates);
      
	
		typename std::list< class OctPoints<T,Dim> * >::iterator  iter=candidates.begin();
		
		if (candidates.size()>1) // more point than the point itself
		while (iter!=candidates.end())
		{
		    Points<T,Dim> * point=(Points<T,Dim> *)(*iter);
		    if (point!=&npoint) // no self connections ;-)
		    {
		      
//  		      	T scale_candidate=point->scale;
			T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-npoint.position).norm2();
			
			if (search_connection_point_center_candidate>candidate_center_dist_sq)
			{
			    for (int pointside=0;pointside<=1;pointside++)
			    {
				///! updating endpoints (not done automatically)
				point->update_endpoint(pointside);
				Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
				T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
			    if (debug)
				printf("endpointdist: %f\n",std::sqrt(candidate_dist_sq));
				
				/*!
				  if not in radius remove
				    point from further consideration
				*/
				if (searchrad>candidate_dist_sq)
				//if (endpoint_searchrad>candidate_dist_sq)
				{
				    class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
				    //printf("side: %d\n",freehub->side);
				    /*!
				      if there are no free connections, remove
					endpoint from further consideration
				    */
				    if (freehub!=NULL)
				    {
				      connet_candidates.push_back(freehub);
				      connet_side.push_back(side);
				    }
				}
			    }
			}
		    }
		    iter++;			
		}  
    }
    
    if (connet_candidates.size()==0)
      return false; // no candidates
      
  
  
    int candidates_count=0;
    {
	if (debug)
	    printf("computing probabilities for %d points\n",connet_candidates.size());
  
	  std::vector<T> connet_cost;
	  connet_cost.resize(connet_candidates.size());
	  std::vector<T> connet_prop;
	  connet_prop.resize(connet_candidates.size());
	  std::vector<T> connet_prop_acc;
	  connet_prop_acc.resize(connet_candidates.size());
	  std::vector<int> connet_side_indx;
	  connet_side_indx.resize(connet_candidates.size());
	  
	  candidates_count=0;
	  std::vector<class Points< T, Dim >::CEndpoint *> connet_candidates_array;	
	  connet_candidates_array.resize(connet_candidates.size());
	  
	  typename std::list<int>::iterator conn_side_iter=connet_side.begin();
	  
	  for (typename std::list<class Points< T, Dim >::CEndpoint *>::iterator conn_iter=connet_candidates.begin();
		conn_iter!=connet_candidates.end();
		conn_iter++,conn_side_iter++)
	    {
		class Points< T, Dim >::CEndpoint * candidate_hub=*conn_iter;
		int side=*conn_side_iter;
		connet_side_indx[candidates_count]=side;
		Vector<T,Dim> endpoint=npoint.endpoints_pos[side]; // REF OK?
		
		connet_candidates_array[candidates_count]=candidate_hub;
		
		Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
		Points<T,Dim> * candidate_point=candidate_hub->point;
	    
		Vector<T,Dim> & candidate_point_pos=candidate_point->position;
		T   candidate_point_size=candidate_point->scale;
		
		T offset=L;
		  
		T u=ucon<T,Dim>(npoint.position,
		      endpoint,
		      npoint.scale,
		      candidate_point_pos,
		      candidate_endpoint_pos,
		      candidate_point_size,
		      offset);
		
		if (debug)
		{
		printf("u:  %f\n",u);
		printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
		printf("(%f %f)\n",endpoint[0],endpoint[1]);		      
		printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
		printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// 		      sta_assert_error(false);
		}
		
		connet_cost[candidates_count]=u;
		connet_prop[candidates_count]=mexp(u/TempConnect);
		
		if (candidates_count==0)
		{
		  connet_prop_acc[candidates_count]=connet_prop[candidates_count];
		}
		else
		{
		  connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
		}
		candidates_count++;
	    }
  
	  sta_assert_error(candidates_count>0);
	  candidates_count--; //deting the index to the last array elelemtn
	  
  if (debug)
      printf("creating proposal\n");
	  
	  /*!
	    now we choose a candidate with probability connet_prop
	    using connet_prop_acc
	    
	    and we randomliy (uniformly) choose one port from the current point
	    (if there is no port, we create one)
	  */		
	  {
	    std::size_t connection_candidate=rand_pic_array(connet_prop_acc.data(),connet_candidates.size(),connet_prop_acc[candidates_count]);
	    
    
	    

if (debug)
{
	    for (int g=0;g<candidates_count+1;g++)
	    {
		printf("%f,",connet_prop_acc[g]);
	    }	
	    printf("\n");
	    for (int g=0;g<candidates_count;g++)
	    {
		printf("%f,",connet_cost[g]);
	    }	
	    printf("\n");
	    for (int g=0;g<candidates_count;g++)
	    {
		printf("%f,",connet_prop[g]);
	    }	
	    printf("\n");			  
}		  


  if (debug)
      printf("choosing %d\n",connection_candidate);

	    sta_assert_error(connection_candidate>=0);
	    sta_assert_error(connection_candidate<=connet_candidates.size());
	    
	    
	    
	     std::vector<typename Points<T,Dim>::CEndpoint>  & p_pole=npoint.endpoints[connet_side_indx[connection_candidate]];
	    

	    // old connection does not exists (birthday)
	    proposed_connection.old_connection_exists=false;  
	    
	    /*!
	      * propose a connection
	      */
	    {
		  if (debug)
		    printf("connection proposal\n");		    
	      
		sta_assert_error(connection_candidate>=0);
		sta_assert_error(connection_candidate<connet_candidates.size());
	      

		proposed_connection.connection_new.pointA=&(p_pole[port_id]);
		proposed_connection.connection_new.pointB=connet_candidates_array[connection_candidate];
		
		if (debug)
		printf("side %d with side %d\n",connet_candidates_array[connection_candidate]->side,p_pole[port_id].side);
		
		
		proposed_connection.new_cost=connet_cost[connection_candidate];
		proposed_connection.new_prob=connet_prop[connection_candidate]/connet_prop_acc[candidates_count];
		proposed_connection.new_connection_exists=true;
		//proposed_connection.new_prob_no_connection=const_no_connection_weight/connet_prop_acc[candidates_count];		      
		
		sta_assert_error(proposed_connection.connection_new.pointA!=NULL);
		sta_assert_error(proposed_connection.connection_new.pointB!=NULL);	      
		  //throw mhs::STAError("B is null");
		
		if (debug)
		  printf("conn index %d\n",connection_candidate);		      
		return true;
	    }
	    
	  }
	  
    }

  /*
  
  
  
  
      
    std::vector<typename Points<T,Dim>::CEndpoint>  & p_pole=npoint.endpoints[side];
    
    std::size_t port_id=std::rand()%Points< T, Dim >::maxconnections;
        
    class Points<T,Dim>::CEndpoint * enpt=&p_pole[port_id];
    class Points< T, Dim >::CEndpoint * hub=NULL;        
    
  
    

    
    

    sta_assert_error(npoint.maxconnections>0);
    
    /// no points nearby. do nothing
    if (candidates.size()==1)
    {
      if (debug)
	printf("no points\n",candidates.size());
      return false;
    }    
   
    searchrad*=searchrad;    
    search_connection_point_center_candidate*=search_connection_point_center_candidate;
	
    
	if (debug)
	    printf("collecting endpoints in circle\n");
    
	
	

	
	{
	  
	}*/
	
	throw mhs::STAError("should not be reached!");
} 



// searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
template<typename T,int Dim>
bool  do_track_death(
	       TConnecTProposal<T,Dim> & proposed_connection,
	       OctTreeNode<T,Dim> & tree,
	       Points<T,Dim> & npoint,
	       T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
	       T searchrad, // TODO currently ignored. using point scale instead
	       T TempConnect=1,
	       T L=0,
	       int side=Points< T, Dim >::a_side)
{
   bool debug=false;
    
	
    if (debug)
	  printf("do_tracking\n");
    
    
    std::list<class Points< T, Dim >::CEndpoint *> connet_candidates;	
    std::list<int> connet_side;	
    std::size_t port_id=0;
    
    /*!
     collect all points in search rectangle
    */
//     AABB<T,Dim>   range;
//     Vector<T,Dim> halfDimension;
//     halfDimension=search_connection_point_center_candidate;
    
    for (int side=0;side<2;side++)
    {
	npoint.update_endpoint(side);
// 	range.setAABB(npoint.endpoints_pos[side].v,halfDimension.v);
	
	Vector<T,Dim> endpoint=npoint.endpoints_pos[side]; // REF OK?
	
	std::list<class OctPoints< T, Dim > *>  candidates;
	//tree.queryRange(range,candidates);
	tree.queryRange(npoint.endpoints_pos[side],search_connection_point_center_candidate,candidates);
      
	
		typename std::list< class OctPoints<T,Dim> * >::iterator  iter=candidates.begin();
		
		if (candidates.size()>1) // more point than the point itself
		while (iter!=candidates.end())
		{
		    Points<T,Dim> * point=(Points<T,Dim> *)(*iter);
		    if (point!=&npoint) // no self connections ;-)
		    {
		      
//  		      	T scale_candidate=point->scale;
			T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-npoint.position).norm2();
			
			if (search_connection_point_center_candidate>candidate_center_dist_sq)
			{
			    for (int pointside=0;pointside<=1;pointside++)
			    {
				///! updating endpoints (not done automatically)
				point->update_endpoint(pointside);
				Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
				T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
			    if (debug)
				printf("endpointdist: %f\n",std::sqrt(candidate_dist_sq));
				
				/*!
				  if not in radius remove
				    point from further consideration
				*/
				if (searchrad>candidate_dist_sq)
				//if (endpoint_searchrad>candidate_dist_sq)
				{
				    class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
				    //printf("side: %d\n",freehub->side);
				    /*!
				      if there are no free connections, remove
					endpoint from further consideration
				    */
				    if (freehub!=NULL)
				    {
				      connet_candidates.push_back(freehub);
				      connet_side.push_back(side);
				    }
				}
			    }
			}
		    }
		    iter++;			
		}  
    }
    
    if (connet_candidates.size()==0)
      return false; // no candidates
      
  
  
    int candidates_count=0;
    {
	if (debug)
	    printf("computing probabilities for %d points\n",connet_candidates.size());
  
	  std::vector<T> connet_cost;
	  connet_cost.resize(connet_candidates.size());
	  std::vector<T> connet_prop;
	  connet_prop.resize(connet_candidates.size());
	  std::vector<T> connet_prop_acc;
	  connet_prop_acc.resize(connet_candidates.size());
	  std::vector<int> connet_side_indx;
	  connet_side_indx.resize(connet_candidates.size());
	  
	  candidates_count=0;
	  std::vector<class Points< T, Dim >::CEndpoint *> connet_candidates_array;	
	  connet_candidates_array.resize(connet_candidates.size());
	  
	  typename std::list<int>::iterator conn_side_iter=connet_side.begin();
	  
	  for (typename std::list<class Points< T, Dim >::CEndpoint *>::iterator conn_iter=connet_candidates.begin();
		conn_iter!=connet_candidates.end();
		conn_iter++,conn_side_iter++)
	    {
		class Points< T, Dim >::CEndpoint * candidate_hub=*conn_iter;
		int side=*conn_side_iter;
		connet_side_indx[candidates_count]=side;
		Vector<T,Dim> endpoint=npoint.endpoints_pos[side]; // REF OK?
		
		connet_candidates_array[candidates_count]=candidate_hub;
		
		Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
		Points<T,Dim> * candidate_point=candidate_hub->point;
	    
		Vector<T,Dim> & candidate_point_pos=candidate_point->position;
		T   candidate_point_size=candidate_point->scale;
		
		T offset=L;
		  
		T u=ucon<T,Dim>(npoint.position,
		      endpoint,
		      npoint.scale,
		      candidate_point_pos,
		      candidate_endpoint_pos,
		      candidate_point_size,
		      offset);
		
		if (debug)
		{
		printf("u:  %f\n",u);
		printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
		printf("(%f %f)\n",endpoint[0],endpoint[1]);		      
		printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
		printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// 		      sta_assert_error(false);
		}
		
		connet_cost[candidates_count]=u;
		connet_prop[candidates_count]=mexp(u/TempConnect);
		
		if (candidates_count==0)
		{
		  connet_prop_acc[candidates_count]=connet_prop[candidates_count];
		}
		else
		{
		  connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
		}
		candidates_count++;
	    }
  
	  sta_assert_error(candidates_count>0);
	  candidates_count--; //deting the index to the last array elelemtn
	  
  if (debug)
      printf("creating proposal\n");
	  
	  /*!
	    now we choose a candidate with probability connet_prop
	    using connet_prop_acc
	    
	    and we randomliy (uniformly) choose one port from the current point
	    (if there is no port, we create one)
	  */		
	  {
	    std::size_t connection_candidate=rand_pic_array(connet_prop_acc.data(),connet_candidates.size(),connet_prop_acc[candidates_count]);
	    
    
	    

if (debug)
{
	    for (int g=0;g<candidates_count+1;g++)
	    {
		printf("%f,",connet_prop_acc[g]);
	    }	
	    printf("\n");
	    for (int g=0;g<candidates_count;g++)
	    {
		printf("%f,",connet_cost[g]);
	    }	
	    printf("\n");
	    for (int g=0;g<candidates_count;g++)
	    {
		printf("%f,",connet_prop[g]);
	    }	
	    printf("\n");			  
}		  


  if (debug)
      printf("choosing %d\n",connection_candidate);

	    sta_assert_error(connection_candidate>=0);
	    sta_assert_error(connection_candidate<=connet_candidates.size());
	    
	    
	    
	     std::vector<typename Points<T,Dim>::CEndpoint>  & p_pole=npoint.endpoints[connet_side_indx[connection_candidate]];
	    

	    // old connection does not exists (birthday)
	    proposed_connection.old_connection_exists=false;  
	    
	    /*!
	      * propose a connection
	      */
	    {
		  if (debug)
		    printf("connection proposal\n");		    
	      
		sta_assert_error(connection_candidate>=0);
		sta_assert_error(connection_candidate<connet_candidates.size());
	      

		proposed_connection.connection_new.pointA=&(p_pole[port_id]);
		proposed_connection.connection_new.pointB=connet_candidates_array[connection_candidate];
		
		if (debug)
		printf("side %d with side %d\n",connet_candidates_array[connection_candidate]->side,p_pole[port_id].side);
		
		
		proposed_connection.new_cost=connet_cost[connection_candidate];
		proposed_connection.new_prob=connet_prop[connection_candidate]/connet_prop_acc[candidates_count];
		proposed_connection.new_connection_exists=true;
		//proposed_connection.new_prob_no_connection=const_no_connection_weight/connet_prop_acc[candidates_count];		      
		
		sta_assert_error(proposed_connection.connection_new.pointA!=NULL);
		sta_assert_error(proposed_connection.connection_new.pointB!=NULL);	      
		  //throw mhs::STAError("B is null");
		
		if (debug)
		  printf("conn index %d\n",connection_candidate);		      
		return true;
	    }
	    
	  }
	  
    }
	throw mhs::STAError("should not be reached!");
  
}



#endif
