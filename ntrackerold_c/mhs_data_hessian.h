#ifndef MHS_DATA_HESSIANL_H
#define MHS_DATA_HESSIANL_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataHessian: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * hessian;
    const TData * gradient;
    const TData * gradient_v;
    int num_alphas_hessian;
    int num_alphas_gradient;
//     bool soverride=false;

    const TData * debug;
    T min_scale;
    T max_scale;
    T scale_correction;
    T logscalefac;

    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
    T DataScaleGrad;

//     std::size_t shape[3];


public:
//     template<typename S>
//     void setshape(S  shape[])
//       {
// 	for (int i=0;i<Dim;i++)
// 	{
// 	  sta_assert(this->shape[i]>0);
// 	  shape[i]=this->shape[i];
// 	}
//       }

    float get_minscale()
    {
//        if (soverride)
// 	 return min_scale;
        return min_scale/scale_correction;
    };
    float get_maxscale()
    {
//       if (soverride)
// 	 return max_scale;
        return max_scale/scale_correction;
    };
    float get_scalecorrection()
    {
	//return 1.0/scale_correction;
	//return scale_correction;
          return 1;
    };


    CDataHessian()
    {
//       shape[0]=shape[1]=shape[2]=0;
    }

    ~CDataHessian()
    {
    }

    void init(const mxArray * feature_struct)
    {
//         for (int i=0; i<3; i++)
//             this->this->shape[i]=this->shape[i];
        for (int i=0; i<3; i++)
            this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
        std::swap(this->shape[0],this->shape[2]);


        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"alphas_hessian");
            hessian=tmp.data;
            sta_assert(tmp.dim.size()==5);
            sta_assert(tmp.dim[3]==6);
            num_alphas_hessian=tmp.dim[4];
            printf("hessian  pol degree: %d \n",num_alphas_hessian-1);
            tmp=mhs::dataArray<TData>(feature_struct,"alphas_gradient");
            gradient=tmp.data;
            sta_assert(tmp.dim.size()==5);
            sta_assert(tmp.dim[3]==3);
            num_alphas_gradient=tmp.dim[4];
            printf("gradient pol degree: %d \n",num_alphas_gradient-1);
        }
        gradient_v=mhs::dataArray<TData>(feature_struct,"gradient_vessel").data;

	 scale_correction=1.0/std::sqrt(2.0);
	try {
	  const TData * scales=mhs::dataArray<TData>(feature_struct,"override_scales").data;
	  min_scale=scales[0]*scale_correction;
	  max_scale=scales[mhs::dataArray<TData>(feature_struct,"override_scales").dim[0]-1]*scale_correction;
// 	  soverride=true;
        } catch (mhs::STAError error)
        {
	    const TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	    min_scale=scales[0];
	    max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
        }
	
	
	
	
	
       
        logscalefac=mhs::dataArray<TData>(feature_struct,"Scales_fac").data[0];
        printf("scalefac: %f\n",logscalefac);
        //sta_assert(logscalefac>0);
	sta_assert(!(logscalefac<1));
        logscalefac=std::log(logscalefac);
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGradVessel=1;
        DataScaleGrad=1;
        DataThreshold=1;

        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGrad")!=-1)
                DataScaleGrad=mhs::mex_getParam<T>(params,"DataScaleGrad",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
        }


    }

private:



private:

    bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius)
    {
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;

	
        radius*=scale_correction;
	
	if ((radius<min_scale-0.0000001)
	  ||(radius>max_scale+0.0000001))
	{
	  printf("%f %f %f\n",min_scale,radius,max_scale);
	  sta_assert(!(radius<min_scale));
	  sta_assert(!(radius>max_scale));
	}
//         if (radius<min_scale)
//             radius=min_scale;

	//T multi_scale=std::sqrt(radius);
        //T multi_scale=std::sqrt(mylog(radius+1)/logscalefac);
        T multi_scale=std::sqrt(std::log(radius+1)/logscalefac);
        //T multi_scale=std::sqrt(mylog(radius)/logscalefac);
        //T multi_scale=radius;

        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);

        Vector<T,Dim> vessel_grad=T(0);
        for (int i=0; i<3; i++)
        {
            T & g=vessel_grad.v[i];
            g+=(1-wz)*(1-wy)*(1-wx)*gradient_v[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(1-wz)*(1-wy)*(wx)*gradient_v[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(1-wz)*(wy)*(1-wx)*gradient_v[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(1-wz)*(wy)*(wx)*gradient_v[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(1-wy)*(1-wx)*gradient_v[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(wz)*(1-wy)*(wx)*gradient_v[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(wy)*(1-wx)*gradient_v[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(wz)*(wy)*(wx)*gradient_v[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
        }
        std::swap(vessel_grad[2],vessel_grad[0]);

//         T vessel_grad_l=(direction.dot(vessel_grad));

        T vessel_grad_l=std::sqrt(vessel_grad.norm2());

        vessel_grad/=vessel_grad_l+std::numeric_limits<T>::epsilon();
        T vessel_grad_p=(direction.dot(vessel_grad));
        vessel_grad_l*=vessel_grad_p*vessel_grad_p;

        T H[6];
        for (int i=0; i<6; i++)
        {
            T & g=H[i];
            //g=eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+Y)*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g=0;
            g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+Y)*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
        }

        T & xx=  H[0]; //xx
        T & xy=  H[3]; //xy
        T & xz=  H[5]; //xz
        T & yy=  H[1]; //yy
        T & yz=  H[4]; //yz
        T & zz=  H[2]; //zz

        T & nx=direction[2];
        T & ny=direction[1];
        T & nz=direction[0];

        T steer_filter=nx*nx*xx+ny*ny*yy+nz*nz*zz+
                       +2*nx*(ny*xy+nz*xz)
                       +2*ny*nz*yz;

        for (int i=0; i<3; i++)
        {
            T & g=H[i];
            g=0;
            g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
        }
        std::swap(H[2],H[0]);

        T l_g_vessel=direction.dot(H);

        T grad=std::sqrt((direction*(-l_g_vessel)+H).norm2());

        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }

        result=-DataScale*steer_filter*scale1
               -DataThreshold
               +DataScaleGrad*grad
               +DataScaleGradVessel*std::abs(vessel_grad_l);
        return true;
    }

    
public:
  

    bool eval_data2(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius)
    {
        std::size_t  indexes[8];
        T weights[8];

        if (!sub2indInter(
                    this->shape,
                    position,
                    indexes,
                    weights))
        {
            return false;
        }



        radius*=scale_correction;
        if (radius<min_scale)
            radius=min_scale;

        T multi_scale=std::sqrt(mylog(radius)/logscalefac);
        Vector<T,Dim> vessel_grad=T(0);
        for (int i=0; i<3; i++)
        {
            for (int a=0; a<8; a++)
            {
                T & g=vessel_grad.v[i];
                g+=weights[a]*gradient_v[indexes[a]*3+i];
            }
        }

        std::swap(vessel_grad[2],vessel_grad[0]);

        T vessel_grad_l=std::sqrt(vessel_grad.norm2());

        vessel_grad/=vessel_grad_l+std::numeric_limits<T>::epsilon();
        T vessel_grad_p=(direction.dot(vessel_grad));
        vessel_grad_l*=vessel_grad_p*vessel_grad_p;

        T H[6];
        for (int i=0; i<6; i++)
        {
            T & g=H[i];
            g=0;
            for (int a=0; a<8; a++)
            {
                g+=weights[a]*eval_polynom1(multi_scale,hessian+(indexes[a]*6+i)*num_alphas_hessian,num_alphas_hessian-1);
            }
        }

        T & xx=  H[0]; //xx
        T & xy=  H[3]; //xy
        T & xz=  H[5]; //xz
        T & yy=  H[1]; //yy
        T & yz=  H[4]; //yz
        T & zz=  H[2]; //zz

        T & nx=direction[2];
        T & ny=direction[1];
        T & nz=direction[0];

        T steer_filter=nx*nx*xx+ny*ny*yy+nz*nz*zz+
                       +2*nx*(ny*xy+nz*xz)
                       +2*ny*nz*yz;

        for (int i=0; i<3; i++)
        {
            T & g=H[i];
            g=0;
            for (int a=0; a<8; a++)
            {
                g+=weights[a]*eval_polynom1(multi_scale,gradient+(indexes[a]*3+i)*num_alphas_gradient,num_alphas_gradient-1);
            }
        }
        std::swap(H[2],H[0]);

        T l_g_vessel=direction.dot(H);

        T grad=std::sqrt((direction*(-l_g_vessel)+H).norm2());

        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }

        result=-DataScale*steer_filter*scale1
               -DataThreshold
               +DataScaleGrad*grad
               +DataScaleGradVessel*std::abs(vessel_grad_l);
        return true;
    }
  
  
    bool eval_data3(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius)
    {
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;

        radius*=scale_correction;
        if (radius<min_scale)
            radius=min_scale;

        T multi_scale=std::sqrt(mylog(radius)/logscalefac);

        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);

        T vessel_grad_l;
        T steer_filter;
        T grad;

	
	#pragma omp parallel num_threads(2)
        #pragma omp sections 
        {
            #pragma omp section
            {
                Vector<T,Dim> vessel_grad=T(0);
                for (int i=0; i<3; i++)
                {
                    T & g=vessel_grad.v[i];
                    g+=(1-wz)*(1-wy)*(1-wx)*gradient_v[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                    g+=(1-wz)*(1-wy)*(wx)*gradient_v[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                    g+=(1-wz)*(wy)*(1-wx)*gradient_v[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                    g+=(1-wz)*(wy)*(wx)*gradient_v[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                    g+=(wz)*(1-wy)*(1-wx)*gradient_v[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                    g+=(wz)*(1-wy)*(wx)*gradient_v[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                    g+=(wz)*(wy)*(1-wx)*gradient_v[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                    g+=(wz)*(wy)*(wx)*gradient_v[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                }
                std::swap(vessel_grad[2],vessel_grad[0]);

//         T vessel_grad_l=(direction.dot(vessel_grad));

                vessel_grad_l=std::sqrt(vessel_grad.norm2());

                vessel_grad/=vessel_grad_l+std::numeric_limits<T>::epsilon();
                T vessel_grad_p=(direction.dot(vessel_grad));
                vessel_grad_l*=vessel_grad_p*vessel_grad_p;
//             }
//             #pragma omp section
//             {
                T H[6];
                for (int i=0; i<6; i++)
                {
                    T & g=H[i];
                    //g=eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+Y)*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g=0;
                    g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+Y)*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,hessian+(((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                    g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,hessian+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i)*num_alphas_hessian,num_alphas_hessian-1);
                }

                T & xx=  H[0]; //xx
                T & xy=  H[3]; //xy
                T & xz=  H[5]; //xz
                T & yy=  H[1]; //yy
                T & yz=  H[4]; //yz
                T & zz=  H[2]; //zz

                T & nx=direction[2];
                T & ny=direction[1];
                T & nz=direction[0];

                steer_filter=nx*nx*xx+ny*ny*yy+nz*nz*zz+
                +2*nx*(ny*xy+nz*xz)
                +2*ny*nz*yz;
            }
            #pragma omp section
            {
                T H[3];
                for (int i=0; i<3; i++)
                {
                    T & g=H[i];
                    g=0;
                    g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,gradient+(((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                    g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,gradient+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i)*num_alphas_gradient,num_alphas_gradient-1);
                }
                std::swap(H[2],H[0]);

                T l_g_vessel=direction.dot(H);

                grad=std::sqrt((direction*(-l_g_vessel)+H).norm2());
            }
        }

        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }

        result=-DataScale*steer_filter*scale1
               -DataThreshold
               +DataScaleGrad*grad
               +DataScaleGradVessel*std::abs(vessel_grad_l);
        return true;
    }

};

#endif
