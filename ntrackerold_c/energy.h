#ifndef ENERGY_H
#define ENERGY_H
#include "rand_help.h"
#include "specialfunc.h"
#include <numeric>

const std::size_t query_buffer_size=1000;


static bool normal_scale=true;

static int track_id=0;
static int track_point_id=0;

template<typename T,int Dim> class Points;
template<typename T,int Dim> class Connection;
template<typename T,int Dim> class TConnecTProposal;


template<typename T,int Dim>
T ucon(
       class Points<T,Dim> & pointA,
       Vector<T,Dim> & endpointA,
       class Points<T,Dim> & pointB,
       Vector<T,Dim> & endpointB,
       T L,
       T Lscale,
       T Lpointscale,
       T thickness_scale,
       T anglescale,
       T max_endpoint_dist,
       int & ok,
       bool debug=false
      )
{
  
//   sta_assert_error(max_endpoint_dist==-1);
//    lengthA*=Points<T,Dim>::endpoint_nubble_scale;
//    lengthB*=Points<T,Dim>::endpoint_nubble_scale;
   //Vector<T,Dim>  center=cenerA*(lengthA/(lengthA+lengthB))+cenerB*(lengthB/(lengthA+lengthB));
//   if (max_endpoint_dist!=100)
//   {
//    printf("%f\n",max_endpoint_dist); 
//   }
//   sta_assert_error(max_endpoint_dist==100) ;
  if (max_endpoint_dist>-1)
  {
   if (!(max_endpoint_dist>(endpointA-endpointB).norm2())) 
   {
     //sta_assert_error(ok);
     ok = -2;
     return 1000000000000000;
   }
  }
   
   
   
//  T iprod=std::abs(pointA.direction.dot(pointB.direction));
//   if (iprod<0.6675)
//   {
//     ok = -1;
//     return 1000000000000000;
//   }
  
  ok = 1;
  
  
 
  
     const T & lengthA=pointA.scale;
     const T & lengthB=pointB.scale;
     const Vector<T,Dim> & cenerA=pointA.position;
     const Vector<T,Dim> & cenerB=pointB.position;
  
  
//    if (3<(endpointA-endpointB).norm2()) 
//        return 1000000000000000;
    
   T u;
   T dist_cost;
   if (Points<T,Dim>::thickness<0)
   //if (true)
   {
     
     T center[3];
	const T * cA=cenerA.v;
	const T * cB=cenerB.v;
	
	
	//Vector<T,Dim>  center=cenerA*(lengthB/(lengthA+lengthB))+cenerB*(lengthA/(lengthA+lengthB));
#ifdef 		SCALENORMAL	
	T lA=-Points<T,Dim>::thickness*lengthA;
	T lB=-Points<T,Dim>::thickness*lengthB;
	if (Points<T,Dim>::min_thickness>0)
	{
	  lA=std::max(Points<T,Dim>::min_thickness,lA);
	  lB=std::max(Points<T,Dim>::min_thickness,lB);
	}
	  
#else	
	T lA=-Points<T,Dim>::thickness*mysqrt(lengthA);
	T lB=-Points<T,Dim>::thickness*mysqrt(lengthB);
#endif	
	
	T wa=lB/(lA+lB);
	T wb=lA/(lA+lB);
	center[0]=(wa*(*cA++)+wb*(*cB++));
	center[1]=(wa*(*cA++)+wb*(*cB++));
	center[2]=(wa*(*cA)+wb*(*cB));
	
	
// 	wa=lA*lA;
// 	wb=lB*lB;
 	wa=lengthA;
 	wb=lengthB;
	
	
/*	wa=lengthA*lengthA;
	wb=lengthB*lengthB;*/	

if (normal_scale)
{
	cA=endpointA.v;
	cB=endpointB.v;	
     
	T tmp=(*cA++)-center[0];
	dist_cost=tmp*tmp/(wa);
	tmp=(*cB++)-center[0];
	dist_cost+=tmp*tmp/(wb);
	
	tmp=(*cA++)-center[1];
	dist_cost+=tmp*tmp/(wa);
	tmp=(*cB++)-center[1];
	dist_cost+=tmp*tmp/(wb);
	
	tmp=(*cA)-center[2];
	dist_cost+=tmp*tmp/(wa);
	tmp=(*cB)-center[2];
	dist_cost+=tmp*tmp/(wb);		
}else
{
	Vector<T,Dim> v1=endpointA-pointA.position;
	Vector<T,Dim> v2=endpointB-pointB.position;
	v1.normalize();
	v2.normalize();
	Vector<T,Dim> v1c=(pointA.position-center);
	T vl1=std::sqrt(v1c.norm2());
	Vector<T,Dim> v2c=(pointB.position-center);
	T vl2=std::sqrt(v2c.norm2());
	v1*=vl1;
	v2*=vl2;
	
	dist_cost=(v1+v1c).norm2()+(v2+v2c).norm2();	
	
}
	
     
	  /*
	    Vector<T,Dim>  center=cenerA*(lengthB/(lengthA+lengthB))+cenerB*(lengthA/(lengthA+lengthB));
	
	// u=(endpointA-center).norm2()/(lengthA*lengthA)+(endpointB-center).norm2()/(lengthB*lengthB);
	    dist_cost=(endpointA-center).norm2()+(endpointB-center).norm2();
	// u=(endpointA-center).norm2()/(lengthA)+(endpointB-center).norm2()/(lengthB);
	// u=(endpointA-center).norm2()/(lengthA*lengthA)+(endpointB-center).norm2()/(lengthB*lengthB);
	// u=(endpointA-center).norm2()/(lengthA)+(endpointB-center).norm2()/(lengthB);
	*/
   }else
   {
	T center[3];
	const T * cA=cenerA.v;
	const T * cB=cenerB.v;
	
	
	center[0]=((*cA++)+(*cB++))/2;
	center[1]=((*cA++)+(*cB++))/2;
	center[2]=((*cA)+(*cB))/2;
	
	cA=endpointA.v;
	cB=endpointB.v;	
	
// 	T tmp=(*cA++)-center[0];
// 	u=tmp*tmp;
// 	tmp=(*cB++)-center[0];
// 	u+=tmp*tmp;
// 	
// 	tmp=(*cA++)-center[1];
// 	u+=tmp*tmp;
// 	tmp=(*cB++)-center[1];
// 	u+=tmp*tmp;
// 	
// 	tmp=(*cA)-center[2];
// 	u+=tmp*tmp;
// 	tmp=(*cB)-center[2];
// 	u+=tmp*tmp;
if (false)
{
	T tmp=(*cA++)-center[0];
	dist_cost=tmp*tmp/lengthA;
	tmp=(*cB++)-center[0];
	dist_cost+=tmp*tmp/lengthB;
	
	tmp=(*cA++)-center[1];
	dist_cost+=tmp*tmp/lengthA;
	tmp=(*cB++)-center[1];
	dist_cost+=tmp*tmp/lengthB;
	
	tmp=(*cA)-center[2];
	dist_cost+=tmp*tmp/lengthA;
	tmp=(*cB)-center[2];
	dist_cost+=tmp*tmp/lengthB;	
}else
{
	T tmp=(*cA++)-center[0];
	dist_cost=tmp*tmp/(lengthA*lengthA);
	tmp=(*cB++)-center[0];
	dist_cost+=tmp*tmp/(lengthB*lengthB);
	
	tmp=(*cA++)-center[1];
	dist_cost+=tmp*tmp/(lengthA*lengthA);
	tmp=(*cB++)-center[1];
	dist_cost+=tmp*tmp/(lengthB*lengthB);
	
	tmp=(*cA)-center[2];
	dist_cost+=tmp*tmp/(lengthA*lengthA);
	tmp=(*cB)-center[2];
	dist_cost+=tmp*tmp/(lengthB*lengthB);	
}
     
     
      
//       Vector<T,Dim> edge=cenerA-cenerB;
//       edge.normalize();
//       Vector<T,Dim> NA=endpointA-cenerA;
//       NA.normalize();
//       Vector<T,Dim> NB=endpointB-cenerB;
//       NB.normalize();
//       T iprodA=std::min(myacos(-NA.dot(edge))/(0.5*M_PI),T(1));
//       T iprodB=std::min(myacos(NB.dot(edge))/(0.5*M_PI),T(1));
//       
//       NA*=Points<T,Dim>::thickness+iprodA*(lengthA-Points<T,Dim>::thickness);
//       NB*=Points<T,Dim>::thickness+iprodB*(lengthB-Points<T,Dim>::thickness);
//       
//       Vector<T,Dim> center=(cenerA+cenerB)/2;
//       
//       u+=(center-(NA+cenerA)).norm2()+(center-(NB+cenerB)).norm2();
   }
   
//    Vector<T,Dim> n1=endpointA-cenerA;
//    Vector<T,Dim> n2=endpointB-cenerB;
//    n1/=std::sqrt(n1.norm2());
//    n2/=std::sqrt(n2.norm2());
//    T idot=(1/n1.dot(n2));
//    u+=idot*idot-1;
   
if (debug)
{
 printf("lengthA %f, length B %f\n",lengthA,lengthB); 
 printf("thickness :%f\n",(std::pow(std::max(lengthA,lengthB)/std::min(lengthA,lengthB),T(2))-1)) ;
 printf("dist      :%f\n",dist_cost) ;
}
   
   
   u=Lscale*dist_cost+thickness_scale*(std::pow(std::max(lengthA,lengthB)/std::min(lengthA,lengthB),T(2))-1);
   
//    T saliency=(pointA.saliency+pointB.saliency)/2;
//    u+=((1+mexp(saliency*saliency*10)))*L;  
   
   //u=Lscale*u+L;
   if ((anglescale>0)&&(dist_cost>0.01))
   //if ((anglescale>0))
   {
//   Vector<T,Dim>	epA=(cenerA*0.1+endpointA*0.9);
//   Vector<T,Dim>	epB=(cenerB*0.1+endpointB*0.9);
//   Vector<T,Dim> edge=epA-epB;
//   edge.normalize();
//   Vector<T,Dim> NA=endpointA-cenerA;
//   NA.normalize();
//   Vector<T,Dim> NB=endpointB-cenerB;
//   NB.normalize();
//    T iprodA=myacos(-NA.dot(edge));
//    T iprodB=myacos(NB.dot(edge));
//   
//    if (!(iprodA<anglescale)||!(iprodB<anglescale))
//    {
//      //u+=1000000000000000000;
//       ok = -2;
//       return 1000000000000000;      
//    }
   
   
    Vector<T,Dim> edge;//=epA-epB;
    T *  e=edge.v;
//     const T *  cA=cenerA.v;
//     const T *  cB=cenerB.v;    
    const T *  eA=endpointA.v;
    const T *  eB=endpointB.v;   
    
    (*e++)=(*eA++)-(*eB++);
    (*e++)=(*eA++)-(*eB++);
    (*e)=(*eA)-(*eB);
    
    edge.normalize();
    
    Vector<T,Dim> NA;//=endpointA-cenerA;
    e=NA.v;
    const T *  cA=cenerA.v;
    eA=endpointA.v;
    
    (*e++)=(*eA++)-(*cA++);
    (*e++)=(*eA++)-(*cA++);
    (*e)=(*eA)-(*cA);
    NA.normalize();
    
    Vector<T,Dim> NB;//=endpointB-cenerB;
    e=NB.v;
    const T *  cB=cenerB.v;    
    eB=endpointB.v;           
    (*e++)=(*eB++)-(*cB++);
    (*e++)=(*eB++)-(*cB++);
    (*e)=(*eB)-(*cB);
    NB.normalize();
    
/*    Vector<T,Dim> edge;//=epA-epB;
    e=edge.v;
    cA=cenerA.v;
    cB=cenerB.v;    
    eA=NA.v;
    eB=NB.v;   
    
    T lA=(lengthA-0.01);
    T lB=(lengthB-0.01);
    (*e++)=(*cA++)+lA*(*eA++)-(*cB++)-lB*(*eB++);
    (*e++)=(*cA++)+lA*(*eA++)-(*cB++)-lB*(*eB++);
    (*e)=(*cA)+lA*(*eA)-(*cB)-lB*(*eB);*/    
    
    
    T iprodA=myacos(-NA.dot(edge));
    T iprodB=myacos(NB.dot(edge));     
     
    if (!(iprodA<anglescale)||!(iprodB<anglescale))
    {
      ok = -1;
      return 1000000000000000; 
    }   
   
   
   }
//   if (iprod<0.6675)
//   {
//     ok = -1;
//     return 1000000000000000;
//   }

   
   if (Lpointscale<0)
   {
    T tmp=(lengthA+lengthB)/(T(2));
    u=u+Lpointscale*(tmp)+L;
   }else
   {
     return u+L;
   }
   
   return u;
}






template<typename T,int Dim>
T edge_energy(Points<T,Dim> & npoint,T L,T sL,T pS,T tS,T AngleScale,T maxendpointdist,int & inrange)
{
  
    inrange=1;
    T U=0;
    for (int end_id=0;end_id<2;end_id++)
    {
	npoint.update_endpoint(end_id);
	//class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	
	for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	{
	  if (endpointsA[i].connected!=NULL)
	  {
		endpointsA[i].connected->point->update_endpoint(endpointsA[i].connected->side);
		
		U+=ucon<T,Dim>(
			    npoint,
			    *(endpointsA[i].position),
			    *(endpointsA[i].connected->point),
			    *(endpointsA[i].connected->position),
// 			    npoint.position,
// 			    (*endpointsA[i].position),
// 			    npoint.scale,
// 			    endpointsA[i].connected->point->position,
// 			    (*endpointsA[i].connected->position),
// 			    endpointsA[i].connected->point->scale,
			    L,sL,pS,tS,AngleScale,maxendpointdist,inrange);
		if (inrange<0)
		  return 0;
	  }
	}
    }
    return U;
}





template<typename T,int Dim>
T edge_point_energy(Points<T,Dim> & npoint,
	      TConnecTProposal<T,Dim> * cproposal=NULL,
	      bool *  dend_id=NULL,	      
	      int depths=0,
	      std::list<T> * edge_lenghts=NULL)
{
  
  
  if (depths==0) //! first node setup
  {
      if (npoint.endpoint_connections[0]+npoint.endpoint_connections[1]<3)
	return 0;
      
      //! first node searches both sides for directly connected graphs
      dend_id = new bool[2];
      dend_id[0]=true;
      dend_id[1]=true;
      
      track_id++;
      edge_lenghts=new std::list<T>();
      npoint.update_endpoint(0);
      npoint.update_endpoint(1);
  }
      
   for (int end_id=0;end_id<2;end_id++)
   {
	if ((dend_id[end_id])&&(npoint.endpoint_connections[end_id]>1))
	{
	    //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	  class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	    
	    for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	    {
	      if (endpointsA[i].connected!=NULL)
	      {
		//! check if edge is not the edge that will be deleted and edge has not been tracked so far
		if (
		  ((cproposal==NULL)||((!cproposal->old_connection_exists)||(endpointsA[i].connection!=cproposal->connection_old)))
		  &&(endpointsA[i].connection->track_me_id!=track_id))
		{
		    endpointsA[i].connection->track_me_id=track_id;
		    
		    endpointsA[i].connected->point->update_endpoint(endpointsA[i].connected->side);
		    edge_lenghts->push_back(std::sqrt(((*endpointsA[i].position)-(*endpointsA[i].connected->position)).norm2()));
		    
		    bool dend_id[2];
		    dend_id[0]=false;
		    dend_id[1]=false;
		    dend_id[endpointsA[i].connected->side]=true;
		    edge_point_energy(*(endpointsA[i].connected->point),
		    cproposal,
		    dend_id,	      
		    depths+1,
		    edge_lenghts);
		}
	      }
	    }
	}
   }
  
  
        
  if (depths==0)
  {
    
    if ((cproposal!=NULL)&&
      (cproposal->new_connection_exists))
    {
      //! has already been update during tracking
      edge_lenghts->push_back(std::sqrt(((*cproposal->connection_new.pointA->position)-(*cproposal->connection_new.pointB->position)).norm2()));
      
      bool dend_id[2];
      dend_id[0]=false;
      dend_id[1]=false;
      dend_id[cproposal->connection_new.pointB->side]=true;
      edge_point_energy(*(cproposal->connection_new.pointB->point),
      cproposal,
      dend_id,	      
      depths+1,
      edge_lenghts);
    }

    if (edge_lenghts->size()<2)
      return 0;    
    // compute mean & energy

    T num_edges=edge_lenghts->size();
    


    T mean_length=0;    
    for (typename std::list<T>::iterator iter=edge_lenghts->begin();
      iter!=edge_lenghts->end();
    iter++)
	mean_length+=*iter;
	mean_length/=num_edges;
	
//     T mean_length=1000000000000000000;    
//     for (typename std::list<T>::iterator iter=edge_lenghts->begin();
//       iter!=edge_lenghts->end();
//     iter++)
// 	mean_length=std::min(*iter,mean_length);
	

    if (mean_length<2*std::numeric_limits<T>::epsilon( ))
      return 0;

    
	

    T total_energy=0;
	
    for (typename std::list<T>::iterator iter=edge_lenghts->begin();
      iter!=edge_lenghts->end();
    iter++)
	 {
	  T tmp= (*iter)-mean_length;
 	  //total_energy+=tmp*tmp/num_edges;
	  total_energy+=tmp*tmp;
	 }
	
/*	printf("%d\n",(int)num_edges); */ 
	
    
    delete [] dend_id;
    delete edge_lenghts;
    
    return total_energy;
  }    
    
    return 0;
}


/*
template<typename T,int Dim>
T edge_point_energy(Points<T,Dim> & npoint,
  TConnecTProposal<T,Dim> * cproposal=NULL)
{
  
    return 0;
  
    if ((npoint.endpoint_connections[0]<2)&&(npoint.endpoint_connections[1]<2))
      return 0;
    
    T edge_enery_total=0;
    for (int end_id=0;end_id<2;end_id++)
    {
      T edge_enery=0;  
      npoint.update_endpoint(end_id);
      T num_connections=0;
      if (npoint.endpoint_connections[end_id]>1)
      {
	  T edge_enery_endpoint=0;
	  //T length1_sqr=100000000000000;
	  T length1_sqr=0;
	  //! find smallest lengths
	  class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      if ((cproposal==NULL)||
		(!cproposal->old_connection_exists)||
		(endpointsA[i].connection!=cproposal->connection_old))
	      {
		endpointsA[i].connected->point->update_endpoint(endpointsA[i].connected->side);
		//length1_sqr=std::min((((*endpointsA[i].position)-(*endpointsA[i].connected->position)).norm2()),length1_sqr);
		length1_sqr=std::sqrt(((*endpointsA[i].position)-(*endpointsA[i].connected->position)).norm2())+length1_sqr;
		num_connections+=1;
	      }
	    }
	  }
	  
	   if ((cproposal!=NULL)&&(cproposal->new_connection_exists)&&(cproposal->connection_new.pointA->side==end_id))
	   {
	     //! has already been update during tracking
	     //cproposal->connection_new.pointB->point->update_endpoint(cproposal->connection_new.pointB->side);
	     //length1_sqr=std::min((((*cproposal->connection_new.pointA->position)-(*cproposal->connection_new.pointB->position)).norm2()),length1_sqr);
	     length1_sqr=std::sqrt(((*cproposal->connection_new.pointA->position)-(*cproposal->connection_new.pointB->position)).norm2())+length1_sqr;
	     num_connections+=1;
	   }

	   if (!(num_connections>0))
	     continue;
	   
	   T length1=length1_sqr/num_connections;
// 	   printf("%f\n",length1);
	   //T length1=std::sqrt(length1_sqr);
	  
	  
	  for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	  {
	    if (endpointsA[i].connected!=NULL)
	    {
	      if ((cproposal==NULL)||
		(!cproposal->old_connection_exists)||
		(endpointsA[i].connection!=cproposal->connection_old))
	      {
		//endpointsA[i].connected->point->update_endpoint(endpointsA[i].connected->side);
		T length2=std::sqrt(((*endpointsA[i].position)-(*endpointsA[i].connected->position)).norm2());
		T n=1;//(length1*length1+length2*length2);
		if (n>std::numeric_limits<T>::epsilon( ))
		edge_enery+=(length1-length2)*(length1-length2)/n;
	      }
	    }
	  }
	  
	  if ((cproposal!=NULL)&&(cproposal->new_connection_exists)&&(cproposal->connection_new.pointA->side==end_id))
	  {
	    //! has already been update during tracking
	    //cproposal->connection_new.pointB->point->update_endpoint(cproposal->connection_new.pointB->side);
	    T length2=std::sqrt((((*cproposal->connection_new.pointA->position)-(*cproposal->connection_new.pointB->position)).norm2()));
	    T n=1;//(length1*length1+length2*length2);
	    if (n>std::numeric_limits<T>::epsilon( ))
	    edge_enery+=(length1-length2)*(length1-length2)/n;
	  }
	  
	  if (edge_enery>0)
	    edge_enery_total=edge_enery/num_connections;
	  
      }
    }
    return edge_enery_total;
}*/






/*
 *
 *  compute partical interaction (spherical)
 *
 * */
// template<typename T,int Dim>
// bool colliding( OctTreeNode<T,Dim> & tree,OctPoints<T,Dim> & npoint,T searchrad)
// {
//     Points<T,Dim> & newpoint= *((Points<T,Dim>  *)&npoint);
//     AABB<T,Dim>  range;
//     Vector<T,Dim> halfDimension;
//     halfDimension=searchrad;
//     
//     range.setAABB(newpoint.position.v,halfDimension.v);
// 
//     searchrad*=searchrad;
// 
//     T energy=0;
//     std::list<class OctPoints< T, Dim > *>  candidates;
//     tree.queryRange(range,candidates);
// 
// 
//     typename std::list< class OctPoints<T,Dim> * >::iterator  iter=candidates.begin();
//     while (iter!=candidates.end())
//     {
//         Points<T,Dim> * point=(Points<T,Dim> *)(*iter);
//         if (point!=&newpoint)
// 	{
// 	    T * cand_pos=point->position.v;
// 	    T   cand_size=point->scale;
// 
// 	    T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-newpoint.position).norm2();
// 	    T mindist=(cand_size+newpoint.scale);
// 	    mindist*=mindist;
// 	    if (candidate_center_dist_sq<mindist)
// 	    {
// 		return true;
// 	    }
// 	}
//         iter++;
//     }
//     return false;
// }


template<typename T>
bool compCorr(Vector<T,3> &R1, 
		     Vector<T,3> &R2,
		     Vector<T,3> &N1, 
		     Vector<T,3> &N2,
		     T gamma_wn ,
		     T gamma_sn, 
		     T gamma_wm,
		     T gamma_sm)
{   

      Vector<T,3> Rdif = R1-R2;

      T nr = (N1.dot(Rdif));
      Vector<T,3> ARdif = N1*(nr*gamma_sn) + Rdif*gamma_wn - N1*(nr*gamma_wn);
      T rAr = ARdif.dot(Rdif);

      //N1.storeXYZ();
      T & Nx = N1[0];
      T & Ny = N1[1];
      T & Nz = N1[2];
      //N2.storeXYZ();
      T & Mx = N2[0];
      T & My = N2[1];
      T & Mz = N2[2];


      T AB11 =  Nx*Nx*gamma_sn + (1-Nx*Nx)*gamma_wn +  Mx*Mx*gamma_sm + (1-Mx*Mx)*gamma_wm;                
      T AB22 =  Ny*Ny*gamma_sn + (1-Ny*Ny)*gamma_wn +  My*My*gamma_sm + (1-My*My)*gamma_wm;                
      T AB33 =  Nz*Nz*gamma_sn + (1-Nz*Nz)*gamma_wn +  Mz*Mz*gamma_sm + (1-Mz*Mz)*gamma_wm;                
      T AB12 =  Nx*Ny*gamma_sn + ( -Nx*Ny)*gamma_wn +  Mx*My*gamma_sm + ( -Mx*My)*gamma_wm;                
      T AB13 =  Nx*Nz*gamma_sn + ( -Nx*Nz)*gamma_wn +  Mx*Mz*gamma_sm + ( -Mx*Mz)*gamma_wm;                
      T AB23 =  Ny*Nz*gamma_sn + ( -Ny*Nz)*gamma_wn +  My*Mz*gamma_sm + ( -My*Mz)*gamma_wm;                

      T detAB =  AB11*AB22*AB33 + 2*AB12*AB23*AB13  - AB13*AB13*AB22 - AB11*AB23*AB23 - AB12*AB12*AB33;

      //ARdif.storeXYZ();
      T & ARx = ARdif[0];
      T & ARy = ARdif[1];
      T & ARz = ARdif[2];

      T rAABAr = (
	(AB33*AB22-AB23*AB23)*ARx*ARx   -2*(AB33*AB12-AB23*AB13)*ARy*ARx     +2*(AB23*AB12-AB22*AB13)*ARz*ARx  
					  +(AB33*AB11-AB13*AB13)*ARy*ARy     -2*(AB23*AB11-AB12*AB13)*ARz*ARy 
									      +(AB22*AB11-AB12*AB12)*ARz*ARz ) /detAB;

//              return mexp(2*(-rAABAr+rAr))/sqrt(detAB);
      //return (2*(-rAABAr+rAr) >4)? 0 : INFINITY;
//	return std::exp(-2*(-rAABAr+rAr))/std::sqrt(detAB);
	
      return (2*(-rAABAr+rAr) >4)? false : true;
	//return (2*(-rAABAr+rAr) >M_PI)? false : true;
      
      
      
      
//              return (mexp(2*(-rAABAr+rAr))/sqrt(detAB) < 0.1)? 0 : INFINITY;
}


template<typename T>
bool compCorr2(Vector<T,3> &R1, 
		     Vector<T,3> &R2,
		     Vector<T,3> &N1, 
		     Vector<T,3> &N2,
		     T gamma_wn ,
		     T gamma_sn, 
		     T gamma_wm,
		     T gamma_sm)
{   

      Vector<T,3> Rdif = R1-R2;

      T nr = (N1.dot(Rdif));
      Vector<T,3> ARdif = N1*(nr*(gamma_sn-gamma_wn)) + Rdif*gamma_wn;
      T rAr = ARdif.dot(Rdif);

      //N1.storeXYZ();
      T & Nx = N1[0];
      T & Ny = N1[1];
      T & Nz = N1[2];
      //N2.storeXYZ();
      T & Mx = N2[0];
      T & My = N2[1];
      T & Mz = N2[2];


      T AB11 =  Nx*Nx*gamma_sn + (1-Nx*Nx)*gamma_wn +  Mx*Mx*gamma_sm + (1-Mx*Mx)*gamma_wm;                
      T AB22 =  Ny*Ny*gamma_sn + (1-Ny*Ny)*gamma_wn +  My*My*gamma_sm + (1-My*My)*gamma_wm;                
      T AB33 =  Nz*Nz*gamma_sn + (1-Nz*Nz)*gamma_wn +  Mz*Mz*gamma_sm + (1-Mz*Mz)*gamma_wm;                
      T AB12 =  Nx*Ny*gamma_sn + ( -Nx*Ny)*gamma_wn +  Mx*My*gamma_sm + ( -Mx*My)*gamma_wm;                
      T AB13 =  Nx*Nz*gamma_sn + ( -Nx*Nz)*gamma_wn +  Mx*Mz*gamma_sm + ( -Mx*Mz)*gamma_wm;                
      T AB23 =  Ny*Nz*gamma_sn + ( -Ny*Nz)*gamma_wn +  My*Mz*gamma_sm + ( -My*Mz)*gamma_wm;                

      T detAB =  AB11*AB22*AB33 + 2*AB12*AB23*AB13  - AB13*AB13*AB22 - AB11*AB23*AB23 - AB12*AB12*AB33;

      //ARdif.storeXYZ();
      T & ARx = ARdif[0];
      T & ARy = ARdif[1];
      T & ARz = ARdif[2];

      T rAABAr = (
	(AB33*AB22-AB23*AB23)*ARx*ARx   -2*(AB33*AB12-AB23*AB13)*ARy*ARx     +2*(AB23*AB12-AB22*AB13)*ARz*ARx  
					  +(AB33*AB11-AB13*AB13)*ARy*ARy     -2*(AB23*AB11-AB12*AB13)*ARz*ARy 
									      +(AB22*AB11-AB12*AB12)*ARz*ARz ) /detAB;

//              return mexp(2*(-rAABAr+rAr))/sqrt(detAB);
      //return (2*(-rAABAr+rAr) >4)? 0 : INFINITY;
//	return std::exp(-2*(-rAABAr+rAr))/std::sqrt(detAB);
	
      return (2*(-rAABAr+rAr) >4)? false : true;
	//return (2*(-rAABAr+rAr) >M_PI)? false : true;
      
      
      
      
//              return (mexp(2*(-rAABAr+rAr))/sqrt(detAB) < 0.1)? 0 : INFINITY;
}

		    

template<typename T,int Dim>
bool colliding( OctTreeNode<T,Dim> & tree,OctPoints<T,Dim> & npoint,T searchrad)
{
    Points<T,Dim> & newpoint= *((Points<T,Dim>  *)&npoint);
//     AABB<T,Dim>  range;
//     Vector<T,Dim> halfDimension;
//     halfDimension=searchrad;
//     
//     range.setAABB(newpoint.position.v,halfDimension.v);

    

  
  std::size_t found;
  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
  class OctPoints<T,Dim> ** candidates;
  candidates=query_buffer;
  
  try{
  tree.queryRange(
	newpoint.position,searchrad,candidates,query_buffer_size,found); 
  } catch (mhs::STAError & error)
    {
     throw error;
    }
//    printf("%d\n",found);
//     searchrad*=searchrad;
/*    
//     T energy=0;
    std::list<class OctPoints< T, Dim > *>  candidates;
    //tree.queryRange(range,candidates);
    tree.queryRange(newpoint.position,searchrad,candidates);*/
    


//     typename std::list< class OctPoints<T,Dim> * >::iterator  iter=candidates.begin();
//     while (iter!=candidates.end())
    for (std::size_t a=0;a<found;a++)  
    {
        Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
        if (point!=&newpoint)
	{
// 	    T * cand_pos=point->position.v;
	    T   cand_size=point->scale;

	    
		
	      
// 		bool test=(compCorr(
// 		     newpoint.position, 
// 		     point->position,
// 		     newpoint.direction, 
// 		     point->direction,
// 		     1.0/(newpoint.scale*newpoint.scale),
// 		     1.0/(newpoint.scale*newpoint.scale),
// 		     1.0/(cand_size*cand_size), 
// 		     1.0/(cand_size*cand_size) 
// 				   ));		      
// 		
// 		if (test)  
// 		  return true;	    
	    
	    T candidate_center_dist_sq=(point->position-newpoint.position).norm2();
	    
	    
	    
	      
	    
	    if (Points<T,Dim>::thickness<0)
	    {
	      T mindist;
	      if (std::abs(Points<T,Dim>::thickness)>1)
// #ifdef 		SCALENORMAL		
		if (Points<T,Dim>::min_thickness>0)
		mindist= std::max(Points<T,Dim>::min_thickness,-Points<T,Dim>::thickness*cand_size)
			+std::max(Points<T,Dim>::min_thickness,-Points<T,Dim>::thickness*newpoint.scale);
			else
			  mindist=Points<T,Dim>::thickness*(cand_size+newpoint.scale);
		
// #else		
// 		mindist=Points<T,Dim>::thickness*(mysqrt(cand_size)+mysqrt(newpoint.scale));
// #endif		
	      else
		mindist=(cand_size+newpoint.scale);
	      mindist*=mindist;
// 	      if (!(candidate_center_dist_sq>mindist))
// 		return true;
	      if (!(candidate_center_dist_sq>mindist))
	      {
		
		
		
#ifdef 		SCALENORMAL
		bool test2;
		if (Points<T,Dim>::min_thickness>0)
		{
		  T thickinessA=std::max(Points<T,Dim>::min_thickness,-Points<T,Dim>::thickness*newpoint.scale);
		  T thickinessB=std::max(Points<T,Dim>::min_thickness,-Points<T,Dim>::thickness*cand_size);
		  
		  T scaleA=1.0/(newpoint.scale*newpoint.scale);
		  T scaleB=1.0/(cand_size*cand_size);
		  
		     test2=(compCorr2(
			newpoint.position, 
			point->position,
			newpoint.direction, 
			point->direction,
			  
			scaleA,
					1.0/(thickinessA*thickinessA),
						
			scaleB,
					1.0/(thickinessB*thickinessB)
				      ));		 		  
		  
		}
		else
		  {
		  T thickiness=1/(T(Points<T,Dim>::thickness*Points<T,Dim>::thickness));
		  
		  T scaleA=1.0/(newpoint.scale*newpoint.scale);
		  T scaleB=1.0/(cand_size*cand_size);
		  
		     test2=(compCorr2(
			newpoint.position, 
			point->position,
			newpoint.direction, 
			point->direction,
			  
			scaleA,
					scaleA*thickiness,
						
			scaleB,
					scaleB*thickiness	
				      ));		 
		}
#else
		  
		T thickiness=1/(T(Points<T,Dim>::thickness*Points<T,Dim>::thickness));
		
		T scaleA=1.0/(newpoint.scale);
		T scaleB=1.0/(cand_size);
		
		  bool test2=(compCorr2(
		      newpoint.position, 
		      point->position,
		      newpoint.direction, 
		      point->direction,
			
		      scaleA*scaleA,
				      scaleA*thickiness,
					      
		      scaleB*scaleB,
				      scaleB*thickiness	
				    ));		 
		
#endif		
		  
		  if (test2)  
		    return true;
	      }
	    }else 
	    {
	      T mindist=(cand_size+newpoint.scale);
	      mindist*=mindist;
	      
	      if (!(candidate_center_dist_sq>mindist))
	      {
		T thickiness=1/(T(Points<T,Dim>::thickness*Points<T,Dim>::thickness));
		
		  bool test2=(compCorr2(
		      newpoint.position, 
		      point->position,
		      newpoint.direction, 
		      point->direction,
			
		      1.0/(newpoint.scale*newpoint.scale),
				      thickiness,
					      
		      1.0/(cand_size*cand_size),
				      thickiness	
				    ));		 
		  
		  if (test2)  
		    return true;
	      }
	    }
	}
//         iter++;
    }
    return false;
}		    






#endif