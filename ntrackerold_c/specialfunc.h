#ifndef SPECIALFUNC_H
#define SPECIALFUNC_H


// #include "sta_mex_helpfunc.h"



template<typename T>
T mexp(T v)
{
    static bool doinit=true;
    static const std::size_t accuracy=10000000;//std::numeric_limits<std::size_t>::max()
    static const T min=-50;
    static const T max=30;
    static const T range=max-min;
    static T data[accuracy];
    if (v<min)
        return std::numeric_limits<T>::max();
    if (v>=max)
        return 0;

    if (doinit)
    {
        printf("init exp ... ");
        for (std::size_t i=0; i<accuracy; i++)
        {
            T value=range*T(i)/accuracy+min;
            data[i]=std::exp(-value);
        }
        printf("done\n");
        doinit=false;
    }
    std::size_t index=std::floor(accuracy*(v-min)/range);
    sta_assert_error(index>=0);
    sta_assert_error(index<accuracy);
    return data[index];
}



template<typename T>
T mylog(T v)
{
    static bool doinit=true;
    static const std::size_t accuracy=10000000;//std::numeric_limits<std::size_t>::max()
    static const T min=1;
    static const T max=30;
    static const T range=max-min;
    static T data[accuracy];
    if (v<min)
    {
        printf("%f %f %f %f\n",min,max,range,v);
        sta_assert_error(1==0);
    }
    if (v>=max)
        sta_assert_error(1==0);

    if (doinit)
    {
        printf("init log ... ");
        for (std::size_t i=0; i<accuracy; i++)
        {
            T value=range*T(i)/accuracy+min;
            data[i]=std::log(value);
        }
        printf("done\n");
        doinit=false;
    }
    std::size_t index=std::floor(accuracy*(v-min)/range);
    sta_assert_error(index>=0);
    sta_assert_error(index<accuracy);
    return data[index];
}


template<typename T>
T myacos(T v)
{
    static bool doinit=true;
    static const std::size_t accuracy=10000000;//std::numeric_limits<std::size_t>::max()
    static const T min=-1;
    static const T max=1;
    static const T range=max-min;
    static T data[accuracy];
    if (v<min)
        return M_PI;
    if (!(v<max-std::numeric_limits<T>::epsilon()))
        return 0;

    if (doinit)
    {
        printf("init cos ... ");
        for (std::size_t i=0; i<accuracy; i++)
        {
            T value=range*T(i)/accuracy+min;
            data[i]=std::acos(value);
        }
        printf("done\n");
        doinit=false;
    }
    std::size_t index=std::floor(accuracy*(v-min)/range);
    sta_assert_error(index>=0);
//   if (index>=accuracy)
//     printf("%f %d\n",v,index);
    sta_assert_error(index<accuracy);
    return data[index];
}


template<typename T>
T mysqrt(T v)
{
    static bool doinit=true;
    static const std::size_t accuracy=10000000;//std::numeric_limits<std::size_t>::max()
    static const T min=0;
    static const T max=10;
    static const T range=max-min;
    static T data[accuracy];
    if (v<min)
        sta_assert_error(1==0);
    if (v>=max)
        sta_assert_error(1==0);

    if (doinit)
    {
        printf("init sqrt ... ");
        for (std::size_t i=0; i<accuracy; i++)
        {
            T value=range*T(i)/accuracy+min;
            data[i]=std::sqrt(value);
        }
        printf("done\n");
        doinit=false;
    }
    std::size_t index=std::floor(accuracy*(v-min)/range);
    sta_assert_error(index>=0);
    sta_assert_error(index<accuracy);
    return data[index];
}


template<typename T,int Dim>
T normal_dist(T pos[], T sigma, T mue=0)
{

    T norm=1/std::sqrt(std::pow(sigma*sigma*(2*M_PI),Dim));
    //T norm=1/(sigma*std::sqrt(2*M_PI));

    T v=0;
    T tmp;
    for (int i=0; i<Dim; i++)
    {
        tmp=(pos[i]-mue);
        v+=tmp*tmp;
    }
    return norm*mexp(v/(2*sigma*sigma));
//   return norm*std::exp(-v/(2*sigma*sigma));
}

template<typename T>
T normal_dist_sphere(Vector<T,3> & m,Vector<T,3> & n, T sigma)
{


    T dot=(m.dot(n));
    T g = mexp((1- dot*dot)/(2*sigma*sigma));
    T sign=1;
    if (dot<0)
        sign=-1;


    T v=(1+sign*std::erf(std::abs(dot)/std::sqrt(2)/sigma))* std::sqrt(M_PI/2)* sigma* g;

    return v;
}

template<typename T>
T normal_dist_sphere_full(Vector<T,3> & m,Vector<T,3> & n, T sigma)
{

    T dot=(m.dot(n));
    T g = mexp((1- dot*dot)/(2*sigma*sigma));

    T v= std::sqrt(M_PI/2)* sigma* g;

    return v;
}

#endif
