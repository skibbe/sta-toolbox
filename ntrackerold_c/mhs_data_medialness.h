#ifndef MHS_DATA_MEDIAL_H
#define MHS_DATA_MEDIAL_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataMedial: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * gm;
    
    const TData * debug;
    const TData * gm_scales;
    T min_scale;
    T max_scale;
    T scale_correction;
    int num_alphas;
//     int num_scales;
    T logscalefac;

    T MedialSigma;

    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
//     std::size_t shape[3];
    T ** circle_pts;
    const int maxrad=15;


public:
//    template<typename  S>
//     void setshape(S  shape[])
//       {
// 	for (int i=0;i<Dim;i++)
// 	{
// 	  sta_assert(this->shape[i]>0);
// 	  shape[i]=this->shape[i];
// 	}
// 
//       }
  
 float get_minscale()
    {
        return min_scale/scale_correction;
    };
    float get_maxscale()
    {
        return max_scale/scale_correction;
    };
    float get_scalecorrection()
    {
	//return 1.0/scale_correction;
	//return scale_correction;
          return 1;
    };

    CDataMedial()
    {
//       shape[0]=shape[1]=shape[2]=0;
        circle_pts=new T*[maxrad];
        for (int r=0; r<maxrad; r++)
        {
            int N=std::floor(2*M_PI*r+1);
            circle_pts[r]=new T[2*N];
            for (int n=0; n<N; n++)
            {
                circle_pts[r][2*n]=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
                circle_pts[r][2*n+1]=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
            }
        }

    }

    ~CDataMedial()
    {
        for (int r=0; r<maxrad; r++)
        {
            delete [] circle_pts[r];
        }
        delete [] circle_pts;

    }

    void init(const mxArray * feature_struct)
    {
//         for (int i=0; i<3; i++)
//             this->shape[i]=shape[i];
		for (int i=0; i<3; i++)	
	this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];

        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"alphas");
            gm=tmp.data;
            sta_assert(tmp.dim.size()==4);
            num_alphas=tmp.dim[3];
            printf("found %d num_alphas\n",num_alphas);
        }
        gm_scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	min_scale=gm_scales[0];
	//max_scale=gm_scales[mhs::dataArray<TData>(feature_struct,"scales").dim.size()-1];        
	max_scale=gm_scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
	if (max_scale>maxrad)
	  max_scale=maxrad;
	  
	scale_correction=1.0/std::sqrt(2.0);
//         for (int i=0; i<num_scales; i++)
//             printf("found %3.3f\n",gm_scales[i]);
        logscalefac=mhs::dataArray<TData>(feature_struct,"Scales_fac").data[0];
        printf("scalefac: %f\n",logscalefac);
        sta_assert(logscalefac>0);
        logscalefac=std::log(logscalefac);
	
// 	test(feature_struct);	
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGradVessel=1;
        DataThreshold=1;

        MedialSigma=0.5;


        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];



            if (mhs::mex_hasParam(params,"MedialSigma")!=-1)
                MedialSigma=mhs::mex_getParam<T>(params,"MedialSigma",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
        }

        MedialSigma=2*MedialSigma*MedialSigma;

    }

private:

    void test(const mxArray * feature_struct)
    {
      debug=mhs::dataArray<TData>(feature_struct,"gradient_img").data;
      mhs::dataArray<TData> scales(feature_struct,"scales");
      
      for (int z=0;z<this->shape[0];z++)
	for (int y=0;y<this->shape[1];y++)
	  for (int x=0;x<this->shape[2];x++)
	  {
	    
	      T testg=debug[((z*this->shape[1]+y)*this->shape[2]+x)*scales.dim[0]];
	      if (testg<0.1)
		continue;
	      
	         
	      Vector<T,Dim> position(z,y,x);
	      int Z=std::floor(position[0]);
	      int Y=std::floor(position[1]);
	      int X=std::floor(position[2]);

	      if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
		      (Z<0)||(Y<0)||(X<0))
		  continue;

	      printf("%3.d %3.d %3.d | %3.d %3.d %3.d | :",this->shape[2],this->shape[1],this->shape[0],x,y,z);
	 
	      T wz=(position[0]-Z);
	      T wy=(position[1]-Y);
	      T wx=(position[2]-X);

	      for (int s=0;s<scales.dim[0];s++)
	      {
		T multi_scale=std::sqrt(mylog(scales.data[s])/logscalefac);
		T vessel_grad_l=0;
		{
		    T & g=vessel_grad_l;
		    g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
		    g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
		    g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
		    g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
		    g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
		    g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
		    g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
		    g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
		}
		T testg=debug[((z*this->shape[1]+y)*this->shape[2]+x)*scales.dim[0]+s];
		printf("%f %f\n",testg,vessel_grad_l);
	      }
	  }
    }
    
  

    bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius)
    {
// 	radius*=scale_correction;
// 	if (radius<min_scale)
// 	  radius=min_scale;
// 	sta_assert(!(radius<min_scale));
// 	sta_assert(!(radius>max_scale));      
// 	
      
      
	radius*=scale_correction;
	
	if ((radius<min_scale-0.0000001)
	  ||(radius>max_scale+0.0000001))
	{
	  printf("%f %f %f\n",min_scale,radius,max_scale);
	  sta_assert(!(radius<min_scale));
	  sta_assert(!(radius>max_scale));
	}	
      
      
        int r=std::floor(radius+0.5);
        int N=std::floor(2*M_PI*r+1);
        sta_assert(r<maxrad);
        sta_assert(r>-1);

        Vector<T,3> vn1;
        Vector<T,3> vn2;

        const  Vector<T,Dim> & direction2=direction;
        mhs_graphics::createOrths(direction2,vn1,vn2);


        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;
	
	
	T multi_scale=std::sqrt(mylog(radius)/logscalefac);

        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);

        T vessel_grad_l=0;
        {
            T & g=vessel_grad_l;
            g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
            g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
            g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
            g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
            g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
            g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
            g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
            g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
        }

        result=0;

        Vector<T,Dim> sample_dir;
        Vector<T,Dim> sample_p;

        T v=0;
        for (int n=0; n<N; n++)
        {
            sample_dir=vn1*(circle_pts[r][2*n])
                       +vn2*(circle_pts[r][2*n+1]);

            sample_p=position+sample_dir*radius;

            int Z=std::floor(sample_p[0]);
            int Y=std::floor(sample_p[1]);
            int X=std::floor(sample_p[2]);

            if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                    (Z<0)||(Y<0)||(X<0))
                continue;

            T wz=(sample_p[0]-Z);
            T wy=(sample_p[1]-Y);
            T wx=(sample_p[2]-X);


            {
                T & g=v;
                g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
                g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
                g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
                g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
            }
        }
        T v0=v/N+std::numeric_limits<T>::epsilon();
        v=0;
        for (int n=0; n<N; n++)
        {
            sample_dir=vn1*(circle_pts[r][2*n])
                       +vn2*(circle_pts[r][2*n+1]);

            sample_p=position+sample_dir*radius;

            int Z=std::floor(sample_p[0]);
            int Y=std::floor(sample_p[1]);
            int X=std::floor(sample_p[2]);

            if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                    (Z<0)||(Y<0)||(X<0))
                continue;

            T wz=(sample_p[0]-Z);
            T wy=(sample_p[1]-Y);
            T wx=(sample_p[2]-X);

            T tmp=0;
            {
                T & g=tmp;
                g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
                g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
                g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
                g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*num_alphas,num_alphas-1);
                g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,gm+(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*num_alphas,num_alphas-1);
            }
            T tmp2=(1-tmp/v0);
            tmp2*=tmp2;
            v+=tmp*std::exp(-tmp2/(MedialSigma));
        }

        result=v/N;


        switch(scale_power)
        {
        case 1:
            result*=radius;
            break;
        case 2:
            result*=radius*radius;
            break;
        case 3:
            result*=radius*radius*radius;
            break;
        }
        
//         printf("%f\n",result);

        result=-DataScale*result
               -DataThreshold
               +DataScaleGradVessel*vessel_grad_l;
        return true;
    }
};

#endif
