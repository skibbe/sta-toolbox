#ifndef MHS_DATA_H
#define MHS_DATA_H

#include "rand_help.h"
#include "specialfunc.h"
#include <numeric>

template <typename T,typename TData>
T eval_polynom1(T & x1,
                const TData * alphas,
                int mode=4)
{

    T r;

    const TData * & a=alphas;


    switch (mode)
    {
    case 1: // 2
    {
      r=a[0]+a[1]*x1;
    }break;
  
      
    case 2: // 3
    {
      T x2=x1*x1;

      r=a[0]+a[1]*x1+a[2]*x2;
    }break;
    
    case 3: // 4
    {
       T x2=x1*x1;
       T x3=x2*x1;

       r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3;
    }break;

    case 4: // 5
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;

        r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3+a[4]*x4;
// 	printf("alphas: %f %f %f %f %f\n",a[0],a[1],a[2],a[3],a[4]);
//  	printf("x: %f %f %f %f %f\n",1.0f,x1,x2,x3,x4);
    }break;
    case 5: // 6
    {
	T x2=x1*x1;
	T x3=x2*x1;
	T x4=x3*x1;
	T x5=x4*x1;

	r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3+a[4]*x4+a[5]*x5;      
	
    }break;
    }

    return r;
}

template <typename T,typename TData,int Dim>
T eval_polynom2(Vector<T,Dim>& vector,
                const TData * alphas,
                int mode=4)
{
    sta_assert_error(Dim==2);
    T r;

    const TData * & a=alphas;
    const T & x1=vector.v[1];
    const T & y1=vector.v[0];

    switch (mode)
    {


    case 2: // 4
    {
        T x2=x1*x1;
        T y2=y1*y1;

        r=a[0]+a[1]*x2+a[2]*x1*y1+a[3]*y2;
    }
    break;

    case 4: //9
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x1*y1+a[4]*x3*y1+a[5]*y2+a[6]*x2*y2+a[7]*x1*y3+a[8]*y4;
    }
    break;

    case 6: //16
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T x5=x4*x1;
        T x6=x5*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T y5=y4*y1;
        T y6=y5*y1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x1*y1+a[5]*x3*y1+a[6]*x5*y1+a[7]*y2+a[8]*x2*y2+a[9]*x4*y2+a[10]*x1*y3+a[11]*x3*y3+a[12]*y4+a[13]*x2*y4+a[14]*x1*y5+a[15]*y6;
    }
    break;

    case 8: //25
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T x5=x4*x1;
        T x6=x5*x1;
        T x7=x6*x1;
        T x8=x7*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T y5=y4*y1;
        T y6=y5*y1;
        T y7=y6*y1;
        T y8=y7*y1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x8+a[5]*x1*y1+a[6]*x3*y1+a[7]*x5*y1+a[8]*x7*y1+a[9]*y2+a[10]*x2*y2+a[11]*x4*y2+a[12]*x6*y2+a[13]*x1*y3+a[14]*x3*y3+a[15]*x5*y3+a[16]*y4+a[17]*x2*y4+a[18]*x4*y4+a[19]*x1*y5+a[20]*x3*y5+a[21]*y6+a[22]*x2*y6+a[23]*x1*y7+a[24]*y8;
    }
    break;
    }

    return r;
}



template <typename T,typename TData,int Dim>
T eval_polynom3(Vector<T,Dim>& vector,
                const TData * alphas,
                int mode=4)
{
    sta_assert_error(Dim==3);
    T r;

    const   TData * & a=alphas;
    const T & x1=vector.v[2];
    const T & y1=vector.v[1];
    const T & z1=vector.v[0];

    switch (mode)
    {
    case 4: // 4
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T z2=z1*z1;
        T z3=z2*z1;
        T z4=z3*z1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x1*y1+a[4]*x3*y1+a[5]*y2+a[6]*x2*y2+a[7]*x1*y3+a[8]*y4+a[9]*x1*z1+a[10]*x3*z1+a[11]*y1*z1+a[12]*x2*y1*z1+a[13]*x1*y2*z1+a[14]*y3*z1+a[15]*z2+a[16]*x2*z2+a[17]*x1*y1*z2+a[18]*y2*z2+a[19]*x1*z3+a[20]*y1*z3+a[21]*z4;
    }
    break;

    case 6: // 4
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T x5=x4*x1;
        T x6=x5*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T y5=y4*y1;
        T y6=y5*y1;
        T z2=z1*z1;
        T z3=z2*z1;
        T z4=z3*z1;
        T z5=z4*z1;
        T z6=z5*z1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x1*y1+a[5]*x3*y1+a[6]*x5*y1+a[7]*y2+a[8]*x2*y2+a[9]*x4*y2+a[10]*x1*y3+a[11]*x3*y3+a[12]*y4+a[13]*x2*y4+a[14]*x1*y5+a[15]*y6+a[16]*x1*z1+a[17]*x3*z1+a[18]*x5*z1+a[19]*y1*z1+a[20]*x2*y1*z1+a[21]*x4*y1*z1+a[22]*x1*y2*z1+a[23]*x3*y2*z1+a[24]*y3*z1+a[25]*x2*y3*z1+a[26]*x1*y4*z1+a[27]*y5*z1+a[28]*z2+a[29]*x2*z2+a[30]*x4*z2+a[31]*x1*y1*z2+a[32]*x3*y1*z2+a[33]*y2*z2+a[34]*x2*y2*z2+a[35]*x1*y3*z2+a[36]*y4*z2+a[37]*x1*z3+a[38]*x3*z3+a[39]*y1*z3+a[40]*x2*y1*z3+a[41]*x1*y2*z3+a[42]*y3*z3+a[43]*z4+a[44]*x2*z4+a[45]*x1*y1*z4+a[46]*y2*z4+a[47]*x1*z5+a[48]*y1*z5+a[49]*z6;
    }
    break;

    case 2: // 4
    {
        T x2=x1*x1;
        T y2=y1*y1;
        T z2=z1*z1;

        r=a[0]+a[1]*x2+a[2]*x1*y1+a[3]*y2+a[4]*x1*z1+a[5]*y1*z1+a[6]*z2;
    }
    break;

    default:
        sta_assert_error(1==0);

    }
    return r;
}




template <typename T,typename TData,int Dim>
T eval_polynom(
    Vector<T,Dim>& dir,
    T scale,
    const TData * alphas,
    int numalphas)
{
//Vector<T,Dim> vector=dir*std::sqrt(scale+10) ;
    Vector<T,Dim> vector=dir*scale;
    if (Dim==1)
    {
        int mode=-1;
        switch (numalphas)
        {
	case 2:
            mode=1;
            break;  
	case 3:
            mode=2;
            break;  
	case 4:
            mode=3;
            break;  
        case 5:
            mode=4;
            break;
        }

        return eval_polynom1<T,TData>(vector[0],alphas,mode);
    }

    if (Dim==2)
    {
        int mode=-1;
        switch (numalphas)
        {
        case 4:
            mode=2;
            break;

        case 9:
            mode=4;
            break;

        case 16:
            mode=6;
            break;

        case 25:
            mode=8;
            break;
        }

        return eval_polynom2<T,TData,Dim>(vector,alphas,mode);
    }
    if (Dim==3)
    {

        int mode=-1;
        switch (numalphas)
        {
        case 7:
            mode=2;
            break;
        case 22:
            mode=4;
            break;
        case 50:
            mode=6;
            break;
        }
        return eval_polynom3<T,TData,Dim>(vector,alphas,mode);

    }
    sta_assert_error("eval_polynom");
    return 0;
}


template <typename T,typename TData>
T eval_polynom(T scale,
               const TData * alphas,
               int numalphas)
{

    Vector<T,1> vector;
    vector=scale;
    {
        int mode=-1;
        switch (numalphas)
        {
        case 5:
            mode=4;
            break;
        }

        return eval_polynom1<T,TData,1>(vector,alphas,mode);
    }

}


template <typename T,typename TData,int Dim>
class CData
{
protected:
    std::size_t shape[3];
    
public:
    CData()
    {
      for (int i=0;i<Dim;i++)
      shape[i]=0; 
    }
    
    //virtual ~CData()=0;
    virtual ~CData(){};
  
    virtual void init(const mxArray * feature_struct)=0;

    virtual void set_params(const mxArray * params=NULL)=0;
    
    virtual bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T scale)=0;

   template<typename S>
    void getshape(S shape[])
      {
	for (int i=0;i<Dim;i++)
	{
	  sta_assert(this->shape[i]>0);
	  shape[i]=this->shape[i];
	}
      }
      
   
    virtual float get_minscale()=0;
    virtual float get_maxscale()=0;
    virtual float get_scalecorrection()=0;
   
};

#endif
