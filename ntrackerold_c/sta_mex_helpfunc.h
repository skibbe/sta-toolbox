#ifndef STA_MEX_HELPFUNC
#define STA_MEX_HELPFUNC

#include "mex.h"
#include "mhs_error.h"
#include <complex>
#include <vector>
#include <algorithm> 
#include <string>

#ifndef printf
  #define printf mexPrintf
#endif




// #ifdef __cplusplus 
//     extern "C" bool utIsInterruptPending();
// #else
//     extern bool utIsInterruptPending();
// #endif

//  #ifndef _mxGetPropertyPtr_
//   #define _mxGetPropertyPtr_
//     /*
//      * mxGetPropertyPtr by James Tursa
//      * see 
//      * mxGetPropertyPtr/mxGetPropertyPtr_20110307.pdf
//      * for further details
//     */
//     #include "./mxGetPropertyPtr/mxGetPropertyPtr.c"
//   #endif 




namespace  mhs
{
 
 
template<typename T>
T * mex_getPtr(const mxArray *mx_array,
	       std::vector<std::size_t> & dim)
{   
    //const mxArray *mx_array= mxGetField(feature_struct,0,(char *)(fieldname.c_str()));
    sta_assert_error(mx_array!=NULL);
    const int  mx_ndims = mxGetNumberOfDimensions ( mx_array );
    const int *mx_dims = mxGetDimensions ( mx_array );
    dim.resize(mx_ndims);
    for (int i=0;i<mx_ndims;i++)
      dim[i]=mx_dims[i];
    return  ( T * ) ( mxGetData ( mx_array ) );
}

template<typename T>
T * mex_getPtr(const mxArray *mx_array)
{   
    std::vector<std::size_t>  dim;
    return  mex_getPtr<T>(mx_array,dim);
}


template<typename T>
T * mex_getFieldPtr(const mxArray *mx_array,
	       std::string fieldname,
	       std::vector<std::size_t> & dim)
{   
    sta_assert_error(mxIsStruct(mx_array));
    const mxArray *mx_array2= mxGetField(mx_array,0,(char *)(fieldname.c_str()));
    T * ptr=NULL;
    try
    {
      ptr=mex_getPtr<T>(mx_array2,dim);
    }catch (mhs::STAError error)
    {
	throw error;
    }
     
    return ptr;
}

template<typename T>
T * mex_getFieldPtr(const mxArray *mx_array,
	       std::string fieldname)
{   
    std::vector<std::size_t>  dim;
    return mex_getFieldPtr<T>(mx_array,fieldname,dim);
}


template<typename T>
class dataArray
{
public:
  T * data;
  std::vector<std::size_t>  dim;
  dataArray(const mxArray *mx_array,std::string field="")
  {
    try {
      if (mxIsStruct(mx_array))
      {
	data=mex_getFieldPtr<T>(mx_array,field,dim);
      }else
      {
	data=mex_getPtr<T>(mx_array,dim);
      }
      std::reverse(dim.begin(),dim.end());
    }catch (mhs::STAError error)
    {
	throw error;
    }
  }
  
  dataArray() : data(NULL){};
  
  void print()
  {
    printf("[");
    for (int i=0;i<dim.size();i++)
    printf(" %d ",dim[i]);  
    printf("]\n");
  }
};  
  

void mex_initMatlabFFTW()
{
    mexEvalString("ifft(fft(single([1,2,3])));ifft(fft(double([1,2,3])));");
}

void mex_dumpStringNOW()
{
#ifndef _VALGIND_CONSOLE_  
    mexEvalString("drawnow;");
#endif
}




template <typename T>
std::complex<T> mex_getComplexValue(const mxArray *  ptr)
{
    if (mxGetNumberOfDimensions(ptr)==2)
    {
        if (mxGetDimensions(ptr)[0]*mxGetDimensions(ptr)[1]==2)
            return std::complex<T>((*(T*)mxGetData(ptr)),(*((T*)mxGetData(ptr)+1)));
        if (mxGetDimensions(ptr)[0]*mxGetDimensions(ptr)[1]==1)
            return (*(T*)mxGetData(ptr));
    }

    mexErrMsgTxt("cannot parse the alpha parameter?!\n");
    return T(1.0);
}


std::string mex_mex2string(const mxArray * s)
{
    char buffer[255];
    if (mxGetString(s, buffer, 255)==0)
        return(std::string(buffer));
    else mexErrMsgTxt("error parsing string\n");
    return("");
}

template<typename T>
std::string mex_value2string(T value)
{
    std::stringstream s;
    s<<value;
    return s.str();
}


template <typename T>
mxClassID mex_getClassId()
{
    return  mxUNKNOWN_CLASS;
}

template<>
mxClassID mex_getClassId<float>()
{
    return mxSINGLE_CLASS;
}

template<>
mxClassID mex_getClassId<double>()
{
    return  mxDOUBLE_CLASS;
}


template <typename T>
std::string mex_getPrecisionFlag()
{
    return  "unknown";
}

template<>
std::string mex_getPrecisionFlag<float>()
{
    return "single";
}

template<>
std::string mex_getPrecisionFlag<double>()
{
    return  "double";
}



template<typename T>
bool mex_isStaFieldStruct(const mxArray * s)
{
    if (!mxIsStruct(s))
        return false;
    if (mxGetField(s,0,"storage")==NULL)
        return false;
    if (mxGetField(s,0,"L")==NULL)
        return false;
    if (mxGetField(s,0,"type")==NULL)
        return false;
    if (mxGetField(s,0,"data")==NULL)
        return false;
    if (mxGetClassID(mxGetField(s,0,(char*)"data"))!=mex_getClassId<T>())
        return false;
    return true;
}



template<typename T>
std::vector<T> mex_getParam_helper(std::string params)
{
    for (std::size_t a=0;a<params.length();a++)
        if (params[a]==',')
            params[a]=' ';
    std::vector<T> param_v;
    std::stringstream param_s;
    param_s<<params;
    int eexit=0;
    while (!param_s.eof() && param_s.good())
    {
        T tmp;
        param_s>>tmp;
        param_v.push_back(tmp);
        eexit++;
        if (eexit>42)
            mexErrMsgTxt("ahhh, something went completely wrong while parsing parameters! ");
    }
    return param_v;
}


int mex_hasParam(const mxArray * plist,std::string s,bool required=false)
{
    if (!mxIsCell(plist))
        mexErrMsgTxt("error hasParam: parameterlist is not a cell array\n");
    if (mxGetNumberOfElements(plist)%2==1)
        mexErrMsgTxt("error hasParam:  length of parameterlist is not even");

    for (std::size_t t=0;t<mxGetNumberOfElements(plist);t+=2)
    {
        if (mxGetClassID(mxGetCell(plist,t))!=mxCHAR_CLASS)
        {
            std::stringstream s;
            s<<"error: parameter "<<t<<"is not a string"<<"\n";
            mexErrMsgTxt(s.str().c_str());
        }

        std::string param(mex_mex2string(mxGetCell(plist,t)));

	//printf("%s %s %d\n",s.c_str(),param.c_str(),(param==s));
	
        if (param==s) return (int)t;
    }
    if (required)
    {
        std::string error="error: missing parameter "+s+"\n";
        mexErrMsgTxt(error.c_str());
    }
    return -1;
}



mxArray * mex_getParamPtr(const mxArray * plist,std::string s)
{
    int pos=mex_hasParam(plist,s);
    if (pos==-1)
        mexErrMsgTxt("error getParamPtr: couldn't find parameter\n");
    return(mxGetCell(plist,pos+1));
}



std::string  mex_getParamStr(const mxArray * plist,std::string s)
{
    if (mex_hasParam(plist,s)==-1)
        mexErrMsgTxt("error getParam: couldn't find parameter\n");

    mxArray * params=mex_getParamPtr(plist,s);



    if (mxGetClassID(params)!=mxCHAR_CLASS)
        mexErrMsgTxt("string parameter expected!\n");

    return(mex_mex2string(params));
}



template<typename T>
std::vector<std::complex<T> > mex_getParamC(const mxArray * plist,std::string s,int expected=-1)
{
    if (mex_hasParam(plist,s)==-1)
        mexErrMsgTxt("error getParam: couldn't find parameter\n");

    mxArray * params=mex_getParamPtr(plist,s);

    int numParams=mxGetNumberOfElements(params);
    if ((numParams!=expected)&&(expected!=-1))
        mexErrMsgTxt("number of parameters differs from number of expected parameters!\n");

    if (mxGetClassID(params)==mxCHAR_CLASS)
        mexErrMsgTxt("string parameter cannot be parsed with this function!\n");

    std::vector<std::complex<T> > result(numParams);


    for (int t=0;t<numParams;t++)
    {
        switch (mxGetClassID(params))
        {

        case mxSINGLE_CLASS:
        {
            float * data_r=(float *)mxGetPr(params);
            float * data_i=(float *)mxGetPi(params);
            if (data_i!=NULL)
                result[t]=std::complex<T>((T)data_r[t],(T)data_i[t]);
            else
                result[t]=(T)data_r[t];
        }
        break;

        case mxDOUBLE_CLASS:
        {
            double * data_r=(double *)mxGetPr(params);
            double * data_i=(double *)mxGetPi(params);
            if (data_i!=NULL)
                result[t]=std::complex<T>((T)data_r[t],(T)data_i[t]);
            else
                result[t]=(T)data_r[t];
        }
        break;
        default:
            mexErrMsgTxt("unsupported data type!\n");
        }
    }
    return result;
}

template<typename T>
std::vector<T> mex_getParam(const mxArray * plist,std::string s,int expected=-1)
{
    if (mex_hasParam(plist,s)==-1)
        mexErrMsgTxt("error getParam: couldn't find parameter\n");

    mxArray * params=mex_getParamPtr(plist,s);

    int numParams=mxGetNumberOfElements(params);
    if ((numParams!=expected)&&(expected!=-1))
        mexErrMsgTxt("number of parameters differs from number of expected parameters!\n");

    if (mxGetClassID(params)==mxCHAR_CLASS)
        mexErrMsgTxt("string parameter cannot be parsed with this function!\n");

    std::vector<T > result(numParams);



    for (int t=0;t<numParams;t++)
    {
        switch (mxGetClassID(params))
        {

        case mxSINGLE_CLASS:
        {
            float * data_r=(float *)mxGetPr(params);
            result[t]=(T)data_r[t];
        } break;

        case mxDOUBLE_CLASS:
        {
            double * data_r=(double *)mxGetPr(params);
            result[t]=(T)data_r[t];
        } break;

        case mxLOGICAL_CLASS:
        {
            bool * data_r=(bool *)mxGetPr(params);
            result[t]=(T)data_r[t];
        } break;
        default:
            mexErrMsgTxt("unsupported data type!\n");
        }
    }
    return result;
}

template<typename T>
T mex_get1Param(const mxArray * param)
{
    int numParams=mxGetNumberOfElements(param);
    if (numParams!=1)
        mexErrMsgTxt("number of parameters differs from 1 (number of expected parameters)!\n");
    T result=-1;
    switch (mxGetClassID(param))
    {

    case mxSINGLE_CLASS:
    {
        float * data_r=(float *)mxGetPr(param);
        result=(T)data_r[0];
    } break;

    case mxDOUBLE_CLASS:
    {
        double * data_r=(double *)mxGetPr(param);
        result=(T)data_r[0];
    } break;

    case mxLOGICAL_CLASS:
    {
        bool * data_r=(bool *)mxGetPr(param);
        result=(T)data_r[0];
    } break;

    default:
        mexErrMsgTxt("unsupported data type!\n");
    }
    return result;
}

template<typename T>
std::complex<T> mex_get1ParamC(const mxArray * param)
{
    int numParams=mxGetNumberOfElements(param);
    if (numParams!=1)
        mexErrMsgTxt("number of parameters differs from 1 (number of expected parameters)!\n");
    std::complex<T>  result=-1;
    switch (mxGetClassID(param))
    {

    case mxSINGLE_CLASS:
    {
        float * data_r=(float *)mxGetPr(param);
        float * data_i=(float *)mxGetPi(param);
        if (data_i!=NULL)
            result=std::complex<T>((T)data_r[0],(T)data_i[0]);
        else
            result=(T)data_r[0];
    }
    break;

    case mxDOUBLE_CLASS:
    {
        double * data_r=(double *)mxGetPr(param);
        double * data_i=(double *)mxGetPi(param);
        if (data_i!=NULL)
            result=std::complex<T>((T)data_r[0],(T)data_i[0]);
        else
            result=(T)data_r[0];
    }
    break;

    default:
        mexErrMsgTxt("unsupported data type!\n");
    }
    return result;
}
}

#endif





