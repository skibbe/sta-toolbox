(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     30081,        656]
NotebookOptionsPosition[     28700,        609]
NotebookOutlinePosition[     29036,        624]
CellTagsIndexPosition[     28993,        621]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"R1", "=", "5"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"o", "=", "0.1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"G", "[", "x_", "]"}], ":=", 
   RowBox[{"1", "/", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Exp", "[", 
       RowBox[{
        RowBox[{"-", 
         RowBox[{"(", 
          RowBox[{"x", "-", "1"}], ")"}]}], "*", "5"}], "]"}], "+", "1"}], 
     ")"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"G", "[", "0", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"G", "[", "2", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"G", "[", "x", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "0", ",", "R1"}], "}"}], ",", 
   RowBox[{"PlotRange", "\[Rule]", "Full"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.5732581862954893`*^9, 3.57325827502124*^9}, {
  3.573258323653535*^9, 3.573258390876266*^9}, {3.573258486371995*^9, 
  3.573258520403851*^9}}],

Cell[BoxData["0.0066928509242848554`"], "Output",
 CellChangeTimes->{{3.573258512139554*^9, 3.573258520886292*^9}, 
   3.573258653013274*^9}],

Cell[BoxData["0.9933071490757153`"], "Output",
 CellChangeTimes->{{3.573258512139554*^9, 3.573258520886292*^9}, 
   3.573258653014803*^9}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwd1nc8ld8fAPDLNa910U4oQpRSkkSfo6SIhihKRImsUomMhspIInuLlBGh
jIzsLXtkfmVvN3vzO8/vn/u83q/nuefzOed8Pud5dhve1zCiJ5FI5/EPcQ17
Zsgkdcf+5Jnhp3cX7tlDcaOg2jVBHYhwXLsbHmEPG6W5rwQF78HTgs/Jic32
EJbvIcAo+BTkvPUQy2kHMFewtlgQcIPPerJvi/Y4QuOKie8/gSAgKdVasy48
gydl/IdoKcHw1fvust+R57DDp6lq4nQoFBqdXg548BwMxRF5zDgCIgKaJjfG
noNtAlvm9J8oEJHrVZd//AKYO3pFJwTioNVWccPc4SW05v+yHh+Og+a9DC/8
wl9C/JfAorGUeIjrNt9LKngJFx5e0B89nQDzzKgljMkJ/Fmz/IeMk8CAj6OK
xc8JeELL6af+fIdZl+perrxXEFkZYTUmkAle9Jz6CeedYTYwTbz7XSYIkPq6
K+2cQdm4qq9hKRP6UyVbz8c7wzh5QSurMQt4dTo0bSkuIKtwQc7NJQciwm50
fqh1gbqkVXpRWh6QrwnKPrdwAx5g1eOQzIelI+daRD65wQkRXzmXO/mg+Mj4
+Mk2N3g/GzdjV58PaXKUjjzltyD9odnI8GsB3E1vLRQVdYfnlftVpfSL4Lrz
pLfO+juITckQ+epXBF/GEj3MTnhAQ+Ap8t7fRUD+I6G6ausBwsbaOduOF4Nr
bg9T+JwHVJJfS5J4SyAg88rk+ux72KLQwVNbWgofKk51ezp7wbcktw5zyUq4
9uie8rlmbxjlGX2nalAJC4VhKyLL3iD6RPWkmG8lXLYPfa4m4AMf5dki+5Yq
IeHUoQ0LUx/wrnh393pJFXw1dryxncEXnvR5Tp3VrYZb8/z6rmf84Lvyv6i9
ntUgWLNZkPe+H9DiLmmSC6uBoz/u1ESgH5g84M7IFa0Bq5PT/z5M+MH1NW+H
ozM1kB65nyEx2B8i2KpecZ+pg3HB0uREhkA4qWCv+0KvDprPmj1/fCQQOi0l
jtJs8H2NKklfw0DY1uA++Du+Dv7SHFP+FgTCh0A1FVdqPQxLTVHyXwfBq701
nBsd9WCwGuK0jy8EjKE+aPxhI6SYjgbd8w8HUtR8Vod7I0gan3AwrwuHQAa+
zsroRlC9v3m2jRIB5RV3+eNaGqHdx/jFwosIENNcjTKSawJ0/Ue02PGPMGwi
mvAfXTOw9+225zkaCabejrl1Xi0gPlLt8EfnE9DPRXXnxbVAjpz7E4VnnyD4
WjkpqbAFfu612D8Q9Qkq+XhPe8y2QCq0PVsZ/wQSMbFlKtp/YFz+fc7F19Ew
nt1YVyjQCj/MEnQW8z6D5cC+vtRvbRCVerjtsWUs/NVrYymoaAO/Si/b6ohY
uNLqKlnd3wZtyqeTX9fHgmzV0NOBHe24Dl6Zc8vEATnlM3WLSzu43xzpTCPH
Q6DD7pM2+h3w/MQmseyUr1DAuz3oOLUL/JiyjxjcTAJpj/LcMxJdQDG8rNcQ
kgRfmGz7Lyt3wUK1X0hGexK8W2o5aOrQBaR6Ib9/+5PhardvafBwFzjsc9Tv
/ZYMo/HU2ZX8/yBE0tdKNjcFKLtfi3bL/oXZmIXJcxypUC7/4eCzM3+BZqrx
tk0qFZy1w4/t0vgL392deGOupgK918+zN8z+QkdxoNTyx1RYXh83/hP2F2oD
pR66yabBWKdWbC19Dzh5rGx4P0qH34Gi+/J/90Dj3Z4zHjsz4W2qtJR+Ww/4
Ci9v0zmXCefqFI+vD/RAB9MHFnPrTChm1lWR3+iBTsMfTiZ1mZDzxOveT6le
WI3bV1zslgWJmktxyf69kBG+X/QyJQfeU6vEI2/1gVqeAu+gUh5kHtuVdsGy
D5RqrqTlPc+Dfr37sGrfBy/Onn+ymJUHJxJ5Na8F9EGCZ62x0vZ8GFbRdeSo
6YMMz5j3W33y4fSridqnJ/rBxzuAZ8qzABbnuKw1tgzAtLZdbXhSEQjtMtzY
EBqAO/85yV1pLIILSqluiVID8Odkvo38QhF89tYOZ1EfgNJzETOJUAwaByPL
cl8NwL7JbWHUxmJINDm8Q2JqAOQi3ov9I5eCYYdmHvn3INQxnr5R/74cKCE9
FlZtg7BOF/TY62c5fL9uydc9OAi/OtMfvuktB3K7y9MsuiHI6y5U5T5WAdGt
WUesjg2B/hdpu5TeChhsFoz9L2oIjgSdcNuuVAWmdeNemU+HQdu8QcL5IO7z
kteG90VHgUuz90C6RgPw9yszzR8ZhfFCCXYd8waoomeNd0CjcOLhrDy3cwPs
RR5TbjqjYLIkKFeQ2QBtWX4vot1HoaXdk3HXnkZQTP4S3kYbhc99vBaJ843A
HVLefvrnGJjGruoYfWuGlAfsV7apTMCcttNWjUttEGlvNnro2j94+8bra0nb
X3CKWL08Oz4Fipeyzz9P6IdLh35aScxMgbolR1RKVj/wFzz6YLg0BctCJ5s6
yvshq3e0ro5xGmKLhx23DvTD9N62i4n803Cvila9b9cAGCakqd+9NA1c8mKX
490HQDHTUqX1xzRUZatPfDYYBE5V8XtcWdMQdNVxKMVyEDrbB1yV86chcvBT
2g/7QbBd1S1P/T0NaeW9IYF+g5AM589+GJiGmQ0ZGCwfBMFS0TOqW2fg09rv
rS4HhoDU+Bfl2M2A889wl9DJIXg9khtp93IGSDX7t6ivDAErKYz+uOsMeHkE
a/5jHgaeAzrFaX4zYJ//oIRz9zDsdak/m5Q8A8bp3iXxGsOgeqLwQtTgDGS1
0H28/mMYfD590nXVmIXJw/yDTqYjsC3r5S9lnVlQut6i8NJ6BELr9PkZb83C
3vXKMasXI/BlbefflxazcNCk9qK4/wj8vOZ72951Fko9TU0X8kegk+2NqWXu
LKwoyEsybhoFkcd3bTXF56D+8gcHtuRROCDXfSXi0By0/zMi5/wcBWmS9sFR
mTmIr/3Ab1AwCqfeqQw+Pz0H1BbXfW4No6AXvV/zq+4cXCSfUbg8Owr+TdMH
6T3noKOrhKPs6BgwHn0+lDQzBw8yvKvtk8eAfWWhcHlpDl6GV4r/zhgDnoIH
4WdI83DFSZSZJ28MBNUNtTrY52HbSpv+s+oxkDc6U8QkMg8wfdS5fWQMHvux
RdzUngdLGz3HWMFx6J8LuMr2ax44bNaOdzqPQ5TikUddRfMwvk/32AOPcdD3
qPFMqpyH5cgxhSWfcWgXZqy40joPNxYFg0Y/jkP9lYcnQmfmoZZEN3Y+axxy
U9QED4gvwF2WBtFNY+MQYEE3eiFgAaIDNLg2n5kAzcxQpt3hC7AS9m/dUHUC
eBhlhWaiF+Cro493zMUJeB9qqRvwfQF2SJ9u23R9Al5XddR0Vy+Ay3vm8y8t
J+DBvvQfDxgWYZPnruEEvwlQHTB19LZahFAGtz0xXROg7CiUoGS7CA0c9tdr
eibg1ObO9vlni5AcPc85OTABcmfUZW+8W4Qrhmw7t05OgPjngzNCMYsQI3lu
lbo+AWxGsyZpnYtgzSUUn8g3Cb/7HTVbzy6BYdk5WovmJJQ7yLx6e2EJMrQ9
uIW0J6F4Ey1FXmsJmrdQKPduTEKO0i2uSMMlWE0T/tJrMAkJ0acq7zkuAYPn
z08elpPgcYcZrXxfAtWUzVernCfhQr+XBD//MjTKMZwX+4HjSfQ36Qovw4tP
3qf90ibh/KNjz0LEl+HT+NzAWsYknKP/r27bsWUgZxgpFeZMwilBiSe8l5bh
qfleiW2lk3BUtySfxWkZaOJcYet/JoGvaUlrZmAZhMVKD/QvTkLwTvUNqfFl
uNYWqN+4PAnbb3+MfTC9DGDTz5a3OglbppVXJtaXgS8nc8ydRAMq1ffj8NYV
2JSrZrHKTAOymuRYl8oK5EorvlbYQoPRIsMX5YkrkMp2IO3uIRoIfGg1fZ+6
AtfUXqpxH6aBpt4FLc3sFZApjPf4eYQGuYvHxf+Wr8B4RI7ImgwNvA9wNy30
rUC6mbqhvgINTgTkiYruWIWVF3WXPFRo8ODOUZ4JwVXoGn1kxHieBl+kvq5+
F12Fb+zotZ0azu+3f/3Jo6vA/IN54cZFGvSTLO2vXlqFsDNiF1Y1afDOlK/2
jfMqCC93uWXo06DgmHfmeY9VgBqDFnoDGswzsERz+65Cup/nq/OGNLgVMWMb
FrkK3xdOb2q+QwPppso9aTmr4ODqH1JxjwZdCk+f9E+vgo2vmJv8IxpIcrfs
Oq2/BnG6xddNX9Ng0GPQyc9oDTSvZo3rvKFBGGVhaMhsDVLXzqWddaYBO8O2
7+9s1yDG9/bCLle8fvM6yi0f1oCpMT04zZ0Gnzu7LO8Vr4HR5SZhX28a6OpM
NmZXrkG9aP5HEx8abGpZl+WsXwPndx5vTvjSwKlWgPyjaw0+VEb5dfrh/AsN
Albn1+AEq+Vu9iAa8MUO5HnuWwcWlaA7QhE0aBKeF+49uA475PnLurHdI5ne
SsusA8/rD3nBH2mwHCyq2XZqHX5MtzuwR9Gg1ePe8B7ddbjmuf1hdzQNfB5N
UNPfr4Mbv+TLM/E0oMCcQefMOnA+fLpNKJUGtopiLOnL67jepzSLsAdP3/jm
SbcBNE/eU4ZpNCg8V7B8imsDihN5joem08BOw8MnTnwDSvYx9JMyaTBmtLfk
icEGrPWQLDx/0eC6ibbZJZMNaDb1HNuZS4NyU3du8fsbIKJgejoGO/r+1M0u
hw2oN3/hmpWH1+vpr/nTgRuQ88AtrrGABr/faYlx125AY0DNqYYSGsh5utaM
Nm/AF3Oue+qlNIj9kP24uHMDNsfbvivDfu2/u8BmFP/fbHfjzzIa8NfN76fR
k1DgfosgzwoatLQVlmjxk9BbPc8KrmoanJ24vrBHi4Q46iWHjRtpEL7pVeNz
bRKKSILcYuzZE1+TOm+QUHHSlibBJhp8fLti7G9IQvaNciUt2Muioa2sD0ho
R2L80PEWGiQYdmX+e0tCL583W//XSgP6t4z+6u9JaNFTcflgGw20Uw48jP9A
QjFs8c0vsBlJz8TvBJLQ/ESbv2A7DfTC+EP+fCahSXk6E+0O3B+t+g65+SSU
Pc9vHN9Fg7sbLto7i/F8zkotTmHniCRL25bh+NJps7L/0eCeNWlCqoaEOhNa
pEuwC3kib37uIKEqvRvTLd00eKzee/LdPAndPR1g8LcH10fhHbrr++lQrcCi
r/cADf4ptu9wPEiH8tMHWEqxmQsuSn88TIeG5xe2LWHL5MkZD8rSISdwp+oN
0sA3m1r98AwdGp3a81BoiAYXU3MC3fXoUMMp2Y+hwzjfI4e/fzOgQ9ni0Qcq
sZ99j6mqv0OH/rkEGC5gJyR7r281o0O/ynJLLo3gekw0uRNtQ4dKPO6/2sAu
/bzp0C8vnE+Cupn6GA3+E36r8teHDq2GvM+1xp77tGFIDqBD/l0PisKwhaNG
/VTC6FCo0ZP+cWyn8PyV5jg61DRS7+MyToOTAeZlk4V06Hiet0jSBA3SXIv1
BOfoUFSUdkclDdeTatU2l0U6RLnTtH0CO5i9oWFihQ5d5t5B5fpHgxde3crZ
9PTI7ustEQ1stcBlyatUenQ7TpjSjN0bc2jDXYIehcHY/popGjTfO5Y5LUmP
ypwc949jl0ucfKRzmB45/S4ZZ52mQWLS+SGR4/So6JN7ihK2bcbd2gJlerRg
JHM9A5uzLDRiwYAe6Y0M+n+YoQGd2ycdPSN6pOA+0vUVe1Y1nrfEhB4N11pP
lGC31WS4eN+nRx9F1oyXsKNbGu4fcKRHylcu/rk5i/tnkAXdDqRHRqEKpF1z
eH8Yrf/W1NCj4pNbrtnM02CHQMWflXp6pKg3J/4Wu0Z2V61YMz26wZDSFoot
Y17yy6mDHjmn9JQXYDM1bg6RGaZHzZsZ5lkX8Pn3MV0rnI6MGncZlvtga2dR
1KsYyEikZYdZNDZ7k57SIjMZpWSSJlKxHzMzH9HgJKNQtquezdhKlteoTDvJ
yMl/u9bmRfw+OLFYaSGNfYF5pyd2oJZaYfAxMjLgq6sOw1a7/zGzTI6MLrX5
307ATo06F7tbkYzslemPV2C/Zg1606xORo/DrT/QLeH6+HMcKRiTUfJZTzEL
7NsP7dPYg8lIW1tTW2oZz+/wf2n7wsio7RlvoDw2ZRqlK38koxIV9sKz2MlW
TBkvvpDRUKtG803s1Qcffs5+JyM5n6w1V2z/+zHZnVVk9Oqw2NMO7HsHKTlL
NWSkfsHz/gC2PM08Z0sDGcmXJWjQsPssD/+61EpGI+kR3fQrNDhk+Su3uJ+M
gk+YbNuHXWHeWJCwRkYBXqnCD7FD98sUVpAYEO+V2wt22A/GAwsHyQzofPDq
z1fYW8z1igQpDMjt00NWP2xDs5Fi3y0MyFBxuTcde/XeRpnDQQYkPrzddwH7
oLFEjZoBAxK19cmzWMX9dU/gfuIdBhTlno6ssd+Z81I5TRjQVost3x2wR6xW
LtdaMqAtX6h33bE/OVQ1X3ZgQC0nHLbEYm/1Nuu6GsCAtH1yU//DLvXVf5YR
zIAaTULPDWA/DrgisC2cAZEpcTVj2PWhJwzaohkQx5PClEVs9xi2gRvfGZDK
Yd4/3Gs02Mj5On6rmgFVmGilK2J/y4vwKKhjQHvbZezOYusW+kjuaWJAn1NX
pNSxM8vsH/S1MyAGET9HbexHDednjYYZkB2twMUSe2hobMWUzIh8cqQcA7Fr
efazWR9nRAkWje792CctLEZC5BnRnWbhb8NEvLJvZYXAiBLfXC8fx37vIPWa
qsyI1I9+75/FVhuUWfuqwYguK5XnMqzToDJTkdZrxogqGv4wCmHLbXpVzXqf
EQ1mhSeKYMdbFn899JARzYrKXRDHdhM6a/LMlhEVJYnYSWGf81Dr2f6GEdGu
6DwD7JJb1xovhTOiUt3+09expbMDU2wiGVFfzMXnN7GjN7d7hkczIunx26m3
sF9X6qqNxzOiaLpMNmPsU9KGJS4ZjKjqmdmDR9j5zBYZuXWMaFxLi88dO/ub
U8h+MhMqrchWS8dmUtPaEcDEhFw1QlgzsS+PiAbRUZjQ9wNx+dnYQ0LV/i1U
JrRN5uWuAmzeoK3eL/iZ0Iz2E58qbLNXCa5Nx5nQxkaeXTd2uuBzZlBgQrqO
2/72YNPlXnaOQ0zIQUUH9WMHLM6/enaWCR0i5U0NYxdZKD4X02JCFapK26ex
d2q3WDs8YEJGQbKvyRv4vJqLnR18xIS6ohdCGbFTvO0fXbZhQt0liinM2Ger
Ba1EnuF8t4T9ZsN+dMrMvM6dCQUef1zLi121n3RbOIYJ1cw4cwhh29OJX/rd
xYRUHFy/nMQeVGJrFelhQl5ndsoh7Euu4/ov+5kQX/HtSkVsYa6k+zJjTEhc
RapHCfs3n7Tnx0UmFHvIflgVm1/2ZM1jXmbkuOB0+yq2q73A1dotzEjx0duS
a9jTuaT/9u1gRpLMScI62GVniib+E2TG7wfj9hvYD66c41CRZEb+N08IG2AX
Wmqo7VJhRs2Wj2TMsPd/P9Jko8aMFKwYLMyx/ec26TZcZEYLJzkiLbDNHP6Y
uVxlRhqaIXQPsDe91XWfus2M7sfJfX5MrN/nu5Ulz5iRrcCEnSM2pdPunGUq
MyrVV37pgS1jE8fhmcGMIuPg1ntsA57WhqQsZnQm11nBE/vnuaM3p/KZ0U0b
thkvYrw0mtXjamZkw6Wq6otd8P5OiN0gM0oJ00sIxh7f53MrZIQZ/RX/YRWC
va2kYG/OODN6YuVyNBTbcoU/eW2aGUm1ZP0Mw+YzaSt+scGMuK3CEj9uEN+j
Fyadt7Gg1PtFV75gR3c6/IjZyYIoB+7Sx2DX2ny1LednQelNqUmERb6xkCl7
WVCPzC2mOOzGHUVbPaRYUPxTmeiv2JKzxxR9VFmQphclOhl74Mtu3wgHFnRf
L644k8j3dc8S+QULmuv/p51F7JdhpL7JKxb0Q29jjDBJYLeE1FsWNPzUnjMH
e0eAYGFhAAtqPnXsTC5RLy4C/wa+s6C7T/rsC7GvGXVrqaazoKGbxWuE9U5H
ZH/LZEHS3D7Pioj92uB3sclnQQM99I7F2M42/PysNSwo8bKpRSl2jvEutf0j
RP5y/JXYRWe6UrzGWdAOtTVPwpVCYVvnaCyIbqqWVIX9p5uv79c8C0LBCd2E
p6/x2V1kYEW32596V2OLndsZ+1CAFY1epNTUYfuKbWfI1GJFto5mcS3Yunoi
MX3arEhMcXSZsLDvEVVOXVakpFt+/g926oa6121DViTkFjNGuKnFiY/zPivK
KucVbsPmfTMufduVFQ0+TbDtwG7PWvrj4c6KJOeS8glH/WOy//meFTXb7mXp
xD6su7uAw48VvZs39SOsceSq+s9IVrT41zGuC9u7J8+II5sVbVVK/NmNfX1r
NYtsLivq7Li4SHiPevtXwwJWpEKyPvYX+/vP2amMMlb0uFI1lXCD575nhk2s
yGHpZGwPNvdJH/+MCVY0fGubXR+2V/DdMgNBClpZiakaxLZ54vTIQYiCmhr1
VgnraYQLBIhQUPXrjP1DRP9RWmx+76cgv3OX3xGueHpG7NhxCoocPn52GJte
R9iNXYOC3qbZJIxgj0ijo6JaFOTqEP6HcB1Vt0dRm4ImO63oR7Ejyn2O2+hR
kN2E4DXC8rLk0R5TCsqNSlklbL21VzXjFQU1iAkfHyf2Z2Ztvt6Zghbsb+oT
Vqrd/mncjYKcJ9XeEOZxubyy24uC1s0u1RH+Np//9V0oBeXtWTKcwB5ujmA3
TKMg8cFz9pPEevrerGEfoqCZaBP7f0T//aDsNBuloFv2UyGEu+szjCsmKKh/
p0wO4TtcPHTOsxTEPj+7QtjybemRDTo2VHBP23YK2+nFwaApPjb08v4Bo2ki
v4jOgYuCbOiy1Y+XhJlz3Q5/E2JD9F/Wwgi/W+mrMhVnQ89D/zYT9n8SuN53
jA2JSU+emsGOM6e706LBhq4/9+KZxTZ3/5YsfZUNnVbyliAsGX9jzVuHDVmG
6CsR/j6U5n/xFo6XoWdNOMfQtKLcgg3lPVxuIlyn3SiZ7cqGNruR3eewF5U+
L0XksqGASxzN89gO2RGvigvYULj72iBh0uFgjpFiNiSSWLlImFnAc/fhKjY0
lra6cwF7y5KtSvEfNmSAym4RPpKoFjT8jw3tijjYTzhd6KwQxywbKtXkmCUs
F6yYKLXAhtbWq8iL2KdcZArs1tiQv9DEHsKXDARH2Cns6OTREn3ClptnZaWE
2NHMyY1GwtPuk4VaIuxocyZzL2Fr+hE1u33saODXLI2wA63rVtFBdkQ96ci2
hO1eUeaqJc+OZLvVFQnHOoa0PtViR4vuO2IJ9w2csi10YUd/ulJvLxP5RNRE
ibuzoy7H4PuEP2pfr/Z+z45+9d21J6xXZbXnth87cm9J9SbcnvzxN/kTO3LY
IpFPuMF+XfBMLjva5Ja4ZQVb6ujb84kF7Oho7NbdhL0mNz/ZXMKOhlLMJAhf
MDhQNVDFjkahAQhXKutaO7exo6vq4caEC7mzKspm2dEjcZ0fhHdXnZk9uMiO
7Hlscgi/eF3PH7jCjt58fllC+OTC8CMTeg6k+uDmH8JZnVv5WakcaMfGkWXC
32OsH6pKcKDuPc/lV4nzSEFqZ7UBB9p58H0q4cILlFcrdzhQ849N2YR79ftG
95lwoKnz7wsICzn5ZTlbciBXvhs1hKNLl3SQPQd6s3FziPDni4WBP3w5kJUk
/9Y17NJbIaTeAA606NDDR3jQ6rEJNYQDuRgE7SEs4isiaxHJgRaUhw4Q/tL6
9o/INw4kMbV2mnCswZUtwWUcuD/3WRIuf7j/WXklB9rlgR4RHn7FODhfzYHy
b6nbEhb7kpF2pYkD7duh4kQ4bnSnFkcPB+pYrvQjHP+o3+fFMgdaEmTNJpz4
xprb+AAnMpwYXyc8Ln/T6c0hTiSsXE+/Tpx3s0ozn45woi3Ub0yE4w03Nf89
zon4D1/mJBwLPwKvK3OijFOwi3D00j+BC/qcSL+j9Djh/uRWL3NDTmQ6JqtA
WMgkn87diBN5qUYjwlEtnn1lZpxok7jxWcIRPyRjFG05kX9juCbhYAtzyaMf
OJFV4l8Lwu3CmhFXfDlRkNfEA8LbO09QHwZwoqsus48IB6qyTX8L40SOXf+e
EvYTjU8Ti+dEe+5FvyHs1TMkz1fEidJDSKGEXbRunyfPcaKG3S3F/8/nYqeu
0SIn6ii1KCOco6JlWbaC489vVBCeVjj7wZ2eC+0X2lRLWFdE4g8vlQvlTq62
Ej68MG0oLMGFBHraxgirT5k9dpbkQg16ApOETcb63wxLcSHqplv/CId1t8Qm
yHKh2+qNs4RZyrMmpZW5kBzVaY3wf4FOdmcM8PNyrJwb2Js8HoexmnChQXYS
D2GVl3fzqy25kDUMbiacek+V6aoDF3rV9nIX4bdyPN53A7jQlNyaBOGjnZFx
LtVc6OPGCWXC488lz7Y2cqHW97wqhKOFsvvF2rnQ29Xu8/+PZ9YkUDnIhbbu
1r5MeNmBfJ6JREXM4pU3CLfUVARJkqlop7q6HuHvgl7DV5mo6EJQ6S3CpsV8
zjHsVBSjHGpEuI1NpuDcdiqyZG61/H++N1e5rPioSPgt1YqwV1KhXpAAFcF1
9IjwuSsXV0f2UpGpqbMN4Z/BJsfcD1PRQl/Sc8KB+0ISq89T0U9zn/eEma6o
vvusQUVikerehK0dlkwddfD4dsN+hDVqr4kdMKYi1UPFoYTzF5mYGS2pSJPM
/JHwwT3pA52Pqei4seQnwuzWmz69c6IiaWvJOMJ24UUv77hRkV4LSwLhkbKH
t+S9qIjzWNU3wqU76neNh1ERdRtbKuHn+R6B5zOpSKPV/Rfh/TLX3cj5VNTx
NzmPcOtXEbvsUiq6JlVcQPhwQN4NiSYqWp/MKCH8zklbIaSHiiLNzSoID1pM
8bPRqKg8t/c3YaTzlmS/SkVRGbvrCAcrCfWOsnIjGJNoJKy+U+tzpTA3GjyS
1Eo4wUjI4YoUN+rLZuggTEme0uhU4EYmlTJdhE2W8/YZqXKjILuz3YTLlN6T
Jq9yo6gy2R7Cr9vEv23c50YbnsX9hBUty25wu3Gj0YKhof/PZ/skB4rmRvJW
g6OEH5RsyrfM40ZjTW8mCLNYnXgY1s6NZEvqaYQj+AyFf89xo9eRTVOEe2mv
b3qI8aDo9n+zhIWLYvwv3OBBr9X1FghfX0y9RfXgQXOjckv/r5cDheINuTyI
MTt5mbC9V99a9SwPWkgeWyF8d4ahvkKcF/V+7VolfKdhxXbfTV4UGv5ujbBh
yrSgmycv8jbbvP7/8RrYPYrLeJH/Uff/O3dGBMfB9zP6/+//AZaF+7k=
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0, 0},
  PlotRange->{{0, 5}, {0.0066928543161513575`, 0.9999999979388454}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{{3.573258512139554*^9, 3.573258520886292*^9}, 
   3.5732586530161467`*^9}]
}, Open  ]],

Cell[BoxData[""], "Input"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"K", "=", "1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"S", "=", "1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Scalef", "=", "10"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"H", "[", "x_", "]"}], ":=", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "-", "x"}], ")"}], "/", 
     RowBox[{"(", 
      RowBox[{"1", "+", "x"}], ")"}]}], ")"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"J", "[", "x_", "]"}], ":=", 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", "S"}], " ", 
     RowBox[{"H", "[", 
      RowBox[{"x", "^", "K"}], "]"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"J", "[", 
     RowBox[{"x", "/", "Scalef"}], "]"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "0", ",", "10"}], "}"}], ",", 
   RowBox[{"PlotRange", "\[Rule]", "Full"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"S", " ", 
     RowBox[{"H", "[", 
      RowBox[{"x", "/", "Scalef"}], "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "0", ",", "10"}], "}"}], ",", 
   RowBox[{"PlotRange", "\[Rule]", "Full"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"J", "[", "0", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"H", "[", "0", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"J", "[", "1", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"H", "[", "1", "]"}], "]"}]}], "Input",
 CellChangeTimes->{{3.573258756424573*^9, 3.5732592958187857`*^9}, {
  3.573259337216173*^9, 3.5732594099732037`*^9}, {3.5732609930126143`*^9, 
  3.573261072327327*^9}, {3.5732611045779953`*^9, 3.5732611520834007`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwVxX08lAccAHCGCJWrpSTuzmNdVKil9bL6/cbNxGlJKL3YyVslyboPoVYt
ypCXbhHJRCJDL6qpyLx05G2hWF667qXrzp0ncbgjt/bH9/OlBxzdEfSFlpaW
x2f/n3sqYNbqwKQta/eyXHySeqChi8bypR0EsmS02iC1BzTPan6l0U7Azm2p
jo8zeiC3NoWqR0uE3/U51rTsHgjbzDkyQb0C67Xua6TFPdA1Fcr9QC2BFJo0
/+SzHtDvEzAU1Crg648wC7V7If953rEhajMIdj1MlUf1QnlFYl+Y3WvY8aCj
0DH6XwiBF1fkkUKI6J+ifRXyGsLFNsLKcin89SJHKt7eB4b0c4w36z+Az6bB
jlqPfrho0mKb/9MozI4unBxePwABfTuf6rSOQ80/FY4t6wbhWOO5gKMMNdjQ
m/z5dm/gToSx1+Ktn+DSnwZMriEf8mMPyxx8tfBqw9eZDQl8OJs37Tkm18aC
GZ8ns9V80Ori45MYHWy7fhnYh97CsuPB0Ttt9XBsPEhxSvgWRMpMH6PqWTh/
E7fxqZcA3MSHTmYcM8CIqLvmGa0C2CZKW2FpaYilGeXvtzoJQVYfcLqpzAgP
hzyVYoUQ7CivLJz952COQXplvLUIDEHJ7h+di8275jouSRfBDwq/CStvEzyR
x2c164mhty5Q228lBR+I4tN3R4nh/oWG/TQlBe1DeXWBfDEE63H47e3z8YNe
U4mb5zs4EBl73zh7AYpcTNfVV70D+5AV7Sz2lxg+MGPJokqgY/5KI86GhXgm
Kkjyc7IEHpefzVmpY4pCXrYZe0QCsdq221sHTHG/sqq7et97MOyPcQ2vXISk
3KrFlPcexEV0bl7cYrSoVxwKWy4F7nIz3SpvMyzbznKaTJNCWnYwj01bguzd
47rXRqTgx93XbixZgpqN+gdjfpTBJPOGKq/GHG8yed/7VspAKHaKrju/FK2T
bxUnUIbg+ubV5m1sCzx9oNstiTMEZfEcSsgqS5SpX5tqdw7Bee8D7jpKS/zj
6hrdphVycOzPLznfRsVqYeSt1clyyLLJKWtzp6H/K5N5LmI5/FKbkuVeRUPd
piyXbqYCPMy9bzy3pmNXJj25JF8B34Xz9lAS6Rgrq2AETiggb2mAdauSjvb1
az1DvYbBuv7m5W17rDDZhsmKKB6GtFV1tp01VtjgfNd1Wj0MwaO6L5ptCWQv
X5hQ6EVCwJ2PtMRUAnPVoj0XbpAQdtZr2i2dwLIsjf1YEQkcr8oe40sEFrlS
x/yLSbig5FxMu0ygQ24E65tSEso3qKYycwnk7gjfLL5NgupvzauiUgIpA0fp
W56QkNZpnNLAI1DauHdc2ElCdsGR0IRmAh/Vhrd6dJNQcLzd2bWFwHsvczIe
viThgWmauqWdwHMUN/K3XhL6/RaEdr0kUGVjKHMYJIEhNHMWCAk0abwnPiEl
waEyxrJQTKBk1/NqgYyEjfF9qiAJgXaL9RPc5SSwGLm3pTICTw0q3y0lSYg8
TLMcGSGQ8fGxrGaMhLhvz6jujhK4aesiJmOchPg5gu7jSgJjfK+lpE6QkFVR
kDQ5SeBH/zUj/moSrp/WDXmkJjCuGo2apkgo9QxyipsmsCPr5EKHTyRUWj2z
2DJDYHGBdF7WDAk1o8tUGg2BLanxao2GhP8ARWpnrQ==
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0, 0.4},
  PlotRange->{{0, 10}, {0.36787945618692963`, 0.9999999897959183}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{{3.5732610216216097`*^9, 3.5732610728375473`*^9}, {
  3.573261130896433*^9, 3.573261152772773*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwVzHk41AkYwHG52mdQjppMMocfpibZ7qWV910da10l6dBuMZNhkRrMbocy
FLFWppImNYts9Xg8jmpqc8SEWNfYTFSOyBjHoFHMZqRs+8f3+fz3ZbCP7grW
1dHR8f7S/4rPsA3XHE51/QbIqrk5NdTK6V576T/DriO0ce2MGubqKs/S6Scg
aWK6dEqjBrE0jWZATwELk0MlY6NqiNjMP/KBdg0C4hpIgy/UIP8YmjFBywfs
7fD6XKyG+V39zHFaKRQZtlXWBKohtzGbN0prgI7gGF9R7VsoKk7pinDshJtp
irWU2HEIgWfXxqIUUAWTo1v9xyBSuUIhKRoBC1VbqtR1FEiMc8xepwmI3vpH
da2VCi6YNrFyAyfhAMk7nEIeAXbX7iq95n+BEyp59Uh/GHhPz7GPMmfA6fjC
ForuENw9Zuxn+cMnyJufHMWgDELuqXDV6r06GLZ8PN/UTgkJ2bO+U2PzsCjf
SNHpPgA68j6sOKmHK2vkPQ2/KsA+hnt8N8sAha/M32680Q8Dmqt7jB4bYviV
XNv29jfgoQw7fYn3FfJeShelGrwBnwHhSiqVhOtfd326BH2gqmEL/i40Qhf7
G2Wzlr3gaNZhveWQCXI4DFyc0wMk0AR1Ty5AyyZ5dIFHN3w/HvDBxt8UtU4h
O1IUnfCy+vC8AAcz9CLvdw1NewUPkmsP0jVmGHB6vNBx70vgGvD7ZDJz/L2z
qs/R9AVwok49MM6ywJQlm/Z5DbXD1yErZV5Bi9DzHvtEW9NzaDV3MOI7L0ZG
5utYXo4cyosSrjvokbGQ5TM/XNgGp+axdjb3kNF4+xkbT94zIHWfdI+ULEEm
4bJu8OA/oLzNyMiOtcSe1UbMpkYZZCyn6Jf6U9AsrK7GrrwZhFnc+iD6Uoz0
fp/Y2tkIARk/yYyHlqJljqH4w4IGmN56S5tdaYXlvK66YWY9KJRux6vPL8P9
tQrcFvEUbm5eY9USZI1cXrysJLMGChP5ZiGrqOhQPmwtUD2B8/4cTz0NFU3W
iO4b7JTChu7c/PMtNCR9m37xofNjEK24XtjiScf43zpZpbQyiJOmiTxL6biQ
Wn3iUsdD8Lbyv9Voy0ATpd0TUfF9+C6y/oBZCgPP2FRwKa0lkL2MbdusYeA+
cytm0NkCsK25k+lzwAYnfav+uqy+BcJV1ay2ShsUkKLcNmbkAHdS/1kDi0C3
mB+j3euuAPvue3pKOoHrF7jTqZVJEJHgN+txkUB36pQdk50EfD/JC+PLBN7J
CN/jZJgEyRr+BWEmgSx9zrpon0QoctZ+vCom0HmKVOXRexa0T+Y6bhcQuEMp
Ygl040HYZpxWW0/gYcXCdK34F8jKOxKa1EBgvzbb2nyYD3kxsi3uTQQG1Q1W
bFrLh4dk4UyTjMB7Q4vtKuqioTvAIlTeTqBo1enAuIljwFRQtvQrCMwXJPga
XeDCaslJ6p9KAimqsGQX22DYlNilDR4isK9VV5xWxgEvprhkREVg8Z1wn/Sh
QIgKp1PfvSPwsrPqET1pD8S6xGvvTRI4kbWDLJD4QaJJ//MYDYHrGgdGqYqd
ICrOS52eJjBdZ3vFhoTtcFOgH1I2Q2BzL/Fc3YdQ4BvsFjtLYKr23VpfwUaQ
2NRZu37+8t/t8TYuzh4qJ+21c3ME2h+UtktHtrn+B4XpQkA=
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0, 0},
  PlotRange->{{0, 10}, {1.020408173933553*^-8, 0.9999999591836745}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{{3.5732610216216097`*^9, 3.5732610728375473`*^9}, {
  3.573261130896433*^9, 3.573261152782152*^9}}],

Cell[BoxData["0.36787944117144233`"], "Output",
 CellChangeTimes->{{3.5732610216216097`*^9, 3.5732610728375473`*^9}, {
  3.573261130896433*^9, 3.5732611528091383`*^9}}],

Cell[BoxData["1.`"], "Output",
 CellChangeTimes->{{3.5732610216216097`*^9, 3.5732610728375473`*^9}, {
  3.573261130896433*^9, 3.573261152828484*^9}}],

Cell[BoxData["1.`"], "Output",
 CellChangeTimes->{{3.5732610216216097`*^9, 3.5732610728375473`*^9}, {
  3.573261130896433*^9, 3.573261152854456*^9}}],

Cell[BoxData["0.`"], "Output",
 CellChangeTimes->{{3.5732610216216097`*^9, 3.5732610728375473`*^9}, {
  3.573261130896433*^9, 3.5732611528725777`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.57325876835096*^9, 3.5732587687514544`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Plot", "::", "\<\"pllim\"\>"}], 
  RowBox[{
  ":", " "}], "\<\"\\!\\(\\*StyleBox[\\\"\\\\\\\"Range specification \\\\\\\"\
\\\", \\\"MT\\\"]\\)\[NoBreak]\\!\\(\\*StyleBox[\\!\\(PlotRange \[Rule] \
Full\\), \\\"MT\\\"]\\)\[NoBreak]\\!\\(\\*StyleBox[\\\"\\\\\\\" is not of the \
form {x, xmin, xmax}.\\\\\\\"\\\", \\\"MT\\\"]\\) \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/Plot/pllim\\\", ButtonNote -> \
\\\"Plot::pllim\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.5732587643314962`*^9}],

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   FractionBox[
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "+", "x"}], ")"}], " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "R1"}], "}"}]}], 
    RowBox[{"1", "-", "x"}]], ",", 
   RowBox[{"PlotRange", "\[Rule]", "Full"}]}], "]"}]], "Output",
 CellChangeTimes->{3.573258764337817*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"F", "[", "x_", "]"}], ":=", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "x"}], ")"}], "/", 
        RowBox[{"(", 
         RowBox[{"1", "+", "x"}], ")"}]}], ")"}], "*", "10"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"F", "[", "x_", "]"}], ":=", 
     RowBox[{
      RowBox[{"1", "/", "x"}], "-", "1"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"F", "[", "x", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0.1", ",", "1"}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", "Full"}]}], "]"}], "\[IndentingNewLine]", 
   RowBox[{"F", "[", "0", "]"}], "\[IndentingNewLine]", 
   RowBox[{"F", "[", "0.1", "]"}], "\[IndentingNewLine]", 
   RowBox[{"F", "[", "1", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.5741209021591053`*^9, 3.574121027601603*^9}, {
  3.574121295746118*^9, 3.574121295905089*^9}, {3.574121371091*^9, 
  3.574121371214101*^9}, {3.574151089961377*^9, 3.5741511959282417`*^9}, {
  3.574151246314424*^9, 3.5741512688536673`*^9}, {3.574151574553594*^9, 
  3.5741515976827183`*^9}, {3.5741516330290613`*^9, 3.5741516487039633`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV0Xs4E/ofwPHVLyG3hSkhW4iGRI5Lqc/XckqUkERPK512ohQSHTEOTaiQ
pMylUimRS3WcOgxtszMznLRybbnb5jKT4siR/dYfn+fzvJ7383z++RB+Cff9
dTkGg4lTzI9t/ooqycurgfDsY2y5nIBcXrwYev6sBtJ6mBzxZwLyLRcJm+pr
IDrT+s7wMAFRH3q/mx2sgTO5/8Po8QhIkGla60OshT+evI0/cZOAEoJbr6sy
amFLwM+LEeYE1LHW0OaSkAEJHXeLooPwaFzHZ1PWFAOIKhpdmX54hNFKMSnB
1EEKmj332h2PiErTazpM62By953dDrZ4lPi5EWN7rg4YEyejxcvxyIofKpB8
rwOHfdLsJR9jRItjRAcYNwA2OdP27pIRchg+OP/VrgGuHcmvuioxQhKPydib
uxvgiJ5nU7bACHmtM0rkn2sAIfu06NsTI7SuJuGaS10DFLsR5nA+Ruj5nOt9
fOAboN54VH64zBBNOVEoUXuZEM7cyqsIN0C8s88fkw4yoeLo/oWWowboQdGi
CEtmQprjvTaMhwHyU7l9uiKCCT0x0j8rTA1QTdffYaJcJsR3h9hjP65DyRfN
LwWImLC+r9q703MdWlc9nuFCYwF52EAS6qKP9DxToinpLFDOPzRGtNZHq4cI
5PTbLDCREtq+GekjFWyglbCEBSr+9B6ufC2aC23ix7WyIMnbjGzAWYsEJsXK
DBwbeKUFp/MOrEXXb5GTnJ+ywecxZWr/+TVoMbI9yqGtEYqqPnmRPuKQRd/K
X7Z0NsJL2hWVG29x6NDeHQeI/Y3gr2rlJ2rEoUrjsk3rPzdCjuBoSHU5DgW1
0vqUcBzYgndaaE3AIY6Zo3vHUQ6kEfM5w6Y4lN591yBqkgOqpOC2fTG6yGhn
KPuFGhfEG25gLznrIPrx9xfCcFwIdtQq2G6jg3STtptZGnMhOX/3JmUzHbSK
o5ZWbMeFQ7JgQg1WB83tLd+fG8iFVdMCp1SJNmo/JO2mlnAhvWyHKjtPG10+
Fy7bs6sJvmoXkU4t10aSwguGfbE8uCZa5kEax6IrBMuorzQeeDq+picNYdGG
J0MtqzJ4kEd9UsztxSJylU+c430epA419Z5owSIBy6Y3q5EHT49vxQrLsahe
NJG7S70ZeLTEnb0RWJS9haJTercZTlWbv3/6TQvt4PitimLy4dVZ0zT8Hk1k
Id+xcT+fDznjh4M0nTWR7jZz0sYPfPgnXz1jpaUmmqz6dqlbzIewzfmvcFqa
qLDg3piLVgvM4PdgK7s0FP+R8JSOtYDpGjGp/owGqiNQU3P/awHSdOq8Gl0d
ufz+aAXDoQ14IcJ2Tw01FLu75Ffag7cQxa7ouXNcBREmXCuOfG6HxMk9Mxud
VqLeRZH1PqIANAoOZFy3WYG6I2NiIr3eg83JGTuVK8vRbBjlYs/ZD8AOFXhZ
rV6GxPapC/RHHZDAvfeS4iuH9cfvuGf83QmXrwR0dv67CMSDQsNeeReE6cmV
psoWwL+NWhxs3wMutreH6UXz0PRMdW6TXy+QNkQoZ43MgYZfW5tb+kewuimd
mLScBXpQfHhOlRCEZMuSiJdfYN6Bkusy8Qm8+ZbuAucZ+O3W40gzQj+EWdjP
4EanYX3Z2HUCYQCImz2Vr1FlECoreCCsH4DYKAHBjiiF3lNuhnbeg1CplqTr
bDYBgYT86ZL+QShbCHZP9BgDNlJT8YgbAvXoiHmNEDF4ZUcmp6gPA11qcvgu
axQ47rO7ZKXDoJdR4O+pMwIeJDNyjNsIZH7ayNqWMgQz5zw/FnwYgacXXfD3
LQbhylJ64UTEKLgbBVyKS+6HbSwOu2O5CLLG9YLc1T6BnFtKGrgpghUhsZX0
+l6osi1TWmMthqMeea9GyN2Q9E+RtaReDFRvq5EhUifsbH9JHz0iAadTXps/
6n0AoxWMkHipBOrs+dmBFgJQj4vI/il+DIRvU0IMAtth1N9Vrq8/Dkz8QqXv
TCsMUlTe0cvGAc+aPYPJb4amrvHRXe4T4L7aqc97AxfqM2L9tIYnAPtmTP5l
gA0d0RiL6AuTwAzTbl2aegPd7Ac7lbFSCPq0/khLfw18d3Wu3fpQCuj1YBLl
fDXYrUi4kWg7BczC38oaHlaCr2VfybfWKUBTNt3lskeQk2xvpnFMBpglaWn/
kxz4imKmc4IUdkqjFvnmwMHvDIbhSYWbi0fClm6B9sVdvlYhMkAsUoi9/y24
Eez7u2ekDJj/kgdPrMyGax4RPVdTZJD4Ey8pVZAJSdjyjJVVCnsN2OtHJsNA
qywg84Xi3pxu6ZdWGqCrW031qhXdzjRWYk6DpWWMWrMaRY++o0/oSwLql2ax
W6PCtdbbC3ITIKZT7ErrUtjqv3jDgCjozrbUUOtV+NkX4zWrL4DjgfDubOGP
fvOPn/nnYa5pLuzhoMIC2iI1Ngwu1CgVsiYUTlTN2uZJAUH03uC9UwpHWmw/
yzwBdnYZdu+mFabKVFadPQbTZbrNA7MKZ5GNu9z8wDs4ICdkXuEtry6PULyg
yqTw+PSCwhjLv/50dQPNgX5izHeFpzf9R0L2EFZoMieX/+gYq6a/Du78P+Of
qzM=
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0.2, 0},
  PlotRange->{{0.1, 1}, {1.8367347420777946`*^-8, 8.999998163265643}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{{3.574120925330243*^9, 3.5741210281253843`*^9}, 
   3.574121296419856*^9, 3.574121371747101*^9, {3.5741510969284554`*^9, 
   3.5741511967798567`*^9}, {3.5741512522448177`*^9, 
   3.5741512697656813`*^9}, {3.574151579434987*^9, 3.574151598649274*^9}, {
   3.574151635275359*^9, 3.574151650095477*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Power", "::", "\<\"infy\"\>"}], 
  RowBox[{
  ":", " "}], "\<\"\\!\\(\\*StyleBox[\\\"\\\\\\\"Infinite expression \\\\\\\"\
\\\", \\\"MT\\\"]\\)\[NoBreak]\\!\\(\\*StyleBox[\\!\\(1\\/0\\), \
\\\"MT\\\"]\\)\[NoBreak]\\!\\(\\*StyleBox[\\\"\\\\\\\" encountered.\\\\\\\"\\\
\", \\\"MT\\\"]\\) \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\
\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/infy\\\", ButtonNote -> \
\\\"Power::infy\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.574151579441164*^9, 3.574151598655922*^9}, {
  3.57415163528622*^9, 3.5741516500999937`*^9}}],

Cell[BoxData["ComplexInfinity"], "Output",
 CellChangeTimes->{{3.574120925330243*^9, 3.5741210281253843`*^9}, 
   3.574121296419856*^9, 3.574121371747101*^9, {3.5741510969284554`*^9, 
   3.5741511967798567`*^9}, {3.5741512522448177`*^9, 
   3.5741512697656813`*^9}, {3.574151579434987*^9, 3.574151598649274*^9}, {
   3.574151635275359*^9, 3.574151650100819*^9}}],

Cell[BoxData["9.`"], "Output",
 CellChangeTimes->{{3.574120925330243*^9, 3.5741210281253843`*^9}, 
   3.574121296419856*^9, 3.574121371747101*^9, {3.5741510969284554`*^9, 
   3.5741511967798567`*^9}, {3.5741512522448177`*^9, 
   3.5741512697656813`*^9}, {3.574151579434987*^9, 3.574151598649274*^9}, {
   3.574151635275359*^9, 3.5741516501019077`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.574120925330243*^9, 3.5741210281253843`*^9}, 
   3.574121296419856*^9, 3.574121371747101*^9, {3.5741510969284554`*^9, 
   3.5741511967798567`*^9}, {3.5741512522448177`*^9, 
   3.5741512697656813`*^9}, {3.574151579434987*^9, 3.574151598649274*^9}, {
   3.574151635275359*^9, 3.57415165010315*^9}}]
}, Open  ]],

Cell[BoxData["*"], "Input",
 CellChangeTimes->{3.574151181572242*^9}],

Cell[CellGroupData[{

Cell[BoxData[{"0", "\[IndentingNewLine]", 
 RowBox[{"N", "[", 
  RowBox[{"Exp", "[", 
   RowBox[{"-", "3"}], "]"}], "]"}]}], "Input",
 CellChangeTimes->{{3.5741212339266243`*^9, 3.574121265799911*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.574121252492543*^9, 3.574121266291223*^9}}],

Cell[BoxData["0.049787068367863944`"], "Output",
 CellChangeTimes->{{3.574121252492543*^9, 3.574121266293008*^9}}]
}, Open  ]]
},
WindowSize->{640, 750},
WindowMargins->{{Automatic, 355}, {230, Automatic}},
FrontEndVersion->"7.0 for Linux x86 (64-bit) (April 23, 2009)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[567, 22, 1019, 29, 143, "Input"],
Cell[1589, 53, 141, 2, 31, "Output"],
Cell[1733, 57, 138, 2, 31, "Output"],
Cell[1874, 61, 11923, 201, 248, "Output"]
}, Open  ]],
Cell[13812, 265, 26, 0, 32, "Input"],
Cell[CellGroupData[{
Cell[13863, 269, 1832, 51, 253, "Input"],
Cell[15698, 322, 2177, 41, 236, "Output"],
Cell[17878, 365, 2180, 41, 246, "Output"],
Cell[20061, 408, 168, 2, 31, "Output"],
Cell[20232, 412, 149, 2, 31, "Output"],
Cell[20384, 416, 149, 2, 31, "Output"],
Cell[20536, 420, 151, 2, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20724, 427, 93, 1, 32, "Input"],
Cell[20820, 430, 618, 11, 23, "Message"],
Cell[21441, 443, 353, 11, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21831, 459, 1284, 32, 165, "Input"],
Cell[23118, 493, 3317, 60, 250, "Output"],
Cell[26438, 555, 652, 12, 41, "Message"],
Cell[27093, 569, 362, 5, 31, "Output"],
Cell[27458, 576, 352, 5, 31, "Output"],
Cell[27813, 583, 347, 5, 31, "Output"]
}, Open  ]],
Cell[28175, 591, 69, 1, 32, "Input"],
Cell[CellGroupData[{
Cell[28269, 596, 201, 4, 55, "Input"],
Cell[28473, 602, 94, 1, 31, "Output"],
Cell[28570, 605, 114, 1, 31, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
