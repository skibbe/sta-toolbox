function samples=paper_extract_img_samples(A,img,varargin)



dim=3;
zp=0;
b=48;
bif=false;
sample=false;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

A.data(15,:)=A.data(21,:);
      

if sample

        valid=unique(randi(size(A.data,2),1,50));

        coll=coll(valid);

        shape=size(img)';



        count=1;
    %     for s=1:numel(coll)
    %         pos=ceil(A.data(1:3,coll(s)));
    %         %pos=pos([3,2,1]);
    %         if (~any(pos+[-b,-b,-zp]'<1)) && (~any(pos+[b,b,zp]'>shape))
    % 
    % 
    %             selection=[pos(1)-b,pos(1)+b,pos(2)-b,pos(2)+b];
    %             selection=max(selection,1);
    %             selection=min(selection,[shape(1),shape(1),shape(2),shape(2)]);
    %             sample_list{count}=max(img(selection(1):selection(2),selection(3):selection(4),pos(3)-zp:pos(3)+zp),[],3);
    %             %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
    %             count=count+1;
    %         end;
    %     end;
        for s=1:numel(coll)
            pos=ceil(A.data(1:3,coll(s)));
            %pos=pos([3,2,1]);
            switch dim
                case 1
                if (~any(pos+[-zp,-b,-b]'<1)) && (~any(pos+[zb,b,b]'>shape))
                    selection=[pos(2)-b,pos(2)+b,pos(3)-b,pos(3)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(2),shape(2),shape(3),shape(3)]);
                    sample_list{count}=max(img(pos(1)-zp:pos(1)+zp,selection(1):selection(2),selection(3):selection(4)),[],1);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
                case 2
                if (~any(pos+[-b,-zp,-b]'<1)) && (~any(pos+[b,zp,b]'>shape))
                    selection=[pos(1)-b,pos(1)+b,pos(3)-b,pos(3)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(1),shape(1),shape(3),shape(3)]);
                    sample_list{count}=max(img(selection(1):selection(2),pos(2)-zp:pos(2)+zp,selection(3):selection(4)),[],2);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
                case 3
                if (~any(pos+[-b,-b,-zp]'<1)) && (~any(pos+[b,b,zp]'>shape))
                    selection=[pos(1)-b,pos(1)+b,pos(2)-b,pos(2)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(1),shape(1),shape(2),shape(2)]);
                    sample_list{count}=max(img(selection(1):selection(2),selection(3):selection(4),pos(3)-zp:pos(3)+zp),[],3);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
            end;
        end;
    
else
    if ~bif

        G=A;


        npt_attrib=size(G.data,1);
        if npt_attrib<26
            missing_attrib=26-npt_attrib;
            G.data=(cat(1,G.data,zeros([missing_attrib,size(G.data,2)])));
        end;

        C=G;

        C.data(17,:)=0;
        C.data(19,:)=0;
        C.data(20,:)=0;
        C.data(22,:)=0;
        C.connections=zeros([6,0]);

        offset=(min(C.data(1:3,:),[],2));

        if ~any(offset<0)
            offset(:)=0;
        end;

        C.data(1:3,:)=C.data(1:3,:)-repmat(offset,1,size(C.data,2))+1;



        if ~isfield(C,'cshape')
            C.cshape=single(max(A.data(1:3,:),[],2)).'+1;
            %C.cshape=C.cshape([3,2,1]);
            %C.cshape=C.cshape
        end;
        if ~isfield(C,'scales')
            C.scales=single([min(A.data(8,:)),max(A.data(8,:))]);
        end;
        C.data=double(C.data);


        C.cshape=C.cshape+10;    
        C.cshape=max(ceil(single(max(C.data(1:3,:),[],2)).'),C.cshape);




        options={'opt_particles_per_voxel',5};    
        [D,all_pts]=ntrack_graph_tools(C,{'tool',4,'params',options});
        all_pts=all_pts+1;
        coll=all_pts(all_pts>0);


        dirsA=A.data(6,all_pts>0);
        dirsB=A.data(6,coll);

        valid=(abs(dirsA)<0.25) & (abs(dirsB)<0.25);

        coll=coll(valid);

        shape=size(img)';



        count=1;
    %     for s=1:numel(coll)
    %         pos=ceil(A.data(1:3,coll(s)));
    %         %pos=pos([3,2,1]);
    %         if (~any(pos+[-b,-b,-zp]'<1)) && (~any(pos+[b,b,zp]'>shape))
    % 
    % 
    %             selection=[pos(1)-b,pos(1)+b,pos(2)-b,pos(2)+b];
    %             selection=max(selection,1);
    %             selection=min(selection,[shape(1),shape(1),shape(2),shape(2)]);
    %             sample_list{count}=max(img(selection(1):selection(2),selection(3):selection(4),pos(3)-zp:pos(3)+zp),[],3);
    %             %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
    %             count=count+1;
    %         end;
    %     end;
        for s=1:numel(coll)
            pos=ceil(A.data(1:3,coll(s)));
            %pos=pos([3,2,1]);
            switch dim
                case 1
                if (~any(pos+[-zp,-b,-b]'<1)) && (~any(pos+[zb,b,b]'>shape))
                    selection=[pos(2)-b,pos(2)+b,pos(3)-b,pos(3)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(2),shape(2),shape(3),shape(3)]);
                    sample_list{count}=max(img(pos(1)-zp:pos(1)+zp,selection(1):selection(2),selection(3):selection(4)),[],1);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
                case 2
                if (~any(pos+[-b,-zp,-b]'<1)) && (~any(pos+[b,zp,b]'>shape))
                    selection=[pos(1)-b,pos(1)+b,pos(3)-b,pos(3)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(1),shape(1),shape(3),shape(3)]);
                    sample_list{count}=max(img(selection(1):selection(2),pos(2)-zp:pos(2)+zp,selection(3):selection(4)),[],2);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
                case 3
                if (~any(pos+[-b,-b,-zp]'<1)) && (~any(pos+[b,b,zp]'>shape))
                    selection=[pos(1)-b,pos(1)+b,pos(2)-b,pos(2)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(1),shape(1),shape(2),shape(2)]);
                    sample_list{count}=max(img(selection(1):selection(2),selection(3):selection(4),pos(3)-zp:pos(3)+zp),[],3);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
            end;
        end;
    else


        connection_listGT={};
        for d=1:size(A.data,2)
            connection_listGT{d}=[];
        end;
        for d=1:size(A.connections,2)
            pointIDA=A.connections(1,d)+1;
            pointIDB=A.connections(2,d)+1;
            connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
            connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
        end;          


        % for d=1:size(A.data,2)
        %     assert(numel(connection_listGT{d})<4);
        % end;

        terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
        terminalsTERMPos=A.data(1:3,terminalsTERM);

        terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
        terminalsBIF=find(terminalsBIFa);

        terminalsBIFPos=A.data(1:3,terminalsBIF);

        terminalsDirs=A.data(4:6,terminalsBIF);

        nice_bif=find((abs(terminalsDirs(dim,:))<0.25));

        %nice_bif=[1:numel(terminalsBIF)];

        shape=size(img)';



        count=1;
        for s=1:numel(nice_bif)
            pos=ceil(terminalsBIFPos(:,nice_bif(s)));
            %pos=pos([3,2,1]);
            switch dim
                case 1
                if (~any(pos+[-zp,-b,-b]'<1)) && (~any(pos+[zb,b,b]'>shape))
                    selection=[pos(2)-b,pos(2)+b,pos(3)-b,pos(3)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(2),shape(2),shape(3),shape(3)]);
                    sample_list{count}=max(img(pos(1)-zp:pos(1)+zp,selection(1):selection(2),selection(3):selection(4)),[],1);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
                case 2
                if (~any(pos+[-b,-zp,-b]'<1)) && (~any(pos+[b,zp,b]'>shape))
                    selection=[pos(1)-b,pos(1)+b,pos(3)-b,pos(3)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(1),shape(1),shape(3),shape(3)]);
                    sample_list{count}=max(img(selection(1):selection(2),pos(2)-zp:pos(2)+zp,selection(3):selection(4)),[],2);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
                case 3
                if (~any(pos+[-b,-b,-zp]'<1)) && (~any(pos+[b,b,zp]'>shape))
                    selection=[pos(1)-b,pos(1)+b,pos(2)-b,pos(2)+b];
                    selection=max(selection,1);
                    selection=min(selection,[shape(1),shape(1),shape(2),shape(2)]);
                    sample_list{count}=max(img(selection(1):selection(2),selection(3):selection(4),pos(3)-zp:pos(3)+zp),[],3);
                    %sample_list{count}=(img(selection(1):selection(2),selection(3):selection(4),pos(3):pos(3)));
                    count=count+1;
                end;
            end;
        end;
    end;
end;

if exist('sample_list','var')
    samples=zeros([2*b+1,2*b+1,numel(sample_list)]);
    for a=1:numel(sample_list)
            samples(:,:,a)=squeeze(sample_list{a});
    end;
else
    samples=zeros([2*b+1,2*b+1,1]);
end;





