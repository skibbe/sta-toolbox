function ImgRGB=mhs_brainbowrise(A,shape,varargin)


consider_scale=true;
sigma=0.5;
min_particles=10;
element_size=[1,1,1];

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

element_size=element_size./min(element_size(:));


shape=ceil(shape.*element_size);

A.data(1:3,:)=A.data(1:3,:).*repmat(element_size.',1,size(A.data,2));

[paths,A]=extract_paths(A,'spacing',3,'smoothing',5,'smoothing_scale',5,'nice_bifucations',true);


for c=1:3
    ImgRGB{c}=zeros(shape,'single');
end;

path_ids=unique(A.data(21,:));

path_length=path_ids;
count=1;
for p=path_ids
    path_length(count)=sum(A.data(21,:)==p);
    count=count+1;
end;





valid=(path_length>min_particles);
path_ids=path_ids(valid);
path_length=path_length(valid);

path_length=path_length./max(path_length(:));

count=1;
for p=path_ids
    fprintf('%d %d\n',count,numel(path_ids));
    D=mhs_distmap(A,shape,5,consider_scale,p);
    Img_=exp(-D.^2./(2*sigma^2));
    
    rgb=hsv2rgb(rand,0.7+0.3*path_length(count),0.5+0.5*path_length(count));
    
    for c=1:3
        ImgRGB{c}=max(ImgRGB{c},Img_.*rgb(c));
    end;
    
   count=count+1;
end;