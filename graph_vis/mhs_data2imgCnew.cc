#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>

#ifdef __CLANG__
  //#include   <libiomp/omp.h>
//   #include <omp.h>
#else  
  #include "omp.h"
#endif  
#include <sstream>
#include <cstddef>
#include <vector>
#include "EigDecomp3x3.h"


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"

#define EPSILON 0.00000000000001


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
    std::vector<int> shape;
    shape.resize(3);
    shape[0]=256;
    shape[1]=256;
    shape[2]=256;
    int mode=1;
    
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

         if (mhs::mex_hasParam(params,"shape")!=-1)
             shape=mhs::mex_getParam<int>(params,"shape",3);
	 
	 if (mhs::mex_hasParam(params,"mode")!=-1)
             mode=mhs::mex_getParam<int>(params,"mode")[0];
	 
    }
    
    std::swap(shape[0],shape[2]);
    printf("generating image of size [%d %d %d]\n",shape[0],shape[1],shape[2]);
    mhs::dataArray<T> data;

    printf("mode :%d\n",mode);
    
    data=mhs::dataArray<T>(prhs[0]); 
    
    printf("found %d segments\n",data.dim[0]);
    
    std::size_t ndims[3];
    ndims[0]=shape[2];
    ndims[1]=shape[1];
    ndims[2]=shape[0];
    
    plhs[0] = mxCreateNumericArray(3,ndims,mxGetClassID(prhs[0]),mxREAL);
    T *img = (T*) mxGetData(plhs[0]); 
   std::size_t num_v=ndims[0]*ndims[1]*ndims[2];
    
    
    for (std::size_t i=0;i<data.dim[0];i++)
    {
      if (i%50==0)
      {
	printf("*");
	if (i%1000==0)
	{
	  printf("[%d/%d]\n",i,data.dim[0]);  
	}
	mhs::mex_dumpStringNOW();
      }
      
//       printf("[%d/%d]\n",i,data.dim[0]);  
//       mhs::mex_dumpStringNOW();
      
      T * cdata=data.data+i*data.dim[1];
      
      Vector<int,3> bbl;
      Vector<int,3> bbu;
      
      Vector<T,3> A;
      A=cdata;
      Vector<T,3> Ad;
      Ad=cdata+3;
      T As;
      As=*(cdata+6);

      Vector<T,3> B;
      B=cdata+7;
      Vector<T,3> Bd;
      Bd=cdata+10;
      T Bs;
      Bs=*(cdata+13);
      
      T weight=*(cdata+14);
      
      
      
      
//       A.print();
//       B.print();
//       printf("[ %f %f ]\n",As,Bs);
//       mhs::mex_dumpStringNOW();

      
      const int boffset=1+(mode==2);
      for (int d=0;d<3;d++)
      {
	T low=std::min(A[d]-As,B[d]-Bs)-boffset;
	T up=std::max(A[d]+As,B[d]+Bs)+boffset;
	bbl[d]=std::max(std::floor(low),T(0));
	//bbu[d]=std::min(std::ceil(up),T(shape[d]));
	bbu[d]=std::min(std::ceil(up),T(shape[2-d]));
      }

      Vector<T,3> ABDir=B-A;
      T segment_length2=ABDir.norm2();
      
      ABDir.normalize();
      if (ABDir.dot(Ad)<0)
	Ad*=-1;
      if (ABDir.dot(Bd)>0)
	Bd*=-1;
      
      for (int x=bbl[0];x<bbu[0];x++)
	for (int y=bbl[1];y<bbu[1];y++)
	  for (int z=bbl[2];z<bbu[2];z++)
	  {
	    Vector<T,3> pos(x,y,z);
	    
	    const T anglet=-0.001; 
	    if (!(Ad.dot(pos-A)>anglet))
	      continue;
	    if (!(Bd.dot(pos-B)>anglet))
	      continue;
	    
	    
	    T  l=(ABDir.dot(pos-A));
	    Vector<T,3>  distv=(pos-A)-ABDir*l;
	    
	    T segment_length=std::sqrt(segment_length2);
	    T w=l/segment_length+std::numeric_limits<T>::epsilon();
	    
// 	    if ((w<0)||(w>1))
// 	      continue;
	    //if ((l<-As)||(l>segment_length+Bs))
	    
	    if ((mode!=2)&&((l<-Bs/2)||(l>segment_length+As/2)))
	    {
	      continue;
	    }else
	    {
	      if (((l<-Bs/2-1)||(l>segment_length+As/2+1)))
	      continue;
	    }
	    
	    
	    if (w<0)
	      w=0;
	    if (w>1)
	      w=1;
 	    T s=w*Bs+(1-w)*As;
	    //T s=w*As+(1-w)*Bs;
	    //T s=(As+Bs)/2;
	    //T s=std::min(As,Bs);
	    
	    
	    
	    switch (mode)
	    {
	      case 0:
	      {
		T dist2=distv.norm2();
		if (!(dist2-0.5<s*s))
		  continue;
	      
		std::size_t index=(z*shape[1]+y)*shape[2]+x;
		//img[index]=std::exp(-dist2/(s*s/2));
		if (index<num_v)
		  img[index]=std::max(img[index],weight*std::exp(-dist2/(s*s/2)));
	      }break;
	      case 1:
	      {
		if (!(distv.norm2()-0.5<s*s))
		  continue;
	      
		
		std::size_t index=(z*shape[1]+y)*shape[2]+x;
		//int index=(z*shape[1]+y)*shape[2]+x;
		if (index<num_v)
		  img[index]=weight;
	      }break;
	      case 2:
	      {
		if (!(distv.norm2()-0.5<(1+s)*(1+s)))
		  continue;
		if ((distv.norm2()-0.5<(s-0.5)*(s-0.5)))
		  continue;
	      
		
		std::size_t index=(z*shape[1]+y)*shape[2]+x;
		//int index=(z*shape[1]+y)*shape[2]+x;
		if (index<num_v)
		  img[index]=weight;
	      }break;
	    }
	  }
      
      
      if (i==3)
      {
// 	A.print();
// 	printf("%f\n",As);
// 	B.print();
// 	printf("%f\n",Bs);
// 	printf("%d %d %d\n",bbl[0],bbl[1],bbl[2]);
// 	printf("%d %d %d\n",bbu[0],bbu[1],bbu[2]);
// 	A.print();
// 	Ad.print();
// 	printf("%f\n",As);
// 	B.print();
// 	Bd.print();
// 	printf("%f\n",Bs);
      }

      
      //bbl[0]=
      
      
//       if ((i%50==0)||(i==data.dim[0]-1))
// 	printf("processed segments %i/%i\n",i+1,data.dim[0]);
    }
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}