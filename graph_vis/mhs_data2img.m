function img=mhs_data2img(A,varargin)
% mex CXXFLAGS="-std=c++11 -fPIC -march=native -DSCALENORMAL"  CFLAGS="-DSCALENORMAL -std=c++11 -fPIC -march=native"   mhs_data2imgC.cc -l gsl -l gslcblas


nice=false;
supernice=false;
select_tree=-1;
select_tree_ids={-1,0};
offset=[0,0,0];
shape=[256,256,256];
rescale=1;
smooth=false;
scalefunc=@(x)(x);
min_pathlength=0;
mode=0;
lengthweight=false;





for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


A.data(1,:)=A.data(1,:)+offset(1)-1;
A.data(2,:)=A.data(2,:)+offset(2)-1;
A.data(3,:)=A.data(3,:)+offset(3)-1;

if 1
    if nice
        D2=ntrack_graph_tools(D2,{'tool',11,'iter',5,'weight',0.001});
        D2=ntrack_graph_tools(D2,{'tool',12,'dist',1});
    end;
else
    if supernice
        [paths,A]=extract_paths(A,'spacing',1,'smoothing',1,'smoothing_scale',1,'nice_bifucations',false);
        [paths,A]=extract_paths(A,'spacing',1,'smoothing',1,'smoothing_scale',0,'nice_bifucations',false);
    end;

    if nice
        [paths,A]=extract_paths(A,'spacing',1,'smoothing',0,'smoothing_scale',0,'nice_bifucations',false);
    end;
end;





if min(select_tree)>0
    unum=unique([A.data(21,:),1]);
    
    for a=1:numel(unum)
        tmembers(a)=sum([A.data(21,:),1]==unum(a));
    end;
    [v,indx]=sort(tmembers,'descend');
    select_tree=unum(indx(select_tree));
    fprintf('rendering tree %d\n',select_tree);
elseif min(select_tree_ids{1})>0
    select_tree=select_tree_ids{1};
    fprintf('rendering tree %d\n',select_tree);
 elseif (min_pathlength>0)

        unum=unique([A.data(21,:),1]);

        for a=1:numel(unum)
            tmembers(a)=sum([A.data(21,:),1]==unum(a));
        end;

        select_tree=unum(tmembers>=min_pathlength);
        fprintf('printing tree %d\n',select_tree);
end;


connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          

terminalsTERM=(1==cellfun(@(x)numel(x),connection_listGT));
%terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
%terminalsTERMPos=A.data(1:3,terminalsTERM);

terminalsBIF=(cellfun(@(x)numel(x),connection_listGT)>2);
fprintf('%d\n',sum(terminalsBIF));
% if select_tree==-1
%     terminalsBIF=find(cellfun(@(x)numel(x),connection_listGT)>2);
% else
%     tselections=max((repmat(A.data(21,:),size(select_tree,2),1)==repmat(select_tree',1,size(A.data(21,:),2))),[],1)==1;
%     terminalsBIF=find((cellfun(@(x)numel(x),connection_listGT)>2)&(tselections));
% end;

%terminalsBIFPos=A.data(1:3,terminalsBIF);

if lengthweight
    path_ids=unique(A.data(21,:));path_id_v=[min(path_ids):max(path_ids)];
    PMAP=repmat(A.data(21,:),numel(path_id_v),1);
    PID_MAP=repmat([min(path_ids):max(path_ids)]',1,size(A.data(:,:),2));
    PMAT=(PMAP==PID_MAP).*(PID_MAP>-1);

    weight=sum(PMAT,2);
    %weight=weight/max(weight);
    PID_W=repmat(weight,1,size(A.data(:,:),2)).*PMAT;
    weight=max(PID_W,[],1);
end;


%Data=[];
%Data=zeros(14,size(A.connections,2));
Data=zeros(15,size(A.connections,2));

for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        
                       %if terminalsBIF(pointIDA)||terminalsBIF(pointIDB)
                       %        continue;
                       %end;
                                
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        
                         N2=([Ax,Ay,Az]-[Bx,By,Bz])';
                         N2=N2./norm(N2);
                         
%                          if terminalsBIF(pointIDA)%||(terminalsTERM(pointIDA))
% %                              Adx=N2(1);
% %                              Ady=N2(2);
% %                              Adz=N2(3);
%                              Adx=Bdx;
%                              Ady=Bdy;
%                              Adz=Bdz;
%                          end;
%                          if terminalsBIF(pointIDB)%||(terminalsTERM(pointIDB))
% %                              Bdx=N2(1);
% %                              Bdy=N2(2);
% %                              Bdz=N2(3);
%                              Bdx=Adx;
%                              Bdy=Ady;
%                              Bdz=Adz;
%                          end;
                        
                        As=scalefunc(A.data(8,pointIDA))*rescale;
                        Bs=scalefunc(A.data(8,pointIDB))*rescale;
                        
                        if lengthweight
                            w=0.5*(weight(pointIDA)+weight(pointIDB));
                        else
                            w=1;
                        end;
                       
                        
                         data=[Ax,Ay,Az,Adx,Ady,Adz,As,...
                                  Bx,By,Bz,Bdx,Bdy,Bdz,Bs,w]';
                        %Data=[Data,data];
                        Data(:,d)=data;
                        
%                         if terminalsBIF(pointIDA)||terminalsBIF(pointIDB)
% %                             l=(norm([Ax,Ay,Az]-[Bx,By,Bz]));
% %                             steps=ceil(l/2);
% %                             for b=1:steps
% %                                 pos=[(b-1)*(l/steps),(b)*(l/steps)];
% %                                 w=1-pos/l;
% %                                 
% %                                 A2x=Ax*w(1)+(1-w(1))*Bx;
% %                                 A2y=Ay*w(1)+(1-w(1))*By;
% %                                 A2z=Az*w(1)+(1-w(1))*Bz;
% % 
% %                                 Ad2x=Adx*w(1)+(1-w(1))*Bdx;
% %                                 Ad2y=Ady*w(1)+(1-w(1))*Bdy;
% %                                 Ad2z=Adz*w(1)+(1-w(1))*Bdz;
% % 
% %                                 B2x=Ax*w(2)+(1-w(2))*Bx;
% %                                 B2y=Ay*w(2)+(1-w(2))*By;
% %                                 B2z=Az*w(2)+(1-w(2))*Bz;
% % 
% %                                 Bd2x=Adx*w(2)+(1-w(2))*Bdx;
% %                                 Bd2y=Ady*w(2)+(1-w(2))*Bdy;
% %                                 Bd2z=Adz*w(2)+(1-w(2))*Bdz;
% % 
% %                                 A2s=As*w(1)+Bs*(1-w(1));
% %                                 B2s=As*w(2)+Bs*(1-w(2));
% %                                 
% %                                 N2=[Ad2x,Ad2y,Ad2z];
% %                                 Ad2x=Ad2x./norm(N2);
% %                                 Ad2y=Ad2y./norm(N2);
% %                                 Ad2z=Ad2z./norm(N2);                                
% %                                 
% %                                 N2=[Bd2x,Bd2y,Bd2z];
% %                                 Bd2x=Bd2x./norm(N2);
% %                                 Bd2y=Bd2y./norm(N2);
% %                                 Bd2z=Bd2z./norm(N2);
% %                                 
% %                                 
% %                                 data=[A2x,A2y,A2z,Ad2x,Ad2y,Ad2z,A2s,...
% %                                   B2x,B2y,B2z,Bd2x,Bd2y,Bd2z,B2s]';
% %                               data
% %                                 Data=[Data,data];
% %                             end;
% %                             er=0;
%                         else
%                             data=[Ax,Ay,Az,Adx,Ady,Adz,As,...
%                                   Bx,By,Bz,Bdx,Bdy,Bdz,Bs]';
%                             Data=[Data,data];
%                         end;                        
                        
    
end;

if 0
    Data=[];
    
    
    Ax=10;
    Ay=10;
    Az=10;

    Adx=1;
    Ady=1;
    Adz=1;

    Bx=40;
    By=40;
    Bz=40;

    Bdx=1;
    Bdy=1;
    Bdz=1;

    As=4;
    Bs=8;

    N2=[Adx,Ady,Adz];
    Adx=Adx./norm(N2);
    Ady=Ady./norm(N2);
    Adz=Adz./norm(N2);


    N2=[Bdx,Bdy,Bdz];
    Bdx=Bdx./norm(N2);
    Bdy=Bdy./norm(N2);
    Bdz=Bdz./norm(N2);


    data=[Ax,Ay,Az,Adx,Ady,Adz,As,...
          Bx,By,Bz,Bdx,Bdy,Bdz,Bs]';
   Data=[Data,data];
   
    Ax=40;
    Ay=40;
    Az=40;

    Adx=1;
    Ady=1;
    Adz=1;

    Bx=80;
    By=80;
    Bz=80;

    Bdx=1;
    Bdy=3;
    Bdz=3;

    As=8;
    Bs=12;

    N2=[Adx,Ady,Adz];
    Adx=Adx./norm(N2);
    Ady=Ady./norm(N2);
    Adz=Adz./norm(N2);


    N2=[Bdx,Bdy,Bdz];
    Bdx=Bdx./norm(N2);
    Bdy=Bdy./norm(N2);
    Bdz=Bdz./norm(N2);


    data=[Ax,Ay,Az,Adx,Ady,Adz,As,...
          Bx,By,Bz,Bdx,Bdy,Bdz,Bs]';
   Data=[Data,data];
  
end;


img=mhs_data2imgCnew(Data,{'shape',shape,'smooth',smooth,'mode',mode});

%Data(:,4)

%img=Data;