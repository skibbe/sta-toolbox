function SURFACE=renderFDATA(img,A,varargin)
            
show_bifurcations=true;
clear_figure=true;


%set(gca,'Projection','orthographic')
%set(gca,'Projection','perspective')

convert_new2old=true;
render_mode=2;
render_dim=3;
showtext=true;
bifurcation_sphere_scale=1.6;
mode=1;
vpos=false;
thickness=1;
clear_bg=false;

slice_select=-1;
slice_select_pos=-1;

hide_costfunc=false;
line_connect_center=1;
ParticleFaceAlpha=1;

invert_color=false;
if isfield(A,'particle_thickness')
    thickness=A.particle_thickness;
end;
if isfield(A,'thickness')
    thickness=A.thickness;
end;
imgzposition=0;
textcolor=[0,0,0];
textP=[0.02,0.02];

intitle=0;

pmute=[1,2,3];

scale_correction=1;
if isfield(A,'scale_correction')
    scale_correction=A.scale_correction;
end;


rescale=1;
thickfunc=@(x,y)(x*y);
scalefunc=@(x)(x);
%OFFSET=0.75;MA=max(A.data(8,:));MI=min(A.data(8,:));scalefunc=@(x)(((MA*(x-MI)/(MA-MI+OFFSET)))+OFFSET);


isothreshold=0.1;
hide_single_pts=false;
transparent_vessel_center_mode=1;
transparent_vessel=false;
surface_alpha2=1; % not trans vessel -> trans
grid_select=-1;
offset=[0,0,0];
preprocess=true;

select_tree=-1;
select_tree_ids={-1,0};
LineWidth=3.5;
edge_energy_option=1;
hide_crossmarker=false;
triangle_crosmarker=false;
new_scene=true;
iso_alpha=0.25;
min_pathlengt=-1;

surface_alpha=0.65;
transp_centerline_color=[0,0,0];
transp_centerline_thickness=1;

resolution_cylinder=64;
resolution_disk=16;
resolution_cylinder_low=12;

bifurcations=false;


for a=0:2
    if (thickness<0)
    A.data(9+a,:)=A.data(1+a,:)-A.data(4+a,:)*thickness.*A.data(8,:);
    A.data(12+a,:)=A.data(1+a,:)+A.data(4+a,:)*thickness.*A.data(8,:);
    else
    A.data(9+a,:)=A.data(1+a,:)-A.data(4+a,:)*thickness;
    A.data(12+a,:)=A.data(1+a,:)+A.data(4+a,:)*thickness;    
    end;
end;
 %A.data(21,A.data(21,:)==-1)=1;
 
% uids=unique([A.data(21,:),1]);
%num_paths=numel(unique([A.data(21,:),1]));
 uids=unique(A.data(21,:));                
 num_paths=numel(uids);

 new_ids=[1:num_paths];
 lids=A.data(21,:);
 for a=1:num_paths
        A.data(21,lids==uids(a))=new_ids(a);
 end;
 
                
                
              
                
use_pathcolors=false;    
crossmarker_color=[1,0.25,0.25];      

%if isfield(A,'thickness')
%    thickness=A.thickness;
%end;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


if convert_new2old && (max(A.data(17,:)==2)==1) && ((mode==0)||(mode==6))
    A=mhs_graph_new2old(A);
end;


full_img=(numel(img)>3);

A.data(1,:)=A.data(1,:)+offset(1);
A.data(2,:)=A.data(2,:)+offset(2);
A.data(3,:)=A.data(3,:)+offset(3);


A.data(9,:)=A.data(9,:)+offset(1);
A.data(10,:)=A.data(10,:)+offset(2);
A.data(11,:)=A.data(11,:)+offset(3);

A.data(12,:)=A.data(12,:)+offset(1);
A.data(13,:)=A.data(13,:)+offset(2);
A.data(14,:)=A.data(14,:)+offset(3);

A.data(8,:)=A.data(8,:)*scale_correction;

A.data(8,:)=A.data(8,:)*rescale;


if full_img
    if sum(pmute==[1,2,3])<3
        A.data(1:3,:)=A.data(pmute,:);
        A.data(4:6,:)=A.data(3+pmute,:);
        A.data(9:11,:)=A.data(8+pmute,:);
        A.data(12:14,:)=A.data(11+pmute,:);
        img=permute(img,pmute);
    end;
    switch  preprocess
        case 1
            Img=img;
            imgM=mean(Img(:));
            imgV=sqrt(var(Img(:)));
            Img=Img-imgM;
            Img=Img./(imgV);
            Img(:)=max(Img(:),0);
            %Img(:)=min(Img(:),1);
            Img(:)=Img(:)./max(Img(:));
            img=Img;
        case 2
            img=img-min(img(:));
            img=img./max(img(:));
    end;
else
    img=img(pmute);
end;

                for a=1:num_paths
                    if num_paths==1
                        colors{a}=[0,1,0];
                    elseif select_tree_ids{2}
                        ww=0.9;
                        colors{a}=[1,ww*(1-a/num_paths),ww*(1-a/num_paths)]';
                    else
                        %colors{a}=squeeze(hsv2rgb(a/num_paths,1,1))';
                        s=mod(a,2)+mod(a,5)+mod(a,ceil(num_paths/3))+mod(a,ceil(num_paths/2));
                        w=2+5+ceil(num_paths/3)+ceil(num_paths/2)-4;
                        v=mod(a,4)+mod(a,3)+mod(a,ceil(num_paths/5))+mod(a,ceil(num_paths/3));
                        wv=4+3+ceil(num_paths/5)+ceil(num_paths/3)-4;
                      %  fprintf('%f  %f\n',s/w,v/wv);
                        colors{a}=squeeze(hsv2rgb(a/num_paths,(0.7*s/w+0.3),(0.3*v/wv+0.7)))';
                    end;
                end;

                
if exist('custom_color')
    if numel(custom_color)==1
        for a=1:num_paths
            colors{a}=custom_color{1};
        end;
   % else
   %     colors=custom_color;
    end;  
end;
                
        


if exist('pointpos')
    if (size(pointpos{1},1)==1), pointpos{1}=[pointpos{1};pointpos{1}];end;
    pos=A.data(1:3,:);
    %pos(:,A.data(21,:)==1)=inf;
    pos(:,A.data(22,:)~=1)=inf;
    [bla,vindx]=sort(distmat(pointpos{1},pos'),1);
    
    nnn=size(A.data,2);
    if numel(pointpos)>2
        nnn=pointpos{3};
    end;
    
    
    valid_indx=vindx(1:nnn,:);
    valid_dist=bla(1:nnn,:)<pointpos{2}^2;
    valid_indx=valid_indx(valid_dist(:));
    select_tree=unique(A.data(21,valid_indx));
    fprintf('printing tree %d\n',select_tree);
else
    if min(select_tree)>0
        unum=unique([A.data(21,:),1]);

        for a=1:numel(unum)
            tmembers(a)=sum([A.data(21,:),1]==unum(a));
        end;
        [v,indx]=sort(tmembers,'descend');
        select_tree_=select_tree;
        select_tree=unum(indx(select_tree));
        fprintf('printing tree %d-> %d\n',[select_tree_;select_tree]);
        if numel(select_tree)==1
            fprintf('total points %d, selected tree %d\n',size(A.data,2),sum(A.data(21,:)==select_tree));
        end;

    elseif min(select_tree_ids{1})>0
        select_tree=select_tree_ids{1};
        fprintf('printing tree %d\n',select_tree);
    elseif (min_pathlengt>0)

        unum=unique([A.data(21,:),1]);

        for a=1:numel(unum)
            tmembers(a)=sum([A.data(21,:),1]==unum(a));
        end;

        select_tree=unum(tmembers>=min_pathlengt);
        fprintf('printing tree %d\n',select_tree);
    end;
end;


connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          


% for d=1:size(A.data,2)
%     assert(numel(connection_listGT{d})<4);
% end;

terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
terminalsTERMPos=A.data(1:3,terminalsTERM);

if select_tree==-1
    terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
    terminalsBIF=find(terminalsBIFa);
else
    tselections=max((repmat(A.data(21,:),size(select_tree,2),1)==repmat(select_tree',1,size(A.data(21,:),2))),[],1)==1;
    terminalsBIFa=(cellfun(@(x)numel(x),connection_listGT)>2)&(tselections);
    terminalsBIF=find(terminalsBIFa);
end;

terminalsBIFPos=A.data(1:3,terminalsBIF);

bifurcation_groups=zeros(size(A.data,2),1);
num_bifurcation_groups=0;
for a=1:numel(terminalsBIF)
    if bifurcation_groups(terminalsBIF(a))==0
        candidates=connection_listGT{terminalsBIF(a)};
        %fprintf('%d ',numel(candidates));
        
        %dowsnt wor if two bifurcations are directy connected
        %candidates=candidates(A.data(17,candidates)==1);
        %fprintf('-> %d\n',numel(candidates));
        candidates2=[];
        if numel(candidates)>2
            for d=1:numel(candidates)
                for e=1:numel(candidates)
                    if numel(intersect(candidates(d),connection_listGT{candidates(e)}))>0
                    candidates2=[candidates2,d];
                    end;
                end;
            end;
            %candidates=[candidates2,terminalsBIF(a)];
            candidates=candidates(candidates2);
        end;
        %fprintf('-> %d\n',numel(candidates));
        %fprintf('-> %d %d %d\n',[candidates2,terminalsBIF(a)]);
        
        num_bifurcation_groups=num_bifurcation_groups+1;
        bifurcation_groups([candidates(:)',terminalsBIF(a)])=num_bifurcation_groups;
    end;
end;

bifurcation_positions=zeros(3,num_bifurcation_groups);
for a=1:num_bifurcation_groups
    bifurcation_positions(:,a)=mean(A.data(1:3,bifurcation_groups==a),2);
end;
if numel(bifurcation_positions)>0
   terminalsBIFPos=bifurcation_positions;
end;





%%



if exist('custom_color')
    if numel(custom_color)>1
        count=1;
        for a=1:num_paths
            if sum(a==(select_tree))>0
                colors{a}=custom_color{count};
                count=count+1;
            end;
        end;
   % else
   %     colors=custom_color;
    end;  
end;

img_render_options={varargin{:},'alpha',iso_alpha,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim,'invert_color',invert_color,'imgzposition',imgzposition,'vpos',vpos,'slice_select',slice_select,'slice_select_pos',slice_select_pos,'clear_bg',clear_bg};


                %cylinder_offset_corr=0.5;
                cylinder_offset_corr=0;
                sphere_offset_corr=0;
                
                min_scale=min(A.data(8,:));
                max_scale=max(A.data(8,:)-min_scale)+eps;
                
               % num_scale=numel(scales);

                sfactor=1.0;
                clear s;
                

                [x y z] = sphere(16); 
                ps=surf2patch(x,y,z,z);
                 ps.vertices(:,:)=ps.vertices(:,:)+0.5;
                smallspherevnormals=-compute_vnormals2(ps,false); 
                
                
                %[x y z] = sphere(32); 
                [x y z] = sphere(resolution_disk); 
                p_bifurcation=surf2patch(x,y,z,z); 
                n_bifurcation=-compute_vnormals2(p_bifurcation,false); 
                
                if thickness>0
                    z=thickness*z./max(abs(z(:)));     
                else
                    z=z./max(abs(z(:)));     
                end;
                p=surf2patch(x,y,z,z); 
                %p.vertices(:,:)=p.vertices(:,:)+0.5;
                psvnormals=-compute_vnormals2(p,thickness~=-1); 
                
                
                    
                
%                 psvnormals=p.vertices;
%                 psvnormals(1:2:end,:)=repmat([0,0,1],size(psvnormals,1)/2,1);
%                 psvnormals(1:2:end,:)=repmat([0,0,-1],size(psvnormals,1)/2,1);
                %psvnormals=compute_vnormals(p);
                
                
                [x,y,z] = cylinder(1,resolution_cylinder);
                %[x,y,z] = cylinder(1,8);
                pcylinder=surf2patch(x,y,z,z); 
                pcylinder.vertices(:,3)=pcylinder.vertices(:,3)-cylinder_offset_corr;
                vnormals=compute_vnormals(pcylinder);
                
                [x y z] = sphere(16); 
                pssphere=surf2patch(x,y,z,z);
                
                 
                % if exist('centerline_width')
                %     pssphere.vertices(:,:)=(pssphere.vertices(:,:))*2*centerline_width;
                % end;
                 pssphere.vertices(:,:)=pssphere.vertices(:,:)+0.5;
                 
                smallspherenormals2=-compute_vnormals2(pssphere,(thickness~=-1)&&(mode~=6)); 
                
                
                [x,y,z] = cylinder(1,resolution_cylinder_low);
                %[x,y,z] = cylinder(1,8);
                pcylinder2=surf2patch(x,y,z,z); 
                pcylinder2.vertices(:,3)=pcylinder2.vertices(:,3)-cylinder_offset_corr;
                vnormals3=compute_vnormals(pcylinder2);
                %psvnormals=compute_vnormals(pssphere);
                

                
% num_bifurcation_groups=1;                
%                 
% R=[0,1,0];N=3;
% [X,Y,Z] = cylinder(R,N);
% bifurcation_patch=surf2patch(X,Y,Z,Z);
% 
%  PATCHES=bifurcation_patch;
%  %PATCHES.vertices=PATCHES.vertices-repmat(mean(PATCHES.vertices,1),size(PATCHES.vertices,1),1);
%  
%  PATCHES_NORMALS=PATCHES.vertices;
%  nfaces=size(PATCHES.faces,1);
%  nverts=size(PATCHES.vertices,1);
%  PATCHES.faces=repmat(PATCHES.faces,num_bifurcation_groups,1);
%  PATCHES.vertices=repmat(PATCHES.vertices,num_bifurcation_groups,1);
%  %PATCHES.facevertexcdata=PATCHES.vertices;
%  PATCHES.facevertexcdata=repmat([1,0,0],nverts*num_bifurcation_groups,1);
%  for d=1:num_bifurcation_groups
%      PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
%      b_points=A.data([2,1,3],bifurcation_groups==d)+1;
%      b_center=mean(b_points,2);
%      b_vindeces=[2:3:11];
%      bpatch=bifurcation_patch;
%      bpatch.vertices(b_vindeces(1),:)=b_points(:,1);
%      bpatch.vertices(b_vindeces(2),:)=b_points(:,2);
%      bpatch.vertices(b_vindeces(3),:)=b_points(:,3);
%      bpatch.vertices(b_vindeces(4),:)=b_points(:,1);
%      b_n=cross(b_points(:,1)-b_points(:,2),b_points(:,1)-b_points(:,3));
%      b_height=5;
%      b_n=b_height*b_n./norm(b_n);
%      
%      b_vindeces=[1:3:10];
%      bpatch.vertices(b_vindeces,:)=repmat(b_center+b_n,1,4)';
%      b_vindeces=[3:3:12];
%      bpatch.vertices(b_vindeces,:)=repmat(b_center-b_n,1,4)';
%      PATCHES.vertices((d-1)*nverts+1:(d)*nverts,:)=bpatch.vertices;
%      tmp=bpatch.vertices-repmat(b_center,1,nverts)';
%      tmp=tmp./repmat(sqrt(sum(tmp.^2,2)),1,3);
%      PATCHES_NORMALS((d-1)*nverts+1:(d)*nverts,:)=tmp;
%  end;
% 
%  
% %  if new_scene
% %     if clear_figure,clf;end;
% %     %render_img(img,'alpha',iso_alpha,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim,'invert_color',invert_color,'imgzposition',imgzposition,'vpos',vpos,'slice_select',slice_select,'slice_select_pos',slice_select_pos,'clear_bg',clear_bg);
% %     render_img(img,img_render_options{:});
% % 
% %     end;
% clf;
%  patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS);
%  hide_crossmarker=true;
%    camlight; 
%     
%     lighting phong
% return;
                
                
                
%                 vnormals=zeros(size(pcylinder.vertices));
%                 for a=1:size(pcylinder.faces,1)
%                     %idx=pcylinder.face()
%                     %snormal=cross(pcylinder.vertices(pcylinder.faces(a,1),:),pcylinder.vertices(pcylinder.faces(a,2),:));
%                     n1=pcylinder.vertices(pcylinder.faces(a,2),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
%                     n1=n1/norm(n1);
%                     n2=pcylinder.vertices(pcylinder.faces(a,4),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
%                     n2=n2/norm(n2);
%                     snormal=cross(n1,n2);
%                     for b=1:4
%                         vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+snormal;
%                     end;
%                 end;
%                 vnormals=vnormals./repmat(sqrt(sum(vnormals.^2,2)),1,3);
                
switch (mode)
    
    
    case 0

                if new_scene
                    if clear_figure,clf;end;
                    %render_img(img,'alpha',iso_alpha,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim,'invert_color',invert_color,'imgzposition',imgzposition,'vpos',vpos,'slice_select',slice_select,'slice_select_pos',slice_select_pos,'clear_bg',clear_bg);
                    render_img(img,img_render_options{:});
                    
                end;
                
                hold on;
                shape=size(img);
                
                
                mat={'SpecularColorReflectance',1,...%[0..1]
                    'SpecularExponent',5,...% [5..20]
                    'SpecularStrength',0.3, ... % [0..1]
                    'DiffuseStrength',0.8, ... %[0..1]
                    'AmbientStrength',0.15,... %[0..1]
                    }; 
                
                hold on;
                
               
                num_connections=0;
                 for d=1:size(A.data,2)
                     if transparent_vessel
                            if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                                    continue;
                            end;

                            if ((A.data(end,d)==1)||(numel(A.connections)==0))||(~hide_single_pts)
                                  num_connections=num_connections+1;
                            end;
                     else
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;

                        if numel(connection_listGT{d})==1
                            num_connections=num_connections+1;
                        end;
                     end;
                 end;
                 
                 PATCHES=p;
                 nfaces=size(PATCHES.faces,1);
                 nverts=size(PATCHES.vertices,1);
                 PATCHES.faces=repmat(PATCHES.faces,num_connections,1);
                 PATCHES.vertices=repmat(PATCHES.vertices,num_connections,1);
                 PATCHES.facevertexcdata=PATCHES.vertices;
                 
                 PATCHES_NORMALS=PATCHES.vertices;
               
                 for d=1:num_connections
                     PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
                 end;
                    
                
count=0;                
if transparent_vessel
    
    
%     if transparent_vessel_center_mode==2
%                 num_connections=0;
%                  for d=1:size(A.connections,2)
%                         pointIDA=A.connections(1,d)+1;
%                         pointIDB=A.connections(2,d)+1;
%                         if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
%                             continue;
%                         end;
%                         num_connections=num_connections+1;
%                  end;
%                  
%                  
%                  PATCHES2=pcylinder2;
%                  
%                  nfaces2=size(PATCHES2.faces,1);
%                  nverts2=size(PATCHES2.vertices,1);
%                  PATCHES2.faces=repmat(PATCHES2.faces,num_connections,1);
%                  PATCHES2.vertices=repmat(PATCHES2.vertices,num_connections,1);
%                  PATCHES2.facevertexcdata=PATCHES2.vertices;
%                  
%                  PATCHES_NORMALS2=PATCHES2.vertices;
%                
%                  for d=1:num_connections
%                      PATCHES2.faces((d-1)*nfaces2+1:(d)*nfaces2,:)=PATCHES2.faces((d-1)*nfaces2+1:(d)*nfaces2,:)+(d-1)*nverts;
%                  end;
%                  
%                 count=0;
%                 for d=1:size(A.connections,2)
%                         pointIDA=A.connections(1,d)+1;
%                         pointIDB=A.connections(2,d)+1;
%                         
%                         if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
%                             continue;
%                         end;
%                         
%                         count=count+1;
%                         
%                         
%                         Ax=A.data(1,pointIDA)+1;
%                         Ay=A.data(2,pointIDA)+1;
%                         Az=A.data(3,pointIDA)+1;
%                         
%                         Adx=A.data(4,pointIDA);
%                         Ady=A.data(5,pointIDA);
%                         Adz=A.data(6,pointIDA);
% 
%                         Bx=A.data(1,pointIDB)+1;
%                         By=A.data(2,pointIDB)+1;
%                         Bz=A.data(3,pointIDB)+1;
%                         
%                           %  carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
%                            % carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
%                         
%                         Bdx=A.data(4,pointIDB);
%                         Bdy=A.data(5,pointIDB);
%                         Bdz=A.data(6,pointIDB);
%                         
%                         As=0.5;%A.data(8,pointIDA);  
%                         
%                         Bs=0.5;%A.data(8,pointIDB);  
%                         
%                         
%                         if exist('centerline_width')
%                             As=centerline_width;
%                             Bs=centerline_width;
%                         end;
%                        
%                         
%                         
% %                         px=(Ax+Bx)/2;
% %                         py=(Ay+By)/2;
% %                         pz=(Az+Bz)/2;
% % 
% %                          le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
%                          
%                         
%                         N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
%                          
%                          
%                         N1=[0,0,1]';
%                         N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
%                         if (dot(N0,N2)<0)
%                             N2=-N2;
%                         end;
%                         M1=tranmat(N1,N2);
%                   
%                         
%                         N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
%                         if (dot(N3,N2)<0)
%                             N3=-N3;
%                         end;
%                         M2=tranmat(N2,N3);
%                 
%                         
%                         p2=pcylinder2;
%                         
%                         p2.vertices(:,3)=0;
%                          p2.vertices(1:2:end,1:2)=transp_centerline_thickness*sfactor*As*p2.vertices(1:2:end,1:2);
%                          p2.vertices(2:2:end,1:2)=transp_centerline_thickness*sfactor*Bs*p2.vertices(2:2:end,1:2);
% 
%                         
%                         v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
%                         
%                         %line_connect_center=false;
%                         if line_connect_center
%                             centerMatA=[Ay,Ax,Az];
%                             centerMatB=[By,Bx,Bz];
%                         else
%                             pointIDA_side=A.connections(3,d);
%                             pointIDB_side=A.connections(4,d);
%                             centerMatA=A.data(3*pointIDA_side+[10,9,11],pointIDA)'+1;
%                             centerMatB=A.data(3*pointIDB_side+[10,9,11],pointIDB)'+1;
%                         end;
%                         
%                         
%                         p2.vertices(1:2:end,:)=v1+repmat(centerMatA,size(pcylinder2.vertices(1:2:end,:),1),1);    
%                         
%                         p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat(centerMatB,size(pcylinder2.vertices(2:2:end,:),1),1);    
% 
%                    
%                         vnormals2=vnormals3;
%                         vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
%                         vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
%                         
%                   
%                  
%                  
%                
% 
%                         
%                          if (use_pathcolors)
%                             color=colors{A.data(21,pointIDA)};
%                             %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
%                          else
%                             %color=[1,0.25,0.25];
%                             %color=0.5*[1,1,1];
%                             color=[1,1,0];
%                             %patch(p2,'FaceColor',[1,0.25,0.25],'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
%                          end;
%                          
%                          
%                  PATCHES2.vertices((count-1)*nverts2+1:(count)*nverts2,:)=p2.vertices;
%                  PATCHES2.facevertexcdata((count-1)*nverts2+1:(count)*nverts2,:)=repmat(color,nverts2,1);
%                  PATCHES_NORMALS2((count-1)*nverts2+1:(count)*nverts2,:)=vnormals2;
%                 end 
%                 
%                 if size(A.connections,2)>0
%                     patch(PATCHES2,'FaceVertexCData',PATCHES2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS2);
%                     drawnow
%                 end;
%     end;
    
    
    
     for d=1:size(A.data,2)
                    if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                        continue;
                    end;
                    
                    if ((A.data(end,d)==1)||(numel(A.connections)==0))||(~hide_single_pts)
                        
                        count=count+1;
                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        dx=A.data(4,d);
                        dy=A.data(5,d);
                        dz=A.data(6,d);

                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;

                        
                        N1=[0,0,1]';
                        N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
                        
                        [M]=tranmat(N1,N2);
                        Ss=scalefunc(A.data(8,d));
                        p2=p;
                        
                        if thickness<0
                            p2.vertices(:,3)=thickfunc(-thickness,Ss)*p2.vertices(:,3);    
                        end;
                        
                        
                        p2.vertices(:,1:2)=sfactor*Ss*p2.vertices(:,1:2);
                        
                        
                        p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(p.vertices,1),1);
                        
                        vnormals2=sta_matmult(psvnormals',M)';
                        if (use_pathcolors)
                             color=colors{A.data(21,d)};
                        else
                            color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                     
%                             if (A.data(7,d)==1)
%                                 color=[1,0.5,0.5];
%                                            color=[0.7+0.3*(Ss-min_scale)/max_scale,0.3+0.4*(Ss-min_scale)/max_scale,1-0.4*(Ss-min_scale)/max_scale];
%                             else
%                                 color=[0,0,0];
%                                 color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
%                             end;
                        end;
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                         PATCHES.vertices((count-1)*nverts+1:(count)*nverts,:)=p2.vertices;
                         PATCHES.facevertexcdata((count-1)*nverts+1:(count)*nverts,:)=repmat(color,nverts,1);
                        PATCHES_NORMALS((count-1)*nverts+1:(count)*nverts,:)=vnormals2;
                
                
                
                        
                        %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1, 'VertexNormals',vnormals2);
                      
                        
                        if mod(d,250)==0;
                            drawnow;
                        end;
                        
                    end;
                end;

else
    
                for d=1:size(A.data,2)
                    
                    if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if numel(connection_listGT{d})==1 %(sum(A.data(19:20,d))==1)
                        count=count+1;
                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        dx=A.data(4,d);
                        dy=A.data(5,d);
                        dz=A.data(6,d);

                        
                       
                        
                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;

                        
                        N1=[0,0,1]';
                        N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
                        
                        [M]=tranmat(N1,N2);
                        
                        Ss=scalefunc(A.data(8,d));
                        p2=p;
                        p2.vertices(:,1:2)=sfactor*Ss*p2.vertices(:,1:2);
                        
                        vnormals2=sta_matmult(psvnormals',M)';
                        p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(p.vertices,1),1);
                        
                        color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        if (use_pathcolors)
                            color=colors{A.data(21,d)};
                        end;                        
                        
                         PATCHES.vertices((count-1)*nverts+1:(count)*nverts,:)=p2.vertices;
                         PATCHES.facevertexcdata((count-1)*nverts+1:(count)*nverts,:)=repmat(color,nverts,1);
                        PATCHES_NORMALS((count-1)*nverts+1:(count)*nverts,:)=vnormals2;
                        %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1, 'VertexNormals',vnormals2,'NormalMode','manual',mat{:});
                        
%                         if mod(d,250)==0;
%                             drawnow;
%                         end;

                    end;
                end;
    
end;
                

patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS);
              drawnow;  

                num_connections=0;
                 for d=1:size(A.connections,2)
                      pointIDA=A.connections(2,d)+1;
                        
                          if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        num_connections=num_connections+1;
                 end;
                 
                 PATCHES=pcylinder;
                 nfaces=size(PATCHES.faces,1);
                 nverts=size(PATCHES.vertices,1);
                 PATCHES.faces=repmat(PATCHES.faces,num_connections,1);
                 PATCHES.vertices=repmat(PATCHES.vertices,num_connections,1);
                 PATCHES.facevertexcdata=PATCHES.vertices;
                 
                 PATCHES_NORMALS=PATCHES.vertices;
               
                 for d=1:num_connections
                     PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
                 end;
                 
                 if transparent_vessel
                         PATCHES2=pcylinder2;
                         nfaces2=size(PATCHES2.faces,1);
                         nverts2=size(PATCHES2.vertices,1);
                         PATCHES2.faces=repmat(PATCHES2.faces,num_connections,1);
                         PATCHES2.vertices=repmat(PATCHES2.vertices,num_connections,1);
                         PATCHES2.facevertexcdata=PATCHES2.vertices;

                         PATCHES2_NORMALS=PATCHES2.vertices;

                         for d=1:num_connections
                             PATCHES2.faces((d-1)*nfaces2+1:(d)*nfaces2,:)=PATCHES2.faces((d-1)*nfaces2+1:(d)*nfaces2,:)+(d-1)*nverts2;
                         end;
                     
                 end;

               fprintf('computing vessel-mesh\n');
                 count=0;
                for d=1:size(A.connections,2)
                        if (mod(d,ceil(size(A.connections,2)/100))==0)||(d==size(A.connections,2))
                            fprintf('*');
                        end;
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                          if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                         count=count+1;
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
%                             carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
%                             carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        As=scalefunc(A.data(8,pointIDA));  
                        
                        Bs=scalefunc(A.data(8,pointIDB));  
                       
                        px=(Ax+Bx)/2;
                        py=(Ay+By)/2;
                        pz=(Az+Bz)/2;

                         le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
                        N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
                         
                         
                        N1=[0,0,1]';
                        N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
                        if (dot(N0,N2)<0)
                            N2=-N2;
                        end;
                        M1=tranmat(N1,N2);
                  
                        
                        N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
                        if (dot(N3,N2)<0)
                            N3=-N3;
                        end;
                        
                        M2=tranmat(N2,N3);
                        %M2=tranmat(N3,N2);
                
                        
                        p2=pcylinder;
                        
                        
                        
                        p2.vertices(:,3)=0;
                        %p2.vertices(1:2:end,1:2)=3*p2.vertices(1:2:end,1:2);
                        %p2.vertices(2:2:end,1:2)=3*p2.vertices(2:2:end,1:2);
                         p2.vertices(1:2:end,1:2)=sfactor*As*p2.vertices(1:2:end,1:2);
                         p2.vertices(2:2:end,1:2)=sfactor*Bs*p2.vertices(2:2:end,1:2);
                        %p2.vertices(:,3)=sfactor*le*p2.vertices(:,3);
                        
                        
%                         n1=cross(p2.vertices(1,:),p2.vertices(3,:));n1=n1/norm(n1);
%                         n2=cross(p2.vertices(1,:),n1);n2=n2/norm(n2);
%                         M2=tranmat(N2,N3);
                        
                        v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                        p2.vertices(1:2:end,:)=v1+repmat([Ay,Ax,Az],size(pcylinder.vertices(1:2:end,:),1),1);    
                        
                        
                        
                        
                        p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat([By,Bx,Bz],size(pcylinder.vertices(2:2:end,:),1),1);    
                        %p2.vertices(2:2:end,:)=sta_matmult(v1',M2)'+repmat([By,Bx,Bz],size(pcylinder.vertices(2:2:end,:),1),1);    
                        
                        
                        vnormals2=vnormals;
                        vnormals2(1:2:end,:)=sta_matmult(vnormals(1:2:end,:)',M1)';
                        vnormals2(2:2:end,:)=sta_matmult(vnormals(2:2:end,:)',M2*M1)';
                        
                  
                        
                        Ss=min(Bs,As);
                        
                   
                         color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        
                        colorA=[0.81-0.2*(As-min_scale)/max_scale,1-0.3*(As-min_scale)/max_scale,0.4+0.5*(As-min_scale)/max_scale];
                        colorB=[0.81-0.2*(Bs-min_scale)/max_scale,1-0.3*(Bs-min_scale)/max_scale,0.4+0.5*(Bs-min_scale)/max_scale];
                        
                        if (use_pathcolors)
                            if exist('custom_surface_color')
                                color=custom_surface_color;
                            else
                                color=colors{A.data(21,pointIDA)};
                            end;
                            colorA=color;
                            colorB=color;
                        end;
                        
                        
                        p2.facevertexcdata=zeros(size(p2.vertices));
                        p2.facevertexcdata(1:2:end,:)=repmat(colorA,size(p2.vertices,1)/2,1);
                        p2.facevertexcdata(2:2:end,:)=repmat(colorB,size(p2.vertices,1)/2,1);
                      
                        

                        color=[1,1,1];
%                         if transparent_vessel
%                             patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',0.65,'VertexNormals',vnormals2,mat{:});
%                         else
%                             patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2,mat{:});
%                         end;
                        
                        PATCHES.vertices((count-1)*nverts+1:(count)*nverts,:)=p2.vertices;
                        PATCHES.facevertexcdata((count-1)*nverts+1:(count)*nverts,:)=p2.facevertexcdata;
                        PATCHES_NORMALS((count-1)*nverts+1:(count)*nverts,:)=vnormals2;
                        
                         if transparent_vessel
                            p2=pcylinder2;

                            p2.vertices(:,3)=0;
                            p2.vertices(:,1:2)=transp_centerline_thickness*p2.vertices(:,1:2);


                            v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                            p2.vertices(1:2:end,:)=v1+repmat([Ay,Ax,Az],size(pcylinder2.vertices(1:2:end,:),1),1);    
                            p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat([By,Bx,Bz],size(pcylinder2.vertices(2:2:end,:),1),1);    

                            
                            vnormals2=vnormals3;
                            vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
                            vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
                            
                            %patch(p2,'FaceColor',[0,0,0],'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                            PATCHES2.vertices((count-1)*nverts2+1:(count)*nverts2,:)=p2.vertices;
                            %PATCHES2.facevertexcdata((count-1)*nverts2+1:(count)*nverts2,:)=p2.facevertexcdata;
                            PATCHES_NORMALS2((count-1)*nverts2+1:(count)*nverts2,:)=vnormals2;
                         end;
                        
%                         if mod(d,250)==0;
%                             drawnow;
%                         end;
                        
                end     
                fprintf('\n');
                 if transparent_vessel
                    if (size(A.connections,2)>0) 
                        patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',surface_alpha,'VertexNormals',PATCHES_NORMALS);

                        patch(PATCHES2,'FaceColor',transp_centerline_color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS2);
                    end;
                 else
                     chunksize=5000;
                     count=0;
                     dcount=0;
                     while count<num_connections
                         
                        select=[count,min(count+chunksize,num_connections)];
                        %PATCHES2.faces=PATCHES.faces(select*nfaces,:);
                        current_chunksize=select(2)-select(1);
                        PATCHES2.faces=repmat(pcylinder.faces,current_chunksize,1);
                      
                         
                         for d=1:current_chunksize
                             PATCHES2.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES2.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
                         end;
                        
                        
                        PATCHES2.vertices=PATCHES.vertices(select(1)*nverts+1:select(2)*nverts,:);
                        PATCHES2.facevertexcdata=PATCHES.facevertexcdata(select(1)*nverts+1:select(2)*nverts,:);
                        PATCHES_NORMALS2=PATCHES_NORMALS(select(1)*nverts+1:select(2)*nverts,:);
                        
                        patch(PATCHES2,'FaceVertexCData',PATCHES2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',surface_alpha2,'VertexNormals',PATCHES_NORMALS2);
                        count=count+current_chunksize;
                        dcount=dcount+1;
                        if mod(dcount,2)==0
                         drawnow;
                        end;
                        fprintf('%d %d\n',count,num_connections);
                     end;
                     %patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS);
                 end;
                 
                 
                 if nargout>0
                     SURFACE=patchQuad2Tri(PATCHES);
                     %SURFACE=(PATCHES);
                 end;

                
                
                lighting phong

                drawnow;
    
    
    case 1

                if new_scene
                    if clear_figure,clf;end;
                        render_img(img,img_render_options{:});
                end;
                
                
                if numel(A.data)==0
                    return;
                end;
                 
                hold on;
                
                num_connections=0;
                 for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        num_connections=num_connections+1;
                 end;
                 
                 
                 PATCHES=pcylinder2;
                 
                 nfaces=size(PATCHES.faces,1);
                 nverts=size(PATCHES.vertices,1);
                 PATCHES.faces=repmat(PATCHES.faces,num_connections,1);
                 PATCHES.vertices=repmat(PATCHES.vertices,num_connections,1);
                 PATCHES.facevertexcdata=PATCHES.vertices;
                 
                 PATCHES_NORMALS=PATCHES.vertices;
               
                 for d=1:num_connections
                     PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
                 end;
                 
                count=0;
                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        count=count+1;
                        
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
                          %  carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
                           % carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        
                        if (A.data(17,pointIDA))==2
                            Adx=Bdx;
                            Ady=Bdy;
                            Adz=Bdx;
                        end;
                        
                        if (A.data(17,pointIDB))==2
                            Bdx=Adx;
                            Bdy=Ady;
                            Bdz=Adx;
                        end;
                        
                        assert(~(((A.data(17,pointIDB))==2)&&((A.data(17,pointIDA))==2)));
                        
                        As=0.5;%A.data(8,pointIDA);  
                        
                        Bs=0.5;%A.data(8,pointIDB);  
                        
                        
                        if exist('centerline_width')
                            As=centerline_width;
                            Bs=centerline_width;
                        end;
                       
                        
                        
%                         px=(Ax+Bx)/2;
%                         py=(Ay+By)/2;
%                         pz=(Az+Bz)/2;
% 
%                          le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
                         
                        
                        N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
                         
                         
                        N1=[0,0,1]';
                        N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
                        if (dot(N0,N2)<0)
                            N2=-N2;
                        end;
                        M1=tranmat(N1,N2);
                  
                        
                        N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
                        if (dot(N3,N2)<0)
                            N3=-N3;
                        end;
                        M2=tranmat(N2,N3);
                
                        
                        p2=pcylinder2;
                        
                        p2.vertices(:,3)=0;
                         p2.vertices(1:2:end,1:2)=transp_centerline_thickness*sfactor*As*p2.vertices(1:2:end,1:2);
                         p2.vertices(2:2:end,1:2)=transp_centerline_thickness*sfactor*Bs*p2.vertices(2:2:end,1:2);

                        
                        v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                        
                        %line_connect_center=false;
                        if line_connect_center
                            centerMatA=[Ay,Ax,Az];
                            centerMatB=[By,Bx,Bz];
                        else
                            pointIDA_side=A.connections(3,d);
                            pointIDB_side=A.connections(4,d);
                            centerMatA=A.data(3*pointIDA_side+[10,9,11],pointIDA)'+1;
                            centerMatB=A.data(3*pointIDB_side+[10,9,11],pointIDB)'+1;
                        end;
                        
                        
                        p2.vertices(1:2:end,:)=v1+repmat(centerMatA,size(pcylinder2.vertices(1:2:end,:),1),1);    
                        
                        p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat(centerMatB,size(pcylinder2.vertices(2:2:end,:),1),1);    

                   
                        vnormals2=vnormals3;
                        vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
                        vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
                        
                  
                 
                 
               
                         if (show_bifurcations)
                                if (A.connections(5,d))
                                    color=[1,0.9,0.9];
                                else
                                    color=[0.2,0.1,0.1];
                                end;
                         elseif  (use_pathcolors)
                            color=colors{A.data(21,pointIDA)};
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                         else
                            %color=[1,0.25,0.25];
                            %color=0.5*[1,1,1];
                            color=[1,1,0];
                            %patch(p2,'FaceColor',[1,0.25,0.25],'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                         end;
                         
                         
                 PATCHES.vertices((count-1)*nverts+1:(count)*nverts,:)=p2.vertices;
                 PATCHES.facevertexcdata((count-1)*nverts+1:(count)*nverts,:)=repmat(color,nverts,1);
                 PATCHES_NORMALS((count-1)*nverts+1:(count)*nverts,:)=vnormals2;
                end 
                
                if size(A.connections,2)>0
                    patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS);
                    drawnow
                end;
                
                
                
                
                 num_connections=0;
                 for d=1:size(A.data,2)
                          %if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                        if ((A.data(end,d)==1)||(numel(A.connections)==0))||(~hide_single_pts)
                            num_connections=num_connections+1;
                        end;
                 end;
                 
                 PATCHES=p;
                 nfaces=size(PATCHES.faces,1);
                 nverts=size(PATCHES.vertices,1);
                 PATCHES.faces=repmat(PATCHES.faces,num_connections,1);
                 PATCHES.vertices=repmat(PATCHES.vertices,num_connections,1);
                 PATCHES.facevertexcdata=PATCHES.vertices;
                 PATCHES_NORMALS=PATCHES.vertices;
                 for d=1:num_connections
                     PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
                 end;
          
                 
                count=0;
                for d=1:size(A.data,2)
 
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if ((A.data(end,d)==1)||(numel(A.connections)==0))||(~hide_single_pts)
                        
                        count=count+1;
                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        dx=A.data(4,d);
                        dy=A.data(5,d);
                        dz=A.data(6,d);

                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;

                         if (A.data(17,d)~=2)
                             
                                N1=[0,0,1]';
                                N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
                                [M]=tranmat(N1,N2);

                                Ss=scalefunc(A.data(8,d));
                                p2=p;

                                if thickness<0
                                    p2.vertices(:,3)=thickfunc(-thickness,Ss)*p2.vertices(:,3);   % -thickness*thickfunc(Ss,p2.vertices(:,3));    
                                end;
                                p2.vertices(:,1:2)=sfactor*Ss*p2.vertices(:,1:2);


                                p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(p.vertices,1),1);


                                vnormals2=sta_matmult(psvnormals',M)';
                                
                         else      
                               p2= p_bifurcation;
                               Ss=scalefunc(A.data(8,d));
                               p2.vertices(:,1:3)=sfactor*Ss*p2.vertices(:,1:3);
                               p2.vertices=p2.vertices+repmat([py,px,pz],size(p.vertices,1),1);
                               vnormals2=n_bifurcation;
                         end;
                                
                        if (use_pathcolors)
                             color=colors{A.data(21,d)};
                        else
                            switch (A.data(7,d))
                                case 0
                                  color=[1,0,0];
                                case 1
                                  color=[0,1,0];
                                case 2
                                  color=[0,0,1];                                  
                                case 3
                                  color=[1,1,0];                                    
                            end;
                        end;
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        PATCHES.vertices((count-1)*nverts+1:(count)*nverts,:)=p2.vertices;
                        PATCHES.facevertexcdata((count-1)*nverts+1:(count)*nverts,:)=repmat(color,nverts,1);
                        PATCHES_NORMALS((count-1)*nverts+1:(count)*nverts,:)=vnormals2;
                 
                        if mod(d,250)==0;
                            drawnow;
                        end;
                    end;
                end;
                
               patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',ParticleFaceAlpha,'VertexNormals',PATCHES_NORMALS);
               %patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS);

                 lighting phong
                 drawnow
    
    
    
                
    case 2
               % fhandle2=figure(1001);
               
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;
                
                %if clear_figure,clf;end;
                %fhandle2=figure(1001);
                %subplot(1,2,2);
                %render_img(saliency_map,'alpha',iso_alpha,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim);
                %render_img(img,'alpha',iso_alpha,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim);
                hold on;
               % lighting none
                
                shape=size(img);
                %carray=zeros(shape+1);
               %  pscarray=zeros([shape+1,3]);
%                  faces=[...
%                     1 , 2 , 4;...
%                     2 , 3 , 4;...        
%                     1 , 4 , 6;...  
%                     4 , 5 , 6;...        
%                         ];

                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        connectedA=sum(A.data(19:20,pointIDA));
                        connectedB=sum(A.data(19:20,pointIDB));

                        if 1 %(connectedA>1)&&(connectedB>1)
                            Ax=A.data(1,pointIDA)+1;
                            Ay=A.data(2,pointIDA)+1;
                            Az=A.data(3,pointIDA)+1;

                            Bx=A.data(1,pointIDB)+1;
                            By=A.data(2,pointIDB)+1;
                            Bz=A.data(3,pointIDB)+1;
                                                        
                            col=colors{A.data(21,pointIDB)};
                            
                            plot3([Ay,By],[Ax,Bx],[Az,Bz],'color',col,'LineWidth',LineWidth);
                        end;
                        if mod(d,5000)==0;
                            drawnow;
                        end;
                end
                 
if ~hide_crossmarker                
                for d=1:size(terminalsBIFPos,2)
                    p2=ps;
                    p2.vertices=1.6*p2.vertices;
                    posv=terminalsBIFPos(:,d)';
                    
                    p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);


                    color=[1,0.25,0.25];
                    %color=[0.25,1,0.25];
                    %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
                    patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherevnormals);  
                    
                end;
    
    
%                 indx=find(carray>2);
%                 [Ix,Iy,Iz]=ind2sub(shape+1,indx);
%                 for d=1:numel(Ix)
%                             p2=ps;
%                             p2.vertices=1.5*p2.vertices;
%                             %p2.vertices=p2.vertices+repmat([Iy(d),Ix(d),Iz(d)],size(ps.vertices,1),1);
%                             
%                             posv=squeeze(pscarray(Ix(d),Iy(d),Iz(d),:))';
%                             %p2.vertices=p2.vertices+repmat([Iy(d),Ix(d),Iz(d)],size(pssphere.vertices,1),1);
%                             p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(ps.vertices,1),1);
%                             
%                             
%                             
%                             
%                             %color=[1,1,0];
%                             color=[1,0.25,0.25];
%                             %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
%                             
%                             
%                             patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1, 'VertexNormals',smallspherevnormals);
%                             
%                     
%                 end;
end;                
                
%                indx=find((carray==2));
%                [Ix,Iy,Iz]=ind2sub(shape+1,indx);
%                 for d=1:numel(Ix)
%                             p2=ps;
%                             p2.vertices=1.5*p2.vertices;
%                             p2.vertices=p2.vertices+repmat([Iy(d),Ix(d),Iz(d)],size(ps.vertices,1),1);
%                             %color=[0.5,1,0.5];
%                             color=[1,1,0.5];
%                             patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
%                     
%                 end;
                
                    lighting phong
                   drawnow;
               
    case 3                   
               % fhandle2=figure(1002);

                if new_scene
                    if clear_figure,clf;end;
                end;
                
                
                %imshow(real(img.^0.5),'InitialMagnification','fit');
                %imshow(real(squeeze(max(saliency_map,[],3))),'InitialMagnification','fit');
                
                
               
                
                for a=1:3
                    if (grid_select==a)||(grid_select==-1)
                        
                        if (grid_select==-1)
                            subplot(2,2,a);
                        end;
                        
                        if new_scene
                            imshow(real(squeeze(max(img,[],a))),'InitialMagnification','fit');
                        end;
                        hold on;

                         for d=1:size(A.connections,2)
                             
                            
                        
                                pointIDA=A.connections(1,d)+1;
                                pointIDB=A.connections(2,d)+1;
                                
                                tsel=[max(select_tree==A.data(21,pointIDA))==0,max(select_tree==A.data(21,pointIDB))==0];
                                if (min(select_tree)>-1)&&(((numel(select_tree)==1)&&(min(tsel)==1))||(max(tsel)))   
                                %if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                                    continue;
                                end;
                                
                                
                                %if (min(select_tree)>-1)&&(max(tsel))   
                                
                                
%fprintf('%d %d %d\n',pointIDA,pointIDB,size(A.data,2));
                                connectedA=sum(A.data(19:20,pointIDA));
                                connectedB=sum(A.data(19:20,pointIDB));

                                if (connectedA>1)&&(connectedB>1)
                                    %fprintf('%d ',d);
                                    Ax=A.data(1,pointIDA)+1;
                                    Ay=A.data(2,pointIDA)+1;
                                    Az=A.data(3,pointIDA)+1;

                                    Bx=A.data(1,pointIDB)+1;
                                    By=A.data(2,pointIDB)+1;
                                    Bz=A.data(3,pointIDB)+1;
                                    
                                    
                                    
                                    p1=[Ax,Ay,Az];
                                    p2=[Bx,By,Bz];
                                    p1(a)=[];
                                    p2(a)=[];
                                    %=[d/size(A.connections,2),0.5,1];
                                    %plot([Ay,By],[Ax,Bx],'r','LineWidth',3);
                                    
                                    %plot([p1(2),p2(2)],[p1(1),p2(1)],'r','LineWidth',3);
                                 
                                    col=colors{A.data(21,pointIDB)};
                                    plot([p1(2),p2(2)],[p1(1),p2(1)],'color',col,'LineWidth',3);
                                    
                                    %plot([Ay,By],[Ax,Bx],'Color',hsv2rgb(c),'LineWidth',3);
                                end;
                         end
                       if exist('pointpos')  
                       for d=1:size(pointpos{1},1)
                            posv=pointpos{1}(d,:);
                            posv(a)=[];
                            col=0.8*[1,1,1];
                            cscale=3;
                            p1=posv-cscale*[1,1];
                            p2=posv+cscale*[1,1];
                            plot([p1(2),p2(2)],[p1(1),p2(1)],'color',col,'LineWidth',2);
                            p1=posv-cscale*[1,-1];
                            p2=posv+cscale*[1,-1];
                            plot([p1(2),p2(2)],[p1(1),p2(1)],'color',col,'LineWidth',2);
                       end;
                       end;
                         
                    end;
                end;
                   lighting phong
                   drawnow;

                   

    case 4

               % fhandle2=figure(1004);
                %if clear_figure,clf;end;
                %render_img(img,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim);
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;
                
                %render_img(img,'threshold',max(img(:))+1);
                %shape=(size(img));
                %axis([1 shape(1) 1 shape(2) 1 shape(3)]);
                
                hold on;
                shape=size(img);
                           % carray=zeros(shape+1);
                %scarray=zeros(shape+1);
                

                
               % material([1,0,0]);
                
                mat={'SpecularColorReflectance',1,...%[0..1]
                    'SpecularExponent',5,...% [5..20]
                    'SpecularStrength',0.3, ... % [0..1]
                    'DiffuseStrength',0.8, ... %[0..1]
                    'AmbientStrength',0.15,... %[0..1]
                    }; 
                
                
if transparent_vessel
    
     for d=1:size(A.data,2)

                    
                    %if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                        %if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if ((A.data(end,d)==1)||(numel(A.connections)==0))||(~hide_single_pts)
                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        dx=A.data(4,d);
                        dy=A.data(5,d);
                        dz=A.data(6,d);

                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;

                        
                        N1=[0,0,1]';
                        N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
                        
                        [M]=tranmat(N1,N2);
                        Ss=A.data(8,d);
                        p2=p;
                        
                        if thickness<0
                            p2.vertices(:,3)=thickfunc(-thickness,Ss)*p2.vertices(:,3);    %-thickness*thickfunc(Ss,p2.vertices(:,3));    
                        end;
                        
                        
                        p2.vertices(:,1:2)=sfactor*Ss*p2.vertices(:,1:2);
                        
                        
                        p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(p.vertices,1),1);
                        
                        vnormals2=sta_matmult(psvnormals',M)';
                        if (use_pathcolors)
                             color=colors{A.data(21,d)};
                        else
                            if (A.data(7,d)==1)
                                color=[1,0.5,0.5];
                                           color=[0.7+0.3*(Ss-min_scale)/max_scale,0.3+0.4*(Ss-min_scale)/max_scale,1-0.4*(Ss-min_scale)/max_scale];
                            else
                                color=[0,0,0];
                                color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                            end;
                        end;
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1, 'VertexNormals',vnormals2);
                      
                        
                        if mod(d,250)==0;
                            drawnow;
                        end;
                        
                    end;
                end;
%                 for d=1:size(A.data,2)
%                     
%                     
%                     if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
%                             continue;
%                         end;
%                     
%                    % if ((A.data(end,d)==1)||(numel(A.connections)==0))
%                         
% 
%                         px=A.data(1,d)+1;
%                         py=A.data(2,d)+1;
%                         pz=A.data(3,d)+1;
% 
%                         dx=A.data(4,d);
%                         dy=A.data(5,d);
%                         dz=A.data(6,d);
% 
%                         
%                        
%                         
%                         
%                         Ax=A.data(9,d)+1;
%                         Ay=A.data(10,d)+1;
%                         Az=A.data(11,d)+1;
%                         
%                         Bx=A.data(12,d)+1;
%                         By=A.data(13,d)+1;
%                         Bz=A.data(14,d)+1;
% 
%                         
%                         N1=[0,0,1]';
%                         N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
%                         
%                         [M]=tranmat(N1,N2);
%                         
%                         Ss=A.data(8,d);
%                         p2=p;
%                         p2.vertices(:,1:2)=0.95*sfactor*Ss*p2.vertices(:,1:2);
%                         
%                         vnormals2=sta_matmult(psvnormals',M)';
%                         p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(p.vertices,1),1);
%                         
%                         
%                         %if (A.data(7,d)==1)
%                         %    color=[1,0.5,0.5];
%                         %    color=[0.7+0.3*(Ss-min_scale)/max_scale,0.3+0.4*(Ss-min_scale)/max_scale,1-0.4*(Ss-min_scale)/max_scale];
%                         %else
%                             color=[0,0,0];
%                             color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
%                         %end;
%                         
%                         
%                         if sum(isnan(color))>0
%                             fprintf('dsds');
%                         end;
%                         
%                         if (use_pathcolors)
%                             color=colors{A.data(21,d)};
%                         end;
%                         
%                         patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1, 'VertexNormals',vnormals2);
%                         
%                         if mod(d,250)==0;
%                             drawnow;
%                         end;                        
%                         
%                         
%                         %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
%                   %  end;
%                 end;
                
                
                
%                 for d=1:size(A.connections,2)
%                         pointIDA=A.connections(1,d)+1;
%                         pointIDB=A.connections(2,d)+1;
%                         endpointoffsetA=1;
%                         endpointoffsetB=1;
% 
%                         Ax=A.data(endpointoffsetA,pointIDA)+1;
%                         Ay=A.data(1+endpointoffsetA,pointIDA)+1;
%                         Az=A.data(2+endpointoffsetA,pointIDA)+1;
% 
%                         Bx=A.data(endpointoffsetB,pointIDB)+1;
%                         By=A.data(1+endpointoffsetB,pointIDB)+1;
%                         Bz=A.data(2+endpointoffsetB,pointIDB)+1;
%                         plot3([Ay,By],[Ax,Bx],[Az,Bz],'k','LineWidth',3);
%                 end
                
else
    
                for d=1:size(A.data,2)
                    
                    if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if numel(connection_listGT{d})==1 %(sum(A.data(19:20,d))==1)

                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        dx=A.data(4,d);
                        dy=A.data(5,d);
                        dz=A.data(6,d);

                        
                       
                        
                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;

                        
                        N1=[0,0,1]';
                        N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
                        
                        [M]=tranmat(N1,N2);
                        
                        Ss=A.data(8,d);
                        p2=p;
                        p2.vertices(:,1:2)=sfactor*Ss*p2.vertices(:,1:2);
                        
                        vnormals2=sta_matmult(psvnormals',M)';
                        p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(p.vertices,1),1);
                        
                        
                        %if (A.data(7,d)==1)
                        %    color=[1,0.5,0.5];
                        %    color=[0.7+0.3*(Ss-min_scale)/max_scale,0.3+0.4*(Ss-min_scale)/max_scale,1-0.4*(Ss-min_scale)/max_scale];
                        %else
                            color=[0,0,0];
                            color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                        %end;
                        
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        if (use_pathcolors)
                            color=colors{A.data(21,d)};
                        end;                        
                        
                 

                        patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1, 'VertexNormals',vnormals2,'NormalMode','manual',mat{:});
                        
                        if mod(d,250)==0;
                            drawnow;
                        end;

                        
                        
                        %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
                    end;
                end;
    
end;
                
                drawnow;
                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                          if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
%                             carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
%                             carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        As=A.data(8,pointIDA);  
                        
                        Bs=A.data(8,pointIDB);  
                       
                        px=(Ax+Bx)/2;
                        py=(Ay+By)/2;
                        pz=(Az+Bz)/2;

                         le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
                        N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
                         
                         
                        N1=[0,0,1]';
                        N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
                        if (dot(N0,N2)<0)
                            N2=-N2;
                        end;
                        M1=tranmat(N1,N2);
                  
                        
                        N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
                        if (dot(N3,N2)<0)
                            N3=-N3;
                        end;
                        M2=tranmat(N2,N3);
                
                        
                        p2=pcylinder;
                        
                        p2.vertices(:,3)=0;
                        %p2.vertices(1:2:end,1:2)=3*p2.vertices(1:2:end,1:2);
                        %p2.vertices(2:2:end,1:2)=3*p2.vertices(2:2:end,1:2);
                         p2.vertices(1:2:end,1:2)=sfactor*As*p2.vertices(1:2:end,1:2);
                         p2.vertices(2:2:end,1:2)=sfactor*Bs*p2.vertices(2:2:end,1:2);
                        %p2.vertices(:,3)=sfactor*le*p2.vertices(:,3);
                        
                        
%                         n1=cross(p2.vertices(1,:),p2.vertices(3,:));n1=n1/norm(n1);
%                         n2=cross(p2.vertices(1,:),n1);n2=n2/norm(n2);
%                         M2=tranmat(N2,N3);
                        
                        v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                        p2.vertices(1:2:end,:)=v1+repmat([Ay,Ax,Az],size(pcylinder.vertices(1:2:end,:),1),1);    
                        
                        p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat([By,Bx,Bz],size(pcylinder.vertices(2:2:end,:),1),1);    
                        %p2.vertices(2:2:end,:)=sta_matmult(v1',M2)'+repmat([By,Bx,Bz],size(pcylinder.vertices(2:2:end,:),1),1);    
                        
                        
                        vnormals2=vnormals;
                        vnormals2(1:2:end,:)=sta_matmult(vnormals(1:2:end,:)',M1)';
                        vnormals2(2:2:end,:)=sta_matmult(vnormals(2:2:end,:)',M2*M1)';
                        
                  
                        
                        Ss=min(Bs,As);
                        
                       % scarray(ceil(Ax),ceil(Ay),ceil(Az))=max(scarray(ceil(Ax),ceil(Ay),ceil(Az)),Ss);
                       % scarray(ceil(Bx),ceil(By),ceil(Bz))=max(scarray(ceil(Bx),ceil(By),ceil(Bz)),Ss);      

                       % plot3([Ay,By],[Ax,Bx],[Az,Bz],'k','LineWidth',2*Ss^2);
                        
                        
%                          if (A.data(7,d)==1)
%                             color=[1,0.5,0.5];
% 
%                             color=[0.7+0.3*(Ss-min_scale)/max_scale,0.3+0.4*(Ss-min_scale)/max_scale,1-0.4*(Ss-min_scale)/max_scale];
%                         else
%                             color=[0,0,0];
%                             color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
%                         end;
                         color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        
                        colorA=[0.81-0.2*(As-min_scale)/max_scale,1-0.3*(As-min_scale)/max_scale,0.4+0.5*(As-min_scale)/max_scale];
                        colorB=[0.81-0.2*(Bs-min_scale)/max_scale,1-0.3*(Bs-min_scale)/max_scale,0.4+0.5*(Bs-min_scale)/max_scale];
                        
                        if (use_pathcolors)
                            color=colors{A.data(21,pointIDA)};
                            colorA=color;
                            colorB=color;
                        end;
                        
                        
                        p2.facevertexcdata=zeros(size(p2.vertices));
                        p2.facevertexcdata(1:2:end,:)=repmat(colorA,size(p2.vertices,1)/2,1);
                        p2.facevertexcdata(2:2:end,:)=repmat(colorB,size(p2.vertices,1)/2,1);
                      
                        

                        color=[1,1,1];
                        %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
                        if transparent_vessel
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',0.65,'VertexNormals',vnormals2);
                            patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',0.65,'VertexNormals',vnormals2,mat{:});
                            %patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',0.25,'VertexNormals',vnormals2,mat{:});
                        else
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                            patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2,mat{:});
                        end;
                        
                         if transparent_vessel
                            p2=pcylinder2;

                            p2.vertices(:,3)=0;
                            p2.vertices(:,1:2)=1*p2.vertices(:,1:2);


                            v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                            p2.vertices(1:2:end,:)=v1+repmat([Ay,Ax,Az],size(pcylinder2.vertices(1:2:end,:),1),1);    
                            p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat([By,Bx,Bz],size(pcylinder2.vertices(2:2:end,:),1),1);    

                            
                            vnormals2=vnormals3;
                            vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
                            vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',0.65,'VertexNormals',vnormals2);
                            patch(p2,'FaceColor',[0,0,0],'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                         end;
                        
                        if mod(d,250)==0;
                            drawnow;
                        end;
                        
                        
                        %lighting phong
                        %lighting gouraud
                        %lighting flat

                        %isonormals(D,p1)
                end     
                
                lighting phong

%                     mat={'SpecularColorReflectance',0,...%[0..1]
%                     'SpecularExponent',5,...% [5..20]
%                     'SpecularStrength',0, ... % [0..1]
%                     'DiffuseStrength',0, ... %[0..1]
%                     'AmbientStrength',1,... %[0..1]
%                     }; 
%                 set(backh,'FaceLighting','none',mat{:});
                drawnow;
                
 case 5

                %fhandle2=figure(1005);
                %if clear_figure,clf;end;
                %render_img(img,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim);
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;                
                %render_img(img,'threshold',max(img(:))+1);
                %shape=(size(img));
                %axis([1 shape(1) 1 shape(2) 1 shape(3)]);
                
                hold on;
                shape=size(img);
%                             carray=zeros(shape+1);
%                 scarray=zeros(shape+1);
                
                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                          if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
%                             carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
%                             carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        As=A.data(8,pointIDA);  
                        Bs=A.data(8,pointIDB);  
                       
                        px=(Ax+Bx)/2;
                        py=(Ay+By)/2;
                        pz=(Az+Bz)/2;

                      
                        le=norm([Ax,Ay,Az]-[Bx,By,Bz])/1.5;
                        N1=[0,0,1]';
                        N2=([Ay,Ax,Az]-[By,Bx,Bz])';N2=N2./norm(N2);
                        %N2=([Ax,Ay,Az]-[Bx,By,Bz])';N2=N2./norm(N2);
                        [M]=tranmat(N1,N2);
                        
                        
                        %Ss=A.data(8,d);
                        p2=pcylinder;
                        %p2.vertices(1:end/2,1:2)=sfactor*2*As*p2.vertices(1:end/2,1:2);
                        %p2.vertices(end/2+1:end,1:2)=sfactor*Bs*p2.vertices(end/2+1:end,1:2);
                        p2.vertices(1:2:end,1:2)=sfactor*Bs*p2.vertices(1:2:end,1:2);
                        p2.vertices(2:2:end,1:2)=sfactor*As*p2.vertices(2:2:end,1:2);
                        
                        p2.vertices(:,3)=sfactor*le*p2.vertices(:,3);
                        p2.vertices=sta_matmult(p2.vertices',M)'+repmat([py,px,pz],size(pcylinder.vertices,1),1);    
   
                 
                        
                        Ss=min(Bs,As);
                        
%                         scarray(ceil(Ax),ceil(Ay),ceil(Az))=max(scarray(ceil(Ax),ceil(Ay),ceil(Az)),Ss);
%                         scarray(ceil(Bx),ceil(By),ceil(Bz))=max(scarray(ceil(Bx),ceil(By),ceil(Bz)),Ss);      

                        plot3([Ay,By],[Ax,Bx],[Az,Bz],'k','LineWidth',2*Ss^2);
                        
                        
  %                      if (A.data(7,pointIDA)==1)
 %                           color=[1,0.5,0.5];
%
   %                         color=[0.7+0.3*(Ss-min_scale)/max_scale,0.3+0.4*(Ss-min_scale)/max_scale,1-0.4*(Ss-min_scale)/max_scale];
                       % else
                            color=[0,0,0];
                            color=[0.81-0.2*(Ss-min_scale)/max_scale,1-0.3*(Ss-min_scale)/max_scale,0.4+0.5*(Ss-min_scale)/max_scale];
                        %end;
                        
                        
                        if sum(isnan(color))>0
                            fprintf('dsds');
                        end;
                        
                        if (use_pathcolors)
                            color=colors{A.data(21,pointIDA)};
                        
                        end;
                        
                        
                        patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
                        %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                        
                        %lighting phong
                        %lighting gouraud
                        %lighting flat

                        %isonormals(D,p1)
                end     
                
                lighting phong
                
%                 %psvnormals=-psvnormals;
%                 indx=find(carray>2);
%                 [Ix,Iy,Iz]=ind2sub(shape+1,indx);
%                 for d=1:numel(Ix)
%                             p2=pssphere;
%                             scale=scarray(Ix(d),Iy(d),Iz(d));
%                             
%                             p2.vertices=1.5*scale*p2.vertices;
%                             p2.vertices=p2.vertices+repmat([Iy(d),Ix(d),Iz(d)],size(pssphere.vertices,1),1);
%                             color=[1,0.25,0.25];
%                             %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
%                             patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1);
%                     
%                 end;
                                
                
                 lighting phong
                   drawnow;
                

    case 6

              %  fhandle2=figure(1006);
                %if clear_figure,clf;end;
                %render_img(img,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim);
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;
                %render_img(img,'threshold',max(img(:))+1);
                %shape=(size(img));
                %axis([1 shape(1) 1 shape(2) 1 shape(3)]);
                
                hold on;
                shape=size(img);
                %carray=zeros(shape+1);
                %scarray=zeros(shape+1);
               % pscarray=zeros([shape+1,3]);
                mat={'SpecularColorReflectance',1,...%[0..1]
                    'SpecularExponent',5,...% [5..20]
                    'SpecularStrength',0.3, ... % [0..1]
                    'DiffuseStrength',0.8, ... %[0..1]
                    'AmbientStrength',0.15,... %[0..1]
                    }; 
                
                
                %line_connect_center=false;

                
                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
                          %  carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
                           % carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        As=0.8;%A.data(8,pointIDA);  
                        
                        Bs=0.8;%A.data(8,pointIDB);
                        
                        if exist('centerline_width')
                            As=centerline_width;
                            Bs=centerline_width;
                        end;
                       
                        px=(Ax+Bx)/2;
                        py=(Ay+By)/2;
                        pz=(Az+Bz)/2;

                         le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
                        N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
                         
                         
                        N1=[0,0,1]';
                        N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
                        if (dot(N0,N2)<0)
                            N2=-N2;
                        end;
                        M1=tranmat(N1,N2);
                  
                        
                        N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
                        if (dot(N3,N2)<0)
                            N3=-N3;
                        end;
                        M2=tranmat(N2,N3);
                
                        
                        p2=pcylinder2;
                        
                        p2.vertices(:,3)=0;
                         p2.vertices(1:2:end,1:2)=sfactor*As*p2.vertices(1:2:end,1:2);
                         p2.vertices(2:2:end,1:2)=sfactor*Bs*p2.vertices(2:2:end,1:2);

                        
                        if line_connect_center
                            centerMatA=[Ay,Ax,Az];
                            centerMatB=[By,Bx,Bz];
                        else
                            pointIDA_side=A.connections(3,d);
                            pointIDB_side=A.connections(4,d);
                            centerMatA=A.data(3*pointIDA_side+[10,9,11],pointIDA)'+1;
                            centerMatB=A.data(3*pointIDB_side+[10,9,11],pointIDB)'+1;
                        end;                         
                         
                        v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                        p2.vertices(1:2:end,:)=v1+repmat(centerMatA,size(pcylinder2.vertices(1:2:end,:),1),1);    
                        
                        p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat(centerMatB,size(pcylinder2.vertices(2:2:end,:),1),1);    
                         
                        
                        vnormals2=vnormals3;
                        vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
                        vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
                        
                  

                        
                        
                        %carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))=carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))+1;%max(A.data(19:20,pointIDA)>1);
                        %carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))=carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))+1;%max(A.data(19:20,pointIDB)>1);%max(scarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5)),Ss);      
                        %carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))=carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))+max(A.data(19:20,pointIDA)>1);
                        %carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))=carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))+max(A.data(19:20,pointIDB)>1);%max(scarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5)),Ss);      
                        %pscarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5),:)=[Bx,By,Bz];
                        %pscarray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5),:)=[Ax,Ay,Az];
                        

                        
                        As=A.data(8,pointIDA);  
                        
                        Bs=A.data(8,pointIDB);  
                           
                        
                         if line_connect_center
                            colorA=[0.81-0.2*(As-min_scale)/max_scale,1-0.3*(As-min_scale)/max_scale,0.4+0.5*(As-min_scale)/max_scale];
                            colorB=[0.81-0.2*(Bs-min_scale)/max_scale,1-0.3*(Bs-min_scale)/max_scale,0.4+0.5*(Bs-min_scale)/max_scale];
                         else
                             colorA=[1,1,0];
                             colorB=[1,1,0];
                         end;
                            
                            
                        if (use_pathcolors)
                            color=colors{A.data(21,pointIDA)};
                            colorA=color;
                            colorB=color;
                        end;
                        
                        
                        
                        %colorA=[0.9,1,0.9];
                        %colorA=[0.4,0.6,0.4];
                        %colorA=[0.25,1,0.25];
                        
                        %colorA=[1,0.25,0.25];
                        
                        %colorA=[0.75,1,0.75];
                        %colorB=colorA;
                        p2.facevertexcdata=zeros(size(p2.vertices));
                        p2.facevertexcdata(1:2:end,:)=repmat(colorA,size(p2.vertices,1)/2,1);
                        p2.facevertexcdata(2:2:end,:)=repmat(colorB,size(p2.vertices,1)/2,1);
                      
                       
                       patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2,mat{:});
                       
                       if mod(d,250)==0;
                            drawnow;
                        end;
                end     
                
                
if ~line_connect_center
                count=0;
                for d=1:size(A.data,2)
 
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if (A.data(end,d)==1)
                        
                        count=count+1;
                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        Adx=A.data(4,d);
                        Ady=A.data(5,d);
                        Adz=A.data(6,d);

                        Bdx=-A.data(4,d);
                        Bdy=-A.data(5,d);
                        Bdz=-A.data(6,d);
                        
                        
                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;
                        
                        
                        %old
                        
                        As=0.8;%A.data(8,pointIDA);  
                        
                        Bs=0.8;%A.data(8,pointIDB);
                        
                        if exist('centerline_width')
                            As=centerline_width;
                            Bs=centerline_width;
                        end;
                       
                        %px=(Ax+Bx)/2;
                        %py=(Ay+By)/2;
                        %pz=(Az+Bz)/2;

                         le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
                        N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
                         
                         
                        N1=[0,0,1]';
                        N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
                        if (dot(N0,N2)<0)
                            N2=-N2;
                        end;
                        M1=tranmat(N1,N2);
                  
                        
                        N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
                        if (dot(N3,N2)<0)
                            N3=-N3;
                        end;
                
                        
                        M2=tranmat(N2,N3);
                
                       
                        
                        p2=pcylinder2;
                        
                        p2.vertices(:,3)=0;
                         p2.vertices(1:2:end,1:2)=sfactor*As*p2.vertices(1:2:end,1:2);
                         p2.vertices(2:2:end,1:2)=sfactor*Bs*p2.vertices(2:2:end,1:2);

                        
                            centerMatA=[Ay,Ax,Az];
                            centerMatB=[By,Bx,Bz];
                        
                         
                        v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                        p2.vertices(1:2:end,:)=v1+repmat(centerMatA,size(pcylinder2.vertices(1:2:end,:),1),1);    
                        
                        p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat(centerMatB,size(pcylinder2.vertices(2:2:end,:),1),1);    
                         
                        
                        vnormals2=vnormals3;
                        vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
                        vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
                        
                  

                        
                        
                        %carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))=carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))+1;%max(A.data(19:20,pointIDA)>1);
                        %carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))=carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))+1;%max(A.data(19:20,pointIDB)>1);%max(scarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5)),Ss);      
                        %carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))=carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))+max(A.data(19:20,pointIDA)>1);
                        %carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))=carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))+max(A.data(19:20,pointIDB)>1);%max(scarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5)),Ss);      
                        %pscarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5),:)=[Bx,By,Bz];
                        %pscarray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5),:)=[Ax,Ay,Az];
                        

                        
                        As=A.data(8,d);  
                        
                        Bs=A.data(8,d);  
                           
                        %colorA=[0.81-0.2*(As-min_scale)/max_scale,1-0.3*(As-min_scale)/max_scale,0.4+0.5*(As-min_scale)/max_scale];
                        %colorB=[0.81-0.2*(Bs-min_scale)/max_scale,1-0.3*(Bs-min_scale)/max_scale,0.4+0.5*(Bs-min_scale)/max_scale];
                        
                        colorB=0.5*[1,1,1];
                        colorA=0.5*[1,1,1];
                        
                        if (use_pathcolors)
                            color=colors{A.data(21,d)};
                            colorA=(color);
                            colorB=(color);
                        end;
                        
                        
                        
                        %colorA=[0.9,1,0.9];
                        %colorA=[0.4,0.6,0.4];
                        %colorA=[0.25,1,0.25];
                        
                        %colorA=[1,0.25,0.25];
                        
                        %colorA=[0.75,1,0.75];
                        %colorB=colorA;
                        p2.facevertexcdata=zeros(size(p2.vertices));
                        p2.facevertexcdata(1:2:end,:)=repmat(colorA,size(p2.vertices,1)/2,1);
                        p2.facevertexcdata(2:2:end,:)=repmat(colorB,size(p2.vertices,1)/2,1);
                      
                       
                       patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2,mat{:});
                       
                       if mod(d,250)==0;
                            drawnow;
                        end;
                        
                    end;
                    
                end;
end;
                
               

if triangle_crosmarker
    R=[0,1,0];N=3;
    [X,Y,Z] = cylinder(R,N);
    bifurcation_patch=surf2patch(X,Y,Z,Z);

     PATCHES=bifurcation_patch;
     %PATCHES.vertices=PATCHES.vertices-repmat(mean(PATCHES.vertices,1),size(PATCHES.vertices,1),1);

     PATCHES_NORMALS=PATCHES.vertices;
     nfaces=size(PATCHES.faces,1);
     nverts=size(PATCHES.vertices,1);
     PATCHES.faces=repmat(PATCHES.faces,num_bifurcation_groups,1);
     PATCHES.vertices=repmat(PATCHES.vertices,num_bifurcation_groups,1);
     %PATCHES.facevertexcdata=PATCHES.vertices;
     PATCHES.facevertexcdata=repmat(crossmarker_color,nverts*num_bifurcation_groups,1);
     for d=1:num_bifurcation_groups
         PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)=PATCHES.faces((d-1)*nfaces+1:(d)*nfaces,:)+(d-1)*nverts;
         b_points=A.data([2,1,3],bifurcation_groups==d)+1;
         b_center=mean(b_points,2);
         b_vindeces=[2:3:11];
         bpatch=bifurcation_patch;
         bpatch.vertices(b_vindeces(1),:)=b_points(:,1);
         bpatch.vertices(b_vindeces(2),:)=b_points(:,2);
         bpatch.vertices(b_vindeces(3),:)=b_points(:,3);
         bpatch.vertices(b_vindeces(4),:)=b_points(:,1);
         b_n=cross(b_points(:,1)-b_points(:,2),b_points(:,1)-b_points(:,3));
         b_height=2;
         b_n=b_height*b_n./norm(b_n);

         b_vindeces=[1:3:10];
         bpatch.vertices(b_vindeces,:)=repmat(b_center+b_n,1,4)';
         b_vindeces=[3:3:12];
         bpatch.vertices(b_vindeces,:)=repmat(b_center-b_n,1,4)';
         PATCHES.vertices((d-1)*nverts+1:(d)*nverts,:)=bpatch.vertices;
         tmp=bpatch.vertices-repmat(b_center,1,nverts)';
         tmp=tmp./repmat(sqrt(sum(tmp.^2,2)),1,3);
         PATCHES_NORMALS((d-1)*nverts+1:(d)*nverts,:)=tmp;
     end;

    if num_bifurcation_groups>0
         patch(PATCHES,'FaceVertexCData',PATCHES.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',PATCHES_NORMALS);
         hide_crossmarker=true;
    end;
end;

if ~hide_crossmarker  
                for d=1:size(terminalsBIFPos,2)
                    p2=pssphere;
                   
                    
                        p2.vertices=bifurcation_sphere_scale*p2.vertices;
                   
                    posv=terminalsBIFPos(:,d)';
                    
                    p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);


                    color=crossmarker_color;
                    %color=[1,0.25,0.25];
                    %color=[0.25,1,0.25];
                    %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
                    patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);  
                    
                    
                    
                end;
    
                
                 if exist('draw_closepoints')
                 unum=unique([A.data(21,:),1]);
                 pointPos=[];
                 for a=1:numel(unum)
                     posA=A.data(1:3,A.data(21,:)==unum(a));
                     for b=a+1:numel(unum)
                         posB=A.data(1:3,A.data(21,:)==unum(b));
                         v=min(distmat(posA',posB'));   
                         [v,indx]=min(v);
                         if v<draw_closepoints^2
                             pointPos=[pointPos,posA(1:3,indx)];
                         end;
                     end;
                 end;
                    
                 for d=1:size(pointPos,2)
                    p2=pssphere;
                        p2.vertices=bifurcation_sphere_scale*p2.vertices;
                   
                    posv=pointPos(:,d)';
                    
                    p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
                    color=crossmarker_color;
                    
                    patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);  
                 end;
                end;                
                
    
    
%                 indx=find(carray>2);
%                 [Ix,Iy,Iz]=ind2sub(shape+1,indx);
%                 for d=1:numel(Ix)
%                             p2=pssphere;
%                             %scale=scarray(Ix(d),Iy(d),Iz(d));
%                             
%                             %p2.vertices=2.0*p2.vertices;
%                             p2.vertices=1.6*p2.vertices;
%                             posv=squeeze(pscarray(Ix(d),Iy(d),Iz(d),:))';
%                             %p2.vertices=p2.vertices+repmat([Iy(d),Ix(d),Iz(d)],size(pssphere.vertices,1),1);
%                             p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
%                             
%                             
%                             color=[1,0.25,0.25];
%                             %color=[0.25,1,0.25];
%                             %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
%                             patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);
%                     
%                 end;
end;                
                lighting phong

                drawnow;                
                
                
    case 7


                %if clear_figure,clf;end;
                %render_img(img,'threshold',isothreshold,'render_mode',render_mode,'render_dim',render_dim);
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;                
                
                hold on;
                shape=size(img);
                          %  carray=zeros(shape+1);
                %pscarray=zeros([shape+1,3]);
                
                
                switch edge_energy_option
                    case 1
                        edgecost=A.connections(5,:)+A.connections(6,:);
                    case 2
                        edgecost=A.connections(5,:);    
                    case 3
                        edgecost=A.connections(6,:);                            
                    case 4
                        edgecost=A.connections(7,:);    
                end;
                
                
                edgecost=sign(edgecost).*sqrt(abs(edgecost));
                
                edge_v_range=[min(edgecost),max(edgecost)];
                
                
                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        Ax=A.data(1,pointIDA)+1;
                        Ay=A.data(2,pointIDA)+1;
                        Az=A.data(3,pointIDA)+1;
                        
                        Adx=A.data(4,pointIDA);
                        Ady=A.data(5,pointIDA);
                        Adz=A.data(6,pointIDA);

                        Bx=A.data(1,pointIDB)+1;
                        By=A.data(2,pointIDB)+1;
                        Bz=A.data(3,pointIDB)+1;
                        
                          %  carray(ceil(Ax),ceil(Ay),ceil(Az))=carray(ceil(Ax),ceil(Ay),ceil(Az))+1;
                           % carray(ceil(Bx),ceil(By),ceil(Bz))=carray(ceil(Bx),ceil(By),ceil(Bz))+1;                        
                        
                        Bdx=A.data(4,pointIDB);
                        Bdy=A.data(5,pointIDB);
                        Bdz=A.data(6,pointIDB);
                        
                        As=0.8;%A.data(8,pointIDA);  
                        
                        Bs=0.8;%A.data(8,pointIDB);  
                       
                        px=(Ax+Bx)/2;
                        py=(Ay+By)/2;
                        pz=(Az+Bz)/2;

                         le=norm([Ax,Ay,Az]-[Bx,By,Bz])/2;
                        N0=([Ay,Ax,Az]-[By,Bx,Bz])';N0=N0./norm(N0);
                         
                         
                        N1=[0,0,1]';
                        N2=[Ady,Adx,Adz]';N2=N2./norm(N2);
                        if (dot(N0,N2)<0)
                            N2=-N2;
                        end;
                        M1=tranmat(N1,N2);
                  
                        
                        N3=[Bdy,Bdx,Bdz]';N3=N3./norm(N3);
                        if (dot(N3,N2)<0)
                            N3=-N3;
                        end;
                        M2=tranmat(N2,N3);
                
                        
                        p2=pcylinder2;
                        
                        p2.vertices(:,3)=0;
                         p2.vertices(1:2:end,1:2)=sfactor*As*p2.vertices(1:2:end,1:2);
                         p2.vertices(2:2:end,1:2)=sfactor*Bs*p2.vertices(2:2:end,1:2);

                        
                        v1=sta_matmult(p2.vertices(1:2:end,:)',M1)';
                        p2.vertices(1:2:end,:)=v1+repmat([Ay,Ax,Az],size(pcylinder2.vertices(1:2:end,:),1),1);    
                        
                        p2.vertices(2:2:end,:)=sta_matmult(p2.vertices(2:2:end,:)',M2*M1)'+repmat([By,Bx,Bz],size(pcylinder2.vertices(2:2:end,:),1),1);    
                         
                        
                        vnormals2=vnormals3;
                        vnormals2(1:2:end,:)=sta_matmult(vnormals3(1:2:end,:)',M1)';
                        vnormals2(2:2:end,:)=sta_matmult(vnormals3(2:2:end,:)',M2*M1)';
                        
                  

                        
                        
                        %carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))=carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))+1;%max(A.data(19:20,pointIDA)>1);
                       % carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))=carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))+1;%max(A.data(19:20,pointIDB)>1);%max(scarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5)),Ss);      
                        %carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))=carray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5))+max(A.data(19:20,pointIDA)>1);
                        %carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))=carray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5))+max(A.data(19:20,pointIDB)>1);%max(scarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5)),Ss);      
                       % pscarray(floor(Bx+0.5),floor(By+0.5),floor(Bz+0.5),:)=[Bx,By,Bz];
                       % pscarray(floor(Ax+0.5),floor(Ay+0.5),floor(Az+0.5),:)=[Ax,Ay,Az];
                        

                        
                        As=A.data(8,pointIDA);  
                        
                        Bs=A.data(8,pointIDB);  
                        
                        
                        
                        if (edgecost(d)>0)
                            if (edge_v_range(1)>0)
                                ecolor=(edgecost(d)-edge_v_range(1))/(edge_v_range(2)-edge_v_range(1)+eps);
                            else
                                ecolor=(edgecost(d))/(edge_v_range(2)+eps);
                            end;
                            %color=[ecolor,0.25*ecolor,0.25*ecolor];
                            color=[1,1-ecolor,1-ecolor];
                        else
                            if (edge_v_range(2)<0)
                                ecolor=(-edgecost(d)+edge_v_range(1))/(-edge_v_range(2)+edge_v_range(1)+eps);
                            else
                                ecolor=(-edgecost(d))/(-edge_v_range(1)+eps);
                            end;
                            %color=[0.25*ecolor,0.25*ecolor,ecolor];
                            color=[1-ecolor,1-ecolor,1];
                        end;
                        colorA=color;
                        colorB=color;
                        
                        p2.facevertexcdata=zeros(size(p2.vertices));
                        p2.facevertexcdata(1:2:end,:)=repmat(colorA,size(p2.vertices,1)/2,1);
                        p2.facevertexcdata(2:2:end,:)=repmat(colorB,size(p2.vertices,1)/2,1);
                      
                       
                       patch(p2,'FaceVertexCData',p2.facevertexcdata,'FaceColor','interp','EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                end     
                
if ~hide_crossmarker                  
                for d=1:size(terminalsBIFPos,2)
                    p2=pssphere;
                    p2.vertices=1.6*p2.vertices;
                    posv=terminalsBIFPos(:,d)';
                    
                    p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);


                   color=[0.25,0.25,0.25];
                    %color=[0.25,1,0.25];
                    %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
                    patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);  
                    
                end;
    
%                 indx=find(carray>2);
%                 [Ix,Iy,Iz]=ind2sub(shape+1,indx);
%                 for d=1:numel(Ix)
%                             p2=pssphere;
%                             %scale=scarray(Ix(d),Iy(d),Iz(d));
%                             
%                             %p2.vertices=2.0*p2.vertices;
%                             p2.vertices=1.6*p2.vertices;
%                             posv=squeeze(pscarray(Ix(d),Iy(d),Iz(d),:))';
%                             %p2.vertices=p2.vertices+repmat([Iy(d),Ix(d),Iz(d)],size(pssphere.vertices,1),1);
%                             p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
%                             
%                             
%                             color=[0.25,0.25,0.25];
%                             %color=[0.25,1,0.25];
%                             %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
%                             patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);
%                     
%                 end;
end;                
                lighting phong

                drawnow;        
                
                
case 8
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;
                hold on;
                
                
                
                
                for d=1:size(A.data,2)

                    
                    if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                        continue;
                    end;
                    
                    if ~((~bifurcations) || (terminalsBIFa(d)))
                        continue;
                    end;
                    
                    
                        px=A.data(1,d)+1;
                        py=A.data(2,d)+1;
                        pz=A.data(3,d)+1;

                        s=A.data(8,d);
                        
                        if (bifurcations)
                            s=1;
                        end;
                        
                        p2=pssphere;
                        
                        if s>-1
                            %p2.vertices=s*1.6*p2.vertices;
                            p2.vertices=s*1.6*p2.vertices;
                        else
                            p2.vertices=1.5*1.6*p2.vertices;
                        end;
                        posv=[px,py,pz];

                        p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);

                        
                        if (use_pathcolors)
                            color=colors{A.data(21,d)};
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);  
                        else
                             color=[1,0.25,0.25];
                            %patch(p2,'FaceColor',[1,0.25,0.25],'EdgeColor','none','FaceAlpha',1,'VertexNormals',vnormals2);
                            %patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherenormals2);  
                         end;
                        
                        
                        %color=[0.25,1,0.25];
                        if s>-1
                            patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',0.25,'VertexNormals',psvnormals);
                        else
                            patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',psvnormals);
                        end;
                        
                end;
                
              
                
                
                 lighting phong
                 drawnow             
                 
                 
case 9
                if new_scene
                    if clear_figure,clf;end;
                    render_img(img,img_render_options{:});
                end;
                hold on;
                
                shape=size(img);
if ~hide_costfunc      
                for d=1:size(A.connections,2)
                        pointIDA=A.connections(1,d)+1;
                        pointIDB=A.connections(2,d)+1;
                        
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,pointIDA))==0)   
                            continue;
                        end;
                        
                        connectedA=sum(A.data(19:20,pointIDA));
                        connectedB=sum(A.data(19:20,pointIDB));

                        if 1 %(connectedA>1)&&(connectedB>1)
                            pointIDA_side=A.connections(3,d);
                            pointIDB_side=A.connections(4,d);
                            centerMatA=A.data(3*pointIDA_side+[9,10,11],pointIDA)'+1;
                            centerMatB=A.data(3*pointIDB_side+[9,10,11],pointIDB)'+1;
                            
                            
                            Ax=A.data(1,pointIDA)+1;
                            Ay=A.data(2,pointIDA)+1;
                            Az=A.data(3,pointIDA)+1;

                            Bx=A.data(1,pointIDB)+1;
                            By=A.data(2,pointIDB)+1;
                            Bz=A.data(3,pointIDB)+1;
                            
                            As=A.data(8,pointIDA);
                            Bs=A.data(8,pointIDB);
                            
                            Cx=(Bx*As+Ax*Bs)/(As+Bs);
                            Cy=(By*As+Ay*Bs)/(As+Bs);
                            Cz=(Bz*As+Az*Bs)/(As+Bs);

                            Adx=centerMatA(1);
                            Ady=centerMatA(2);
                            Adz=centerMatA(3);
                            
                            
                            Bdx=centerMatB(1);
                            Bdy=centerMatB(2);
                            Bdz=centerMatB(3);
                            
                            col=[1,0,0];
                            plot3([Ady,Bdy],[Adx,Bdx],[Adz,Bdz],'color',col,'LineWidth',LineWidth);
                            
                            %col=colors{A.data(21,pointIDB)};
                            col=[0.3,1,0.3];
                            plot3([Ady,Cy],[Adx,Cx],[Adz,Cz],'color',col,'LineWidth',LineWidth);
                            col=[0.3,0.3,1];
                            plot3([Bdy,Cy],[Bdx,Cx],[Bdz,Cz],'color',col,'LineWidth',LineWidth);
                            
                            
                            %col=[0.9,0.9,0.9];%
                            col=[0.9,0.9,0];%
                            
                            plot3([Ay,By],[Ax,Bx],[Az,Bz],'color',col,'LineWidth',LineWidth);
                        end;
                        if mod(d,5000)==0;
                            drawnow;
                        end;
                end
                
                 count=0;
                for d=1:size(A.data,2)
 
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if (A.data(end,d)==1)
                        
                        count=count+1;
                        
                        Ax=A.data(9,d)+1;
                        Ay=A.data(10,d)+1;
                        Az=A.data(11,d)+1;
                        
                        Bx=A.data(12,d)+1;
                        By=A.data(13,d)+1;
                        Bz=A.data(14,d)+1;
                        
                        col=[0.6,0.6,0.6];
                        %col=[0,1,0];
                        plot3([Ay,By],[Ax,Bx],[Az,Bz],'color',col,'LineWidth',LineWidth);
                        
                    end;
                end;
end;

if ~hide_crossmarker      
               for d=1:size(A.data,2)
 
                        if (min(select_tree)>-1)&&(max(select_tree==A.data(21,d))==0)  
                            continue;
                        end;
                    
                    if (A.data(end,d)==1)
                        
                        
                        
                        
                        if hide_costfunc
                            Ax=A.data(1,d)+0.25;
                        Ay=A.data(2,d)+0.25;
                        Az=A.data(3,d)+0.25;

                        posv=[Ax,Ay,Az];
                         p2=ps;
                             p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
                             color=[1,0.25,0.25];
                        else
                            Ax=A.data(1,d)+0.75;
                        Ay=A.data(2,d)+0.75;
                        Az=A.data(3,d)+0.75;

                        posv=[Ax,Ay,Az];
                         p2=ps;
                            p2.vertices=0.5*p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
                            color=[0.1,0.1,0.1];
                        end;
                        patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherevnormals);  

                    
                    %    col=[0.1,0.1,0.1];
                    %    plot3([Ay,By],[Ax,Bx],[Az,Bz],'color',col,'LineWidth',LineWidth);
                        
                    end;
                end;
    
%                 for d=1:size(terminalsBIFPos,2)
%                     p2=ps;
%                     p2.vertices=1.6*p2.vertices;
%                     posv=terminalsBIFPos(:,d)';
%                     
%                     p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
% 
% 
%                     color=[1,0.25,0.25];
%                     patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherevnormals);  
%                     
%                 end;
    
end;                
                
                
                    lighting phong
                   drawnow;
                
                   
end;



if exist('pointpos') && (mode~=3)
%         if mode==3
%                    for a=1:3
%                            subplot(2,2,a);
% 
%                                    for d=1:size(pointpos{1},1)
%                                         p2=pssphere;
%                                         p2.vertices=5*p2.vertices;
%                                         posv=pointpos{1}(d,:);
%                                         p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
%                                         color=[0.85,0.85,0.85];
%                                         patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherevnormals);  
%                                    end;
% 
% 
%                    end;
%         else

                           for d=1:size(pointpos{1},1)
                                p2=pssphere;
                                p2.vertices=5*p2.vertices;
                                posv=pointpos{1}(d,:);
                                p2.vertices=p2.vertices+repmat(posv([2,1,3]),size(pssphere.vertices,1),1);
                                color=[0.85,0.85,0.85];
                                patch(p2,'FaceColor',color,'EdgeColor','none','FaceAlpha',1,'VertexNormals',smallspherevnormals);  
                           end;

                   lighting phong
%         end;
end;     

if isfield(A,'total_num_iter') && (showtext)
    if mode==3
           for a=1:3
                   subplot(2,2,a);
                        textstring={['iterations: ',num2str(A.total_num_iter)],...
                                    ['time: ',num2str(ceil(A.total_iteration_time)),' sec']};
                        textstring={[,num2str(A.total_num_iter),' (iter) / ',num2str(ceil(A.total_iteration_time)),' (sec)']};
                        %text(0.5, 0.5, textstring, 'horizontalAlignment', 'center','BackgroundColor',[.7 .9 .7],'EdgeColor',[0,0,0],'LineWidth',1,'FontSize',10,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
                        %text(0.5, 0.5, textstring, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',10,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
                        xlabel(textstring);
                        

           end;
    else
        

        
        
        if (intitle>0)
%         textstring={['iterations: ',num2str(A.total_num_iter)],...
%                     ['time: ',num2str(ceil(A.total_iteration_time)),' sec']};
        textstring={[,num2str(A.total_num_iter),' (iter) / ',num2str(ceil(A.total_iteration_time)),' (sec)'],...
                    [num2str(size(A.data,2)),' particles, ',num2str(size(A.connections,2)),' edges']};
                
        %title(textstring, 'FontSize', intitle);
           % annotation('textbox', [.1 .1 .1 .1],'String',textstring, 'FontSize', intitle);
           shape=size(img);
           shape=[shape(2),shape(1),shape(3)];
           shape=permute(shape,pmute);
%             omax=0;
%                 for r=1:numel(textstring)
%                     omax=max(omax,textstring{r});
%                 end;
%                 offset=intitle*[omax/2,numel(textstring)];
              %text(shape(1)/2,shape(2)/2,shape(3)/2, textstring, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',intitle,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
              
%                VDIR=camtarget-campos;VDIR./norm(VDIR);
%                 VDIR=campos+VDIR;
                
              %adata=axis;  
              h=gcf;  
              ha=axes('parent',h, 'HitTest','off','color','none');
              axis(ha,'off');
              %axis(ha,[adata]);
              %daspect(ha,[1,1,1]);
              
              
              text(textP(1),textP(2),0,textstring,'parent',ha, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',intitle,'FontUnits','normalized','margin',1,'VerticalAlignment','bottom','HorizontalAlignment','left','color',textcolor,'FontName','Times');
              %text(textP(1),textP(2),0,textstring,'parent',ha, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',intitle,'FontUnits','normalized','margin',2,'VerticalAlignment','bottom','HorizontalAlignment','left','color',textcolor,'FontName','Times New Roman');
              
              %plot(ha,[1,2,34])
              %text(ha,5,5, textstring, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',intitle,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
              %text(h,VDIR(1),VDIR(2),VDIR(3), textstring, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',intitle,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
              
              %, 'FontSize', intitle
        else
        textstring={['iterations: ',num2str(A.total_num_iter)],...
                    ['time: ',num2str(ceil(A.total_iteration_time)),' sec']};
               
        textstring={[,num2str(A.total_num_iter),' (iter) / ',num2str(ceil(A.total_iteration_time)),' (sec)'],...
                    [num2str(size(A.data,2)),' particles, ',num2str(size(A.connections,2)),' edges']};
        %text(0.5, 0.5, textstring, 'horizontalAlignment', 'center','BackgroundColor',[.7 .9 .7],'EdgeColor',[0,0,0],'LineWidth',1,'FontSize',10,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
%         VDIR=camtarget-campos;VDIR./norm(VDIR);
%         campos+
            text(0.5, 0.5, textstring, 'horizontalAlignment', 'center','BackgroundColor','none','EdgeColor','none','LineWidth',2,'FontSize',10,'FontUnits','normalized','margin',5,'VerticalAlignment','bottom','HorizontalAlignment','left');
        end;
    end;
end;

if (render_mode~=2)&&(render_mode~=4)
    material([0.4 0.2 0.5])
end;


function vnormals=compute_vnormals(pcylinder)

               % fprintf('precomputing normals ...');
                
                vnormals=zeros(size(pcylinder.vertices));
                for a=1:size(pcylinder.faces,1)
                    %idx=pcylinder.face()
                    %snormal=cross(pcylinder.vertices(pcylinder.faces(a,1),:),pcylinder.vertices(pcylinder.faces(a,2),:));
                    n1=pcylinder.vertices(pcylinder.faces(a,2),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n1=n1/norm(n1);
                    n2=pcylinder.vertices(pcylinder.faces(a,4),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n2=n2/norm(n2);
                    snormal=cross(n1,n2);
                    if sum(isnan(snormal(:)))==0
                    for b=1:4
                        vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+snormal;
                    end;
                    end;
                end;
                vnormals=vnormals./repmat(eps+sqrt(sum(vnormals.^2,2)),1,3);
                
                %fprintf('done\n');
                
function vnormals=compute_vnormals2(pcylinder,makeflat)

               % fprintf('precomputing normals ...');
                
                vnormals=zeros(size(pcylinder.vertices));
                for a=1:size(pcylinder.faces,1)
                    %idx=pcylinder.face()
                    %snormal=cross(pcylinder.vertices(pcylinder.faces(a,1),:),pcylinder.vertices(pcylinder.faces(a,2),:));
                    n1=pcylinder.vertices(pcylinder.faces(a,2),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n1=n1/norm(n1);
                    n2=pcylinder.vertices(pcylinder.faces(a,4),:)-pcylinder.vertices(pcylinder.faces(a,1),:);
                    n2=n2/norm(n2);
                    snormal=cross(n1,n2);
                    if sum(isnan(snormal(:)))==0
                        for b=1:4
                            vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+snormal;
                        end;
                    else
                        for b=1:4
                            vnormals(pcylinder.faces(a,b),:)=vnormals(pcylinder.faces(a,b),:)+[0,0,-1];
                        end;
                    end;
                end;
                vnormals=vnormals./repmat(eps+sqrt(sum(vnormals.^2,2)),1,3);                
                
                 DM=sqrt(distmat(pcylinder.vertices,pcylinder.vertices));
                 
                 
                    checked=zeros(1,size(DM,1));
                     for a=1:size(DM,1)
                         if ~checked(a)
                                 sel=find(DM(a,:)<0.0001);
                                 if numel(sel)>1
                                     newn=mean(vnormals(sel,:));
                                     %checked(sel)=true;
                                     newn=newn./(norm(newn)+eps);
                                     vnormals(sel,:)=repmat(newn,numel(sel),1);
                                 end;
                         end;
                     end;   
                     
if makeflat                     
                     for n=1:size(vnormals,1)
                         vnormals(n,:)=vnormals(n,:)+5*dot(vnormals(n,:),[0,0,1])*[0,0,1];
                         vnormals(n,:)=vnormals(n,:)./norm(vnormals(n,:)+eps);
                     end;
end;                
                
% 
                 %    fprintf('done\n');
                
                

                    
                   