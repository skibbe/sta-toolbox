 
function [R]=tranmat(N1,N2)

                    
                        N1=N1./norm(N1);
                        N2=N2./norm(N2);
                        t=dot(N1,N2);
                        if t>0.9999
                            R=[1,0,0;0,1,0;0,0,1];
                        else
%                             N3=cross(N1,N2);
%                             N3=N3./norm(N3);
%                             %R=[N1,N3,cross(N3,N1)];
%                             R=[cross(N3,N1),N3,N1];

                              axis=cross(N1,N2);
                              
                            axis=axis./norm(axis);
                            ux=axis(1);
                            uy=axis(2);
                            uz=axis(3);

                            ct=t;
                            st=sqrt(1-t^2);
                            %ct=cos(t);
                            %st=sin(t);

                            R=[ct+ux^2*(1-ct) , ux*uy*(1-ct)-uz*st , ux*uz*(1-ct)+uy*st;    
                               uy*ux*(1-ct)+uz*st , ct+uy^2*(1-ct) , uy*uz*(1-ct)-ux*st;    
                               uz*ux*(1-ct)-uy*st , uz*uy*(1-ct)+ux*st , ct+uz^2*(1-ct)];


                        end;
                        