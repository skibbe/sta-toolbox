%mex CXXFLAGS="-fPIC  -fexceptions -O2  -Wall -Wno-unknown-pragmas -march=native -fopenmp "  CFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "   sta_EVGSL.cc -l gsl -l gslcblas -l gomp
%mex CXXFLAGS="-fPIC  -fexceptions -O2  -Wall  -Wno-unknown-pragmas -march=native -fopenmp -std=c++11"  CFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp -std=c++11 "   sta_gradorientdist.cc -l gsl -l gslcblas -l gomp

%mex CXXFLAGS="  -std=c++11 -fPIC -march=native  -Wno-unknown-pragmas"  CFLAGS="  -std=c++11 -fPIC -march=native "   sta_orientdist.cc -l gsl -l gslcblas  -l gomp

%mex  LDOPTIMFLAGS="-O -static-libgcc -static-libstdc++  -Wno-unknown-pragmas"  CXXFLAGS="-std=c++11 -fPIC -march=native -DSCALENORMAL"  CFLAGS="-DSCALENORMAL -std=c++11 -fPIC -march=native"  ntrack.cc -l gsl -l gslcblas -l gomp

%mex  LDOPTIMFLAGS="-O3 -static-libgcc -static-libstdc++  "  CXXFLAGS="-std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"  CFLAGS=" -std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"  ntrack.cc -l gsl -l gslcblas  -l gomp 
%mex  LDOPTIMFLAGS="-O3 -static-libgcc -static-libasan  -static-libstdc++  -Wno-unknown-pragmas"  CXXFLAGS="-static-libasan -fsanitize=address -std=c++11 -fopenmp-simd -fopenmp -fPIC -march=native"  CFLAGS="-static-libasan -fsanitize=address  -std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"  ntrack.cc -l gsl -l gslcblas -l gomp
 

%mex  LDOPTIMFLAGS="-O -static-libgcc -static-libstdc++  -Wno-unknown-pragmas"  CXXFLAGS="-std=c++11 -fPIC -march=native -DSCALENORMAL -D_DEBUG_CONNECT_LOOP_EXTRA_CHECK"  CFLAGS="-DSCALENORMAL -D_DEBUG_CONNECT_LOOP_EXTRA_CHECK -std=c++11 -fPIC -march=native"  ntrack.cc -l gsl -l gslcblas -l gomp


%mex  LDOPTIMFLAGS="-O -static-libgcc -static-libstdc++  -Wno-unknown-pragmas"  CXXFLAGS="-std=c++11 -fPIC -march=native -DSCALENORMAL"  CFLAGS="-DSCALENORMAL -std=c++11 -fPIC -march=native"  mhs_track.cc -l gsl -l gslcblas -l gomp




mex  mhs_data2imgC.cc -I../ntracker_c/
mex  mhs_data2imgCnew.cc -I../ntracker_c/ -largeArrayDims
%mex  LDOPTIMFLAGS="-O3 -static-libgcc -static-libstdc++  "  CXXFLAGS="-std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"  CFLAGS=" -std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"  ntrack_dataeval.cc -l gsl -l gslcblas  -l gomp 