function render_img(img,varargin)


alpha=0.25;
reduce=2;
threshold=0.2;
render_mode=2;
render_dim=3;
invert_color=false;
imgzposition=0;
FaceColor=0.2*[1,1,1];
slice_select=-1;
slice_select_pos=-1;
clear_bg=false;
addlight=true;  
element_size=[1,1,1];
for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;
full_img=(numel(img)>3);

if full_img
    shape=(size(img));
else
    shape=img;
end;


override_axis_shape=shape;
override_imageoffset=[0,0,0];
for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

    
    

switch (render_mode)
    case 0
                        
                        set(gcf, 'Renderer','OpenGL'); 
                        set (gca, 'Clipping', 'on','DrawMode','fast');
                        
    
                        switch(render_dim)
                            case 1
                                axis([1 override_axis_shape(2) 1 override_axis_shape(1) 1 override_axis_shape(3)]);
                                daspect([1,1,1])
                                colormap(gray(100))

                               
                                if full_img
                                    planeimg = double(real(squeeze(max(img,[],2))))*255;

                                    if (invert_color)
                                        planeimg=255-planeimg;
                                    end;
                                    imgzposition = shape(2)*2;

                                    if ~clear_bg
                                        surface([override_imageoffset(2)+1 override_imageoffset(2)+shape(2)],[override_imageoffset(3)+1 override_imageoffset(3)+shape(3)],repmat(60, [2 2]),...
                                        planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',0.6,'FaceLighting','none','CDataMapping','direct');
                                    end;
                                end;
                                lighting phong
                                axis off

                                %view(0,0)
                                view(90,-90)
                                lightangle(0,90)
                                axis tight
                                
                            case 3
                                axis([1 override_axis_shape(2) 1 override_axis_shape(1) 1 override_axis_shape(3)]);
                                daspect([1,1,1])
                                colormap(gray(100))

                                if full_img
                                     if slice_select>0
                                        planeimg = real(squeeze(img(:,:,slice_select)));
                                        planeimg = planeimg  -  min(planeimg(:));
                                        planeimg = planeimg  ./ max(planeimg(:));
                                        planeimg = double(planeimg)*255;
                                     else
                                        planeimg = double(real(squeeze(max(img,[],3))))*255;
                                     end;
                                    if (invert_color)
                                        planeimg=255-planeimg;
                                    end;
                                    imgzposition = shape(3)*2;

                                    if ~clear_bg
                                    if slice_select_pos>0
                                        % plot the image plane using surf.
                                        surf([override_imageoffset(2)+1 override_imageoffset(2)+shape(2)],[override_imageoffset(1)+1 override_imageoffset(1)+shape(1)],repmat(slice_select, [2 2]),...
                                            planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',0.6,'FaceLighting','none');
                                    else
                                        surf([override_imageoffset(2)+1 override_imageoffset(2)+shape(2)],[override_imageoffset(1)+1 override_imageoffset(1)+shape(1)],repmat(imgzposition, [2 2]),...
                                            planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',0.6,'FaceLighting','none');
                                    end;
                                    end;
                                end;

                                lighting phong
                                axis off

                                view(0,90)
                                lightangle(0,90)
                                axis tight
                        end;
    case 1
       %imshow(real(squeeze(max(img,[],3))),'InitialMagnification','fit');
                        set(gcf, 'Renderer','OpenGL'); 
                        %set(gcf, 'Renderer','painters'); 
                        set (gca, 'Clipping', 'on','DrawMode','fast');
                        %shape=(size(img));
                        axis([1 override_axis_shape(2) 1 override_axis_shape(1) 1 override_axis_shape(3)]);
                        daspect([1,1,1])
                        colormap(gray(100))
                        
                        if full_img
                            if slice_select>0
                                planeimg = real(squeeze(img(:,:,slice_select)));
                                planeimg = planeimg  -  min(planeimg(:));
                                planeimg = planeimg  ./ max(planeimg(:));
                                planeimg = double(planeimg)*255;
                            else
                                planeimg = double(real(squeeze(max(img,[],3))))*255;
                            end;
                            if (invert_color)
                                        planeimg=255-planeimg;
                            end;
                            imgzposition = -shape(3)*2;
                            
%                             pimg=zeros(override_axis_shape([2,1]));
%                             pimg(override_imageoffset(1)+1:override_imageoffset(1)+shape(1),override_imageoffset(2)+1:override_imageoffset(2)+shape(2))=planeimg;
%                             planeimg=pimg;
                            
                            if ~clear_bg
                            if slice_select_pos>0
                                % plot the image plane using surf.
                                %
                                %surf([1 shape(2)],[1 shape(1)],repmat(slice_select, [2 2]),...
                                surf([override_imageoffset(2)+1 override_imageoffset(2)+shape(2)],[override_imageoffset(1)+1 override_imageoffset(1)+shape(1)],repmat(slice_select, [2 2]),...
                                    planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',0.9999,'FaceLighting','none');
                                    %planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',1,'FaceLighting','none');
                            else
                                %surf([1 shape(2)],[1 shape(1)],repmat(imgzposition, [2 2]),...
                                    surf([override_imageoffset(2)+1 override_imageoffset(2)+shape(2)],[override_imageoffset(1)+1 override_imageoffset(1)+shape(1)],repmat(slice_select, [2 2]),...
                                    planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',0.9999,'FaceLighting','none');
                            end;
                            end;
                        end;
                        
                        %lh=camlight; 
                        lighting phong
                       % axis([1 shape(2) 1 shape(1) 1 shape(3)]);
                        axis off
                        
                        view(0,90)
                        %camlight; 
                        lightangle(0,90)
                         axis tight
                        % axis([1 override_axis_shape(2) 1 override_axis_shape(1) 1 override_axis_shape(3)]);
                        %material([0.4 0.2 0.5])
                        %lightangle(lh,0,90)

    case 2



    % if nargin>1
    %     figure(handle)
    % else
    %     clf
    % end;
    set(gcf, 'Renderer','OpenGL'); 
    %set (gca, 'Clipping', 'on','HitTest','off','DrawMode','fast');
    %set (gca,'HitTest','off','DrawMode','fast');
    %set (gca, 'Clipping', 'on','DrawMode','fast');
    set (gca, 'Clipping', 'on');
    %

    % 

    if full_img
        if reduce>0
            img = smooth3(img);
            [x,y,z,D] = reducevolume(img,reduce*[1,1,1]);
            isurf=isosurface(x,y,z,D, threshold,'noshare');
        else
            D=img;
            isurf=isosurface(D, 0.1,'noshare');
        end;
    
%         p0 = patch(isurf,'FaceColor',FaceColor,...
%                  'EdgeColor','none','FaceAlpha',alpha);    
%         
%         N=isonormals(D,p0);
%         N
        %delete(p0);
        
        %isurf0=isurf;
        
        
        
        %isurf.vertices=isurf.vertices+repmat(override_imageoffset([2,1,3]), size(isurf.vertices,1),1);
%         
%               
%          p1 = patch(isurf,'FaceVertexCData',isurf.vertices,'FaceColor',FaceColor,...
%                  'EdgeColor','none','FaceAlpha',alpha,'VertexNormals',N);    

        if sum(element_size==[1,1,1])~=3
            isurf.vertices=repmat(element_size,size(isurf.vertices,1),1);
        end;

        p1 = patch(isurf,'FaceColor',FaceColor,...
               'EdgeColor','none','FaceAlpha',alpha);    
            
        
            
%          p1 = patch(isurf,'FaceColor',FaceColor,...
%                 'EdgeColor','none','FaceAlpha',alpha);
    end;
    %     D=img;
    %   p1 = patch(isosurface(D, 0.2),'FaceColor',0.2*[1,1,1],...
    %  	'EdgeColor','none','FaceAlpha',0.25);


    %p2 = patch(isocaps(D, 5),'FaceColor','interp',...
        %'EdgeColor','none');
    %view(3);
    %camup([0,1,1])
    view([1,0,10]);


     %az = -90;
     %el = 90;
     %view(az, el);

     %camup([0,-1,1])
    % 
    % az = 90;
    % el = 90;
    % view(az, el);

    % az = 90;
    % el = 180;
    % view(az, el);


    %axis tight; 
    %shape=(size(img));
    axis([1 override_axis_shape(2) 1 override_axis_shape(1) 1 override_axis_shape(3)]);
    daspect([1,1,1])
    colormap(gray(100))
    %camlight left;
    if addlight
        camlight; 
    end;
    %camlight HEADLIGHT 
    lighting phong
    %lighting gouraud
    %lighting flat

    if full_img
        %isonormals(D,isurf.vertices);
        
        
        %isonormals(X-override_imageoffset(1),Y-override_imageoffset(2),Z-override_imageoffset(3),D,p1);
        isonormals(D,p1);
        
        vert=get(p1,'Vertices');
        if numel(vert)>0
            set(p1,'Vertices',vert+repmat(override_imageoffset([2,1,3]), size(vert,1),1));
        end;
        
        
%         vert=get(p1,'XData');
%         set(p1,'XData',vert+override_imageoffset(2));
%         vert=get(p1,'YData');
%         set(p1,'YData',vert+override_imageoffset(1));
%         vert=get(p1,'ZData');
%         set(p1,'ZData',vert+override_imageoffset(3));
        %vert=get(p1,'Vertices');
        %isonormals(D,vert);
        %isonormals(D,vert-repmat(override_imageoffset([2,1,3]), size(vert,1),1));
    end;
    axis off

    camup([0,-1,0])
    %toc
    
    case 3
        if full_img
                        imshow(real(squeeze(max(img,[],3))),'InitialMagnification','fit');
        else
            warning('without an image, this rendermode is not really well supported :(');
        end;

    case 4
        error('update ovveride options')
                        %imshow(real(squeeze(max(img,[],3))),'InitialMagnification','fit');
                        set(gcf, 'Renderer','OpenGL'); 
                        set (gca, 'Clipping', 'on','DrawMode','fast');
                        
                        
                        if full_img
                            planeimg = double(real(squeeze(img(:,:,imgzposition))))*255;
                            if (invert_color)
                                planeimg=255-planeimg;
                            end;
                            % plot the image plane using surf.
                            surf([1 shape(2)],[1 shape(1)],repmat(imgzposition, [2 2]),...
                                planeimg,'facecolor','texture','AmbientStrength',1,'FaceAlpha',1,'FaceLighting','none');

                        end;
                        view([1,0,10]);
                        axis([1 shape(2) 1 shape(1) 1 shape(3)]);
                        daspect([1,1,1])
                        colormap(gray(100))
                        camlight;                         

                        lighting phong
                        axis off

                        view(0,90)
                        camup([0,-1,0])
                        %lightangle(0,90)
                        %axis tight

end;



if exist('vpos')
    if numel(vpos)>1
    view(vpos);
    end;
end;

