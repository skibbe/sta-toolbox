function mhs_setPaths
[pathstr, name, ext]=fileparts([mfilename('fullpath'),'.m']);
addpath(pathstr);
addpath([pathstr,'/matlab_help']);
addpath([pathstr,'/ntracker_matlab']);
addpath([pathstr,'/ntracker_c']);
addpath([pathstr,'/graph_vis']);
addpath([pathstr,'/graph_tools']);
addpath([pathstr,'/graph_import']);
addpath([pathstr,'/graph_simulate']);
addpath([pathstr,'/test/']);
%addpath([pathstr,'/cuda/']);
%addpath([pathstr,'/paper_revision']);