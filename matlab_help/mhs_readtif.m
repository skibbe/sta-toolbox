function [img,RGB]=mhs_readtif(fname,progress,remap)

if (nargin<2)
    progress=0;
end;

if (nargin<3)
    remap=true;
end;

switch progress
    case 1
        h = waitbar(0,'loading image'); 
    otherwise
        
end;

info = imfinfo(fname);
num_images = numel(info);
RGB=false;

channels=-1;
numslices=-1;
%has imageJ image tags

try 
    if isfield(info,'ImageDescription')

        numChannels=info.SamplesPerPixel;
        numImagesStr = regexp(info.ImageDescription, 'images=(\d*)', 'tokens');
        numImages = str2double(numImagesStr{1}{1});
        % Use low-level File I/O to read the file
        fp = fopen(fname , 'rb');
        % The StripOffsets field provides the offset to the first strip. Based on
        % the INFO for this file, each image consists of 1 strip.
        fseek(fp, info.StripOffsets*numChannels, 'bof');
        % Assume that the image is 16-bit per pixel and is stored in big-endian format.
        % Also assume that the images are stored one after the other.
        % For instance, read the first 100 frames

      assert(numChannels==1);

      img=zeros([info.Width,info.Height,numImages],'single');
            for d = 1:numImages
                %tmp = fread(fp, [numChannels*info.Width*info.Height], 'uint16', 0, 'ieee-be')';
                tmp = fread(fp, [info.Width*info.Height], 'uint16', 0, 'ieee-be')';
                img(:,:,d)=reshape(tmp,[info.Width,info.Height]);
                %fprintf('%d %d\n',d,numImages);
            end

        fclose(fp);
        if remap
            %img=permute(img,[2,1,3]);
            img=(img(end:-1:1,:,:));
            %@(x)(x(end:-1:1,:,:)/2^12)
        end;
        
        return;
    end;
catch
    
end;

tmp=myreadtif(fname);
    img=tmp{1};

if remap
    img=permute(img,[2,1,3]);
    %img=(img(end:-1:1,:,:));
end;
