function Img=mhs_local_noramlize_img(Img,varargin)

    sigma=3;
    element_size=[1,1,1];
    crop=[0,0,0];
    epsilon=0.1;
    


   for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;


[i_stdv,i_mean]=mhs_compute_std_mask(Img,'sigman',sigma,'epsilon',epsilon,'crop',crop,'inv',true,'element_size',element_size);
Img=(Img-i_mean).*i_stdv;