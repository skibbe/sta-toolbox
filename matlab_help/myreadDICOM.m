function Img=myreadDICOM(folder)


fi=dir([folder,'/*.DCM']);
X = dicominfo([folder,'/',fi(1).name]);

Y = dicomread([folder,'/',fi(1).name]);

Img=zeros([size(Y),numel(fi)],'single');

for a=1:numel(fi)
    if mod(a,5)==0
        fprintf('.');
    end;
    Img(:,:,a)=single(dicomread([folder,'/',fi(a).name]))/single(2^X.BitDepth);
end;
fprintf('\n');

