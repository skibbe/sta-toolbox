function I=mhs_img2_ownscale2_max(I)


shape=size(I);
shape2=2*floor(shape/2);
if numel(shape)==3
    I=max(I(1:2:shape2(1),:,:),I(2:2:shape2(1),:,:));
    I=max(I(:,1:2:shape2(2),:),I(:,2:2:shape2(2),:));
    I=max(I(:,:,1:2:shape2(3)),I(:,:,2:2:shape2(3)));
else
    I=max(I(1:2:shape2(1),:),I(2:2:shape2(1),:));
    I=max(I(:,1:2:shape2(2)),I(:,2:2:shape2(2)));
end