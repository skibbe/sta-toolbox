function I=mhs_img_downscale2_median(I,p)


if nargin<2
    p=0.5;
end;

assert(p>0);
assert(p<1);

fact=cast(1/8,class(I));
shape=size(I);


if numel(shape)==3
  shape2=2*floor(shape/2);
  tmp=zeros([8,shape2/2],class(I));
  tmp(1,:,:,:)=I(1:2:shape2(1),1:2:shape2(2),1:2:shape2(3));
  tmp(2,:,:,:)=I(2:2:shape2(1),1:2:shape2(2),1:2:shape2(3));
  tmp(3,:,:,:)=I(1:2:shape2(1),2:2:shape2(2),1:2:shape2(3));
  tmp(4,:,:,:)=I(1:2:shape2(1),1:2:shape2(2),2:2:shape2(3));
  tmp(5,:,:,:)=I(2:2:shape2(1),2:2:shape2(2),1:2:shape2(3));
  tmp(6,:,:,:)=I(1:2:shape2(1),2:2:shape2(2),2:2:shape2(3));
  tmp(7,:,:,:)=I(2:2:shape2(1),1:2:shape2(2),2:2:shape2(3));
  tmp(8,:,:,:)=I(2:2:shape2(1),2:2:shape2(2),2:2:shape2(3));
  
  if p==0.5
    I=squeeze(median(tmp,1));
  else
      tmp=sort(tmp,1);
      
      indx=round(7*(1-p))+1;
      I=squeeze(tmp(indx,:,:,:));
  end
else
  shape2=2*floor(shape/2);
  tmp=zeros([4,shape2/2],class(I));
  tmp(1,:,:,:)=I(1:2:shape2(1),1:2:shape2(2));
  tmp(2,:,:,:)=I(2:2:shape2(1),1:2:shape2(2));
  tmp(3,:,:,:)=I(1:2:shape2(1),2:2:shape2(2));
  tmp(4,:,:,:)=I(2:2:shape2(1),2:2:shape2(2));
  
  if p==0.5
    I=squeeze(median(tmp,1));
  else
      tmp=sort(tmp,1);
      
      indx=round(7*(1-p))+1;
      I=squeeze(tmp(indx,:,:,:));
  end   
end;