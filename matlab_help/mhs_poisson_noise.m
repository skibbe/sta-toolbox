function img=mhs_poisson_noise(img,lscale)
    img=poissrnd(img*lscale)/lscale;

