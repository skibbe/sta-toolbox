% S=mhs_data_stat('./sim7_v4/');
% sortme=[1,3,4,5,6,7,8,9,2];
% S(sortme,:)
function S=mhs_data_stat(folder)



files=dir([folder,'/*.mat']);

f={};
count=1;
for a=1:numel(files)
    if isempty(strfind(files(a).name,'iso')) && isempty(strfind(files(a).name,'result'))
        f{count}=files(a).name;
        count=count+1;
    end;
end;


S=[];
for a=1:numel(f)
    fprintf('%s\n',f{a});  
    data=load([folder,'/',f{a}]);
    %S{a}=mhs_graph_stats(data.data.GT);
    S=[S;mhs_graph_stats(data.data.GT)];
end;