function I=mhs_img_downscale2_min(I)


shape=size(I);
shape2=2*floor(shape/2);

if numel(shape2)==3
    I=min(I(1:2:shape2(1),:,:),I(2:2:shape2(1),:,:));
    I=min(I(:,1:2:shape2(2),:),I(:,2:2:shape2(2),:));
    I=min(I(:,:,1:2:shape2(3)),I(:,:,2:2:shape2(3)));
else
    I=min(I(1:2:shape2(1),:),I(2:2:shape2(1),:));
    I=min(I(:,1:2:shape2(2)),I(:,2:2:shape2(2)));
end