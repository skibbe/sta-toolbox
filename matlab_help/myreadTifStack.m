function Img=myreadTifStack(folder,filter)


if nargin<2
    filen=dir([folder,'/*.tif']);
else
    filen=dir([folder,'/',filter,'.tif']);
end;


name = {filen.name};
str  = sprintf('%s#', name{:});
if nargin<2
    num  = sscanf(str, '%d.tif#');
    [dummy, index] = sort(num);
else
    index=[1:numel(filen)];
    
end;

%name = name(index);


for a=1:numel(filen)
    fname=[folder,'/',filen(index(a)).name];
    fprintf('reading %s\n',fname);
    tmp=myreadtif(fname);
    if (a==1)
        numchannels=numel(tmp);
        if numchannels==1
            img=tmp{1};
            shape=[size(img,1),size(img,2),numel(filen)];
            Img=zeros(shape,'single');
        else
            for b=1:numchannels
                img=tmp{b};
                shape=[size(img,1),size(img,2),numel(filen)];
                Img{b}=zeros(shape,'single');
            end;
        end;
    end;
    if numchannels==1
            Img(:,:,a)=tmp{1};
    else
        for b=1:numchannels
           Img{b}(:,:,a)=tmp{b}; 
        end
    end;
end