%%


function [V2,V3]=vesselness3D(img,varargin)


if 0
    %%
    count=1;
    for C=[0.03]
    for A=[0.5]
        for B=[0.1,0.2,0.3]
            

                V1=vesselness3D(Img,'Scales',[1.25,1.25^2,1.25^3,1.25^4,1.25^5],'a',A,'b',B,'c',C,'STD',false);
                
                if count==1
                    hdf5write('test2.h5',['Img-c',num2str(C),'-a',num2str(A),'-b',num2str(B)],squeeze(max(V1,[],4)));   
                else
                    hdf5write('test2.h5',['Img-c',num2str(C),'-a',num2str(A),'-b',num2str(B)],squeeze(max(V1,[],4)),'WriteMode', 'append');   
                end;
            count=count+1;
        end;
    end;
    end;
    
end;

crop=[0,0,0];
epsilon=0.01;
threshold=0.1;


Scales_fac=1.5;
poldeg=4;
ndensescales=5;
sigman=-1;
STD=false;
sigma_r=2;
sigma_v=2;
a=0.5;b=0.5;c=0.25;
mode=3;
tube_steps=1;
tube_length=0;
circle_mode=false;
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    
if ~exist('poldeg_vdir')
    poldeg_vdir=poldeg;
end

    
    
    classid=class(img);
    threshold=cast(threshold,classid);
    Scales=cast(Scales,classid);
     nscales=numel(Scales);


    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

if STD
    if (sigman==-1)
        sigman=1.5*max(Scales);
    end;
    mean=mhs_smooth_img(img,sigman,'normalize',true);
    mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
    std_dev=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
    std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
end;    

% COMPUTING HESSIAN
    for sindx=1:nscales
        scale=Scales(sindx);
        %HField=scale^2*Hessian(img,scale,'crop',crop,'normalize',true);
        %HField=Hessian(img,scale,'crop',crop,'normalize',true);
        if STD
            HField=scale^2*reshape(repmat(std_dev(:)',6,1),[6,cshape]).*Hessian(img,scale,'crop',crop,'normalize',true);
        else
            HField=scale^2.*Hessian(img,scale,'crop',crop,'normalize',true);
        end;
        if nargout>1
        switch (mode)
            case 1
                if STD
                    GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
                else
                    GField=Grad(img,scale,'crop',crop,'normalize',true);
                end;
            case 3
                if STD
                   GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,scale,'crop',crop,'normalize',true);
                else
                    GField=Grad(img,scale,'crop',crop,'normalize',true);
                end;
            case 4
                HOUGH_s=3;
                if STD
                    GField=reshape(repmat(std_dev(:)',3,1),[3,cshape]).*Grad(img,HOUGH_s,'crop',crop,'normalize',true);
                else
                    GField=Grad(img,HOUGH_s,'crop',crop,'normalize',true);
                end;
                
        end;
            
        %GField=Grad(img,1.5,'crop',crop,'normalize',true);
        GMField_back{sindx}=squeeze(sqrt(sum(GField.^2,1)));
        GField_back{sindx}=GField;
        end;
        
        HField_back{sindx}=HField;
        
    end;
        
    
    for sindx=1:nscales
        scale=Scales(sindx);
        
        HField=HField_back{sindx};        
        [myevec_main,myev_main]=(sta_EVGSL(HField));
        
        myevec{sindx}=myevec_main;
        myev{sindx}=myev_main;
    end;
    
    %
    
    V2=[];
    V3=[];
     for sindx=1:nscales
        scale=Scales(sindx);
        myev_main=myev{sindx};
        S=squeeze(sqrt(sum(myev_main.^2,1)));
        Ra=squeeze(abs(myev_main(2,:,:,:))./(eps+abs(myev_main(1,:,:,:))));
        Rb=squeeze(abs(myev_main(3,:,:,:))./sqrt((eps+abs(myev_main(1,:,:,:).*abs(myev_main(2,:,:,:))))));
        V=(1-exp(-Ra.^2/(2*a^2))).*exp(-Rb.^2/(2*b^2)).*(1-exp(-S.^2/(2*c^2))).*squeeze(((myev_main(1,:,:,:)<0)&(myev_main(2,:,:,:)<0)));
        V2=[V2,V(:)];
        if nargout>1
        
        %DirImg=myevec{sindx}([3,2,1],:,:,:);
        DirImg=myevec{sindx}([1,2,3],:,:,:);
        %DirImg=myevec{sindx}([2,1,3],:,:,:);
        DirImg=reshape(DirImg,[3,1,size(img)]);
%         DirImg(1,:)=1;
%         DirImg(2,:)=0;
%         DirImg(3,:)=0;
        switch (mode)
            case 1
                %VM2=mhs_medialness(GField_back{sindx},DirImg,{'radius',scale,'sigma_r',sigma_r,'sigma_v',sigma_v,'mode',mode});
                VM2=mhs_medialness(GField_back{sindx},DirImg,{'radius',scale,'sigma_v',sigma_v,'sigma_r',sigma_r,'mode',mode,'tube_steps',tube_steps,'tube_length',tube_length,'circle_mode',circle_mode});
            case 3
                %DirImg=single([0,0,1]);
                VM=mhs_medialness(GMField_back{sindx},DirImg,{'radius',scale,'sigma_r',sigma_r,'sigma_v',sigma_v,'mode',mode});
                VM2=(squeeze(VM)-GMField_back{sindx});
                %if STD
                %    VM2=scale*VM2.*(VM2>0).*std_dev;
                %else
                    VM2=scale*VM2.*(VM2>0);
                %end;
            case 4
                VM=mhs_medialness(GField_back{sindx},DirImg,{'radius',scale,'sigma_r',sigma_r,'sigma_v',sigma_v,'mode',mode});
                VM2=(squeeze(VM)-GMField_back{sindx});
                %VM2=scale*VM;
                %VM2=scale*(squeeze(VM)-GMField_back{sindx});
                VM2=VM2.*(VM2>0);
        end;
        %VM2=scale*(squeeze(VM));
        
        V3=[V3,VM2(:)];
        end;
        
%         if nargout>1
%             V=-squeeze(myev_main(2,:,:,:))./(img+eps);
%             V3=[V3,V(:)];
%         end;
    end;
    
    %V2=reshape(V2,[size(V{1}),nscales]);
    V2=reshape(V2,[size(img),nscales]);
%      if nargout>1
%          V3=reshape(V3,[size(img),nscales]);
%      end;
    %%reshape(max([V{1}(:),V{2}(:)],[],2),size(V{1}))
    V3=reshape(V3,[size(img),nscales]);