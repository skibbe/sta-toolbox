function Cropme=save_figure(shape,fname,f,varargin)

autocrop=false;
stretch=true;    
psize=16;
hideaxis=false;
fext='png';
maketrans=false;
flop=false;
flip=false;
flipflop='';
autocrop_dim=[1,1];

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

if flop
    flipflop=' -flop ';
end;
if flip
    flipflop=[flipflop,' -flip '];
end;

ax=get(f,'CurrentAxes');
tmp=shape(2);
shape(2)=shape(1);
shape(1)=tmp;





shape=shape./max(shape);
%psize=7;

xSize=psize*shape(1);
ySize=psize*shape(2);
if stretch
    set(ax, 'position', [0,0,1,1]);
    legend(get(f,'CurrentAxes'),'hide');
else
    if hideaxis
        set(ax, 'position', [0,0,0.9,0.9]);
    end;
end;
%f=gcf;

if hideaxis
    axis off;
end;

if true
        set(f,'PaperSize',[xSize ySize]);
        set(f,'PaperPosition',[0 0 xSize ySize]);
set(get(f,'CurrentAxes'),'Box','on');
end
if strcmp(fext,'png')
    saveas(f,[fname],'png');
elseif strcmp(fext,'pdf')
    ft=[fname,'.epsc'];
    ft(strfind(ft,'.pdf'):strfind(ft,'.pdf')+3)='';
    saveas(f, ft, 'epsc');
    %if ~autocrop
        system(['epstopdf ',ft,';']);
        system(['rm ',ft,';']);
    %end;
else
    saveas(f,[fname],fext);
    %error('?');
end;



if autocrop && ~strcmp(fext,'pdf')
%     if strcmp(fext,'pdf')
%         ft=[fname,'.eps'];
%         ft(strfind(ft,'.pdf'):strfind(ft,'.pdf')+3)='';
%     else
%         tmp=imread(ft);
%     end;
    
     tmp=imread(fname);
    
    if ~exist('Cropme')
        Img=~(squeeze(min(tmp==255*ones(size(tmp)),[],3)));
        Cropme=[];
        for d=1:2
            dims=[1,2];
            dims(d)=[];
            pimg=Img;
            for c=1:1
                pimg=max(pimg,[],dims(c));
            end;
            if autocrop_dim(d)
                indx=find(pimg(:)>0);
            else
                indx=[1,size(pimg,d)];
            end;
            Cropme=[Cropme;[indx(1),indx(end)]];
        end;
    end;
    Img=tmp(Cropme(1,1):Cropme(1,2),Cropme(2,1):Cropme(2,2),:);
    %fname(end-2:end)='png';
    imwrite(Img,fname);
end;

% if strcmp(fext,'pdf')
%     if autocrop
%         system(['epstopdf ',ft,';']);
%         system(['rm ',ft,';']);
%     end;
% end;    

    
    
if maketrans && strcmp(fext,'png')
    system(['convert  -transparent white ',fname,'   ',fname]);
end;

if numel(flipflop)>0 && strcmp(fext,'png')
    system(['convert  ',flipflop,'  ',fname,'   ',fname]);
end;






