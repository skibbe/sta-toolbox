%%

%V=medialness(Img,'scale_range',[1.5,8]);
function A=medialness_treestruct(img,A,varargin)



DirImg=single(A.data(1:6,:));


crop=[0,0,0];
threshold=0.1;
sigma_r=0.5;
nscales=5;
normalize=true;
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
  
    
for a=1:3
    DirImg(a,:)=DirImg(a,:)+crop(a);
end    
    
    classid=class(img);
    threshold=cast(threshold,classid);
    
if exist('scale_range')    
     Scales_fac=scale_range(2)-scale_range(1);
     Scales=scale_range(1):Scales_fac/(nscales-1):scale_range(2);
    Scales=cast(Scales,classid);
else
    nscales=numel(Scales);
end;
    
     
    %shape=size(img);
if numel(size(img))==4    
                 GVF2=img;
                 GVF2(:,:,:,1)=GVF2(:,:,:,2);
                 GVF2(:,:,:,2)=GVF2(:,:,:,1);
                 img=GVF2;
                 clear GVF2;
end;    
      
        V2=[];
        for sindx=1:nscales
            scale=Scales(sindx);

            if numel(size(img))==3
                GField=Grad(img,scale,'normalize',true);
                GMField=squeeze(sqrt(sum(GField.^2,1)));
                [VM,Debug]=mhs_medialness_posAdirs(GMField,DirImg,{'radius',scale,'sigma_r',sigma_r});
            else
                 [VM,Debug]=mhs_medialness_posAdirsGVF(img,DirImg,{'radius',scale,'sigma_r',sigma_r});
            end;
            
            V2=[V2,VM(:)];
        end;
         %V2=reshape(V2,[size(img),nscales]);

        [v,scale_indx]=max(V2,[],2);
        scale_steps=size(V2,2);
        scale_img=squeeze(scale_range(1)+(scale_indx-1).*(scale_range(2)-scale_range(1))/(scale_steps-1));
        
        A.data(8,:)=2*scale_img;
        