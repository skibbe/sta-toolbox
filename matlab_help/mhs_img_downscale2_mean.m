function I=mhs_img_downscale2_mean(I)


fact=cast(1/8,class(I));
shape=size(I);

if numel(shape)==3
  shape2=2*floor(shape/2);
  I=I(1:2:shape2(1),:,:)+I(2:2:shape2(1),:,:);
  I=I(:,1:2:shape2(2),:)+I(:,2:2:shape2(2),:);
  I=(I(:,:,1:2:shape2(3))+I(:,:,2:2:shape2(3)))*fact;
else   numel(shape)==4
  shape2=2*floor(shape(2:end)/2);
  I=I(:,1:2:shape2(1),:,:)+I(:,2:2:shape2(1),:,:);
  I=I(:,:,1:2:shape2(2),:)+I(:,:,2:2:shape2(2),:);
  I=(I(:,:,:,1:2:shape2(3))+I(:,:,:,2:2:shape2(3)))*fact;
end;