function Img=ntrack_preprocess(img)

shape=size(img);

border=16;
qsize=200;

steps=ceil(shape/(qsize));

Img=img;
for x=1:steps(1)
    intX=[1+(x-1)*qsize,(x-1)*qsize+qsize];
    intX(2)=min(intX(2),shape(1));
    for y=1:steps(2)
        intY=[1+(y-1)*qsize,(y-1)*qsize+qsize];
        intY(2)=min(intY(2),shape(2));
        for z=1:steps(3)
            intZ=[1+(z-1)*qsize,(z-1)*qsize+qsize];
            intZ(2)=min(intZ(2),shape(3));
            intX_=mod([intX(1)-border:intX(2)+border]+shape(1),shape(1))+1;
            intY_=mod([intY(1)-border:intY(2)+border]+shape(2),shape(2))+1;
            intZ_=mod([intZ(1)-border:intZ(2)+border]+shape(3),shape(3))+1;
            
            %intX_=mod(intX+shape(1)+[-border,border],shape(1));
            
            
            Imgtmp=img(intX_,intY_,intZ_);
            Imgtmp=scaleimage3D(mhs_smooth_img(scaleimage3D(Imgtmp,size(Imgtmp)*2,false,'spline'),1.5,'normalize',true),size(Imgtmp),false,'spline');
            %Imgtmp=scaleimage3D(real(mhs_smooth_img(scaleimage3D(Imgtmp,size(Imgtmp)*2,false,'spline'),1.5,'normalize',true).^0.5),size(Imgtmp),false,'spline');
            qsizeX=intX(2)-intX(1)+1;
            qsizeY=intY(2)-intY(1)+1;
            qsizeZ=intZ(2)-intZ(1)+1;
            Img(intX(1):intX(2),intY(1):intY(2),intZ(1):intZ(2))=Imgtmp(border:border+qsizeX-1,border:border+qsizeY-1,border:border+qsizeZ-1);
        end;
    end;
end;

%