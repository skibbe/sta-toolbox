%%

%V=medialness(Img,'scale_range',[1.5,8]);
function V2=medialness(img,varargin)


crop=[0,0,0];
threshold=0.1;
sigma_r=0.5;
nscales=5;
normalize=true;
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
  
    
    classid=class(img);
    threshold=cast(threshold,classid);
    
if exist('scale_range')    
     Scales_fac=scale_range(2)-scale_range(1);
     Scales=scale_range(1):Scales_fac/(nscales-1):scale_range(2);
    Scales=cast(Scales,classid);
else
    nscales=numel(Scales);
end;
    
    
     


    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

 
      
        V2=[];
        for sindx=1:nscales
            scale=Scales(sindx);

            
            HField=scale^2.*Hessian(img,scale,'crop',crop,'normalize',true);
            [myevec_main,myev_main]=(sta_EVGSL(HField));
            DirImg=myevec_main([1,2,3],:,:,:);
            DirImg=reshape(DirImg,[3,1,size(img)]);            
            clear  myevec_main myev_main HField;
            
            GField=Grad(img,scale,'crop',crop,'normalize',true);
            GMField=squeeze(sqrt(sum(GField.^2,1)));

            
            %DirImg=myevec{sindx}([2,1,3],:,:,:);
            
            %imgs=mhs_smooth_img(img,scale,'normalize',true);
            mode=3;
            VM=mhs_medialness2(GMField,DirImg,{'radius',scale,'sigma_r',sigma_r,'mode',mode});
            if normalize
                VM2=(squeeze(VM)-GMField);
                VM2=scale*VM2.*(VM2>0);            
            else
                VM2=scale*VM;            
            end;
            
            V2=[V2,VM2(:)];
            
        end;
         V2=reshape(V2,[size(img),nscales]);

