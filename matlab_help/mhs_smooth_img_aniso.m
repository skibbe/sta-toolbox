%img_as=mhs_smooth_img_aniso(img_s_C2(:,:,:),1.5,0.1,100,0.1);
%img_as=mhs_smooth_img_aniso(img_s_C1,2.5,1,100,0.1);
function img=mhs_smooth_img_aniso(img,sigma,tau,iter,alpha,debug)

if nargin<6
    debug=false;
end;

if debug
    figure(13);
end;


        
for a=1:iter
    % [I11, I22, I33, I12, I13, I23, I1, I2, I3] = compST(img,tau,sigma);
     [I11, I22, I33, I12, I13, I23, I1, I2, I3] = compH2(img,tau,sigma);
    
    img = img+alpha*smAlongST(img,I11,I22,I33,I12,I13,I23,I1,I2,I3);
    
    if debug
        %imagesc(squeeze(img(:,:,ceil(end/2))));
    
        imagesc(squeeze(max(img,[],3)));
        drawnow;
    end;
    fprintf('%d\n',a);
end;



