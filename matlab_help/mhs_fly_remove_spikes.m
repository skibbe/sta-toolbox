function Img=mhs_fly_remove_spikes(Img,mval)

if nargin<2
    mval=2^15;
end;




susp=find(Img(:)>mval);

fprintf('fixing %d voxel\n',numel(susp));



[x,y,z]=ind2sub(size(Img),susp);


X=([x,x-1,x+2  ,x,x-1,x+2  ,x,x-1,x+2,x,x-1,x+2  ,x,x-1,x+2  ,x,x-1,x+2,x,x-1,x+2  ,x,x-1,x+2  ,x,x-1,x+2])';

Y=([y,y,y     ,y-1,y-1,y-1, y+1,y+1,y+1, y,y,y     ,y-1,y-1,y-1, y+1,y+1,y+1 , y,y,y     ,y-1,y-1,y-1, y+1,y+1,y+1])';

Z=([z,z,z,   z,z,z,         z,z,z, z-1,z-1,z-1, z-1,z-1,z-1, z-1,z-1,z-1, z+1,z+1,z+1, z+1,z+1,z+1, z+1,z+1,z+1 ])';

% X=min(max(X(:),1),numel(Img));
% Y=min(max(Y(:),1),numel(Img));
% Z=min(max(Z(:),1),numel(Img));


X=min(max(X(:),1),size(Img,1));
Y=min(max(Y(:),1),size(Img,2));
Z=min(max(Z(:),1),size(Img,3));



%neighbors=neighbors((neighbors>0)&(neighbors<numel(Img)));

neighbors=Img(sub2ind(size(Img),X,Y,Z));



neighbors = (reshape(neighbors(:),[27,numel(neighbors)/27]));

old_values=Img(susp).';
new_values1=(median(neighbors,1));
new_values2=(min(neighbors,[],1));

new_values=new_values1.*(old_values>new_values1) + new_values2.*(~(old_values>new_values1));

new_values(new_values(:)>2^15)=0;

Img(susp)=new_values;


%Img(susp)=0;

%for a=1:numel(susp)
    
    
%end;



