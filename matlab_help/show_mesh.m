function show_mesh(mesh)

clf;
hold on;
for f=1:size(mesh.f,2)
    indx=mesh.f(:,f)+1;
    triangle=mesh.v(:,indx);
    plot3(triangle(1,:),triangle(2,:),triangle(3,:))
end;

for v=1:size(mesh.v,2)
    
    normal=[mesh.v(:,v),mesh.v(:,v)-mesh.n(:,v)];
    plot3(normal(1,:),normal(2,:),normal(3,:))
end;
