function img=mhs_crop_zeros(img,border)

if nargin<2
    border=0;
end;


imgX=squeeze(max(max(img,[],2),[],3));
imgY=squeeze(max(max(img,[],1),[],3));
imgZ=squeeze(max(max(img,[],1),[],2));

indX0=max(find(imgX>0)-border,1);
indX1=min(1+size(img,1)-find(imgX(end:-1:1)>0)+border,size(img,1));
indY0=max(find(imgY>0)-border,1);
indY1=min(1+size(img,2)-find(imgY(end:-1:1)>0)+border,size(img,2));
indZ0=max(find(imgZ>0)-border,1);
indZ1=min(1+size(imgZ,1)-find(imgZ(end:-1:1)>0)+border,size(img,3));



img=img(indX0(1):indX1(1),indY0(1):indY1(1),indZ0(1):indZ1(1));