function [I11 I22 I33 I12 I13 I23 I1 I2 I3] = compH(img,tau,sigma)


a=0.5;b=0.5;c=0.25;


Dx = @(x) imfilter(x,[1 0 -1]','circular')/2;
Dy = @(x) imfilter(x,[1 0 -1],'circular')/2;
Dz = @(x) imfilter(x,permute([1 0 -1]',[2 3 1]),'circular')/2;

  H=Hessian(img,sigma,'normalize',true);    
  

       Lap=(sum(H(1:3,:,:,:),1));
        H(1,:,:,:)=H(1,:,:,:)-Lap;
        H(2,:,:,:)=H(2,:,:,:)-Lap;
        H(3,:,:,:)=H(3,:,:,:)-Lap;
if false        
        [myevec,myev_main]=(sta_EVGSL(H));

        OT=cat(1,cat(1,myevec(1,:).^2,myevec(2,:).^2,myevec(3,:).^2),myevec(1,:).*myevec(2,:),myevec(2,:).*myevec(3,:),myevec(1,:).*myevec(3,:));
       %  OT=cat(1,cat(1,myevec(7,:).^2,myevec(8,:).^2,myevec(9,:).^2),myevec(7,:).*myevec(8,:),myevec(8,:).*myevec(9,:),myevec(7,:).*myevec(9,:));

       %V=squeeze(myev_main(1,:,:,:))./squeeze(myev_main(3,:,:,:)+eps);
       V=squeeze(myev_main(3,:,:,:))./(squeeze(myev_main(1,:,:,:))+eps);
       V(V<0)=0;
       c=100;
       %ee = tau*(1-exp(-V*c))+(1-tau);

        iso=tau*V+(1-tau);
        %iso=1-exp(-V.^2/0.1);
        
        ee=1;
        %iso=0.0001;
        %iso=(1-exp(-V*tau));
        iso_tmp=reshape(repmat(iso(:,:).',6,1),[6,size(img)]);
        
        OT=(1-iso_tmp).*reshape(OT,[6,size(img)]);
    
        
        
         I11 = squeeze(iso+squeeze(OT(1,:,:,:))).*ee;
         I22 = squeeze(iso+squeeze(OT(2,:,:,:))).*ee;
         I33 = squeeze(iso+squeeze(OT(3,:,:,:))).*ee;
         I12 = squeeze(OT(4,:,:,:)).*ee;
         I13 = squeeze(OT(6,:,:,:)).*ee;
         I23 = squeeze(OT(5,:,:,:)).*ee;      
else
    
          [myevec,myev_main]=(sta_EVGSL(H));

         H=cat(1,cat(1,myevec(1,:).^2,myevec(2,:).^2,myevec(3,:).^2),myevec(1,:).*myevec(2,:),myevec(2,:).*myevec(3,:),myevec(1,:).*myevec(3,:));
 
         H=reshape(H,[6,size(img)]);
         
         iso=0.01;
         ee=1;
         H=tau*H;
         I11 = squeeze(iso+squeeze(H(1,:,:,:))).*ee;
         I22 = squeeze(iso+squeeze(H(2,:,:,:))).*ee;
         I33 = squeeze(iso+squeeze(H(3,:,:,:))).*ee;
         I12 = squeeze(H(4,:,:,:)).*ee;
         I13 = squeeze(H(6,:,:,:)).*ee;
         I23 = squeeze(H(5,:,:,:)).*ee;      
    
end;

I1 = Dx(I11) + Dy(I12) + Dz(I13);
I2 = Dx(I12) + Dy(I22) + Dz(I23);
I3 = Dx(I13) + Dy(I23) + Dz(I33);
