
function mhs_write_feature_struct(filename,data,savedebug)

if nargin<3
    savedebug=false;
end;
f=fields(data);

command=['hdf5write(''',filename,''''];
for a=1:numel(f)
    if isempty(strfind(f{a},'debug')) || savedebug
        command=[command,',''',f{a},''', data.',f{a}];
    end;    
end;

%command=[command,',''V71Dimensions'',true);'];
command=[command,');'];

command

eval(command);


%hdf5write(filename,location,dataset)