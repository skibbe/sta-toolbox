function [std_dev,img_mean]=mhs_compute_max(Img,varargin)

crop=[0,0,0];
epsilon=0;%.001;
sigman=6;
inv=true;
element_size=[1,1,1];
psf=[1,1,1];


for k = 1:2:length(varargin),
    eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

classid=class(Img);
shape=size(Img);

    
    img_mean=mhs_smooth_img(Img,sigman,'normalize',true,'element_size',element_size,'psf',psf);
    img_mean2=mhs_smooth_img(Img.^2,sigman,'normalize',true,'element_size',element_size,'psf',psf);

    if inv
        %std_dev=cast(1./real((sqrt(img_mean2-img_mean.^2)+(epsilon+eps))),classid);
        std_dev=cast(1./((sqrt(abs(img_mean2-img_mean.^2))+(epsilon+eps))),classid);
    else
        %std_dev=cast(real((sqrt(img_mean2-img_mean.^2))),classid);
        std_dev=cast(((sqrt(abs(img_mean2-img_mean.^2)))),classid);
    end;

    std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));
    img_mean=img_mean(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));




function std_dev2=old(img,sigman,epsilon)
classid=class(img);
        mean=mhs_smooth_img(img,sigman,'normalize',true);
        mean2=mhs_smooth_img(img.^2,sigman,'normalize',true);
        std_dev2=real(sqrt(mean2-mean.^2));
        %std_dev2=cast(real(1./(sqrt(mean2-mean.^2)+(epsilon+eps))),classid);
        %std_dev=std_dev(crop(1)+1:end-crop(1),crop(2)+1:end-crop(2),crop(3)+1:end-crop(3));

        