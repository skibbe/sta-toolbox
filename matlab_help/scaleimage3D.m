function result=scaleimage3D(image,dim,smoothed,method,cyclic,gamma)

imgsz=size(image);
if (numel(imgsz)==2)&&(numel(dim==3))
    image=reshape(repmat(image(:),1,dim(3)),[imgsz,dim(3)]);
    imgsz=[imgsz,dim(3)];
end;

classid=class(image);

scale=dim./imgsz;
dim=floor(dim);

if nargin<=3
    method = 'cubic';
end
if nargin<=2
    smoothed = 0;
end

if nargin<5
    cyclic = 0;
end
 
 if (min(scale)<1) && smoothed,

    sigma=1./scale;
    sigma=sigma/2; 
    
    shape=size(image);
    imgszp=ceil(size(image)+6*sigma); 
    imgszp=sta_fft_bestshape(imgszp);
    
    if (cyclic==1),
        imgszp=size(image);
    end;
    
    sigma=2*sigma.^2;
    
    [X Y Z] = ndgrid(0:(imgszp(1)-1),0:(imgszp(2)-1),0:(imgszp(3)-1));
    X = X - ceil(imgszp(1)/2);
    Y = Y - ceil(imgszp(2)/2);
    Z = Z - ceil(imgszp(3)/2);
    R2 = cast((X.^2./sigma(1) + Y.^2./sigma(2) + Z.^2./sigma(3)),classid);
    clear X Y Z
    kernel = fftshift(exp(-R2));
    clear R2
    kernel=kernel./sum(kernel(:));
    
    %padsize=(imgszp-shape)/2;
    %tmp=padarray(img,padsize,'symmetric');
    tmp=zeros(imgszp,classid);
    tmp(1:imgsz(1),1:imgsz(2),1:imgsz(3))=image;
    tmp=ifftn(fftn(tmp).*fftn(kernel));%./numel(tmp);
    image=tmp(1:imgsz(1),1:imgsz(2),1:imgsz(3));
    clear tmp
end;

    if nargin>=6
        image=abs(image.^gamma);
    end


%dim=floor(imgsz.*scale);


[Nr, Nc, Ns] = size(image);
Mr = dim(1);
Mc = dim(2);
Ms = dim(3);

[X, Y, Z] = meshgrid(1:Nc, 1:Nr, 1:Ns);
X=cast(X,classid);
Y=cast(Y,classid);
Z=cast(Z,classid);
% Ir = [1:Nr/Mr:Nr] + (Nr - (Mr-1)*Nr/Mr - 1)/2;
% Ic = [1:Nc/Mc:Nc] + (Nc - (Mc-1)*Nc/Mc - 1)/2;
% Is = [1:Ns/Ms:Ns] + (Ns - (Ms-1)*Ns/Ms - 1)/2;
Ir = linspace(1,Nr,Mr);
Ic = linspace(1,Nc,Mc);
Is = linspace(1,Ns,Ms);
Ir=cast(Ir,classid);
Ic=cast(Ic,classid);
Is=cast(Is,classid);

[Xi,Yi,Zi] = meshgrid(Ic,Ir,Is);

clear Ir Ic Is

Zero=min(image(:));
%fprintf('interpolating ..'); 
result = interp3(X,Y,Z,image,Xi,Yi,Zi,method,0); 
%fprintf('done \n'); 
