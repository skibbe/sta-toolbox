%%commands={};for d=1:10, commands{d}=['mymatlab -r "d=',num2str(d),';mode=1;do_sim5;exit;"'];end;mhs_create_bash_commands(commands,'nthreads',6);
%%commands={};for d=1:10, commands{d}=['d=',num2str(d),';mode=1;do_sim5;'];end;mhs_create_bash_commands(commands,'nthreads',6,'ismatlab',true);
%% commands={};for d=1:10, commands{d}=['load([''data',num2str(d),'.mat'']);img=data.ImgD2;img=mhs_smooth_img_aniso(mhs_smooth_img(img,1.0,''normalize'',true),1.5,0.1,200,0.1);save([''data',num2str(d),'_iso_plus_aniso_smoothed.mat''],''img'');'];end;mhs_create_bash_commands(commands,'nthreads',10,'ismatlab',true);
%%commands={};for d=1:10, commands{d}=['load([''data_chaos_',num2str(d),'.mat'']);img=data.ImgD;img=mhs_smooth_img_aniso(mhs_smooth_img(img,1.0,''normalize'',true),1.5,0.1,200,0.1);save([''data',num2str(d),'_iso_plus_aniso_smoothed.mat''],''img'');'];end;mhs_create_bash_commands(commands,'nthreads',10,'ismatlab',true);
 
function fcommand=mhs_create_bash_commands(commands,varargin)


pbs=false;
slurm=false; 

sta_threads=8;
nthreads=80;
ismatlab=false;
node_mem=990;
node_cpu=96;
min_mem=32;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

        %mem=max(floor(sta_threads*node_mem/(node_cpu)),min_mem);
        mem=min_mem;
        %sta_threads=max(floor(node_cpu/(node_mem/mem)),sta_threads);

if ~exist('matcommand','var')
    %matcommand=['LD_PRELOAD=\"/home/skibbe-h/projects/x86_64/lib/libfftw3f.so /home/skibbe-h/projects/x86_64/lib/libfftw3.so /home/skibbe-h/projects/x86_64/lib/libfftw3_omp.so /home/skibbe-h/projects/x86_64/lib/libfftw3f_omp.so /home/skibbe-h/projects/x86_64/lib64/libstdc++.so  /home/skibbe-h/projects/x86_64/lib64/libstdc++.so.6\" OMP_NUM_THREADS=',num2str(sta_threads),' STA_NUMTHREADS=\"',num2str(sta_threads),'\" MATLAB_PREFDIR=/home/skibbe-h/.sever_matlab_settings  matlab '];
    %matcommand=['OMP_NUM_THREADS=',num2str(sta_threads),' STA_NUMTHREADS=\"',num2str(sta_threads),'\" MATLAB_PREFDIR=/home/skibbe-h/.sever_matlab_settings  matlab '];
    matcommand=['LD_PRELOAD=\"/usr/lib/x86_64-linux-gnu//libfftw3.so /usr/lib/x86_64-linux-gnu//libfftw3f.so /usr/lib/x86_64-linux-gnu//libfftw3_omp.so /usr/lib/x86_64-linux-gnu//libfftw3f_omp.so\" OMP_NUM_THREADS=',num2str(sta_threads),' STA_NUMTHREADS=\"',num2str(sta_threads),'\" MATLAB_PREFDIR=/home/skibbe-h/.sever_matlab_settings  matlab '];
end;

if pbs
    mc=numel(commands);
    group=mc;

    fcommand='';
    %fcommand=['for a in ',num2str([1:mc]),' ;'];
    for a=1:mc 
        

        
%         if (mem<max_mem)
%             warning('not enough mem for threads, optimizing sta_threads');
%             
%         end;
        
        fcommand=['echo "'];
        if ~ismatlab
            fcommand=[fcommand,' STA_NUMTHREADS=\"',num2str(sta_threads),'\" ',commands{a},';'];
        else
            fcommand=[fcommand,matcommand,' -r \"',commands{a},';exit;\";'];
        end;

  
    %fcommand=[fcommand,'" |   qsub -V  -q bigmem  -l nodes=1:ppn=',num2str(sta_threads),':bigmem,mem=',num2str(mem),'gb,pmem=',num2str(mem),'gb,vmem=',num2str(mem),'gb']; 
    fcommand=[fcommand,'" |   qsub -V  -q bigmem  -l nodes=1:ppn=',num2str(sta_threads),':bigmem,vmem=',num2str(mem),'gb']; 
    fprintf('%s\n',fcommand);
    end;
elseif slurm
       mc=numel(commands);
    group=mc;

    fprintf('histchars=\n');
    fcommand='';
    for a=1:mc 
        
        fcommand=[''];
        if ~ismatlab
            fcommand=[fcommand,'STA_NUMTHREADS=\"',num2str(sta_threads),'\" ',commands{a},';'];
        else
            fcommand=[fcommand,matcommand,' -r \"',commands{a},';exit;\";'];
        end;

  
    %fcommand=[fcommand,'" |   qsub -V  -q bigmem  -l nodes=1:ppn=',num2str(sta_threads),':bigmem,mem=',num2str(mem),'gb,pmem=',num2str(mem),'gb,vmem=',num2str(mem),'gb']; 
    %fcommand=[fcommand,'" |   qsub -V  -q bigmem  -l nodes=1:ppn=',num2str(sta_threads),':bigmem,vmem=',num2str(mem),'gb']; 
    %fcommand=['histchars= && printf "#!/bin/bash \n#SBATCH --mem ',num2str(mem*1000),'\n#SBATCH -c ',num2str(sta_threads),'\n#SBATCH -p bigmem\n',fcommand,'" | sbatch && unset histchars'];
    
    if ~exist('outputs','var')
        fcommand=['printf "#!/bin/bash \n#SBATCH --mem ',num2str(mem*1000),'\n#SBATCH -c ',num2str(sta_threads),'\n#SBATCH -p bigmem\n',fcommand,'" | sbatch'];    
    else
        fcommand=['printf "#!/bin/bash \n#SBATCH --mem ',num2str(mem*1000),'\n#SBATCH -c ',num2str(sta_threads),...
            '\n#SBATCH --error ',outputs{a},...
            '\n#SBATCH --output ',outputs{a},...
            '\n#SBATCH -p bigmem\n',fcommand,'" | sbatch'];    
    end;
        
         
         
    
    
    fprintf('%s\n',fcommand);
    end;
    fprintf('unset histchars\n');
else
    mc=numel(commands);
    group=ceil(mc/nthreads);

    fcommand='';
    %fcommand=['for a in ',num2str([1:mc]),' ;'];
    for a=1:mc 
        if (mod(a-1,group))==0
            if a>1
                fcommand=[fcommand,'" &']; 
                fprintf('%s\n',fcommand)
                fcommand='';
                %fcommand=[fcommand,'" &']; 
                %fcommand=[fcommand,'" &']; 
            end;

            %if ~ismatlab
                fcommand=[fcommand,' nohup bash -c "'];
            %else
            %    fcommand=[fcommand,' nohup bash -c "',matcommand,'-r \" ;exit;\" "'];
            %end;


        end
        if ~ismatlab
            fcommand=[fcommand,' STA_NUMTHREADS=\"',num2str(sta_threads),'\" ',commands{a},';'];
        else
            fcommand=[fcommand,matcommand,' -r \"',commands{a},';exit;\";'];
        end;

    end;
    fcommand=[fcommand,'" &']; 
    fprintf('%s\n',fcommand);
end
