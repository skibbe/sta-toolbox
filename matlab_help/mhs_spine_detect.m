function saliency_map=mhs_spine_detect(img,varargin)


    element_size=[1,1,1];
    pad=false;
      epsilon=0.01;
    scale_range=[1.15,2.5];
    psf=[1,1,1];
    nscales=4;
 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    classid=class(img);
    
    
    element_size=element_size/min(element_size);
    
    pad=max(crop)<0;
    
    if pad
        border=-crop;
        img=padarray(img,border,'symmetric');
        crop=border;
    end;
    
    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);
    
    saliency_map=zeros(cshape,classid);
    
     scale_stepsize_pol=(scale_range(2)-scale_range(1))/(nscales-1);
     Scales_pol=scale_range(1):scale_stepsize_pol:scale_range(2);
    
     
     normalize_derivatives=false;  
     classid=class(img);

    nscales_pol=numel(Scales_pol);

     fprintf('computing saliency map: ');
    for sindx=1:nscales_pol
        scale=Scales_pol(sindx);
        sigmanorm=max(scale*sqrt(2),2);

        %std_dev=1./(img_slocal_sdv(sindx,:)+epsilon);
        
        %std_dev=img_slocal_sdv(:);
        fprintf('1');
        HField=Hessian(img,scale,'crop',crop,'normalize',normalize_derivatives,'psf',psf,'element_size',element_size);    
        fprintf('2');
        [myevec,myev]=(sta_EVGSL(HField));
        fprintf('3');
        N=scale^2/((2*pi*scale^2)^(3/2));
        %detections=(-N)*squeeze((max(myev,[],1)<0).*prod(myev,1)).*img_slocal_sdv;
        
        clear HField myevec;
        img_slocal_sdv=mhs_compute_std_mask(img,'sigman',sigmanorm,'epsilon',epsilon,'crop',crop,'inv',true);
        fprintf('4');
        detections=(-N)*squeeze((max(myev,[],1)<0).*sum(myev,1)).*img_slocal_sdv;
        
        clear myev img_slocal_sdv;
        fprintf('5');
        
        saliency_map=max(detections,saliency_map);
        fprintf('.');
    end;
    
