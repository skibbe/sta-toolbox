function h=mhs_error_plot(values,errormargin,varargin)


        M=values;
        E_up=M+errormargin;
        E_down=M-errormargin;
        
        
        mask=ones(size(M));
        
        %P=zeros(size(errormargin).*[2,1]);
        %P(1:2:end,:)=E_up;
        %P(2:2:end,:)=E_down;
        color=[1,0,0];
        transparent=1;
        
        for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
        end;
        
        num_items=size(errormargin,1);
        num_pts=size(errormargin,2);
        
        for a=2:num_pts
            if ~mask(a)
               M(a)= M(a-1);
               E_up(a)= E_up(a-1);
               E_down(a)= E_down(a-1);
            end;
        end;
        for a=num_pts-1:-1:1
            if ~mask(a)
               M(a)= M(a+1);
               E_up(a)= E_up(a+1);
               E_down(a)= E_down(a+1);
            end;
        end;
        
        
        mask2=1-reshape([mask,mask]',numel(mask)*2,1);
        Y=[mask2(2:end);mask2(end)];
        xps=[1:num_pts];
        mask_end=find((mask(1:end-1)-mask(2:end))==-1);
        mask_start=find((mask(1:end-1)-mask(2:end))==1);
       xps(mask_end)=xps(mask_end)+0.5;
       xps(mask_start)=xps(mask_start)+0.5;
        X=reshape([xps',xps']',numel(xps)*2,1);
        %clf
        
        h=[];
        
        hold on;
        if num_items>1
            
            for a=1:num_items
               color=hsv2rgb(a/num_items,1,1);

               s=area(E_up(:,a),'FaceColor',color); 
               alpha(s,transparent);
               %set( s, 'FaceColor', transparent)
               s=area(E_down(:,a),'FaceColor',[1,1,1]);  
               %alpha(s,transparent);
            end;
        else
%                s=area(E_up,'FaceColor',color,'linewidth',1,'linestyle','none'); 
%                %set( s, 'FaceColor', transparent)
%                alpha(s,transparent);
%                
%                s=area(E_down,'FaceColor',[1,1,1],'linewidth',1,'linestyle','none');
%                %alpha(s,transparent);
%                
%                plot(M,'k','linewidth',2);
%                plot(E_up,'k','linewidth',2,'linestyle','--');
%                plot(E_down,'k','linewidth',2,'linestyle','--');
%                s=area(X,Y,'FaceColor',[1,0.8,0.8],'linewidth',1,'linestyle','none');
%                alpha(s,transparent);
               
               dy=[E_up,E_down(end:-1:1)];
               dx=[1:numel(E_up),numel(E_down):-1:1];

               s=fill(dx,dy,color);
                %s=area(E_up,'FaceColor',color,'linewidth',1,'linestyle','none'); 
               %set( s, 'FaceColor', transparent)
               alpha(s,transparent);
               h=[h,s];
               
               %s=area(E_down,'FaceColor',[1,1,1],'linewidth',1,'linestyle','none');
               %alpha(s,transparent);
               
               plot(M,'k','linewidth',2);
               plot(E_up,'k','linewidth',2,'linestyle','--');
               plot(E_down,'k','linewidth',2,'linestyle','--');
               %s=area(X,Y,'FaceColor',[1,0.8,0.8],'linewidth',1,'linestyle','none');
              % alpha(s,transparent);
        end;
        axis([1 num_pts 0 1 ])
        
        
        
        
        
%         h=area(P,0);
%         for a=1:num_items
%             color=hsv2rgb(a/num_items,1,1);
%             h(2*(a-1)+1).FaceColor = color;
%             h(2*(a-1)+2).FaceColor = color;
%         end;
        
        