function x = smAlongST(a,I11,I22,I33,I12,I13,I23,I1,I2,I3)


D11 = [ 0 1 0 ;
        0 -2 0 ;
        0 1 0];
D22 = permute(D11,[2 1 3]);
D33 = permute(D11,[2 3 1]);

D12 = [1 0 -1 ; 
      0 0 0 ;
     -1 0 1]/4;
D13 = permute(D12,[1 3 2]);
D23 = permute(D12,[3 1 2]);

D1 = [0 1 0; 0 0 0 ; 0 -1 0]/2;
D2 = permute(D1,[2 1 3]);
D3 = permute(D1,[2 3 1]);

fil = @(x,y) imfilter(x,y,'circular');


x = ( fil(a,D1).*I1 + fil(a,D2).*I2 + fil(a,D3).*I3 +  ...
    fil(a,D11).*I11 +fil(a,D22).*I22 +fil(a,D33).*I33 + 2*(fil(a,D12).*I12 +fil(a,D13).*I13 +fil(a,D23).*I23 ) );
