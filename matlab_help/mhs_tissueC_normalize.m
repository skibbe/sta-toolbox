function ImgN=mhs_tissueC_normalize(Img_stack,varargin)


epsilon=0.0;
rescale=true;
 sigman=10;
 med=3;
  
 
for k = 1:2:length(varargin),
    eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

ImgN=Img_stack;
for a=1:size(Img_stack,3)

    Img=squeeze(Img_stack(:,:,a));
    if med>0
        %Img=medfilt2(Img,[med,med]);
        Img=medfilt2(Img);
    end;
    
    img_mean=mhs_smooth_img(Img,sigman,'normalize',true);
    img_mean2=mhs_smooth_img(Img.^2,sigman,'normalize',true);

    
    std_dev=real((sqrt(max(img_mean2-img_mean.^2,0))));
    slice=(Img-img_mean)./(std_dev+epsilon+eps);
    %ImgN(:,:,a)=medfilt2(slice);
    ImgN(:,:,a)=slice;
   
end;

if rescale
    ImgN=ImgN-min(ImgN(:));
    ImgN=ImgN./max(ImgN(:));
end;