function [I11 I22 I33 I12 I13 I23 I1 I2 I3] = compST(t1,tau,sigma)


Dx = @(x) imfilter(x,[1 0 -1]','circular')/2;
Dy = @(x) imfilter(x,[1 0 -1],'circular')/2;
Dz = @(x) imfilter(x,permute([1 0 -1]',[2 3 1]),'circular')/2;

D1x = @(x) imfilter(x,[1 -1]','circular');
D1y = @(x) imfilter(x,[1 -1],'circular');
D1z = @(x) imfilter(x,permute([1 -1]',[2 3 1]),'circular');

dx = D1x(t1);
dy = D1y(t1);
dz = D1z(t1);

[gx gy gz] = ndgrid(fspecial('gaussian',[7 1],sigma)); g = gx.*gy.*gz;


smu = @(x) x; %imfilter(x,g,'circular');
% 
% AB11 = smu(dx.^2)+tau;
% AB22 = smu(dy.^2)+tau;
% AB33 = smu(dz.^2)+tau;
% AB12 = smu(dx.*dy);
% AB13 = smu(dx.*dz);
% AB23 = smu(dy.*dz);
% 
% detAB =  power(AB11.*AB22.*AB33 + 2*AB12.*AB23.*AB13  - AB13.*AB13.*AB22 - AB11.*AB23.*AB23 - AB12.*AB12.*AB33,1);
% 
% I11 =  (AB33.*AB22-AB23.*AB23)./detAB;
% I12 = -(AB33.*AB12-AB23.*AB13)./detAB;
% I13 =  (AB23.*AB12-AB22.*AB13)./detAB;
% I22 =  (AB33.*AB11-AB13.*AB13)./detAB;
% I23 = -(AB23.*AB11-AB12.*AB13)./detAB;
% I33 =  (AB22.*AB11-AB12.*AB12)./detAB;

ee = eps+sqrt(dx.^2 + dy.^2 + dz.^2);
dx = dx ./ ee;
dy = dy ./ ee;
dz = dz ./ ee;
 c = 1; %1./(1+ee*0.1);
ee = 0.98*(1-exp(-tau*ee));



I11 = smu(1-dx.^2 .* ee)*c;
I22 = smu(1-dy.^2 .* ee)*c;
I33 = smu(1-dz.^2 .* ee)*c;
I12 = smu(-dx.*dy .* ee)*c;
I13 = smu(-dx.*dz .* ee)*c;
I23 = smu(-dy.*dz .* ee)*c;


I1 = Dx(I11) + Dy(I12) + Dz(I13);
I2 = Dx(I12) + Dy(I22) + Dz(I23);
I3 = Dx(I13) + Dy(I23) + Dz(I33);
