
function data=mhs_read_feature_struct(filename)

hinfo = hdf5info(filename);






f={hinfo.GroupHierarchy.Datasets.Name};
for a=1:numel(f)
    f{a}(1)=[];
end;

data=struct();

%command=['hdf5write(''',filename,''''];
for a=1:numel(f)
    %data=setfield(data,f{a},hdf5read(filename,f{a},'V71Dimensions',true));
    data=setfield(data,f{a},hdf5read(filename,f{a}));
    shape=size(getfield(data,f{a}));
    if (ndims(shape)==2) && (shape(2)==1)
        data=setfield(data,f{a},getfield(data,f{a})');
    end;
end;
