function img=mhs_apply_trafo(img,T,B)


if isstruct(img)
        A=img;
        posA=A.data(1:3,:);
        dirA=A.data(4:6,:);
        nptsA=size(A.data,2);


        posA=(cat(1,posA,ones(1,nptsA)));


        tmp=(T*posA);
        A.data(1:3,:)=tmp(1:3,:); 

        
        tmp=((T(1:3,1:3).')*dirA);
        A.data(4:6,:)=tmp; 
        
        
        if nargin>2
            C=B;
            offset=size(C.data,2);

            tmp=A.data;
            C.data=cat(2,C.data,tmp);
            tmp=A.connections;
            tmp(1:2,:)=tmp(1:2,:)+offset;
            C.connections=cat(2,C.connections,tmp);
            img=C;
        else
            img=A;
        end;
else
    
    shape=size(img);
    [Y, X, Z] = meshgrid(1:shape(2), 1:shape(1), 1:shape(3));

    coord=reshape(inv(T)*[X(:)'; Y(:)'; Z(:)'; ones(size(Z(:)'))],[4,shape]);

     method = 'linear';

    Zero=min(img(:));
    img = interp3(Y,X,Z,img,squeeze(coord(2,:,:,:)),squeeze(coord(1,:,:,:)),squeeze(coord(3,:,:,:)),method,Zero); 
end;