%%

%V=vesselness(Img,'scale_range',[1.5,8],'STD',true);
function [V2,data]=vesselness(img,varargin)


crop=[0,0,0];
threshold=0.1;

epsilon=0.01;
sigman=-1;
STD=false;
a=0.5;b=0.5;c=0.25;
 nscales=5;
sato=true; 
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
  
    
    classid=class(img);
    threshold=cast(threshold,classid);
    
if exist('scale_range')    
     Scales_fac=scale_range(2)-scale_range(1);
     Scales=scale_range(1):Scales_fac/(nscales-1):scale_range(2);
    Scales=cast(Scales,classid);
else
    nscales=numel(Scales);
end;
    
    
     


    shape=size(img);
    cshape=shape-2*crop;
    assert(min(cshape)>0);

if sato
 
        if STD
            if (sigman==-1)
                sigman=1.5*max(Scales);
            end;
            std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon);
        end;    

        V2=[];
        for sindx=1:nscales
            scale=Scales(sindx);

             if STD
                normalize_derivatives=true;
                G=Grad(img,scale,'normalize',normalize_derivatives).*reshape(repmat(std_dev(:)',3,1),[3,shape]);
                GX=Grad(squeeze(G(1,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
                GY=Grad(squeeze(G(2,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
                GZ=Grad(squeeze(G(3,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
                HField=[GX(1,:,:,:);GY(2,:,:,:);GZ(3,:,:,:);GX(2,:,:,:);GY(3,:,:,:);GX(3,:,:,:)];
                clear GX GY GZ G;
             else
                normalize_derivatives=true;
                HField=Hessian(img,scale,'crop',crop,'normalize',normalize_derivatives);
            end
            [myevec_main,myev_main]=(sta_EVGSL(scale^2*HField));
            clear myevec_main;
            clear HField;
                myev_main=sort(myev_main,1,'descend');
                Ra=-myev_main(2,:,:,:).*exp(-myev_main(1,:,:,:).^2./(2*0.5^2*myev_main(2,:,:,:).^2));
                Rb=-myev_main(2,:,:,:).*exp(-myev_main(1,:,:,:).^2./(2*2^2*myev_main(2,:,:,:).^2));
                V=(myev_main(2,:,:,:)<0).*(((myev_main(1,:,:,:)<0).*Ra)+((myev_main(1,:,:,:)>0).*Rb));
                V2=[V2,V(:)];
            
        end;
         V2=reshape(V2,[size(img),nscales]);

else   

    % COMPUTING HESSIAN
    if ~exist('data','var')
        if STD
        if (sigman==-1)
            sigman=1.5*max(Scales);
        end;
        std_dev=mhs_compute_std_mask(img,'sigman',sigman,'epsilon',epsilon);
        end;    


        for sindx=1:nscales
            scale=Scales(sindx);

             if STD
                normalize_derivatives=true;
                G=Grad(img,scale,'normalize',normalize_derivatives).*reshape(repmat(std_dev(:)',3,1),[3,shape]);
                GX=Grad(squeeze(G(1,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
                GY=Grad(squeeze(G(2,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
                GZ=Grad(squeeze(G(3,:,:,:)),0,'normalize',normalize_derivatives,'crop',crop);
                HField=[GX(1,:,:,:);GY(2,:,:,:);GZ(3,:,:,:);GX(2,:,:,:);GY(3,:,:,:);GX(3,:,:,:)];
                clear GX GY GZ G;
             else
                normalize_derivatives=true;
                HField=Hessian(img,scale,'crop',crop,'normalize',normalize_derivatives);
            end
            HField_back{sindx}=scale^2*HField;
        end;


        for sindx=1:nscales
            scale=Scales(sindx);

            HField=HField_back{sindx};        
            [myevec_main,myev_main]=(sta_EVGSL(HField));

            %data.myevec{sindx}=myevec_main;
            data.myev{sindx}=myev_main;
        end;
    end;    
        %

        V2=[];
        V3=[];
         for sindx=1:nscales
            scale=Scales(sindx);

            if sato
                myev_main=sort(data.myev{sindx},1,'descend');
                Ra=-myev_main(2,:,:,:).*exp(-myev_main(1,:,:,:).^2./(2*0.5^2*myev_main(2,:,:,:).^2));
                Rb=-myev_main(2,:,:,:).*exp(-myev_main(1,:,:,:).^2./(2*2^2*myev_main(2,:,:,:).^2));
                V=(myev_main(2,:,:,:)<0).*(((myev_main(1,:,:,:)<0).*Ra)+((myev_main(1,:,:,:)>0).*Rb));
                V2=[V2,V(:)];
            else
                myev_main=data.myev{sindx};
                S=squeeze(sum(myev_main.^2,1));
                Ra=squeeze(abs(myev_main(2,:,:,:))./(eps+abs(myev_main(1,:,:,:))));
                Rb=squeeze(abs(myev_main(3,:,:,:))./sqrt((eps+abs(myev_main(1,:,:,:).*abs(myev_main(2,:,:,:))))));
                V=(1-exp(-Ra.^2/(2*a^2))).*exp(-Rb.^2/(2*b^2)).*(1-exp(-S/(2*c^2))).*squeeze(((myev_main(1,:,:,:)<0)&(myev_main(2,:,:,:)<0)));
                V2=[V2,V(:)];
            end;
        end;

        V2=reshape(V2,[size(img),nscales]);
        
        
end;
