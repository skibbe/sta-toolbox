function FeatureData=mhs_preprocess(Img,crop,SETTINGS,varargin)

    threshold=0.1;
    threshold_spines=0.01;
    ndirs=66;
    poldeg=6;
    poldeg_vdir=4;
%poldeg_vdir=6;    
    
    
    normalization='NCC';
    normalization='NONE';
    normalization='STD';
    epsilon=0.01;
    normalization_saliency='STD';
    secondary_ev_system=false;
    secondary_ev_system_full=false;
    epsilon=0.01;
    trueSF=false;
    scale_range=[1.5,6];
    nscales=4;
    ndensescales=20;
    saliancy_offset=-1;
    
    
    BLOOD_INTERNET_STFILTER=1;
    BLOOD_INTERNET_NEWSCALE=3;
    BLOOD_INTERNET=4;
    BLOOD_INTERNET_HOUGH=11;
    PHANTOM=6;
    PHANTOM_FAR=9;
    PHANTOM_NEWSCALE=5;
    DIADEM_STFILTER=7;
    DIADEM=8;
    
 presmooth=-1;
 addparams={};
 vesselness=false;
 MultiscaleMedialnessValue=false;
 Hough=false;
 hough_gamma=-1;
 hough_std=false;
 hough_sigman=3;
 hough_epsilon=0.1;
 hough_c=0.05;
 
 
 online_hough=false;
 online_medial=false;
 online_hesse=false;
 
%SETTINGS=BLOOD_INTERNET;    
%SETTINGS=BLOOD_MARCO;    
%SETTINGS=PHANTOM;    
%SETTINGS=-1;        
%SETTINGS=FLY;    

switch SETTINGS
    
     
    
      case PHANTOM    
        %############# SPINE DATA ############%   
            epsilon=0.001;               
%presmooth=1.5;
            scale_range=[1.25,8];
            sigman=max(scale_range)*1.5;    
            nscales=5;         
            %scale_range=[1.5,6];
            %sigman=max(scale_range)*1.5;    
            %nscales=4;      
            
            online_hesse=true;
            

        %############# SPINE DATA ############%    
     case PHANTOM_NEWSCALE    
        %############# SPINE DATA ############%   
            epsilon=0.001;               
%presmooth=1.5;
            scale_range=[1.25,8];
            sigman=max(scale_range)*1.5;    
            nscales=5;         
            
     case PHANTOM_FAR    
        %############# SPINE DATA ############%   
            epsilon=0.001;               
            scale_range=[1.25,5];
            sigman=3;    
            nscales=5;         
            
            
    case BLOOD_INTERNET      
        %############# BLOOD DATA ############%   
            epsilon=0.01;
           nscales=5;         
           scale_range=[1.25,8];
           online_hesse=true;
            %online_medial=true;
           
    case BLOOD_INTERNET_HOUGH
          epsilon=0.01;
          nscales=5;         
          scale_range=[1.25,14];
          %online_hough=true;
          online_medial=true;
           
    case BLOOD_INTERNET_NEWSCALE      
        %############# BLOOD DATA ############%   
            epsilon=0.01;
           nscales=5;         
           scale_range=[1.25,8];       
   
        %############# BLOOD DATA ############%    
        
     case BLOOD_INTERNET_STFILTER         
        %############# BLOOD DATA ############%   
            epsilon=0.01;               
           nscales=5;         
           scale_range=[1.25,8];
           Hough=true;
           hough_gamma=1;
           hough_std=true;
           hough_sigman=3;
           hough_epsilon=0.1;
           hough_c=0.05;
        %############# BLOOD DATA ############%        
 case DIADEM      
        %############# BLOOD DATA ############%   
           epsilon=0.01;               
           nscales=4;         
           scale_range=[1.25,4];
           Hough=false;
           %hough_gamma=1;
           %hough_std=true;
           %hough_sigman=3;
           %hough_epsilon=0.1;
        %############# BLOOD DATA ############%          
 case DIADEM_STFILTER         
        %############# BLOOD DATA ############%   
           epsilon=0.01;               
           nscales=4;         
           scale_range=[1.25,6];
           Hough=true;
           hough_gamma=1;
           hough_std=true;
           hough_sigman=3;
           hough_epsilon=0.1;
        %############# BLOOD DATA ############%        

     
    otherwise
        error('unknown flag')
    

end;   

    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;


    if presmooth>0
        %scale_range(end)=sqrt(scale_range(end)^2+presmooth^2);
        Img=mhs_smooth_img(Img,1.5,'normalize',true);
    end;

    if ~exist('Scales')
         Scales_fac=(scale_range(2)/scale_range(1))^(1/(nscales-1));
         Scales=scale_range(1);
         for a=2:nscales
             Scales(a)=Scales(a-1)*Scales_fac;
         end;

        %Scales_fac=(scale_range(2)-scale_range(1))/(nscales-1);
        %Scales=[scale_range(1):Scales_fac:scale_range(2)];
        %Scales_fac=1;
    end;
    
   if ~exist('sigman') 
    sigman=max(Scales)*2;
   end;
    
    shape=size(Img);


if  vesselness
%
[FeatureData]=init_mhs3Dvessel(single(Img),...
    'Scales',Scales,...
    'Scales_fac',Scales_fac,...
    'threshold',threshold,...
    'epsilon',epsilon,...
    'poldeg',poldeg,...
    'poldeg_vdir',poldeg_vdir,...
    'threshold_spines',threshold_spines,...
    'ndensescales',ndensescales,...
    'crop',crop,...
    'sigman',sigman,...
    'ndirs',ndirs,...
    'normalization',normalization,...
    'normalization_saliency',normalization_saliency,...
    'secondary_ev_system',secondary_ev_system,...
    'trueSF',trueSF,...
    'secondary_ev_system_full',secondary_ev_system_full,...
    addparams{:}...
    );
elseif online_hough
    
    
  [FeatureData]=init_mhs3DHough_test(single(Img),...
    'Scales',Scales,...
    'Scales_fac',Scales_fac,...
    'epsilon',epsilon,...
    'ndensescales',ndensescales,...
    'crop',crop,...
    'sigman',sigman,...
    'ndirs',ndirs,...
    'normalization',normalization,...
    addparams{:}...
    );     
    
    
elseif online_medial
    
    [FeatureData]=init_mhs3DMedial(single(Img),...
    'Scales',Scales,...
    'Scales_fac',Scales_fac,...
    'epsilon',epsilon,...
    'ndensescales',ndensescales,...
    'crop',crop,...
    'sigman',sigman,...
    'ndirs',ndirs,...
    'normalization',normalization,...
    addparams{:}...
    );      

elseif online_hesse
    
     Scales_fac=scale_range(2)-scale_range(1);
     Scales=scale_range(1):Scales_fac/(nscales-1):scale_range(2);
    
    
    [FeatureData]=init_mhs3DHessian_test(single(Img),...
    'Scales',Scales,...
    'trueSF',trueSF,...
    'Scales_fac',Scales_fac,...
    'epsilon',epsilon,...
    'ndensescales',ndensescales,...
    'crop',crop,...
    'sigman',sigman,...
    'ndirs',ndirs,...
    'normalization',normalization,...
    'saliancy_offset',saliancy_offset,...
    addparams{:}...
    ); 
    
else
    
 %[FeatureData]=init_mhs3D3(single(Img),...
 %%
 [FeatureData]=init_mhs3D4(single(Img),...
     'hough_c',hough_c,...
     'hough_epsilon',hough_epsilon,...
     'hough_sigman',hough_sigman,...
     'hough_gamma',hough_gamma,...
     'hough_std',hough_std,...
    'Hough',Hough,...
    'MultiscaleMedialnessValue',MultiscaleMedialnessValue,...
    'Scales',Scales,...
    'Scales_fac',Scales_fac,...
    'threshold',threshold,...
    'epsilon',epsilon,...
    'poldeg',poldeg,...
    'poldeg_vdir',poldeg_vdir,...
    'threshold_spines',threshold_spines,...
    'ndensescales',ndensescales,...
    'crop',crop,...
    'sigman',sigman,...
    'ndirs',ndirs,...
    'normalization',normalization,...
    'normalization_saliency',normalization_saliency,...
    'secondary_ev_system',secondary_ev_system,...
    'trueSF',trueSF,...
    'secondary_ev_system_full',secondary_ev_system_full,...
    addparams{:}...
    );   
    
    
end;
FeatureData.presmooth=presmooth;




