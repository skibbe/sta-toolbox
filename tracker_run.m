function [Aout,A,MOVIE]=tracker_run(FeatureData,SETTINGS,varargin)
    
showplots=true;
  return_statistic=false;
  reset_tmp=-1;
  doobar=true;
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    MedialSigma=0.5;
    BLOOD_INTERNET_HOUGH=11;
    BLOOD_INTERNET_STFILTER=1;
    BLOOD_INTERNET_NEWSCALE=3;
   BLOOD_INTERNET=4;
   PHANTOM_NEWSCALE=5;
         PHANTOM=6;
          DIADEM_STFILTER=7;
          DIADEM=8;
          PHANTOM_FAR=9;
        
          
        nerving_edge_check=true;
        
        edge_length_energy=false;

        pointw=3;
             
     
        lambda=10*1440;
        randwalk=true;
        thickness=1.5;  
        min_thickness=0;  
        endpoint_offset=-0.25;
        
        maxcontinue=1;   
        iterations=100000000;           
     
          TT=1;                   
        isothreshold=0.2;
        
        DataEVRatio=100;
        DataEV3Ratio=0;
        
        max_connections=2;
        DataScaleGrad=100;
        DataScaleGradVessel=1000;
        %DataScale=(2*pi)^(3/2);
        DataScale=1;
        ConnectionScale=2;
        
        DataThreshold=-40;
        ConnectionSearchrad=15;  
        L=-40;
        
        loop_depth=50;          
        
         propprop=[1,1,1,1,1,1,1,1]; 
         
         
         EVsigncheck=true;

        ThicknessScale=1;
        PointWeightScale=1;
        ConnectionPointWeightScale=0;
        %AntiPointWeightScale=0;
        render_final=true;
        
         switchproposal=2;
               second_thickness_fact=false;
        Lprior=0;
        AngleScale=0;
        
        custom_grid_spacing=-1;
        multi_egde_check=-1;
        
                update_swc=false;
                crop=[0,0,0];       
                
PointScaleCost=0;                
        noedgescales=false;
        %thickfunc=@(x)(max(x,min_thickness));
        %thickfunc=@(x)(x);
        thickfunc=@(x,y)(x*y);
        
        HoughGamma=1;
        HoughSigma=1;
    	HoughTubeLengths=1;
        HoughTubeSteps=1;        
        
        
        point_weight=2;
        scale_power=1;
        searchrad_scales=false;
                
cT=1;                 
pT=-1;
ccT=-1;
dT=-1; %datatemp

        ealpha=0.9999;
                
        
switch SETTINGS
    
                 
  case BLOOD_INTERNET         
 %############# BLOOD DATA ############%   
                custom_grid_spacing=-1;
                
                TT=1;                   
                EVsigncheck=false;
                DataEVRatio=-1;                
                max_connections=3;
                nerving_edge_check=true;
                isothreshold=0.3;                
    


                AngleScale=-1;
                ConnectionSearchrad=1.5*max(FeatureData.scales);

                ThicknessScale=1;
                loop_depth=25;    



                PointWeightScale=2*pi*[1,1,1];
                DataScale=2;
                DataScaleGrad=100;
                DataScaleGradVessel=100;


                Lprior=-10;

                randwalk=false;      

                 lambda=numel(FeatureData.img);
                 use_saliency_map=true;
                  PointWeightOffset=[0,-5,-10];
                  %L=-20;
                  L=-10;
                  ConnectionScale=3;
                  ealpha=0.999;
                  
                  maxcontinue=1;    
                  iterations=400000000;
                  
                  
                  
                  thickness=-1/4; 
                  min_thickness=1.5/2;
                  
                  thickfunc=@(x,y)(max(x*y,min_thickness));
                  
                  %DataScaleGradVessel=500;
                  DataThreshold=-20;
                    
                  propprop=[1,1,2,3,2,2,1,1,1,1];
                 
                 propprop2=propprop;
                 render_final=false;
                 
                 
                 DataThreshold=-30;
                 PointWeightScale=4*[1,1,1];   
                 
                 ConnectionScale=1;
                 DataThreshold=-40;
                 DataScaleGradVessel=500;
                 
                 ConnectionScale=2;
                 DataThreshold=-40;
                 ConnectionSearchrad=1.1*max(FeatureData.scales);
                 
                 
                 % neu 
                 DataScaleGradVessel=100;
                 DataScaleGrad=20;
                 DataThreshold=-40;
                 %ConnectionScale=1;
                 DataThreshold=-45;
                 
                 %L=-20;
                 %PointWeightOffset=[0,L/2,L];
                 
                 
                 % HOUGH
                 
                 
         
end;        
        
        subpixel=true;
        
        
        vessel_weights=false;
         
        
        if (reset_tmp>0)
            TT=1;
            pT=-1;
            ccT=-1;
            dT=-1;
        end;
        
        T=TT;
        if pT<0
            pT=TT;
        end;
        if ccT<0
            ccT=TT;
        end;
        if dT<0
            dT=TT; %datatemp
        end;
        
         if (reset_tmp>0)
            A.Temp=TT;
            A.PointTemp=pT;
            A.ConnCostTemp=ccT;
            A.DataTemp=dT;
        end;
        
 if (exist('A'))
    if isfield(A,'params')
        for k = 1:2:length(A.params),
                eval(sprintf('%s=A.params{k+1};',A.params{k}));
        end;
    end;
 end;        
if (exist('override'))
        for k = 1:2:length(override),
                eval(sprintf('%s=override{k+1};',override{k}));
        end;
 end;         
 

%for tt=1:1
      
        docontinue=0;
        CPU=1;
        
      
        
        clear Stats;
        
         if (nargout>2)&&(~return_statistic)
             if doobar
                h=waitbar(0,'wait');
             else
                 
             end;
         end;

        while docontinue<maxcontinue
            
             
            
            
            tic
            if (exist('A'))
                params={'iterations',iterations,'edge_length_energy',edge_length_energy,'max_connections',max_connections,'vessel_weights',vessel_weights,'DataThreshold',DataThreshold,'DataScale',DataScale,'dT',A.DataTemp,'ccT',A.ConnCostTemp,'cT',A.ConnTemp,'T',A.Temp,'L',L,'lambda',lambda,'pointw',pointw,'propprop',propprop,'loop_depth',loop_depth,'randwalk',randwalk,'pT',A.PointTemp,'ConnectionSearchrad',ConnectionSearchrad,'thickness',thickness,'subpixel',subpixel,'CPU',CPU,'DataScaleGrad',DataScaleGrad,'DataScaleGradVessel',DataScaleGradVessel,'endpoint_offset',endpoint_offset,'DataEVRatio',DataEVRatio,'return_statistic',return_statistic,'ConnectionScale',ConnectionScale,'EVsigncheck',EVsigncheck,'nerving_edge_check',nerving_edge_check,'ThicknessScale',ThicknessScale,'PointWeightScale',PointWeightScale,'ConnectionPointWeightScale',ConnectionPointWeightScale,'DataEV3Ratio',DataEV3Ratio,'Lprior',Lprior,'PointWeightOffset',PointWeightOffset,'AngleScale',AngleScale,'custom_grid_spacing',custom_grid_spacing,'multi_egde_check',multi_egde_check,'noedgescales',noedgescales,'use_saliency_map',use_saliency_map,'ealpha',ealpha,'min_thickness',min_thickness,'PointScaleCost',PointScaleCost,'point_weight',point_weight,'scale_power',scale_power,'HoughGamma',HoughGamma,'HoughSigma',HoughSigma,'HoughTubeLengths',HoughTubeLengths,'HoughTubeSteps',HoughTubeSteps,'searchrad_scales',searchrad_scales,'MedialSigma',MedialSigma};
                if (return_statistic)
                    [A,Stats{docontinue+1}]=ntrack(FeatureData,A,params);
                else
                    A=ntrack(FeatureData,A,params);
                end;
            else
                params={'iterations',iterations,'edge_length_energy',edge_length_energy,'max_connections',max_connections,'vessel_weights',vessel_weights,'DataThreshold',DataThreshold,'DataScale',DataScale,'dT',dT,'ccT',ccT,'cT',cT,'T',T,'L',L,'lambda',lambda,'pointw',pointw,'propprop',propprop,'loop_depth',loop_depth,'randwalk',randwalk,'pT',pT,'ConnectionSearchrad',ConnectionSearchrad,'thickness',thickness,'subpixel',subpixel,'CPU',CPU,'DataScaleGrad',DataScaleGrad,'DataScaleGradVessel',DataScaleGradVessel,'endpoint_offset',endpoint_offset,'DataEVRatio',DataEVRatio,'return_statistic',return_statistic,'ConnectionScale',ConnectionScale,'EVsigncheck',EVsigncheck,'nerving_edge_check',nerving_edge_check,'ThicknessScale',ThicknessScale,'PointWeightScale',PointWeightScale,'ConnectionPointWeightScale',ConnectionPointWeightScale,'DataEV3Ratio',DataEV3Ratio,'Lprior',Lprior,'PointWeightOffset',PointWeightOffset,'AngleScale',AngleScale,'custom_grid_spacing',custom_grid_spacing,'multi_egde_check',multi_egde_check,'noedgescales',noedgescales,'use_saliency_map',use_saliency_map,'ealpha',ealpha,'min_thickness',min_thickness,'PointScaleCost',PointScaleCost,'point_weight',point_weight,'scale_power',scale_power,'HoughGamma',HoughGamma,'HoughSigma',HoughSigma,'HoughTubeLengths',HoughTubeLengths,'HoughTubeSteps',HoughTubeSteps,'searchrad_scales',searchrad_scales,'MedialSigma',MedialSigma};
                if (return_statistic)
                    [A,Stats{docontinue+1}]=ntrack(FeatureData,params);    
                else
                    A=ntrack(FeatureData,params);    
                    %fprintf('A=mhs3D5(FeatureData,params);    ');
                    %A=mhs3D5debug(FeatureData,params);    
                end;
            end
            
            if (docontinue>switchproposal)
                propprop=propprop2;
            end;
            
            toc
            docontinue=docontinue+1;

            if (docontinue==floor(maxcontinue/2))&&(second_thickness_fact)
                thickness_new=thickness/2;
                ConnectionSearchrad=ConnectionSearchrad+thickness_new;
                thickness=thickness_new;
            end;            
            
            
            
             AA=A;
             if (FeatureData.presmooth>0)
                AA.data(8,:)=sqrt(AA.data(8,:).^2-FeatureData.presmooth^2);
             end;
            
             if (nargout>2)&&(~return_statistic)
                 %if mod(docontinue,10)==0
                 if doobar
                    waitbar(docontinue/maxcontinue,h,num2str(floor(100*docontinue/maxcontinue)));
                 else
                     
                 end;
                 %end;
                 
                 MOVIE{docontinue}=AA;
             else
                 
                 if showplots
                  figure(3);
                 renderFDATA(FeatureData.img.^0.5,AA,'mode',3,'isothreshold',isothreshold,'preprocess',false,'thickfunc',thickfunc);

                 figure(14);renderFDATA(FeatureData.img,AA,'mode',1,'isothreshold',isothreshold,'select_tree',-1,'use_pathcolors',true,'transparent_vessel',false,'thickfunc',thickfunc);
                 end
             end;
        
                if update_swc
                    path2SWC(A,'test.swc',crop);
                end;
        end;
        
        if (nargout>2)&&(~return_statistic)
            if doobar
             close(h);
            end;
         end;


%         A.Temp=T;
%         A.ConnCostTemp=ccT;
%         A.ConnTemp=cT;
%         A.PointTemp=pT;
%         A.DataTemp=dT;

if render_final     && showplots   
                     figure(1);
             %renderFDATA(FeatureData.img,A,'mode',1,'isothreshold',isothreshold);
             renderFDATA(FeatureData.img,A,'mode',1,'isothreshold',isothreshold,'use_pathcolors',true);
             figure(2);
             renderFDATA(FeatureData.img,A,'mode',6,'isothreshold',isothreshold);
             
end;             
%end;

A.params=params;
Aout=A;
if (FeatureData.presmooth>0)
    Aout.data(8,:)=sqrt(A.data(8,:).^2-FeatureData.presmooth^2);
end;

if return_statistic
    MOVIE=Stats;
end;
