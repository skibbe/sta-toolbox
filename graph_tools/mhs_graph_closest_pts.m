function pts=mhs_graph_closest_pts(A,B,varargin)

G={A,B};

for a=1:2
    npt_attrib=size(G{a}.data,1);
    if npt_attrib<26
        missing_attrib=26-npt_attrib;
        G{a}.data=(cat(1,G{a}.data,zeros([missing_attrib,size(G{a}.data,2)])));
    end;
    G{a}.data(15,:)=a;
end;

C=mhs_graph_append(G{1},G{2});

    C.data(17,:)=0;
    C.data(19,:)=0;
    C.data(20,:)=0;
    C.data(22,:)=0;
    C.connections=zeros([6,0]);
    
    offset=(min(C.data(1:3,:),[],2));
    
    if ~any(offset<0)
        offset(:)=0;
    end;
    
    C.data(1:3,:)=C.data(1:3,:)-repmat(offset,1,size(C.data,2))+1;
    
    
    
    if ~isfield(C,'cshape')
        C.cshape=single(max(A.data(1:3,:),[],2)).'+1;
        %C.cshape=C.cshape([3,2,1]);
        %C.cshape=C.cshape
    end;
    if ~isfield(C,'scales')
        C.scales=single([min(A.data(8,:)),max(A.data(8,:))]);
    end;
    C.data=double(C.data);
    

C.cshape=C.cshape+10;    
C.cshape=max(ceil(single(max(C.data(1:3,:),[],2)).'),C.cshape);

%options={'opt_particles_per_voxel',2};    
options={'opt_particles_per_voxel',5};    

[D,all_pts]=ntrack_graph_tools(C,{'tool',4,'params',options,varargin{:}});


nptsA=size(A.data,2);
nptsB=size(B.data,2);



pts{1}=(all_pts(1:nptsA))+1;
pts{1}(pts{1}>0)=pts{1}(pts{1}>0)-nptsA;
pts{2}=(all_pts(nptsA+1:end))+1;

assert(~(max(pts{2})>nptsA))
assert(~(max(pts{1})>nptsB))