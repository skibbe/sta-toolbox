function A=mhs_detachtrees(A,varargin)



for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;



connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          

%terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
%terminalsTERMPos=A.data(1:3,terminalsTERM);

    terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
    terminalsBIF=find(terminalsBIFa);

%terminalsBIFPos=A.data(1:3,terminalsBIF);



for b=1:numel(terminalsBIF)
    indx=terminalsBIF(b);
    A.data(:,indx)=[];
    eindx=find((A.connections(1,:)==indx-1)|(A.connections(2,:)==indx-1));
    A.connections(:,eindx)=[];
    for t=1:2
        updatei=(A.connections(t,:)>indx-1);
        A.connections(t,updatei)=A.connections(t,updatei)-1;
    end;
    
    terminalsBIF(terminalsBIF>indx)=terminalsBIF(terminalsBIF>indx)-1;
end;

connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          

terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));


A.data(21,:)=0;
for b=1:numel(terminalsTERM)
    %current=connection_listGT{terminalsTERM(b)};
    current=terminalsTERM(b);
    previous=-1;
    count=1;
    %while (numel(current>1)||(count==1))
    while numel(current>1)
        A.data(21,current)=b;
        c=connection_listGT{current}(connection_listGT{current}~=previous);
        previous=current;
        current=c;
    end;
    A.data(21,current)=b;
end;




