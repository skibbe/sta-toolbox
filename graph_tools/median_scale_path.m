function A=median_scale_path(A,direct_neighbors)

if nargin<2
    direct_neighbors=true;
end;

connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          

if direct_neighbors
    for a=1:size(A.data,2)
        if numel(connection_listGT{a})>0
            A.data(8,a)=median(A.data(8,[a,connection_listGT{a}]));
        end;
    end;
else
    B=A;
    for a=1:size(A.data,2)
        if numel(connection_listGT{a})>0
            neighbors=[a,connection_listGT{a}];
            for b=1:numel(connection_listGT{a})
                current=connection_listGT{a}(b);
                candidates=connection_listGT{current};
                candidates=candidates(candidates~=a);
                neighbors=[neighbors,candidates];
            end;
            %B.data(8,a)=median(A.data(8,neighbors));
            B.data(8,a)=median(A.data(8,neighbors));
        end;
    end;
    A=B;
end;


% 
% terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
% terminalsTERMPos=A.data(1:3,terminalsTERM);
% 
% if select_tree==-1
%     terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
%     terminalsBIF=find(terminalsBIFa);
% else
%     tselections=max((repmat(A.data(21,:),size(select_tree,2),1)==repmat(select_tree',1,size(A.data(21,:),2))),[],1)==1;
%     terminalsBIFa=(cellfun(@(x)numel(x),connection_listGT)>2)&(tselections);
%     terminalsBIF=find(terminalsBIFa);
% end;
% 
% terminalsBIFPos=A.data(1:3,terminalsBIF);