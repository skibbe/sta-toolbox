function A=mhs_graph_crop(A,crop_l,crop_r)


if numel(crop_l)==1
    crop_l=[crop_l,crop_l,crop_l];
end;
if numel(crop_r)==1
    crop_r=[crop_r,crop_r,crop_r];
end;

shape=A.cshape;

%A=ntrack_graph_tools(A,{'tool',10,'crop_l',single(crop_l),'crop_r',single(shape-crop_r),'params',{'maxendpointdist',100}});
A=ntrack_graph_tools(A,{'tool',10,'crop_l',single(crop_l),'crop_r',single(shape-crop_r)});
for d=1:3
    A.data(d,:)=A.data(d,:)-crop_l(d);
end;
A.cshape=A.cshape-crop_l-crop_r;