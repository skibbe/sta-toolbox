function pos=mhs_get_bif_pos(A,mode)
if nargin<2
   mode=1; 
end

 connection_listGT={};
    for d=1:size(A.data,2)
        connection_listGT{d}=[];
    end;
    for d=1:size(A.connections,2)
        pointIDA=A.connections(1,d)+1;
        pointIDB=A.connections(2,d)+1;
        connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
        connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
    end;          


switch(mode)    
    case 1
        terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
    otherwise
            terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)==mode;
end
    terminalsBIF=find(terminalsBIFa);

    pos=A.data(1:3,terminalsBIF);