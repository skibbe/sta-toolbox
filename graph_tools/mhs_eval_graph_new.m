% function score=mhs_eval_graph(GT,B)
function score=mhs_eval_graph_new(GT,B,varargin)



if isempty(B)
    score.TP=0;
    score.FP=1;
    score.success=false;
    return;
end;

coll_sharp=2;
spacing=2;
graphs=true;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

    

    


    A=GT;
    A=extract_paths(A,'spacing',spacing);
    B=extract_paths(B,'spacing',spacing);
    
    
    
    B_back=B;

    A.data=single(A.data);
    B.data=single(B.data);

    posTA=A.data(1:3,:).';
    posTB=B.data(1:3,:).';
     
    nptsA=size(A.data,2);
    nptsB=size(B.data,2);

    scaleA=max(A.data(8,:),eps);
    scaleB=max(B.data(8,:),eps);



    A=upid(A);
    B=upid(B);
    pathid_A=A.data(21,:);
    pathid_B=B.data(21,:);

    pidA=[1:nptsA];
    pidB=[1:nptsB];


    
    
   
    
    upidA=unique(pathid_A);
    upidB=unique(pathid_B);
    
    assert(~any(upidA==0))
    assert(~any(upidB==0))

    
  
    collision_search_rad=max(A.data(8,:))*1.1;
    
    
    pts=mhs_graph_closest_pts(A,B,'collision_search_rad',collision_search_rad,'coll_sharp',coll_sharp);
    
    
    best_coll_AwithB_particle_id=pts{1};
    valid=(best_coll_AwithB_particle_id>0);
    best_coll_AwithB_path_id=zeros(size(valid));
    best_coll_AwithB_path_id(valid)=pathid_B(best_coll_AwithB_particle_id(valid));
    
    best_coll_AwithB_particle_id=best_coll_AwithB_particle_id.';
    best_coll_AwithB_path_id=best_coll_AwithB_path_id.';
    
    best_coll_BwithA_particle_id=pts{2};
    valid=(best_coll_BwithA_particle_id>0);
    best_coll_BwithA_path_id=zeros(size(valid));
    best_coll_BwithA_path_id(valid)=pathid_A(best_coll_BwithA_particle_id(valid));
    
if false
    scaleMatA=repmat(scaleA,nptsB,1);
    scaleMatB=repmat(scaleB,nptsA,1).';

    colldist=(2*min(scaleMatA,scaleMatB)).^2;

    clear scaleMatA scaleMatB;

    
    path_id_MatA=repmat(pathid_A,nptsB,1);
    path_id_MatB=repmat(pathid_B,nptsA,1).';    
    
    D=(distmat(posTA,posTB));
    
    clear postTB postTA;
    
    coll=sparse(colldist>D);

    p_id_MatB=repmat(pidB,nptsA,1).';
     
    best_coll_AwithB=(~(D>(repmat(min(D,[],1),nptsB,1)))) & coll;
    best_coll_AwithB_path_id=max(full(best_coll_AwithB).*path_id_MatB,[],1);
    best_coll_AwithB_particle_id=max(full(best_coll_AwithB).*p_id_MatB,[],1);
    
    clear path_id_MatB best_coll_AwithB;
    
    p_id_MatA=repmat(pidA,nptsB,1);
    
    best_coll_BwithA=(~(D>(repmat(min(D,[],2),1,nptsA)))) & coll;
    best_coll_BwithA_path_id=max(full(best_coll_BwithA).*path_id_MatA,[],2);
    best_coll_BwithA_particle_id=max(full(best_coll_BwithA).*p_id_MatA,[],2);
    
    clear path_id_MatA best_coll_BwithA;
    
    clear D colldist coll;
end;    

  
    BPID=bif_per_id(A);
    
    AT=terminal_pos(A);
    BT=terminal_pos(B);
    B_back=B;
    A_back=A;
    
    
    A.data(21,:)=1;
    B.data(21,:)=2;
    Bbest=B;
    Bbest_paths=B;
    %TDIST=single(zeros([2,size(B.data,2)]));
    TDIST_max={};
    TDIST_mean={};
    
    TDIST_max_all={};
    TDIST_mean_all={};
    %FP_TP=B;
    for a=1:5
        Bbest2{a}=B;
        Bbest2{a}.data(21,:)=2;
    end;
    
    for a=1:5
        Bbest3{a}=B;
        Bbest3{a}.data(21,:)=2;
    end;
    
    
    
    count=1;
    for idA=upidA
        fprintf('%d %d ',count,numel(upidA));
        select_A=(pathid_A==idA);
        coll_apthsAB_all=best_coll_AwithB_path_id(select_A);
        coll_pathsAB=unique(coll_apthsAB_all);
        coll_pathsAB=coll_pathsAB(coll_pathsAB>0);
        
        if numel(coll_pathsAB)>0
        
            cmem=zeros(1,numel(coll_pathsAB));
            for cp=1:numel(coll_pathsAB)
                cmem(cp)=sum(coll_apthsAB_all(:)==coll_pathsAB(cp));
            end;

            [members,indx]=max(cmem);

            best_path_in_B=coll_pathsAB(indx);
            
            
            %Bindx=best_coll_AwithB_particle_id(best_coll_AwithB_path_id==best_path_in_B);% & select_A;
            Bindx=best_coll_AwithB_particle_id((best_coll_AwithB_path_id==best_path_in_B) & select_A);
            

            total_collisions_A=sum(coll_apthsAB_all>0);
            total_elements_A=sum(pathid_A==idA);

            %collisions_AB_best_non_unique=sum(coll_apthsAB_all==best_path_in_B);
            collisions_AB_best=numel(unique(Bindx));
            collisions_AB_best_non_unique=numel(Bindx);
            
            total_elements_B=sum(pathid_B==best_path_in_B);
            
            
            select_B=(pathid_B==best_path_in_B);
            Aindx=best_coll_BwithA_particle_id((best_coll_BwithA_path_id==idA).' & (select_B));% & select_A;
            collisions_BA_best=numel(unique(Aindx));
            collisions_BA_best_non_unique=numel(Aindx);
            
            
            
            
            fprintf('[%d %d %d %d]\n',collisions_AB_best_non_unique,collisions_AB_best,collisions_BA_best_non_unique,collisions_BA_best);

            cTP=collisions_AB_best_non_unique./(total_elements_A+eps);
            cFP=(total_elements_B-collisions_BA_best_non_unique)./(total_elements_B+eps);
            
            pidB_=unique(B_back.data(21,Bindx));
            
            pidA_=idA;
            term_dist_max=max(min(sqrt(distmat(BT(1:3,BT(4,:)==pidB_).',AT(1:3,AT(4,:)==pidA_).')),[],1));
            term_dist_mean=mean(min(sqrt(distmat(BT(1:3,BT(4,:)==pidB_).',AT(1:3,AT(4,:)==pidA_).')),[],1));
            if numel(TDIST_max_all)>=pidA_
                TDIST_max_all{pidA_}=min(term_dist_max,TDIST_max_all{pidA_});
                TDIST_mean_all{pidA_}=min(term_dist_mean,TDIST_mean_all{pidA_});                
            else
                TDIST_max_all{pidA_}=term_dist_max;
                TDIST_mean_all{pidA_}=term_dist_mean;                
            end
            
            if (cTP>0.9) && (cFP<0.1)
                Bbest.data(21,Bindx)=3;
                Bbest_paths.data(21,B_back.data(21,:)==pidB_)=3;
                
                assert(numel(pidB_)==1)
                
               
                TDIST_max{pidB_}=term_dist_max;
                TDIST_mean{pidB_}=term_dist_mean;
                %TDIST(1,Bindx)=term_dist_max;
                %TDIST(2,Bindx)=term_dist_mean;
            end;
            for e=1:5
                if (cTP>1-0.1*e) && (cFP<0.1*e)
                    Bbest2{e}.data(21,B_back.data(21,:)==best_path_in_B+1)=1;
                    %Bworst{e}.data(21,Aindx)=5;
                end;
                if (cFP<0.1*e)
                    Bbest3{e}.data(21,B_back.data(21,:)==best_path_in_B+1)=1;
                    %Bworst{e}.data(21,Aindx)=5;
                end;
            end;
            %if (cTP>0.9) && (cFP<0.1)

                    %TP(count)=collisions_AB_best./(total_elements_A+eps);
                    %TP(count)=collisions_BA_best./(total_elements_A+eps);
                    TP(count)=cTP;%collisions_AB_best_non_unique./(total_elements_A+eps);

                    FP(count)=cFP;
                    %FP(count)=(total_elements_B-collisions_AB_best)./total_elements_B;

                    BP(count)=best_path_in_B-1;

                    A.data(21,Aindx)=4;
                    B.data(21,Bindx)=3;
                    
                    BIFcount(count)=sum(idA==BPID);
%             else
%                     TP(count)=0;
%                     FP(count)=1;
%                     BP(count)=-1;
%             end;
        else
            TP(count)=0;
            FP(count)=1;
            BP(count)=-1;
            BIFcount(count)=sum(idA==BPID);
        end;
        
        
        count=count+1;

    end;
    
 Bbest.data(21,1)=-1;    
    

 
score.TP=TP;
score.FP=FP;
score.BP=BP;
score.BIFcount=BIFcount;
score.TDIST_max=TDIST_max;
score.TDIST_mean=TDIST_mean;
score.TDIST_max_all=TDIST_max_all;
score.TDIST_mean_all=TDIST_mean_all;
    
if graphs
    
    score.B=B_back;
    Bbest.data=double(Bbest.data);
    for a=1:5
        Bbest2{a}.data(21,1)=-1; 
        Bbest2{a}.data=double(Bbest2{a}.data);
        
        Bbest3{a}.data(21,1)=-1; 
        Bbest3{a}.data=double(Bbest3{a}.data);
    end;
    score.Best=Bbest;
    score.Best2=Bbest2;
    score.Best3=Bbest3;
    score.Bbest_paths=Bbest_paths; 
    %score.TDIST=TDIST;
    
    B.data(1:3,:)=B.data(1:3,:)+1;
    score.Cvis=mhs_graph_append(A,B);
    score.Cvis.data=double(score.Cvis.data);
end;
score.success=true;    




fprintf('---\n');




function B=upid(A)

pathid_A=A.data(21,:);
upathid_A=unique(pathid_A);
B=A;
count=1;
for a=upathid_A
    B.data(21,pathid_A==a)=count;
    count=count+1;
end;


function BPID=bif_per_id(A)
connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          



terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));

terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
%terminalsBIF=find(terminalsBIFa);

BPID=unique(find(A.data(21,terminalsBIFa)));


function AT=terminal_pos(A)
connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          



terminalsTERM=(1==cellfun(@(x)numel(x),connection_listGT));

AT=A.data([1:3,21],terminalsTERM);



