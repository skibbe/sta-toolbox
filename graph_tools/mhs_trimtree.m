function A=mhs_trimtree(A,varargin)
%function [A,path_set]=mhs_trimtree(A,varargin)


select_tree=-1;
select_tree_ids={-2,0};
min_pathlengt=-1;
max_pathlengt=inf;
offset=[0,0,0];
addoffset=[0,0,0];
remove_nonconnected=false;
selection_invert=false;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


 uids=unique(A.data(21,:));                
 num_paths=numel(uids);

 new_ids=[1:num_paths];
 lids=A.data(21,:);
 for a=1:num_paths
        A.data(21,lids==uids(a))=new_ids(a);
 end;

%  if exist('path_set')
%      
%  end


if exist('pointpos')
    pos=A.data(1:3,:);
    %pos(:,A.data(21,:)==1)=inf;
    pos(:,A.data(22,:)~=1)=inf;    
    
    [bla,vindx]=sort(distmat(pointpos{1}-repmat(offset,size(pointpos{1},1),1),pos'),1);
    
    nnn=size(A.data,2);
    if numel(pointpos)>2
        nnn=pointpos{3};
    end;
    
    valid_indx=vindx(1:nnn,:);
    valid_dist=bla(1:nnn,:)<pointpos{2}^2;
    valid_indx=valid_indx(valid_dist(:));
    select_tree=unique(A.data(21,valid_indx));
    fprintf('printing tree %d\n',select_tree);
else
    if min(select_tree)>0
        unum=unique([A.data(21,:),1]);

        for a=1:numel(unum)
            tmembers(a)=sum([A.data(21,:),1]==unum(a));
        end;
        [v,indx]=sort(tmembers,'descend');
        select_tree_=select_tree;
        select_tree=unum(indx(select_tree));
        fprintf('printing tree %d-> %d\n',[select_tree_;select_tree]);
        if numel(select_tree)==1
            fprintf('total points %d, selected tree %d\n',size(A.data,2),sum(A.data(21,:)==select_tree));
        end;

    elseif min(select_tree_ids{1})>-2
        select_tree=select_tree_ids{1};
        fprintf('printing tree %d\n',select_tree);
    elseif (min_pathlengt>0)||(max_pathlengt<inf)

        unum=unique([A.data(21,:),1]);
        
        %no sinlge particles
        unum=unum(unum>-1);

        for a=1:numel(unum)
            tmembers(a)=sum([A.data(21,:),1]==unum(a));
        end;

        
        select_tree=unum((tmembers>=min_pathlengt)&(tmembers<=max_pathlengt));
        
        fprintf('printing tree %d\n',select_tree);
    end;
end;

%removed_paths=unique([A.data(21,:),1]);removed_paths(select_tree)=[];
%path_set=[select_tree,removed_paths];


if isempty(select_tree)
    A=[];
    return
end;


if remove_nonconnected
        connection_listGT={};
        for d=1:size(A.data,2)
            connection_listGT{d}=[];
        end;
        for d=1:size(A.connections,2)
            pointIDA=A.connections(1,d)+1;
            pointIDB=A.connections(2,d)+1;
            connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
            connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
        end;          

        valid_particles=(cellfun(@(x)numel(x),connection_listGT)>0);
        %valid_particles=max((repmat(A.data(21,:),numel(select_tree),1)==repmat(select_tree',1,size(A.data,2))),[],1);
else
        valid_particles=max((repmat(A.data(21,:),numel(select_tree),1)==repmat(select_tree',1,size(A.data,2))),[],1);
end;
    old_ids=1:size(A.data,2);
    new_ids=old_ids;
    new_ids(:)=-1;
    new_ids(valid_particles)=1:sum(valid_particles);


    valid_edges1=valid_particles(A.connections(1,:)+1)&valid_particles(A.connections(2,:)+1);
    valid_edges2=valid_particles(A.connections(1,:)+1)|valid_particles(A.connections(2,:)+1);
%assert(min(valid_edges1==valid_edges2)==1);
%assert(size(A.connections,2)==numel(valid_edges1));
    A.connections(1,valid_edges1)=new_ids(A.connections(1,valid_edges1)+1)-1;
    %A.connections(2,valid_edges2)=new_ids(A.connections(2,valid_edges1)+1)-1;
    A.connections(2,valid_edges1)=new_ids(A.connections(2,valid_edges1)+1)-1;

    A.data(:,~valid_particles)=[];
    A.connections(:,~valid_edges1)=[];




A.data(1,:)=A.data(1,:)+addoffset(1);
A.data(2,:)=A.data(2,:)+addoffset(2);
A.data(3,:)=A.data(3,:)+addoffset(3);


A.data(9,:)=A.data(9,:)+addoffset(1);
A.data(10,:)=A.data(10,:)+addoffset(2);
A.data(11,:)=A.data(11,:)+addoffset(3);

A.data(12,:)=A.data(12,:)+addoffset(1);
A.data(13,:)=A.data(13,:)+addoffset(2);
A.data(14,:)=A.data(14,:)+addoffset(3);


% for t=1:numel(select_tree)
%     tid=select_tree(t);
%         if isfield(Data,'featuredata')
%             [v,indx]=min(sum((Data.featuredata.data(1:3,:)+1-repmat(Data.slice',1,size(Data.featuredata.data,2))).^2,1));
%             Data.featuredata.data(:,indx)=[];
%             
%             eindx=find((Data.featuredata.connections(1,:)==indx-1)|(Data.featuredata.connections(2,:)==indx-1));
%             %eindx=find((Data.featuredata.connections(1,:)==indx-1)|(Data.featuredata.connections(2,:)==indx-1));
%             Data.featuredata.connections(:,eindx)=[];
%             for t=1:2
%                 updatei=(Data.featuredata.connections(t,:)>indx-1);
%                 Data.featuredata.connections(t,updatei)=Data.featuredata.connections(t,updatei)-1;
%             end;
%             fprintf(':-)\n');
%         end;
% end;        