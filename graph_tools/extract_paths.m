%
%   [paths,B]=extract_paths(A,'spacing',1,'smoothing',5,'smoothing_scale',5,'nice_bifucations',true);
%
function [paths,B]=extract_paths_new(A,varargin)

spacing=1;
recur=0;

smoothing=-1;
smoothing_scale=-1;
nice_bifucations=false;
refine=0;
oldversion=false;

verbose=false;



for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


connection_list={};
for d=1:size(A.data,2)
    connection_list{d}=[];
    
end;

 fprintf('creating connection list ..');
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    
    connection_list{pointIDA}=[connection_list{pointIDA},pointIDB];
    connection_list{pointIDB}=[connection_list{pointIDB},pointIDA];
end;                        
                        
fprintf('done\n');


pathid=unique(A.data(21,:));
num_paths=numel(pathid);
sub_paths={};
sub_paths_ids={};
sub_paths_pos={};
sub_paths_dir={};
sub_paths_scale={};
sub_data_count=1;
node_ids=[1:size(A.data,2)];

point_in_seglist([1:size(A.data,2)])=false;
for a=1:num_paths
    if (mod(a,10)==0) && verbose
        fprintf('%d %d\n',a,num_paths);
    elseif (mod(a,max(ceil(num_paths/100),10))==0)
        fprintf('%d %d\n',a,num_paths);
    end;
    spathdid=pathid(a);
    if spathdid>0
        %B=A.data(:,A.data(21,:)==spathdid);
        connlist={connection_list{find(A.data(21,:)==spathdid)}};
        all_pt_ids=find(A.data(21,:)==spathdid);
        %all_pt_ids=unique([connlist{:},list_pt_ids(:)]);
        %start_nodes=(cellfun(@numel,connection_list)>2)|(cellfun(@numel,connection_list)==1);
        %start_nodes=(cellfun(@numel,connection_list)~=2);
        start_nodes=(cellfun(@numel,connlist)~=2);
        %start_nodes=find(start_nodes(all_pt_ids));
        start_nodes=unique(all_pt_ids(start_nodes));

        %num_sub_apths=1;
        for b=1:numel(start_nodes)
            if ~point_in_seglist(start_nodes(b))
                assert(numel(connection_list{start_nodes(b)})~=2)
                
                for c=1:numel(connection_list{start_nodes(b)})
                    nex_pt=connection_list{start_nodes(b)}(c);
                    if ~point_in_seglist(nex_pt)
                                prev_pt=start_nodes(b);
                                stop=false;
                                sub_path=[];
                                while ~stop
                                    %fprintf('%d\n',nex_pt);
                                    if (~point_in_seglist(nex_pt)) 
                                        sub_path=[sub_path,nex_pt];
                                        new_pt=connection_list{nex_pt}(connection_list{nex_pt}~=prev_pt);
                                        prev_pt=nex_pt;
                                        nex_pt=new_pt;
                                        if (numel(nex_pt)==1)%&&(numel(connection_list{nex_pt})==2)
                                            point_in_seglist(prev_pt)=true;
                                        else

                                            stop=true;
                                        end;
                                    else
                                        stop=true;
                                    end;
                                end;
                                if 1 % numel(sub_path)>0
                                    %sub_path=[start_nodes(b),sub_path,nex_pt];
                                    sub_path=[start_nodes(b),sub_path];
                                    path_pos=A.data(1:3,sub_path);

                %                     div_pts=find(max(abs(imfilter(double(imfilter(path_pos(1:3,:),[-1,1],'replicate')>0),[-1,1],'replicate'))>0,[],1));
                %                     if numel(div_pts)>0
                %                         div_pts=div_pts+1;
                %                     end;
                %                     div_pts=unique([1,div_pts,numel(sub_path)]);
                %                     for e=1:numel(div_pts)-1
                %                         %div_pts
                %                         sub_path_t=sub_path([div_pts(e):div_pts(e+1)]);
                %                         sub_paths={sub_paths{:},sub_path_t};   
                %                         sub_paths_pos={sub_paths_pos{:},A.data(1:3,sub_path_t)};
                %                         sub_paths_dir={sub_paths_dir{:},A.data(4:6,sub_path_t)};
                %                         sub_paths_ids={sub_paths_ids{:},spathdid};
                %                     end;

                                     if oldversion   
                                         sub_paths={sub_paths{:},sub_path};   
                                         sub_paths_pos={sub_paths_pos{:},A.data(1:3,sub_path)};
                                         sub_paths_dir={sub_paths_dir{:},A.data(4:6,sub_path)};
                                         sub_paths_ids={sub_paths_ids{:},[spathdid,node_ids(sub_path(1)),node_ids(sub_path(end))]};
                                         sub_paths_scale={sub_paths_scale{:},A.data(8,sub_path)};   
                                     else
                                         sub_paths{sub_data_count}=sub_path;   
                                         sub_paths_pos{sub_data_count}=A.data(1:3,sub_path);
                                         sub_paths_dir{sub_data_count}=A.data(4:6,sub_path);
                                         sub_paths_ids{sub_data_count}=[spathdid,node_ids(sub_path(1)),node_ids(sub_path(end))];
                                         sub_paths_scale{sub_data_count}=A.data(8,sub_path);   
                                         sub_data_count=sub_data_count+1;
                                     end;
                                    %sub_paths={sub_paths{:},sub_path};
                                    %sub_paths_pos={sub_paths_pos{:},A.data(1:3,sub_path)};

                                end
                    end;
                end;
            end;
        end;
    end;   
end;
%if verbose
fprintf('\n');
%end;

paths.sub_paths=sub_paths;
paths.sub_paths_pos=sub_paths_pos;

newpaths={};
%newpaths_dir={};
newpaths_scale={};
new_path_data_count=1;

for a=1:numel(sub_paths)
    if (mod(a,10)==0) && verbose
        fprintf('%d %d\n',a,numel(sub_paths));
    end;
    sub_path=sub_paths{a};
    sub_path_p=sub_paths_pos{a};
    sub_path_d=sub_paths_dir{a};
    
    %minsteps=ceil(max(abs(sub_path_p(:,end)-sub_path_p(:,1))));
    
    [v,indx]=max(max(abs(double(imfilter(sub_path_p,[-1,1],'replicate'))),[],2));
    
    if exist('plength')
    plength=sum(sqrt(sum((sub_path_p(:,1:end-1)-sub_path_p(:,2:end)).^2,1)));
        minsteps=ceil(plength/spacing);
    else
        minsteps=ceil(v*size(sub_path_p,2)/spacing);
    end;
    minsteps=max(minsteps,3);
    
    %stepw=(sub_path_p(:,end)-sub_path_p(:,1))/(v*numel(sub_path));
    %stepw=(sub_path_p(:,end)-sub_path_p(:,1))/(minsteps);
    %Interp_posX=[sub_path_p(1,1):stepw(1):sub_path_p(1,end)];
    %Interp_posY=[sub_path_p(2,1):stepw(2):sub_path_p(2,end)];
    %Interp_posZ=[sub_path_p(3,1):stepw(3):sub_path_p(3,end)];
    
    %[Interp_posX;Interp_posY;Interp_posZ]
    
    
    posI=[1:numel(sub_path)];
    Interp_posI= [1:(numel(sub_path)-1)/(minsteps):numel(sub_path)];
    if numel(Interp_posI)==2
            assert(false)
    end;
    
    W=0.1;
    
    if nice_bifucations
       if numel(connection_list{sub_path(1)})>1
           Interp_posI=[1,((1-W)+W*Interp_posI(2)),Interp_posI(2:end)];
           %Interp_posI=[Interp_posI(1:end-2),Interp_posI(end-2),Interp_posI(end-2)+(0.01*Interp_posI(end)+0.99*Interp_posI(end-2)),Interp_posI(end)];
       end;
       if numel(connection_list{sub_path(end)})>1
           Interp_posI=[Interp_posI(1:end-1),W*Interp_posI(end-1)+(1-W)*numel(sub_path),numel(sub_path)];
           %Interp_posI=[Interp_posI(1:end-2),Interp_posI(end-2),Interp_posI(end-2)+(0.01*Interp_posI(end)+0.99*Interp_posI(end-2)),Interp_posI(end)];
           
       end;
       %Interp_posI=[1,(0.99+0.01*Interp_posI(2)),Interp_posI(2:end-2),Interp_posI(end-2)+(0.01*Interp_posI(end)+0.99*Interp_posI(end-2)),Interp_posI(end)];
    end;
    %Interp_posI=[Interp_posI(1),(0.01*Interp_posI(end)+0.99*Interp_posI(end-2)),Interp_posI(end),Interp_posI(end-2)+(0.01*Interp_posI(end)+0.99*Interp_posI(end-2)),Interp_posI(end)];
    
    
    intC=[];
    %intD=[];
    for b=1:3
        %intC=[intC;interp1(posI,sub_path_p(b,:),Interp_posI,'cubic')];
        %intC=[intC;interp1(posI,sub_path_p(b,:),Interp_posI,'spline')];
        if smoothing>0
            kernel=exp(-([0:4*smoothing]-2*smoothing).^2/(2*smoothing));kernel=kernel/sum(kernel(:));
            sub_p_s=imfilter(sub_path_p(b,:),kernel,'replicate');
        else
            sub_p_s=sub_path_p(b,:);
        end;
        %sub_p_s=imfilter(sub_path_p(b,:),[0.05,0.15,0.6,0.15,0.05],'replicate');
        intC=[intC;interp1(posI,sub_p_s,Interp_posI,'spline')];
        
       % intD=[intD;interp1(posI,sub_path_d(b,:),Interp_posI,'cubic')];
    end;
    
    %intE=interp1(posI,sub_paths_scale{a},Interp_posI,'cubic');
    
    
    if smoothing_scale>0
        kernel=exp(-([0:4*smoothing_scale]-2*smoothing_scale).^2/(2*smoothing_scale));kernel=kernel/sum(kernel(:));
        sub_p_s=imfilter(sub_paths_scale{a},kernel,'replicate');
    else
        sub_p_s=sub_paths_scale{a};
    end;
    
    intE=interp1(posI,sub_p_s,Interp_posI,'spline');

    if oldversion 
        newpaths_scale={newpaths_scale{:},intE};
        newpaths={newpaths{:},intC};
    else
        newpaths_scale{new_path_data_count}=intE;
        newpaths{new_path_data_count}=intC;
        new_path_data_count=new_path_data_count+1;
    end;
        
    
    %intD=intD./repmat(sqrt(sum(intD.^2,1)),3,1);
    %newpaths_dir={newpaths_dir{:},intD};
    
end;
if verbose
fprintf('\n');
end;
paths.new_paths=newpaths;
paths.sub_paths_ids=sub_paths_ids;
%paths.newpaths_dir=newpaths_dir;
paths.newpaths_scale=newpaths_scale;

tmp=[sub_paths_ids{:}];
terminals=unique([tmp(2:3:end),tmp(3:3:end)]);
%terminals=unique([tmp(2:3:end)]);

terminal_mapping=zeros(size(node_ids));
terminal_mapping(terminals)=[1:numel(terminals)];

numpts=numel(terminals)+size([newpaths{:}],2)-2*numel(newpaths);

clear B;
B.data=zeros(26,numpts);

count=numel(terminals)+1;
new_conn={};
conn_count=1;

for n=1:numel(newpaths)
    %if mod(n,10)==0
    if verbose
        fprintf('path %d %d [%d]:',n,numel(newpaths),size(newpaths{n},2));
    end;
    %end;
    B.data(1:3,terminal_mapping((sub_paths_ids{n}(2))))=newpaths{n}(:,1);
    %B.data(4:6,terminal_mapping((sub_paths_ids{n}(2))))=newpaths_dir{n}(:,1);
    
    B.data(4:6,terminal_mapping((sub_paths_ids{n}(2))))=newpaths{n}(:,2)-newpaths{n}(:,1);
    B.data(4:6,terminal_mapping((sub_paths_ids{n}(2))))=B.data(4:6,terminal_mapping((sub_paths_ids{n}(2))))/norm(B.data(4:6,terminal_mapping((sub_paths_ids{n}(2)))));
    B.data(21,terminal_mapping((sub_paths_ids{n}(2))))=sub_paths_ids{n}(1);
    
    
    B.data(1:3,terminal_mapping((sub_paths_ids{n}(3))))=newpaths{n}(:,end);
    B.data(4:6,terminal_mapping((sub_paths_ids{n}(3))))=newpaths{n}(:,end)-newpaths{n}(:,end-1);
    B.data(4:6,terminal_mapping((sub_paths_ids{n}(3))))=B.data(4:6,terminal_mapping((sub_paths_ids{n}(3))))/norm(B.data(4:6,terminal_mapping((sub_paths_ids{n}(3)))));
    
    
    B.data(8,terminal_mapping((sub_paths_ids{n}(2))))=paths.newpaths_scale{n}(1);
    B.data(8,terminal_mapping((sub_paths_ids{n}(3))))=paths.newpaths_scale{n}(end);
    
    %B.data(8,ID)= scale
    
    %B.data(4:6,terminal_mapping((sub_paths_ids{n}(3))))=newpaths_dir{n}(:,end);
    B.data(21,terminal_mapping((sub_paths_ids{n}(3))))=sub_paths_ids{n}(1);
    
    for b=2:size(newpaths{n},2)-1
        if (mod(b,ceil(size(newpaths{n},2)/10))==0) && verbose
         fprintf('.');
        end;
        if b==2
           new_conn{conn_count}=[terminal_mapping((sub_paths_ids{n}(2))),count];
           conn_count=conn_count+1;
        elseif b==size(newpaths{n},2)-1
           new_conn{conn_count}=[count,terminal_mapping((sub_paths_ids{n}(3))),count-1,count];
           conn_count=conn_count+1;
           
           new_conn{conn_count}=[count-1,count];
           conn_count=conn_count+1;
        else
           new_conn{conn_count}=[count-1,count];
           conn_count=conn_count+1;           
       end;
       
%        if b==2
%            new_conn={new_conn{:},[terminal_mapping((sub_paths_ids{n}(2))),count]};
%         elseif b==size(newpaths{n},2)-1
%            new_conn={new_conn{:},[count,terminal_mapping((sub_paths_ids{n}(3)))]};
%            new_conn={new_conn{:},[count-1,count]};
%         else
%         new_conn={new_conn{:},[count-1,count]};
%        end;
       
        assert(min(new_conn{end})>0)
        B.data(1:3,count)=newpaths{n}(:,b);
        
        B.data(4:6,count)=newpaths{n}(:,b)-newpaths{n}(:,b-1);
        L=norm(B.data(4:6,count));
        if (L>eps)
            B.data(4:6,count)=B.data(4:6,count)/(L+eps);
        else
            B.data(4:6,count)=[1,0,0];
        end;
        
        B.data(8,count)=paths.newpaths_scale{n}(b);
        
        %B.data(4:6,count)=newpaths_dir{n}(:,b);
        B.data(21,count)=sub_paths_ids{n}(1);
        count=count+1;
    end;
    if verbose
    fprintf('\n');
    end;
end;
%fprintf('\n');
% % %count=numel(terminals)+1;
% % count=1;
% % new_conn={};
% % for n=1:numel(newpaths)
% % %     B.data(1:3,terminal_mapping((sub_paths_ids{n}(2))))=newpaths{n}(:,1);
% % %     B.data(4:6,terminal_mapping((sub_paths_ids{n}(2))))=newpaths_dir{n}(:,1);
% % %     B.data(21,terminal_mapping((sub_paths_ids{n}(2))))=sub_paths_ids{n}(1);
% % %     B.data(1:3,terminal_mapping((sub_paths_ids{n}(3))))=newpaths{n}(:,end);
% % %     B.data(4:6,terminal_mapping((sub_paths_ids{n}(3))))=newpaths_dir{n}(:,end);
% % %     B.data(21,terminal_mapping((sub_paths_ids{n}(3))))=sub_paths_ids{n}(1);
% %     
% %     %for b=2:size(newpaths{n},2)-1
% %     for b=1:size(newpaths{n},2)
% %         %if b==2
% %         %    new_conn={new_conn{:},[terminal_mapping((sub_paths_ids{n}(2))),count]};
% %         %elseif b==size(newpaths{n},2)-1
% %         %    new_conn={new_conn{:},[count,terminal_mapping((sub_paths_ids{n}(3)))]};
% %         %else
% %         %new_conn={new_conn{:},[count-1,count]};
% %        % end;
% %        if b>1
% %         new_conn={new_conn{:},[count-1,count]};
% %          assert(min(new_conn{end})>0)
% %        end
% % %        assert(min(new_conn{end})>0)
% %         B.data(1:3,count)=newpaths{n}(:,b);
% %         B.data(4:6,count)=newpaths_dir{n}(:,b);
% %         B.data(21,count)=sub_paths_ids{n}(1);
% %         count=count+1;
% %     end;
% % end;

for ID=1:size(B.data,2)
    if (mod(ID,10)==0) && verbose
        fprintf('.');
    end;
    pos=B.data(1:3,ID);
    B.data(9:11,ID)=pos+B.data(4:6,ID);
    B.data(12:14,ID)=pos-B.data(4:6,ID);
    B.data(19:20,ID)=1;
end;
if verbose
fprintf('\n');
end;
B.connections=zeros(6,numel(new_conn));

for n=1:numel(new_conn)
    if (mod(n,10)==0) && verbose
            fprintf('.');
    end;
         B.connections(1,n)=new_conn{n}(1)-1;
         B.connections(2,n)=new_conn{n}(2)-1;
         B.data(19:20,new_conn{n}(1))=1;
         B.data(22,new_conn{n}(1))=1;
         B.data(22,new_conn{n}(2))=1;
end;
if verbose
fprintf('\n');
end;

inv_terminal_mapping=find(terminal_mapping>0);

for n=1:numel(terminals)
    %if sum(A.data(19:20,inv_terminal_mapping(n)))==1
        %fprintf('%d %d\n',n,inv_terminal_mapping(n));
        %B.data(19,inv_terminal_mapping(n))=0;
    %end;
end;


if recur>0
    [paths,B]=extract_paths(B,varargin{:},'recur',recur-1);
end;




if isfield(A,'particle_thickness')
    B.particle_thickness=A.particle_thickness;
end;

if any( min(B.data(1:3,:),[],2)<0)
    warning('psoitions < 0 , stting them to 0');
    B.data(1:3,:)=max(B.data(1:3,:),0);
end;



% 
% if size(B.data,1)<26
%  B.data=(cat(1,B.data,zeros([26-size(B.data,1),size(B.data,2)])));    
% end;

paths=B;
% 
% if refine>0
%     [paths,B]=extract_paths(A,varargin)
% 
% end;


% B.connections=zeros(4,edges);
% 
% count=1;
% for a=1:numel(traces)
%     for b=1:numel(traces(a).NID)
%         if traces(a).NID(b)>traces(a).ID
%         B.connections(1,count)=traces(a).ID;
%         B.connections(2,count)=traces(a).NID(b);
%         B.data(19:20,B.connections(2,count)+1)=1;
%         count=count+1;
%         end;
%     end;
% end;
% 
% 
% 
% clear B
% B.data=zeros(22,numel(traces));
% 
% edges=0;
% for a=1:numel(traces)
%     fprintf('%d %d\n',a,numel(traces));
%     ID=traces(a).ID+1;
%     pos=traces(a).pos;
%     pos=pos([2,1,3]);
%     B.data(1:3,ID)=pos;
%     
%     B.data(4:6,ID)=traces(a).R*[0,0,1]';
%     B.data(7,ID)=0;
%     B.data(8,ID)=(traces(a).scale(1)+traces(a).scale(2))/2;
%     
%     B.data(9:11,ID)=pos+B.data(4:6,ID)';
%     B.data(12:14,ID)=pos-B.data(4:6,ID)';
%     
%     
%     B.data(21,ID)=traces(a).PATHID;
%     
%     B.data(end,ID)=1;
%     
%     edges=edges+sum(traces(a).NID>traces(a).ID);
% end;
% 
% 
% 
% %17,18