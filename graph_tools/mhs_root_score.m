% [root_score,Avis]=mhs_root_score(A);
% ntrack_viewer(Avis);
function [root_score,Avis]=mhs_root_score(A)

fid = fopen(filename);
s = textscan(fid,'%s','Delimiter','\n');
s = s{1};
%s{1}=[s{1},'<root>'];
%s{end+1}='</root>';
fclose(fid);
clear neuro_data B
count =1;
pathid=0;


for a=1:numel(s)
    if numel(s{a})>0
        if s{a}(1)~='#'
             vec=str2num(s{a});
             if vec(end)<0
                 pathid=pathid+1;
             end;
             ID=vec(1);
             edge=[ID,vec(end)];
             pos=vec(3:5);
             pos=pos([2,1,3]);
             scale=vec(6);
             neuro_data(count).ID=ID;
             neuro_data(count).edge=edge;
             neuro_data(count).pos=pos;
             neuro_data(count).scale=scale;
             neuro_data(count).pathid=pathid;
             count=count+1;
        end;
    end;
end;
% 
% 
%     connection_listGT={};
%     for d=1:size(A.data,2)
%         connection_listGT{d}=[];
%     end;
%     for d=1:size(A.connections,2)
%         pointIDA=A.connections(1,d)+1;
%         pointIDB=A.connections(2,d)+1;
%         connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
%         connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
%     end;          
% 
% 
%     terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
%     terminalsTERMPos=A.data(1:3,terminalsTERM);
% 
%     D=distmat(terminalsTERMPos.',terminalsTERMPos.');
%     root_score=1-(mean(D.^0.5,1))./max(D(:).^0.5);
%     
%     if nargout>1
%         Avis=A;
%         Avis.data(8,:)=0.25;
%         Avis.data(8,terminalsTERM)=5*(1-0.9*root_score);
%         [v,indx]=min(root_score(:));
%         fprintf('%d\n',indx);
%         Avis.data(8,terminalsTERM(indx))=10;
%     end
%     
    
    
    