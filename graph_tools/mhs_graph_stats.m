%stats=[];for a=1:20,load(['~/home/data/nopro_paper/sim5/data',num2str(a),'.mat']);stats=[stats;mhs_graph_stats(data.GT)];end;
%figure;plot(stats(:,4:5))
%figure(1);plot(stats(:,4:5),'linewidth',2);axis([1 20 0 max(stats(:,5))]);legend({'bifurcations','independent segments'});
%save_figure([600,800],'sim_data_prop.pdf',gcf,'stretch',false,'fext','pdf','psize',4)
function stats=mhs_graph_stats(A)



connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          



terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));


terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
terminalsBIF=find(terminalsBIFa);


total_num_pts=size(A.data,2);
total_num_edges=size(A.connections,2);


Atmp=mhs_assign_path_ids(A);
if ~isempty(Atmp)
    A=Atmp;
end;


num_bif_particles=sum(A.data(17,:)==2);

check_has_bif_particle=(num_bif_particles>0);



num_path_ids=numel(unique(A.data(21,:)));

fprintf('particles    : %d\n',total_num_pts-numel(terminalsBIF)*(check_has_bif_particle));
fprintf('edges        : %d\n',total_num_edges);
fprintf('terminals    : %d\n',numel(terminalsTERM));


if (check_has_bif_particle)
    fprintf('bifurcations : %d (%d)\n',numel(terminalsBIF),num_bif_particles);
    
else
    fprintf('bifurcations : %d\n',numel(terminalsBIF));
end;


fprintf('numpaths     : %d\n',num_path_ids);
fprintf('#################\n');
fprintf('bif / path   : %f\n',(numel(terminalsBIF))/num_path_ids);
fprintf('segments     : %d\n',get_num_graph_segments(numel(terminalsTERM),numel(terminalsBIF)));


stats=[total_num_pts-numel(terminalsBIF)*(check_has_bif_particle),...
       total_num_edges,...
       numel(terminalsTERM),...
       numel(terminalsBIF),...
       num_path_ids,...
       (numel(terminalsBIF))/num_path_ids,...
       get_num_graph_segments(numel(terminalsTERM),numel(terminalsBIF))];



function s=get_num_graph_segments(nterms,nbifs)

	s=(nterms+nbifs*3)/2;
    
    
    
    