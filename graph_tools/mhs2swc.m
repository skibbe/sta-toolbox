function mhs2swc(A,filename)

    connection_listGT={};
    for d=1:size(A.data,2)
        connection_listGT{d}=[];
    end;
    for d=1:size(A.connections,2)
        pointIDA=A.connections(1,d)+1;
        pointIDB=A.connections(2,d)+1;
        connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
        connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
    end;          


    terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
   
    [Uid,IA]=unique(A.data(21,terminalsTERM));
    
    UterminalsTERM=terminalsTERM(IA);
    
    fid=fopen(filename,'w');
    
    pathid=1;
    for uid=UterminalsTERM
        current=uid;
        next=connection_listGT{current};
        assert(numel(next)==1);
        next=next(next~=current);
        if 5290==pathid
                1
            end; 
        fprintf(fid,'%d 2 %f %f %f %f -1\n',pathid,A.data(1:3,current),A.data(8,current)/2);
        pathid=pathid+1;
        
        while ~isempty(next)
            if 5290==pathid
                1
            end;            
            previous=current;
            current=next;
            next=connection_listGT{current};
            next=next(next~=previous);
            fprintf(fid,'%d 2 %f %f %f %f %d\n',pathid,A.data(1:3,current),A.data(8,current)/2,pathid-1);
            pathid=pathid+1;
        end;
        
%         if (uid==UterminalsTERM(2))
%             return;
%         end;
    end;
    
    fclose(fid);
    fprintf('done \n');
    
%     terminalsTERMPos=A.data(1:3,terminalsTERM);
% 
%     D=distmat(terminalsTERMPos.',terminalsTERMPos.');
%     root_score=1-(mean(D.^0.5,1))./max(D(:).^0.5);
%     
%     if nargout>1
%         Avis=A;
%         Avis.data(8,:)=0.25;
%         Avis.data(8,terminalsTERM)=5*(1-0.9*root_score);
%         [v,indx]=min(root_score(:));
%         fprintf('%d\n',indx);
%         Avis.data(8,terminalsTERM(indx))=10;
%     end
    