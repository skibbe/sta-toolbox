function B=mhs_eval_graph_apply(A,score,TP,FP)



    valid=(score.TP>TP) &  (score.FP<FP);
    valid_paths=score.BP(valid);
    
    B=A;
    B.data(21,:)=-1;
    for a=1:numel(valid_paths)
        indx=(A.data(21,:)==valid_paths(a));
        assert(numel(indx)>0)
        B.data(21,indx)=1;
    end;
     
    %B.data(21,1)=0;
    
    B=mhs_trimtree(B,'select_tree_ids',{2,0});
    %B=mhs_trimtree(A,'select_tree_ids',{4,0});
    return;
    
    
    B=A;
    B.data(15,:)=-1;
    for a=1:numel(valid_paths)
        indx=(A.data(21,:)==valid_paths(a));
        B.data(15,indx)=1;
    end;
    B=ntrack_graph_tools(B,{'tool',5});