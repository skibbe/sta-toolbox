function A=mhs_graph_new2old(A)





connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          
% 
% terminalsBIFa=cellfun(@(x)numel(x),connection_listGT)>2;
%terminalsBIF=find(terminalsBIFa);
%terminalsBIFPos=A.data(1:3,terminalsBIF);




B=A;

indeces=find(A.data(17,:)==2);

for in=1:numel(indeces)
    indx=indeces(in);
    conn_part=find(A.connections(1,:)+1==indx);
    conn_points=connection_listGT{indx};
    [m_v,m_ind]=max(min(abs([dot(A.data(4:6,conn_points),A.data(4:6,conn_points([2,3,1])));dot(A.data(4:6,conn_points),A.data(4:6,conn_points([3,1,2])))])));
    remove_edge=find(A.connections(2,conn_part)==conn_points(m_ind)-1);
    %bif_point=conn_points(remove_edge);
    %conn_points(remove_edge)=[];
    %edge_point=conn_points;
    
    %A.connections([1,3],1:2)
    %A.connections([1,3],conn_part)=repmat(A.connections([2,4],bif_point),1,2);
    %A.connections([2,4],conn_part)=A.connections([2,4],edge_point);
    B.connections([1,3],conn_part(1:2))=repmat(A.connections([2,4],conn_part([1,2,3]==remove_edge)),1,2);
    B.connections([2,4],conn_part(1:2))=A.connections([2,4],conn_part([1,2,3]~=remove_edge));
    
    %A.connections([1,3],conn_part)=A.connections([2,4],conn_part([2,3,1]));
end;
    
A=B;
                
for in=1:numel(indeces)
    indx=indeces(in);
    A.data(:,indx)=[];
    eindx=find((A.connections(1,:)==indx-1)|(A.connections(2,:)==indx-1));
    %eindx=find((A.connections(1,:)==indx-1)|(A.connections(2,:)==indx-1));
    A.connections(:,eindx)=[];
    for t=1:2
        updatei=(A.connections(t,:)>indx-1);
        A.connections(t,updatei)=A.connections(t,updatei)-1;
    end;
    indeces=indeces-1;
    fprintf('.');
end;
fprintf(':-)\n');



uids=unique(A.data(21,:));                
num_paths=numel(uids);

new_ids=[1:num_paths];
lids=A.data(21,:);
for a=1:num_paths
    A.data(21,lids==uids(a))=new_ids(a);
end;


