%
%
% java -jar ./ext_data/bin/DiademMetric.jar  -G ./ext_data/olfactory/Olfactory\ Projection\ Fibers/Gold\ Standard\ Reconstructions/OP_1.swc -T ./ext_data/O1.swc -D 5
%
%
function Anew=path2SWC(A,filename,offset,scale)

% numpts=size(A.data,2);
% 
% 
% FileID(1:numpts)=-2;
% swc={};
% 
% 
% for d=1:size(A.connections,2)
%     pointIDA=A.connections(1,d)+1;
%     pointIDB=A.connections(2,d)+1;
%     
%     Ax=A.data(1,pointIDA)+1;
%     Ay=A.data(2,pointIDA)+1;
%     Az=A.data(3,pointIDA)+1;
% 
%     Bx=A.data(1,pointIDB)+1;
%     By=A.data(2,pointIDB)+1;
%     Bz=A.data(3,pointIDB)+1;
%     
%     Ss=A.data(8,d);
%     
%     if (FileID(pointIDA)~=-2)&&(FileID(pointIDB)~=-2)
%         fprintf('?');
%     end;
%     
%     if FileID(pointIDA)==-2
%         FileID(pointIDA)=numel(swc)+1;
%         swc=[swc,{[d,]}]
%     end;    
% end;        
% 
% 
% fprintf('\n');


%fprintf('adasd')



if true
            Anew=A;
            %[paths,Anew]=extract_paths(A,'spacing',2);
            
            
            pathdid=Anew.data(21,:);
            upathdids=unique(pathdid);
            newids=[1:numel(upathdids)];
            fprintf(' numpaths %d\n',numel(upathdids));

            npathdid=pathdid;
            for a=1:numel(upathdids)
                npathdid(pathdid==upathdids(a))=newids(a);
            end;

            Anew.data(21,:)=npathdid;
            
            connection_listGT={};
            for d=1:size(Anew.data,2)
                connection_listGT{d}=[];
            end;
            for d=1:size(Anew.connections,2)
                pointIDA=Anew.connections(1,d)+1;
                pointIDB=Anew.connections(2,d)+1;
                connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
                connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
            end;              


            clear visited;
            visited(1:numel(connection_listGT))=0;

            set(0,'RecursionLimit',1500);
            %set(0,'RecursionLimit',200);
            
            Plists={};
            plcount=1;
            timestamp=1;
            pcount=0;
            for d=1:numel(connection_listGT)
                if mod(d,50)==0 
                    fprintf('.');
                end;
                    
                if (numel(connection_listGT{d})==1) && (visited(d)==0)
                    %for b=1:numel(connection_listGT{a})
                        
                        pcount=pcount+1;
                        pointlist={[pcount,d,-1]};
                        [Anew,visited,pointlist2,pcount]=exclude_path(connection_listGT,Anew,visited,timestamp,d,connection_listGT{d}(1),pcount);
                        pointlist=[pointlist,pointlist2];
                       
                        
                        timestamp=timestamp+1;
                        
                        Plists{plcount}=pointlist;
                        plcount=plcount+1;
                    %end;
                end;
            end;               
            fprintf('\n');
            
            fid = fopen(filename,'w');
            
            [v,indx]=max(cellfun(@(x)numel(x),Plists));
            
                %for a=1:numel(Plists)
                a=indx;
                    fprintf('path %d / %d\n',a,numel(Plists));
                    offsetcorrection=min([cellfun(@(x)(x(1)),Plists{a})]);
                    for b=1:numel(Plists{a})
                        %fprintf('%d %d %d\n',Plists{a}{b}(1),Plists{a}{b}(2),Plists{a}{b}(3));
                        %P=floor(Anew.data(:,Plists{a}{b}(2))+0.5);
                        P=(Anew.data(:,Plists{a}{b}(2)));
                        if nargin>2
                            P(1:3)=P(1:3)+offset';
                        end;
                        if nargin>3
                            P(1:3)=P(1:3)*scale;
                            P(8)=P(8)*scale;
                        end;                        
                        %display(Anew.data(1:3,Plists{a}{b}(2)));
                        myid=Plists{a}{b}(1)-offsetcorrection+1;
                        myto=Plists{a}{b}(3);
                        if myto>0
                            myto=myto-offsetcorrection+1;
                        end;
                        fprintf(fid,'%d 10 %.5f %.5f %.5f %.5f %d\n',myid,P(2),P(1),P(3),P(8),myto);
                    end;
                %end;
                
            fclose(fid);

%             timestamp=1;
%             for d=1:size(Anew.connections,2)
%                     pointIDA=Anew.connections(1,d)+1;
%                     pointIDB=Anew.connections(2,d)+1;
% 
%                     IDA=(Anew.data(21,pointIDA));
%                     IDB=(Anew.data(21,pointIDB));
% 
%                     n(1)=Cdet(3,pointIDA)+1;
%                     n(2)=Cdet(3,pointIDB)+1;
%                     ok=false;
% 
%                     for a=1:2
%                         if n(a)>0
%                             cl=connection_listGT{n(a)};
%                             if numel(cl)==1
%                                 ok=true;
%                             end;
%                         end;
%                     end;
% 
%                     if ok
%                         cont={};
%                         if (IDA==2)&&(IDB==3)
%                             [Anew,visited,cont]=exclude_path(connection_list,Anew,visited,timestamp,pointIDB,pointIDA,0);
%                         elseif (IDA==3)&&(IDB==2)
%                                 [Anew,visited,cont]=exclude_path(connection_list,Anew,visited,timestamp,pointIDA,pointIDB,0);
%                         end;
%                         
%                         while ~isempty(cont)
%                             cont2={};
%                             for r=1:numel(cont)
%                                 [Anew,visited,cont3]=exclude_path(connection_list,Anew,visited,timestamp,cont{r}(1),cont{r}(2),0);
%                                 cont2=[cont3,cont2];
%                             end;
%                             cont=cont2;
%                         end;
%                         timestamp=timestamp+1;
%                     end;
%             end;   

end;
            
            
            


function [A,visited,pointlist,pcount]=exclude_path(connection_list,A,visited,timestamp,indx0,indx1,pcount)

%cont_new={};
pointlist={};
connections=connection_list{indx1};
connections(connections==indx0)=[];
visited(indx0)=timestamp;
if (visited(indx1)<timestamp)
    visited(indx1)=timestamp;
    %if ((A.data(21,indx1))==2)
        %A.data(21,indx1)=4;

            cpoint=pcount;
            for b=1:numel(connections)
                pcount=pcount+1;                
                pointlist=[pointlist,{[pcount,connections(b),cpoint]}];
                [A,visited,pointlist2,pcount]=exclude_path(connection_list,A,visited,timestamp,indx1,connections(b),pcount);
                pointlist=[pointlist,pointlist2];
            end;
        
    %end;
end;
visited(indx1)=timestamp;





            
