function C=mhs_graph_append(C,B,varargin)

mark_graphs=false;
for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

if isempty(C)
    C=B;
    return;
end;

%if size(C.data,1)<26
%C.data=(cat(1,C.data,zeros([26-size(C.data,1),size(C.data,2)])));    
%end;

pid=max(C.data(21,:));

if isempty(pid)
    C=B;
    return;
end;

if mark_graphs
    C.data(21,:)=2;
    B.data(21,:)=1;    
    %B.data(1:3,:)=B.data(1:3,:)+4;
end;

pad_size=max(size(C.data,1),size(B.data,1));

if size(C.data,1)<pad_size
    pad=pad_size-size(C.data,1);
    C.data=cat(1,C.data,zeros([pad,size(C.data,2)],class(C.data)));
end;
if size(B.data,1)<pad_size
    pad=pad_size-size(B.data,1);
    B.data=cat(1,B.data,zeros([pad,size(B.data,2)],class(B.data)));
end;


offset=size(C.data,2);

tmp=B.data;
%tmp(21,:)=pid+1;
tmp(21,:)=tmp(21,:)+pid+1;
C.data=cat(2,C.data,tmp);
tmp=B.connections;
tmp(1:2,:)=tmp(1:2,:)+offset;
C.connections=cat(2,C.connections,tmp);

if isfield(C,'cshape')
    C.cshape=max(ceil(single(max(C.data(1:3,:),[],2)).'),C.cshape);
else
    C.cshape=ceil(single(max(C.data(1:3,:),[],2)).'+1);
end;