% function score=mhs_eval_graph(GT,B)
function score=mhs_eval_graph(GT,B,shape,varargin)





warning(' will use the new version of mhs_eval_graph');
score=mhs_eval_graph_new(GT,B,shape,varargin{:});
return;


spacing=2;


if (nargin>2) 
    if numel(shape)==1
    


    A=GT;
    A=extract_paths(A,'spacing',spacing);
    B=extract_paths(B,'spacing',spacing);

    A.data=single(A.data);
    B.data=single(B.data);

    posTA=A.data(1:3,:).';
    posTB=B.data(1:3,:).';
     
    nptsA=size(A.data,2);
    nptsB=size(B.data,2);

    scaleA=max(A.data(8,:),eps);
    scaleB=max(B.data(8,:),eps);

    scaleMatA=repmat(scaleA,nptsB,1);
    scaleMatB=repmat(scaleB,nptsA,1).';

    %more than touching -> coll
    %colldist=((scaleMatA+scaleMatB)).^2;
    
    %centers are penetrating the shells
    %colldist=(min(scaleMatA,scaleMatB)).^2;
    
    %centers are penetrating the 2* shells
    colldist=(2*min(scaleMatA,scaleMatB)).^2;

    clear scaleMatA scaleMatB;


    A=upid(A);
    B=upid(B);
    pathid_A=A.data(21,:);
    pathid_B=B.data(21,:);

    pidA=[1:nptsA];
    pidB=[1:nptsB];

    path_id_MatA=repmat(pathid_A,nptsB,1);
    path_id_MatB=repmat(pathid_B,nptsA,1).';
    
    
   
    
    upidA=unique(pathid_A);
    upidB=unique(pathid_B);
    
    assert(~any(upidA==0))
    assert(~any(upidB==0))

    D=(distmat(posTA,posTB));
    
    clear postTB postTA;
    
    coll=sparse(colldist>D);

    p_id_MatB=repmat(pidB,nptsA,1).';
     
    best_coll_AwithB=(~(D>(repmat(min(D,[],1),nptsB,1)))) & coll;
    best_coll_AwithB_path_id=max(full(best_coll_AwithB).*path_id_MatB,[],1);
    best_coll_AwithB_particle_id=max(full(best_coll_AwithB).*p_id_MatB,[],1);
    
    clear path_id_MatB best_coll_AwithB;
    
    p_id_MatA=repmat(pidA,nptsB,1);
    
    best_coll_BwithA=(~(D>(repmat(min(D,[],2),1,nptsA)))) & coll;
    best_coll_BwithA_path_id=max(full(best_coll_BwithA).*path_id_MatA,[],2);
    best_coll_BwithA_particle_id=max(full(best_coll_BwithA).*p_id_MatA,[],2);
    
    clear path_id_MatA best_coll_BwithA;
    
    clear D colldist coll;
    
    
  
    
    A.data(21,:)=1;
    B.data(21,:)=2;
    D=[];
    
    count=1;
    for idA=upidA
        fprintf('%d %d ',count,numel(upidA));
        select_A=(pathid_A==idA);
        coll_apthsAB_all=best_coll_AwithB_path_id(select_A);
        coll_pathsAB=unique(coll_apthsAB_all);
        coll_pathsAB=coll_pathsAB(coll_pathsAB>0);
        
        if numel(coll_pathsAB)>0
        
            cmem=zeros(1,numel(coll_pathsAB));
            for cp=1:numel(coll_pathsAB)
                cmem(cp)=sum(coll_apthsAB_all(:)==coll_pathsAB(cp));
            end;

            [members,indx]=max(cmem);

            best_path_in_B=coll_pathsAB(indx);
            
            
            %Bindx=best_coll_AwithB_particle_id(best_coll_AwithB_path_id==best_path_in_B);% & select_A;
            Bindx=best_coll_AwithB_particle_id((best_coll_AwithB_path_id==best_path_in_B) & select_A);
            

            total_collisions_A=sum(coll_apthsAB_all>0);
            total_elements_A=sum(pathid_A==idA);

            %collisions_AB_best_non_unique=sum(coll_apthsAB_all==best_path_in_B);
            collisions_AB_best=numel(unique(Bindx));
            collisions_AB_best_non_unique=numel(Bindx);
            
            total_elements_B=sum(pathid_B==best_path_in_B);
            
            
            select_B=(pathid_B==best_path_in_B);
            Aindx=best_coll_BwithA_particle_id((best_coll_BwithA_path_id==idA).' & (select_B));% & select_A;
            collisions_BA_best=numel(unique(Aindx));
            collisions_BA_best_non_unique=numel(Aindx);
            
            
            A.data(21,Aindx)=4;
            B.data(21,Bindx)=3;
            
            fprintf('[%d %d %d %d]\n',collisions_AB_best_non_unique,collisions_AB_best,collisions_BA_best_non_unique,collisions_BA_best);

            %TP(count)=collisions_AB_best./(total_elements_A+eps);
            %TP(count)=collisions_BA_best./(total_elements_A+eps);
            TP(count)=collisions_AB_best_non_unique./(total_elements_A+eps);

            FP(count)=(total_elements_B-collisions_BA_best_non_unique)./(total_elements_B+eps);
            %FP(count)=(total_elements_B-collisions_AB_best)./total_elements_B;
        else
            TP(count)=0;
            FP(count)=0;
        end;
        
        
        count=count+1;

    end;
    
    
score.TP=TP;
score.FP=FP;
B.data(1:3,:)=B.data(1:3,:)+1;
%B.data(8,B.data(21,:)~=3)=0.1;
%A.data(8,A.data(21,:)~=4)=0.1;

score.Cvis=mhs_graph_append(A,B);
score.Cvis.data=double(score.Cvis.data);
    


    return
    end;
end;



A=GT;
A=extract_paths(A,'spacing',1.5);
B=extract_paths(B,'spacing',1.5);
%A=extract_paths(A,'spacing',2);
%B=extract_paths(B,'spacing',2);

A.data=single(A.data);
B.data=single(B.data);

voxel_based=false;

if nargin>2
    voxel_based=true;
end;

posA=A.data(1:3,:);
posB=B.data(1:3,:);
 posTA=posA.';
 posTB=posB.';

nptsA=size(A.data,2);
nptsB=size(B.data,2);

%posA=(cat(1,posA,ones(1,nptsA)));
%posB=(cat(1,posB,ones(1,nptsB)));

scaleA=max(A.data(8,:),eps);
scaleB=max(B.data(8,:),eps);

scaleMatA=repmat(scaleA,nptsB,1);
scaleMatB=repmat(scaleB,nptsA,1).';

colldist=((scaleMatA+scaleMatB)).^2;

clear scaleMatA scaleMatB;


A=upid(A);
B=upid(B);
pathid_A=A.data(21,:);
pathid_B=B.data(21,:);




path_id_MatA=repmat(pathid_A,nptsB,1);
path_id_MatB=repmat(pathid_B,nptsA,1).';


upidA=unique(pathid_A);
upidB=unique(pathid_B);


D=(distmat(posTA,posTB));
coll=sparse(colldist>D);

clear D colldist;




coll_A=(max(coll,[],1));
coll_B=(max(coll,[],2));

path_memA=pathid_A;
path_memB=pathid_B;
for p=1:numel(upidA)
    path_memA(p)=sum(upidA(p)==A.data(21,:));
end;
for p=1:numel(upidB)
    path_memB(p)=sum(upidB(p)==B.data(21,:));
end;


count=1;
for idA=upidA
    fprintf('%d %d ',count,numel(upidA));
    coll_idA=sparse((path_id_MatA==idA) & coll);
    
    MB=path_id_MatB(coll_idA);
    coll_paths=unique(MB);
    
    
    cmem=zeros(1,numel(coll_paths));
    cmemB=zeros(1,numel(coll_paths));
    
    max_coll_idA=max(coll_idA,[],2);
    
    
    if numel(coll_paths)>0
        fprintf('(%d) ',numel(coll_paths));
    end;
    for p=1:numel(coll_paths)
        %identify collisions in A
        cmem(p)=sum(max(((path_id_MatB==coll_paths(p))& coll_idA),[],1));
        
        
        cmemB(p)=sum(max_coll_idA & (pathid_B==coll_paths(p)).');
        assert(cmem(p)<=path_memA(count));
        
        assert(cmemB(p)<=path_memB(coll_paths(p)));
        fprintf('.');
    end;
    
    
    [coll_path_max_mem,indx]=max(cmem);
    
    if numel(coll_path_max_mem)>0
        if voxel_based
        ImgA=mhs_data2img(A,'shape',shape,'mode',0,'lengthweight',false,'select_tree_ids',{idA,0});
        ImgB=mhs_data2img(B,'shape',shape,'mode',0,'lengthweight',false,'select_tree_ids',{coll_paths(indx),0});
        ImgA=ImgA>0.5;
        ImgB=ImgB>0.5;
        TPv(count)=sum(ImgA(:).*ImgB(:))./sum(ImgA(:));
        FPv(count)=(sum(ImgB(:))-sum(ImgA(:).*ImgB(:)))./sum(ImgB(:));
        end;
        
        TP(count)=coll_path_max_mem./(path_memA(count)+eps);
        
        FP(count)=(path_memB(coll_paths(indx))-cmemB(indx))./path_memB(coll_paths(indx));
        
        1;
    else
        TP(count)=0;    
        FP(count)=0;    
    end;
    
    fprintf('\n');
    count=count+1;
end;



if voxel_based
    ImgA=mhs_data2img(A,'shape',shape,'mode',0,'lengthweight',false);
    ImgB=mhs_data2img(B,'shape',shape,'mode',0,'lengthweight',false);
    
    total_TP=sum(ImgA(:).*ImgB(:));
    total_FP=sum(ImgB(:))-total_TP;
    total_FN=sum(ImgA(:))-total_TP;


    score.total_TP=total_TP;
    score.total_FP=total_FP;
    score.total_FN=total_FN;
    
    score.TPv=TPv;
    score.FPv=FPv;

end;

score.TP=TP;
score.FP=FP;


fprintf('---\n');




function B=upid(A)

pathid_A=A.data(21,:);
upathid_A=unique(pathid_A);
B=A;
count=1;
for a=upathid_A
    B.data(21,pathid_A==a)=count;
    count=count+1;
end;




