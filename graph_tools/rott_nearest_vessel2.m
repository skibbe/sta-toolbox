function [A,B,Bbest,C]=rott_nearest_vessel2(A,pointA,pointB,varargin)

% set(0,'RecursionLimit',5000);
offset=[0,0,0];

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

% numpts=size(A.data,2);
% %for t=1:numpts
%     connect_side_data(1:numpts) = struct('cc',[0,0],'l',[],'r',[]);
% %end;
% 
% for a=1:size(A.connections,2)
%     idA=A.connections(1,a)+1;
%     idB=A.connections(2,a)+1;    
%     sideA=A.connections(3,a)+1;
%     sideB=A.connections(4,a)+1;
%     connect_side_data(idA).cc(sideA)=connect_side_data(idA).cc(sideA)+1;
%     connect_side_data(idB).cc(sideB)=connect_side_data(idB).cc(sideB)+1;  
%     
%     connect_side_data(idA).l=unique([connect_side_data(idA).l,idB]);
%     connect_side_data(idB).r=unique([connect_side_data(idB).r,idA]);
% end;
% 
% new_edges=[];
% for t=1:size(A.data,2)
%     for a=1:2
%         if connect_side_data(t).cc(a)>1
%             %fprintf('b');
%             if a==1
%                 side=0;
%                 tmp=connect_side_data(t).l;
%             else
%                 tmp=connect_side_data(t).r;
%                 side=1;
%             end;
%             for b=1:numel(tmp), 
%                 for c=b+1:numel(tmp), 
%                     new_edges=[new_edges;tmp([b,c])-1,side,0]; 
%                 end;
%             end;
%             
%         end;
%     end;
% end;
% %new_edges
% B=A;
% B.connections=[B.connections,new_edges'];

pos=A.data(1:3,:);
pos(:,A.data(22,:)~=1)=inf;    

[bla,vindx]=sort(distmat(pointA-offset,pos'),1);
select_tree=unique(A.data(21,vindx(1)));
fprintf('extracting from tree %d\n',select_tree);

% [bla2,vindx2]=sort(distmat(pointB-offset,pos'),1);
% 
% vindx2=vindx2(bla2<10^2);
% select_tree2=unique(A.data(21,vindx2));
% fprintf('extracting from tree %d\n',select_tree2);
% 
% assert(sum(select_tree2==select_tree)>0);



numpts=size(A.data,2);


connect_data(1:numpts) = struct('c',[],'v',false);
%connect_data(numpts).c=[];
%connect_data(numpts).v=false;


for a=1:size(A.connections,2)
    idA=A.connections(1,a)+1;
    idB=A.connections(2,a)+1;
    connect_data(idA).c=unique([connect_data(idA).c,idB]);
    connect_data(idB).c=unique([connect_data(idB).c,idA]);
end;






terminals=[];
for a=1:size(A.data,2)
    connect_data(a).numc=numel(connect_data(a).c);
    if ((connect_data(a).numc==1)&&(A.data(21,a)==select_tree))
        terminals=[terminals,a];
    end;
end;

idx=struct('data',[]);
count=1;
tindex=[];
for t=1:numel(terminals)
%    fprintf('\n');
    idx2=track(connect_data,-1,terminals(t),A);
    for c=1:numel(idx2)
        idx(count)=idx2(c);
        tindex=[tindex;[idx2(c).data([1,end])]];
        count=count+1;
    end;
end;
%tindex
valid_paths(1:size(tindex,1))=true;
dmat=(distmat(sort(tindex,2),sort(tindex,2)));
for b=1:size(tindex,1)
    d=dmat(b,:);
    d(b)=1;
    c=find(d==0);
    if (valid_paths(b))
        valid_paths(c)=false;
    end;
end;

idx=idx(valid_paths);
for a=1:numel(idx)
    fprintf('%d %d\n',idx(a).data([1,end]));
end;


worst_rank=ones(1,numel(idx));
worst_sig=zeros(1,numel(idx));
worst_distA=zeros(1,numel(idx));
worst_distB=zeros(1,numel(idx));
for a=1:numel(idx)
    vindx=zeros(1,size(A.data,2));
    vindx(idx(a).data)=1;
    nidx=numel(idx(a).data);
    
    pos=A.data(1:3,idx(a).data);
    worst_distA(a)=min(distmat(pointA-offset,pos'));
    worst_distB(a)=min(distmat(pointB-offset,pos'));
    
    
    worst=1;
    for b=1:nidx
        pid=idx(a).data(b);
        nn=A.data(4:6,pid);
        
        if connect_data(pid).numc<3
         for c=1:connect_data(pid).numc
            if vindx(connect_data(pid).c(c))
                pid2=connect_data(pid).c(c);
                if connect_data(pid2).numc<3
                    worst=min(worst,abs(dot(A.data(4:6,connect_data(pid).c(c)),nn)));   
                    worst_sig(a)=worst_sig(a)+1;
                end;
            end;
         end;   
        end;
        %for c=1:connect_data(pid).numc
        %    if vindx(connect_data(pid).c(c))
        %     worst=min(worst,abs(dot(A.data(4:6,connect_data(pid).c(c)),nn)));   
        %    end;
        %end;
    end;
    worst_rank(a)=worst;
end;
worst_sig=worst_sig/max(worst_sig);


ranks=[sqrt(worst_distA);sqrt(worst_distB);worst_rank;worst_sig;];
%display([worst_rank;worst_sig;sqrt(worst_distA);sqrt(worst_distB)]);
%display(ranks);
ranks2=ranks;
for a=1:4
    vA=min(ranks2(a,:));
    ranks2(:,ranks2(a,:)>vA)=inf;
end;
%display(ranks2);
[val,Bbest]=min(ranks2(1,:));

C=A;
C.data=[];
C.connections=[];
for a=1:numel(idx)
    valid_particles(1:numpts)=false;
    valid_particles(idx(a).data)=true;
    B{a}=trim_tree(A,valid_particles);    
    data=B{a}.data;
    data(21,:)=a;
    %data(1:3,:)=data(1:3,:)+a;
    connections=B{a}.connections;
    connections(1:2,:)=connections(1:2,:)+size(C.data,2);
    C.connections=[C.connections,connections];
    C.data=[C.data,data];    
end;


valid_particles=max((repmat(A.data(21,:),numel(select_tree),1)==repmat(select_tree',1,size(A.data,2))),[],1);

A=trim_tree(A,valid_particles);

%valid_particles(1:numpts)=false;
%idx=track(connect_data,valid_indx,A,1);
%valid_particles(idx)=true;

% 
% function V=scoreme_(A,connection_listGT,score,previous,current)
% 
% if numel(score)>2
%     
% end;
% 
% 
% 
% if numel(score)>3
%     score=score(2:end);    
% end;
% 
% 
% function V=scoreme(A)
% 
%     connection_listGT={};
%     for d=1:size(A.data,2)
%         connection_listGT{d}=[];
%     end;
%     for d=1:size(A.connections,2)
%         pointIDA=A.connections(1,d)+1;
%         pointIDB=A.connections(2,d)+1;
%         connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
%         connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
%     end;          
% 
%     terminalsTERM=find(1==cellfun(@(x)numel(x),connection_listGT));
%     %terminalsTERMPos=A.data(1:3,terminalsTERM);    
%     




function A=trim_tree(A,valid_particles)



old_ids=1:size(A.data,2);
new_ids=old_ids;
new_ids(:)=-1;
new_ids(valid_particles)=1:sum(valid_particles);


valid_edges1=valid_particles(A.connections(1,:)+1)&valid_particles(A.connections(2,:)+1);
valid_edges2=valid_particles(A.connections(1,:)+1)|valid_particles(A.connections(2,:)+1);

valid_edges2=valid_edges1;
assert(size(A.connections,2)==numel(valid_edges1));
A.connections(1,valid_edges1)=new_ids(A.connections(1,valid_edges1)+1)-1;
A.connections(2,valid_edges2)=new_ids(A.connections(2,valid_edges1)+1)-1;

A.data(:,~valid_particles)=[];
A.connections(:,~valid_edges1)=[];



function [idx]=track(connect_data,previous,current,A)
    %fprintf(' %d',connect_data(current).numc);
    connect_data(current).v=true;
    %fprintf('.');
    idx.data=current;
    idx_=[];
    
    connect_data_=connect_data;
    count=1;
    for c=1:connect_data(current).numc   
        %
        next=connect_data(current).c(c);
       % fprintf('(%d,%d,%d)',previous,current,next);
        if (next~=previous)&&(~connect_data(next).v)
            idx2=track(connect_data,current,next,A);
            if numel(idx2)>0
                for c2=1:numel(idx2)
                    %idx(count).data=[idx(count).data,idx2(c2).data];
                    idx_(count).data=[current,idx2(c2).data];
                    count=count+1;
                end;
            end;
        end;
%         idx2
        %fprintf('%d ',numel(idx2));
%         if numel(idx2)>0
%             for c2=1:numel(idx2)
%                 %idx(count).data=[idx(count).data,idx2(c2).data];
%                 idx(count).data=[current,idx2(c2).data];
%                 count=count+1;
%             end;
%         end;
    end;
    
    if numel(idx_)>0
        idx=idx_;
    end;
    
    %idx(count).data=current;
    
    