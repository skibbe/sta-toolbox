function B=mhs_cluster_paths(A,numclusters)

    [B,pathdata]=ntrack_graph_tools(A,{'tool',2});
    [c_id,c_centers]=kmeans(pathdata(2,:).',numclusters);
    
    A=B;
    fprintf('reassigning paths ');
    for pid=1:numel(pathdata(1,:)), 
        if mod(pid,5000)==0
            fprintf('.');
        end;
        B.data(21,A.data(21,:)==pathdata(1,pid))=c_id(pid);
    end;
    fprintf('\n');