function A=mhs_assign_path_ids(A)


set(0,'RecursionLimit',500)
set(0,'RecursionLimit',2000)
connection_listGT={};
for d=1:size(A.data,2)
    connection_listGT{d}=[];
end;
for d=1:size(A.connections,2)
    pointIDA=A.connections(1,d)+1;
    pointIDB=A.connections(2,d)+1;
    connection_listGT{pointIDA}=[connection_listGT{pointIDA},pointIDB];
    connection_listGT{pointIDB}=[connection_listGT{pointIDB},pointIDA];
end;          


A.data(21,:)=-2;

num_pts=size(A.data,2);

  pathids=zeros(1,num_pts);
    pathids(:)=-2;
    pathids_valid=zeros(1,num_pts);
    pathids_valid(:)=true;

    current_id=1;
    for p=1:num_pts
        if (pathids(p)==-2)
            [pathids,current_id]=assign_id(connection_listGT,pathids,p,current_id,0);
            if (current_id==-1)
                A=[];
                warning('assigning path ids failed (RecursionLimit)');
                return;
            end;
            %current_id=current_id+1;
        end;
        A.data(22,p)=numel(connection_listGT{p})>0;
    end;
    
    A.data(21,:)=pathids;
    
    

        

% 
%     B.data(22,idA)=1;
%     B.data(22,idB)=1;

% while ok
%     select=find(A.data(21,:)==-2);
%     ok=numel(select)>0;
%     
%     for a=select
%         pts=connection_listGT{a};
%         if numel(pts)==0
%             A.data(21,a)=-1;
%         else
%             
%             pids=unique(A.data(21,pts));
% 
%             assert(sum(pids==-1)==0);
%             if max(pids)<-1
%                 A.data(21,pts)=pathd_id;
%                 pathd_id=pathd_id+1;
%             else
%                 assert(numel(pids)<3);    
%                 A.data(21,pts)=max(pids);
%             end;
%         end;
%         
%     end;
% end;



    function [pathids,current_id]=assign_id(connections,pathids,p,current_id,depths)
        if numel(connections{p})==0
            pathids(p)=-1;
              return;  
        end;
        if (pathids(p)>-2)
            return;
        end
        if depths==0
            current_id=current_id+1;
        end;
        %if (depths>495)
        if (depths>995)
            current_id=-1;
            return;
        end;
        pathids(p)=current_id;
        for a=connections{p}
            [pathids,current_id]=assign_id(connections,pathids,a,current_id,depths+1);
            if (current_id==-1)
                return;      
            end;
        end;
%         
%         if connectionsA(p)>0
%         if pathids(connectionsA(p))<0
%             pnew=connectionsA(p);
%             %pathids(connectionsA(p))=current_id;
%             [pathids,l,id_current]=assign_id(connectionsA,connectionsB,current_id,pathids,pnew,l,id_current);
%         end;
%         end;
%         if connectionsB(p)>0
%         if pathids(connectionsB(p))<0
%             pnew=connectionsB(p);
%             %pathids(connectionsA(p))=current_id;
%             [pathids,l,id_current]=assign_id(connectionsA,connectionsB,current_id,pathids,pnew,l,id_current);
%         end;
%         end;






