#ifndef CUDA_KERNELS_H
#define CUDA_KERNELS_H

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include "./ext/helper_math.h"

#define _CUDA_PINNED_MEM_

 class GPU_CPU_common_buffer
    {
    public: 
      float * gpu;
      float * cpu;
      int buffer_size;
      
      GPU_CPU_common_buffer()
      {
	gpu=NULL;
	cpu=NULL;
	buffer_size=0;
      }
      
      GPU_CPU_common_buffer(int buffer_size)
      {
	resize(buffer_size);
      }
      
#ifdef _CUDA_PINNED_MEM_
      void clean_buffer()
      {
	if (gpu!=NULL)
	{
	printf("cleaning up gpu/cpu buffer\n");
	cudaFreeHost(cpu);
	gpu=NULL;
	}
	buffer_size=0;
      }
      
      void resize(int buffer_size)
      {
	clean_buffer();
	printf("allocating gpu/cpu buffer (%d)\n",buffer_size);
	cudaHostAlloc ( (void **)&cpu, buffer_size*sizeof(float),cudaHostAllocMapped) ;
	cudaHostGetDevicePointer((void **)&gpu, (void *)cpu, 0);
	this->buffer_size=buffer_size;
      }
      
      void gpu2cpu()
      {
	cpu2gpu();
      }
      
      void cpu2gpu()
      {
	cudaError_t cuda_error_code=cudaDeviceSynchronize();      
	if (cuda_error_code!=cudaSuccess)
	  {
	    mhs::STAError error;
	    error<<"cuda error: "<<cudaGetErrorString(cuda_error_code)<<"\n";
	    throw error;
	  }
      }
#else  

      void gpu2cpu()
      {
	cuda_error_code=cudaMemcpy ( buffer_GPU[thread_id].cpu, buffer_GPU[thread_id].gpu, num_total_blocks*sizeof(float) , cudaMemcpyDeviceToHost ) ;
	if (cuda_error_code!=cudaSuccess)
	  {
	    mhs::STAError error;
	    error<<"cuda error: "<<cudaGetErrorString(cuda_error_code)<<"\n";
	    throw error;
	  }
      }
      
      void cpu2gpu()
      {
	cuda_error_code=cudaMemcpy ( buffer_GPU[thread_id].gpu,buffer_GPU[thread_id].cpu, num_total_blocks*sizeof(float) , cudaMemcpyHostToDevice ) ;
	if (cuda_error_code!=cudaSuccess)
	  {
	    mhs::STAError error;
	    error<<"cuda error: "<<cudaGetErrorString(cuda_error_code)<<"\n";
	    throw error;
	  }
      }

      void clean_buffer()
      {
	if (gpu!=NULL)
	{
	printf("cleaning up gpu/cpu buffer\n");
	cudaFree (gpu) ;
	delete [] cpu;
	}
	buffer_size=0;
      }
      
      void resize(int buffer_size)
      {
	clean_buffer();
	printf("allocating gpu/cpu buffer (%d)\n",buffer_size);
	cudaMalloc ( (void **)&gpu, buffer_size*sizeof(float)) ;
	cpu=new float[buffer_size];
	this->buffer_size=buffer_size;
      }
#endif
      ~GPU_CPU_common_buffer()
      {
	clean_buffer();
      }
    };
    
    
    
    
__global__ void kernel_interpolate( 
    const float3  position,
    const float3  v0,
    const float3  v1,
    const float3  v2,
    const float3  offset,
    const float3  box2kernel,
    const float3  scales2,
    const float2  scales4,
    const int     num_total_voxels,
    const int3    bb,
    cudaTextureObject_t tex,
    float * gpu_buffer,
    float * result)
{
  
    extern __shared__ float chache[];
    const unsigned int chacheindex = threadIdx.x ;
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float valid=0;
    float intensity=0;
    float mask=0;
    float kernel=0;

    if (indx<num_total_voxels)
    {
      int z=(indx/(bb.x*bb.y))-offset.z;
      indx%=(bb.y*bb.x);
      int y=(indx/(bb.x))-offset.y;
      int x=(indx%(bb.x))-offset.x;
     
      float dist=(z/offset.z);
      dist*=dist;
      if (!(dist>1+__FLT_EPSILON__))
      {
	float tmp=(y/offset.y);
	dist+=tmp*tmp;
	if (!(dist>1+__FLT_EPSILON__))
	{
	  tmp=(x/offset.x);
	  dist+=tmp*tmp;
	  if (!(dist>1+__FLT_EPSILON__))
	  {

		float kx=x*box2kernel.x;
		float ky=y*box2kernel.y;
		float kz=z*box2kernel.z;
		
		  
		const float & s1_2=scales2.z;
		const float & s2_2=scales2.y;
		const float & s3_2=scales2.x;
		const float & s2_4=scales4.y;
		const float & s3_4=scales4.x;
		
 		float3 img_pos;
 		img_pos.x=position.x+v0.x*kz+v1.x*kx+v2.x*ky+0.5f;
 		img_pos.y=position.y+v0.y*kz+v1.y*kx+v2.y*ky+0.5f;
 		img_pos.z=position.z+v0.z*kz+v1.z*kx+v2.z*ky+0.5f;
		
 		intensity = tex3D<float>(tex, img_pos.x, img_pos.y, img_pos.z);
		  
		
		float  pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
		float  pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/2.0f;

		mask=expf(-pol_t);
		kernel=mask*pol;
		valid=1;
	  }
	}
      }
      int indx2=((z+offset.z)*bb.y+(y+offset.y))*bb.x+(x+offset.x);
      gpu_buffer[indx2*4]=valid;
      gpu_buffer[indx2*4+1]=intensity;
      gpu_buffer[indx2*4+2]=mask;
      gpu_buffer[indx2*4+3]=kernel;
    }
    
   chache[chacheindex] = valid;
   chache[chacheindex+blockDim.x] = intensity;
   chache[chacheindex+2*blockDim.x] = kernel;

    __syncthreads () ;
    
    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
      {
	    chache[chacheindex] += chache [chacheindex + i] ;
	    chache[chacheindex+blockDim.x] += chache [chacheindex+blockDim.x + i] ;
	    chache[chacheindex+2*blockDim.x] += chache [chacheindex+2*blockDim.x + i] ;
      }
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
      {
	    result[blockIdx.x] = chache [0] ;
	    result[blockIdx.x+gridDim.x] = chache [blockDim.x] ;
	    result[blockIdx.x+2*gridDim.x] = chache [2*blockDim.x] ;
      }
}


__global__ void kernel_var( 
    const int     num_total_voxels,
    const float mean_intensities,
    const float mean_kernel,
    const float * gpu_buffer,
    float * result)
{
    extern __shared__ float chache[];
    const unsigned int chacheindex = threadIdx.x ;
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float var_img=0;
    float var_kernel=0;

    if (indx<num_total_voxels)
    {

	if (gpu_buffer[indx*4]>0)
	{
	  var_img=gpu_buffer[indx*4+1]-mean_intensities;
	  var_kernel=gpu_buffer[indx*4+3]-mean_kernel;
	}
    }
    
   chache[chacheindex] = var_img*var_img;
   chache[chacheindex+blockDim.x] = var_kernel*var_kernel;

    __syncthreads () ;
    
    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
      {
	    chache[chacheindex] += chache [chacheindex + i] ;
	    chache[chacheindex+blockDim.x] += chache [chacheindex+blockDim.x + i] ;
      }
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
      {
	    result[blockIdx.x] = chache [0] ;
	    result[blockIdx.x+gridDim.x] = chache [blockDim.x] ;
      }
}



__global__ void kernel_evaluate( 
    const int     num_total_voxels,
    const float * gpu_buffer,
    float * result,
    float *debug_kernel)
{
    extern __shared__ float chache[];
    const unsigned int chacheindex = threadIdx.x ;
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float value=0;

    if (indx<num_total_voxels)
    {

	if (gpu_buffer[indx*4]>0)
	{
	  value=gpu_buffer[indx*4+1]*gpu_buffer[indx*4+3];
	}
       if (debug_kernel!=NULL)
       {
        debug_kernel[indx]=value;
       }
    }
    
    chache[chacheindex] = value;

    __syncthreads () ;
    

//       if ( chacheindex == 0 )
//       {
// 	result[blockIdx.x]=0;
// 	for (int i=0;i<blockDim.x;i++)
// 	    result[blockIdx.x]+= chache [i] ;
//       }

    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
	    chache[chacheindex] += chache [chacheindex + i] ;
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
	    result[blockIdx.x] = chache [0] ;
    
}






__global__ void kernel_mean( 
    const float3  position,
    const float3  offset,
    const float3  box2kernel,
    const float3  scales2,
    const int     num_total_voxels,
    const int3    bb,
    cudaTextureObject_t tex,
    float * result
  ,float * debug_kernel
			      )
{
    extern __shared__ float chache[];
    
    const unsigned int chacheindex = threadIdx.x ;
    
    
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float value=0;
    float window=0;
    
    if (indx<num_total_voxels)
    {
      int z=(indx/(bb.x*bb.y))-offset.z;
      indx%=(bb.y*bb.x);
      int y=(indx/(bb.x))-offset.y;
      int x=(indx%(bb.x))-offset.x;
      
      float dist=(z/offset.z);
      dist*=dist;
      if (!(dist>1+__FLT_EPSILON__))
      {
	float tmp=(y/offset.y);
	dist+=tmp*tmp;
	if (!(dist>1+__FLT_EPSILON__))
	{
	  tmp=(x/offset.x);
	  dist+=tmp*tmp;
	  if (!(dist>1+__FLT_EPSILON__))
	  {

		float kx=x*box2kernel.x;
		float ky=y*box2kernel.y;
		float kz=z*box2kernel.z;
		
		  
		const float & s1_2=scales2.z;
		const float & s2_2=scales2.y;
		const float & s3_2=scales2.x;
		
 		float3 img_pos;
 		img_pos.x=position.x+kx+0.5f;
 		img_pos.y=position.y+ky+0.5f;
 		img_pos.z=position.z+kz+0.5f;
		
		float pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/2.0f;
		window=expf(-pol_t);
 		value=tex3D<float>(tex, img_pos.x, img_pos.y, img_pos.z)*window;
	  }
	}
      }
       if (debug_kernel!=NULL)
       {
        int indx2=((z+offset.z)*bb.y+(y+offset.y))*bb.x+(x+offset.x);
        debug_kernel[indx2]=value;
       }
    }
    
    
    chache[chacheindex] = value;
    chache[chacheindex+blockDim.x] = window;

    __syncthreads () ;
    

//       if ( chacheindex == 0 )
//       {
// 	result[blockIdx.x]=0;
// 	for (int i=0;i<blockDim.x;i++)
// 	    result[blockIdx.x]+= chache [i] ;
//       }

    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
      {
	    chache[chacheindex] += chache [chacheindex + i] ;
	    chache[chacheindex+blockDim.x] += chache [chacheindex+blockDim.x + i] ;
      }
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
      {
	    result[blockIdx.x] = chache [0] ;
	    result[blockIdx.x+gridDim.x] = chache [blockDim.x] ;
      }
}

__global__ void kernel_sfilter( 
    const float3  position,
    const float3  v0,
    const float3  v1,
    const float3  v2,
    const float3  offset,
    const float3  box2kernel,
    const float3  scales2,
    const float2  scales4,
    const int     num_total_voxels,
    const int3    bb,
    cudaTextureObject_t tex,
    float * result
  ,float * debug_kernel
			      )
{
  
//     __shared__ float chache[blockDim.x] ;
    extern __shared__ float chache[];
//        extern __shared__ int chache[];
//     int *integerData = s;                        // nI ints
//     float *floatData = (float*)&integerData[nI]; // nF floats
//     char *charData = (char*)&floatData[nF];      // nC chars
    
    
    const unsigned int chacheindex = threadIdx.x ;
    //   int i = blockIdx.x *blockDim.x + threadIdx.x;
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float value=0;
    
  
    
  
    

    if (indx<num_total_voxels)
    {
      int z=(indx/(bb.x*bb.y))-offset.z;
      indx%=(bb.y*bb.x);
      int y=(indx/(bb.x))-offset.y;
      int x=(indx%(bb.x))-offset.x;
      
      
	
     
      float dist=(z/offset.z);
      dist*=dist;
      if (!(dist>1+__FLT_EPSILON__))
      {
	float tmp=(y/offset.y);
	dist+=tmp*tmp;
	if (!(dist>1+__FLT_EPSILON__))
	{
	  tmp=(x/offset.x);
	  dist+=tmp*tmp;
	  if (!(dist>1+__FLT_EPSILON__))
	  {

		float kx=x*box2kernel.x;
		float ky=y*box2kernel.y;
		float kz=z*box2kernel.z;
		
		  
		const float & s1_2=scales2.z;
		const float & s2_2=scales2.y;
		const float & s3_2=scales2.x;
		const float & s2_4=scales4.y;
		const float & s3_4=scales4.x;
		
 		float3 img_pos;
 		img_pos.x=position.x+v0.x*kz+v1.x*kx+v2.x*ky+0.5f;
 		img_pos.y=position.y+v0.y*kz+v1.y*kx+v2.y*ky+0.5f;
 		img_pos.z=position.z+v0.z*kz+v1.z*kx+v2.z*ky+0.5f;
		
 		float intensity = tex3D<float>(tex, img_pos.x, img_pos.y, img_pos.z);
		  
		float h_img=intensity;
		  
		  float pol=0;
		  float pol_t=0;
		  pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
		  pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/2.0f;

  		  float g=expf(-pol_t);
//  		  float g=__expf(-pol_t);

		  float h_kernel=g*pol;
		  
		  value=h_img*h_kernel;
	  }
	}
      }
       if (debug_kernel!=NULL)
       {
        int indx2=((z+offset.z)*bb.y+(y+offset.y))*bb.x+(x+offset.x);
        debug_kernel[indx2]=value;
       }
    }
    
    
    chache[chacheindex] = value;

    __syncthreads () ;
    

//       if ( chacheindex == 0 )
//       {
// 	result[blockIdx.x]=0;
// 	for (int i=0;i<blockDim.x;i++)
// 	    result[blockIdx.x]+= chache [i] ;
//       }

    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
	    chache[chacheindex] += chache [chacheindex + i] ;
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
	    result[blockIdx.x] = chache [0] ;
    
    
}

__global__ void kernel_sfilter( 
    const float3  position,
    const float3  v0,
    const float3  v1,
    const float3  v2,
    const float3  offset,
    const float3  box2kernel,
    const float3  box2image,
    const float3  scales2,
    const float2  scales4,
    const int     num_total_voxels,
    const int3    bb,
    cudaTextureObject_t tex,
    float * result
  ,float * debug_kernel
			      )
{
  
//     __shared__ float chache[blockDim.x] ;
    extern __shared__ float chache[];
//        extern __shared__ int chache[];
//     int *integerData = s;                        // nI ints
//     float *floatData = (float*)&integerData[nI]; // nF floats
//     char *charData = (char*)&floatData[nF];      // nC chars
    
    
    const unsigned int chacheindex = threadIdx.x ;
    //   int i = blockIdx.x *blockDim.x + threadIdx.x;
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float value=0;
    
  
    
  
    

    if (indx<num_total_voxels)
    {
      int z=(indx/(bb.x*bb.y))-offset.z;
      indx%=(bb.y*bb.x);
      int y=(indx/(bb.x))-offset.y;
      int x=(indx%(bb.x))-offset.x;
      
      
	
     
      float dist=(z/offset.z);
      dist*=dist;
      if (!(dist>1+__FLT_EPSILON__))
      {
	float tmp=(y/offset.y);
	dist+=tmp*tmp;
	if (!(dist>1+__FLT_EPSILON__))
	{
	  tmp=(x/offset.x);
	  dist+=tmp*tmp;
	  if (!(dist>1+__FLT_EPSILON__))
	  {

		float kx=x*box2kernel.x;
		float ky=y*box2kernel.y;
		float kz=z*box2kernel.z;

		float ix=x*box2image.x;
		float iy=y*box2image.y;
		float iz=z*box2image.z;		
		
		  
		const float & s1_2=scales2.z;
		const float & s2_2=scales2.y;
		const float & s3_2=scales2.x;
		const float & s2_4=scales4.y;
		const float & s3_4=scales4.x;
		
 		float3 img_pos;
 		img_pos.x=position.x+v0.x*iz+v1.x*ix+v2.x*iy+0.5f;
 		img_pos.y=position.y+v0.y*iz+v1.y*ix+v2.y*iy+0.5f;
 		img_pos.z=position.z+v0.z*iz+v1.z*ix+v2.z*iy+0.5f;
		
 		float intensity = tex3D<float>(tex, img_pos.x, img_pos.y, img_pos.z);
		  
		float h_img=intensity;
		  
		  float pol=0;
		  float pol_t=0;
		  pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
		  pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/2.0f;

  		  float g=expf(-pol_t);
//  		  float g=__expf(-pol_t);

		  float h_kernel=g*pol;
		  
		  value=h_img*h_kernel;
	  }
	}
      }
       if (debug_kernel!=NULL)
       {
        int indx2=((z+offset.z)*bb.y+(y+offset.y))*bb.x+(x+offset.x);
        debug_kernel[indx2]=value;
       }
    }
    
    
    chache[chacheindex] = value;

    __syncthreads () ;
    

//       if ( chacheindex == 0 )
//       {
// 	result[blockIdx.x]=0;
// 	for (int i=0;i<blockDim.x;i++)
// 	    result[blockIdx.x]+= chache [i] ;
//       }

    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
	    chache[chacheindex] += chache [chacheindex + i] ;
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
	    result[blockIdx.x] = chache [0] ;
    
}


__global__ void kernel_sfilter2( 
    const float3  position,
    const float3  v0,
    const float3  v1,
    const float3  v2,
    const float3  offset,
//     const float3  box2kernel,
    const float3  scales2,
    const float2  scales4,
    const int     num_total_voxels,
    const int3    bb,
    cudaTextureObject_t tex,
    float * result
  ,float * debug_kernel
			      )
{
  
//     __shared__ float chache[blockDim.x] ;
    extern __shared__ float chache[];
//        extern __shared__ int chache[];
//     int *integerData = s;                        // nI ints
//     float *floatData = (float*)&integerData[nI]; // nF floats
//     char *charData = (char*)&floatData[nF];      // nC chars
    
    
    const unsigned int chacheindex = threadIdx.x ;
    //   int i = blockIdx.x *blockDim.x + threadIdx.x;
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;
    float value=0;
    
    if (indx<num_total_voxels)
    {
      int z=(indx/(bb.x*bb.y))-offset.z;
      indx%=(bb.y*bb.x);
      int y=(indx/(bb.x))-offset.y;
      int x=(indx%(bb.x))-offset.x;
      
      
	
     
      float dist=(z/offset.z);
      dist*=dist;
      if (!(dist>1+__FLT_EPSILON__))
      {
	float tmp=(y/offset.y);
	dist+=tmp*tmp;
	if (!(dist>1+__FLT_EPSILON__))
	{
	  tmp=(x/offset.x);
	  dist+=tmp*tmp;
	  if (!(dist>1+__FLT_EPSILON__))
	  {

		

/*		float ix=x*box2kernel.x;
		float iy=y*box2kernel.y;
		float iz=z*box2kernel.z;	*/	
		
		  
		const float & s1_2=scales2.z;
		const float & s2_2=scales2.y;
		const float & s3_2=scales2.x;
		const float & s2_4=scales4.y;
		const float & s3_4=scales4.x;
		
 		float3 img_pos;
		img_pos.x=position.x+v0.x*z+v1.x*x+v2.x*y+0.5f;
 		img_pos.y=position.y+v0.y*z+v1.y*x+v2.y*y+0.5f;
 		img_pos.z=position.z+v0.z*z+v1.z*x+v2.z*y+0.5f;
//  		img_pos.x=position.x+v0.x*iz+v1.x*ix+v2.x*iy+0.5f;
//  		img_pos.y=position.y+v0.y*iz+v1.y*ix+v2.y*iy+0.5f;
//  		img_pos.z=position.z+v0.z*iz+v1.z*ix+v2.z*iy+0.5f;
		
 		float intensity = tex3D<float>(tex, img_pos.x, img_pos.y, img_pos.z);
		  
		int xx=x*x;
		int yy=y*y;

		float  pol=(  s3_2*s2_4  -  s2_4*(xx) + s3_4 *s2_2 - s3_4*(yy)  )/(s2_4*s3_4);
		float  pol_t=((xx)/s3_2 + (yy)/s2_2 +(z*z)/s1_2)/2.0f;

		float g=expf(-pol_t);
//  		  float g=__expf(-pol_t);

// 		float h_kernel=g*pol;
		
		value=intensity*(g*pol);
	  }
	}
      }
       if (debug_kernel!=NULL)
       {
        int indx2=((z+offset.z)*bb.y+(y+offset.y))*bb.x+(x+offset.x);
        debug_kernel[indx2]=value;
       }
    }
    
    
    chache[chacheindex] = value;

    __syncthreads () ;
    

//       if ( chacheindex == 0 )
//       {
// 	result[blockIdx.x]=0;
// 	for (int i=0;i<blockDim.x;i++)
// 	    result[blockIdx.x]+= chache [i] ;
//       }

    int i  = blockDim.x / 2 ;

    while ( i!=0 )
    {

      if ( chacheindex < i )
	    chache[chacheindex] += chache [chacheindex + i] ;
    
    __syncthreads () ;

      i/=2 ;
    }

      if ( chacheindex == 0 )
	    result[blockIdx.x] = chache [0] ;
    
}
#endif

