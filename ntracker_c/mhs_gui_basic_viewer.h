#ifndef MHS_INTERACTIVE_BASIC_VIEWER_H
#define MHS_INTERACTIVE_BASIC_VIEWER_H


#include "mhs_error.h"
#include "mhs_gui.h"
#include "ext/ArcBall.cpp"
#include "mhs_graphics.h"
#include "proposals.h"
#include  <iomanip>
#include  "mhs_jpg.h"


#include <sys/stat.h>

bool file_exists(const std::string& name) {
    struct stat buffer;   
      return (stat (name.c_str(), &buffer) == 0); 
      }





template<typename TData,typename T, int Dim>
class CViewer;
//   class CViewer: public CSceneRenderer, public CVrender<TData,T>;


template<typename T, int Dim>
class CCamPath
{
public:

    CCamPath() {

    }

    ~CCamPath() {

    }
};


template<typename TData,typename T, int Dim>
class Mouse
{
public:
    int mouse_pos_old[2];
    bool b_down[3];
    bool b_down_old[3];

    bool arcball_on;
    bool trackball_on;
    bool trackballZ_on;
    bool transition_on;
    bool zoom_on;
    bool do_rendering;
    bool point_select_on;

    int  point_select_mode;

    //Mouse mouse_state;
    ArcBallT * arcBall;
    Matrix3fT   LastRot;
    Matrix3fT   ThisRot;

    Matrix<float,4> m_transition;
    Vector<T,Dim> rot_center;

    Vector<float,3>  focus_point;
    Vector<float,3>  focus_point_img;

    bool exit_program;
    int num_keys=1000;
    Uint8 * keystates;
    Uint8 * keystates_back;
    Uint8 Fkeys[12];

    int call_from_main;

    static const Matrix3fT   Identity3x3;


    int mouse_pos[2];
    const Uint8* keys;
    int mouse_wheel;
    int cpy_keys;


    mhs::CtimeStopper timer;
    bool cockpit_Z_on;    
    double cockpit_last_update;

    CViewer<TData,T,Dim> * viewer;

    bool key_is_pressed;

    Mouse ( CViewer<TData,T,Dim> * viewer=NULL ) {

	
	
      
        sta_assert_error ( viewer!=NULL );
        this->viewer=viewer;
        mouse_pos_old[0]=mouse_pos_old[1]=0;
        b_down[0]=b_down[1]=b_down[2]=false;
        b_down_old[0]=b_down_old[1]=b_down_old[2]=false;

	focus_point=float(0);
	focus_point_img=float(0);
	m_transition.Identity();
	

        arcball_on=false;
        trackball_on=false;
        trackballZ_on=false;
	cockpit_Z_on=false;
	
        zoom_on=false;
        do_rendering=true;
        transition_on=false;
        point_select_mode=0;
        point_select_on=false;
        cpy_keys=0;
        point_select_on=false;
	call_from_main=0;
	
	key_is_pressed=false;

        memcpy ( &LastRot,&Identity3x3,sizeof ( Matrix3fT ) );
        memcpy ( &ThisRot,&Identity3x3,sizeof ( Matrix3fT ) );

        arcBall=new ArcBallT ( viewer->height,viewer->width );

        exit_program=false;
        rot_center=T ( 0 );

        const Uint8* keys  = SDL_GetKeyboardState ( &num_keys );
        printf ( "max number of keys: %d\n",num_keys );
        keystates= new Uint8[num_keys];
        keystates_back= new Uint8[num_keys];
        for ( int i=0; i<num_keys; i++ ) {
            keystates[i]=0;
            keystates_back[i]=0;
        }

        Fkeys[0]=SDL_SCANCODE_F1;
        Fkeys[1]=SDL_SCANCODE_F2;
        Fkeys[2]=SDL_SCANCODE_F3;
        Fkeys[3]=SDL_SCANCODE_F4;
        Fkeys[4]=SDL_SCANCODE_F5;
        Fkeys[5]=SDL_SCANCODE_F6;
        Fkeys[6]=SDL_SCANCODE_F7;
        Fkeys[7]=SDL_SCANCODE_F8;
        Fkeys[8]=SDL_SCANCODE_F9;
        Fkeys[9]=SDL_SCANCODE_F10;
        Fkeys[10]=SDL_SCANCODE_F11;
        Fkeys[11]=SDL_SCANCODE_F12;
    }

    ~Mouse() {
        delete arcBall;
        delete [] keystates;
        delete [] keystates_back;

    }


    bool is_busy() {
        return ( trackball_on||arcball_on||trackballZ_on||transition_on||point_select_on||cockpit_Z_on );
    }
    void reset() {
        // NOTE just added this :point_select_on (0716)
        trackball_on=arcball_on=trackballZ_on=transition_on=point_select_on=cockpit_Z_on=false;
    }




    bool  update_state() {
        int num_events=0;
        SDL_Event event;
        mouse_wheel=0;
	
	 key_is_pressed=false;

        while ( SDL_PollEvent ( &event ) ) {
            num_events++;
            switch ( event.type ) {
            case SDL_MOUSEWHEEL: {
                mouse_wheel=event.wheel.y;
            }break;
	    case SDL_KEYDOWN: {
	      key_is_pressed=true;
	    }break;
            }
        }


       

        if ( num_events==0 ) {
            return false;
        }

        

        b_down_old[0]=b_down[0];
        b_down_old[1]=b_down[1];
        b_down_old[2]=b_down[2];


        Uint32 button_state=SDL_GetMouseState ( mouse_pos,mouse_pos+1 );
        b_down[0]= ( button_state & SDL_BUTTON ( SDL_BUTTON_LEFT ) ) && ( ! ( button_state & SDL_BUTTON ( SDL_BUTTON_RIGHT ) ) );
        b_down[1]= ( button_state & SDL_BUTTON ( SDL_BUTTON_RIGHT ) ) && ( ! ( button_state & SDL_BUTTON ( SDL_BUTTON_LEFT ) ) );
        b_down[2]= ( button_state & SDL_BUTTON ( SDL_BUTTON_RIGHT ) ) && ( button_state & SDL_BUTTON ( SDL_BUTTON_LEFT ) );




        if ( cpy_keys>0 ) {
            memcpy ( keystates,keystates_back,sizeof ( Uint8 ) *cpy_keys );
        }

        keys=NULL;
        cpy_keys=0;
        keys  = SDL_GetKeyboardState ( &cpy_keys );
        sta_assert_error ( keys!=NULL );

        //printf("num events: %d / %d\n",num_events, keys[SDL_SCANCODE_Q]);

        sta_assert_error_m ( ! ( cpy_keys>num_keys ),cpy_keys<<" "<<num_keys );
        memcpy ( keystates_back,keys,sizeof ( Uint8 ) *cpy_keys );

        return true;
    }







    virtual bool  ui() {
// 	printf("updating state");
//
//       update_state();
//       printf("updating state done\n");
//       return false;


        CViewer<TData,T,Dim> & V=*viewer;

        if ( !V.is_ready() ) {
            return false;
        }

        float rescale=V.rescale;

        Vector<float,3> shape=V.get_shape();

//  	float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);




        if ( ( transition_on ) ) {

            if ( focus_point.norm1() <rescale ) {
                V.m_transform=V.m_transform_current*V.m_transform;
                V.m_transform_current.Identity();
                V.update_focus_point ( this );
                reset();
            } else {
                focus_point=m_transition.multv3 ( focus_point );
                V.m_transform_current=m_transition*V.m_transform_current;
            }

            return false;
        }



        if ( !update_state() ) {
// 	  if (( transition_on ) )
// 	    return true;
	  
            return false;
        }


       

        //process mouse wheel events
        if ( mouse_wheel!=0 ) {
            if ( keys[SDL_SCANCODE_LCTRL] ) {
                {
                    if ( mouse_wheel<0 ) {
                        V.clip_plane_dist+=1/50.0f;
                    } else {
                        V.clip_plane_dist-=1/50.0f;
                    }
                }
//                 V.clip_plane_dist=std::min ( 1.0f,std::max ( V.clip_plane_dist,0.0f ) );
//  		  V.clip_plane_dist=std::min ( float(V.viewer_dist*0.9),std::max ( V.clip_plane_dist,0.0f ) );
		  V.clip_plane_dist=std::min ( float(V.viewer_dist*0.9),std::max ( V.clip_plane_dist,-5.0f ) );
		  
// 		  printf("clipdist %f\n",V.clip_plane_dist);
		
	    } else   if ( keys[SDL_SCANCODE_LSHIFT] ) {
                {
                    if ( mouse_wheel<0 ) {
                        V.viewer_dist-=1/50.0f;
                    } else {
                        V.viewer_dist+=1/50.0f;
                    }
                }
                V.viewer_dist=std::min ( 2.0f,std::max ( V.viewer_dist,0.01f ) );
		V.viewer_pos[2]=V.viewer_dist;
		V.update_projection_mat();
            //} else if  (keys[SDL_SCANCODE_SPACE]){//(!key_is_pressed) {
		} else if  ((!key_is_pressed)&&(b_down[1])) {
                {
                    if ( mouse_wheel<0 ) {
                        V.zoom/=1.15;
                    } else {
                        V.zoom*=1.15;
                    }
                }
                V.zoom=std::min ( 4.0f,std::max ( rescale,V.zoom ) );
//  		printf("%f\n",V.zoom);
                V.update_projection_mat();
// 		printf("zoom %f\n",V.zoom);
            }
        }


        //processing keyboard events
        {
            if ( keys[SDL_SCANCODE_Q]&& ( !keystates[SDL_SCANCODE_Q] ) ) {
                if ( exit_program ) {
                    return false;
                }
                V.add_info ( "exit" );
// 		printf("exiting program\n");
                exit_program=true;
            }

            if ( keys[SDL_SCANCODE_V]&& ( !keystates[SDL_SCANCODE_V] ) ) {
                do_rendering=!do_rendering;
                if ( do_rendering ) {
                    V.add_info ( "volume rendering: on" );
                } else {
                    V.add_info ( "volume rendering: off" );
                }
            }


            
            
            if ( keys[SDL_SCANCODE_G]&& ( !keystates[SDL_SCANCODE_G] ) ) {
                if ( call_from_main==0 ) {
                    V.add_info ( "showing matlab rendering GUI" );
                    call_from_main=2;
                }
            }
            
            
           if ( keys[SDL_SCANCODE_H]&& ( !keystates[SDL_SCANCODE_H] ) ) {
                
                V.clip_plane_hard=!V.clip_plane_hard;
            }
            
            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_M]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_M] ) )
                    {
                    //V.render_mode=(V.render_mode+1)%4+1;
                    
                    V.render_mode+=1;
                    V.render_mode%=4;
                    printf("render_mode basic viewer:%d\n",V.render_mode);
                    //V.render_mode=(V.render_mode+1)%3+1;
                     //V.render_mode++;
                     //V.render_mode%=5;

                    }            

            
            if ( !transition_on )
	    {
	      
	      Uint8 ckeys[2];
	      ckeys[0]=SDL_SCANCODE_UP;
	      ckeys[1]=SDL_SCANCODE_DOWN;
	      for (int a=0;a<2;a++)
	      {
		if ( keys[ckeys[a]]) {
		  if (!cockpit_Z_on)
		  {
			cockpit_Z_on=true;   
			cockpit_last_update=timer.get_total_runtime();
			V.m_transform_current.m[3][2]=0;
			V.update_focus_point ( this );
		  }else
		  {
		    double current_time=timer.get_total_runtime();
		    if ((current_time-cockpit_last_update)>0.01)
		    {
			cockpit_last_update=current_time;
			V.m_transform_current.m[3][2]+=(1-2*a)*0.02;
			V.update_focus_point ( this );
		    }
		  }   
		}else  if ( keystates[ckeys[a]] )
		{
		  
		  cockpit_Z_on=false; 
		  V.m_transform=V.m_transform_current*V.m_transform;
			  V.m_transform_current.Identity();
		}
	      }
	      
	      
	    }
             
            
        }

// 	if (is_busy())
// 	  printf("is busy\n");
// 	else
// 	  printf("is not busy\n");



        /// mouse interaction
        if ( ( !transition_on ) ) {
            //printf("%d %d %d / %d %d\n",b_down[0],b_down[1],b_down[2],is_busy(),transition_on);
            for ( int b=0; b<3; b++ ) {
                if ( b_down[b] ) {
                    if ( !b_down_old[b] ) { // mouse_down_event
                        if ( !is_busy() ) {
                            switch ( b ) {
                            case 0: {
//  			      printf("case 0\n");
                                if ( keys[SDL_SCANCODE_LSHIFT] ) {
                                    {
                                        V.add_info ( "data cursor: on" );
                                        point_select_mode=1;
                                    }
                                    point_select_on=true;
                                    mouse_pos_old[0]=-1;
                                    mouse_pos_old[1]=-1;
                                } else
                                    //TODO basically if NO key is pressed
                                    if ( ! ( ( keys[SDL_SCANCODE_LCTRL] ) || ( keys[SDL_SCANCODE_LSHIFT] ) ) ) {
//  				printf("1");
                                        mouse_pos_old[0]=mouse_pos[0];
                                        mouse_pos_old[1]=mouse_pos[1];
                                        arcball_on=true;
                                        Point2fT    MousePt;
                                        MousePt.s.Y=mouse_pos[0];
                                        MousePt.s.X=V.height-mouse_pos[1];
                                        LastRot = ThisRot;										// Set Last Static Rotation
                                        arcBall->click ( &MousePt );
                                        float tx= ( mouse_pos[0] ) / ( float ( V.width ) )-0.5f;
                                        float ty= ( mouse_pos[1] ) / ( float ( V.height ) )-0.5f;
                                        rot_center[0]=tx;
                                        rot_center[1]=ty;
                                        memcpy ( &LastRot,&Identity3x3,sizeof ( Matrix3fT ) );
                                    }
                            }
                            break;
                            case 1: {
                                mouse_pos_old[0]=mouse_pos[0];
                                mouse_pos_old[1]=mouse_pos[1];
                                trackball_on=true;
                            }
                            break;
                            case 2: {
                                mouse_pos_old[0]=mouse_pos[0];
                                mouse_pos_old[1]=mouse_pos[1];
                                trackballZ_on=true;
                            }
                            break;
                            }
                        }
                    } else { //  mouse_is_down
                        if ( arcball_on ) {
                            Quat4fT     ThisQuat;
                            Point2fT    MousePt;
                            MousePt.s.Y=mouse_pos[0];
                            MousePt.s.X=V.height-mouse_pos[1];
                            arcBall->drag ( &MousePt, &ThisQuat );						// Update End Vector And Get Rotation As Quaternion
                            Matrix3fSetRotationFromQuat4f ( &ThisRot, &ThisQuat );		// Convert Quaternion Into Matrix3fT
                            Matrix3fMulMatrix3f ( &ThisRot, &LastRot );				// Accumulate Last Rotation Into This One

                            V.m_transform_current.OpenGL_from3x3 ( ThisRot.M );
                            V.update_focus_point ( this );
// 				printf("yea!\n");

                        } else if ( trackball_on ) {
                            float tx= ( mouse_pos_old[0]-mouse_pos[0] ) / ( float ( V.width ) );
                            float ty= ( mouse_pos_old[1]-mouse_pos[1] ) / ( float ( V.height ) );

//                             V.m_transform_current.m[3][0]=ty*V.zoom*3/V.viewer_dist;
//                             V.m_transform_current.m[3][1]=tx*V.zoom*3/V.viewer_dist;
			    V.m_transform_current.m[3][0]=ty*V.zoom*30/V.viewer_dist;
                            V.m_transform_current.m[3][1]=tx*V.zoom*30/V.viewer_dist;
                            V.update_focus_point ( this );

                        } else if ( trackballZ_on ) {
                            {

                                float ty= ( mouse_pos_old[1]-mouse_pos[1] ) / ( float ( V.height ) );

                                 V.m_transform_current.m[3][2]=-ty*3.0f;
				//V.m_transform_current.m[3][2]=-ty*300.0f;
                                V.update_focus_point ( this );
                            }
                        } else if ( point_select_on && ( point_select_mode==1 ) ) {
                            if ( ( mouse_pos_old[0]!=mouse_pos[0] ) && ( mouse_pos_old[1]!=mouse_pos[1] ) ) {

                                V.img_hittest ( mouse_pos[0],mouse_pos[1],focus_point,focus_point_img );
                                mouse_pos_old[0]=mouse_pos[0];
                                mouse_pos_old[1]=mouse_pos[1];
                            }
                        }
                    }
                } else if ( b_down_old[b] ) { // mouse_up_event
                    if ( b==0 ) {
                        if ( arcball_on ) {
                            arcball_on=false;
                            V.m_transform=V.m_transform_current*V.m_transform;
                            V.m_transform_current.Identity();
                        }
                        if ( ( point_select_on ) ) {
                            transition_on=true;
                            V.m_transform_current.Identity();
                            m_transition.Identity();
                            m_transition.OpenGL_set_tranV ( focus_point*-0.2 );

                            if ( point_select_mode==1 ) {
                                V.add_info ( "data cursor: off" );
                            }

                        }
                    }
                    if ( b==1 ) {
                        trackball_on=false;
                        V.m_transform=V.m_transform_current*V.m_transform;
                        V.m_transform_current.Identity();
                    }
                    if ( b==2 ) {
                        trackballZ_on=false;
                        V.m_transform=V.m_transform_current*V.m_transform;
                        V.m_transform_current.Identity();
                    }
                    V.m_transform.OpenGL_reorthonormalize();
                }
                //TODO hae?
                //b_down_old[b]=b_down[b];
            }
        }
        
        return true;
    }
};


template<typename TData,typename T, int Dim>
const Matrix3fT Mouse<TData,T,Dim>::Identity3x3= {  1.0f,  0.0f,  0.0f,					// NEW: Last Rotation
                                    0.0f,  1.0f,  0.0f,
                                    0.0f,  0.0f,  1.0f
                                                 };



template<typename TData,typename T, int Dim>
class CViewer: public CSceneRenderer, public CVrender<TData,T>
{

    friend   Mouse<TData,T, Dim>;
public:


  bool vis_options_updated;



protected:

    Vector<float,4> shader_options;

    bool ready;

#ifdef  _USE_CGC_    
    CGparameter _modelview;
    CGparameter _normalmat;
    CGparameter _projection;

   
    CGprogram   _gcMyPixelProgram;
    CGprogram   _gcMyVertexProgram;

    CGparameter _focuspointV;
    CGparameter _vertex_mode;
    CGparameter _clipping_plane;
    
#else
	  GLint glsl_projection;
	  GLint glsl_modelview;
	  GLint glsl_normalmat;
	   
	   GLint glsl_focuspoint;
	   GLint glsl_vertex_mode;
	   GLint glsl_clipping_plane;
	   GLint glsl_colorness_min;
	   GLint glsl_alpha;
       
       GLint  glsl_dist_clip;

	   
	   GLint glsl_render_mode;
	   
	   GLint glsl_ambient;
	   
	   GLint glsl_clipping_hard;
	   
	   Shader *shader;
    
#endif    
    
    CGLtext<T>  * text_HUD;
    
    const TData * 	img;
    float rescale;
    Vector<float,3> new_center;

    bool do_vrendering;
    
    int tex_height=-1;
    int tex_width=-1;

    Vector<float,3> background_color;
    Vector<float,3> font_color;

    Matrix<float,4> m_transform;
    Matrix<float,4> m_transform_current;
    Matrix<float,4> m_world_trafo;

    Matrix<float,4> projection_mat;


//     Vector<float,Dim> /*viewer__start_pos*/;
    Vector<float,Dim> viewer_pos;
    Vector<float,Dim> viewer_target;
    Vector<float,Dim> viewer_up;

    Vector<float,3> clip_plane;
    
//     Vector<float,3> visual_voxel_size;



    float zoom;
    float clip_plane_dist;
    bool clip_plane_hard;
    float viewer_dist;
    
    int render_mode=0;

    Mouse<TData,T, Dim> * mouse_state;



    std::list<std::string> string_info_stack;
    int string_info_stack_mmem=3;
    void add_info ( std::string s ) {
        string_info_stack.push_front ( s );
        if ( string_info_stack.size() >string_info_stack_mmem ) {
            string_info_stack.pop_back();
        }
    }
    
    
    std::size_t num_extra_options=5;
    float extra_options[5];

public:
  
    const  Matrix<float,4> & get_world_trafo(){return m_world_trafo;};
    const  Matrix<float,4> & get_current_trafo(){return m_transform_current;};
    void reset_current_trafo(){m_transform_current.Identity();};
    void set_trafo(Matrix<float,4>  & trafo){m_transform=trafo;};
    
    
//     float get_zoom(){return zoom;};
//     float set_zoom(float zoom){this->zoom=zoom;update_projection_mat();};
//     
//     float get_clip_plane_dist(){return clip_plane_dist;};
//     float set_clip_plane_dist(float clip_plane_dist){this->clip_plane_dist=clip_plane_dist;};
// 
//     float get_viewer_dist(){return viewer_dist;};
//     float set_viewer_dist(float viewer_dist){this->viewer_dist=viewer_dist;update_projection_mat();};
    
     std::vector<float> get_options_vector()
    {
      
      std::vector<float> options;
      options.resize(4+1+1+1+3+3+3+1+1+1+num_extra_options);
      int count=0;
      for (int a=0;a<4;a++)
      options[count++]=this->shader_light_options[a];
      
      options[count++]=this->parameter_pixelDensity;
      options[count++]=this->parameter_pixelDensityThreshold;
      options[count++]=this->parameter_gamma;
      
      for (int a=0;a<3;a++)
      options[count++]=this->color[a];
      
      for (int a=0;a<3;a++)
      options[count++]=this->background_color[a];
      
      for (int a=0;a<3;a++)
      options[count++]=this->font_color[a];
      
      
      options[count++]=zoom;
      options[count++]=clip_plane_dist;
      options[count++]=viewer_dist;
      
      for (int a=0;a<num_extra_options;a++)
      options[count++]=this->extra_options[a];
      return options;
    }
      

    
    void set_options_vector(std::vector<float>  & options)
    {
      sta_assert_error(options.size()==4+1+1+1+3+3+3+1+1+1+num_extra_options)
      int count=0;
      for (int a=0;a<4;a++)
      this->shader_light_options[a]=options[count++];
      
      this->parameter_pixelDensity=options[count++];
      this->parameter_pixelDensityThreshold=options[count++];
      this->parameter_gamma=options[count++];
      
      for (int a=0;a<3;a++)
      this->color[a]=options[count++];
      
      for (int a=0;a<3;a++)
      this->background_color[a]=options[count++];
      
      for (int a=0;a<3;a++)
      this->font_color[a]=options[count++];
      
      zoom=options[count++];
      clip_plane_dist=options[count++];
      viewer_dist=options[count++];
      
       for (int a=0;a<num_extra_options;a++)
      this->extra_options[a]=options[count++];
      
      
      viewer_pos[2]=viewer_dist;
      update_projection_mat();
    }
    
    
    bool is_ready() {
        return ready;
    }
    
    bool is_busy()
    {
     return mouse_state->is_busy(); 
    }

    Vector<float,3> get_shape() {

        Vector<float,3> scene_shape;
        scene_shape=CSceneRenderer::shape;
        return scene_shape;
    }



    bool  ui() {
// 	printf("\ncalling ui");
        return mouse_state->ui();
// 	printf("done\n");

    }

    bool is_exit_program() {
        return mouse_state->exit_program;
    }

    void send_exit_program_signal() {


//        printf("sending exit signal (thred %d)\n",omp_get_thread_num());
        mouse_state->exit_program=true;
    }



    bool img_hittest ( GLdouble  winX,  GLdouble  winY, Vector<float,3> & focus_point, Vector<float,3> & focus_point_img ) {
        //TODO ; consider cliplane
        Matrix<double,4> projection;
        Matrix<double,4> modelview;

        Vector<double,3> hit0;
        Vector<double,3> hit1;

        Vector<int,4> viewport;
        viewport[0]=0;
        viewport[1]=0;
        viewport[2]=this->width;
        viewport[3]=this->height;

        projection=CViewer<TData,T,Dim>::projection_mat;
        modelview= ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform );

        float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );

        Vector<float,3> new_center;
        new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
        new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
        new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;
        Matrix<float,4> trafo;
        trafo=modelview;

        gluUnProject ( winX,this->height-winY,0.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit0.v[0],&hit0.v[1],&hit0.v[2] );
        gluUnProject ( winX,this->height-winY,1.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit1.v[0],&hit1.v[1],&hit1.v[2] );

        Vector<float,3> ray_start;
        Vector<float,3> ray_dir;

        //without clipping
        ray_start=hit0;
        ray_dir= ( hit1-hit0 );
        ray_dir.normalize();

        {

            ray_start+=new_center;
            ray_start/=rescale;

            if ( img!=NULL ) {
                Vector<float,3> selection;
                int max_depth=2.0*std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );
                int num_intersections=0;
// 		TData sensitivity=0.25;
                TData sensitivity=0.5;

                TData max_value=std::numeric_limits<TData>::min();
                TData min_value=std::numeric_limits<TData>::max();
                TData threshold;
                TData stepsize=1;



		
		 Vector<float,3> step;
                for ( int run=0; run<2; run++ ) {
                    Vector<float,3> pt=ray_start;
                    num_intersections=0;
                    threshold=min_value+ ( max_value-min_value ) *sensitivity;

		    step=ray_dir*stepsize;
// 		    if (run==1)
// 		    {
// 		    printf("[%f %f %f]\n",min_value,threshold,max_value);
// 		    }

                    for ( int r=0; r<max_depth; r++ ) {
                        bool pt_in_img= ( pt[0]>0 ) && ( pt[1]>0 ) && ( pt[2]>0 ) && ( pt[0]<CSceneRenderer::shape[0] ) && ( pt[1]<CSceneRenderer::shape[1] ) && ( pt[2]<CSceneRenderer::shape[2] );

                        if ( ( ( pt_in_img ) && ( num_intersections==0 ) )
                                || ( ( !pt_in_img ) && ( num_intersections==1 ) ) ) {
                            num_intersections++;
                        }

                        if ( num_intersections==1 ) {
                            std::size_t position= ( std::floor ( pt[0] ) *CSceneRenderer::shape[1]
                                                    +std::floor ( pt[1] ) ) *CSceneRenderer::shape[2]
                                                  +std::floor ( pt[2] );
                            if ( position>CSceneRenderer::shape[0]*CSceneRenderer::shape[1]*CSceneRenderer::shape[2] ) {
                                printf ( "point selection: out of volume!\n" );
                                continue;
                            }
                            TData value=img[position];
                            if ( run==0 ) {
                                max_value=std::max ( max_value,value );
                                min_value=std::min ( min_value,value );
                            } else {
                                if ( value>threshold ) {
// 				printf("depth: %d\n",r);
                                    focus_point=trafo.multv3 ( ( pt*rescale-new_center ) );
                                    focus_point_img=pt;

                                    return true;
                                }
                            }
                        }
                        if ( num_intersections==2 ) {
                            stepsize=0.75;
                            max_depth=r;
                            break;
                        }
                        pt+=step;
                    }
                }


            }
        }



        return false;

    }

    //TODO move to Mouse class
    Vector<float,3> focusGL2img2 ( Vector<float,3> & focus_point ) {
//         Matrix<double,4> projection;
        Matrix<double,4> modelview;


        Vector<int,4> viewport;
        viewport[0]=0;
        viewport[1]=0;
        viewport[2]=this->width;
        viewport[3]=this->height;


//         projection=CViewer<TData,T,Dim>::projection_mat;
        modelview= ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform );

        float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );

        Vector<float,3>new_center;
        new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
        new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
        new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;
        Matrix<float,4> trafo;
        trafo=modelview;

//         return ( ( trafo.OpenGL_invert().multv3 ( focus_point ) +new_center ) /rescale );
	return ( ( trafo.OpenGL_inverse().multv3 ( focus_point ) +new_center ) /rescale );
    }
    
    
      void camposGL2img (Vector<float,3> & campos,Vector<float,3> & camvirtualpos,Vector<float,3> & lookat) {
      
      
      double winX=width/2.0;
      double winY=height/2.0;
      
      
      

      Vector<double,3> hit0;
      Vector<double,3> hit1;

      Vector<int,4> viewport;
      viewport[0]=0;
      viewport[1]=0;
      viewport[2]=this->width;
      viewport[3]=this->height;


      Matrix<float,4> & projection=CViewer<TData,T,Dim>::projection_mat;
      Matrix<float,4> & modelview= m_world_trafo;
      
      Matrix<double,4> projection_double;
      projection_double=projection;
      Matrix<double,4> modelview_double;
      modelview_double= modelview;
      
      

      float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );

      Vector<float,3>new_center;
      new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
      new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
      new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;
//       Matrix<float,4> trafo;
//       trafo=modelview;

      gluUnProject ( winX,winY,0.0, ( const GLdouble * ) ( modelview_double.v ), ( const GLdouble * ) ( projection_double.v ), ( const GLint * ) ( viewport.v ), &hit0.v[0],&hit0.v[1],&hit0.v[2] );
      //gluUnProject ( winX,winY,1.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit1.v[0],&hit1.v[1],&hit1.v[2] );

      Vector<float,3> ray_start;
//       Vector<float,3> ray_end;
      Vector<float,3> ray_dir;

      ray_start=hit0;
//       ray_end=hit1;
//       ray_dir= ( hit1-hit0 );
//       ray_dir.normalize();
      
      
//        Vector<float,3> lookat;
       lookat=modelview.OpenGL_inverse().multv3 ( mouse_state->focus_point );
      
       
//        (Quaternion<float>(trafo).getMatrix()).print();
//        trafo.print();
       
//       Vector<float,3> ray_dir2;
//       ray_dir2=lookat-ray_start;
//       ray_dir2.normalize();

       Vector<float,3> camd_dir;
       camd_dir=(lookat-ray_start);
       camd_dir.normalize();
       
//        camd_dir.print();
       
//        ray_start.print();
       
//        trafo=Matrix<float,4>::OpenGL_glTranslate(new_center*float(-1))*trafo*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(rescale,rescale,rescale));
//         (trafo.OpenGL_inverse().multv3 ( viewer_pos )).print();
       //(trafo.multv3 ( viewer_pos )).print();
       
//        campos=lookat-camd_dir*zoom*4;
//        campos=ray_start-camd_dir*zoom;
      camvirtualpos=lookat-(lookat-ray_start)*zoom*4;
      
      
	camvirtualpos+=new_center;
        camvirtualpos/=rescale;
      
       campos=ray_start;
       campos+=new_center;
       campos/=rescale;
       
       lookat+=new_center;
       lookat/=rescale;
       
       
       //campos.print();
      
    }
    

    //TODO move to Mouse class
    void update_focus_point ( Mouse<TData,T, Dim> * mouse_state ) {
        {
            mouse_state->focus_point=T ( 0 );
            mouse_state->focus_point_img=focusGL2img2 ( mouse_state->focus_point );
        }
    }



    void update_projection_mat() {
//         float zNear=1;
//    	float zNear=viewer_pos[2]/2.0f;
      
       float zNear=viewer_pos[2]*0.1;
//        float zNear=viewer_pos[2]/std::sqrt(2);
        float zFar=200;
	
	float scale_w=1;
	float scale_h=1;
	
	float ratio=(float)width/(float)height;
	if (ratio>1)
	{
	  scale_w=ratio;
	}else{
	  scale_h=1/ratio;
	}
	
	
	
	
         projection_mat=Matrix<float,4>::OpenGL_glFrustum ( -zoom*scale_w, zoom*scale_w,
                        - zoom*scale_h, zoom*scale_h,
                        zNear, zFar );
         
         
         //Matrix<float,4> tex_scale=Matrix<float,4>::OpenGL_glScale(0.5,0.5,0.5);
         
		projection_mat*=Matrix<float,4>::OpenGL_glLooktAt ( viewer_pos,viewer_target,viewer_up );
                

        //if ((tex_width>0)&&(tex_height>0))
         if (false)       
        {
               
            float correct_sw=((float)tex_width)/(width);
            float correct_sh=((float)tex_height)/(height);
            
              Matrix<float,4> tex_scale=Matrix<float,4>::OpenGL_glScale(Vector<float,3> (correct_sw,correct_sh,1.0f));
            float correct_w=-((float)width-(float)tex_width)/(width);
            float correct_h=-((float)height-(float)tex_height)/(height);
            Matrix<float,4> tex_trans=Matrix<float,4>::OpenGL_glTranslate(Vector<float,3> (correct_w,correct_h,0.0f));
            projection_mat=tex_trans*tex_scale*projection_mat;
            //projection_mat.print();
        }      
// 	  projection_mat=Matrix<float,4>::OpenGL_glFrustum ( -scale_w, scale_w,
//                        - scale_h, scale_h,
//                        zNear/zoom, zFar );
//          projection_mat*=Matrix<float,4>::OpenGL_glLooktAt ( viewer_pos*zoom,viewer_target,viewer_up );
	
	//projection_mat.print();
    }

    void save_state_in_struct ( mxArray * tracker_state ) {
        if ( tracker_state!=NULL ) {
            try {
                float * data_p=mhs::mex_getFieldPtrCreate<float> ( tracker_state,16,"observer_trafo",0 );
                for ( int i=0; i<16; i++ ) {
                    data_p[i]=m_transform.v[i];
                }
                data_p=mhs::mex_getFieldPtrCreate<float> ( tracker_state,1,"observer_zoom",0 );
                *data_p=zoom;

                data_p=mhs::mex_getFieldPtrCreate<float> ( tracker_state,1,"observer_clipdist",0 );
                *data_p=clip_plane_dist;
		
		data_p=mhs::mex_getFieldPtrCreate<float> ( tracker_state,1,"observer_viewer_dist",0 );
                *data_p=viewer_dist;


// 	    data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,1,"observer_show_volume",0);
// 	    *data_p= mouse_state.do_rendering;


            } catch ( mhs::STAError error ) {
                printf ( "could'nt save oberser state :%s\n",error.str().c_str() );
            }
        }
    }


    void load_state_from_struct ( const mxArray * tracker_state ) {
        if ( tracker_state!=NULL ) {
            try {
                float * data_p =mhs::mex_getFieldPtr<float> ( tracker_state,"observer_trafo" );
                for ( int i=0; i<16; i++ ) {
                    m_transform.v[i]=data_p[i];
                }
                data_p =mhs::mex_getFieldPtr<float> ( tracker_state,"observer_zoom" );
                zoom=*data_p;

                data_p =mhs::mex_getFieldPtr<float> ( tracker_state,"observer_clipdist" );
                clip_plane_dist=*data_p;
		

// 	  data_p=mhs::mex_getFieldPtr<float>(tracker_state,"observer_show_volume");
// 	  mouse_state.do_rendering=*data_p;

                update_projection_mat();
            } catch ( mhs::STAError error ) {
                printf ( "could'nt find oberser state :%s\n",error.str().c_str() );
            }
            try {
		
	      float * data_p =mhs::mex_getFieldPtr<float> ( tracker_state,"observer_trafo" );
	      data_p=mhs::mex_getFieldPtr<float> ( tracker_state,"observer_viewer_dist" );
                viewer_dist=*data_p;
            } catch ( mhs::STAError error ) {
                printf ( "warning: could'nt find newer oberser states :%s\n",error.str().c_str() );
            }

            
        }
    }


    void set_shape ( const std::size_t shape[] ) {

        CSceneRenderer::set_shape ( shape );
        rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );

        new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
        new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
        new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;
    }



    void init ( const mxArray * feature_struct ) {
        if ( feature_struct==NULL ) {
            return;
        }

        do_vrendering=true;

        try {

            CVrender<TData,T>::init ( feature_struct );

        } catch ( mhs::STAError error ) {
            do_vrendering=false;
            printf ( "volume rendering disabled: %s\n",error.str().c_str() );
        }


        try {

//             clip_plane_dist=1;
            update_focus_point ( mouse_state );

            img=mhs::dataArray<TData> ( feature_struct,"img" ).data;
        }  catch ( mhs::STAError error ) {
            printf ( "error, missing raw image: %s\n",error.str().c_str() );

        }
        
        printf ( "updateing controls\n" );
        update_tracker_controls ( true );
        printf ( "ok\n" );

    }

    CViewer ( std::size_t * existing_handle=NULL ) : CSceneRenderer ( existing_handle ), CVrender<TData,T>() {

      
	vis_options_updated=false;
        ready=false;
        shader_options=0.0f;
        do_vrendering=false;
        this->set_window_handle ( this->get_window_handle() );

        //background_color=0.0f;
	background_color=0.0f;
        font_color=1.0f;

	viewer_dist=2;
        viewer_pos[0]=0;
        viewer_pos[1]=0;
        viewer_pos[2]=viewer_dist;
	viewer_target=T ( 0 );
        viewer_up=T ( 0 );
        viewer_up[0]=1;
	
// 	viewer_pos[0]=viewer_dist;
//         viewer_pos[1]=0;
//         viewer_pos[2]=0;
// 	viewer_target=T ( 0 );
//         viewer_up=T ( 0 );
//         viewer_up[1]=1;
	
//         viewer__start_pos=viewer_pos;
//         viewer_target=T ( 0 );
//         viewer_up=T ( 0 );
//         viewer_up[0]=1;

        m_transform.Identity();
        m_transform_current.Identity();
	
// 	update_focus_point ( mouse_state );

	for (int i=0;i<num_extra_options;i++)
	extra_options[i]=0;
	
	
	
//         zoom=0.45;
	zoom=0.1;
// 	printf("zoom %f\n",zoom);
        clip_plane_dist=viewer_dist*0.9;
        clip_plane=float ( 0 );
        clip_plane[0]=float ( clip_plane_dist );
	
	clip_plane_hard=true;

        update_projection_mat();

        mouse_state=new Mouse<TData,T, Dim> ( this );

        img=NULL;

        text_HUD=new CGLtext<T> ( this->width,this->height );


        rescale=1;
        new_center=float ( 0.0f );


        for ( int i=0; i<string_info_stack_mmem; i++ ) {
            string_info_stack.push_front ( "" );
        }

#ifdef  _USE_CGC_    
        try {
            {
                std::stringstream s;
                s<<shader_path<<"model_render_shader.cg";
                CSceneRenderer::tracker_data_p->_cgprogram->disableProgram ( CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );

                CSceneRenderer::tracker_data_p->_cgprogram-> Load_Program ( _gcMyVertexProgram,s.str(),CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );
                this->_normalmat= cgGetNamedParameter ( _gcMyVertexProgram, "normalmat" );
                this->_modelview= cgGetNamedParameter ( _gcMyVertexProgram, "modelview" );
                this->_projection= cgGetNamedParameter ( _gcMyVertexProgram, "projection" );
                _focuspointV= cgGetNamedParameter ( _gcMyVertexProgram, "focuspoint" );
                _vertex_mode= cgGetNamedParameter ( _gcMyVertexProgram, "vertex_mode" );



            }
            {
                std::stringstream s;
                s<<shader_path<<"model_render_pshader.cg";
                CSceneRenderer::tracker_data_p->_cgprogram->disableProgram ( CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
                CSceneRenderer::tracker_data_p->_cgprogram-> Load_Program ( _gcMyPixelProgram,s.str(),CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
                _clipping_plane= cgGetNamedParameter ( _gcMyPixelProgram, "clipping_plane" );
            }
            if ( !PrintlastCGerror() ) {
                printf ( "sucessfully loaded the shader programs\n" );
            } else {

                throw 0;
            }

        } catch ( ... ) {

            printf ( "error loading the shader programs\n" );

        }
        PrintlastCGerror();
#else
	shader=NULL;
	try {
	shader=new Shader(application_directory+"/model_render_vshader.glsl",
	  application_directory+"/model_render_pshader.glsl");
	
	glsl_projection=shader->get_variable("glsl_projection");
	   mhs_check_gl
	glsl_modelview=shader->get_variable("glsl_modelview");
	   mhs_check_gl
	glsl_normalmat=shader->get_variable("glsl_normalmat");
	   mhs_check_gl
	glsl_focuspoint=shader->get_variable("glsl_focuspoint");
	   mhs_check_gl
	glsl_vertex_mode=shader->get_variable("glsl_vertex_mode");
	   mhs_check_gl
	glsl_clipping_plane=shader->get_variable("glsl_clipping_plane");
	   mhs_check_gl
	   
	glsl_render_mode=shader->get_variable("glsl_render_mode");
	   mhs_check_gl   
	   
	   
      glsl_ambient=shader->get_variable("glsl_ambient");
	   mhs_check_gl	   
	   
	   
	   glsl_dist_clip=shader->get_variable("glsl_dist_clip");
       mhs_check_gl	   
	   
	
	glsl_colorness_min=shader->get_variable("glsl_colorness_min");
	   mhs_check_gl
	   
	 glsl_alpha=shader->get_variable("glsl_alpha");
	   mhs_check_gl  
	 
	 glsl_clipping_hard=shader->get_variable("glsl_clipping_hard");
	   
// 	shader->get_log();
	  mhs_check_gl
	 } catch ( mhs::STAError error ) {

            mhs::STAError error2;
            error2<<error.str() <<"\n"<<"could not create the object";
            throw error2;
        }
	
#endif

    }

    ~CViewer() {
        if ( mouse_state!=NULL ) {
            delete mouse_state;
        }
        mouse_state=NULL;

#ifdef  _USE_CGC_ 
        std::cerr<<"deleting cgprograms"<<std::endl;
        if ( CSceneRenderer::tracker_data_p!=NULL ) {
            cgDestroyProgram ( _gcMyVertexProgram );
        }
#else
	if (shader!=NULL)
	  delete shader;
#endif
        
        if ( text_HUD!=NULL ) {
            delete text_HUD;
        }
    }

    
    
    void save_image_to_file(std::string filename)
    {
//       printf("screenshot\n");
	mwSize ndims[3];
	ndims[0]=height;
	ndims[1]=width;
	ndims[2]=3;
        mxArray *arg1 = mxCreateNumericArray ( 3,ndims,mxSINGLE_CLASS,mxREAL );
        float * data_p= ( float  * ) mxGetPr ( arg1 );
	mxArray *arg2 = mhs::mex_string2mex(filename);
      
        

	mxArray *pargin[2] = {arg1, arg2};
	
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_PACK_SKIP_ROWS, 0);
	glPixelStorei(GL_PACK_ROW_LENGTH, 0);
	
	float * buffer=new float [height*width*3];
	
       glReadPixels(0,0,this->width,this->height,
   	GL_RGB,
   	GL_FLOAT,buffer); 
       
       std::size_t num_pixels=width*height;
//        printf("%d %d \n",width,height);
       for (std::size_t t=0;t<num_pixels;t++)
       {
 	 std::size_t t_source_y=(t/width);
 	 std::size_t t_source_x=t%width;
  	 std::size_t t_dest=(t_source_x)*height+(height-t_source_y-1);
	 if (t_dest>=num_pixels)
	   break;
	data_p[t_dest]=buffer[t*3];
	data_p[t_dest+num_pixels]=buffer[t*3+1]; 
	data_p[t_dest+2*num_pixels]=buffer[t*3+2]; 
       }
       
       delete [] buffer;
	
        if ( mexCallMATLAB ( 0, NULL,2,pargin , "imwrite" ) !=0 ) {
            throw mhs::STAError( "coud'nt load vertex data\n" );
            
        }
        
        mxDestroyArray(arg1);
	mxDestroyArray(arg2);
    }
    
    
      void save_image_to_file2(std::string filename)
    {
//       printf("screenshot\n");

	
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_PACK_SKIP_ROWS, 0);
	glPixelStorei(GL_PACK_ROW_LENGTH, 0);
	
	unsigned char * buffer=new unsigned char [height*width*3];
// 	char * buffer2=new char [height*width*3];
	
       glReadPixels(0,0,this->width,this->height,
   	GL_RGB,
   	GL_UNSIGNED_BYTE,buffer); 
       
       
       
//       std::size_t num_pixels=width*height;
//        printf("%d %d \n",width,height);
//        for (std::size_t t=0;t<num_pixels;t++)
//        {
//  	 std::size_t t_source_y=(t/width);
//  	 std::size_t t_source_x=t%width;
//   	 std::size_t t_dest=(t_source_x)*height+(height-t_source_y-1);
// 	 if (t_dest>=num_pixels)
// 	   break;
// 	buffer2[t_dest]=buffer[t*3];
// 	buffer2[t_dest+num_pixels]=buffer[t*3+1]; 
// 	buffer2[t_dest+2*num_pixels]=buffer[t*3+2]; 
//        }
        write_jpg(filename,width,height, buffer);
       
       delete [] buffer;
//        delete [] buffer2;
	

    }

    bool render_init_default(bool center_axes=true) {

        ready=true;


        GLfloat light_position[] = { 0, 0, -2.0, 0.0 };
        GLfloat light_dir[4] = { 0,0,1, 1 };

        GLfloat light_ambient[] = { 0.1, 0.1, 0.1, 1.0 };
        GLfloat light_diffuse[] = { 0.9, 0.9, 0.9, 1.0 };
        GLfloat light_specular[] = { 0.5, 0.5, 0.5, 1.0 };
        GLfloat global_ambient[]= {0.0,0.0,0.0,1.0};


        GLfloat whiteSpecularMaterial[] = {1.0,1.0, 1.0};
        GLfloat mShininess[] = {23};
        GLfloat materialEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};






        glMatrixMode ( GL_PROJECTION );
        glLoadIdentity();


        glMultMatrixf ( ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v );

   
	m_world_trafo=CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform;
	
	
        glEnable ( GL_CULL_FACE );
        glCullFace ( GL_BACK );

        glLightfv ( GL_LIGHT0, GL_POSITION, light_position );
        glLightfv ( GL_LIGHT0, GL_SPOT_DIRECTION, light_dir );
        glLightfv ( GL_LIGHT0, GL_AMBIENT, light_ambient );

        glLightfv ( GL_LIGHT0, GL_DIFFUSE, light_diffuse );
        glLightfv ( GL_LIGHT0, GL_SPECULAR, light_specular );
        glEnable ( GL_COLOR_MATERIAL );
        glMaterialfv ( GL_FRONT_AND_BACK, GL_SHININESS, mShininess );
        glMaterialfv ( GL_FRONT_AND_BACK, GL_SPECULAR, whiteSpecularMaterial );
        glMaterialfv ( GL_FRONT_AND_BACK, GL_EMISSION, materialEmission );
        glMaterialfv ( GL_FRONT_AND_BACK, GL_DIFFUSE, light_diffuse );

        glLightModeli ( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
        glLightModelfv ( GL_LIGHT_MODEL_AMBIENT,global_ambient );

        glLightf ( GL_LIGHT0,GL_SPOT_EXPONENT,64 );
        glLightf ( GL_LIGHT0,GL_SPOT_CUTOFF,70 );

        glEnable ( GL_NORMALIZE );

        glClearColor ( CViewer<TData,T,Dim>::background_color[0],CViewer<TData,T,Dim>::background_color[1],CViewer<TData,T,Dim>::background_color[2],1 );
        glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        glDisable ( GL_LIGHTING );

        glDisable ( GL_BLEND );

        glDisable ( GL_CLIP_PLANE0 );
        glDisable ( GL_CLIP_PLANE1 );
        glDisable ( GL_CLIP_PLANE2 );
        glDisable ( GL_CLIP_PLANE3 );
        glDisable ( GL_CLIP_PLANE4 );
        glDisable ( GL_CLIP_PLANE5 );

        glMatrixMode ( GL_MODELVIEW );

	if (center_axes)
        {
            glLoadIdentity();

            glTranslatef ( mouse_state->focus_point[0],mouse_state->focus_point[1],mouse_state->focus_point[2] );

            glMultMatrixf ( ( GLfloat * ) ( ( m_world_trafo ).OpenGL_get_rot().v ) );
            Vector<T,3>axis_pos ( 0,0,0 );
            Vector<T,3>axis_color ( 1,0.5,1 );
            glLineWidth ( 2 );
            CViewer<TData,T,Dim>::text_HUD->XYZAxis ( 0.05,axis_pos,axis_color );
        }

        glLoadIdentity();



        CViewer<TData,T,Dim>::shader_options[0]=0;
        if ( mouse_state->is_busy() ) { //((mouse_state.trackballZ_on)||(mouse_state.trackball_on)||(mouse_state.arcball_on))
            CViewer<TData,T,Dim>::shader_options[0]=1;
        }



        CViewer<TData,T,Dim>::shader_options[1]=1/rescale;

      

        clip_plane[0]=CViewer<TData,T,Dim>::clip_plane_dist;

	
	

	

	if (false)
	{
        glMultMatrixf ( ( GLfloat * ) ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform ).v );

        Vector<float,3> center=new_center*T ( -1 );;

        glTranslatef ( center[0],center[1],center[2] );
	}else
	{
	  Vector<float,3> center=new_center*T ( -1 );;
	 Matrix<float,4>  translation=Matrix<float,4>::OpenGL_glTranslate(center);
	 
	  glMultMatrixf ( ( GLfloat * ) ( m_world_trafo*translation ).v ); 
	}
	
	

	
	
// 	new_center.print();
	//
	
	 

        return true;
    }

    void enable_model_shader() {
#ifdef  _USE_CGC_       
        if ( CSceneRenderer::tracker_data_p->_cgprogram ) {
            CSceneRenderer::tracker_data_p->_cgprogram->enableProfile ( CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );
            CSceneRenderer::tracker_data_p->_cgprogram->enableProfile ( CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
            cgGLBindProgram ( _gcMyVertexProgram );
            cgGLBindProgram ( _gcMyPixelProgram );
        }
#else
      shader->bind();
#endif
    }

    void disable_model_shader() {
#ifdef  _USE_CGC_    
        if ( CSceneRenderer::tracker_data_p->_cgprogram ) {
            CSceneRenderer::tracker_data_p->_cgprogram->disableProfile ( CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
            CSceneRenderer::tracker_data_p->_cgprogram->disableProfile ( CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );
        }
#else
      shader->unbind();
#endif
    }



    void render_volume() {
        if ( ( do_vrendering ) && ( mouse_state->do_rendering ) ) {
// 	float new_center[3];
// 	new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
// 	new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
// 	new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;

            Vector<float,3>  center;
            center[0]=0.5*CSceneRenderer::shape[0]*rescale-new_center[0];
            center[1]=0.5*CSceneRenderer::shape[1]*rescale-new_center[1];
            center[2]=0.5*CSceneRenderer::shape[2]*rescale-new_center[2];

            Matrix<float,4> trafo= ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform );
            Vector<float,3> translation=trafo.multv3 ( center );
            Matrix<float,4> rotation=trafo.OpenGL_get_rot();

            this->paint ( CViewer<TData,T,Dim>::projection_mat,
			  rotation,
                          translation,
                          shader_options,
                          mouse_state->focus_point,
                          clip_plane );
        }

    }


    void render_print_info_stack() {

        int offset=0;
        for ( std::list<std::string>::iterator iter=string_info_stack.begin(); iter!=string_info_stack.end(); iter++ ) {

            if ( offset==1 ) {
                glColor4f ( 0.5*font_color[0],0.5*font_color[1],0.5*font_color[2],1 );
            }
            CViewer<TData,T,Dim>::text_HUD->StringoutLine ( *iter,
                    CViewer<TData,T,Dim>::text_HUD->Stringou_getLastLine()-string_info_stack.size() +offset++ );
        }

    }

    void do_render() {
        if ( mouse_state->call_from_main>0 ) {
            switch ( mouse_state->call_from_main ) {
	    case 2:
	    {
		  mexCallMATLAB(0, NULL,0,NULL, "start_gui2");
		  mouse_state->call_from_main=0;
	    }break;
            }
            
        }

    }
    
    
    
     void render_outline() {
//         MyMouse<TData,T,Dim> * mouse_state= ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state;
        if ( ( mouse_state->is_busy() )  )  {
            float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );
           

            //if (!(mouse_state.point_select_on))
            {
                float left=0;
                float right=CSceneRenderer::shape[0]*rescale;
                float top=0;
                float bottom=CSceneRenderer::shape[1]*rescale;
                float front=0;
                float back=CSceneRenderer::shape[2]*rescale;
                glLineWidth ( 1 );


                glDisable ( GL_CULL_FACE );
                glLineStipple ( 1, ( short ) 0x00FF );
                glEnable ( GL_LINE_STIPPLE );
                glPolygonMode ( GL_FRONT_AND_BACK,GL_LINE );
                //glColor3f(0.0f,0.0f,0.7f);
                glColor3fv ( this->particle_colors[CSceneRenderer::Corange] );
                glBegin ( GL_QUADS );
                glVertex3f ( left,top,back );
                glVertex3f ( left,top,front );
                glVertex3f ( left,bottom,front );
                glVertex3f ( left,bottom,back );
                glVertex3f ( right,top,back );
                glVertex3f ( right,top,front );
                glVertex3f ( right,bottom,front );
                glVertex3f ( right,bottom,back );
                glVertex3f ( left,bottom,back );
                glVertex3f ( right,bottom,back );
                glVertex3f ( right,top,back );
                glVertex3f ( left,top,back );
                glVertex3f ( left,bottom,front );
                glVertex3f ( right,bottom,front );
                glVertex3f ( right,top,front );
                glVertex3f ( left,top,front );
                glVertex3f ( left,top,front );
                glVertex3f ( left,top,back );
                glVertex3f ( right,top,back );
                glVertex3f ( right,top,front );
                glVertex3f ( left,bottom,front );
                glVertex3f ( left,bottom,back );
                glVertex3f ( right,bottom,back );
                glVertex3f ( right,bottom,front );
                glEnd();
                glPolygonMode ( GL_FRONT_AND_BACK,GL_FILL );
                glDisable ( GL_LINE_STIPPLE );
                glEnable ( GL_CULL_FACE );
            }

        }
    }
    
    
    
    virtual void update_tracker_controls ( bool force_update=false ) {
#ifndef _OCTAVE_    
	try 
	{
        mhs::mex_dumpStringNOW();
        const mxArray * handle=mexGetVariablePtr ( "global", "tracker_rendering_window_controls");
        if ( handle!=NULL ) {
            const mxArray *changed= mxGetField ( handle,0, ( char * ) ( "changed" ) );
            if ( changed!=NULL ) {
                double * handle_p= ( double  * ) mxGetPr ( changed );
                if ( ( ( *handle_p ) ==1 ) || ( force_update ) ) {
                    ( *handle_p ) =2;
		    vis_options_updated=true;
//                     printf ( "controls have been updated\n" );


                    const mxArray *
                    parameter= mxGetField ( handle,0, ( char * ) ( "volume_pixelDensityThreshold" ) );
                    if ( parameter!=NULL ) {
                        this->parameter_pixelDensityThreshold=* ( double* ) mxGetPr ( parameter );
                    }

                    parameter= mxGetField ( handle,0, ( char * ) ( "volume_pixelDensity" ) );
                    if ( parameter!=NULL ) {
                        this->parameter_pixelDensity=* ( double* ) mxGetPr ( parameter );
                    }

                    parameter= mxGetField ( handle,0, ( char * ) ( "volume_bgc" ) );
                    if ( parameter!=NULL ) {
                        this->background_color[0]=* ( ( double* ) mxGetPr ( parameter ) );
                        this->background_color[1]=* ( ( ( double* ) mxGetPr ( parameter ) ) +1 );
                        this->background_color[2]=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
                        CViewer<TData,T,Dim>::font_color=1-std::max ( std::max (
                                                             CViewer<TData,T,Dim>::background_color[0],
                                                             CViewer<TData,T,Dim>::background_color[1] ),
                                                         CViewer<TData,T,Dim>::background_color[2] );
                    };

                    parameter= mxGetField ( handle,0, ( char * ) ( "volume_lighting" ) );
                    if ( parameter!=NULL ) {
                        if ( this->do_vrendering ) {
                            this->set_light ( * ( ( double* ) mxGetPr ( parameter ) ),
                                              * ( ( ( double* ) mxGetPr ( parameter ) ) +1 ),
                                              * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ),
                                              * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) );
                        }
                    };
		    
		    parameter= mxGetField ( handle,0, ( char * ) ( "color_mode" ) );
                    if ( parameter!=NULL ) {
                        if ( this->do_vrendering ) {
                            this->set_color_mode(* ( double* ) mxGetPr ( parameter ));
                        }
                    };
		    
		    parameter= mxGetField ( handle,0, ( char * ) ( "parameter_gamma" ) );
                    if ( parameter!=NULL ) {
                        if ( this->do_vrendering ) {
                            this->parameter_gamma=(* ( double* ) mxGetPr ( parameter ));
                        }
                    };
		    
		    
		     parameter= mxGetField ( handle,0, ( char * ) ( "color" ) );
                    if ( parameter!=NULL ) {
                        if ( this->do_vrendering ) {
                            this->color[0]=* ( ( double* ) mxGetPr ( parameter ) );
			    this->color[1]=* ( ( ( double* ) mxGetPr ( parameter ) ) +1 );
			    this->color[2]=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
                        }
                    };
		    
		    
                }
            }
        }
        } catch (mhs::STAError error)
	{
	 throw error; 
	}
#endif	
    }


};





















#endif
