#ifndef CONSTRAINTS_H
#define CONSTRAINTS_H

#include "global.h"
#include "edges.h"


template<typename T,int Dim> class Connection;
template<typename T,int Dim> class Points;

class Constraints
{
public:

    //must be at least 3, otherwise the inser death can create 
    //a double connection out of a triagnle
    template<typename T,int Dim>
    static bool constraint_hasloop_follow(Points<T,Dim> & npoint,
                                   class Connection<T,Dim> * new_connection,
                                   int max_depths=1,
                                   int depths=0,
                                   Points<T,Dim> * startpoint=NULL,
				   ///TODO: das unten brauch ich nicht mehr wenn bif center particle
				   EDGE_TYPES last_edge_type=EDGE_TYPES::EDGE_UNDEFINED,
				   int thread_id=-1)
    {
      
// 	return constraint_hasloop(npoint,
// 				      max_depths,
// 				      depths,
// 				      startpoint);
      
#ifdef D_USE_MULTITHREAD
	if (thread_id==-1)
	  thread_id= omp_get_thread_num();
#else
	thread_id= 0;
#endif
	
		      #ifdef  D_USE_GUI
			//npoint.update_depth(depths);
			  npoint.depth=(npoint.depth > depths) ? npoint.depth : depths;
		      #endif
	
  
        if (depths>max_depths)
            return false;

	
        //! first node should follow new connection
        if (depths==0)
        {
            if (new_connection!=NULL)
            {

                track_unique_id[thread_id]++;
                sta_assert_error(new_connection->pointA->point==&npoint);
                new_connection->track_me_id=track_unique_id[thread_id];
		new_connection->track_depth=depths;
                return constraint_hasloop_follow(*(new_connection->pointB->point),
                                          new_connection,
                                          max_depths,
                                          depths+1,
                                          &npoint,
					  new_connection->edge_type,thread_id);

            } else
                return false; //! no new connection. so if there was no loop there is still no loop
        } else
        {

            for (int end_id=0; end_id<2; end_id++)
            {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

                for (int i=0; i<Points<T,Dim>::maxconnections; i++)
                {
                    if (endpointsA[i].connected!=NULL)
                    {
                        //! check if edge has not been tracked so far
//                         if (endpointsA[i].connection->track_me_id!=track_id)
//                         {
//                             endpointsA[i].connection->track_me_id=track_id;
			    
			if (((endpointsA[i].connection->track_me_id!=track_unique_id[thread_id])||
			  ((endpointsA[i].connection->track_me_id==track_unique_id[thread_id])&&(
			    endpointsA[i].connection->track_depth>depths)))
// 			    &&(
// 			     (endpointsA[i].connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION)||
// 			     (last_edge_type!=EDGE_TYPES::EDGE_BIFURCATION)
// 			    )
			   )
                         {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];
 			    endpointsA[i].connection->track_depth=depths;			    
			    

                            if (endpointsA[i].connected->point==startpoint)
			    {
                                return true;
			    }


                            if (constraint_hasloop_follow(*(endpointsA[i].connected->point),
                                                   new_connection,
                                                   max_depths,
                                                   depths+1,
                                                   startpoint,
						   endpointsA[i].connection->edge_type,thread_id))
			    {
                                return true;
			    }
                        }
                    }
                }
            }
        }
        return false;
	
    }
    
    
    //must be at least 3, otherwise the inser death can create 
    //a double connection out of a triagnle
    template<typename T,int Dim>
    static bool constraint_hasloop(Points<T,Dim> & npoint,
                                   int max_depths=1,
                                   int depths=0,
                                   Points<T,Dim> * startpoint=NULL,
				   ///TODO: das unten brauch ich nicht mehr wenn bif center particle
				   EDGE_TYPES last_edge_type=EDGE_TYPES::EDGE_UNDEFINED,int thread_id=-1)
    {
        if (depths>max_depths)
            return false;
	
	
	#ifdef D_USE_MULTITHREAD
	if (thread_id==-1)
	  thread_id= omp_get_thread_num();
	#else
		thread_id= 0;
	#endif


        if (depths==0)
	{
                track_unique_id[thread_id]++;
		startpoint=&npoint;
	}else
	{
	    sta_assert_error(startpoint!=NULL);
	}
        {

            for (int end_id=0; end_id<2; end_id++)
            {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

                for (int i=0; i<Points<T,Dim>::maxconnections; i++)
                {
                    if (endpointsA[i].connected!=NULL)
                    {
                        //! check if edge has not been tracked so far
//                         if (endpointsA[i].connection->track_me_id!=track_id)
		                              //! check if edge has not been tracked so far
                        if (((endpointsA[i].connection->track_me_id!=track_unique_id[thread_id])||
			  ((endpointsA[i].connection->track_me_id==track_unique_id[thread_id])&&(
			    endpointsA[i].connection->track_depth>depths)))
// 			    &&(
// 			     (endpointsA[i].connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION)||
// 			     (last_edge_type!=EDGE_TYPES::EDGE_BIFURCATION)
// 			    )
			   )
// 		      if (endpointsA[i].connection->track_me_id!=track_id)
                         {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];
 			    endpointsA[i].connection->track_depth=depths;
                        

			    sta_assert_error(endpointsA[i].connected->point!=&npoint);
                            if (endpointsA[i].connected->point==startpoint)
                                return true;


                            if (constraint_hasloop(*(endpointsA[i].connected->point),
                                                   max_depths,
                                                   depths+1,
                                                   startpoint,
						   endpointsA[i].connection->edge_type,thread_id))
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    //must be at least 3, otherwise the inser death can create 
    //a double connection out of a triagnle
    template<typename T,int Dim>
    static bool constraint_printloop(Points<T,Dim> & npoint,
                                   int max_depths=1,
                                   int depths=0,
                                   Points<T,Dim> * startpoint=NULL,int thread_id=-1)
    {
        if (depths>max_depths)
            return false;
	
	
	#ifdef D_USE_MULTITHREAD
	if (thread_id==-1)
	  thread_id= omp_get_thread_num();
	#else
		thread_id= 0;
	#endif
		

	std::string prefix="";
	for (int j=0;j<depths;j++)
	  prefix+=" ";
	
	printf("%s->N (%d) %u %u\n",prefix.c_str(),depths,&npoint,startpoint);
        if (depths==0)
	{
                track_unique_id[thread_id]++;
		startpoint=&npoint;
	}else
	{
	  if (&npoint==startpoint)
	      printf("%s->LOOP \n",prefix.c_str());
	    sta_assert_error(startpoint!=NULL);
	}
        {

            for (int end_id=0; end_id<2; end_id++)
            {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];

                for (int i=0; i<Points<T,Dim>::maxconnections; i++)
                {
                    if (endpointsA[i].connected!=NULL)
                    {
                        //! check if edge has not been tracked so far
//                         if ((endpointsA[i].connection->track_me_id!=track_id)||
// 			  ((endpointsA[i].connection->track_me_id==track_id)&&(
// 			    endpointsA[i].connection->track_depth>depths)))
		      if (endpointsA[i].connection->track_me_id!=track_unique_id[thread_id])
                        {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];
// 			    endpointsA[i].connection->track_depth=depths;

			    sta_assert_error(endpointsA[i].connected->point!=&npoint);
			    
                            


                            if (constraint_printloop(*(endpointsA[i].connected->point),
                                                   max_depths,
                                                   depths+1,
                                                   startpoint,thread_id))
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

//     template<typename T,int Dim>
//     static bool constraint_isdirectedge(class Connection<T,Dim> * connection)
//     {
//         return (((connection->pointA->point->get_num_connections())>2)&&
//                 (((connection->pointB->point->endpoint_connections[0])>1)||
//                  ((connection->pointB->point->endpoint_connections[1])>1)))
//                ||
//                (((connection->pointB->point->get_num_connections())>2)&&
//                 (((connection->pointA->point->endpoint_connections[0])>1)||
//                  ((connection->pointA->point->endpoint_connections[1])>1)));
// 
//         //			    return (*(connection->pointA->endpoint_connections)>1)&&
//         // 				   (*(connection->pointB->endpoint_connections)>1);
//     }
};


#endif

