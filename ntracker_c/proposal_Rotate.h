#ifndef PROPOSAL_ROTATE_H
#define PROPOSAL_ROTATE_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CRotate : public CProposal<TData,T,Dim>
{
protected:
  
  
  bool temp_dependent;

public:
  
       T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
    return CProposal<TData,T,Dim>::dynamic_weight_particle_update(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
    
    CRotate(): CProposal<TData,T,Dim>()
    {
      temp_dependent=true;
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_ROTATE);
    };
    
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_ROTATE);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        if (params==NULL)
            return;
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
    }


    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

        try {

        } catch (mhs::STAError error)
        {
            throw error;
        }

    }

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	
	T temp_fact_rot=1;
	
	
// 	

	this->proposal_called++;

        OctPoints<T,Dim> *  cpoint;
        cpoint=tree.get_rand_point();
        if (cpoint==NULL)
            return false;


        
	

        Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	
	
	
	
	if (point.isfreezed())
            {
		return false;
	    }
	
	// do not touch bifurcation centers !
	if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		 return false;
	
	if (temp_dependent)
	  temp_fact_rot=proposal_temp_trafo(temp,point.get_scale(),T(1));
// 	  temp_fact_rot=std::sqrt(temp);
	
        /*
        if (return_statistic)
        {
            std::size_t center=(std::floor(point.position[0])*shape[1]
              +std::floor(point.position[1]))*shape[2]
              +std::floor(point.position[2]);
          static_data[center*2*numproposals+10]+=1;
        }
        */

        typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
        T old_cost=point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			       options.bifurcation_bonus_L,
			       inrange);
        sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),
			 //  sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),printf("[%d]",inrange));
			   
			   
	  (point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			       options.bifurcation_bonus_L,
			       inrange,true,true))
	);
	
	

	T particle_interaction_cost_old=0;
	T particle_interaction_cost_new=0;
	
	
	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	class Collision<T,Dim>::Candidates cand(query_buffer);
	if (collision_fun_p.is_soft())    
	{
	  
	  //sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,&cand));
	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp,&cand,true,-1));
	}
	
	  //NOTE: debug collision sorting
// 	  
// 	  class OctPoints<T,Dim> * query_buffer2[query_buffer_size];
// 	  class Collision<T,Dim>::Candidates cand2(query_buffer2);
// 	  T particle_interaction_cost_old2=0;
// 	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old2, tree,point,collision_search_rad,&cand2,true));
// 	  
	

        T oldenergy_data=point.point_cost;


        Vector<T,Dim> old_dir=point.get_direction();

	//T collision_finite_costs=0;
//  	class OctPoints<T,Dim> * search_query_buffer[query_buffer_size];
//  	class OctPoints<T,Dim> ** search_candidates;
//         std::size_t  search_found=0;
	
// 	if (Points<T,Dim>::collision_two_steps>0)
// 	{
// 	  sta_assert_error((!colliding( tree,point,collision_search_rad,collision_finite_costs)));
// 	}	
	
        point.set_direction(random_pic_direction<T,Dim>(point.get_direction(),temp_fact_rot));

// 	if (Points<T,Dim>::collision_two_steps>0)	    
// 	{
// 	  T collision_finite_costs_old=0;
// 	  if (colliding( tree,point,collision_search_rad,collision_finite_costs_old))
// 	  {
// 	    point.direction=old_dir;
// 	    return false;
// 	  }
// 	  collision_finite_costs=collision_finite_costs_old-collision_finite_costs;
// 	}else
	
	  bool out_of_bounce=false;
	{
	  
	  
	  bool check_for_bif=point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
	  
	  if (out_of_bounce)
	  {
	   point.set_direction(old_dir);
	    point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
	    sta_assert_debug0(!out_of_bounce);
	    return false;
	  }
	  
	  if (options.bifurcation_particle_hard && check_for_bif)
	  {
	      
	      for (int i=0;i<2;i++) 
	      {
		  if ((point.endpoint_connections[i]==1) && 
		     (point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
		  {
		    sta_assert_debug0(point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);  
		    
		    Points<T,Dim> & bif_center_point=*(point.endpoints[i][0]->connected->point);
		    
		 
		      if (collision_fun_p.colliding( tree,bif_center_point,collision_search_rad,temp))
		      {
			point.set_direction(old_dir);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
			return false;
		      }
		  }
	      }
	  }
	  
	  
	  /*
	  
	  if (options.bifurcation_particle_hard &&
	    point.bifurcation_center_neighbourhood_update())
	  {
	      for (int i=0;i<2;i++) 
	      {
		  if ((point.endpoint_connections[i]==1) && 
		     (point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
		  {
		    sta_assert_debug0(point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);  
		    
		     Points<T,Dim> & bif_center_point=*(point.endpoints[i][0]->connected->point);
		    
		      if ((bif_center_point.position[0]<0)||
		    (bif_center_point.position[1]<0)||
		    (bif_center_point.position[2]<0)||
		    (bif_center_point.position[0]>shape[0]-1)||
		    (bif_center_point.position[1]>shape[1]-1)||
		    (bif_center_point.position[2]>shape[2]-1))
		      {
			point.direction=old_dir;
			point.bifurcation_center_neighbourhood_update();
		      }
		      
		    if (colliding( tree,bif_center_point,collision_search_rad))
		      {
			point.direction=old_dir;
			point.bifurcation_center_neighbourhood_update();
			return false;
		      }
		    
		  }
	      }
	  }*/
	  
  //NOTE: debug collision sorting
//   {
//     if (collision_fun_p.is_soft())
//     {
//       T eA=0;
//       T eB=0;
//       
//       bool checkA=(collision_fun_p.colliding(eA,tree,point,collision_search_rad,&cand,false));
//       bool checkB=(collision_fun_p.colliding(eB,tree,point,collision_search_rad,&cand2,false));
//     
// 
//       if ((eA!=eB)||(checkA!=checkB))
//       {
// 	printf("%d %d / %f %f / %d %d\n",checkA,checkB,eA,eB,cand.found,cand2.found);
//       }
//       
//       //if ((10+cand.found)!=(cand2.found))
//       if ((cand.found+10)<(cand2.found))
//       {
// 	printf("%d %d\n",cand.found,cand2.found);
//       }
//   
//   
//     }
//   }
	  
	  if (collision_fun_p.colliding(particle_interaction_cost_new,tree,point,collision_search_rad,temp,&cand,!(collision_fun_p.is_soft())))
	  {
	      point.set_direction(old_dir);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
	      return false;
	  }
	  
	  
	  
	  
	
	}

        T new_cost=point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			       options.bifurcation_bonus_L,
			       inrange);
        if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
        {
            point.set_direction(old_dir);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
            return false;
        }
        

        T newenergy_data;
        if (!data_fun.eval_data(
                    newenergy_data,
		    point))
// 		    point.get_direction(),
//                     point.get_position(),
//                     point.get_scale()))
        {
            point.set_direction(old_dir);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
	    return false;
        }

        
        T E=(new_cost-old_cost)/temp;
        E+=(newenergy_data-oldenergy_data)/temp;
	
	if (collision_fun_p.is_soft())   
	{
	    E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	}

        T R=mhs_fast_math<T>::mexp(E);

        if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
        {
	  
/*printf("%f\n",new_cost-options.bifurcation_bonus_L);	*/  

            this->tracker->update_energy(E,static_cast<int>(get_type()));
            this->proposal_accepted++;

            point.point_cost=newenergy_data;

            /*
              if (return_statistic)
              {
                  std::size_t center=(std::floor(point.position[0])*shape[1]
            	+std::floor(point.position[1]))*shape[2]
            	+std::floor(point.position[2]);
                static_data[center*2*numproposals+11]+=1;
              }
              */
	    #ifdef _DEBUG_PARTICLE_HISTORY_
	      point.last_successful_proposal=PARTICLE_HISTORY::PARTICLE_HISTORY_ROTATE;
	    #endif
	    #ifdef  D_USE_GUI
// 	      (GuiPoints<T>* (&point))->touch(get_type());
	      point.touch(get_type());
	    #endif
	      
	     // point.bifurcation_center_neighbourhood_update();
            return true;
        }

        point.set_direction(old_dir);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
        return false;
    }
};



#endif


