#ifndef MY_OCTREE_H
#define MY_OCTREE_H

#include <list>
#include <string>
#include <cstddef>
#include <complex>
#include <cmath>
#include <sstream>
#include <cstddef>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <string>
#include <limits>

#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"


//#define GRID_DEBUG

#ifdef GRID_DEBUG 
    int grid_verbose = 0;
#endif

int verbose=0;
unsigned int pointid=1;
// const unsigned int mhs_maxpoints=10000000;

//#define _POINT_PROB_

template<typename T,int Dim> class OctTreeNode;
template<typename T,int Dim> class Cell;
template<typename T,int Dim> class volume_lock;
template<class C> class pool;
template<class C> class pool_element;


template<typename T,int Dim>
class OctPoints: public pool_element<class OctPoints<T,Dim> >
{
    friend class Cell<T,Dim>;
    friend class OctTreeNode<T,Dim>;
    friend pool<OctPoints>;

protected:

   
    unsigned int id;
    
    //static const unsigned int max_num_vox_grids=3;
    static const unsigned int max_num_vox_grids=4;

    //Voxgrid related attributes  
    struct Tvox_struct {
      Cell<T,Dim> * owner;
      unsigned int index_id;
      unsigned int cell_index_sub_id;
    };
    

    Tvox_struct vox_grid_data[max_num_vox_grids];

    virtual void clear()
    {
        #ifdef GRID_DEBUG   
            grid_x = -1;
        #endif
	pool_element<class OctPoints<T,Dim> >::clear();
	id=pointid++;
	for (int v=0;v<max_num_vox_grids;v++)
	{
	  if (vox_grid_data[v].owner!=NULL)
	  {
	      vox_grid_data[v].owner->removepoint(*this);
	      vox_grid_data[v].owner=NULL;
	  }
	  vox_grid_data[v].index_id=0;
	}
	//position=T(0);
	position=T(-1); //(out of grid)
    }
    
    
    Vector<T,Dim> position;
    
public:
  #ifdef GRID_DEBUG   
        int grid_x = -1;
  #endif
    virtual const Vector<T,Dim> & get_position()const {return position;} 
    virtual void set_position(const  Vector<T,Dim> & pos){position=pos;}
  
  template<typename B>
  bool out_of_bounds(B shape[])
  {
	      if ((position[0]<0)||
                (position[1]<0)||
                (position[2]<0)||
		  (!(position[0]<shape[0]))||
		  (!(position[1]<shape[1]))||
		  (!(position[2]<shape[2])))
		return true;  
	return false;        
    
  }
    
    
    OctPoints(const OctPoints& copy)
    {
      position=copy.position;
      
      id=copy.id;
      for (int v=0;v<max_num_vox_grids;v++)
      {
	vox_grid_data[v].index_id=0;
	vox_grid_data[v].owner=NULL;  // owner is NOT copied!!
      }
    }
    
  OctPoints& operator=( const OctPoints& rhs )
  {
      position=rhs.position;
      //index_id=0;  // owner is NOT copied!!
      id=rhs.id;
      //owner=NULL;  // owner is NOT copied!!
      return *this;
  }
  
  
    bool has_owner() const
    {
      for (int v=0;v<max_num_vox_grids;v++) 
      {
	if (vox_grid_data[v].owner!=NULL) 
	  return true;
      }
      return false;
    };
    
    bool has_owner(unsigned int grid_id) const
    {
      sta_assert_error(grid_id<max_num_vox_grids);
      return (vox_grid_data[grid_id].owner!=NULL);
    };
    
  //bool has_owner(){return (owner!=NULL);}

    unsigned int getid() {
        return id;
    };
    
    T * getposvec() {
        return position.v;
    };

    void print()
    {
            //printf("(%f %f %f %d %d)\n",position[0],position[1],position[2],(owner!=NULL), id);
      printf("(%f %f %f %d | ",position[0],position[1],position[2], id);
      for (int v=0;v<max_num_vox_grids;v++) 
      {
	  printf("%d ",(vox_grid_data[v].owner!=NULL));
//           if ((vox_grid_data[v].owner!=NULL))
//           {
//               vox_grid_data[v].owner->dump();
//           }
      }
      printf("\n");
      
      
    }

    bool operator >(const OctPoints & a) const
    {
        return (id>a.id);
    }
    bool operator <(const OctPoints & a) const
    {
        return (id<a.id);
    }

    OctPoints()
    {
      for (int v=0;v<max_num_vox_grids;v++)
        vox_grid_data[v].owner=NULL;
      
	clear();
//         setpoint(-1,-1,-1);
      position[0]=-1;
      position[1]=-1;
      position[2]=-1;
    }


//     void setpoint(const T point[])
//     {
// 	position=point;
//     }
// 
//     void setpoint(const Vector<T,Dim> & point)
//     {
// 	position=point;
//     }    
//     
//     
//     void setpoint(T x, T y, T z=0)
//     {
//         position[0]=x;
//         position[1]=y;
//         position[2]=z;
//     }

    void  clear_owner()
    {
      for (int v=0;v<max_num_vox_grids;v++)
        vox_grid_data[v].owner=NULL;
    }
    
    void unregister_from_grid(unsigned int grid_id=0)
    {
      sta_assert_error(grid_id<max_num_vox_grids);
      if (vox_grid_data[grid_id].owner!=NULL)
      {
	  if (verbose>1)
	      printf("unregistering %d\n",id);
	  vox_grid_data[grid_id].owner->removepoint(*this);
	  vox_grid_data[grid_id].owner=NULL;
      }
    }


    virtual ~OctPoints()
    {
      for (int v=0;v<max_num_vox_grids;v++)
      {
        if (vox_grid_data[v].owner!=NULL)
        {
            if (verbose>1)
                printf("unregistering %d\n",id);
            vox_grid_data[v].owner->removepoint(*this);
            vox_grid_data[v].owner=NULL;
        }
      }
    }
    
    bool update_pos(bool dry_run=false)
    {
     //#pragma omp critical (GRID_POINTER_UPDATE)
     try{
       
       bool has_at_least_one=false;
       for (int v=0;v<max_num_vox_grids;v++)
       {
       
	if (vox_grid_data[v].owner!=NULL)
	{
	  has_at_least_one=true;
	  OctTreeNode<T,Dim> * root=vox_grid_data[v].owner->parent;
// 	  int old_v=owner->num_pts;
//  	  printf(":-(  ");
	  
	  if (dry_run)
	  {
	    if (!root->insert(*this,false,true,false))
	    {
	      return false;
	    }
	    
	  }else
	  {
	  
	  
	    vox_grid_data[v].owner->removepoint(*this);
// 	    tTreeNode<T,Dim> * back_owner=vox_grid_data[v].owner;
//  	    printf(" . ");
	    if (!root->insert(*this))
	    {
	      
// 		sta_assert_error(back_owner->insert(*this));
// 		return false;
// 		print();
		mhs::STAError error;
		error<<"error updating point position / grid " <<v<<"(re-inserting point)\n";
		throw error;
 		//printf("error updating point position (re-insering point)\n");
	    }
	  }
// 	    sta_assert(root==owner->parent);
// 	    sta_assert(old_v==owner->num_pts);
//  	    printf(" ! ");
	/*      printf(":-)\n");	 */   
	  }
	 
       }
       
        sta_assert_error_c(has_at_least_one,printf("\nnum voxgrids: %d\n",max_num_vox_grids));
       return true;
       
    }  catch (mhs::STAError & error)
    {
        throw error;
    }
//     printf(" done\n");
    }
      
};






template<typename T,int Dim>
class OctTreeNode
{
  protected:
    friend class Cell<T,Dim> ;
    friend class OctPoints<T,Dim> ;
  
    Cell<T,Dim> *** grid;  
    Cell<T,Dim> **  grid_;
    Cell<T,Dim> *   grid__;
    
    class OctPoints<T,Dim> ** point_index;
//     bool * empty_indx;
    
    T cell_size;
    T points_per_voxel;
    std::size_t shape[3];
    std::size_t grid_shape[3];
    std::size_t num_cells;
    std::size_t num_voxel;
    
    unsigned int grid_id;
    
    bool delete_data;
    unsigned int  numpoints;    
     std::size_t out_of_capacity;
    
    std::size_t mhs_maxpoints;
    std::size_t capacity;
  
    bool remove( OctPoints<T,Dim>  & p)
    {
#ifdef _MCPU           
 	#pragma omp critical (TREE_POINTER_UPDATE)
#endif      
	{
	  numpoints--;
	  point_index[numpoints]->vox_grid_data[grid_id].index_id=p.vox_grid_data[grid_id].index_id;
	  point_index[p.vox_grid_data[grid_id].index_id]=point_index[numpoints]; 
	}
	return true;
    };
    
    void init()
    {
	capacity=std::ceil(points_per_voxel*cell_size*cell_size*cell_size);
	
	printf("particles/voxel: %f : per cell: %d\n",points_per_voxel,capacity);
	mhs::mex_dumpStringNOW();
	out_of_capacity=0;
	
	num_cells=1;
	for (int i=0;i<3;i++)
	{
	  grid_shape[i]=ceil(shape[i]/cell_size);
	  num_cells*=grid_shape[i];
	}
	
	//Cell< T,Dim >::set_max_capacity(capacity);
	
      printf("grid shape: %u %u %u\n",grid_shape[0],grid_shape[1],grid_shape[2]);	
      printf("num cells %u\n",num_cells);
      mhs::mex_dumpStringNOW();

	  grid=new Cell< T,Dim >** [grid_shape[0]];
	  grid_=new Cell< T,Dim >* [grid_shape[0]*grid_shape[1]];
	  grid__=new Cell< T,Dim >[grid_shape[0]*grid_shape[1]*grid_shape[2]];
	
printf("-");	  
	  for (std::size_t z=0;z<grid_shape[0];z++)
	    grid[z]=grid_+z*grid_shape[1];
printf("-");	  	  
	  for (std::size_t y=0;y<grid_shape[0]*grid_shape[1];y++)
	    grid_[y]=grid__+y*grid_shape[2];
printf("-");	  	  
	  for (std::size_t x=0;x<num_cells;x++)
      {
          #ifdef GRID_DEBUG  
            grid__[x].init(this,x);
          #else
            grid__[x].init(this);
          #endif
            
      }
printf("init particle pointers - ");	  	  
	  point_index=new class OctPoints<T,Dim> *[mhs_maxpoints];
printf("done\n");	  	  
    };
    
    void cleanup()
    {
	{
	  if (delete_data)
	  {
	    printf("cleaning up all points\n");
	   for (std::size_t i=0; i<numpoints; i++)
	   {
                    delete point_index[i];
	   }
 	    printf("done\n");	   
	  }
	  
	  printf("%d times running out of capacity\n",out_of_capacity );

	  if (grid__!=NULL)
	    delete [] grid__;
	  if (grid_!=NULL)
	    delete [] grid_;
	  if (grid!=NULL)
	    delete [] grid;	  
	  
	  if (point_index!=NULL)
	  {
	    delete [] point_index;
	  }
	}
    }
  public:    
    
    void print(){
      unsigned int min_pts=100000000;
      unsigned int max_pts=0;
      for (int z=0;z<grid_shape[0];z++)
	for (int y=0;y<grid_shape[1];y++)
	  for (int x=0;x<grid_shape[2];x++)
	  {
	    min_pts=std::min((unsigned int) (grid[z][y][x].num_pts),min_pts);
	    max_pts=std::max((unsigned int) (grid[z][y][x].num_pts),max_pts);
	  }
	printf("GRID: min num pts in grid %d\nGRID: max num pts in grid  %d\n",min_pts,max_pts)  ;
      
	printf("GRID: num points (total): %d \n",numpoints);
    };
    
    void get_pointlist(
      class OctPoints<T,Dim> ** & index,
      unsigned int & npoints)
    {
	index=point_index;
	npoints=numpoints; 
    }
    
    int get_numpts(){return numpoints;};

    
    OctPoints<T,Dim> * get_rand_point(
      class volume_lock<T,Dim> & vlock,
      T lock_radius,
      bool & empty)
    {
	empty=false;
	OctPoints<T,Dim> * point_ptr=NULL;
#ifdef _MCPU     	
	#pragma omp critical (TREE_POINTER_UPDATE)
#endif	
	{
	  
	  if (numpoints>0)
	  {
	    point_ptr=((point_index)[std::rand()%numpoints]);
#ifdef _MCPU
	    bool can_lock=vlock.lock(point_ptr->position,lock_radius);
	    if (!can_lock)
		point_ptr=NULL;
#endif	    
	  }else
	    empty=true;
	}
        return point_ptr;
    }
    
    OctPoints<T,Dim> * get_rand_point(bool & empty)
    {
	empty=false;
	OctPoints<T,Dim> * point_ptr=NULL;
#ifdef _MCPU     	
	#pragma omp critical (TREE_POINTER_UPDATE)
#endif	
	{
	  
	  if (numpoints>0)
	  {
	    point_ptr=((point_index)[std::rand()%numpoints]);
	  }else
	    empty=true;
	}
        return point_ptr;
    }
    
    // use only fo single threaded mode
    OctPoints<T,Dim> * get_rand_point()
    {
      #ifdef _MCPU     	
	sta_assert_error(1!=1);
      #endif	

	OctPoints<T,Dim> * point_ptr=NULL;
	if (numpoints>0)
	    point_ptr=((point_index)[std::rand()%numpoints]);
        return point_ptr;
    }    
    
    
    
    bool insert(OctPoints<T,Dim> & p,bool check=false,bool dry_run=false,bool check_owner=true,bool dump_on_error=false)
    {
	
	if ((check)&&(p.vox_grid_data[grid_id].owner!=NULL))
	  return true;
      
// 	if (p.vox_grid_data[grid_id].owner!=NULL)
// 	{
// 	 printf("%d %d ",check,(p.vox_grid_data[grid_id].owner!=NULL)); 
// 	}
	sta_assert_error((!check_owner)||(p.vox_grid_data[grid_id].owner==NULL));
      
	sta_assert_error(numpoints<mhs_maxpoints);
	std::size_t gridpos[3];
	gridpos[0]=std::floor(p.position.v[0]/cell_size);
	gridpos[1]=std::floor(p.position.v[1]/cell_size);
	gridpos[2]=std::floor(p.position.v[2]/cell_size);
	
	if (
	  (gridpos[0]<0)||
	  (gridpos[1]<0)||
	  (gridpos[2]<0)||
	  (gridpos[0]>grid_shape[0]-1)||
	  (gridpos[1]>grid_shape[1]-1)||
	  (gridpos[2]>grid_shape[2]-1)
	) 
	{
	  #pragma omp critical
	  {
	    printf("\n%d %d %d / %d %d %d\n",gridpos[0],gridpos[1],gridpos[2],
	    grid_shape[0],grid_shape[1],grid_shape[2]);
	    p.print();
	  }
	  //printf("???????????????????????????????\n");
	  //sta_assert(false);
	  mhs::STAError error;
	  error<<"point out of grid!";
	  throw error;
	  return false;
	}
	
	if (dry_run)
	{
	  if (!grid[gridpos[0]][gridpos[1]][gridpos[2]].has_capacity())
	  {
	    out_of_capacity++;
	    return false;
	  }
	  return true;
	}
	
	if (!grid[gridpos[0]][gridpos[1]][gridpos[2]].addpoint(p))
	{
            if (dump_on_error)
            {
                grid[gridpos[0]][gridpos[1]][gridpos[2]].dump();  
            }
	  out_of_capacity++;
	  return false;
	}
#ifdef _MCPU     	
	#pragma omp critical (TREE_POINTER_UPDATE)
#endif	
	{
	  p.vox_grid_data[grid_id].index_id=numpoints++;
// 	  empty_indx[p.index_id]=false;
	  point_index[p.vox_grid_data[grid_id].index_id]=&p;
	}
	

	return true;
    }
    
    bool insert(OctPoints<T,Dim> & p,Cell< T,Dim > * cell)
    {
      if (!cell->addpoint(p))
	  return false;
      {
	p.vox_grid_data[grid_id].index_id=numpoints++;
// 	  empty_indx[p.index_id]=false;
	point_index[p.vox_grid_data[grid_id].index_id]=&p;
      }
	return true;
    }
    
    
    
 void queryRange(
      const Vector<T,Dim> &  position,
      T radius,
      class OctPoints<T,Dim>**  & query_buffer,
      std::size_t buffer_size,
      std::size_t & found)
 {
	int gridpos_lower[3];
	int gridpos_upper[3];
	const T * pos=position.v;
	
	
	gridpos_lower[0]=std::floor((*pos-radius)/cell_size);
	if (gridpos_lower[0]<0) 
	  gridpos_lower[0]=0;
	
	gridpos_upper[0]=std::ceil((*pos+radius)/cell_size);
	if (gridpos_upper[0]>grid_shape[0]-1)
	  gridpos_upper[0]=grid_shape[0]-1;
	
	pos++;
	
	gridpos_lower[1]=std::floor((*pos-radius)/cell_size);
	if (gridpos_lower[1]<0) 
	  gridpos_lower[1]=0;
	
	gridpos_upper[1]=std::ceil((*pos+radius)/cell_size);
	if (gridpos_upper[1]>grid_shape[1]-1)
	  gridpos_upper[1]=grid_shape[1]-1;
	
	pos++;

	gridpos_lower[2]=std::floor((*pos-radius)/cell_size);
	if (gridpos_lower[2]<0) 
	  gridpos_lower[2]=0;
	
	gridpos_upper[2]=std::ceil((*pos+radius)/cell_size);
	if (gridpos_upper[2]>grid_shape[2]-1)
	  gridpos_upper[2]=grid_shape[2]-1;
	
	

	class OctPoints<T,Dim>**  query_buffer_ptr=query_buffer;
	found=0;
	radius*=radius;
	for (int z=gridpos_lower[0];z<=gridpos_upper[0];z++)
	  for (int y=gridpos_lower[1];y<=gridpos_upper[1];y++)
	    for (int x=gridpos_lower[2];x<=gridpos_upper[2];x++)
	    {
	      if (grid[z][y][x].num_pts>0)
	      //grid[z][y][x].grap_points(query_buffer_ptr,buffer_size,found,position,radius);
		grid[z][y][x].grap_pointsL2(query_buffer_ptr,buffer_size,found,position,radius);
	    }
	
 }
    
    void deletedata(bool k) {
        delete_data=k;
    }   
    
    
    bool oom(T cell_size)
    {
    	num_cells=1;
	for (int i=0;i<3;i++)
	{
	  num_cells*=ceil(shape[i]/cell_size);
	}
	
      //if (100000000>num_cells)
	if (1000000000>num_cells)
	  return true;
	
	
	return false;
    }
    
//     OctTreeNode(std::size_t shape[], T cell_size=10,unsigned int grid_id=0)
     OctTreeNode(std::size_t shape[], T cell_size, T points_per_voxel, unsigned int grid_id,std::size_t mhs_maxpoints)
    :grid(NULL),grid_(NULL),grid__(NULL),point_index(NULL),numpoints(0)
    {
      for (int i=0;i<3;i++)
      sta_assert_error(shape[i]>0);
      printf("voxgrid: [%d %d %d]\n",shape[0],shape[1],shape[2]);
      printf("cell size %f^3\n",cell_size);
      
      
	this->mhs_maxpoints=mhs_maxpoints;
	sta_assert_error(grid_id<(OctPoints< T, Dim >::max_num_vox_grids));
	this->grid_id=grid_id;
      

	this->shape[0]=shape[0];
	this->shape[1]=shape[1];
	this->shape[2]=shape[2];
	
	T new_cell_size=cell_size;
	while (!oom(new_cell_size))
	{
// 	  new_cell_size*=2;
	  new_cell_size*=1.1;
	}
	if (new_cell_size!=cell_size)
	{
	 cell_size=new_cell_size;
	 printf("image too big, increasing the cell size to %f^3\n",cell_size);
	}
	
	
	
	
	this->cell_size=cell_size;
	this->points_per_voxel=points_per_voxel;
	init();
	delete_data=false;
    }

    ~OctTreeNode()
    {
	cleanup(); 
    }
    
    OctTreeNode()
    {sta_assert_error(1==0)};
};





template<typename T,int Dim>
class Cell
{
  protected:
      #ifdef GRID_DEBUG    
        int x=-1;
        #endif
      
    friend class OctTreeNode<T,3>;
    friend class OctPoints<T,Dim> ;
    
    //typedef unsigned short T_cell_base_type;
    typedef unsigned int T_cell_base_type;
    
    //static T_cell_base_type max_capacity;
//     T_cell_base_type current_capacity;
    T_cell_base_type num_pts;
    unsigned int grid_id;
    OctPoints<T,Dim> ** points;
    OctTreeNode<T,Dim> * parent; 
    
    
//     static void set_max_capacity(unsigned int capacity)
//     {
//       sta_assert_error(capacity>0);
//       max_capacity=capacity;
//     }
    
    #ifdef GRID_DEBUG    
    void init(OctTreeNode<T,Dim> *  parent, int x)
    #else    
    void init(OctTreeNode<T,Dim> *  parent)
    #endif
    {
        sta_assert_error(parent->capacity>0);
        
    #ifdef GRID_DEBUG    
            sta_assert_error(x<2147480000);
            this->x = x;
    #endif
// 	current_capacity=0;
	//points=new OctPoints<T,Dim> *[capacity];
	points=NULL;//new OctPoints<T,Dim> *[capacity];
	this->parent=parent;
	this->grid_id=parent->grid_id;
    };
    
    bool has_capacity(){ return (num_pts<parent->capacity);}
    
//     void realloc(T_cell_base_type request)
//     {
//       sta_assert_error(request<=max_capacity);
//       T_cell_base_type new_capacity=std::min(std::max((T_cell_base_type)(current_capacity*2),request),max_capacity);
//       OctPoints<T,Dim> ** new_points=new OctPoints<T,Dim> *[new_capacity];
//       for (std::size_t i=0;i<current_capacity;i++)
//       {
// 	new_points[i]=points[i];
//       }
//       if (current_capacity>0)
//       {
// 	delete [] points;
//       }
//       points=new_points;
//       current_capacity=new_capacity;
//     }
    
//     bool addpoint(OctPoints<T,Dim>  & point)
//     {
//       if (points==NULL)
//       {
// 	//current_capacity=std::max(max_capacity/2,1);
// 	//current_capacity=std::min(max_capacity,(unsigned int) 1);
// 	current_capacity=1;
// 	points=new OctPoints<T,Dim> *[current_capacity];
//       }
//       if (num_pts<max_capacity)
//       {
// 	if (!(num_pts<current_capacity))
// 	{
// 	  realloc(num_pts+1);
// 	}
// 	point.vox_grid_data[grid_id].cell_index_sub_id=num_pts;
// 	points[num_pts++]=&point;
// 	point.vox_grid_data[grid_id].owner=this;
// 	return true;
//       }
//       return false;
//     }
    
    bool addpoint(OctPoints<T,Dim>  & point)
    {
      if (points==NULL)
      {
          sta_assert_error_c(parent->capacity<=std::numeric_limits<T_cell_base_type>::max(),printf("parent capacity (%d) > T_cell_base_type\n",parent->capacity));
	points=new OctPoints<T,Dim> *[parent->capacity];
      }
      if (num_pts<parent->capacity)
      {
         
          
          #ifdef GRID_DEBUG  
            if ((this->x==0)&&(grid_id==0)&&(grid_verbose>0))
            {
                #ifdef GRID_DEBUG
                    //p.grid_x = grid[gridpos[0]][gridpos[1]][gridpos[2]].x;
                    point.grid_x = this->x;
                #endif
                printf("################# ADD 1 point to %d points\n",num_pts);
            }
          #endif
          
	point.vox_grid_data[grid_id].cell_index_sub_id=num_pts;
	points[num_pts++]=&point;
	point.vox_grid_data[grid_id].owner=this;
// 	bool ok=true;
// 	  for (int i=0;i<num_pts;i++)
// 	  {
// 	    if (i!=points[i]->cell_index_sub_id)
// 	    ok=false;
// 	  }
// 	  if (!ok)
// 	for (int i=0;i<num_pts;i++)
// 	  {
// 	    printf ("%u %u",i,points[i]->cell_index_sub_id);
// 	  }
// 	
	return true;
      }
      //printf("running out of capacity (%d %d)!\n",num_pts,capacity);
      return false;
    }
    
    void removepoint(OctPoints<T,Dim>  & point)
    {
	sta_assert_error((points!=NULL));
    
    sta_assert_error((parent->capacity>0));
    
	{      
    #ifdef GRID_DEBUG  
    if ((this->x==0)&&(grid_id==0)&&(grid_verbose>0))
    {
        printf("################# DEL 1 point from %d pints\n",num_pts);
    }
	sta_assert_error_c(num_pts>0,printf("gird id %d, numpts %d , GRIDPOS: %d , PT GRIDPOS: %d  \n",grid_id,num_pts,this->x,point.grid_x));  
    #endif
	num_pts--;
	points[point.vox_grid_data[grid_id].cell_index_sub_id]=points[num_pts];
	points[point.vox_grid_data[grid_id].cell_index_sub_id]->vox_grid_data[grid_id].cell_index_sub_id=point.vox_grid_data[grid_id].cell_index_sub_id;
	parent->remove(point);
	point.vox_grid_data[grid_id].owner=NULL;
      }
    }
    
    void dump()
    {
        
        
        class OctPoints<T,Dim>** pts=points;
// 	const T * pt_data1;
// 	const T * pt_data2;
// 	T dist2;
// 	T tmp;
        Vector<T,3> min;
        Vector<T,3> max;
        min=std::numeric_limits<T>::max();
        max=-std::numeric_limits<T>::max();
 	for (int i=0;i<num_pts;i++,pts++)
 	{  
          Vector<T,3> v;
         v=   (*pts )->position;
          max.max(v); 
          min.min(v);
        }
        printf("num of pts in cell: %u\n",num_pts);
        min.print();
        max.print();
    }
    
    
    void grap_pointsL2(
      class OctPoints<T,Dim>**  & query_buffer,
      const std::size_t & buffer_size,
      std::size_t & found,
      const Vector<T,Dim> & pos,
      const T & radius2)
    {
	if (points==NULL)
	{
	 return; 
	}
      
	class OctPoints<T,Dim>** pts=points;
	const T * pt_data1;
	const T * pt_data2;
	T dist2;
	T tmp;
	for (int i=0;i<num_pts;i++,pts++)
	{
 	  pt_data1=pos.v;
 	  pt_data2=(*pts)->position.v;
 	  tmp=(*pt_data1++)-(*pt_data2++);
 	  dist2=tmp*tmp;
 	  if (dist2>radius2) {continue;}
 	  tmp=(*pt_data1++)-(*pt_data2++);
 	  dist2+=tmp*tmp;
 	  if (dist2>radius2) {continue;}
 	  tmp=(*pt_data1++)-(*pt_data2++);
 	  dist2+=tmp*tmp;
 	  if (dist2>radius2) {continue;}
	  {
	    (*query_buffer++)=*pts;
	    if (!(found<buffer_size))
	    {
	      printf("search buffer full!\n");
	      sta_assert_error(found<buffer_size);
	    }
	    found++;
	  }
// 	  pts++;
	}
    }
    
public:
  
  const OctPoints<T,Dim> ** get_points()
  {
    sta_assert_error((points!=NULL));
    return points;
  }
  
  std::size_t get_num_points()
  {
    return num_pts;
  }
  
  
  //Cell():points(NULL),num_pts(0),parent(NULL),grid_id(0),current_capacity(0){};
  Cell():points(NULL),num_pts(0),parent(NULL),grid_id(0){};
  ~Cell()
  {
    if (points!=NULL)
      delete [] points;
  };

  
};

//template<typename T,int Dim>
// T Points<T,Dim>::endpoint_nubble_scale=0;
//typename Cell<T,Dim>::T_cell_base_type Cell<T,Dim>::max_capacity=2;










#endif




