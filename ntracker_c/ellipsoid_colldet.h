#ifndef ELLIPSOIDE_COLL_DET_H
#define ELLIPSOIDE_COLL_DET_H



#include "mhs_graphics.h"
#include "mhs_lapack.h"



 

template<typename T>
bool ellipsoids_coolide(
    const Vector<T,3> &R1,
    const Vector<T,3> &R2,
    const Vector<T,3> &N1,
    const Vector<T,3> &N2,
    T t1, // main axis1 (particle thickness)
    T s1, // two seconday axes (particle scale)
    T t2,
    T s2,
    bool debug=false)
{
    T s1_sq=s1*s1;
    T t1_sq=t1*t1;


    // parameter matrix for first ellipsoid (aligned along [1,0,0])
    T a11 = 1/t1_sq;
    T a22 = 1/s1_sq;
    T & a33 = a22;

    const Vector<T,3> & n0=N1;
    Vector<T,3> n1;
    Vector<T,3> n2;
    mhs_graphics::createOrths(n0,n1,n2);
    Vector<T,3> delta=R1-R2;

    T t_1=delta.dot(n0);
    T t_2=delta.dot(n1);
    T t_3=delta.dot(n2);

    Vector<T,3> N3;
    N3[0]=N2.dot(n0);
    N3[1]=N2.dot(n1);
    N3[2]=N2.dot(n2);

    
    
#ifdef _ellipsoids_coolide_ver01_



    T * N2_=N3.v;
    T s2_sq=s2*s2;
    T t2_sq=t2*t2;

    // parameter matrix for second ellipsoid
    T b11=(N2_[0]*N2_[0])/t2_sq + (1-N2_[0]*N2_[0])/s2_sq;
    T b12=(N2_[0]*N2_[1])/t2_sq - (N2_[0]*N2_[1])/s2_sq;
    T b13=(N2_[0]*N2_[2])/t2_sq - (N2_[0]*N2_[2])/s2_sq;
    T b14=-(b11*t_1 + b12*t_2 + b13*t_3);

    T & b21=b12;
    T b22=(N2_[1]*N2_[1])/t2_sq + (1-N2_[1]*N2_[1])/s2_sq;
    T b23=(N2_[1]*N2_[2])/t2_sq - (N2_[1]*N2_[2])/s2_sq;
    T b24=-(b12*t_1 + b22*t_2 + b23*t_3);

    T & b31 = b13;
    T & b32 = b23;
    T b33=(N2_[2]*N2_[2])/t2_sq + (1-N2_[2]*N2_[2])/s2_sq;
    T b34 =-(b13*t_1 + b23*t_2 + b33*t_3);

    T & b41=b14;
    T & b42=b24;
    T & b43=b34;
    T b44 = -(b14*t_1 + b24*t_2 + b34*t_3 + 1);


    // characteristic polynom
    // T4 X^4 + T3 X^3 + T2 X^2 + T1 X +  T0;

    T T4 = (-a11*a22*a33);
    T T3 = (a11*a22*b33 + a11*a33*b22 + a22*a33*b11 - a11*a22*a33*b44);
    T T2 = (a11*b23*b32 - a11*b22*b33 - a22*b11*b33 + a22*b13*b31 -
            a33*b11*b22 + a33*b12*b21 + a11*a22*b33*b44 - a11*a22*b34*b43 +
            a11*a33*b22*b44 - a11*a33*b24*b42 + a22*a33*b11*b44 -
            a22*a33*b14*b41);
    T T1 = (b11*b22*b33 - b11*b23*b32 - b12*b21*b33 + b12*b23*b31 +
            b13*b21*b32 - b13*b22*b31 - a11*b22*b33*b44 + a11*b22*b34*b43 +
            a11*b23*b32*b44 - a11*b23*b34*b42 - a11*b24*b32*b43 +
            a11*b24*b33*b42 - a22*b11*b33*b44 + a22*b11*b34*b43 +
            a22*b13*b31*b44 - a22*b13*b34*b41 - a22*b14*b31*b43 +
            a22*b14*b33*b41 - a33*b11*b22*b44 + a33*b11*b24*b42 +
            a33*b12*b21*b44 - a33*b12*b24*b41 - a33*b14*b21*b42 +
            a33*b14*b22*b41);
    T T0 = (b11*b22*b33*b44 - b11*b22*b34*b43 - b11*b23*b32*b44 +
            b11*b23*b34*b42 + b11*b24*b32*b43 - b11*b24*b33*b42 -
            b12*b21*b33*b44 + b12*b21*b34*b43 + b12*b23*b31*b44 -
            b12*b23*b34*b41 - b12*b24*b31*b43 + b12*b24*b33*b41 +
            b13*b21*b32*b44 - b13*b21*b34*b42 - b13*b22*b31*b44 +
            b13*b22*b34*b41 + b13*b24*b31*b42 - b13*b24*b32*b41 -
            b14*b21*b32*b43 + b14*b21*b33*b42 + b14*b22*b31*b43 -
            b14*b22*b33*b41 - b14*b23*b31*b42 + b14*b23*b32*b41);
#else


    Vector<T,2> pn1;
    pn1[0]=N3[1];
    pn1[1]=N3[2];
    T npn1=std::sqrt(pn1.norm2());
    T tmp_t_2=(t_2*pn1[0]+t_3*pn1[1])/npn1;
    t_3=(t_2*pn1[1]-t_3*pn1[0])/npn1;
    t_2=tmp_t_2;
    N3[1]=npn1;



    T * N2_=N3.v;
    T s2_sq=s2*s2;
    T t2_sq=t2*t2;

    // parameter matrix for second ellipsoid  (aligned to the plane [1,0,0],[0,1,0])
    T b11=(N2_[0]*N2_[0])/t2_sq + (1-N2_[0]*N2_[0])/s2_sq;
    T b12=(N2_[0]*N2_[1])/t2_sq - (N2_[0]*N2_[1])/s2_sq;
    T b14=-(b11*t_1 + b12*t_2);

    T & b21=b12;
    T b22=(N2_[1]*N2_[1])/t2_sq + (1-N2_[1]*N2_[1])/s2_sq;
    T b24=-(b12*t_1 + b22*t_2);

    T b33=1/s2_sq;
    T b34 =-b33*t_3;

    T & b41=b14;
    T & b42=b24;
    T & b43=b34;
    T b44 = -(b14*t_1 + b24*t_2 + b34*t_3 + 1);


    // characteristic polynom
    // T4 X^4 + T3 X^3 + T2 X^2 + T1 X +  T0;


    T T4 = (-a11*a22*a33);
    T T3 = (a11*a22*b33 + a11*a33*b22 + a22*a33*b11 - a11*a22*a33*b44);

    T T2=
        a33* b12* b21 - a33* b11* b22 - a22* b11* b33 - a11* b22* b33 -
        a22* a33* b14* b41 - a11* a33* b24* b42 - a11* a22* b34* b43 +
        a22* a33* b11* b44 + a11* a33* b22* b44 + a11* a22* b33* b44;

    T T1=
        -b12* b21* b33 + b11* b22* b33 + a33* b14* b22* b41 - a33* b12* b24* b41 +
        a22* b14* b33* b41 - a33* b14* b21* b42 + a33* b11* b24* b42 +
        a11* b24* b33* b42 + a22* b11* b34* b43 + a11* b22* b34* b43 +
        a33* b12* b21* b44 - a33* b11* b22* b44 - a22* b11* b33* b44 - a11* b22* b33* b44;

    T T0=
        -b14* b22* b33* b41 + b12* b24* b33* b41 + b14* b21* b33* b42 -
        b11* b24* b33* b42 + b12* b21* b34* b43 - b11* b22* b34* b43 -
        b12* b21* b33* b44 + b11* b22* b33* b44;
	

	
	
	
#endif

    if (debug)
        printf("T:\n %.100f\n %.100f\n %.100f\n %.100f\n %.100f\n\n",T0,T1,T2,T3,T4);

    T abcd[5];
    abcd[0]=1;
    abcd[1]=T3/T4;
    abcd[2]=T2/T4;
    abcd[3]=T1/T4;
    abcd[4]=T0/T4;

    

    
    
    

    T v=0;
    for (int i=0; i<4; i++)
        v+=(abcd[i]*abcd[i+1]<0) ? 1 : 0;

    // 1)
    // test from
    // "An algebraic approach to continuous collision detection for ellipsoids"
    // Jia, Xiaohong and Choi, Yi-King and Mourrain, Bernard and Wang, Wenping
    // 2011
    //

    if (v!=2) // 1)
        return true;

    T & a= abcd[1];
    T & b= abcd[2];
    T & c= abcd[3];
    T & d= abcd[4];
    
    T aa=a*a;


    // 2) 3)
    // two tests from
    // "Solving the separation problem for two ellipsoids involving only the evaluation of six polynomials"
    // Gonzalez-Vega, Laureano and Mainar, Esmeralda
    // 2008
    //
    if (!((3*aa - 8*b)>0)) // 2)
        return true;

    T bb=b*b;
    T bbb=bb*b;
    T aaa=aa*a;
    

    if (!(-4*bbb+aa*bb+16*b*d+14*c*a*b-6*aa*d-3*c*aaa-18*c*c>0) ) // 3)
        return true;

    if (debug)
        printf("%f  %f %f %f\n",abcd[1],abcd[2],abcd[3],abcd[4]);

    double wr[4];
    double wi[4];
    rootsP4(T4,T3,T2,T1,T0,  wr, wi);
    
    //const double myEps=std::numeric_limits<T>::epsilon();
    const double myEps=1e-10;
    int N=0;
    for (int n=0; n<4; n++)
    {
        N+=((std::abs(wi[n])<myEps)? 1 : 0 )*((wr[n]<0)? 1 : 0 );
    }
    

    if (N!=2)
        return true;

    return false;

}


/*
template<typename T>
bool ellipsoids_coolide(
    const Vector<T,3> &R1,
    const Vector<T,3> &R2,
    const Vector<T,3> &N1,
    const Vector<T,3> &N2,
    T t1, // main axis1 (particle thickness)
    T s1, // two seconday axes (particle scale)
    T t2,
    T s2,
    T psf,
    bool debug=false)
{
    T s1_sq=s1*s1;
    T t1_sq=t1*t1;


    // parameter matrix for first ellipsoid (aligned along [1,0,0])
    T a11 = 1/t1_sq;
    T a22 = 1/s1_sq;
    T & a33 = a22;

    const Vector<T,3> & n0=N1;
    Vector<T,3> n1;
    Vector<T,3> n2;
    mhs_graphics::createOrths(n0,n1,n2);
    
    
    Vector<T,3> dir_depth(1,0,0);
    
    T n_d1=dir_depth.dot(n1);
    T n_d2=dir_depth.dot(n2);
    
    
    T weight_z=0;
    if ((std::abs(n_d1)+std::abs(n_d2))>0.001)
    {
      n1=n1*n_d1+n2*n_d2;
      weight_z=std::sqrt(n1.norm2());
      n1/=weight_z+std::numeric_limits< T >::epsilon();
      n2=n0.cross(n1);
      T tmp=std::sqrt((weight_z)*psf+(1-weight_z));
      printf("::::: %f\n",tmp);
      n1*=tmp;
    }
     
    
     
    
    Vector<T,3> delta=R1-R2;

    T t_1=delta.dot(n0);
    T t_2=delta.dot(n1);
    T t_3=delta.dot(n2);

    Vector<T,3> N3;
    N3[0]=N2.dot(n0);
    N3[1]=N2.dot(n1);
    N3[2]=N2.dot(n2);

    
    
#ifdef _ellipsoids_coolide_ver01_



    T * N2_=N3.v;
    T s2_sq=s2*s2;
    T t2_sq=t2*t2;

    // parameter matrix for second ellipsoid
    T b11=(N2_[0]*N2_[0])/t2_sq + (1-N2_[0]*N2_[0])/s2_sq;
    T b12=(N2_[0]*N2_[1])/t2_sq - (N2_[0]*N2_[1])/s2_sq;
    T b13=(N2_[0]*N2_[2])/t2_sq - (N2_[0]*N2_[2])/s2_sq;
    T b14=-(b11*t_1 + b12*t_2 + b13*t_3);

    T & b21=b12;
    T b22=(N2_[1]*N2_[1])/t2_sq + (1-N2_[1]*N2_[1])/s2_sq;
    T b23=(N2_[1]*N2_[2])/t2_sq - (N2_[1]*N2_[2])/s2_sq;
    T b24=-(b12*t_1 + b22*t_2 + b23*t_3);

    T & b31 = b13;
    T & b32 = b23;
    T b33=(N2_[2]*N2_[2])/t2_sq + (1-N2_[2]*N2_[2])/s2_sq;
    T b34 =-(b13*t_1 + b23*t_2 + b33*t_3);

    T & b41=b14;
    T & b42=b24;
    T & b43=b34;
    T b44 = -(b14*t_1 + b24*t_2 + b34*t_3 + 1);


    // characteristic polynom
    // T4 X^4 + T3 X^3 + T2 X^2 + T1 X +  T0;

    T T4 = (-a11*a22*a33);
    T T3 = (a11*a22*b33 + a11*a33*b22 + a22*a33*b11 - a11*a22*a33*b44);
    T T2 = (a11*b23*b32 - a11*b22*b33 - a22*b11*b33 + a22*b13*b31 -
            a33*b11*b22 + a33*b12*b21 + a11*a22*b33*b44 - a11*a22*b34*b43 +
            a11*a33*b22*b44 - a11*a33*b24*b42 + a22*a33*b11*b44 -
            a22*a33*b14*b41);
    T T1 = (b11*b22*b33 - b11*b23*b32 - b12*b21*b33 + b12*b23*b31 +
            b13*b21*b32 - b13*b22*b31 - a11*b22*b33*b44 + a11*b22*b34*b43 +
            a11*b23*b32*b44 - a11*b23*b34*b42 - a11*b24*b32*b43 +
            a11*b24*b33*b42 - a22*b11*b33*b44 + a22*b11*b34*b43 +
            a22*b13*b31*b44 - a22*b13*b34*b41 - a22*b14*b31*b43 +
            a22*b14*b33*b41 - a33*b11*b22*b44 + a33*b11*b24*b42 +
            a33*b12*b21*b44 - a33*b12*b24*b41 - a33*b14*b21*b42 +
            a33*b14*b22*b41);
    T T0 = (b11*b22*b33*b44 - b11*b22*b34*b43 - b11*b23*b32*b44 +
            b11*b23*b34*b42 + b11*b24*b32*b43 - b11*b24*b33*b42 -
            b12*b21*b33*b44 + b12*b21*b34*b43 + b12*b23*b31*b44 -
            b12*b23*b34*b41 - b12*b24*b31*b43 + b12*b24*b33*b41 +
            b13*b21*b32*b44 - b13*b21*b34*b42 - b13*b22*b31*b44 +
            b13*b22*b34*b41 + b13*b24*b31*b42 - b13*b24*b32*b41 -
            b14*b21*b32*b43 + b14*b21*b33*b42 + b14*b22*b31*b43 -
            b14*b22*b33*b41 - b14*b23*b31*b42 + b14*b23*b32*b41);
#else


    Vector<T,2> pn1;
    pn1[0]=N3[1];
    pn1[1]=N3[2];
    T npn1=std::sqrt(pn1.norm2());
    T tmp_t_2=(t_2*pn1[0]+t_3*pn1[1])/npn1;
    t_3=(t_2*pn1[1]-t_3*pn1[0])/npn1;
    t_2=tmp_t_2;
    N3[1]=npn1;



    T * N2_=N3.v;
    T s2_sq=s2*s2;
    T t2_sq=t2*t2;

    // parameter matrix for second ellipsoid  (aligned to the plane [1,0,0],[0,1,0])
    T b11=(N2_[0]*N2_[0])/t2_sq + (1-N2_[0]*N2_[0])/s2_sq;
    T b12=(N2_[0]*N2_[1])/t2_sq - (N2_[0]*N2_[1])/s2_sq;
    T b14=-(b11*t_1 + b12*t_2);

    T & b21=b12;
    T b22=(N2_[1]*N2_[1])/t2_sq + (1-N2_[1]*N2_[1])/s2_sq;
    T b24=-(b12*t_1 + b22*t_2);

    T b33=1/s2_sq;
    T b34 =-b33*t_3;

    T & b41=b14;
    T & b42=b24;
    T & b43=b34;
    T b44 = -(b14*t_1 + b24*t_2 + b34*t_3 + 1);


    // characteristic polynom
    // T4 X^4 + T3 X^3 + T2 X^2 + T1 X +  T0;


    T T4 = (-a11*a22*a33);
    T T3 = (a11*a22*b33 + a11*a33*b22 + a22*a33*b11 - a11*a22*a33*b44);

    T T2=
        a33* b12* b21 - a33* b11* b22 - a22* b11* b33 - a11* b22* b33 -
        a22* a33* b14* b41 - a11* a33* b24* b42 - a11* a22* b34* b43 +
        a22* a33* b11* b44 + a11* a33* b22* b44 + a11* a22* b33* b44;

    T T1=
        -b12* b21* b33 + b11* b22* b33 + a33* b14* b22* b41 - a33* b12* b24* b41 +
        a22* b14* b33* b41 - a33* b14* b21* b42 + a33* b11* b24* b42 +
        a11* b24* b33* b42 + a22* b11* b34* b43 + a11* b22* b34* b43 +
        a33* b12* b21* b44 - a33* b11* b22* b44 - a22* b11* b33* b44 - a11* b22* b33* b44;

    T T0=
        -b14* b22* b33* b41 + b12* b24* b33* b41 + b14* b21* b33* b42 -
        b11* b24* b33* b42 + b12* b21* b34* b43 - b11* b22* b34* b43 -
        b12* b21* b33* b44 + b11* b22* b33* b44;
	

	
	
	
#endif

    if (debug)
        printf("T:\n %.100f\n %.100f\n %.100f\n %.100f\n %.100f\n\n",T0,T1,T2,T3,T4);

    T abcd[5];
    abcd[0]=1;
    abcd[1]=T3/T4;
    abcd[2]=T2/T4;
    abcd[3]=T1/T4;
    abcd[4]=T0/T4;

    

    
    
    

    T v=0;
    for (int i=0; i<4; i++)
        v+=(abcd[i]*abcd[i+1]<0) ? 1 : 0;

    // 1)
    // test from
    // "An algebraic approach to continuous collision detection for ellipsoids"
    // Jia, Xiaohong and Choi, Yi-King and Mourrain, Bernard and Wang, Wenping
    // 2011
    //

    if (v!=2) // 1)
        return true;

    T & a= abcd[1];
    T & b= abcd[2];
    T & c= abcd[3];
    T & d= abcd[4];
    
    T aa=a*a;


    // 2) 3)
    // two tests from
    // "Solving the separation problem for two ellipsoids involving only the evaluation of six polynomials"
    // Gonzalez-Vega, Laureano and Mainar, Esmeralda
    // 2008
    //
    if (!((3*aa - 8*b)>0)) // 2)
        return true;

    T bb=b*b;
    T bbb=bb*b;
    T aaa=aa*a;
    

    if (!(-4*bbb+aa*bb+16*b*d+14*c*a*b-6*aa*d-3*c*aaa-18*c*c>0) ) // 3)
        return true;

    if (debug)
        printf("%f  %f %f %f\n",abcd[1],abcd[2],abcd[3],abcd[4]);

    double wr[4];
    double wi[4];
    rootsP4(T4,T3,T2,T1,T0,  wr, wi);
    
    //const double myEps=std::numeric_limits<T>::epsilon();
    const double myEps=1e-10;
    int N=0;
    for (int n=0; n<4; n++)
    {
        N+=((std::abs(wi[n])<myEps)? 1 : 0 )*((wr[n]<0)? 1 : 0 );
    }
    

    if (N!=2)
        return true;

    return false;

}*/


       



#endif
