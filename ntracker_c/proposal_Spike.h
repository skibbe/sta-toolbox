#ifndef PROPOSAL_SPIKE_H
#define PROPOSAL_SPIKE_H

#include "proposals.h"
#include "mhs_data_spikes.h"
#include <unistd.h>
template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CSpike : public CProposal<TData,T,Dim>
{
protected:
std::size_t count;  
double time = -1;
double prev_time = -1;
double last_spike_time;
double delta_time;

bool update = true;
double anicooldown = 0.9;

       const double * spikes;
    std::size_t num_spikes;
    
    double delay = 1;
    float pscale = 2;
    mhs::CtimeStopper timer;
public:
  
       T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_particle_update(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  
    CSpike(): CProposal<TData,T,Dim>()
    {
      count = 0;
      time = -1;
      prev_time = -1;
        spikes = NULL;
        num_spikes = 0;
        delay = 1;
        last_spike_time=timer.get_total_runtime();
    update = true;
    anicooldown = 0.9;
    delta_time = 0;
    }
    

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_SPIKE);
    };
    
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_SPIKE);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        if (params==NULL)
            return;
        
        try {
            mhs::dataArray<double> darray((mhs::mex_getParamPtr( params,"spikes")));
            spikes=darray.data;
            num_spikes = darray.get_num_elements() / 2;
            
            
            if (mhs::mex_hasParam(params,"anicooldown")!=-1)
                anicooldown  = mhs::mex_getParam<double>(params,"anicooldown",1)[0];
            
            if (mhs::mex_hasParam(params,"delay")!=-1)
                delay  = mhs::mex_getParam<double>(params,"delay",1)[0];
        
            if (mhs::mex_hasParam(params,"pscale")!=-1)
                pscale  = mhs::mex_getParam<float>(params,"pscale",1)[0];  
            //printf("number of spikes: %d\n",num_spikes);
        } catch ( mhs::STAError error ) {
            throw error;
        }
	
    }


    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

        try {

        } catch (mhs::STAError error)
        {
            throw error;
        }

    }

    bool propose()
    {
         //printf("sssds\n");
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);
// 	
 	   this->proposal_called++;
       
       
         const double * get_spikes =spikes;
         const double * get_spikes_times = spikes+ num_spikes;
         
         
         //double current_time=timer.get_total_runtime();
         
        const class Points<T,Dim> ** particle_ptr=this->tracker->particle_pool->getMem();
        std::size_t n_points=this->tracker->particle_pool->get_numpts();
                 
         
       double currrent_time=timer.get_total_runtime();
       
       
//         double t=timer.get_total_runtime()-currrent_time;
        
        
        double delta = currrent_time-last_spike_time;
        double delta_threshold =delay*delta_time;
        printf("detla d :%f %f ? \n",delta , delta_threshold);  
        if (delta  > delta_threshold )
        {
            if ((delta_time>0)&&(delta_time<1.1))
            {
                printf("detla t :%f\n",delta_time);     
            }else
            {
                printf("detla t out of range :%f\n",delta_time);
            }
            update = true; 
        }
        
        
//             if ((delta_time>0)&&(delta_time<1.1))
//             {
//                     usleep((std::size_t)(delay*delta_time));    
//             }else
//             {
//                 printf("detla t :%f\n",delta_time);
//             }

        if (update)
        {
            last_spike_time=timer.get_total_runtime();
            
            if ((time<0)||(count>=num_spikes))
            {
                count = 0;
                time = *(get_spikes_times+count);
                prev_time = time;
            }else
            {
                prev_time = time;
                time = *(get_spikes_times+count);
            }
            delta_time = (time-prev_time);
            
        
            std::size_t range = count;
            bool ok = true;
            while (ok)
            {
                double new_time = *(get_spikes_times+range);
                
                if ((new_time>time)||((range >= num_spikes)))
                {
                    //range-=1;
                    ok = false;
                    //break;
                }else
                {
                    range++;
                }
            }
            
            

            
            for (std::size_t i = time;i<range;i++)
            {
                
                //std::size_t indx = (std::size_t)((*(get_spikes+i))-1);
                std::size_t indx = (std::size_t) (n_points - (*(get_spikes+i)));
                
                
                if (indx<n_points)
                {
                    #ifdef  D_USE_GUI
                    Points<T,Dim> * point_p= ( ( Points<T,Dim>* ) particle_ptr[indx] );
                    {
                        point_p->set_scale(point_p->point_cost*pscale);
                        point_p->touch(PROPOSAL_TYPES::PROPOSAL_MOVE);
                    }
                    
                    #endif
                }
            }
            count = range;
        }
       
       update = false;
       
        
        for (std::size_t i = 0;i<n_points;i++)
        {
            Points<T,Dim> * point_p= ( ( Points<T,Dim>* ) particle_ptr[i]);
            T scale = std::max(point_p->get_scale()*anicooldown,0.0001);
            point_p->set_scale(scale );
        }

        
        
//        printf("bla : %d\n",num_spikes);

          this->proposal_accepted++;
        return true;
    }
};



#endif


