#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"

#define EPSILON 0.00000000000001

#define SUB2IND(X, Y, Z, shape)  (((Z)*(shape[1])+(Y)) *(shape[2])+(X)) 
#include "mhs_vector.h"

 //mex mhs_simulate_dmri.cc -lgomp CXXFLAGS=" -O3   -Wfatal-errors  -std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"

template<typename D,typename T,typename TData>
bool data_interpolate (
    const Vector<T,3> & pos,
    D & value,
    const TData  *  img,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode=1)
{

    
switch (mode)
{
   case 0:
    {
	int Z=std::floor ( pos[0]+T(0.5) );
	int Y=std::floor ( pos[1]+T(0.5) );
	int X=std::floor ( pos[2]+T(0.5) );

	if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}
	value=img[SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx];
	
    }break;
  case 1:
    {
      
   	T iz=pos[0];
	T iy=pos[1];
	T ix=pos[2];
	
	int Z=std::floor ( iz );
	int Y=std::floor ( iy );
	int X=std::floor ( ix );

	if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}

	D wz= ( iz-Z );
	D wy= ( iy-Y );
	D wx= ( ix-X );
	D & g=value;
	g=0;
        g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
    }break;
}

    return true;
}

/*
int ndirs = 64;
float dirs [] = {0.957082,0.901696,0.248801, 
0.542149,-0.679332,-0.878034, 
0.066274,-0.434525,0.064405, 
-0.078518,-0.916714,0.539551, 
-0.722351,-0.193440,0.096481, 
0.519871,-0.777143,-0.003657, 
-0.076887,0.713000,0.595818, 
0.653077,-0.289975,-0.526906, 
-0.740479,0.378118,0.251431, 
-0.224171,-0.472579,-0.251896, 
0.821934,-0.235974,-0.880742, 
-0.811401,0.832012,-0.950932, 
-0.747267,-0.923155,0.591710, 
0.450482,-0.553914,0.694784, 
-0.299979,-0.414526,0.979359, 
-0.118878,0.631821,0.073482, 
0.793682,-0.408694,-0.275679, 
0.338728,0.384929,0.529935, 
0.992022,0.180622,-0.130146, 
0.391437,-0.593921,-0.234747, 
0.834976,-0.402984,0.056797, 
0.681161,-0.077835,0.431846, 
0.854823,0.763682,0.176035, 
0.431082,-0.856087,-0.894811, 
-0.211115,-0.892810,-0.247955, 
-0.457508,-0.688112,0.371088, 
0.473206,-0.848395,-0.523725, 
0.993804,0.078281,0.334446, 
0.145223,-0.165586,-0.943219, 
0.805488,0.668981,-0.202863, 
0.090437,0.890058,0.396671, 
-0.674815,0.049481,-0.667079, 
0.445109,0.303220,-0.316273, 
0.081242,-0.341989,-0.254296, 
0.601777,0.432141,-0.115405, 
-0.660039,0.950378,-0.391179, 
-0.202000,-0.383606,-0.539819, 
-0.724689,0.543312,0.817651, 
-0.204773,-0.669432,0.809637, 
0.596623,0.125660,-0.957243, 
-0.972716,-0.705797,-0.779467, 
0.491778,0.039863,0.089266, 
-0.639883,-0.591230,-0.279171, 
0.021290,0.455386,0.350521, 
0.712405,-0.207904,-0.512565, 
-0.102479,0.975337,0.443538, 
-0.313296,0.706804,-0.068639, 
0.908226,-0.875653,-0.099803, 
-0.348943,-0.111083,0.993962, 
0.616260,-0.789881,0.738967, 
0.162026,-0.271216,-0.064470, 
0.903257,0.963641,-0.396917, 
0.786970,0.693667,0.567430, 
-0.706627,0.161773,-0.499685, 
-0.455771,-0.298544,0.569768, 
0.288301,-0.536417,-0.781230, 
-0.824537,-0.285697,-0.082428, 
-0.821673,0.007209,-0.915813, 
-0.556236,0.685147,-0.273644, 
0.405482,0.939185,-0.661154, 
-0.443078,0.602669,0.010139, 
-0.225967,-0.192058,0.590447, 
0.199221,-0.838480,-0.548841, 
0.910843,-0.766370,0.431818};
*/

int ndirs = 30;
float dirs [] = {0.231033,-0.253819,-0.118215, 
0.336252,0.243317,-0.186844, 
-0.527216,-0.463819,0.013046, 
0.707691,0.659146,0.102148, 
-0.559318,-0.725951,-0.837022, 
0.066256,0.506532,0.943509, 
0.854669,0.526385,-0.299857, 
-0.729483,-0.938122,-0.785015, 
-0.393033,0.462813,0.840095, 
0.984120,0.568774,0.129253, 
0.044775,0.180376,-0.313659, 
-0.379082,0.461612,0.588051, 
-0.168503,-0.591655,-0.712591, 
-0.219655,0.221436,0.844197, 
0.599952,0.215301,-0.232879, 
-0.942950,-0.668558,-0.048353, 
0.425959,0.656409,0.907974, 
0.663391,0.263361,-0.581423, 
-0.861877,-0.874533,-0.484393, 
0.160544,0.812348,0.987494, 
0.971915,0.950284,0.942148, 
0.862109,0.853060,0.786947, 
0.832857,0.659406,0.701458, 
0.671509,0.718674,0.526210, 
0.572033,0.653177,0.495138, 
0.326276,0.544477,0.327800, 
0.296816,0.540413,0.292692, 
0.166634,0.224872,0.213772, 
0.320457,0.144897,0.244137, 
-0.075723,0.128791,0.090278};

//#include "mhs_lapack.h"

//mex CXXFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "  CFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "   sta_EVGSL.cc -l gsl -l gslcblas -l gomp


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

  
    Vector<std::size_t,3>  target_shape;
    target_shape=std::size_t(0);
    
    Vector<std::size_t,3>  skip;
    skip=std::size_t(1);
    
    int slice = -1;
      
    const mxArray *img;
    img = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(img);
    const int *dims = mxGetDimensions(img);
    
    const mxArray * params=prhs[nrhs-1];
     if ( mxIsCell ( params  )  )
	  {
	      if ( mhs::mex_hasParam ( params,"shape" ) !=-1 )
		      {
		      target_shape=mhs::mex_getParam<T> ( params,"shape",3 ) ;
		      }
		      else return;
        if ( mhs::mex_hasParam ( params,"skip" ) !=-1 )
		      {
		      skip=mhs::mex_getParam<T> ( params,"skip",3 ) ;
		      }
                  if ( mhs::mex_hasParam ( params,"slice" ) !=-1 )
		      {
		      slice=mhs::mex_getParam<int> ( params,"slice")[0] ;
		      }      
      }

    
    if (numdim==3)
    {   
        {
                    T *p_img = (T*) mxGetData(img);
                    std::size_t shape[3];  
                    shape[0] = dims[2];
                    shape[1] = dims[1];
                    shape[2] = dims[0];    
                    std::size_t numvoxel=shape[0]*shape[1]*shape[2];
                    
                    int ndims[4];
                    ndims[0]=ndirs;
                    //ndims[0]=1;
                    ndims[1]=target_shape[0];
                    ndims[2]=target_shape[1];
                    ndims[3]=target_shape[2];
                    plhs[0] = mxCreateNumericArray(4,ndims,mxGetClassID(img),mxREAL);
                    T *result = (T*) mxGetData(plhs[0]);
                    
                    
                    int ndims2[3];
                    ndims2[0]=target_shape[0];
                    ndims2[1]=target_shape[1];
                    ndims2[2]=target_shape[2];
                    plhs[1] = mxCreateNumericArray(3,ndims2,mxGetClassID(img),mxREAL);
                    T *result2 = (T*) mxGetData(plhs[1]);
                    
                    
                    
                    std::swap(target_shape[0],target_shape[2]);
                    
                    std::size_t numvoxel_tgt=target_shape[0]*target_shape[1]*target_shape[2];
                    
                    float scale_x = float(target_shape[2])/float(shape[2]);
                    float scale_y = float(target_shape[1])/float(shape[1]);
                    float scale_z = float(target_shape[0])/float(shape[0]);
                    
                    
                    printf("%d %d %d\n",shape[0],shape[1],shape[2]);
                    printf("%d %d %d\n",target_shape[0],target_shape[1],target_shape[2]);
                    
                    //for (std::size_t idx= 0; idx < numvoxel; idx++)
                    #pragma omp parallel for num_threads(omp_get_num_procs())
                    for (std::size_t Y= 0; Y < shape[1]; Y+=skip[1])    
                    {
                        for (std::size_t Z= 0; Z < shape[0]; Z+=skip[2])    
                        for (std::size_t X= 0; X < shape[2]; X+=skip[0])    
                        {
                            float d = 1.5;
                            std::size_t indx_src = SUB2IND(X, Y, Z, shape); 
                            std::size_t Xt = std::round(X * scale_x);
                            std::size_t Yt = std::round(Y * scale_y);
                            std::size_t Zt = std::round(Z * scale_z);
                            std::size_t indx_trget = SUB2IND(Xt, Yt, Zt, target_shape); 
                            
                            //std::size_t indx_src = SUB2IND(X, Y, Z, shape); 
                            float center =  p_img[indx_src];
                            
                            for (int nd=0;nd<ndirs;nd++)
                            {
                                float * dirs_ = dirs + 3*nd;
                                //Vector d(dirs_[0],dirs_[1],dirs_[2]);
                                for (int t=1;t<3;t++)
                                {

                                    for (int s=0;s<2;s++)
                                    {   
                                        float offset = (d*t) * (-1+s*2);
                                        Vector<T,3> pos(Z+dirs_[2]*offset,Y+dirs_[1]*offset,X+dirs_[0]*offset);
                                        float value;
                                        
                                        if (data_interpolate (pos,
                                                value,
                                                p_img,
                                                shape,1,0,1))
                                            {
                                                if (indx_trget<numvoxel_tgt)
                                                {
                                                    result[indx_trget*ndirs + nd]+= (value-center);
                                                    result2[indx_trget]+=1;
                                                }
                                                
                                            }
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    #pragma omp parallel for num_threads(omp_get_num_procs())
                    for (std::size_t idx= 0; idx < numvoxel_tgt; idx++)
                    {
                        if (result2[idx]>0)
                            result[idx]/=result2[idx];
                        
                    }
        }
        
    }
        
        
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}
