#include "mex.h"

#include <unistd.h>
#include <complex>
#include <map>
#include "mhs_error.h"
#include "mhs_vector.h"
#include <algorithm> 


#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"
// #define _SUPPORT_MATLAB_ 

#include <complex>
#include <vector>
#include <string>
#include <ctime>
#include <list>
#include <sstream>
#include <string>
#include <limits>
#include <omp.h>

#define SUB2IND(X, Y, Z, shape)  (((Z)*(shape[1])+(Y)) *(shape[2])+(X)) 
#include "mhs_vector.h"

//mex mhs_distmapC.cc -lgomp CXXFLAGS=" -O3   -Wfatal-errors  -std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"

template<typename T>
void hsv2rgb ( T h, T s ,T v,T &r, T & g ,T & b )
{
    T      hh, p, q, t, ff;
    int        i;


    if ( s <= 0.0 ) {  
        r = v;
        g = v;
        b = v;
    }
    hh = h;
    if ( hh >= 360.0 ) {
        hh = 0.0;
    }
    hh /= 60.0;
    i = ( int ) hh;
    ff = hh - i;
    p = v * ( 1.0 - s );
    q = v * ( 1.0 - ( s * ff ) );
    t = v * ( 1.0 - ( s * ( 1.0 - ff ) ) );

    switch ( i ) {
    case 0:
        r = v;
        g = t;
        b = p;
        break;
    case 1:
        r = q;
        g = v;
        b = p;
        break;
    case 2:
        r = p;
        g = v;
        b = t;
        break;

    case 3:
        r = p;
        g = q;
        b = v;
        break;
    case 4:
        r = t;
        g = p;
        b = v;
        break;
//     case 5:
    default:
        r = v;
        g = p;
        b = q;
        break;
    }
}


class path_indx_class{
public:
    int path_id;
    std::size_t indx;
    path_indx_class()
    {
        
    }
    inline bool operator< (const path_indx_class& rhs){return path_id<rhs.path_id;}
    inline bool operator> (const path_indx_class& rhs){return path_id>rhs.path_id;}
};
  



template<typename T>
void blas_matrix_mult(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
  
}


/*
template<>
void blas_matrix_mult<double>(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    double one[2] = {1,0};
    double zero[2] = {0,0};
    cblas_zgemm (CblasColMajor,
		 CblasTrans,
		 CblasNoTrans,
		 ndir,
		 lp_n,
		 stride,
		 (void*) one,
		 mout,
		 stride,
		 pin,
		 stride,
		 (void*) zero,
		 tmp,
		 ndir);           
}

template<>
void blas_matrix_mult<float>(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    float one[2] = {1,0};
    float zero[2] = {0,0};
    cblas_cgemm (CblasColMajor,
		 CblasTrans,
		 CblasNoTrans,
		 ndir,
		 lp_n,
		 stride,
		 (void*) one,
		 mout,
		 stride,
		 pin,
		 stride,
		 (void*) zero,
		 tmp,
		 ndir);           
}


template <typename T> 
void dir2sh(Vector<T,3> & v, unsigned int L, std::complex<T> * buffer)
{
   buffer[0] = 1;
    
   if (L==0) return;

   std::complex<T> I = std::complex<T>(0,1);
        
   T x = v[0];
   T y = v[1];
   T z = v[2];
  
   std::complex<T> x_minues_Iy = (x -  I* y);
   std::complex<T> x_minues_Iy_2 =  x_minues_Iy*x_minues_Iy;
   std::complex<T> x_minues_Iy_4 =  x_minues_Iy_2*x_minues_Iy_2;
   
   buffer[1] = 0.5* std::sqrt(3.0/2.0) *  x_minues_Iy_2 ;  
   buffer[2] = std::sqrt(3.0/2.0) *  x_minues_Iy  *  z;  
   buffer[3] = -(x*x/2) - y*y/2 + z*z;  
   
   if (L==2) return;
    
   buffer[4] =  1.0/8.0 * std::sqrt(35.0/2.0) *x_minues_Iy_4;
   buffer[5] =  1.0/4.0 *  std::sqrt(35.0) *  x_minues_Iy_2*z;
   buffer[6] =  -(1.0/4.0) *  std::sqrt(5.0/2.0) * x_minues_Iy_2 *  (x*x + y*y - 6 *z*z);
   buffer[7] =  -(1.0/4.0)* std::sqrt(5.0)* x_minues_Iy * z * (3* x*x + 3*y*y - 4* z*z);
   buffer[8] = (3* x*x*x*x)/8 + (3 *y*y*y*y)/8 - 3* y*y* z*z + z*z*z*z + 3.0/4.0 * x*x* (y*y - 4* z*z);
}
*/


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  try
        {
  
	  Vector<std::size_t,3>  target_shape;
	  target_shape=std::size_t(0);
	  const mxArray * params=prhs[nrhs-1];
	  T b=5;
	  bool consider_scale=false;
	  int mode=0;
	bool weight=true;
        bool normalize=true;
		T sigma=0.25;	  
T gamma=0.5;
		Vector<T,3> offset;	
		Vector<T,3> vscale;
		offset=T(0);
		vscale=T(1);
		
	bool lininterp=true;
    
    //mxArray * mapmat = NULL;
    mxArray * dirs = NULL;
    mxArray * dir_array = NULL;
    
    
    

	    

	  if ( mxIsCell ( params  )  )
	  {
	      if ( mhs::mex_hasParam ( params,"shape" ) !=-1 )
		      {
		      target_shape=mhs::mex_getParam<T> ( params,"shape",3 ) ;
		      }
		       if ( mhs::mex_hasParam ( params,"offset" ) !=-1 )
		      {
		      offset=mhs::mex_getParam<T> ( params,"offset",3 ) ;
		      }
		       if ( mhs::mex_hasParam ( params,"vscale" ) !=-1 )
		      {
		      vscale=mhs::mex_getParam<T> ( params,"vscale",3 ) ;
		      }
	      if ( mhs::mex_hasParam ( params,"b" ) !=-1 )
			{
		      b=mhs::mex_getParam<T> ( params,"b",1 ) [0];
		      }
		      
		if ( mhs::mex_hasParam ( params,"consider_scale" ) !=-1 )
			{
		      consider_scale=mhs::mex_getParam<bool> ( params,"consider_scale",1 ) [0];
		      }		
		      
		if ( mhs::mex_hasParam ( params,"mode" ) !=-1 )
			{
		      mode=mhs::mex_getParam<int> ( params,"mode",1 ) [0];
		      }
			if ( mhs::mex_hasParam ( params,"sigma" ) !=-1 )
			{
		      sigma=mhs::mex_getParam<T> ( params,"sigma",1 ) [0];
		      }
if ( mhs::mex_hasParam ( params,"gamma" ) !=-1 )
			{
		      gamma=mhs::mex_getParam<T> ( params,"gamma",1 ) [0];
		      }

	      
		if ( mhs::mex_hasParam ( params,"weight" ) !=-1 )
			{
		      weight=mhs::mex_getParam<bool> ( params,"weight",1 ) [0];
		      }
		      
		      if ( mhs::mex_hasParam ( params,"normalize" ) !=-1 )
			{
		      normalize=mhs::mex_getParam<bool> ( params,"normalize",1 ) [0];
		      }
		      
		if ( mhs::mex_hasParam ( params,"lininterp" ) !=-1 )
			{
		      lininterp=mhs::mex_getParam<bool> ( params,"lininterp",1 ) [0];
		      }
		      /*
            if ( mhs::mex_hasParam ( params,"mapmat" ) !=-1 )
			{
		      mapmat=mhs::mex_getParamPtr( params,"mapmat");
		    }   
		    */
            if ( mhs::mex_hasParam ( params,"dirs" ) !=-1 )
			{
		      dirs=mhs::mex_getParamPtr( params,"dirs");
		    }
		    
		    if ( mhs::mex_hasParam ( params,"dir_array" ) !=-1 )
			{
		      dir_array=mhs::mex_getParamPtr( params,"dir_array");
		    } 
		      
	  }
// 	    
    printf("weight :%d\n",weight);
    printf("normalize :%d\n",normalize);

		offset.print();

	  mhs::dataArray<T>  edges;
	  edges=mhs::dataArray<T> ( prhs[0]);	 
	  
	  T *result = NULL; //( T * ) mxGetData ( plhs[0]);
	  T *result_rgb = NULL;
	  T *result_gray = NULL;
      T *result_tensor = NULL;
      T * tensor_dirs = NULL;
      T * reference_dirs = NULL;
      std::size_t n_tensor_dirs = 0;
      
      std::size_t ref_num_voxel=0;
      //Vector<float,3> scale_to_ref_dirs; 
      //std::size_t reference_dirs_shape[3];
      
          
          T * accu = NULL;
          int * accu_id = NULL;
          
    //if (mapmat!=NULL)
    if (dirs!=NULL)
    {
            mhs::dataArray<T>  dirs_;
            dirs_=mhs::dataArray<T> (dirs);	 
            sta_assert_error(dirs_.dim.size()==2);
            printf("num dirs:%d\n",dirs_.dim[0]);
            printf("dir dim:%d\n",dirs_.dim[1]);
            
            
            sta_assert_error(dirs_.dim[1]==3);
            sta_assert_error(dirs_.dim[0]>1);
            
            
            printf("%f %f %f\n",dirs_.data[0],dirs_.data[1],dirs_.data[2]);
            
            tensor_dirs = dirs_.data;
            n_tensor_dirs = dirs_.dim[0];
            mode = -1;
    }
    
    if (dir_array!=NULL)
    {
            mhs::dataArray<T>  dirs_;
            dirs_=mhs::dataArray<T> (dir_array);	 
            sta_assert_error(dirs_.dim.size()==4);
            
            //printf("num dirs:%d\n",dirs_.dim[3]);
            printf("%d %d %d %d\n",dirs_.dim[0],dirs_.dim[1],dirs_.dim[2],dirs_.dim[3]);
                                
            sta_assert_error(dirs_.dim[3]==3);
            sta_assert_error(dirs_.dim[0]>1);
            
            printf("%f %f %f\n",dirs_.data[0],dirs_.data[
            1],dirs_.data[2]);
            
            ref_num_voxel = dirs_.dim[0]*dirs_.dim[1]*dirs_.dim[2]*dirs_.dim[3];
            //dirs_.dim[3]
            sta_assert_error(target_shape[2]==dirs_.dim[2]);
            sta_assert_error(target_shape[1]==dirs_.dim[1]);
            sta_assert_error(target_shape[0]==dirs_.dim[0]);
            
            reference_dirs = dirs_.data;
            mode = 3;
    }

    
	  
	  switch (mode)
	  {
	    case 0:
	    {
	    mwSize ndims[3];
	    ndims[2]=target_shape[0];
	    ndims[1]=target_shape[1];
	    ndims[0]=target_shape[2];
	    
	    plhs[0] = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    result = ( T * ) mxGetData ( plhs[0]);;
            
            //accu  = new T [ndims[2]*ndims[1]*ndims[0]*2];
	    }break;
	    
	    case 1:
	    {
	    mwSize ndims[3];
	    ndims[2]=target_shape[0];
	    ndims[1]=target_shape[1];
	    ndims[0]=target_shape[2];
	    
	    plhs[0] = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    result_gray = ( T * ) mxGetData ( plhs[0]);;
            
            accu  = new T [ndims[2]*ndims[1]*ndims[0]];
            accu_id = new int [ndims[2]*ndims[1]*ndims[0]];
	    }break;
	    
	    case 2:
	    {
	    mwSize ndims[4];
	    ndims[3]=target_shape[0];
	    ndims[2]=target_shape[1];
	    ndims[1]=target_shape[2];
	    ndims[0]=3;
	    
            accu  = new T [ndims[3]*ndims[2]*ndims[1]*4];
            accu_id = new int [ndims[3]*ndims[2]*ndims[1]];
            
	    plhs[0] = mxCreateNumericArray ( 4,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    
	    result_rgb = ( T * ) mxGetData ( plhs[0]);;
	    }break;
        
         case -1:
	    {
	    mwSize ndims[4];
        ndims[3]=n_tensor_dirs;
	    ndims[2]=target_shape[0];
	    ndims[1]=target_shape[1];
	    ndims[0]=target_shape[2];
	    
	    
        // accu  = new T [ndims[3]*ndims[2]*ndims[1]*4];
        // accu_id = new int [ndims[3]*ndims[2]*ndims[1]];
            
	    plhs[0] = mxCreateNumericArray ( 4,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    
	    result_tensor = ( T * ) mxGetData ( plhs[0]);;
	    }break;
        
         case 3:
	   {
           sta_assert_error(dir_array!=NULL);
	   mwSize ndims[4];
	    ndims[3]=target_shape[0];
	    ndims[2]=target_shape[1];
	    ndims[1]=target_shape[2];
	    ndims[0]=3;
	    
            accu  = new T [ndims[3]*ndims[2]*ndims[1]*4];
            accu_id = new int [ndims[3]*ndims[2]*ndims[1]];
            
	    plhs[0] = mxCreateNumericArray ( 4,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    
	    result_rgb = ( T * ) mxGetData ( plhs[0]);;
	    }break;
	    
	    default: 
	      return;
	  }
	 

	  
	  
	 
	  
		  
	  sta_assert_error(edges.dim.size()==2);
// 	  printf("%d %d\n",edges.dim[0],edges.dim[1]);
	  
	  sta_assert_error(edges.dim[0]%2==0);
	  
	  std::size_t num_edges=edges.dim[0]/2;
	  
	  
	  std::size_t numv=target_shape[0]*target_shape[1]*target_shape[2];
	  
	  if (mode==0)
	  {
	   #pragma omp parallel for num_threads(omp_get_num_procs())
	  for (std::size_t e=0;e<numv;e++)
	  {
	    result[e]=b;
            
	  }
	  }
	  if (mode>0)
	  {
              for (std::size_t e=0;e<numv;e++)
            {
                accu_id[e]=-1000;
            }
          }
	  
	  

// 	  //printf("num edges %d \n",num_edges);


	  
	  const T * data=edges.data;
    
	  int path_ids[2];
	  path_ids[0]=std::numeric_limits< int >::max();
	  path_ids[1]=-std::numeric_limits< int >::max();

        
          
          std::vector<path_indx_class> indeces;
          indeces.resize(num_edges);
          for (std::size_t e=0;e<num_edges;e++)
	  {
	   const T * p_data=data+e*10; 
	   int path_id=p_data[4];
           indeces[e].path_id=path_id;
           indeces[e].indx=e;
	  }
	  
	  std::sort (indeces.begin(), indeces.end());
          
	  if (mode>0)
	  {
	  for (std::size_t e=0;e<num_edges;e++)
	  {
	   const T * p_data=data+e*10; 
	   int path_id=p_data[4];
	   path_ids[0]=std::min(path_ids[0],path_id);
	    path_ids[1]=std::max(path_ids[1],path_id);
	  }
	  }

	  

 int deb=0;
	  
	  #pragma omp parallel for num_threads(omp_get_num_procs())
	  for (std::size_t e=0;e<num_edges;e++)
	  {

             const T * p_data=data+indeces[e].indx*10; 
             int path_id=indeces[e].path_id;
           
           
	   Vector<T,3> x1(p_data[2],p_data[1],p_data[0]);
	   T s1=p_data[3];
	   Vector<T,3> x2(p_data[7],p_data[6],p_data[5]);
	   T s2=p_data[8];

	   
		x1+=offset;
		x2+=offset; 

	   int path_id2=p_data[4];
           sta_assert_error(path_id2==path_id);
	   
	   std::size_t bb[2][3];
	   
	   for (int i=0;i<3;i++)
	   {
	     int b2=std::ceil(b*vscale[i]);
	     if (!consider_scale)
	     {
	      bb[0][i]=std::max(std::min(x1[i]-b2,x2[i]-b2),T(0));
	      bb[1][i]=std::max(std::min(std::max(x1[i]+b2,x2[i]+b2),T(target_shape[i])),T(0));
	     }else
	     {
	      bb[0][i]=std::max(std::min(x1[i]-s1-b2*s1,x2[i]-s2-b2*s2),T(0));
	      bb[1][i]=std::max(std::min(std::max(x1[i]+s1+b2*s1,x2[i]+s2+b2*s2),T(target_shape[i])),T(0));
	     }

	   }
	   
	  

	   
	   
	   Vector<T,3>  x3;
	   
	   for (std::size_t z=bb[0][0];z<bb[1][0];z++)
	   {
	     x3[0]=z;
	    for (std::size_t y=bb[0][1];y<bb[1][1];y++)
	    {
	      x3[1]=y;
	      for (std::size_t x=bb[0][2];x<bb[1][2];x++)
	      {
		x3[2]=x;
		
// 		Vector<T,3> x3_2;
// 		  x3_scaled=x3/vscale;
		
		if (true)
		{
		
		std::size_t indx=SUB2IND(x, y, z, target_shape);
		sta_assert_error(indx<numv);

		//get_dist_from_surface(const Vector<T,3> & lx1,T s1,const Vector<T,3> & lx2,T s2,const Vector<T,3> & x3,T max_dist=-1);
		T score=0;
		  Vector<T,3> x1_s=x1/vscale;
		  Vector<T,3> x2_s=x2/vscale;
		  Vector<T,3> x3_s=x3/vscale;		
		if (!consider_scale)
		{
		  //score=get_dist_from_line(x1,x2,x3);
		  score=get_dist_from_line(x1_s,x2_s,x3_s);
		}else
		{
		  score=get_dist_from_surface(x1,s1,x2,s2,x3);
		}
// 		if (score>0)
// 		{
// 		  printf("%f\n",score);
// 		}
		
		//T old_score=result[indx];
		
		switch (mode)
		{
        case 3:
		  {
		   sta_assert_error(result_rgb!=NULL);
		   float alpha;
		   if (lininterp)
		   {
		    alpha=std::max(sigma-score,T(0))/sigma;
		   }else
		   {
		    alpha=std::exp(-score*score/(sigma*sigma));
		   }
            Vector<T,3> direction;
		    direction=x1-x2;
		    direction.normalize();
            
            std::swap(direction[0],direction[2]);
           
                    sta_assert_error(indx*3+2<ref_num_voxel);
                    Vector<T,3> background_dir;
                    background_dir[0] = reference_dirs[indx*3];
                    background_dir[1] = reference_dirs[indx*3+1];
                    background_dir[2] = reference_dirs[indx*3+2];
                    float bgl = background_dir.norm2();
                    
                if (bgl>0.0000001)
                {
                    float red = (background_dir.dot(direction));
                    float green = std::sqrt((direction - background_dir*red).norm2());

                    red = std::abs(red);
               
                {
                    if (weight)
                    {
                                    sta_assert_error(((indx*4+3)<target_shape[0]*target_shape[1]*target_shape[2]*4))
                                        if (accu_id[indx]==path_id)
                                        {
                                            if (accu[indx*4]<alpha)
                                            {
                                                //for (int c=0;c<3;c++)
                                                //{
                                                    int c;
                                                    c = 0;
                                                    sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                                    result_rgb[indx*3+c]+=red*alpha-accu[indx*4+c+1];                                        
                                                    accu[indx*4+c+1]=red*alpha;
                                                    
                                                    c = 1;
                                                    sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                                    result_rgb[indx*3+c]+=green*alpha-accu[indx*4+c+1];                                        
                                                    accu[indx*4+c+1]=green*alpha;
                                                    
                                            // }
                                                accu[indx*4]=alpha;
                                            }
                                        }else
                                        {
                                            // for (int c=0;c<3;c++)
                                            {
                                            //  result_rgb[indx*3+c]+=color[c]*alpha;                                        
                                            //  sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                            //  accu[indx*4+c+1]=color[c]*alpha;
                                                
                                                    int c;
                                                    c = 0;
                                                    sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                                    result_rgb[indx*3+c]+=red*alpha;                                        
                                                    accu[indx*4+c+1]=red*alpha;
                                                    
                                                    c = 1;
                                                    sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                                    result_rgb[indx*3+c]+=green*alpha;                                        
                                                    accu[indx*4+c+1]=green*alpha;
                                            }
                                            accu[indx*4]=alpha;
                                            accu_id[indx]=path_id;
                                        }                                                                
                    }else
                    {
                        T old_score=result_rgb[indx*3+0]*result_rgb[indx*3+0]+
                        result_rgb[indx*3+1]*result_rgb[indx*3+1]+
                        result_rgb[indx*3+2]*result_rgb[indx*3+2];
                        if (alpha*alpha>old_score)
                        {	
                        
                        //for (int c=0;c<3;c++)
                        {
                            //result_rgb[indx*3+c]=result_rgb[indx*3+c]*(1-alpha)+color[c]*(alpha);
                            int c;
                            c=0;
                            result_rgb[indx*3+c]=result_rgb[indx*3+c]*(1-alpha)+red*(alpha);	
                            c=1;
                            result_rgb[indx*3+c]=result_rgb[indx*3+c]*(1-alpha)+green*(alpha);	
                        }
                        
                        }
                    } 
                    }
            }
		   
		  } break;    
        
          
          
          
            case -1:
            {
                sta_assert_error(result_tensor!=NULL);
                 float alpha;
                 
                if (lininterp)
                {
                    alpha=std::max(sigma-score,T(0));
                }else
                {
                    alpha=std::exp(-score*score/(sigma*sigma));
                }
               
                
                if (alpha>0)
                //if (false)
                {
                    Vector<T,3> dir;
                    dir=x1-x2;
                    dir.normalize();
                    
                    
                    for (std::size_t d=0;d<n_tensor_dirs;d++)
                    {
                        //Vector<T,3> tdir(tensor_dirs+d*3);
                        Vector<T,3> tdir(tensor_dirs[d*3+2],tensor_dirs[d*3+1],tensor_dirs[d*3]);     
                        //T score = std::abs(dir.dot(tdir));
                        T score = 1-std::abs(dir.dot(tdir));
                        
                        result_tensor[d*numv+indx]+=score*alpha;   
                        
                    }
                
                }
                
                
            } break;
                
                
            
		  case 0:
		  {
		   sta_assert_error(result!=NULL);
		   result[indx]=std::min(result[indx],score);
		   
		  } break;
		  case 1:
		  {
		   sta_assert_error(result_gray!=NULL);
// 		   float alpha=std::max(sigma-score,T(0));
		   float alpha;
		   if (lininterp)
		   {
		    alpha=std::max(sigma-score,T(0));
		   }else
		   {
		    alpha=std::exp(-score*score/(sigma*sigma));
		   }
		   
			if (weight)
			{
                                if (accu_id[indx]==path_id)
                                {
                                    if (accu[indx]<alpha)
                                    {
                                        result_gray[indx]+=alpha-accu[indx];    
                                        accu[indx]=alpha;
                                    }
                                }else
                                {
                                    result_gray[indx]+=alpha;    
                                    accu[indx]=alpha;
                                    accu_id[indx]=path_id;
                                }                                
                            
				 
			}else
			{
				if (alpha>result_gray[indx])
				{
				  result_gray[indx]=alpha;	
			    
				}
			 }
		   
		  } break;
		  
		   case 2:
		  {
		   sta_assert_error(result_rgb!=NULL);
		   float alpha;
		   if (lininterp)
		   {
		    alpha=std::max(sigma-score,T(0))/sigma;
		   }else
		   {
		    alpha=std::exp(-score*score/(sigma*sigma));
		   }
		    Vector<T,3> color;
		    color=x1-x2;
		    color.normalize();
            
            std::swap(color[0],color[2]);
		    color[0]=std::abs(color[0]);
		    color[1]=std::abs(color[1]);
		    color[2]=std::abs(color[2]);

		  
			
		  {
		    if (weight)
			{
                                
// 				for (int c=0;c<3;c++)
// 				 result_rgb[indx*3+c]+=color[c]*alpha;
                                
                            sta_assert_error(((indx*4+3)<target_shape[0]*target_shape[1]*target_shape[2]*4))
//                             printf("A%u\n",indx);
                                if (accu_id[indx]==path_id)
                                {
//                                      printf("A%u\n",indx);
                                    if (accu[indx*4]<alpha)
                                    {
                                        //result_gray[indx]+=alpha-accu[indx];    
                                        for (int c=0;c<3;c++)
                                        {
                                            sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                            result_rgb[indx*3+c]+=color[c]*alpha-accu[indx*4+c+1];                                        
                                            accu[indx*4+c+1]=color[c]*alpha;
                                        }
                                        accu[indx*4]=alpha;
                                    }
                                }else
                                {
                                    //result_gray[indx]+=alpha;    
                                   // accu[indx*4]=alpha;
//                                     printf("B%u\n",indx);
                                     for (int c=0;c<3;c++)
                                    {
                                        result_rgb[indx*3+c]+=color[c]*alpha;                                        
                                        sta_assert_error((indx*4+c+1)<target_shape[0]*target_shape[1]*target_shape[2]*4)
                                        accu[indx*4+c+1]=color[c]*alpha;
                                    }
//                                     printf("C%u\n",indx);
                                     accu[indx*4]=alpha;
//                                      printf("D%u\n",indx);
                                    accu_id[indx]=path_id;
//                                     printf("E%u\n",indx);
                                }                                                                
                                
                                
			}else
			{
				T old_score=result_rgb[indx*3+0]*result_rgb[indx*3+0]+
				result_rgb[indx*3+1]*result_rgb[indx*3+1]+
				result_rgb[indx*3+2]*result_rgb[indx*3+2];
				if (alpha*alpha>old_score)
				{	
				
				   for (int c=0;c<3;c++)
                   {
                    //if (alpha<0)
                    //    printf("alpha:%f\n",alpha);
                    //if (alpha>1)
                    //    printf("alpha:%f\n",alpha);
                    
                    result_rgb[indx*3+c]=result_rgb[indx*3+c]*(1-alpha)+color[c]*(alpha);	
                   }
			    
				}
			} 
		    }

		   
		  } break;
		    
		}
	      }
		
	      } 
	    } 
	   }
	   
	  }


	if (weight && normalize)
	{
	T maxw=0.000000000000001;
	T maxw_g=0.000000000000001;


	  #pragma omp parallel for num_threads(omp_get_num_procs())
	  for (std::size_t e=0;e<numv;e++)
	  {
	    if (result_rgb!=NULL)
	    {
	      T * c=result_rgb+e*3;
		T n=std::sqrt(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);

		T nsqrt=n;
		if (std::abs(gamma-1)>0.0000001)
		{	
			nsqrt=std::pow(n,gamma);
		}
		T w=nsqrt/(n+0.00000000000000000000001); 
		c[0]*=w;
		c[1]*=w;
		c[2]*=w;
		maxw=std::max(maxw,nsqrt);
	    }
	    if (result_gray!=NULL)
	    {
		maxw_g=std::max(maxw_g,result_gray[e]);
	    }
		//maxw=std::max(maxw,n);
	  }

 	  #pragma omp parallel for num_threads(omp_get_num_procs())
	  for (std::size_t e=0;e<numv;e++)
	  {
	    if (result_rgb!=NULL)
	    {
	    T * c=result_rgb+e*3;
		c[0]/=maxw;
		c[1]/=maxw;
		c[2]/=maxw;
	    }
	    if (result_gray!=NULL)
	    {
		result_gray[e]/=maxw_g;
	    }
	  }
	}	  

	if (accu!=NULL)
        {
            delete [] accu;
            delete [] accu_id;
        }
	}
     catch ( mhs::STAError & error )
        {
        printf ( "error cleaning up!!\n" );
        mexErrMsgTxt ( error.what() );
        }
    catch ( ... )
        {
        printf ( "error cleaning up!!\n" );
        mexErrMsgTxt ( "exeption ?" );
        }
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{    
 

    
    if ( nrhs<2 )
        mexErrMsgTxt ( "error: nrhs<2\n" );
    
   const mxArray * img=prhs[0];
    
    if ((img==NULL)||(mxGetClassID(img)==mxDOUBLE_CLASS))
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    else if (mxGetClassID(img)==mxSINGLE_CLASS)
    _mexFunction<float> ( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported data type\n");
}

