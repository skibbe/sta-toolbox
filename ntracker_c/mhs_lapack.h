#ifndef MHS_LAPACK_H
#define MHS_LAPACK_H



// #ifdef _WIN64
 #ifdef _WINDONGDINGODONG
	bool eig_3x3_symm(const double & xx,
			  const double & xy,
			  const double & xz,
			  const double & yy,
			  const double & yz,
			  const double & zz,
			  double * Evec, 
			  double * Eval,
			  bool sort=true
			) 
	{
	  
	  
	  return true;

	}

	bool eig_3x3_symm(const float & xx,
			  const float & xy,
			  const float & xz,
			  const float & yy,
			  const float & yz,
			  const float & zz,
			  float * Evec, 
			  float * Eval,
			  bool sort=true) 
	{
	  return true;

	  
	}



	bool rootsP4(const double & a,
		    const double & b,
		    const double & c,
		    const double & d,
		    const double & e,
		    double * wr, double * wi) 
	{
	    return true;
	    
	    

	}       
	
#else 

	#ifdef _OCTAVE_
	  #include "lapacke.h"
	#else
	    #include "lapack.h"
	#endif    
	#include "string.h"


	bool eig_3x3_symm(const double & xx,
			  const double & xy,
			  const double & xz,
			  const double & yy,
			  const double & yz,
			  const double & zz,
			  double * Evec, 
			  double * Eval,
			  bool sort=true
			) 
	{
	  double A[]={xx,xy,xz,
			xy,yy,yz,
			xz,yz,zz};      
	  char JOBZ='V';  
	  char UPLO='U';
	  
	//     double D[N];
	//     double E[N-1];
			
	
	  
	  #ifdef _OCTAVE_
	    lapack_int N=3;
	    lapack_int ldh=N;   
	    lapack_int  lwork=3*N-1;
	    lapack_int info;
	    
	    double work[lwork];
	    
	    LAPACK_dsyev(  
		&JOBZ,
		&UPLO,
		&N,
		A,
		&ldh,
		Eval,
		work,
		&lwork,
		&info
		      );
	    
	  #else 
	  
	    std::ptrdiff_t N=3;
	    std::ptrdiff_t ldh=N;
	    std::ptrdiff_t lwork=3*N-1;
	    std::ptrdiff_t info;
	    
	    double work[lwork];
	  
	    dsyev(  
		&JOBZ,
		&UPLO,
		&N,
		A,
		&ldh,
		Eval,
		work,
		&lwork,
		&info
		      );
	    #endif 
	    
	    
	  memcpy(Evec, A,sizeof(double)*9);
	  if (sort)
	  {
	  for (int a=1;a<3;a++)
	  {
	    if (std::abs(Eval[a])>std::abs(Eval[a-1]))
	    {
	      std::swap(Eval[a],Eval[a-1]); 
	      std::swap(Evec[(a)*3],Evec[(a-1)*3]);
	      std::swap(Evec[(a)*3+1],Evec[(a-1)*3+1]);
	      std::swap(Evec[(a)*3+2],Evec[(a-1)*3+2]);
	    }
	  }
	  for (int a=1;a<2;a++)
	  {
	    if (std::abs(Eval[a])>std::abs(Eval[a-1]))
	    {
	      std::swap(Eval[a],Eval[a-1]); 
	      std::swap(Evec[(a)*3],Evec[(a-1)*3]);
	      std::swap(Evec[(a)*3+1],Evec[(a-1)*3+1]);
	      std::swap(Evec[(a)*3+2],Evec[(a-1)*3+2]);
	    }
	  }
	  }   
	  
	  return (info==0);

	}

	bool eig_3x3_symm(const float & xx,
			  const float & xy,
			  const float & xz,
			  const float & yy,
			  const float & yz,
			  const float & zz,
			  float * Evec, 
			  float * Eval,
			  bool sort=true) 
	{
	  
	    
	  char JOBZ='V';  
	  char UPLO='U';
	//     float D[N];
	//     float E[N-1];
	    float A[]={xx,xy,xz,
		    xy,yy,yz,
		    xz,yz,zz};  
			
	  
	  #ifdef _OCTAVE_
	    lapack_int N=3;
	    lapack_int ldh=N;   
	    lapack_int  lwork=3*N-1;
	    lapack_int info;
	    
	    float work[lwork];
	    
	    LAPACK_ssyev(  
		&JOBZ,
		&UPLO,
		&N,
		A,
		&ldh,
		Eval,
		work,
		&lwork,
		&info
		      );
	  #else 
	  
	  
	    std::ptrdiff_t N=3;
	    std::ptrdiff_t ldh=N;
	    std::ptrdiff_t lwork=3*N-1;
	    std::ptrdiff_t info;
	    float work[lwork];

		ssyev( &JOBZ,
		      &UPLO,
		      &N,
		      A,
		      &ldh,
		      Eval,
		      work,
		      &lwork,
		      &info
		);
	      #endif
	
	  
	  memcpy(Evec, A,sizeof(float)*9);
	    if (sort)
	  {
	  for (int a=1;a<3;a++)
	  {
	    if (std::abs(Eval[a])>std::abs(Eval[a-1]))
	    {
	      std::swap(Eval[a],Eval[a-1]); 
	      std::swap(Evec[(a)*3],Evec[(a-1)*3]);
	      std::swap(Evec[(a)*3+1],Evec[(a-1)*3+1]);
	      std::swap(Evec[(a)*3+2],Evec[(a-1)*3+2]);
	    }
	  }
	  for (int a=1;a<2;a++)
	  {
	    if (std::abs(Eval[a])>std::abs(Eval[a-1]))
	    {
	      std::swap(Eval[a],Eval[a-1]); 
	      std::swap(Evec[(a)*3],Evec[(a-1)*3]);
	      std::swap(Evec[(a)*3+1],Evec[(a-1)*3+1]);
	      std::swap(Evec[(a)*3+2],Evec[(a-1)*3+2]);
	    }
	  }
	  }
	  
	  return (info==0);

	  
	}



	bool rootsP4(const double & a,
		    const double & b,
		    const double & c,
		    const double & d,
		    const double & e,
		    double * wr, double * wi) 
	{
	    #ifdef _OCTAVE_
	    lapack_int ilo=1;
	    lapack_int ihi=4;
	    lapack_int ldh=4;
	    lapack_int info;
	    lapack_int n=4;
	    char JOB='E';
	    char COMPZ='N';
	    
	    lapack_int lwork=4;
	    double work[4];
	    double Z[4];
	    
	    
	    double h[]={0.0,1.0,0.0,0.0,
			0.0,0.0,1.0,0.0,
			0.0,0.0,0.0,1.0,
			-e/a,-d/a,-c/a,-b/a};    
	      LAPACK_dhseqr(&JOB,&COMPZ,&n,
			&ilo,
			&ihi,
			h,
			&ldh,
			wr,
			wi,
			Z, //Z
			&ldh, //LDZ
			work,
			&lwork, 
			&info);
		return (info==0);
		
	  #else 
	  std::ptrdiff_t ilo=1;
	    std::ptrdiff_t ihi=4;
	    std::ptrdiff_t ldh=4;
	    std::ptrdiff_t info;
	    std::ptrdiff_t n=4;
	    char JOB='E';
	    char COMPZ='N';
	    
	    std::ptrdiff_t lwork=4;
	    double work[4];
	    double Z[4];
	    
	    
	    double h[]={0.0,1.0,0.0,0.0,
			0.0,0.0,1.0,0.0,
			0.0,0.0,0.0,1.0,
			-e/a,-d/a,-c/a,-b/a};    
	      dhseqr(&JOB,&COMPZ,&n,
			&ilo,
			&ihi,
			h,
			&ldh,
			wr,
			wi,
			Z, //Z
			&ldh, //LDZ
			work,
			&lwork, 
			&info);
		return (info==0);
	  #endif	
	    
	    

	}       
	
#endif








#endif