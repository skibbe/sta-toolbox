#ifndef MHS_DATA_SPINES_H
#define MHS_DATA_SPINES_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataSpines: public CData<T,TData,Dim>
{
private:
  
   
  
  
    int scale_power;

    const TData * scales;
    std::size_t num_scales;

    const TData * local_stdv;
    int num_alphas_local_stdv;
    
//     const TData * local_mean;
//     int num_alphas_local_mean;

//     const TData * spine_data;
//     int num_spine_data;
    
    const TData * img_smooth;
    int num_alphas_img_smooth;
    
    const TData * dist_trafo;
    T max_dist;
    

    Vector<T,Dim> boundary;
    bool scale_dependend_boundary;


    
    
    T min_scale;
    T max_scale;
    T scale_correction;
    

    T DataThreshold;
    T DataScale;
    T StdvEpsilon;
    T Opt_Spine_dist;
    T DistScale;
    T DataGradScale;

    
    Vector<T,3> element_size;
    Vector<std::size_t ,3> feature_shape;
    
    
    
    

public:

    float get_minscale() {
        return min_scale/scale_correction;
    };

    float get_maxscale() {
        return max_scale/scale_correction;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataSpines() {
      max_dist=0;
    }

    ~CDataSpines() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
//                 DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
//                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
//            printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
//        printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
    }
#endif

    void init ( const mxArray * feature_struct ) {

        mhs::dataArray<TData>  tmp0;
        mhs::dataArray<TData>  tmp1;
        mhs::dataArray<TData>  tmp2;
        mhs::dataArray<TData>  tmp3;
	mhs::dataArray<TData>  tmp4;
	mhs::dataArray<TData>  tmp5;
	
	
        try {
            tmp3=mhs::dataArray<TData> ( feature_struct,"shape_boundary" );
            sta_assert_error ( tmp3.get_num_elements() ==3 );
            boundary[0]=tmp3.data[0];
            boundary[1]=tmp3.data[1];
            boundary[2]=tmp3.data[2];
            scale_dependend_boundary=true;
        } catch ( mhs::STAError error ) {
            boundary[0]=boundary[1]=boundary[2]=T ( 0 );
            scale_dependend_boundary=false;
        }
        
       

        try {

	    tmp1=mhs::dataArray<TData> ( feature_struct,"img_data" );
            img_smooth=tmp1.data;
            num_alphas_img_smooth=tmp1.dim[tmp1.dim.size()-1];
            printf ( "img smoothed pol degree: %d \n",num_alphas_img_smooth-1 );
	    
	    
	    max_dist=mhs::dataArray<TData> ( feature_struct,"maxdist" ).data[0];
	    printf("maxdist :%f\n",max_dist);
	    

//             tmp1=mhs::dataArray<TData> ( feature_struct,"spine_data" );
//             spine_data=tmp1.data;
//             num_spine_data=tmp1.dim[tmp1.dim.size()-1];
//             printf ( "spine data samples: %d \n",num_spine_data);

            tmp2=mhs::dataArray<TData> ( feature_struct,"alphas_sdv" );
            local_stdv=tmp2.data;
            num_alphas_local_stdv=tmp2.dim[tmp2.dim.size()-1];
            printf ( "local stdv pol degree: %d \n",num_alphas_local_stdv-1 );

           sta_assert_error ( tmp2.get_num_elements() /num_alphas_local_stdv==tmp1.get_num_elements() /(num_alphas_img_smooth) );
	   
// 	    tmp4=mhs::dataArray<TData> ( feature_struct,"alphas_mean" );
//             local_mean=tmp4.data;
//             num_alphas_local_mean=tmp4.dim[tmp4.dim.size()-1];
//             printf ( "local mean pol degree: %d \n",num_alphas_local_mean-1 );
// 
//            sta_assert_error ( tmp4.get_num_elements() /num_alphas_local_mean==tmp1.get_num_elements() /(num_samples_sd_data*6) );

            for ( int i=0; i<3; i++ ) {
                feature_shape[i]=tmp1.dim[i];
            }
            
           tmp5=mhs::dataArray<TData> ( feature_struct,"dist" );
           dist_trafo=tmp5.data;
            
        } catch ( mhs::STAError error ) {
            throw error;
        }


        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

        std::swap ( this->shape[0],this->shape[2] );

        for ( int i=0; i<3; i++ ) {
            element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
        }

        printf ( "element size data term:" );
        element_size.print();

        scale_correction=1;
        try {
            scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
        } catch ( mhs::STAError error ) {
            throw error;
        }




	sta_assert_error ( num_scales==num_alphas_img_smooth );

        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"override_scales" ).data;
            min_scale=scales[0]*scale_correction;
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"override_scales" ).dim[0]-1]*scale_correction;
        } catch ( mhs::STAError error ) {
            min_scale=scales[0];
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
        }
    }

    void set_params ( const mxArray * params=NULL ) {
        scale_power=2;
        DataScale=1;
        DataThreshold=1;
        StdvEpsilon=0.001;
	DistScale=1;
	DataGradScale=1;
	
	Opt_Spine_dist=1;

        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"Epsilon" ) !=-1 ) {
                    StdvEpsilon=mhs::mex_getParam<T> ( params,"Epsilon",1 ) [0];
		}
		
		if ( mhs::mex_hasParam ( params,"Opt_Spine_dist" ) !=-1 ) {
                    Opt_Spine_dist=mhs::mex_getParam<T> ( params,"Opt_Spine_dist",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DistScale" ) !=-1 ) {
                    DistScale=mhs::mex_getParam<T> ( params,"DistScale",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataGradScale" ) !=-1 ) {
                    DataGradScale=mhs::mex_getParam<T> ( params,"DataGradScale",1 ) [0];
                }
                
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:



public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
      //,const Points<T,Dim> *org_point=NULL
	
    ) {



        try {

	  
	 
	  
            Vector<T,Dim> position=position_org;
            position/=this->element_size;


            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->feature_shape[0] ) || ( Y+1>=this->feature_shape[1] ) || ( X+1>=this->feature_shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }

            T org_scale=radius;

            radius*=scale_correction;

            if ( ( radius<min_scale-0.01 )
                    || ( radius>max_scale+0.01 ) ) {
                printf ( "[%f %f %f]\n",min_scale,radius,max_scale );
                sta_assert_error ( ! ( radius<min_scale ) );
                sta_assert_error ( ! ( radius>max_scale ) );
            }


            radius=std::min ( max_scale,std::max ( radius,min_scale ) );
	    
	    T multi_scale=radius;

            const T boundary_scale=2;
            if ( ( scale_dependend_boundary ) &&
                    ( ( position[0]+boundary_scale*org_scale>=this->feature_shape[0] ) ||
                      ( position[1]+boundary_scale*org_scale>=this->feature_shape[1] ) ||
                      ( position[2]+boundary_scale*org_scale>=this->feature_shape[2] ) ||
                      ( position[0]-boundary_scale*org_scale<0 ) ||
                      ( position[1]-boundary_scale*org_scale<0 ) ||
                      ( position[2]-boundary_scale*org_scale<0 ) ) ) {
                return false;
            }
            
            	    
	    T dist=0;
	    if ( !data_interpolate<T,T,TData> ( position,dist,dist_trafo, feature_shape.v ) ) {
                return false;
            };
	    
	    if (dist>max_dist)
	    {
	     return false; 
	    }
	    
// 	    if (p_point!=NULL)
// 	    {
// 	      class Points<T,Dim> * p=(class Points<T,Dim> *)p_point;
// 	      Vector<T,3> pos;
// 	      Vector<T,3> dir;
// 	      T tmp0,tmp1;
// 	      for (int d=0;d<3;d++)
// 	      {
// 			
// 			pos=position;
// 			pos[d]-=1;
// 			if ( !data_interpolate_polynom ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,tmp0 ,feature_shape.v ) ) {
//                                         return false;
//                         }
//                         pos[d]+=2;
// 			if ( !data_interpolate_polynom ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,tmp1,feature_shape.v ) ) {
//                                         return false;
//                         }
//                         dir[d]=tmp1-tmp0;
// 	      }
// 	      dir.normalize();
// 	      p->set_direction(dir);
// 	    }
	    
	    
            T stdv=0;
            if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
                return false;
            };
            stdv= ( std::max ( stdv,T ( 0 ) ) );
	    
// 	    T mean=0;
//             if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_mean, num_alphas_local_mean,mean,feature_shape.v ) ) {
//                 return false;
//             };
//             


	    T spine_filter=0;
	    T spine_grad=0;
// 	    if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,img_smooth, num_alphas_img_smooth,spine_filter,feature_shape.v ) ) {
//                 return false;
//             };
	    
	    
	    {
                {
                    T h=std::max ( radius/T ( 2 ),T ( 1 ) );
                    //T h=1;
                    T hz=h;
                    T hy=h;
                    T hx=h;
		    
		    //if (directional_weights)
		    {
		      hz/=element_size[0];
		      hy/=element_size[1];
		      hx/=element_size[2];
		    }
		    
                    int count=0;
                    Vector<T,3> pos;
                    bool valid[3];
                    T img_patch[3][3][3];
                    for ( int z=0; z<3; z++ ) {
                        valid[0] = ( z==1 );
                        pos[0]=position[0]+ ( z-1 ) *hz;
                        for ( int y=0; y<3; y++ ) {
                            valid[1] = ( y==1 );
                            pos[1]=position[1]+ ( y-1 ) *hy;
                            for ( int x=0; x<3; x++ ) {
                                img_patch[x][y][z]=0;
                                valid[2] = ( x==1 );
                                if ( valid[0]||valid[1]||valid[2] ) {
                                    count++;
                                    pos[2]=position[2]+ ( x-1 ) *hx;
                                    if ( !data_interpolate_polynom ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,img_patch[x][y][z] ,feature_shape.v ) ) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }


                    T dxx= ( img_patch[0][1][1]-2*img_patch[1][1][1]+img_patch[2][1][1] );
                    T dyy= ( img_patch[1][0][1]-2*img_patch[1][1][1]+img_patch[1][2][1] );
                    T dzz= ( img_patch[1][1][0]-2*img_patch[1][1][1]+img_patch[1][1][2] );

                    spine_filter=dxx+dyy+dzz;
                    
                    T N=radius*radius/ ( h*h );
		    
		    spine_filter=-N*(dxx+dyy+dzz);
		    
		    T dx= ( img_patch[0][1][1]-img_patch[2][1][1] );
                    T dy= ( img_patch[1][0][1]-img_patch[1][2][1] );
                    T dz= ( img_patch[1][1][0]-img_patch[1][1][2] );
		    
		    N=radius/ ( 2*h );
		    
		    spine_grad=std::sqrt(dx*dx+dy*dy+dz*dz)*N;
		    
                    sta_assert_error ( count==19 );
                }
            }

            
	    

	    
            T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=org_scale;
                break;
            case 2:
                scale1=org_scale*org_scale;
                break;
            case 3:
                scale1=org_scale*org_scale*org_scale;
                break;
            }
            

            dist=std::abs(Opt_Spine_dist-dist);

#ifdef D_USE_GUI
            if ( debug!=NULL ) {
                std::stringstream s;
                s.precision ( 3 );
                s<<"------------------";
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<" LAP : "<<spine_filter<<" N:" <<(spine_filter)*stdv;
                debug->push_back ( s.str() );
		
		s.str ( "" );
                s<<" GAR : "<<spine_grad<<" N:" <<(spine_grad)*stdv;
                debug->push_back ( s.str() );
		
		s.str ( "" );
                s<<" Dist : "<<dist;
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<"sdv :"<<stdv;
                debug->push_back ( s.str() );
		
            }
#endif

	     

	      //result=-DataScale*stdv*(spine_filter-mean)*scale1+DistScaledist;
	    result=-((DataScale*spine_filter-DataGradScale*spine_grad)*stdv+DataThreshold-DistScale*dist)*scale1;


            

        } catch ( mhs::STAError error ) {
            throw error;
        }

        return true;
    }
};

#endif
