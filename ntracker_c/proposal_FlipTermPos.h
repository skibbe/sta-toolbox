#ifndef PROPOSAL_FLIP_POS_H
#define PROPOSAL_FLIP_POS_H
#include "proposals.h"

 template<typename TData,typename T,int Dim> class CProposal;



template<typename TData,typename T,int Dim>
class CProposalFlipTerms : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
    T ConnectEpsilon;

public:
  
  T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_edges(num_edges,
		      num_particles,
		      num_connected_particles,		      
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }



    CProposalFlipTerms(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
	
	    
    }
    
    ~CProposalFlipTerms()
    {

    }
    

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_FLIP_TERM);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_FLIP_TERM);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
    }
    

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;
	
		
		
			
	
	try {		 
	  
	      if (connections.get_num_terminals()>0)
              {

		  Points<T,Dim> &  track_point=connections.get_rand_point();
		  if ((track_point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT))
				return false;


		  std::size_t found;
		  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
		  class OctPoints<T,Dim> ** candidates;
		  candidates=query_buffer;

		  try {
		      conn_tree.queryRange(
			  track_point.get_position(),
			  collision_search_rad,
			  candidates,
			  query_buffer_size,
			  found);
		  } catch (mhs::STAError & error)
		  {
		      throw error;
		  }
		  
		  if (found<1)
		    return false;
		  
		  class Points< T, Dim >* pt_candidates_buffer[query_buffer_size];
		  class Points< T, Dim >** pt_candidates=pt_candidates_buffer;
		  
// 		  T connet_cost[query_buffer_size];
// 		  T connet_prop[query_buffer_size];
		  T pt_prop_acc[query_buffer_size];
		  class Points<T,Dim> * pt_query_buffer[query_buffer_size];
		  
		  std::size_t num_pt_candidates=0;
		  {

		      {
			  for (std::size_t a=0; a<found; a++)
			  {
			      Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
			      
			      if (point==&track_point)
				continue;
			      
			      bool is_terminal=(point->get_num_connections()==1);
			      
// 			      if ((!is_terminal)||(point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)||(point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))
// 				continue;
			      if ((!is_terminal)||(point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT))
				continue;
			      
			      
			      
			      T rating=T(1)/((point->get_position()-track_point.get_position()).norm2()+T(0.1));
			      if (num_pt_candidates==0)
			      {
				pt_prop_acc[0]=rating;
			      }else
			      {
				pt_prop_acc[num_pt_candidates]=pt_prop_acc[num_pt_candidates-1]+rating;
			      }
			      pt_query_buffer[num_pt_candidates]=point;
			      
			      num_pt_candidates++;
			  }
		      }
		  }
		  
		  
		   if (num_pt_candidates<1)
		    return false;
		   
		   
		 std::size_t pt_candidate=rand_pic_array(pt_prop_acc,num_pt_candidates,pt_prop_acc[num_pt_candidates-1]);
		 
		 sta_assert_error(pt_candidate<num_pt_candidates);
		 
		 class Points< T, Dim >* change_pts[2];
		 change_pts[0]=&track_point;
		 change_pts[1]=pt_query_buffer[pt_candidate];
		 
		 
		  
		  Vector<T,Dim> new_pos[2];
		  Vector<T,Dim> old_pos[2];
		  
	
		 
		 T old_edge_cost=0;
		 T old_point_costs[2];
		 T old_saliency[2];
		 T particle_interaction_cost_old=0;
		 for (int i=0;i<2;i++)
		 {
		   
		   class Points< T, Dim > & point=*change_pts[i];
		 
		 
		 
		    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		    old_edge_cost+=point.e_cost(edgecost_fun,
					  options.connection_bonus_L,
					  options.bifurcation_bonus_L,
					  inrange);
	    
		    sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),
		      (point.e_cost(edgecost_fun,
					  options.connection_bonus_L,
					  options.bifurcation_bonus_L,
					  inrange,true,true))
		    );
		    
		    
		    old_point_costs[i]=point.point_cost;
		    old_saliency[i]=point.saliency;
		    
		    
		    T temp_fact_pos=point.get_scale();
		    new_pos[i].rand_normal(temp_fact_pos);
		    
		    old_pos[i]=point.get_position();
		    
		    if (collision_fun_p.is_soft())    
		    {
		      T tmp=0;
		      sta_assert_error(!collision_fun_p.colliding(tmp, tree,point,collision_search_rad,temp));
		      particle_interaction_cost_old+=tmp;
		    }
		 }
		 
		 Vector<T,Dim> tmp;
		 tmp=change_pts[0]->get_position();
		 change_pts[0]->set_position(change_pts[1]->get_position()+new_pos[0]);
		 change_pts[1]->set_position(tmp+new_pos[1]);
		 
		 
		  if (!change_pts[0]->update_pos(true))
		  {
		    change_pts[0]->set_position(old_pos[0]);
		    change_pts[1]->set_position(old_pos[1]);
		    return false;
		  }
		  
		  sta_assert_error(change_pts[0]->update_pos());
		  
		  if (!change_pts[1]->update_pos(true))
		  {
		    change_pts[0]->set_position(old_pos[0]);
		    sta_assert_error(change_pts[0]->update_pos());
		    change_pts[1]->set_position(old_pos[1]);
		    return false;
		  }
		  
		  sta_assert_error(change_pts[1]->update_pos());
		  
		  
		 
		 
		 T new_edge_cost=0;
		 T new_point_costs[2];
		 T new_saliency[2];
		 T particle_interaction_cost_new=0;
		 for (int i=0;i<2;i++)
		 {
		   
		   class Points< T, Dim > & point=*change_pts[i];
		 
		 
		    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		    new_edge_cost+=point.e_cost(edgecost_fun,
					  options.connection_bonus_L,
					  options.bifurcation_bonus_L,
					  inrange);

		    if (!(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)))
		    {
		     change_pts[0]->set_position(old_pos[0]);
		     sta_assert_error(change_pts[0]->update_pos());
		     change_pts[1]->set_position(old_pos[1]);
		     sta_assert_error(change_pts[1]->update_pos());
		      return false;
		    }
		    
		    
		    
		  T newpointsaliency=0;
		  if ((!data_fun.saliency_get_value(newpointsaliency,point.get_position()))||((newpointsaliency<std::numeric_limits<T>::min())))
		  {
				 change_pts[0]->set_position(old_pos[0]);
				sta_assert_error(change_pts[0]->update_pos());
				change_pts[1]->set_position(old_pos[1]);
				sta_assert_error(change_pts[1]->update_pos());
				  return false;
		  }
			  
			  
			  
		  		
			  T newenergy_data;
			  if (!data_fun.eval_data(
				newenergy_data,
				point))
				{
				change_pts[0]->set_position(old_pos[0]);
				sta_assert_error(change_pts[0]->update_pos());
				change_pts[1]->set_position(old_pos[1]);
				sta_assert_error(change_pts[1]->update_pos());
				  return false;
				  }

		    new_saliency[i]=newpointsaliency;
		    new_point_costs[i]=newenergy_data;
		    
		    
		  T tmp=0;
		  if (collision_fun_p.colliding(tmp, tree,point,collision_search_rad,temp))
		  {
		   change_pts[0]->set_position(old_pos[0]);
				sta_assert_error(change_pts[0]->update_pos());
				change_pts[1]->set_position(old_pos[1]);
				sta_assert_error(change_pts[1]->update_pos());
				  return false;
		  }
		  particle_interaction_cost_new+=tmp;
		 }
		 
		 
		 
		 
		  
		  T E=(new_point_costs[0]+new_point_costs[1]-(old_point_costs[0]+old_point_costs[1]))/temp;
		  E+=(new_edge_cost-old_edge_cost)/temp;	
		  
		  
		  if (collision_fun_p.is_soft())   
		  {
		      E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
		  }

		  T R=mhs_fast_math<T>::mexp(E);
	  
	  
		  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
		  {
		    
		      #ifdef  D_USE_GUI
			  change_pts[0]->touch(get_type());
			  change_pts[1]->touch(get_type());
		      #endif
		    
			  
		      change_pts[0]->point_cost=new_point_costs[0];  
		      change_pts[1]->point_cost=new_point_costs[1];
		      
		      change_pts[0]->saliency=new_saliency[0];  
		      change_pts[1]->saliency=new_saliency[1];  
		      
		    
		      this->tracker->update_energy(E,static_cast<int>(get_type()));
		      this->proposal_accepted++;
			
		      return true;
		  }
		  
		  
		   change_pts[0]->set_position(old_pos[0]);
		     sta_assert_error(change_pts[0]->update_pos());
		     change_pts[1]->set_position(old_pos[1]);
		     sta_assert_error(change_pts[1]->update_pos());
		      return false;
	  
	  }
	  
	} catch (mhs::STAError error)
	{
	    throw error;
	}
	
	return false;
	
    }
    


};

#endif


