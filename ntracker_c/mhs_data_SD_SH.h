#ifndef MHS_DATA_SD_SH_H
#define MHS_DATA_SD_SH_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataSDSH: public CData<T,TData,Dim>
{
private:
  
   
  
  
    int scale_power;

    const TData * scales;
    std::size_t num_scales;

    const TData * local_stdv;
    int num_alphas_local_stdv;
    
    
    
    const TData * local_mean;
    int num_alphas_local_mean;

    //const TData * sd_data;
    int num_samples_sd_data;
    int order_sd_data;
    int components_sd_data;
    int component_tensor;
    std::vector<Cpyramid_img<T,TData>> pyramid;
    
    
    

    Vector<T,Dim> boundary;
    bool scale_dependend_boundary;


    
    T min_scale;
    T max_scale;
    T scale_correction;
    

    T DataThreshold;
    T DataScale;
    T StdvEpsilon;
    
    T wxx;
    T wyy;
    T wzz;
    T wxy;
    T wxz;
    T wyz;

    
    Vector<T,3> element_size;
    Vector<std::size_t ,3> feature_shape;
    
    
    
    

public:

    float get_minscale() {
        return min_scale/scale_correction;
    };

    float get_maxscale() {
        return max_scale/scale_correction;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataSDSH() {
    }

    ~CDataSDSH() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
//                 DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
//                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
//            printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
//        printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
    }
#endif

    void init ( const mxArray * feature_struct ) {

        mhs::dataArray<TData>  tmp0;
        mhs::dataArray<TData>  tmp1_pyramid;
        mhs::dataArray<TData>  tmp2;
        mhs::dataArray<TData>  tmp3;
	mhs::dataArray<TData>  tmp4;
	mhs::dataArray<TData>  tmp5;
	
	
        try {
            tmp3=mhs::dataArray<TData> ( feature_struct,"shape_boundary" );
            sta_assert_error ( tmp3.get_num_elements() ==3 );
            boundary[0]=tmp3.data[0];
            boundary[1]=tmp3.data[1];
            boundary[2]=tmp3.data[2];
            scale_dependend_boundary=true;
        } catch ( mhs::STAError error ) {
            boundary[0]=boundary[1]=boundary[2]=T ( 0 );
            scale_dependend_boundary=false;
        }
        
        try {
	  
	    const mxArray *pyramid_array= mxGetField(feature_struct,0,"pyramid");
	    sta_assert_error(pyramid_array!=NULL);
	    sta_assert_error(mxIsStruct(pyramid_array));
	    num_samples_sd_data=mxGetNumberOfElements(pyramid_array);
	    std::size_t max_voxel=0;
	    pyramid.resize(num_samples_sd_data);
	    
	    for (std::size_t s=0;s<num_samples_sd_data;s++)
	    {
	      printf("%d %d\n",s,num_samples_sd_data);
	      //pyramid_array= mxGetField(feature_struct,s,"pyramid");
	      sta_assert_error(pyramid_array!=NULL);
	      tmp1_pyramid=mhs::dataArray<TData > ( pyramid_array,"sd_data",s);
	      if (s==0)
	      {
		  for ( int i=0; i<3; i++ ) {
		    feature_shape[i]=tmp1_pyramid.dim[i];
		  }
		  components_sd_data=tmp1_pyramid.dim[tmp1_pyramid.dim.size()-1];
		  max_voxel=tmp1_pyramid.get_num_elements();
	      }
	      pyramid[s].sd_data=tmp1_pyramid.data;
	      printf("POINTER: %u",pyramid[s].sd_data);
	      
	      
	      for ( int i=0; i<3; i++ ) {
		pyramid[s].shape[i]=tmp1_pyramid.dim[i];
	      }
	      
	      std::swap(pyramid[s].shape[0],pyramid[s].shape[2]);
	      
	      pyramid[s].shape.print();
	      
	      pyramid[s].scaling_fact=*(mhs::dataArray<TData > ( pyramid_array,"scale_fact" ,s).data);
		  
// 		  Vector<std::size_t ,3> shape;
// 		  T scaling_fact;
// 		  const TData * sd_data;
	      
            
            
	    }
	    
//             tmp1=mhs::dataArray<TData > ( feature_struct,"sd_data" );
//             sd_data=tmp1.data;
//             num_samples_sd_data=tmp1.dim[tmp1.dim.size()-1];
//             printf ( "sd data samples: %d \n",num_samples_sd_data );
// 	    components_sd_data=tmp1.dim[tmp1.dim.size()-2];
	    
	    tmp5=mhs::dataArray<TData > ( feature_struct,"L" );
	    order_sd_data=*tmp5.data;
	    component_tensor=((order_sd_data+2)*(order_sd_data+2))/4;
	    
	    printf ( "sd order: %d (components %d, components tensor %d)\n",order_sd_data,components_sd_data,component_tensor);

            tmp2=mhs::dataArray<TData> ( feature_struct,"alphas_sdv" );
            local_stdv=tmp2.data;
            num_alphas_local_stdv=tmp2.dim[tmp2.dim.size()-1];
            printf ( "local stdv pol degree: %d \n",num_alphas_local_stdv-1 );

           sta_assert_error ( tmp2.get_num_elements() /num_alphas_local_stdv==max_voxel /((components_sd_data)) );
	   
	    tmp4=mhs::dataArray<TData> ( feature_struct,"alphas_mean" );
            local_mean=tmp4.data;
            num_alphas_local_mean=tmp4.dim[tmp4.dim.size()-1];
            printf ( "local mean pol degree: %d \n",num_alphas_local_mean-1 );

           sta_assert_error ( tmp4.get_num_elements() /num_alphas_local_mean==max_voxel /((components_sd_data)) );

            
        } catch ( mhs::STAError error ) {
            throw error;
        }


        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

        std::swap ( this->shape[0],this->shape[2] );

        for ( int i=0; i<3; i++ ) {
            element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
        }

        printf ( "element size data term:" );
        element_size.print();

        scale_correction=1;
        try {
            scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
        } catch ( mhs::STAError error ) {
            throw error;
        }




	sta_assert_error ( num_scales==num_samples_sd_data );

        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"override_scales" ).data;
            min_scale=scales[0]*scale_correction;
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"override_scales" ).dim[0]-1]*scale_correction;
        } catch ( mhs::STAError error ) {
            min_scale=scales[0];
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
        }
    }

    void set_params ( const mxArray * params=NULL ) {
        scale_power=2;
        DataScale=1;
        DataThreshold=1;
        StdvEpsilon=0.001;

        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"Epsilon" ) !=-1 ) {
                    StdvEpsilon=mhs::mex_getParam<T> ( params,"Epsilon",1 ) [0];
                }
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:



public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
      //,const Points<T,Dim> *org_point=NULL
	
    ) {


        try {

	  
// 	  T bla=mhs_fast_math<T>::plm(1,2,0);
// 	  printf("%f\n",bla);
	  
            Vector<T,Dim> position=position_org;
            position/=this->element_size;


            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->feature_shape[0] ) || ( Y+1>=this->feature_shape[1] ) || ( X+1>=this->feature_shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }
            


            T org_scale=radius;

            radius*=scale_correction;

            if ( ( radius<min_scale-0.01 )
                    || ( radius>max_scale+0.01 ) ) {
                printf ( "[%f %f %f]\n",min_scale,radius,max_scale );
                sta_assert_error ( ! ( radius<min_scale ) );
                sta_assert_error ( ! ( radius>max_scale ) );
            }


            radius=std::min ( max_scale,std::max ( radius,min_scale ) );

            const T boundary_scale=2;
            if ( ( scale_dependend_boundary ) &&
                    ( ( position[0]+boundary_scale*org_scale>=this->feature_shape[0] ) ||
                      ( position[1]+boundary_scale*org_scale>=this->feature_shape[1] ) ||
                      ( position[2]+boundary_scale*org_scale>=this->feature_shape[2] ) ||
                      ( position[0]-boundary_scale*org_scale<0 ) ||
                      ( position[1]-boundary_scale*org_scale<0 ) ||
                      ( position[2]-boundary_scale*org_scale<0 ) ) ) {
                return false;
            }

         

            T stdv=0;
            if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
                return false;
            };
            stdv= ( std::max ( stdv,T ( 0 ) ) );
	    
	    T mean=0;
            if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_mean, num_alphas_local_mean,mean,feature_shape.v ) ) {
                return false;
            };
	    
	    
            


	    std::complex<T> tensor[component_tensor]; 
	    {
	      T * tensor_p=(T*)tensor; 
// 	    for ( int i=0; i<components_sd_data; i++ ) {
  // 		if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[i],feature_shape.v,components_sd_data,i ) ) {
  // 		    return false;
  // 		}
  // 	    }
	      
// 	      Vector<T,3> position(31.31,31.32,31.45);
// 	      T radius=scales[0];
	      std::size_t count=0;
	      std::size_t count_data=0;
	      for (int l=0;l<=order_sd_data;l+=2)
	      {
		for (int m=-l;m<=0;m++)
		{
		  sta_assert_debug0(count_data<components_sd_data);
		  sta_assert_debug0(count<2*component_tensor);
// 		    if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++) ) {
// 			return false;
// 		    }
		  if ( !data_interpolate_multiscale<T,T,TData> (position,radius,scales,pyramid,tensor_p[count],components_sd_data,count_data++) ) {
			return false;
		   }
		  count++;
		  
		  sta_assert_debug0(count<2*component_tensor);
		  
		  if (m!=0)
		  {
		  
		    sta_assert_debug0(count_data<components_sd_data);
// 		    if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++ ) ) {
// 			return false;
// 		    }
		    if ( !data_interpolate_multiscale<T,T,TData> (position,radius,scales,pyramid,tensor_p[count],components_sd_data,count_data++) ) {
			return false;
		    }
		  }else
		  {
		    tensor_p[count]=0;
		  }
		  count++;
		    
		}
	      }
// 	      for (int a=0;a<component_tensor;a++)
// 	      {
// 		printf("[%f %f]\n",tensor[a].real(),tensor[a].imag());
// 	      }
// 	      sta_assert_debug0(false);
	      
//   	      printf("%d %d\n",count_data,count);
	    }
	    
	    
	    
	    T sd_filter=0;
	     std::complex<T> tmp;
	    {
	      const T & x=direction[2];
	      const T & y=direction[1];
	      const T & z=direction[0];
	    
	      //std::complex<T> x_myi=std::complex<T>(x,-y);
	      std::complex<T> x_myi=std::complex<T>(x,y); //contravariant
	      std::complex<T> x_myi2=x_myi*x_myi;
	
	      T x2=x*x;
	      T x4=x2*x2;
	      T y2=y*y;
	      T y4=y2*y2;
	      T z2=z*z;
	      T z4=z2*z2;
	      
	      tmp=tensor[0];
	      
	      if (order_sd_data>=2)
	      {
		const T l_weight=2*2+1;
		tmp+=T(l_weight*std::sqrt(3.0/2.0))*(x_myi2 * tensor[1]);
		tmp+=T(l_weight*(std::sqrt(6.0)*z)) *(x_myi*tensor[2]);
		tmp+=T(l_weight*((-x2-y2)/2+z2))*tensor[3];
	      }
// 	      
	      if (order_sd_data>=4)
	      {
		const T l_weight=4*2+1;
		tmp+=T(l_weight*1.0/4.0 * std::sqrt(35.0/2.0))*(x_myi2*x_myi2*tensor[4]);
		tmp+=T(l_weight*1.0/2.0 * std::sqrt(35.0))*(x_myi2*x_myi*z*tensor[5]);
		tmp+=T(l_weight*-1.0/2.0 * std::sqrt(5.0/2.0)*(x2 + y2 - 6*z2))*(x_myi2*tensor[6]);
		tmp+=T(l_weight*-1.0/2.0*std::sqrt(5.0) * z *(3 * x2+ 3 * y2 - 4 * z2)) *(x_myi*tensor[7]);
		tmp+=T(l_weight*(3* (x4)/8 +  3* (y4)/8 - 3* y2 * z2 + z4 + 3.0/4.0 * x2 *(y2 - 4 * z2)))*tensor[8];
	      }
	      
	     sd_filter=tmp.real();
	    }
	
	    

            T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=org_scale;
                break;
            case 2:
                scale1=org_scale*org_scale;
                break;
            case 3:
                scale1=org_scale*org_scale*org_scale;
                break;
            }
            

#ifdef D_USE_GUI
            if ( debug!=NULL ) {
                std::stringstream s;
                s.precision ( 3 );
                s<<"------------------";
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<" SD : "<<sd_filter<<" N:" <<(sd_filter-mean)*stdv;
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<"sdv :"<<stdv<< "mean: "<<mean;
                debug->push_back ( s.str() );
		
		s.str ( "" );
                s<<"r :"<<tmp.real()<< " i: "<<tmp.imag();
                debug->push_back ( s.str() );
		
            }
#endif

	      result=-DataScale*stdv*(sd_filter-mean)*scale1;

        } catch ( mhs::STAError error ) {
            throw error;
        }

        return true;
    }
};

#endif
