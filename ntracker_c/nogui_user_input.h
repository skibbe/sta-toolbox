#ifndef NOGUI_USER_INPUT_H
#define NOGUI_USER_INPUT_H

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include "global.h"

class C_nogui_command
{
  private:
    inline bool exists(const std::string& name) {
    struct stat buffer;   
      return (stat (name.c_str(), &buffer) == 0); 
      }
    
    std::string get_env_var(std::string varname)
    {
      char * sysenv=getenv ( varname.c_str());
      if ( sysenv!=NULL )
      {
	  return sysenv;
      }
      throw mhs::STAError("couldn't resolve home directory\n ");
    }
    
    
    std::string filename;
  public:
    
    C_nogui_command()
    {
      
      std::size_t id=0;  
      
      std::size_t test=0;
      
      while ((exists(filename) || (filename.length()==0))&&(test<100000))
      {
	std::stringstream full_filename;
	
	test++;
	if (!(test<100000))
	{
	  printf("for some reasons there are soo many lock files - I gave up.\n");
	}
	
	try {
	 full_filename<<get_env_var("HOME")<<"/.tracker_nogui"<<id++<<".lock"; 
	}catch  (mhs::STAError & error)
	{
	 full_filename<<application_directory<<"/.tracker_nogui"<<id++<<".lock";
	}
	
	
	filename=full_filename.str();
      }
      
      
      
//       printf("lockfile :%s\n",filename.c_str());
      std::ofstream myfile(filename,std::ofstream::out);
      if (myfile.is_open ())
      {
	myfile << "0";
	myfile.close();
	printf("created lock file %s\n",filename.c_str());
      }else
      {
	printf("could'nt create lock file\n");
      }
      
    }
    
    bool has_been_terminated()
    {
      std::ifstream myfile(filename);
      
      bool terminate=false;
      if (myfile.is_open ())
      {
	std::string line;
	while ( getline (myfile,line) )
	{
	  //printf("termiante? %s\n",line.c_str());
	  
	  if ((line.compare("1") == 0))
	  {
	    terminate=true;
	  }
	}
	
	myfile.close();
      }else
      {
	printf("could'nt create lock file\n");
      }
      if (!terminate)
      {
	printf("[ echo \"1\" > %s  ] will abort  tracking\n",filename.c_str());
      }else
      {
	printf("aborting tracking....\n");
      }
      
      return terminate;
    }
    
    
    ~C_nogui_command()
    {
      //system.
      if( remove( filename.c_str() ) != 0 )
	perror( "Error deleting file" );
      else
	puts( "File successfully deleted" );
	}
  
};

#endif

