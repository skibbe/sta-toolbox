#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>
#include "EigDecomp3x3.h"


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"

#define EPSILON 0.00000000000001


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   

    
     const mxArray *SalTensor;
    SalTensor = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(SalTensor);
    const int *dims = mxGetDimensions(SalTensor);

    printf("numdim %d\n",numdim);
// if (numdim==3)
// {
//   sta_assert(false);
//     int numdir=32;
//     bool symmetric=false;
//   
//     if (nrhs>1)
//     {
//         const mxArray * params=prhs[nrhs-1] ;
// 
//         if (mhs::mex_hasParam(params,"numdir")!=-1)
//             numdir=mhs::mex_getParam<int>(params,"numdir",1)[0];
// 	
//         if (mhs::mex_hasParam(params,"symmetric")!=-1)
//             symmetric=mhs::mex_getParam<bool>(params,"symmetric",1)[0];	
//     }
//     
//     if ((numdir%2==1)&&(symmetric))
//       symmetric=false;  
//   
//   
//   
//     int shape[2];  
//     shape[0] = dims[2];
//     shape[1] = dims[1];
//     T *saltensor = (T*) mxGetData(SalTensor);
//     if (dims[0]!=3)
//       mexErrMsgTxt("error: first dim must be 3\n");
//  
//      T Numdir=numdir;
//     if (symmetric)
//       numdir/=2;   
//     
//     
//     
//     int ndims[3];
//     ndims[0]=numdir;
//     ndims[1]=dims[1];
//     ndims[2]=dims[2];
//     plhs[0] = mxCreateNumericArray(3,ndims,mxGetClassID(SalTensor),mxREAL);
//     T *ofield = (T*) mxGetData(plhs[0]);
//     
//     
// 
//     
//     T  *directions= new T[numdir*2];
//     
//     std::complex<T> w=exp(std::complex<T>(0,2*M_PI)/(T)(Numdir));
//     
//     for (int i=0;i<numdir;i++)
//     {
//       std::complex<T> w1=std::pow(w,i);
//       printf("%f %f \n",w1.real(),w1.imag());
//       directions[2*i]=w1.real();
//       directions[2*i+1]=w1.imag();
//     }
//     T *stensor=saltensor;
// 
// 		std::size_t numv=shape[0]*shape[1];
// 		  
// 		  for (std::size_t idx= 0; idx < numv; idx++)    
// 		  {
// 			T & a=  stensor[0]; //xx
// 			T & c=  stensor[1]; //yy
// 			T & b=  stensor[2]; //xy
// 			
// 			
// 			for (int i=0;i<numdir;i++)
// 			{
// 			  ofield[i]=directions[2*i]*(a*directions[2*i]+b*directions[2*i+1])
// 			  +directions[2*i+1]*(b*directions[2*i]+c*directions[2*i+1]);
// 			  //ofield[2*i+1]=b*directions[2*i]+c*directions[2*i+1];
// 			}
// 			
// 			ofield+=numdir;
// 			stensor+=3;
// 		}
// 		
// 		delete [] directions;
// }


if (numdim==4)
{
    mhs::dataArray<T>  directions;
//     T * grad_weights=NULL;
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

	if (mhs::mex_hasParam(params,"dirs")!=-1)
	  directions=mhs::dataArray<T>(mhs::mex_getParamPtr(params,"dirs"));
	
// 	mhs::dataArray<T>  grad_weight;
// 	if (mhs::mex_hasParam(params,"grad")!=-1)
// 	{
// 	  grad_weight=mhs::dataArray<T>(mhs::mex_getParamPtr(params,"grad"));
// 	  //if (grad_weight.dim[grad_weight.dim.size()-1]!=6)
// // 	  for (int i=0;i<grad_weight.dim.size();i++)
// // 	    printf("%d\n",grad_weight.dim[i]);
// 	  if (grad_weight.dim[grad_weight.dim.size()-1]!=6)
// 	    mexErrMsgTxt("error: first dim must be 6\n");
// 	  grad_weights=grad_weight.data;
// 	  printf("using gradient weights!\n");
// 	}
// 	
	
    }
    for (int i=0;i<directions.dim.size();i++)
      printf("%d\n",directions.dim[i]);
    

    
    sta_assert(directions.data!=NULL);
    sta_assert(directions.dim.size()==2);
    sta_assert(directions.dim[1]==3);
    
    int shape[3];  
    shape[0] = dims[3];
    shape[1] = dims[2];
    shape[2] = dims[1];
    T *saltensor = (T*) mxGetData(SalTensor);
    if (dims[0]!=6)
      mexErrMsgTxt("error: first dim must be 6\n");
 
    
    int numdir=directions.dim[0];
    
    
    
    
    int ndims[4];
    ndims[0]=numdir;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    ndims[3]=dims[3];
    plhs[0] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
    T *ofield = (T*) mxGetData(plhs[0]);
    
    T *stensor=saltensor;

    printf("numdir %d\n",numdir);
   
    
		std::size_t numv=shape[0]*shape[1]*shape[2];
	
		
	  
		
		  for (std::size_t idx= 0; idx < numv; idx++)    
		  {
// 			T & xx=  stensor[0]; //xx
// 			T & xy=  stensor[3]; //xy
// 			T & xz=  stensor[4]; //xz
// 			T & yy=  stensor[1]; //yy
// 			T & yz=  stensor[5]; //yz
// 			T & zz=  stensor[2]; //zz
		    
			T & xx=  stensor[0]; //xx
			T & xy=  stensor[3]; //xy
			T & xz=  stensor[5]; //xz
			T & yy=  stensor[1]; //yy
			T & yz=  stensor[4]; //yz
			T & zz=  stensor[2]; //zz
			
		    
/*			T & xx=  stensor[0]; //xx
			T & xy=  stensor[3]; //xy
			T & xz=  stensor[5]; //xz
			T & yy=  stensor[1]; //yy
			T & yz=  stensor[4]; //yz
			T & zz=  stensor[2]; //zz*/		    
			
			for (int i=0;i<numdir;i++)
			{
			  T & nx=directions.data[3*i];
			  T & ny=directions.data[3*i+1];
			  T & nz=directions.data[3*i+2];
			  
// 			  printf("%f %f %f\n",nx,ny,nz);
			
			  
			  ofield[i]=nx*nx*xx+ny*ny*yy+nz*nz*zz+
			   +2*nx*(ny*xy+nz*xz)
			   +2*ny*nz*yz;
			}
			
			
			
			
// 			  return;
// 			if (grad_weights!=NULL)
// 			{
// 			  T & xx=  grad_weights[0]; //xx
// 			  T & xy=  grad_weights[3]; //xy
// 			  T & xz=  grad_weights[4]; //xz
// 			  T & yy=  grad_weights[1]; //yy
// 			  T & yz=  grad_weights[5]; //yz
// 			  T & zz=  grad_weights[2]; //zz
// 			  
// 			  for (int i=0;i<numdir;i++)
// 			  {
// 			    T & nx=directions.data[3*i];
// 			    T & ny=directions.data[3*i+1];
// 			    T & nz=directions.data[3*i+2];
// 			    
// 			    ofield[i]*=1-(nx*nx*xx+ny*ny*yy+nz*nz*zz+
// 			    +2*nx*(ny*xy+nz*xz)
// 			    +2*ny*nz*yz);
// 			  }			  
// 			  grad_weights+=6;
// 			}
			
			
			ofield+=numdir;
			stensor+=6;
		}
}
    
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}