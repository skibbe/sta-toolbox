#ifndef OBSERVATION_GUI_H
#define OBSERVATION_GUI_H


#ifdef _WIN64
//#include "./win/glew/GL/glew.h"
#include "./win/glew/glew.c"
#endif


#define GL_GLEXT_PROTOTYPES
#include <SDL2/SDL.h>
#include "SDL2/SDL_opengl.h"
#include "GL/glu.h"
#include "GL/glext.h"
#include "mhs_vector.h"
#include "mhs_matrix.h"
#include "mhs_quat.h"
#include "ext/gl_font.h"





#ifdef _USE_CGC_
#include "mhs_gui_shader_old.h"
#else
#include "mhs_gui_shader_new.h"
#endif
#include "sta_mex_helpfunc.h"
#include "ext/ArcBall.h"

#include "global.h"



std::string shader_path=application_directory;
//std::string shader_path="/home/skibbe/projects/neuro_tracker/ntracker_c/";

//#include "SDL2/SDL_thread.h"


template<typename T>
void hsv2rgb ( T h, T s ,T v,T &r, T & g ,T & b )
{
    T      hh, p, q, t, ff;
    int        i;


    if ( s <= 0.0 ) {  
        r = v;
        g = v;
        b = v;
    }
    hh = h;
    if ( hh >= 360.0 ) {
        hh = 0.0;
    }
    hh /= 60.0;
    i = ( int ) hh;
    ff = hh - i;
    p = v * ( 1.0 - s );
    q = v * ( 1.0 - ( s * ff ) );
    t = v * ( 1.0 - ( s * ( 1.0 - ff ) ) );

    switch ( i ) {
    case 0:
        r = v;
        g = t;
        b = p;
        break;
    case 1:
        r = q;
        g = v;
        b = p;
        break;
    case 2:
        r = p;
        g = v;
        b = t;
        break;

    case 3:
        r = p;
        g = q;
        b = v;
        break;
    case 4:
        r = t;
        g = p;
        b = v;
        break;
//     case 5:
    default:
        r = v;
        g = p;
        b = q;
        break;
    }
}


template<typename T>
GLenum getFormat();

template<>
GLenum getFormat<float>()
{
    return GL_FLOAT;
};
template<>
GLenum getFormat<char>()
{
    return GL_BYTE;
};
template<>
GLenum getFormat<unsigned char>()
{
    return GL_UNSIGNED_BYTE;
};
template<>
GLenum getFormat<int>()
{
    return GL_INT;
};
template<>
GLenum getFormat<unsigned int>()
{
    return GL_UNSIGNED_INT;
};
template<>
GLenum getFormat<short>()
{
    return GL_SHORT;
};
template<>
GLenum getFormat<unsigned short>()
{
    return GL_UNSIGNED_SHORT;
};
template<>
GLenum getFormat<double>()
{
    return GL_DOUBLE;
};

// void  invertMat ( float mat[16] )
// {
//     float temp;
//
//     temp=mat[1];
//     mat[1]=mat[4];
//     mat[4]=temp;
//
//     temp=mat[2];
//     mat[2]=mat[8];
//     mat[8]=temp;
//
//     temp=mat[6];
//     mat[6]=mat[9];
//     mat[9]=temp;
//
//     mat[12]=-mat[12];
//     mat[13]=-mat[13];
//     mat[14]=-mat[14];
// }

bool PrintLastGLerror ( std::string & s )
{
    unsigned int a =glGetError();
    if ( a!=GL_NO_ERROR ) {
        const unsigned  char * e = gluErrorString ( a );
        std::stringstream tmp;
        tmp<<e;
        s=tmp.str();
        return true;
    }
    return false;

}

template<class T>
class CGLtext
{

private:
    bool  _fontLoaded;
    int  _fontoffset;
    int width;
    int height;
// 	      void  Createfont();
    void Createfont() {
        if ( _fontLoaded ) {
            return;
        }
        //std::cerr<<"init ogl-fonts"<<std::endl;
        glPixelStorei ( GL_UNPACK_ALIGNMENT,1 );
        _fontoffset = glGenLists ( 128 );
        for ( int i=32; i<127; i++ ) {
            glNewList ( i+_fontoffset,GL_COMPILE );
            glBitmap ( 8,13,0.0,2.0,10.0,0.0,rasterfont[i-32] );
            glEndList();
        }
        _fontLoaded=true;
    }
public:
    //       template<class T>
    //       CGLtext & operator<<(const T &data)
    //       {
    //          std::ostringstream os;
    //          os << data;
    //          _message += os.str();
    //          return *this;
    //       }
    template<class Data>
    int Stringoutxyz ( Data text,Vector<T,3>  position ) {
        std::stringstream s;
        s<<text;
        int slength;
        slength=s.str().length();//(int)std::strlen(thetext);
        glRasterPos3f ( position[0],position[1],position[2] );
        glPushAttrib ( GL_LIST_BIT );

        glListBase ( _fontoffset );
        std::string tmp ( s.str() );
//         const char * tmp=s.str().c_str();
        glCallLists ( slength,GL_UNSIGNED_BYTE, ( GLubyte* ) ( tmp.c_str() ) );
        glPopAttrib();
        return ( slength );
    }


    template<class Data>
    void StringoutLine ( Data text,int i=0 ) {
        i++;
        float pixel_offset_y=5+13;
        float pixel_offset_x=5+8;
        int maxlines=height/ ( pixel_offset_y );
        Vector<T,3>  position;
        position[0]= ( pixel_offset_x ) /width;
        position[1]= ( height- ( i*pixel_offset_y ) ) /height;
        position[2]=0;
        Stringoutxyz ( text,position );
    }

    int Stringou_getLastLine() {
        float pixel_offset_y=5+13;
        return std::floor ( height/ ( pixel_offset_y ) )-1;
    }


// 	      void XYZAxis ( float scale,blitz::TinyVector<float,3>  position, blitz::TinyVector<float,4>  color );

    CGLtext ( int width,int height ) {
        _fontLoaded=false;
        _fontoffset=0;
        Createfont();
        this->width=width;
        this->height=height;
    };


    float p2gl_w ( float x ) {
        return x/width;
        //position[1]=(height-(i*pixel_offset_y))/height;
    };

    float p2gl_h ( float y ) {
        return y/height;
        //position[1]=(height-(i*pixel_offset_y))/height;
    };

// 	      static void InitHUD ( int width, int height,
// 				    float left=-1.0f,float right=1.0f,
// 				    float bottom=-1.0f,float top=1.0f );
// 	      static void DeinitHUD();





    void InitHUD() /* int width, int height,
 				    float left=-1.0f,float right=1.0f,
 				    float bottom=-1.0f,float top=1.0f )*/
    {

// 			      float left=-1.0f;
// 			      float right=1.0f;
// 			      float bottom=-1.0f;
// 			      float top=1.0f;
        float left=0.0f;
        float right=1.0f;
        float bottom=0.0f;
        float top=1.0f;
        glDepthFunc ( GL_ALWAYS );
        glMatrixMode ( GL_PROJECTION );
        glPushMatrix();
        glLoadIdentity();
        glViewport ( 0, 0, ( GLint ) width, ( GLint ) height );
        gluOrtho2D ( left,right,bottom,top );
        //gluOrtho2D(-1,1,-1,1);
        glMatrixMode ( GL_MODELVIEW );
        glPushMatrix();
        glLoadIdentity();

        // 			glViewport(0, 0, (GLint)width, (GLint)height);
        // 			gluOrtho2D(0.0f, 1.0f, 1.0f, 0.0f);
        //gluOrtho2D(left,right,bottom,top);
        glColor3f ( 1.0f,1.0f,1.0f );
        glDisable ( GL_LIGHTING );
    }


    void DeinitHUD() {
        glMatrixMode ( GL_PROJECTION );
        glPopMatrix();
        glMatrixMode ( GL_MODELVIEW );
        glPopMatrix();
        glDepthFunc ( GL_LESS );
    }


    void XYZAxis ( float scale,Vector<T,3>  position, Vector<T,3>  color ) {
        glBegin ( GL_LINES );
        glColor3f ( 1.0f,0.0f,0.0f );
        glVertex3f ( -scale+position[0],position[1],position[2] );
        glVertex3f ( scale+position[0],position[1],position[2] );
        glColor3f ( 0.0f,1.0f,0.0f );
        glVertex3f ( position[0],-scale+position[1],position[2] );
        glVertex3f ( position[0],scale+position[1],position[2] );
        glColor3f ( 0.0f,0.0f,1.0f );
        glVertex3f ( position[0],position[1],-scale+position[2] );
        glVertex3f ( position[0],position[1],scale+position[2] );
        glEnd();
        //glColor3fv(1.0f,1.0f,1.0f);
        glColor3f ( color[0],color[1],color[2] );
        Stringoutxyz ( "X", Vector<T,3> ( scale+position[0],position[1],position[2] ) );
        Stringoutxyz ( "Y", Vector<T,3> ( position[0],scale+position[1],position[2] ) );
        Stringoutxyz ( "Z", Vector<T,3> ( position[0],position[1],scale+position[2] ) );
    }



};




#define mhs_check_gl \
  { std::string error;\
    if (PrintLastGLerror(error)) \
    {\
     std::stringstream s; \
     s<<"###########################################\n";\
     s<<"Assertion \""<<error<<"\" failed (line "<<__LINE__<<" in "<<__FILE__<<")";\
     s<<"###########################################\n";\
     printf("%s\n",s.str().c_str()); \
    }\
  }




// struct TTracker_window
// {
//     SDL_Window *tracker_window;
//     SDL_GLContext glcontext;
//     CGCprogram  *  _cgprogram;
// };

struct TTracker_window {
    SDL_Window *tracker_window;
    SDL_GLContext glcontext;
#ifdef    _USE_CGC_
    CGCprogram  *  _cgprogram;
#endif
};

class CTriangleObject
{
    float *  normals;
    float *  vertices;
    uint32_t  * faces;
    uint32_t  * f_nindex;
    std::size_t max_f_nindex;
    int num_f;
    int num_v;
    std::vector<std::list<Vector<std::size_t,3> > > vertex_nh;

//     Vector<float,3> color;


public:
    static const std::size_t object_id_sphere=0;
    static const std::size_t object_id_disk=1;
    static const std::size_t object_id_cylinder=2;
    static const std::size_t object_id_cube=3;
    static const std::size_t object_id_disk2=4;
    static const std::size_t object_id_spine=5;

//     typedef struct CVertex {
//         float x, y, z;        //Vertex
//         //float r, g, b;        //Color
//         //float nx, ny, nz;     //Normal
//     } vertexStatic;


    template<typename T>
    class Vertex
    {
    public:
        union {
            struct {
                T x, y, z;        //Vertex
            };
            struct {
                T r, g, b;        //Color
            };
            T position[3];
            T color[3];
        };
        //float r, g, b;        //Color
        //float nx, ny, nz;     //Normal
    };
    
    template<typename T>
    class Vertex4
    {
    public:
        union {
            struct {
                T x, y, z, w;        //Vertex
            };
            struct {
                T r, g, b, a;        //Color
            };
            T position[4];
            T color[4];
        };
        //float r, g, b;        //Color
        //float nx, ny, nz;     //Normal
    };


    typedef  Vertex<float> CVertex;

    void patch2mesh ( const mxArray * vertex_data, std::size_t index=0 ) {
        sta_assert_error ( vertex_data !=NULL );
        sta_assert_error ( mxIsStruct ( vertex_data ) );

        const mxArray * v_array=mxGetField ( vertex_data,index, ( char * ) ( "v" ) );
        const mxArray * f_array=mxGetField ( vertex_data,index, ( char * ) ( "f" ) );
        const mxArray * n_array=mxGetField ( vertex_data,index, ( char * ) ( "n" ) );
	const mxArray * vn_array=mxGetField ( vertex_data,index, ( char * ) ( "ni" ) );

        sta_assert_error ( v_array!=NULL );
        sta_assert_error ( f_array!=NULL );
        sta_assert_error ( n_array!=NULL );
	//sta_assert_error ( vn_array!=NULL );

//         printf ( "preparing\n" );
        normals= ( float * ) mxGetPr ( n_array );
        vertices= ( float * ) mxGetPr ( v_array );
        const mwSize *v_dims = mxGetDimensions ( v_array );
        faces= ( uint32_t  * ) mxGetPr ( f_array );
        const mwSize *f_dims = mxGetDimensions ( f_array );
	
	
	

        int f=0;
        num_f=f_dims[1];

        num_v=v_dims[1];
	
	
	if (vn_array != NULL)
	{
	  f_nindex= ( uint32_t  * ) mxGetPr ( vn_array );
	  const mwSize *vn_dims = mxGetDimensions ( vn_array );
	  max_f_nindex=vn_dims[0];
	}else
	{
	  f_nindex=NULL;
	  max_f_nindex=0;
	}
	
	

//         printf ( "mum faces: %d num verts: %d\n",num_f,num_v );

        for ( int f=0; f<num_f; f++ ) {

            sta_assert_error ( f<num_f );
            sta_assert_error ( f>=0 );
            sta_assert_error ( faces[f*3]<num_v );
            sta_assert_error ( faces[f*3+1]<num_v );
            sta_assert_error ( faces[f*3+2]<num_v );
            sta_assert_error ( faces[f*3]>=0 );
            sta_assert_error ( faces[f*3+1]>=0 );
            sta_assert_error ( faces[f*3+2]>=0 );
        }
    }
    
    std::list<Vector<std::size_t,3>>  * get_nlist(std::size_t vertex)
    {
      if (f_nindex==NULL)
      {
	return NULL;
      }
      
	if (vertex_nh.size()<num_v)
	{
	  printf("init v nlist\n");
	  vertex_nh.resize(num_v);
	  for (std::size_t i=0;i<num_v;i++)
	  {
	    for (int j=0;j<max_f_nindex;j++)
	    {
	      uint32_t tmp=f_nindex[i*max_f_nindex+j];
	      if (tmp>0)
	      {
		tmp--;
  // 	      printf("%d ",tmp );
  // 	      printf("%d %d %d %d\n",i,faces[tmp*3],faces[tmp*3+1],faces[tmp*3+2] );
		vertex_nh[i].push_back(Vector<std::size_t,3>(faces[tmp*3],faces[tmp*3+1] ,faces[tmp*3+2]));
	      }
	    }
  // 	  printf("\n");
	  }
	}
      
      sta_assert_error(vertex<num_v);
      return &(vertex_nh[vertex]);
    }
    

    CTriangleObject ( const mxArray * vertex_data, std::size_t index=0 ) {
//         printf ( "done\n" );
//         mhs::mex_dumpStringNOW();
        try {
            patch2mesh ( vertex_data,index );
        } catch ( mhs::STAError error ) {
            throw error;
        }
    }

    CTriangleObject ( std::size_t mode , int lod=1 ) {
        mwSize ndim=1;
        mxArray *arg1 = mxCreateNumericArray ( 1,&ndim,mxUINT64_CLASS,mxREAL );
        std::size_t * handle_p= ( std::size_t  * ) mxGetPr ( arg1 );
        *handle_p= ( std::size_t ) ( mode );

        mxArray *arg2 = mxCreateNumericArray ( 1,&ndim,mxUINT64_CLASS,mxREAL );
        handle_p= ( std::size_t  * ) mxGetPr ( arg2 );
        *handle_p= ( std::size_t ) ( lod );


        mxArray *pargin[2] = {arg1, arg2};

        mxArray * vertex_data;


//         printf ( "calling matlab func " );
        if ( mexCallMATLAB ( 1, &vertex_data,2,pargin , "mhs_gui_paches" ) !=0 ) {
            throw mhs::STAError ( "coud'nt load vertex data\n" );

        }

        mxDestroyArray ( arg1 );
        mxDestroyArray ( arg2 );
//         printf ( "done\n" );
//         mhs::mex_dumpStringNOW();
        try {
            patch2mesh ( vertex_data );
        } catch ( mhs::STAError error ) {
            throw error;
        }
    }

    std::size_t getNumVerts() {
        return num_v;
    };
    std::size_t getNumIndeces() {
        return num_f;
    };

    void cpy2VertexBuffer ( CVertex * bufferV,CVertex * bufferN ) {
        for ( std::size_t i=0; i<num_v; i++ ) {
            float *v=vertices+i*3;
            bufferV->x=*v++;
            bufferV->y=*v++;
            bufferV->z=*v++;
            v=normals+i*3;
            bufferN->x=*v++;
            bufferN->y=*v++;
            bufferN->z=*v++;



            bufferV++;
            bufferN++;
        }
    }


//     void cpy2VertexBuffer ( CVertex * bufferV,CVertex * bufferN,Matrix<float,4> & vertex_trafo,Matrix<float,4> & normal_trafo)
//     {
//         for ( std::size_t i=0; i<num_v; i++ ) {
// 	    float *v=vertices+i*3;
//
// 	    Vector<float,3> vertex=vertex_trafo*(Vector<float,3>(v));
//
//
//             bufferV->x=vertex[0];
//             bufferV->y=vertex[1];
//             bufferV->z=vertex[2];
//
//             v=normals+i*3;
//
// 	    Vector<float,3> normal=normal_trafo*(Vector<float,3>(v));
//
//             bufferN->x=normal[0];
//             bufferN->y=normal[1];
//             bufferN->z=normal[2];
//
//
//
//             bufferV++;
//             bufferN++;
//         }
//     }


//     void cpy2IndexBuffer ( ushort * buffer,std::size_t offset=0 ) {
//         for ( std::size_t i=0; i<num_f*3; i++ ) {
//             int f=faces[i];
//             if ( ! ( ( f>-1 ) && ( f<num_v ) ) ) {
//                 printf ( "%d\n",f );
//                 sta_assert_error ( false );
//             }
//             *buffer=faces[i]+offset;
//             buffer++;
//         }
//     }

    template<typename IndexType>
    void cpy2IndexBuffer ( IndexType * buffer,std::size_t offset=0 ) {
        for ( std::size_t i=0; i<num_f*3; i++ ) {
            int f=faces[i];
            if ( ! ( ( f>-1 ) && ( f<num_v ) ) ) {
                printf ( "%d\n",f );
                sta_assert_error ( false );
            }
            *buffer=faces[i]+offset;
            buffer++;
        }
    }

    void renderTriangles ( float * color,float scale,float thickness ) {
        sta_assert_error ( scale>0 );
        sta_assert_error ( scale<100 );
        sta_assert_error ( thickness>0 );
        sta_assert_error ( thickness<100 );

        glColor3fv ( color );
        for ( int f=0; f<num_f; f++ ) {
            glNormal3fv ( normals+faces[f*3]*3 );
            float *v=vertices+faces[f*3]*3;
            glVertex3f ( v[0]*scale,v[1]*scale,thickness*v[2] );

            glNormal3fv ( normals+faces[f*3+1]*3 );
            v=vertices+faces[f*3+1]*3;
            glVertex3f ( v[0]*scale,v[1]*scale,thickness*v[2] );
            glNormal3fv ( normals+faces[f*3+2]*3 );
            v=vertices+faces[f*3+2]*3;
            glVertex3f ( v[0]*scale,v[1]*scale,thickness*v[2] );
        }
    }




};



class CSceneRenderer
{
public:

    static const int Cred=0;
    static const int Cgreen=1;
    static const int Cblue=2;
    static const int Cyellow=3;
    static const int Ctur=4;
    static const int Cmag=5;
    static const int Cdarkgray=6;
    static const int Clightgray=7;
    static const int Corange=8;
    static const int Cbrightorange=9;
    static const int CWhite=10;
    static const int Cdarkyello=11;
    static const int Clightyello=12;



    static const GLfloat particle_colors[13][3];


protected:
    TTracker_window * tracker_data_p=NULL;
    bool close_gl_window_on_exit;


    GLfloat light_position[4] = { 0.0, 0.0, 5.0, 1.0 };
    /*
    GLfloat mat_specular[4] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[1] = { 50.0 };  */

    int width;
    int height;

//     float shape[3];
//    float center[3];
    Vector<std::size_t,3> shape;
    Vector<float,3> center;


//   static const unsigned int num_particle_types=4;
//   CTriangleObject * particles[num_particle_types];
//
//
//   GLuint vertex_buffer;
//   GLuint normal_buffer;
//   GLuint index_buffer;
//
//   struct CMesh
//   {
//     std::size_t i_offset;
//     std::size_t i_length;
//   };
//
//   std::size_t num_faces;
//
//
//   CMesh * buffered_objects;
public:

    TTracker_window *  get_window_handle() {
        return tracker_data_p;
    };

    CSceneRenderer ( std::size_t * existing_handle=NULL ) {


        close_gl_window_on_exit= ( existing_handle==NULL );


        //mxArray * handle=mexGetVariable("global", "tracker_window_ptr");
        if ( existing_handle!=NULL ) {
            tracker_data_p= ( TTracker_window* ) ( *existing_handle );
            printf ( "found tracker-gl window : " );
        } else {
            mwSize ndim=1;
            mxArray * handle = mxCreateNumericArray ( 1,&ndim,mxUINT64_CLASS,mxREAL );
            mexCallMATLAB ( 1, &handle,0,NULL, "mhs_gui_window" );
            //mexCallMATLAB(0, NULL,0,NULL, "mhs_gui_create_window");
            //handle=mexGetVariable("global", "tracker_window_ptr");
            if ( handle==NULL ) {
                printf ( "shit: still no tracker-gl window\n" );
                return;
            }

            std::size_t * handle_p= ( std::size_t  * ) mxGetPr ( handle );
            tracker_data_p= ( TTracker_window* ) ( *handle_p );
        }



        SDL_GetWindowSize ( tracker_data_p->tracker_window,&width,&height );
        printf ( "(%dx%d)\n",width,height );


        if ( SDL_GL_MakeCurrent ( tracker_data_p->tracker_window,tracker_data_p->glcontext ) !=0 ) {

            printf ( "error setting opengl context to current window\n" );
            return;
        }


        SDL_FlushEvents ( SDL_KEYDOWN,SDL_KEYUP );

        glDisableClientState ( GL_COLOR_ARRAY );
        glDisableClientState ( GL_TEXTURE_COORD_ARRAY );


    }




    ~CSceneRenderer() {


        if ( close_gl_window_on_exit ) {
            printf ( "closeing tracker window\n" );
            mxArray *arg1 = mxCreateString ( "handle" );

            mwSize ndim=1;
            mxArray *arg2 = mxCreateNumericArray ( 1,&ndim,mxUINT64_CLASS,mxREAL );

            std::size_t * handle_p= ( std::size_t  * ) mxGetPr ( arg2 );
            *handle_p= ( std::size_t ) ( tracker_data_p );
            mxArray *pargin[2] = {arg1, arg2};
            //mexCallMATLAB(0, NULL,2,pargin, "mhs_gui_window");
            //mxArray *pargin[1] = {arg2};
            mexCallMATLAB ( 0, NULL,2,pargin, "mhs_gui_window" );
        }
    }

    void set_shape ( const std::size_t shape[] ) {
        for ( int d=0; d<3; d++ ) {
            this->shape[d]=shape[d];
            //center[d]=this->shape[d]/2;
            center[d]=0;
        }
        mhs_check_gl;

        glViewport ( 0, 0, width, height );
        glShadeModel ( GL_SMOOTH );
        glEnable ( GL_COLOR_MATERIAL );

        glEnable ( GL_LIGHTING );
        glEnable ( GL_LIGHT0 );
        glEnable ( GL_DEPTH_TEST );
        glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
        glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
//      glColorMaterial ( GL_FRONT, GL_AMBIENT_AND_DIFFUSE );

        glMatrixMode ( GL_PROJECTION );
        glLoadIdentity();
        float zNear=1;
        float zFar=20;
        //gluPerspective(65.,(float)width/(float)height,zNear,zFar);
        float left=-1;
        float right=1;
        float bottom=-1;
        float top=1;
        float zoomFactor=0.5;
        //float zoomFactor=1;


//     float zFar=4*this->shape[2];
//     float left=-this->shape[0]/2.0f;
//     float right=this->shape[0]/2.0f;
//     float bottom=-this->shape[1]/2.0f;
//     float top=this->shape[1]/2.0f;
//     float zoomFactor=1;

        printf ( "%f %f %f %f\n",left,right,top,bottom );
        glFrustum ( left*zoomFactor, right*zoomFactor,
                    bottom*zoomFactor, top*zoomFactor,
                    zNear, zFar );
        mhs_check_gl;
//     glFrustum(0,2*right*zoomFactor,
//              0, 2*top*zoomFactor,
//              zNear, zFar);
    }

    void render() {

    }

//   void render()
//     {
//
// 	static int face=num_faces-1;
//        face++;
//        face%=num_faces;
//        //  printf("face %d of %d\n",face,num_faces);
//
//        static int particle_typ=0;
//        particle_typ++;
//        particle_typ%=num_particle_types;
//         glEnable(GL_NORMALIZE);
//
//        //printf("%f %f %f\n",center[0],center[1],center[2]);
//       for (int i=0;i<360;i+=5)
//       {
//        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//
//
//        glMatrixMode( GL_MODELVIEW );
//        glLoadIdentity();
//        //gluLookAt(0,0,-1.0,0,0,0,0.0,1.0,0.0);
//        gluLookAt(0,0,-2,center[0],center[1],center[2],0.0,1.0,0.0);
//
//
//        //glTranslatef(center[0],center[1],center[2]);
//        //glTranslatef(center[0],center[1],center[2]);
//        glRotatef(i,1,1,1);
//        float flatness=0.2;
//
//
//
//        enable_VBO();
//
//        glScalef(0.1,0.1,0.1);
// //        static int particle_typ=0;
// //        particle_typ++;
// //        particle_typ%=num_particle_types;
//           std::size_t offset=buffered_objects[particle_typ].i_offset;
//   	 glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
//
//
// //          std::size_t offset=face*3;
// //  	 glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
//
//
//        disable_VBO();
// //        glBegin(GL_TRIANGLES);
// // 	particles[std::rand()%num_particle_types]->renderTriangles(particle_colors[0],0.5,flatness);
// //        glEnd();
//        glFlush();
//        SDL_GL_SwapWindow(tracker_data_p->tracker_window);
//        //SDL_Delay(50);
//       }
//    }

//   void render(pool<class Points<T,Dim> > *)
//   {
//       std::size_t n_points=particle_pool->get_numpts();
// 	const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
//
//      for (unsigned int i=0; i<n_points; i++)
//         {
//             Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[i]);
//
//          }
//   }
};


const GLfloat CSceneRenderer::particle_colors[13][3] = {
    {1.0f,0.25f,0.25f},
    {0.0f,1.0f,0.25f},
    {0.25f,0.25f,1.0f},
    {1,1,0.25f},
    {0.25f,1,1},
    {1,0.25f,1},
    {0.25f,0.25f,0.25f},
    {0.75f,0.75f,0.75f},
    {0.98f,0.59f,0.20f},
    {0.98f,0.79f,0.60f},
    {1.00f,1.00f,1.00f},
    {0.5f,0.5f,0.1f},
    {1,1,0.75f},
};


template<typename TData,typename T>
class CVrender
{
private:

    TTracker_window * tracker_data_p;
    Shader * render_shader;

    Shader * shader;
    GLint glsl_worldview;
    GLint glsl_modelview;
    GLint glsl_normalmat;
    GLint glsl_textview;

    GLint glsl_image_texture;
    GLint glsl_light_texture;

    GLint glsl_clipping_plane;
    GLint glsl_focuspoint;
//     GLint glsl_vertex_mode;
    GLint glsl_mode;
    GLint glsl_rescale;
    GLint glsl_parameter_pixelDensity;
    GLint glsl_parameter_pixelDensityThreshold;
    GLint glsl_ambient;
    GLint glsl_diffuse;
    GLint glsl_specular;
    GLint  glsl_specularN;

    GLint  glsl_texture_size;
    GLint glsl_color_mode;
    GLint glsl_is_color_tex;

    GLint  glsl_gamma;

    GLint  glsl_colorize;


#ifdef    _USE_CGC_
    CGprogram   _gcMyPixelProgram;
    CGprogram   _gcMyVertexProgram;


    CGparameter _modelview;
    CGparameter _worldview;
    CGparameter _textview;
    CGparameter    _textureRot;
    CGparameter    _vertex_mode;
    CGparameter    _rescale;
    CGparameter    _focuspointV;

    CGparameter    _clipping_plane;

    CGparameter    _parameter_pixelDensity;
    CGparameter    _parameter_pixelDensityThreshold;


    CGparameter  _ambient;
    CGparameter  _diffuse;
    CGparameter  _specular;
    CGparameter  _specularN;
#endif

    unsigned int _textureID;
    unsigned int _lightTextureID;

    std::size_t texture_shape[3];
    float texture_scale[3];

    std::size_t num_indecs;
    GLuint vertex_buffer;
    GLuint index_buffer;

    bool has_texture3D;
    bool has_lighttexture3D;

    int color_mode;
    bool texture_is_color;

//     int GC_PROGRAM_ID=0;




    std::size_t shape[3];

protected:
    Vector<float,4> shader_light_options;
public:

    float parameter_pixelDensity=0.5;
    float parameter_pixelDensityThreshold=0.1;
    float parameter_gamma=1;
    Vector<float,3> color;




    /*
    std::vector<float> get_state()
    {

      std::vector<float> options;
      options.resize(4+3+3);
      int count=0;
      for (int a=0;a<4;a++)
      options[count++]=this->shader_light_options[a];

      for (int a=0;a<3;a++)
      options[count++]=this->background_color[a];

      for (int a=0;a<3;a++)
      options[count++]=this->font_color[a];

    }








    void set_state(std::vector<float>  & options)
    {
      sta_assert_error(options.size()==4+3+3)
      int count=0;
      for (int a=0;a<4;a++)
      this->shader_light_options[a]=options[count++];

      for (int a=0;a<3;a++)
      this->background_color[a]=options[count++];

      for (int a=0;a<3;a++)
      this->font_color[a]=options[count++];
    }*/


    void set_window_handle ( TTracker_window * window_handle ) {
        tracker_data_p=window_handle;


        shader_light_options[0]=0.5;
        shader_light_options[1]=0.5;
        shader_light_options[2]=0.5;
        shader_light_options[3]=4;


        vertex_buffer=0;
        index_buffer=0;


        /*
        tracker_data_p=NULL;
         mxArray * handle=mexGetVariable("global", "tracker_window_ptr");
        if (handle!=NULL)
        {
          printf("found tracker-gl window : ");
        }else{

        mhs::STAError error;
        error<<"no gl window!!!";
        throw error;
        }


        std::size_t * handle_p=(std::size_t  *)mxGetPr(handle);
        tracker_data_p=(TTracker_window*)(*handle_p);
         */

        has_texture3D=false;
        has_lighttexture3D=false;
        _textureID=0;
        _lightTextureID=0;


        shader=NULL;
        try {
            shader=new Shader ( application_directory+"/volview_vshader.glsl",
                                application_directory+"/volview_pshader.glsl" );
            mhs_check_gl
            glsl_worldview=shader->get_variable ( "glsl_worldview" );
            glsl_modelview=shader->get_variable ( "glsl_modelview" );
            glsl_textview=shader->get_variable ( "glsl_textview" );
            glsl_normalmat=shader->get_variable ( "glsl_normalmat" );

            glsl_image_texture=shader->get_variable ( "glsl_image_texture" );
            glsl_light_texture=shader->get_variable ( "glsl_light_texture" );

            glsl_clipping_plane=shader->get_variable ( "glsl_clipping_plane" );
            glsl_focuspoint=shader->get_variable ( "glsl_focuspoint" );
            glsl_mode=shader->get_variable ( "glsl_mode" );
            glsl_rescale=shader->get_variable ( "glsl_rescale" );
            glsl_parameter_pixelDensity=shader->get_variable ( "glsl_parameter_pixelDensity" );
            glsl_parameter_pixelDensityThreshold=shader->get_variable ( "glsl_parameter_pixelDensityThreshold" );
            glsl_ambient=shader->get_variable ( "glsl_ambient" );
            glsl_diffuse=shader->get_variable ( "glsl_diffuse" );
            glsl_specular=shader->get_variable ( "glsl_specular" );
            glsl_specularN=shader->get_variable ( "glsl_specularN" );
            glsl_texture_size=shader->get_variable ( "glsl_texture_size" );
            glsl_color_mode=shader->get_variable ( "glsl_color_mode" );
            glsl_is_color_tex=shader->get_variable ( "glsl_is_color_tex" );
            glsl_gamma=shader->get_variable ( "glsl_gamma" );
            glsl_colorize=shader->get_variable ( "glsl_colorize" );

            mhs_check_gl

        } catch ( mhs::STAError error ) {

            mhs::STAError error2;
            error2<<error.str() <<"\n"<<"could not create the object";
            throw error2;
        }


//     _cgprogram = new CGCprogram();

#ifdef    _USE_CGC_
        if ( PrintlastCGerror() ) {
            printf ( "CGC in invalid stade\n" );
        }


        try {

            {
                std::stringstream s;
                s<<shader_path<<"volview_pixel_shader.cg";
// 		tracker_data_p->_cgprogram[GC_PROGRAM_ID]-> Load_Program(_gcMyPixelProgram,s.str(),CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER);
                tracker_data_p->_cgprogram->Load_Program ( _gcMyPixelProgram,s.str(),CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
                _textureRot = cgGetNamedParameter ( _gcMyPixelProgram, "ModelView" );
                _focuspointV = cgGetNamedParameter ( _gcMyPixelProgram, "focuspoint" );
                _vertex_mode = cgGetNamedParameter ( _gcMyPixelProgram, "mode" );
                _clipping_plane= cgGetNamedParameter ( _gcMyPixelProgram, "clipping_plane" );
                _rescale = cgGetNamedParameter ( _gcMyPixelProgram, "rescale" );
                _parameter_pixelDensity = cgGetNamedParameter ( _gcMyPixelProgram, "parameter_pixelDensity" );
                _parameter_pixelDensityThreshold = cgGetNamedParameter ( _gcMyPixelProgram, "parameter_pixelDensityThreshold" );
                _ambient = cgGetNamedParameter ( _gcMyPixelProgram, "ambient" );
                _diffuse = cgGetNamedParameter ( _gcMyPixelProgram, "diffuse" );
                _specular = cgGetNamedParameter ( _gcMyPixelProgram, "specular" );
                _specularN = cgGetNamedParameter ( _gcMyPixelProgram, "specularN" );
            }
            if ( !PrintlastCGerror() ) {
                printf ( "sucessfully loaded the shader programs (pixel shade)\n" );
            }
            {
                std::stringstream s;
                s<<shader_path<<"volview_vertex_shader.cg";
// 		tracker_data_p->_cgprogram[GC_PROGRAM_ID]->Load_Program(_gcMyVertexProgram,s.str(),CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER);
                tracker_data_p->_cgprogram->Load_Program ( _gcMyVertexProgram,s.str(),CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );
                _modelview = cgGetNamedParameter ( _gcMyVertexProgram, "modelView" );
                _worldview = cgGetNamedParameter ( _gcMyVertexProgram, "worldView" );
                _textview = cgGetNamedParameter ( _gcMyVertexProgram, "textView" );

            }
            if ( !PrintlastCGerror() ) {
                printf ( "sucessfully loaded the shader programs (vertex shade)\n" );
            } else {

                throw 0;
            }

        } catch ( ... ) {

            printf ( "error loading the shader programs\n" );

        }
        PrintlastCGerror();
#endif
        //render_shader=new Shader ("volview_vertex_shader.txt","volview_pixel_shader.txt");
    }

    CVrender() {
        tracker_data_p=NULL;
        color_mode=0;
        texture_is_color=false;
        color=float ( 1 );
    }



    ~CVrender() {
        if ( shader!=NULL ) {
            delete shader;
        }

        if ( tracker_data_p==NULL ) {
            return;
        }
        if ( index_buffer!=0 ) {
            glDeleteBuffers ( 1, &index_buffer );
            glDeleteBuffers ( 1, &vertex_buffer );
        }

//     return;
#ifdef    _USE_CGC_
        std::cerr<<"deleting cgprograms"<<std::endl;
        if ( tracker_data_p!=NULL ) {
            cgDestroyProgram ( _gcMyPixelProgram );
            cgDestroyProgram ( _gcMyVertexProgram );
        }
        std::cerr<<"shaders destroyed"<<std::endl;
#endif

        if ( has_texture3D ) {
            glDeleteTextures ( 1,&_textureID );
        }
        if ( has_lighttexture3D ) {
            glDeleteTextures ( 1,&_lightTextureID );
        }



// 	if (_textureID!=0)
// 	{
// 	  glDeleteTextures(1,&_textureID);
// 	  glBindTexture(GL_TEXTURE_3D, 0);
// 	}
// 	_textureID=0;
//
// 	 if ( _cgprogram!=NULL )
//                {cgDestroyProgram ( _gcMyPixelProgram );cgDestroyProgram ( _gcMyVertexProgram );delete _cgprogram;}
        //delete render_shader;
    }

    void init ( const mxArray * feature_struct ) {
        sta_assert_error ( tracker_data_p!=NULL );
//       printf("gooo!\n");
//       mhs::mex_dumpStringNOW();
        try {
            for ( int i=0; i<3; i++ ) {
                this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
            }
            std::swap ( this->shape[0],this->shape[2] );
            printf ( "shape %d %d %d\n",this->shape[0],this->shape[1],this->shape[2] );
        } catch ( mhs::STAError error ) {

            try {

                mhs::dataArray<TData> img=mhs::dataArray<TData> ( feature_struct,"img" );
                sta_assert_error ( img.dim.size() ==3 );
                for ( int i=0; i<3; i++ ) {
                    this->shape[i]=img.dim[i];
                }
                printf ( "shape %d %d %d\n",this->shape[0],this->shape[1],this->shape[2] );
            } catch ( mhs::STAError error2 ) {

                mhs::STAError error3;
                error3<<error2.str() <<"\n"<<error.str() <<"\n"<<"could not determine shape (volume renderer)";
                throw error3;
            }
        }

        /*******************
         *
         * 	INTENSITY TEXTURE
         *
         *******************/
        if ( has_texture3D ) {
            glDeleteTextures ( 1,&_textureID );
        }

        texture_is_color=false;
        has_texture3D=false;
        uint8_t * texture=NULL;
        try {
            mhs::dataArray<uint8_t>  tmp;
            {
                tmp=mhs::dataArray<uint8_t> ( feature_struct,"texture3D" );

                sta_assert_error ( ( tmp.dim.size() ==3 ) || ( tmp.dim.size() ==4 ) );

                texture_is_color= ( tmp.dim.size() ==4 );

                texture_shape[0]=tmp.dim[2];
                texture_shape[1]=tmp.dim[1];
                texture_shape[2]=tmp.dim[0];


                sta_assert_error ( ( tmp.dim.size() ==3 ) || ( tmp.dim[3]==3 ) );


// 	      sta_assert_error(texture_shape[2]==texture_shape[1]);
// 	      sta_assert_error(texture_shape[2]==texture_shape[0]);
                texture=tmp.data;
                has_texture3D=true;
            }
        } catch ( mhs::STAError error ) {
            mhs::STAError error2;
            error2<<error.str() <<"\n"<<"could load 3D texture";
            throw error2;
        }


        bool warn=false;
        printf ( "found 3D texture: " );
        for ( int i=0; i<3; i++ ) {
            double valid=std::log ( double ( texture_shape[i] ) ) /std::log ( 2.0 );
            printf ( " %d ",texture_shape[i] );
            if ( ( valid-std::floor ( valid ) ) >0.00000000000001 ) {
// 	    mhs::STAError error;
// 	    error<<"texture wrong dim";
// 	    throw error;
                warn=true;
            }
        }
        printf ( "\n" );
        if ( warn ) {
            printf ( "warning: texture NPOT\n" );
        }


// 	float half_voxel_correction=float(texture_shape[0]-0.5f)/float(texture_shape[0]);
        float rescale=1.0/std::max ( std::max ( shape[0],shape[1] ),shape[2] );
        texture_scale[0]=1/ ( rescale*shape[0] );
        texture_scale[1]=1/ ( rescale*shape[1] );
        texture_scale[2]=1/ ( rescale*shape[2] );
	
	

// 	texture_scale[0]=texture_shape[0]/(shape[0]);
// 	texture_scale[1]=texture_shape[1]/(shape[1]);
// 	texture_scale[2]=texture_shape[2]/(shape[2]);

// 	printf("%f %f %f\n",shape[0],shape[1],shape[2]);
// 	printf("%f %f %f\n",texture_scale[0],texture_scale[1],texture_scale[2]);
// 	printf("###############\n");



        glGenTextures ( 1, &_textureID );
        glBindTexture ( GL_TEXTURE_3D, _textureID );
        mhs_check_gl;
        glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE );
        glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE );
        glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_EDGE );
        glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_LINEAR );
        glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_LINEAR );

        mhs_check_gl;
        if ( texture_is_color ) {
            glTexImage3D ( GL_TEXTURE_3D, 0,  GL_RGB8 , texture_shape[0], texture_shape[1], texture_shape[2], 0,
                           GL_RGB ,
                           GL_UNSIGNED_BYTE, texture );
        } else {
            glTexImage3D ( GL_TEXTURE_3D, 0,  GL_R8 , texture_shape[0], texture_shape[1], texture_shape[2], 0,
                           GL_RED ,
                           GL_UNSIGNED_BYTE, texture );
        }
        mhs_check_gl;

        /*******************
         *
         * 	LIGHT TEXTURE
         *
         *******************/

        if ( has_lighttexture3D ) {
            glDeleteTextures ( 1,&_lightTextureID );
        }
        has_lighttexture3D=false;


        texture=NULL;
        try {
            mhs::dataArray<uint8_t>  tmp;
            {
                tmp=mhs::dataArray<uint8_t> ( feature_struct,"light_texture3D" );
                sta_assert_error ( tmp.dim.size() ==4 );

// 	      texture_shape[0]=tmp.dim[2];
// 	      texture_shape[1]=tmp.dim[1];
// 	      texture_shape[2]=tmp.dim[0];
                sta_assert_error ( tmp.dim[0]==texture_shape[2] );
                sta_assert_error ( tmp.dim[1]==texture_shape[1] );
                sta_assert_error ( tmp.dim[2]==texture_shape[0] );
                sta_assert_error ( tmp.dim[3]==3 );

// 	      texture=tmp.data;
                printf ( "found light texture: %d %d %d %d\n",tmp.dim[0],tmp.dim[1],tmp.dim[2],tmp.dim[3] );
                texture=tmp.data;
                has_lighttexture3D=true;
            }
        } catch ( mhs::STAError error ) {
            printf ( "could load 3D light texture: %s\n",error.str().c_str() );

            mhs::STAError error2;
            error2<<error.str() <<"\n"<<"could load 3D light texture";
            throw error2;
        }


        if ( texture!=0 ) {

            glGenTextures ( 1, &_lightTextureID );
            glBindTexture ( GL_TEXTURE_3D, _lightTextureID );
            mhs_check_gl;
            glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE );
            glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE );
            glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_EDGE );
            glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_LINEAR );
            glTexParameteri ( GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_LINEAR );

            mhs_check_gl;
            glTexImage3D ( GL_TEXTURE_3D, 0,  GL_RGB8 , texture_shape[0], texture_shape[1], texture_shape[2], 0,
                           GL_RGB ,
                           GL_UNSIGNED_BYTE, texture );
            mhs_check_gl;
        }






        glTexEnvf ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
        mhs_check_gl;

        //int num_slices=std::max ( ( std::max ( std::max ( texture_shape[1],texture_shape[2] ),texture_shape[0] ) ) *2*std::sqrt ( 0.5*0.5*2 ),300.0 );
	int num_slices=std::max ( ( std::max ( std::max ( texture_shape[1]*texture_scale[1],texture_shape[2]*texture_scale[2] ),texture_shape[0]*texture_scale[0] ) ) *2*std::sqrt ( 0.5*0.5*2 ),300.0 );
	
        init_slices ( num_slices );
    }



    void init_slices ( int num_slices ) {
        sta_assert_error ( tracker_data_p!=NULL );
        float length=std::sqrt ( 0.5f*0.5f*2.0f );
        if ( index_buffer!=0 ) {
            glDeleteBuffers ( 1, &index_buffer );
            glDeleteBuffers ( 1, &vertex_buffer );
        }

        num_indecs=num_slices*4;


        float * tmp_buffer=new float[num_indecs*3];
        ushort * face_buffer=new ushort[num_indecs];
        //float * tmp_buffer_p=tmp_buffer;

        std::size_t count_v=0;
//       std::size_t count_i=0;
        for ( int ii=0; ii<num_slices; ii++ ) {
            float z=-length + ( float ) ( ii ) *length*2.0f/ ( float ) ( num_slices-1 );

            tmp_buffer[count_v*3]=-length;
            tmp_buffer[count_v*3+1]=-length;
            tmp_buffer[count_v*3+2]=z;
            face_buffer[count_v]=count_v;
            count_v++;
            tmp_buffer[count_v*3]=-length;
            tmp_buffer[count_v*3+1]=length;
            tmp_buffer[count_v*3+2]=z;
            face_buffer[count_v]=count_v;
            count_v++;
            tmp_buffer[count_v*3]=length;
            tmp_buffer[count_v*3+1]=length;
            tmp_buffer[count_v*3+2]=z;
            face_buffer[count_v]=count_v;
            count_v++;
            tmp_buffer[count_v*3]=length;
            tmp_buffer[count_v*3+1]=-length;
            tmp_buffer[count_v*3+2]=z;
            face_buffer[count_v]=count_v;
            count_v++;
        }





        glGenBuffers ( 1,&vertex_buffer );
        glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
        glBufferData ( GL_ARRAY_BUFFER, sizeof ( float ) *num_indecs*3, tmp_buffer, GL_STATIC_DRAW );




        mhs_check_gl;
        glGenBuffers ( 1,&index_buffer );
        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
        mhs_check_gl;
        glBufferData ( GL_ELEMENT_ARRAY_BUFFER, sizeof ( ushort ) *num_indecs, face_buffer, GL_STATIC_DRAW );
        mhs_check_gl;

        delete []  tmp_buffer;
        delete [] face_buffer;


        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindBuffer ( GL_ARRAY_BUFFER, 0 );



        mhs_check_gl;
    }


    void set_light ( float ambient, float diffuse, float specular, float specularN ) {

        shader_light_options[0]=ambient;
        shader_light_options[1]=diffuse;
        shader_light_options[2]=specular;
        shader_light_options[3]=specularN;
//         shader_light_options.print();
    }

    void set_color_mode ( int mode ) {
        this->color_mode=mode;
    };

    void paint ( Matrix<float,4>& projection,Matrix<float,4>& rotation,Vector<float,3> & translation,
                 Vector<float,4> shader_options,
                 Vector<float,3> & focus_point,
                 Vector<float,3> & clip_plane
               )

    {

        sta_assert_error ( tracker_data_p!=NULL );
//       float debug_mat[16];
//     glGetFloatv(GL_PROJECTION_MATRIX,debug_mat);
//     printf("2:---------------\n");
//     printf("%f %f %f %f\n",debug_mat[0],debug_mat[1],debug_mat[2],debug_mat[3]);
//     printf("%f %f %f %f\n",debug_mat[4],debug_mat[5],debug_mat[6],debug_mat[7]);
//     printf("%f %f %f %f\n",debug_mat[8],debug_mat[9],debug_mat[10],debug_mat[11]);
//     printf("%f %f %f %f\n",debug_mat[12],debug_mat[13],debug_mat[14],debug_mat[15]);
//     printf("-----------------\n");




        glDisable ( GL_LIGHTING );
        glDisable ( GL_CULL_FACE );
        glEnable ( GL_COLOR_MATERIAL );


#ifdef  _USE_CGC_
        {


            if ( tracker_data_p->_cgprogram ) {
                tracker_data_p->_cgprogram->enableProfile ( CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
                tracker_data_p->_cgprogram->enableProfile ( CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );
                cgGLBindProgram ( _gcMyPixelProgram );
                cgGLBindProgram ( _gcMyVertexProgram );
            }


            cgGLSetParameter1f ( _parameter_pixelDensity,parameter_pixelDensity );
            cgGLSetParameter1f ( _parameter_pixelDensityThreshold,parameter_pixelDensityThreshold );


            cgGLSetParameter1f ( _ambient,shader_light_options[0] );
            cgGLSetParameter1f ( _diffuse,shader_light_options[1] );
            cgGLSetParameter1f ( _specular,shader_light_options[2] );
            cgGLSetParameter1f ( _specularN,shader_light_options[3] );


            cgGLSetParameter1f ( _clipping_plane,clip_plane[0] );

            cgGLSetParameter1f ( _vertex_mode,shader_options[0] );
        }
#else
        {
            shader->bind();

            glUniform1f ( glsl_parameter_pixelDensity,parameter_pixelDensity );
            glUniform1f ( glsl_parameter_pixelDensityThreshold,parameter_pixelDensityThreshold );


            glUniform1f ( glsl_ambient,shader_light_options[0] );
            glUniform1f ( glsl_diffuse,shader_light_options[1] );
            glUniform1f ( glsl_specular,shader_light_options[2] );
            glUniform1f ( glsl_specularN,shader_light_options[3] );

            glUniform1f ( glsl_clipping_plane,clip_plane[0] );

            glUniform1f ( glsl_mode,shader_options[0] );

            glUniform1i ( glsl_color_mode,color_mode );

            glUniform1i ( glsl_is_color_tex,texture_is_color );

            glUniform3fv ( glsl_colorize,1,color.v );


            glUniform1f ( glsl_gamma,parameter_gamma );




        }
#endif

        T max_scale=std::max ( shape[2],std::max ( shape[0],shape[1] ) );
//
// 	printf("TS: %f %f %f \n",texture_scale[0],texture_scale[1],texture_scale[2]);

        shader_options[0]=max_scale/texture_scale[0];
        shader_options[1]=max_scale/texture_scale[1];
        shader_options[2]=max_scale/texture_scale[2];


#ifdef  _USE_CGC_
        {

            cgGLSetParameter3fv ( _rescale,shader_options.v );
        }
#else
        {
            glUniform3fv ( glsl_rescale, 1, ( GLfloat * ) ( shader_options.v ) );
            Vector<float,3> tshape ( texture_shape[0],texture_shape[1],texture_shape[2] );

            glUniform3fv ( glsl_texture_size, 1, ( GLfloat * ) ( tshape.v ) );



        }
#endif
        Vector<float,3> focus_point_V;


        focus_point_V=translation*-1+focus_point;
//         focus_point_V=rotation.OpenGL_invert().multv3 ( focus_point_V );
//	focus_point_V=rotation.OpenGL_inverse().multv3 ( focus_point_V );
        focus_point_V=rotation.OpenGL_inverse_rotation().multv3 ( focus_point_V );
        focus_point_V=focus_point_V*texture_scale;
        focus_point_V+=0.5;
        focus_point_V+=Vector<float,3> ( 0.5/texture_shape[0],0.5/texture_shape[1],0.5/texture_shape[2] );

#ifdef  _USE_CGC_
        {
            cgGLSetParameter3fv ( _focuspointV,focus_point_V.v );
        }
#else
        {

            glUniform3fv ( glsl_focuspoint, 1,focus_point_V.v );
        }
#endif


        glMatrixMode ( GL_MODELVIEW );
        glPushMatrix();

        //setup the modelview matrix
        glLoadIdentity();
        glMultMatrixf ( ( GLfloat * ) ( rotation.v ) );

#ifdef  _USE_CGC_
        {
            cgGLSetStateMatrixParameter ( _textureRot,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY );
        }
#else
        {

            glUniformMatrix4fv ( glsl_normalmat,1,GL_FALSE, ( GLfloat * ) rotation.v );
// 	rotation.print();
        }
#endif

        glBlendFunc ( GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA );
        glEnable ( GL_BLEND );


        glMatrixMode ( GL_TEXTURE );
        glEnable ( GL_TEXTURE_3D );

        //set up the texture matrix
        glLoadIdentity();
// 	    glTranslatef(0.5f,0.5f,0.5f);
        glTranslatef ( 0.5f+0.5/shape[0],0.5f+0.5/shape[1],0.5f+0.5/shape[2] );
        glScalef ( texture_scale[0],texture_scale[1],texture_scale[2] );

//         glMultMatrixf ( ( GLfloat * ) rotation.OpenGL_invert().v );
        glMultMatrixf ( ( GLfloat * ) rotation.OpenGL_inverse_rotation().v );
// 	    glTranslatef(-translation[0],-translation[1],-translation[2]);
        glTranslatef ( -translation[0],-translation[1],-translation[2] );





        glMatrixMode ( GL_MODELVIEW );
        glPushMatrix();

        glLoadIdentity();
        glTranslatef ( translation[0],translation[1],translation[2] );
        //glTranslatef(-translation[0],-translation[1],-translation[2]);

        glActiveTextureARB ( GL_TEXTURE0_ARB );
        glEnable ( GL_TEXTURE_3D );
        glBindTexture ( GL_TEXTURE_3D,_textureID );

#ifdef  _USE_CGC_
        {
            cgGLSetStateMatrixParameter ( _textview,  CG_GL_TEXTURE_MATRIX, CG_GL_MATRIX_IDENTITY );
            cgGLSetStateMatrixParameter ( _worldview,  CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY );
            cgGLSetStateMatrixParameter ( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY );
        }
#else
        {


            glUniform1i ( glsl_image_texture, 0 );
            glUniform1i ( glsl_light_texture, 1 );

            Matrix<float,4> textview=Matrix<float,4>::OpenGL_glTranslate ( Vector<float,3> ( 0.5f+0.5/shape[0],0.5f+0.5/shape[1],0.5f+0.5/shape[2] ) ) *
                                     Matrix<float,4>::OpenGL_glScale ( Vector<float,3> ( texture_scale[0],texture_scale[1],texture_scale[2] ) ) *
                                     rotation.OpenGL_inverse_rotation() *Matrix<float,4>::OpenGL_glTranslate ( Vector<float,3> ( -translation[0],-translation[1],-translation[2] ) );


            glUniformMatrix4fv ( glsl_textview,1,GL_FALSE, ( GLfloat * ) textview.v );

            Matrix<float,4> modelview=Matrix<float,4>::OpenGL_glTranslate ( Vector<float,3> ( translation[0],translation[1],translation[2] ) );
            glUniformMatrix4fv ( glsl_modelview,1,GL_FALSE, ( GLfloat * ) modelview.v );


            glUniformMatrix4fv ( glsl_worldview,1,GL_FALSE, ( GLfloat * ) projection.v );

        }
#endif

        //LIGHTING
        glActiveTextureARB ( GL_TEXTURE1_ARB );
        glEnable ( GL_TEXTURE_3D );
        glBindTexture ( GL_TEXTURE_3D, _lightTextureID );


        //rendering the slices
        mhs_check_gl;
        glEnableClientState ( GL_VERTEX_ARRAY );
        glEnableClientState ( GL_TEXTURE_COORD_ARRAY );

        glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
        glVertexPointer ( 3, GL_FLOAT, sizeof ( float ) *3, 0 );
        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );

        glDrawElements ( GL_QUADS, num_indecs, GL_UNSIGNED_SHORT, 0 );

        glDisableClientState ( GL_VERTEX_ARRAY );
        glDisableClientState ( GL_TEXTURE_COORD_ARRAY );

        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindBuffer ( GL_ARRAY_BUFFER, 0 );
        mhs_check_gl;

#ifdef  _USE_CGC_
        {

            //if (tracker_data_p->_cgprogram[GC_PROGRAM_ID])
            {
                tracker_data_p->_cgprogram->disableProgram ( CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
                tracker_data_p->_cgprogram->disableProgram ( CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );

                tracker_data_p->_cgprogram->disableProfile ( CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER );
                tracker_data_p->_cgprogram->disableProfile ( CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER );

            }
        }
#else
        {
            shader->unbind();
        }
#endif


        glDepthMask ( GL_TRUE );

        glDisable ( GL_ALPHA_TEST );

        //LIGHTING
        glActiveTextureARB ( GL_TEXTURE1_ARB );
        glDisable ( GL_TEXTURE_3D );

        glActiveTextureARB ( GL_TEXTURE0_ARB );
        glDisable ( GL_TEXTURE_3D );
        glEnable ( GL_LIGHTING );



// 		if (tracker_data_p->_cgprogram)
// 		{
// 		//      tracker_data_p->_cgprogram->disableProfile(CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER);
// 		tracker_data_p->_cgprogram->disableProfile(CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER);
// 		}

        glPopMatrix();

        glMatrixMode ( GL_MODELVIEW );
        glPopMatrix();

    }



};






#endif
