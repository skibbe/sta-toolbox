#ifndef MHS_MATRIX_H
#define MHS_MATRIX_H

#include "mhs_vector.h"
#include "mhs_graphics.h"


template<typename T,int Dim>
class Matrix
{
public:
    union{
	  struct{
	      T m[Dim][Dim];
	  };
	  T v[Dim*Dim];
	};
    
    Matrix() {};
    
    void print() const
    {
        std::stringstream s;
	s.precision(4);
        s<<"";
	for (int i=0; i<Dim*6; i++)
	  s<<"-";
	s<<"\n";
        for (int i=0; i<Dim; i++)
	for (int j=0; j<Dim; j++)
	{
            if (j<Dim-1)
                s<<m[i][j]<<",";
            else
                s<<m[i][j]<<"\n";
	}
	for (int i=0; i<Dim*6; i++)
	  s<<"-";
	s<<"\n";
        printf("%s",s.str().c_str());
    }
    
    
    const T &operator[](int i) const {
        return v[i];
    }
    
    T &operator[](int i)  {
        return v[i];
    }

    
    
    bool isIdentity() const
    {
       for (int i=0; i<Dim; i++)
       {
	
	for (int j=0; j<Dim; j++)
	{
	  if (std::abs((m[i][j]-(i==j)))>0.0000001)
	    return false;
	}
       }
       return true;
    }
    
    
    void Identity()
    {
      for (int i=0; i<Dim; i++)
      {
	#pragma omp simd
	for (int j=0; j<Dim; j++)
	{
	  m[i][j]=(i==j);
	}
      }
    }
    
    Matrix & operator=(const Matrix &rhs)
    {
        if (this != &rhs)
            memcpy (m,rhs.m,Dim*Dim*sizeof(T));
        return *this;
    }
    
    template<typename R>
    Matrix & operator=(const Matrix<R,Dim> &rhs)
    {
	#pragma omp simd
        for (int i=0;i<Dim*Dim;i++)
	  v[i]=rhs.v[i];
        return *this;
    }

    Matrix & operator=(const T rhs[])
    {
        if (m != rhs)
            memcpy (m,rhs,Dim*Dim*sizeof(T));
        return *this;
    }
    
    Matrix & operator=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
	{
            v[i]=f;
	}
        return *this;
    }
    
    
     Matrix & operator+=(const Matrix &rhs)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]+=rhs.v[i];
        return *this;
    }

    Matrix & operator+=(const T rhs[])
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]+=rhs[i];
        return *this;
    }

    Matrix & operator+=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]+=f;
        return *this;
    }
    
     Matrix & operator-=(const Matrix &rhs)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]-=rhs.v[i];
        return *this;
    }

    Matrix & operator-=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]-=f;
        return *this;
    }
    
    Matrix & operator/=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]/=f;
        return *this;
    }
    
    Matrix & operator*=(T f)
    {
        #pragma omp simd
        for (int i=0; i<Dim*Dim; i++)
            v[i]*=f;
        return *this;
    }
    
    
    const Matrix operator+(const Matrix &rhs) const
    {
        return Matrix(*this) += rhs;
    }

    const Matrix operator+(const T rhs[]) const
    {
        return Matrix(*this) += rhs;
    }

    const Matrix operator+(T f) const
    {
        return Matrix(*this) += f;
    }

    const Matrix operator-(const Matrix &rhs) const
    {
        return Matrix(*this) -= rhs;
    }

    const Matrix operator-(T f) const
    {
        return Matrix(*this) -= f;
    }
    
     const Matrix operator/(T f) const
    {
        return Matrix(*this) /= f;
    }
    
    const Matrix operator*(T f) const
    {
        return Matrix(*this) *= f;
    }
    
//     Matrix & operator*=(const Matrix &rhs)
//     {
// 	  Matrix mat_old(*this);
// 	  for (int i=0; i<Dim; i++)
// 	  {
// 	  for (int j=0; j<Dim; j++)
// 	  {
// 	    m[i][j]=0;
// 	    #pragma omp simd
// 	    for (int k=0; k<Dim;k++)
// 	    {
// 	      m[i][j]+=mat_old.m[i][k]*rhs.m[k][j];
// 	    }
// 	  }
// 	  }
// 	  return *this;
//     }

    Matrix & operator*=(const Matrix &rhs)
    {
	  Matrix mat_old(*this);
	  for (int i=0; i<Dim; i++)
	  {
	  for (int j=0; j<Dim; j++)
	  {
	    T & value=m[j][i];
	    value=0;
	    #pragma omp simd reduction(+:value)
	    for (int k=0; k<Dim;k++)
	    {
// 	      m[j][i]+=mat_old.m[k][i]*rhs.m[j][k];
	      value+=mat_old.m[k][i]*rhs.m[j][k];
	    }
	  }
	  }
	  return *this;
    }
    
    
    
    
    const Matrix operator*(const Matrix &rhs) const
    {
        return Matrix(*this) *= rhs;
    }

//     const Matrix operator*(T f) const
//     {
//         return Matrix(*this) *= f;
//     }
//     
    const Vector<T,Dim> operator*(const Vector<T,Dim> &rhs) const
    {
         Vector<T,Dim> v;
	  for (int i=0; i<Dim; i++)
	  {
	    T & value=v[i];
	    value=0;
	    #pragma omp simd reduction(+:value)
	    for (int k=0; k<Dim;k++)
	    {
// 	      v[i]+=m[k][i]*rhs[k];
	      value+=m[k][i]*rhs[k];
	    }
	  }
	  return v;
    }
    
    
    const Matrix transpose() const
    {
          Matrix matrix;
	  for (int i=0; i<Dim; i++)
	  {
	  #pragma omp simd
	  for (int j=0; j<Dim; j++)
	  {
	    matrix.m[j][i]=m[i][j];
	  }
	  }
	  return matrix;
    }
    

    const Vector<T,3> multv3(const Vector<T,3> &rhs) const
    {
         Vector<T,3> v;
	  for (int i=0; i<3; i++)
	  {
	    T & value = v[i];
	    value =m[3][i];
	    #pragma omp simd reduction(+:value)
	    for (int k=0; k<3;k++)
	    {
// 	      v[i]+=m[k][i]*rhs[k];
	      value+=m[k][i]*rhs[k];
	    }
	  }
	  return v;
    }
    
    bool operator==(const Matrix &other) const {
        for (int i=0; i<Dim*Dim; i++)
            if (v[i]!=other.v[i])
                return false;
        return true;
    }

    bool operator!=(const Matrix &other) const {
        return !(*this==other);
    }
    
    
    Vector<T,Dim> get_colmn(int i)
    {
        Vector<T,Dim> vec;
	#pragma omp simd
	  for (int j=0; j<Dim; j++)
	    vec.v[j]=m[i][j];
	return vec;
    }
//     Vector<T,Dim> get_colmn(int i)
//     {
//         Vector<T,Dim> vec;
// 	#pragma omp simd
// 	  for (int j=0; j<Dim; j++)
// 	    vec.v[j]=m[j][i];
// 	return vec;
//     }
    
    
    Matrix &  OpenGL_reorthonormalize()
    {
       Matrix<T,3> old_mat;
       old_mat.Identity();
       
       //b1=a1/|a1|
        T s,t;
	s= 1.0 / std::sqrt(m[0][0]*m[0][0]+m[1][0]*m[1][0]+m[2][0]*m[2][0]);
	old_mat.m[0][0]=m[0][0]*s;
	old_mat.m[1][0]=m[1][0]*s;
	old_mat.m[2][0]=m[2][0]*s;
	//b2=a2-<b1,a2>*b1
	s= (old_mat.m[0][0]*m[0][1]+old_mat.m[1][0]*m[1][1]+old_mat.m[2][0]*m[2][1]);
	old_mat.m[0][1] = m[0][1]- s * old_mat.m[0][0];
	old_mat.m[1][1] = m[1][1]- s * old_mat.m[1][0];
	old_mat.m[2][1] = m[2][1]- s * old_mat.m[2][0];
	//b2 = b2 / |b2|
	s= 1.0 /  std::sqrt(old_mat.m[0][1]*m[0][1]+old_mat.m[1][1]*m[1][1]+old_mat.m[2][1]*m[2][1]);
	old_mat.m[0][1]=old_mat.m[0][1]*s;
	old_mat.m[1][1]=old_mat.m[1][1]*s;
	old_mat.m[2][1]=old_mat.m[2][1]*s;
	//b3= a3 - <b1,a3>*b1 - <b2,a3> b2
	s= (old_mat.m[0][0]*m[0][2]+old_mat.m[1][0]*m[1][2]+old_mat.m[2][0]*m[2][2]);
	t= (old_mat.m[0][1]*m[0][2]+old_mat.m[1][1]*m[1][2]+old_mat.m[2][1]*m[2][2]);
	old_mat.m[0][2] = m[0][2] - s* m[0][0] - t * m[0][1];
	old_mat.m[1][2] = m[1][2] - s* m[1][0] - t * m[1][1];
	old_mat.m[2][2] = m[2][2] - s* m[2][0] - t * m[2][1];
	//b3 = b3 / |b3|
	s= 1.0 /  std::sqrt(old_mat.m[0][2]*m[0][2]+old_mat.m[1][2]*m[1][2]+old_mat.m[2][2]*m[2][2]);
	old_mat.m[0][2]=old_mat.m[0][2]*s;
	old_mat.m[1][2]=old_mat.m[1][2]*s;
	old_mat.m[2][2]=old_mat.m[2][2]*s;
       
       for (int i=0;i<3;i++)
       {
	 for (int j=0;j<3;j++)
	 {
	  m[i][j]=old_mat.m[i][j];
	 }
       }
       return *this;
    }
    
    Matrix OpenGL_get_rot() const
    {
      Matrix mat(*this);
      mat.m[0][3]=0;
      mat.m[1][3]=0;
      mat.m[2][3]=0;
      mat.m[3][3]=1;
      mat.m[3][0]=0;
      mat.m[3][1]=0;
      mat.m[3][2]=0;
      return mat;
    }
    
    
    
    Matrix OpenGL_get_tran() const
    {
      Matrix mat(*this);
      mat.m[0][0]=1;
      mat.m[0][1]=0;
      mat.m[0][2]=0;
      
      mat.m[1][0]=0;
      mat.m[1][1]=1;
      mat.m[1][2]=0;
      
      mat.m[2][0]=0;
      mat.m[2][1]=0;
      mat.m[2][2]=1;
      
      mat.m[0][3]=0;
      mat.m[1][3]=0;
      mat.m[2][3]=0;      
      return mat;
    }
    
    
    Vector<T,3> OpenGL_get_tranV() const
    {
      Vector<T,3> vec;
      vec[0]=m[3][0];
      vec[1]=m[3][1];
      vec[2]=m[3][2];      
      return vec;
    }
    
    void OpenGL_set_tranV(const Vector<T,3>  & vec) 
    {
       m[3][0]=vec[0];
       m[3][1]=vec[1];
       m[3][2]=vec[2];

    }

    void OpenGL_from3x3(T * rhs)
    {
      this->Identity();
      for (int i=0;i<3;i++)  
      {
	for (int j=0;j<3;j++)  
	{
	 m[i][j]=*rhs++; 
	}
      }
    }    
   
   Matrix OpenGL_invert() const
    {
        Matrix mat(*this);
	std::swap(mat.v[1],mat.v[4]);
	std::swap(mat.v[2],mat.v[8]);
	std::swap(mat.v[6],mat.v[9]);
	
	mat.v[12]=-(mat.v[0]*v[12]+mat.v[4]*v[13]+mat.v[8]*v[14]);
 	mat.v[13]=-(mat.v[1]*v[12]+mat.v[5]*v[13]+mat.v[9]*v[14]);
 	mat.v[14]=-(mat.v[2]*v[12]+mat.v[6]*v[13]+mat.v[10]*v[14]);
      return mat;
    }
    
    
    
    Matrix OpenGL_inverse_rotation() const
    {
        Matrix mat(*this);
	std::swap(mat.v[1],mat.v[4]);
	std::swap(mat.v[2],mat.v[8]);
	std::swap(mat.v[6],mat.v[9]);
      return mat;
    }
    
    Matrix OpenGL_inverseTranspose3x3() const
    {
        
	T det =
	+ m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
	- m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0])
	+ m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);

	Matrix mat(*this);

	mat.m[0][0] = + (m[1][1] * m[2][2] - m[2][1] * m[1][2]);
	mat.m[0][1] = - (m[1][0] * m[2][2] - m[2][0] * m[1][2]);
	mat.m[0][2] = + (m[1][0] * m[2][1] - m[2][0] * m[1][1]);

	mat.m[1][0] = - (m[0][1] * m[2][2] - m[2][1] * m[0][2]);
	mat.m[1][1] = + (m[0][0] * m[2][2] - m[2][0] * m[0][2]);
	mat.m[1][2] = - (m[0][0] * m[2][1] - m[2][0] * m[0][1]);

	mat.m[2][0] = + (m[0][1] * m[1][2] - m[1][1] * m[0][2]);
	mat.m[2][1] = - (m[0][0] * m[1][2] - m[1][0] * m[0][2]);
	mat.m[2][2] = + (m[0][0] * m[1][1] - m[1][0] * m[0][1]);
	mat.m /= det;

      return mat;
    }
    
    Matrix OpenGL_inverseTranspose() const
    {

	T tmp00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
	T tmp01 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
	T tmp02 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
	T tmp03 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
	T tmp04 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
	T tmp05 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
	T tmp06 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
	T tmp07 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
	T tmp08 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
	T tmp09 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
	T tmp10 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
	T tmp11 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
	T tmp12 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
	T tmp13 = m[1][2] * m[2][3] - m[2][2] * m[1][3];
	T tmp14 = m[1][1] * m[2][3] - m[2][1] * m[1][3];
	T tmp15 = m[1][1] * m[2][2] - m[2][1] * m[1][2];
	T tmp16 = m[1][0] * m[2][3] - m[2][0] * m[1][3];
	T tmp17 = m[1][0] * m[2][2] - m[2][0] * m[1][2];
	T tmp18 = m[1][0] * m[2][1] - m[2][0] * m[1][1];

	Matrix mat;

	mat.m[0][0] = + (m[1][1] * tmp00 - m[1][2] * tmp01 + m[1][3] * tmp02);
	mat.m[0][1] = - (m[1][0] * tmp00 - m[1][2] * tmp03 + m[1][3] * tmp04);
	mat.m[0][2] = + (m[1][0] * tmp01 - m[1][1] * tmp03 + m[1][3] * tmp05);
	mat.m[0][3] = - (m[1][0] * tmp02 - m[1][1] * tmp04 + m[1][2] * tmp05);

	mat.m[1][0] = - (m[0][1] * tmp00 - m[0][2] * tmp01 + m[0][3] * tmp02);
	mat.m[1][1] = + (m[0][0] * tmp00 - m[0][2] * tmp03 + m[0][3] * tmp04);
	mat.m[1][2] = - (m[0][0] * tmp01 - m[0][1] * tmp03 + m[0][3] * tmp05);
	mat.m[1][3] = + (m[0][0] * tmp02 - m[0][1] * tmp04 + m[0][2] * tmp05);

	mat.m[2][0] = + (m[0][1] * tmp06 - m[0][2] * tmp07 + m[0][3] * tmp08);
	mat.m[2][1] = - (m[0][0] * tmp06 - m[0][2] * tmp09 + m[0][3] * tmp10);
	mat.m[2][2] = + (m[0][0] * tmp11 - m[0][1] * tmp09 + m[0][3] * tmp12);
	mat.m[2][3] = - (m[0][0] * tmp08 - m[0][1] * tmp10 + m[0][2] * tmp12);

	mat.m[3][0] = - (m[0][1] * tmp13 - m[0][2] * tmp14 + m[0][3] * tmp15);
	mat.m[3][1] = + (m[0][0] * tmp13 - m[0][2] * tmp16 + m[0][3] * tmp17);
	mat.m[3][2] = - (m[0][0] * tmp14 - m[0][1] * tmp16 + m[0][3] * tmp18);
	mat.m[3][3] = + (m[0][0] * tmp15 - m[0][1] * tmp17 + m[0][2] * tmp18);

	T Determinant =

	+ m[0][0] * mat.m[0][0]

	+ m[0][1] * mat.m[0][1]

	+ m[0][2] * mat.m[0][2]

	+ m[0][3] * mat.m[0][3];

	mat /= Determinant;

      return mat;
    }    
    
    
    void OpenGL_set_basis(Vector<T,4>  a,Vector<T,4>  b,Vector<T,4>  c,Vector<T,4>  d) 
    {
      for (int i=0;i<4;i++)
      {
      m[0][i]=a[i];
      m[1][i]=b[i];
      m[2][i]=c[i];
      m[3][i]=d[i];
      }
    }
    
    Matrix OpenGL_inverse() const
    {

	T Coef00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
	T Coef02 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
	T Coef03 = m[1][2] * m[2][3] - m[2][2] * m[1][3];

	T Coef04 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
	T Coef06 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
	T Coef07 = m[1][1] * m[2][3] - m[2][1] * m[1][3];

	T Coef08 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
	T Coef10 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
	T Coef11 = m[1][1] * m[2][2] - m[2][1] * m[1][2];

	T Coef12 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
	T Coef14 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
	T Coef15 = m[1][0] * m[2][3] - m[2][0] * m[1][3];

	T Coef16 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
	T Coef18 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
	T Coef19 = m[1][0] * m[2][2] - m[2][0] * m[1][2];

	T Coef20 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
	T Coef22 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
	T Coef23 = m[1][0] * m[2][1] - m[2][0] * m[1][1];

	Vector<T,4> Fac0(Coef00, Coef00, Coef02, Coef03);
	Vector<T,4> Fac1(Coef04, Coef04, Coef06, Coef07);
	Vector<T,4> Fac2(Coef08, Coef08, Coef10, Coef11);
	Vector<T,4> Fac3(Coef12, Coef12, Coef14, Coef15);
	Vector<T,4> Fac4(Coef16, Coef16, Coef18, Coef19);
	Vector<T,4> Fac5(Coef20, Coef20, Coef22, Coef23);

	Vector<T,4> Vec0(m[1][0], m[0][0], m[0][0], m[0][0]);
	Vector<T,4> Vec1(m[1][1], m[0][1], m[0][1], m[0][1]);
	Vector<T,4> Vec2(m[1][2], m[0][2], m[0][2], m[0][2]);
	Vector<T,4> Vec3(m[1][3], m[0][3], m[0][3], m[0][3]);

	Vector<T,4> Inv0(Vec1 * Fac0 - Vec2 * Fac1 + Vec3 * Fac2);
	Vector<T,4> Inv1(Vec0 * Fac0 - Vec2 * Fac3 + Vec3 * Fac4);
	Vector<T,4> Inv2(Vec0 * Fac1 - Vec1 * Fac3 + Vec3 * Fac5);
	Vector<T,4> Inv3(Vec0 * Fac2 - Vec1 * Fac4 + Vec2 * Fac5);

	Vector<T,4> SignA(+1, -1, +1, -1);
	Vector<T,4> SignB(-1, +1, -1, +1);
	Matrix mat;
	mat.OpenGL_set_basis(Inv0 * SignA, Inv1 * SignB, Inv2 * SignA, Inv3 * SignB);
	
	

	Vector<T,4> Row0(mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0]);

	Vector<T,4> Dot0(Row0*m[0]);
	T Dot1 = (Dot0[0] + Dot0[1]) + (Dot0[2] + Dot0[3]);

 	T OneOverDeterminant = static_cast<T>(1) / Dot1;
 
	mat *= OneOverDeterminant;
	
 	return mat;
    }    
   
   static Matrix<T,4> OpenGL_glFrustum(double left, 
				       double right,
				       double bottom, 
				       double top,
				       double near,
				       double far) 
   {
    Matrix<T,4> mat;
    mat=T(0);
    mat.m[0][0]=(2* near)/(right-left);
    mat.m[1][1]=(2* near)/(top-bottom);
    mat.m[2][0]=(right+left)/(right-left);
    mat.m[2][1]=(top+bottom)/(top-bottom);
    mat.m[2][2]=-(far+near)/(far-near);
    mat.m[2][3]=-1;
    mat.m[3][2]=-(2*far*near)/(far-near);
    return mat;
   }
   
   
   static Matrix<T,4> OpenGL_glLooktAt( Vector<T,3> eye,
					Vector<T,3> center,
					Vector<T,3> up) 
   {
    Vector<T,3> f=center-eye;
    f.normalize();
    up.normalize();
    Vector<T,3> s=f.cross(up);
    Vector<T,3> sn=s;sn.normalize();
    Vector<T,3> u=sn.cross(f);
    Matrix<T,4> M;
    M=T(0);
    M.m[0][0]=s[0];
    M.m[1][0]=s[1];
    M.m[2][0]=s[2];
    M.m[0][1]=u[0];
    M.m[1][1]=u[1];
    M.m[2][1]=u[2];
    M.m[0][2]=-f[0];
    M.m[1][2]=-f[1];
    M.m[2][2]=-f[2];
    M.m[3][3]=1;
    Matrix<T,4> t;
    t.Identity();
    t.OpenGL_set_tranV(eye*-1);
    
    return (M*t);
   }
   
   
    
   
    static Matrix<T,4> OpenGL_glTranslate(const Vector<T,3>  & vec) 
    {
       Matrix<T,4> mat;
       mat.Identity();
       mat.OpenGL_set_tranV(vec);
       return mat;
    }

    static Matrix<T,4> OpenGL_glScale(const Vector<T,3>  & vec) 
    {
       Matrix<T,4> mat;
       mat.Identity();
       mat.m[0][0]=vec[0];
       mat.m[1][1]=vec[1];
       mat.m[2][2]=vec[2];
       
       return mat;
    }
   
//    const Matrix<T,4> & OpenGL_glRotate( T angle,
// 					Vector<T,3> n)
//    {
//      
//      angle*=M_PI/180;
// //       T & x= n[0];
// //       T & y= n[1];
// //       T & z= n[2];
//       T & x= n[0];
//       T & y= n[1];
//       T & z= n[2];
//       T c=std::cos(angle);
//       T s=std::sin(angle);
//       
//       m[0][0]=x*x*(1-c)+c;
//       m[0][1]=x*y*(1-c)+z*s;
//       m[0][2]=x*z*(1-c)-y*s;
//       m[0][3]=0;
//       
//       m[1][0]=y*x*(1-c)-z*s;
//       m[1][1]=y*y*(1-c)+c;
//       m[1][2]=y*z*(1-c)+x*s;
//       m[1][3]=0;
//       
//       m[2][0]=x*z*(1-c)+y*s;
//       m[2][1]=y*z*(1-c)-x*s;
//       m[2][2]=z*z*(1-c)+c;
//       m[2][3]=0;
//       
//       
//       
//       m[3][0]=0;
//       m[3][1]=0;
//       m[3][2]=0;
//       m[3][3]=1;
//      
//       return *this;
//    }
   
   static Matrix<T,4>  OpenGL_glRotate( T angle,
					Vector<T,3> n)
   {
     Matrix<T,4> mat;
     angle*=M_PI/180;
//       T & x= n[0];
//       T & y= n[1];
//       T & z= n[2];
      T & x= n[0];
      T & y= n[1];
      T & z= n[2];
      T c=std::cos(angle);
      T s=std::sin(angle);
      
      mat.m[0][0]=x*x*(1-c)+c;
      mat.m[0][1]=x*y*(1-c)+z*s;
      mat.m[0][2]=x*z*(1-c)-y*s;
      mat.m[0][3]=0;
      
      mat.m[1][0]=y*x*(1-c)-z*s;
      mat.m[1][1]=y*y*(1-c)+c;
      mat.m[1][2]=y*z*(1-c)+x*s;
      mat.m[1][3]=0;
      
      mat.m[2][0]=x*z*(1-c)+y*s;
      mat.m[2][1]=y*z*(1-c)-x*s;
      mat.m[2][2]=z*z*(1-c)+c;
      mat.m[2][3]=0;
      
      
      
      mat.m[3][0]=0;
      mat.m[3][1]=0;
      mat.m[3][2]=0;
      mat.m[3][3]=1;
     
      return mat;
   }
   
   
  static Matrix<T,4> OpenGL_glRotateN12N2(Vector<T,3> &n1,Vector<T,3> &n2)
  {
			      Matrix<T,4>  rotation;
			      rotation.Identity();
			      T t=n1.dot(n2);
			      
			      //if (std::abs(t)>0.9999)
			      //T tabs=std::abs(t);
			      
			      Vector<T,3> rot_ax;
			      
			      if (t>0.999)
			      {
				return rotation;
			      }
			      if (t<-0.999)
			      {
				//return (rotation*T(-1));
				
				Vector<float,3> tmp;
				mhs_graphics::createOrths(n1,rot_ax, tmp);
			      }else
			      {
				rot_ax=n2.cross(n1);
				rot_ax.normalize();
			      }
    
			      
			      
			      
			      T & ux=rot_ax[0];
			      T & uy=rot_ax[1];
			      T & uz=rot_ax[2];
			      T & ct=t;
			      
			      T st=std::sqrt(1-t*t);

			      {
			        rotation.m[0][0]=ct+ux*ux*(1-ct);
				rotation.m[0][1]=ux*uy*(1-ct)-uz*st;
				rotation.m[0][2]=ux*uz*(1-ct)+uy*st;
				rotation.m[0][3]=0;
				
				rotation.m[1][0]=uy*ux*(1-ct)+uz*st;
				rotation.m[1][1]=ct+uy*uy*(1-ct);
				rotation.m[1][2]=uy*uz*(1-ct)-ux*st;
				rotation.m[1][3]=0;
				
				rotation.m[2][0]=uz*ux*(1-ct)-uy*st;
				rotation.m[2][1]=uz*uy*(1-ct)+ux*st;
				rotation.m[2][2]=ct+uz*uz*(1-ct);
				rotation.m[2][3]=0;
			      }
			      return rotation;
			      
			         
			      /*
			      T c=n1.dot(n2);
			      
			      //v.normalize();
			      
			       if (c>0.999)
			      {
				return rotation;
			      }
			      if (c<-0.999)
			      {
				return (rotation*T(-1));
			      }		
			      
			      Vector<T,3> v=n1.cross(n2);
			      //T s=std::sqrt(v.norm2());
			      T s_sqr=(v.norm2());
			      
			      
			      
			      
			      T & v1=v[0];
			      T & v2=v[1];
			      T & v3=v[2];
			      
			      Matrix<T,3>  tmp;
			      
			      
			      T w=(1-c)/s_sqr;

			      {
			        tmp.m[0][0]=0;
				tmp.m[0][1]=-v3;
				tmp.m[0][2]=v2;
				
				tmp.m[1][0]=v3;
				tmp.m[1][1]=0;
				tmp.m[1][2]=-v1;
				
				tmp.m[2][0]=-v2;
				tmp.m[2][1]=v1;
				tmp.m[2][2]=0;
			      }
			      
			      tmp+=(tmp*tmp)*w;
			      
			      
			   
    
			      for (int x=0;x<3;x++)
			      {
				for (int y=0;y<3;y++)
				{
				  rotation.m[x][y]+=tmp.m[x][y];
				}
				
			      }
			      
			         return rotation;*/
  }

   
   
};






















#endif


