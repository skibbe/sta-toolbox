#ifndef MHS_INTERP_H
#define MHS_INTERP_H

//#include "nrutil.h"


template<typename T>
void spline(const  T * x, const  T *  y, int n, T * y2, T yp1=0.99e31, T ypn=0.99e31)
/*
    Given arrays x[1..n] and y[1..n] containing a tabulated function, i.e., yi = f(xi), with
    x1 < x2 <... < xN , and given values yp1 and ypn for the first derivative of the interpolating
    function at points 1 and n, respectively, this routine returns an array y2[1..n] that contains
    the second derivatives of the interpolating function at the tabulated points xi. If yp1 and/or
    ypn are equal to 1 × 1030 or larger, the routine is signaled to set the corresponding boundary
    condition for a natural spline, with zero second derivative on that boundary.
*/
 {
n-=1;
    
    
    int i,k;
//     T p,qn,sig,un,*u;
    T p,qn,sig,un;
    
    //u=vector(1,n-1);
    T u[n];
    
    if (yp1 > 0.99e30) //The lower boundary condition is set either to be “natural”
    {
      //y2[1]=u[1]=0.0;
      y2[0]=u[0]=0.0;
    } else 
    { 		//or else to have a specified first derivative.
//       y2[1] = -0.5;
//       u[1]=(3.0/(x[2]-x[1]))*((y[2]-y[1])/(x[2]-x[1])-yp1);
      y2[0] = -0.5;
      u[0]=(3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-yp1);
    }
    
//     printf(" %f %f | %f  %f \n",u[0],y2[0],(3.0/(x[1]-x[0])),((y[1]-y[0])/(x[1]-x[0])-yp1)); 
//     printf(" %f %f | %f  %f %f\n",u[0],y2[0],(y[1]-y[0]),(x[1]-x[0]),yp1); 


    //for (i=2;i<=n-1;i++)  
    for (i=1;i<=n-1;i++)  
      /*This is the decomposition loop of the tridiagonal algorithm.
       * storage of the decomposed factors.
       */
    { 
      sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
      p=sig*y2[i-1]+2.0;
      y2[i]=(sig-1.0)/p;
      u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
      u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
    }
    
    if (ypn > 0.99e30) //The upper boundary condition is set either to be “natural”
    {
    qn=un=0.0;
    }
    else  //or else to have a specified first derivative.
    { 
      qn=0.5;
      un=(3.0/(x[n]-x[n-1]))*(ypn-(y[n]-y[n-1])/(x[n]-x[n-1]));
    }
      y2[n]=(un-qn*u[n-1])/(qn*y2[n-1]+1.0);
      for (k=n-1;k>=0;k--) //This is the backsubstitution loop of the tridiagonal algorithm.
      //for (k=n-1;k>=1;k--) //This is the backsubstitution loop of the tridiagonal algorithm.
      {
	y2[k]=y2[k]*y2[k+1]+u[k]; 
      }
      
//     for (int a=0;a<n+1;a++)  
//     {
//      //printf("%d : f(%f)=%f \n",a,x[a],y[a]); 
//      printf("%d : %f %f \n",a,u[a],y2[a]); 
//     }
    //free_vector(u,1,n-1);
}

template<typename T>
void splint(const  T * xa, const T * ya,const   T * y2a, int n, T x, T *y)
/*
Given the arrays xa[1..n] and ya[1..n], which tabulate a function (with the xai’s in order),
and given the array y2a[1..n], which is the output from spline above, and given a value of
x, this routine returns a cubic-spline interpolated value y.
*/
{
  
 
  //void nrerror(char error_text[]);
  int klo,khi,k;
  T h,b,a;
  klo=1;
  /*
    We will find the right place in the table by means of
    bisection. This is optimal if sequential calls to this
    routine are at random values of x. If sequential calls
    are in order, and closely spaced, one would do better
    to store previous values of klo and khi and test if
    they remain appropriate on the next call.
  */
  khi=n;
  while (khi-klo > 1) {
    k=(khi+klo) >> 1;
    if (xa[k-1] > x)
    {
      khi=k;
    }
    else 
    {
      klo=k;
    }
  } // klo and khi now bracket the input value of x.
  h=xa[khi-1]-xa[klo-1];
  if (h == 0.0) 
  {
    sta_assert_error(1==0); //The xa’s must be distinct.
  }
  a=(xa[khi-1]-x)/h;
  b=(x-xa[klo-1])/h; //Cubic spline polynomial is now evaluated.
  *y=a*ya[klo-1]+b*ya[khi-1]+((a*a*a-a)*y2a[klo-1]+(b*b*b-b)*y2a[khi-1])*(h*h)/6.0;
  //*y=a*ya[klo-1]+b*ya[khi-1]*(h*h)/6.0;
}


#endif