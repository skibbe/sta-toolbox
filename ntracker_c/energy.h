#ifndef ENERGY_H
#define ENERGY_H
#include "rand_help.h"
#include "specialfunc.h"
#include <numeric>


#include "proposals.h"
#include "edgecost.h"
#include "global.h"
#include "ellipsoid_colldet.h"
#include <iostream>
#include <fstream>

// static bool normal_scale=true;


//#define _DEBUG_COLLISION_CHECKS_


// enum class PROPOSAL_TYPES;
template<typename T,int Dim> class Points;
template<typename T,int Dim> class Connection;
template<typename T,int Dim> class TConnecTProposal;

template <typename T>
// T proposal_temp_trafo(T value,T scale) {return std::sqrt(value)/scale;};
T proposal_temp_trafo ( T value,T scale, T min_v=T(0.1))
{
    return value/scale+min_v;
};



template <typename T>
T update_energy_particle_costs ( T lambda )
{
    //return -std::log ( lambda );
  return 0;
};





template<typename T,int Dim>
class Collision
{
  protected:
  int verbosity;
  
  public:
    
  class Candidates
  {
    public:
    std::size_t found;
    class OctPoints<T,Dim> ** p_query_buffer;
    //class OctPoints<T,Dim> * query_buffer[query_buffer_size];
    Candidates(class OctPoints<T,Dim> ** p_query_buffer)
    {
      found =0;
      this->p_query_buffer=p_query_buffer;
    }
    
    Candidates()
    {
     sta_assert_error_m(1!=1,"please init Candidates class\n"); 
    }
    
  };
    
    
  Collision()
  {
    verbosity=0;
  }

  
  
  void set_verbosity(int d){verbosity=d;};
  int get_verbosity(){return verbosity;};
    
  virtual bool colliding(
		  T & soft_collision_costs,
		  OctTreeNode<T,Dim> & tree,
                  const OctPoints<T,Dim> & npoint,
                  T searchrad,
		  T temp,
		  Candidates * point_candidates=NULL,
		  bool update=true,
		  T point_candidates_update_max_displacement=-10,
		  int mode=0
		  //,PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED
			)
  {sta_assert_error_m(false,"not implemented");return false;};
  
  virtual bool colliding(
		  T & soft_collision_costs,
		  const OctPoints<T,Dim> & pointA,
		  const OctPoints<T,Dim> & pointB,
		  T temp,
		  int mode=0,
		    T * candidate_center_dist_sq_ptr=NULL,
		  T * mindist_ptr=NULL
			)
  {sta_assert_error_m(false,"not implemented");return false;};
		  
		  
  bool colliding(
		  OctTreeNode<T,Dim> & tree,
                  const OctPoints<T,Dim> & npoint,
                  T searchrad,T temp)
	      {
		  Points<T,Dim> & newpoint= * ( ( Points<T,Dim>  * ) &npoint );
		  
		  // NOTE: currently bifurcation centers can only do HARD collisions
		    sta_assert_error(newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
		    
		    
		    T soft_collision_costs_dummy=0;
		    bool result=colliding(
		      soft_collision_costs_dummy,
			tree,
		      npoint,
		      searchrad,temp);
		    
		    sta_assert_error(std::abs(soft_collision_costs_dummy)<std::numeric_limits< T >::epsilon());
		    
		    return result;
		  };		  
		  
//   virtual T colliding_soft(
// 		  OctTreeNode<T,Dim> & tree,
//                   OctPoints<T,Dim> & npoint,
//                   T searchrad){sta_assert_error_m(false,"not implemented");return 0;};

		  
  virtual  void set_params ( const mxArray * params=NULL )=0; 
  
  
  virtual  bool is_soft (){return false;}; 
  
//   virtual  T get_search_radius_scaling_fact(){return 1;}; 
};


template<typename T,int Dim>
class TabletCollision: public Collision<T,Dim>
{
protected:
 T discard_value;
 bool semi_soft_collision;
 bool soft_collision;
 T semi_soft_collision_value;
 unsigned int power;
 bool no_bifurcation_self_collision;
 bool terminals_collision;
 T clean_term_fact;
 T non_collision_angle;
 T max_clean_term_ratio;
 T inner_sphere_extra;
bool bifurcation_particle_soft;
T bifurcation_particle_soft_scale;
bool intra_path_only;


public:
    
  
  TabletCollision () : Collision<T,Dim>()
  {
    discard_value=-2;
    semi_soft_collision=false;
    soft_collision=false;
    semi_soft_collision_value=0;
    power=2;
    no_bifurcation_self_collision=true;
    terminals_collision=false;
    clean_term_fact=0.01;
    max_clean_term_ratio=1;
    non_collision_angle=-1;
    inner_sphere_extra=-1;
 bifurcation_particle_soft=false;
 bifurcation_particle_soft_scale=0;
 intra_path_only = false;
  }
  
  
   bool is_soft (){return (semi_soft_collision);}; 
   
   bool colliding(
		  T & soft_collision_costs,
		  const Points<T,Dim> & newpoint,
		  const Points<T,Dim> & point,
		  T temp,
		  int mode=0,
		  T * candidate_center_dist_sq_ptr=NULL,
		  T * mindist_ptr=NULL
			)
    {
        
        
        
        
	      bool is_terminal=(newpoint.get_num_connections()==1);
	      bool is_full_segment=(newpoint.get_num_connections()==2);
	      bool is_single=(newpoint.get_num_connections()==0);
	      bool is_bif=( newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
	      
	      bool is_terminal_other=(point.get_num_connections()==1);
	      bool is_fullseg_other=(point.get_num_connections()==2);
	      bool is_single_other=(point.get_num_connections()==0);
	      bool is_bif_other=( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ;
	      
	      //bool connected=(newpoint.is_connected_with(&point));
	      bool clean_terminals=(
		(clean_term_fact>0)&& 
		( (is_terminal || is_terminal_other ) || (is_single&&is_single_other) || (newpoint.is_connected_with(&point)) )
	      );
// 	      bool clean_terminals=(
// 		(clean_term_fact>0)&& 
// 		( (!is_fullseg_other) || (!is_full_segment) || (newpoint.is_connected_with(&point)) )
// 	      );
 	      //bool clean_bifurcation=(is_bif_other && is_bif  && connected);

	      
	      
	    
	      T   cand_size=point.get_scale();

	      T candidate_center_dist_sq= ( point.get_position()-newpoint.get_position() ).norm2();

	      T thickinessA=newpoint.get_thickness();
	      T thickinessB=point.get_thickness();

	      T scaleA=newpoint.get_scale();
	      T scaleB=point.get_scale();

	      T mindist=std::max ( thickinessA,scaleA ) +std::max ( scaleB,thickinessB );
	      //T mindist=scaleA+scaleB;
	      
	      
	      if (candidate_center_dist_sq_ptr!=NULL)
	      {
		sta_assert_debug0(mindist_ptr!=NULL);
		if (mindist<1)
		{
		*candidate_center_dist_sq_ptr=candidate_center_dist_sq;
		*mindist_ptr=mindist;
		}
	      }
	      
	    
		  
		if ((mode==1) && (!clean_terminals))
		{
		  return false;
		}
		
		if (intra_path_only && (newpoint._path_id!=point._path_id))
        {
                return false;
        }
	      
	      
	      if ( ! ( candidate_center_dist_sq>mindist*mindist ) ) {

		  bool check_collision=true;
		  
		  //if ((sort_cand_list) && (!((std::sqrt(candidate_center_dist_sq)+point_candidates_update_max_displacement)>maxdist)))
		  
		  bool bifurcation_involved= ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ||
			      ( newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ;

		  //check for bifurcation self collision
		  if ((no_bifurcation_self_collision)&&
			  bifurcation_involved
			  ){
		      const Points<T,Dim> * points[2];
		      points[0]=&newpoint;
		      points[1]=&point;
		      int bif_n_point= ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ? 0 : 1;
		      for ( int i=0; i<2; i++ ) {
			  if ( ( points[bif_n_point]->endpoint_connections[i]>0 )
				  && ( points[bif_n_point]->endpoints[i][0]->connected->point==points[1-bif_n_point] ) ) {
			      sta_assert_debug0 ( point.particle_type!=newpoint.particle_type );
			      check_collision=false;
			  }
		      }
		  //both particles are ordianry particles: check if they can see each other (angle)    
		  } else { 
		      if (( discard_value>0 )&&(!bifurcation_involved)) {
			  T value=newpoint.get_direction().dot ( point.get_direction() );
			  if ( std::abs ( value ) <discard_value ) {
			      check_collision=false;
			  }
		      }
		  }


		  if ( check_collision ) {

		      T inner_sphereA_rad=std::min ( thickinessA,scaleA );
		      T inner_sphereB_rad=std::min ( thickinessB,scaleB );
		    
		      T maxdist=inner_sphereA_rad+inner_sphereB_rad;
		      if ((!bifurcation_particle_soft)&&(((!soft_collision)||(bifurcation_involved))&&( ! ( candidate_center_dist_sq>maxdist*maxdist ) ))) {
			  return true;
		      }
		      
		      
		      

		      if (!bifurcation_particle_soft)
		      {
		      //NOTE only for debugging: should never happen
		      if ( ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) &&
			      ( newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
			  printf ( "collision:\n" );
			  point.get_position().print();
			  newpoint.get_position().print();
			  printf ( "2: %d \n",check_collision );
		      }
		      }
		      
		      
		
		      
  //                     //NOTE bifurcations are only colliding with inner circle
  //                     if (bifurcation_involved) {
  // 		      continue;
  // 		    }
		      

		      bool collision= ( ellipsoids_coolide (
					    newpoint.get_position(),
					    point.get_position(),
					    newpoint.get_direction(),
					    point.get_direction(),
					    thickinessA,
					    scaleA,
					    thickinessB,
					    scaleB ) );

		      
		      if (bifurcation_involved && bifurcation_particle_soft)
		      {
			if (collision)
			{
			  soft_collision_costs+=bifurcation_particle_soft_scale*(scaleA+scaleB)/2;;
			}
			collision=false;
		      }
		      
		      if (!bifurcation_particle_soft)
		      {
		      if ( ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) &&
			      ( newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
			  printf ( "collisin:\n" );
			  point.get_position().print();
			  newpoint.get_position().print();
			  printf ( "%d %d\n",check_collision,collision );
		      }
		      }
		      if ( collision ) {
			
			//if (semi_soft_collision)
			//NOTE bifurcations do not softly collide :(
			// because; birfucation track birth creates 2 particels a time
			// --> energy computation problematic because of symmetry 
			// --> it wouldbe necessary to exclude two particles from collision search
			//	in second run
			
			
			if (semi_soft_collision && (!bifurcation_involved))
			{
  //  			if ((point->get_num_connections()==1)||(is_terminal))
  //  			{
  //  			  soft_collision_costs+=(scaleA+scaleB)/2;
  //  			}else
			  {
// 			      const bool inner_sphere_extra=false;
// 			      if ((inner_sphere_extra)&&(soft_collision)&&(!( candidate_center_dist_sq>maxdist*maxdist ) )) {
// 				  //soft_collision_costs+=(scaleA+scaleB)/2;
// 				  T min_rad=std::min(inner_sphereA_rad,inner_sphereB_rad);
// 				  T inv_penetration_depth=(maxdist-std::sqrt(candidate_center_dist_sq))/min_rad;
// 				  if (inv_penetration_depth>1) //infinity
// 				  {
// 				  return true; 
// 				  }
// 				  sta_assert_error(!(inv_penetration_depth<0));
// 				  T p=(1-inv_penetration_depth);
// 				  //printf("%f : %f \n",p,(1/(p*p+std::numeric_limits<T>::epsilon())-1));
// 				  soft_collision_costs+=((scaleA+scaleB)/2)*(1/(p+std::numeric_limits<T>::epsilon())-1);
// 			      }else
			      {
				
				
				
				if (mode!=1) 
				{
				  
				  //const bool inner_sphere_extra=false;
				  //if ((inner_sphere_extra>0)&&(!( candidate_center_dist_sq>maxdist*maxdist ) )) {
				  //if ((inner_sphere_extra>0)&&((!is_single)||(!is_single_other)))
				  if (inner_sphere_extra>0)
				  {
				    
				      soft_collision_costs+=inner_sphere_extra*(scaleA+scaleB)/2;
				  }
				  
				  
				  if (terminals_collision && (is_terminal || is_terminal_other))
				  {
				    soft_collision_costs+=(scaleA+scaleB)/2;
				  }else
				  {
				      
// 				      if (newpoint.is_connected_with(&point))
// 				      {
// 					// NOTE making non-segments hard dows not works yet
// 					 soft_collision_costs+=(scaleA+scaleB)/2;;
// 					 //return true;
// 					
// 				      }else  
					
				      {
					T cosa=newpoint.get_direction().dot(point.get_direction());
					if (non_collision_angle>-1)
					{
					  if (std::abs(cosa)<non_collision_angle)
					  {
					    soft_collision_costs+=(scaleA+scaleB)/2;
					  }else 
					  {
					  printf("%f\n",std::abs(cosa)); 
					  }
					}else
					{
					  soft_collision_costs+=mhs_fast_math<T>::pow(cosa*cosa,power)*(scaleA+scaleB)/2;
					}
				      }
				  }
				} else
				{
				 sta_assert_error(clean_terminals);
				}
				
				if (clean_terminals)
				{
				  
				  if (false)
				  {
				      T clean_fact=(scaleA+scaleB)/2*std::min(clean_term_fact/(temp),max_clean_term_ratio);
    // 				  T max_cost=(scaleA+scaleB)/2;
    // 				  T clean_fact=std::min(max_cost*clean_term_fact/(temp),max_clean_term_ratio*max_cost);
				      
				      if (semi_soft_collision)
				      {
					clean_fact/=semi_soft_collision_value;
				      }
				      soft_collision_costs+=clean_fact;
				  }else
				  {
				    // NOTE making non-segments hard dows not works yet
				    //return true;
				    //soft_collision_costs+=(clean_bifurcation+1)*clean_term_fact*(scaleA+scaleB)/2;;
				    soft_collision_costs+=clean_term_fact*(scaleA+scaleB)/2;;
				  }
				  
				}
				
				if ((candidate_center_dist_sq_ptr!=NULL)&&(mindist>0))
				{
				 *mindist_ptr=-1;
				}
			      }
			  }
			  
			}else
			{
			  return true;
			}
		      }
		  }
		  
	      
	      }
      
	return false;
      
    }
  
  
  bool colliding(
		  T & soft_collision_costs,
		  OctTreeNode<T,Dim> & tree,
                  const OctPoints<T,Dim> & npoint,
                  T searchrad,
		  T temp,
		  class Collision<T,Dim>::Candidates * point_candidates=NULL,
		 bool update=true,
		 T point_candidates_update_max_displacement=-10, 
			  // set -9: collect all currently colliding pts only, 
			  //-10: off
			  // value : collect all candidates in searchrad value
		 int mode=0
		  //,PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED
		)
  {
    
    if (((mode==1))&&(!(clean_term_fact>0)))
    {
      soft_collision_costs=0;
      return false; 
    }
    
    Points<T,Dim> & newpoint= * ( ( Points<T,Dim>  * ) &npoint );


    
    //TODO dynamically set to 0 if point_candidates!=NULL
    
    class OctPoints<T,Dim> * query_buffer[query_buffer_size*(point_candidates==NULL)];
    class Collision<T,Dim>::Candidates cand(query_buffer);
    
    
//     std::size_t found;
//     class OctPoints<T,Dim> * query_buffer[query_buffer_size];
    class OctPoints<T,Dim> ** candidates;
    
    if (point_candidates==NULL)
    {
      point_candidates=&cand;
    }
    
    candidates=point_candidates->p_query_buffer;
    std::size_t & found=point_candidates->found;

    bool sort_cand_list=false;
    std::size_t last_valid_candidate=0;
    
    
    
    if (update)
    {
      try {
	  tree.queryRange (
	      newpoint.get_position(),searchrad,candidates,query_buffer_size,found );
      } catch ( mhs::STAError & error ) {
	  throw error;
      }
      sort_cand_list=(point_candidates_update_max_displacement>-10);
    }
    
    
    soft_collision_costs=0;
    for ( std::size_t a=0; a<found; a++ ) {
        Points<T,Dim> * point= ( Points<T,Dim> * ) candidates[a];

        sta_assert_debug0 ( ( point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) || ( ! ( point->get_direction() [0]<1 ) ) );

	T * candidate_center_dist_sq_ptr=NULL;
	T * mindist_ptr=NULL;
	
	T candidate_center_dist_sq;
	T mindist;
	if (sort_cand_list) 	
	{
	  candidate_center_dist_sq_ptr=&candidate_center_dist_sq;
	  mindist=(point_candidates_update_max_displacement<-8);
	  mindist_ptr=&mindist;
	}
	
        if ( point!=&newpoint ) 
	{
	  
	    
	      if (colliding(
		  soft_collision_costs,
		  newpoint,
		  *point,
		  temp,
		  mode,
		  candidate_center_dist_sq_ptr,
		   mindist_ptr
			))
		return true;
	  
		if (sort_cand_list) 	
		{
		  if (mindist<0)
		  {
		    sta_assert_debug0(!(last_valid_candidate>a));
		    std::swap(candidates[a],candidates[last_valid_candidate]);
		    last_valid_candidate++;
		  }else
		  {
		      if (  
			//((candidate_center_dist_sq<0)&&(!(point_candidates_update_max_displacement)>mindist))
			(point_candidates_update_max_displacement<0)
			||
			(!((std::sqrt(candidate_center_dist_sq)-point_candidates_update_max_displacement)>mindist))
		      )
		      {
		      sta_assert_debug0(!(last_valid_candidate>a));
		      std::swap(candidates[a],candidates[last_valid_candidate]);
		      last_valid_candidate++;
		      }
		  }
		}

        }
    }
    
    if (semi_soft_collision)
    {
     soft_collision_costs*=semi_soft_collision_value; 
    }
      
    
    if (sort_cand_list)
    {
      found=last_valid_candidate;
    }
    
    return false;
    
  }
  
  void set_params ( const mxArray * params=NULL )
  {
     if ( params==NULL ) {
            return;
        }
        try {
            if ( mhs::mex_hasParam ( params,"discard_value" ) !=-1 ) {
                discard_value=mhs::mex_getParam<T> ( params,"discard_value",1 ) [0];
            }
            
            if ( mhs::mex_hasParam ( params,"semi_soft_collision" ) !=-1 ) {
                semi_soft_collision_value=mhs::mex_getParam<T> ( params,"semi_soft_collision",1 ) [0];
            }
            
            if ( mhs::mex_hasParam ( params,"semi_soft_collision_power" ) !=-1 ) {
                power=mhs::mex_getParam<T> ( params,"semi_soft_collision_power",1 ) [0];
            }
            
            if ( mhs::mex_hasParam ( params,"no_bifurcation_self_collision" ) !=-1 ) {
                no_bifurcation_self_collision=mhs::mex_getParam<bool> ( params,"no_bifurcation_self_collision",1 ) [0];
            }

            if ( mhs::mex_hasParam ( params,"terminals_collision" ) !=-1 ) {
                terminals_collision=mhs::mex_getParam<bool> ( params,"terminals_collision",1 ) [0];
            }          
            
            if ( mhs::mex_hasParam ( params,"intra_path_only" ) !=-1 ) {
                intra_path_only=mhs::mex_getParam<bool> ( params,"intra_path_only",1 ) [0];
                if (intra_path_only)
                {
                    printf("####################\n");
                    printf("INTRA PATH COLLISION ONLY\n");
                    printf("####################\n");
                }
            }          
            
            
            
            if ( mhs::mex_hasParam ( params,"clean_term_fact" ) !=-1 ) {
                clean_term_fact=mhs::mex_getParam<T> ( params,"clean_term_fact",1 ) [0];
            }            
            
            if ( mhs::mex_hasParam ( params,"max_clean_term_ratio" ) !=-1 ) {
                max_clean_term_ratio=mhs::mex_getParam<T> ( params,"max_clean_term_ratio",1 ) [0];
            }            
            
            
            
            if ( mhs::mex_hasParam ( params,"non_collision_angle" ) !=-1 ) {
                non_collision_angle=mhs::mex_getParam<T> ( params,"non_collision_angle",1 ) [0];
            }            
            
            
            if ( mhs::mex_hasParam ( params,"inner_sphere_extra" ) !=-1 ) {
                inner_sphere_extra=mhs::mex_getParam<T> ( params,"inner_sphere_extra",1 ) [0];
            }            
             

//                             if ( mhs::mex_hasParam ( params,"bifurcation_particle_soft" ) !=-1 ) {
//                     bifurcation_particle_soft=mhs::mex_getParam<bool> ( params,"bifurcation_particle_soft",1 ) [0];
//                 }
            
                           if ( mhs::mex_hasParam ( params,"bifurcation_particle_soft_scale" ) !=-1 ) {
                    bifurcation_particle_soft_scale=mhs::mex_getParam<T> ( params,"bifurcation_particle_soft_scale",1 ) [0];
                }
                
            bifurcation_particle_soft=(bifurcation_particle_soft_scale>0);
            
            
            
            sta_assert_error(power%2==0);
//             printf("\n\n %f \n\n",semi_soft_collision_value);
            
            semi_soft_collision=(semi_soft_collision_value>0);
	    
	    if ( mhs::mex_hasParam ( params,"soft_collision" ) !=-1 ) {
               soft_collision=mhs::mex_getParam<bool> ( params,"soft_collision",1 ) [0];
            }
	    
	    if (soft_collision)
	      sta_assert_error(semi_soft_collision);
	    
	    
            if (soft_collision)
	    {
	      printf("soft collision\n");
	    }
	    else
	    {
	      if (semi_soft_collision)
	      {
		printf("using semi soft collision\n");
	      }
	    }
	    
//             if (semi_soft_collision)
// 	    {
//             clean_term_fact/=semi_soft_collision_value;
// 	    
// 	    }
	    
	    if (!(non_collision_angle<0))
	    {
	      T tmp=std::cos(2*M_PI*non_collision_angle/T(360));
	      printf("no collision angle: %f (%f)\n",non_collision_angle,tmp);
	      non_collision_angle=tmp;
	    }

        } catch ( mhs::STAError & error ) {

            throw error;
        }
  }
};



template<typename T,int Dim>
class DTISoftCollision : public Collision<T,Dim>
{
protected:
 T search_rad_scale;
 T soft_collision_cost_scale;
public:
  
  bool is_soft (){return true;}; 
    
  
  DTISoftCollision  () : Collision<T,Dim>()
  {
    search_rad_scale=M_PI;
    soft_collision_cost_scale=1;
  }
  
  bool colliding(
		  T & soft_collision_costs,
		  const Points<T,Dim> & newpoint,
		  const Points<T,Dim> & point,
		  T temp,
		  int mode=0,
		  T * candidate_center_dist_sq_ptr=NULL,
		  T * mindist_ptr=NULL
			)
      {
	return false;
      }
  
  bool colliding(
		  T & soft_collision_costs,
		  OctTreeNode<T,Dim> & tree,
                  const OctPoints<T,Dim> & npoint,
                  T searchrad,
		 T temp,
		  class Collision<T,Dim>::Candidates * point_candidates=NULL,
		 bool update=true,T point_candidates_update_max_displacement=-10,
		 int mode=0
		// ,PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED
		)
  {
    
    
//     soft_collision_costs=0;
//     return false;
    
//     printf("%f %f \n",searchrad,search_rad_scale);
    
    
    
    
    
    Points<T,Dim> & newpoint= * ( ( Points<T,Dim>  * ) &npoint );

    
    T scale=newpoint.get_scale();
    T scale2=scale*scale;
    
    searchrad*=search_rad_scale*scale;
    
    //TODO dynamically set to 0 if point_candidates!=NULL
//     class Collision<T,Dim>::Candidates cand;
    class OctPoints<T,Dim> * query_buffer[query_buffer_size*(point_candidates==NULL)];
    class Collision<T,Dim>::Candidates cand(query_buffer);
//     std::size_t found;
//     class OctPoints<T,Dim> * query_buffer[query_buffer_size];
    class OctPoints<T,Dim> ** candidates;
    
    
    if (point_candidates==NULL)
    {
      point_candidates=&cand;
    }
    
    candidates=point_candidates->p_query_buffer;
    std::size_t & found=point_candidates->found;

    if (update)
    {
      try {
	  tree.queryRange (
	      newpoint.get_position(),searchrad,candidates,query_buffer_size,found );
      } catch ( mhs::STAError & error ) {
	  throw error;
      }
    }
    
    sta_assert_debug0 ( ( newpoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );
    
    soft_collision_costs=0;
    for ( std::size_t a=0; a<found; a++ ) {
        Points<T,Dim> * point= ( Points<T,Dim> * ) candidates[a];

        sta_assert_debug0 ( ( point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );

        if ( point!=&newpoint ) {
//             T   cand_size=point->get_scale();

            T candidate_center_dist_sq= ( point->get_position()-newpoint.get_position() ).norm2();
	    

            
//             T scaleA=newpoint.get_scale();
//             T scaleB=point->get_scale();
// 
//             T mindist=(scaleA+scaleB)/search_rad_scale;
	    
		T mindist=2*scale/search_rad_scale;
	    
//             T mindist=scaleA+scaleB;

            if ( ! ( candidate_center_dist_sq>mindist*mindist ) ) {
		  soft_collision_costs+=mhs_fast_math<T>::mexp(candidate_center_dist_sq/(scale2));
            }

        }
    }
    soft_collision_costs*=soft_collision_cost_scale;
    return false;
    
  }
  
  void set_params ( const mxArray * params=NULL )
  {
     if ( params==NULL ) {
            return;
        }
        try {
            if ( mhs::mex_hasParam ( params,"search_rad_scale" ) !=-1 ) {
                search_rad_scale=mhs::mex_getParam<T> ( params,"search_rad_scale",1 ) [0];
            }
            if ( mhs::mex_hasParam ( params,"soft_collision_cost_scale" ) !=-1 ) {
                soft_collision_cost_scale=mhs::mex_getParam<T> ( params,"soft_collision_cost_scale",1 ) [0];
            }
            

        } catch ( mhs::STAError & error ) {

            throw error;
        }
  }
  
};




template<typename T,int Dim>
class SimpleCollision : public Collision<T,Dim>
{
protected:
public:
  
  bool is_soft (){return false;}; 
    
  
  SimpleCollision  () : Collision<T,Dim>()
  {
  }
  
  bool colliding(
		  T & soft_collision_costs,
		  const Points<T,Dim> & newpoint,
		  const Points<T,Dim> & point,
		  T temp,
		  int mode=0,
		  T * candidate_center_dist_sq_ptr=NULL,
		  T * mindist_ptr=NULL
			)
  {
    return false;
  }
  
  bool colliding(
		  T & soft_collision_costs,
		  OctTreeNode<T,Dim> & tree,
                  const OctPoints<T,Dim> & npoint,
                  T searchrad,
		 T temp,
		  class Collision<T,Dim>::Candidates * point_candidates=NULL,
		 bool update=true,T point_candidates_update_max_displacement=-10
		// ,PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED
		)
  {
    
    
    
    
    
    Points<T,Dim> & newpoint= * ( ( Points<T,Dim>  * ) &npoint );

    
    
    
    
    //TODO dynamically set to 0 if point_candidates!=NULL
//     class Collision<T,Dim>::Candidates cand;
    class OctPoints<T,Dim> * query_buffer[query_buffer_size*(point_candidates==NULL)];
    class Collision<T,Dim>::Candidates cand(query_buffer);
//     std::size_t found;
//     class OctPoints<T,Dim> * query_buffer[query_buffer_size];
    class OctPoints<T,Dim> ** candidates;
    
    
    if (point_candidates==NULL)
    {
      point_candidates=&cand;
    }
    
    candidates=point_candidates->p_query_buffer;
    std::size_t & found=point_candidates->found;

    if (update)
    {
      try {
	  tree.queryRange (
	      newpoint.get_position(),searchrad,candidates,query_buffer_size,found );
      } catch ( mhs::STAError & error ) {
	  throw error;
      }
    }
    
    sta_assert_debug0 ( ( newpoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );
    
    soft_collision_costs=0;
    for ( std::size_t a=0; a<found; a++ ) {
        Points<T,Dim> * point= ( Points<T,Dim> * ) candidates[a];

        sta_assert_debug0 ( ( point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );

        if ( point!=&newpoint ) {
//             T   cand_size=point->get_scale();

            T candidate_center_dist_sq= ( point->get_position()-newpoint.get_position() ).norm2();
	    

            
//             T scaleA=newpoint.get_scale();
//             T scaleB=point->get_scale();
// 
             
	      T scaleA=newpoint.get_scale();
	      T scaleB=point->get_scale();

	      T mindist=scaleA+scaleB;
	    
		//T mindist=2*scale/search_rad_scale;
	    
//             T mindist=scaleA+scaleB;

            if ( ! ( candidate_center_dist_sq>mindist*mindist ) ) {
		  return  true;
            }

        }
    }
    return false;
    
  }
  
  void set_params ( const mxArray * params=NULL )
  {
     if ( params==NULL ) {
            return;
        }
        try {
            
            

        } catch ( mhs::STAError & error ) {

            throw error;
        }
  }
  
};




#endif
