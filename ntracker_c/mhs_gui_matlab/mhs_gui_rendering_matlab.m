function varargout = mhs_gui_rendering_matlab(varargin)
% MHS_GUI_RENDERING_MATLAB MATLAB code for mhs_gui_rendering_matlab.fig
%      MHS_GUI_RENDERING_MATLAB, by itself, creates a new MHS_GUI_RENDERING_MATLAB or raises the existing
%      singleton*.
%
%      H = MHS_GUI_RENDERING_MATLAB returns the handle to a new MHS_GUI_RENDERING_MATLAB or the handle to
%      the existing singleton*.
%
%      MHS_GUI_RENDERING_MATLAB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MHS_GUI_RENDERING_MATLAB.M with the given input arguments.
%
%      MHS_GUI_RENDERING_MATLAB('Property','Value',...) creates a new MHS_GUI_RENDERING_MATLAB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mhs_gui_rendering_matlab_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mhs_gui_rendering_matlab_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mhs_gui_rendering_matlab

% Last Modified by GUIDE v2.5 03-Aug-2015 18:36:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mhs_gui_rendering_matlab_OpeningFcn, ...
                   'gui_OutputFcn',  @mhs_gui_rendering_matlab_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mhs_gui_rendering_matlab is made visible.
function mhs_gui_rendering_matlab_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mhs_gui_rendering_matlab (see VARARGIN)

% Choose default command line output for mhs_gui_rendering_matlab
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mhs_gui_rendering_matlab wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%gexists=exist('tracker_rendering_window_controls','var');
gexists=numel(who('tracker_rendering_window_controls','global'))>0;


global tracker_rendering_window_controls;

if ~gexists
    tracker_rendering_window_controls.volume_pixelDensityThreshold=0.1;
    tracker_rendering_window_controls.volume_pixelDensity=0.9;
    tracker_rendering_window_controls.changed=double(1);
    
    tracker_rendering_window_controls.volume_bgc=double([0,0,0]);
    tracker_rendering_window_controls.volume_lighting=double([0.5,0.5,0.5,4]);
    
    tracker_rendering_window_controls.color_mode=0;
    tracker_rendering_window_controls.parameter_gamma=1;
    
    tracker_rendering_window_controls.color=double([1,1,1]);
end;

tracker_rendering_window_controls.handles=handles;
tracker_rendering_window_controls.update_func=@update_gui;

tracker_rendering_window_controls.update_func();

function update_gui

fprintf('updating gui\n');
global tracker_rendering_window_controls;

tracker_rendering_window_controls.handles.slider1.Value=tracker_rendering_window_controls.volume_pixelDensityThreshold;
tracker_rendering_window_controls.handles.slider2.Value=tracker_rendering_window_controls.volume_pixelDensity;
tracker_rendering_window_controls.handles.slider2.Value=tracker_rendering_window_controls.volume_pixelDensity;
tracker_rendering_window_controls.handles.pushbutton1.ForegroundColor=tracker_rendering_window_controls.volume_bgc;
tracker_rendering_window_controls.handles.pushbutton1.BackgroundColor=1-tracker_rendering_window_controls.handles.pushbutton1.ForegroundColor;
tracker_rendering_window_controls.handles.slider3.Value=tracker_rendering_window_controls.volume_lighting(1);
tracker_rendering_window_controls.handles.slider4.Value=tracker_rendering_window_controls.volume_lighting(2);
tracker_rendering_window_controls.handles.slider5.Value=tracker_rendering_window_controls.volume_lighting(3);
tracker_rendering_window_controls.handles.slider6.Value=tracker_rendering_window_controls.volume_lighting(4);

tracker_rendering_window_controls.handles.slider25.Value=tracker_rendering_window_controls.color_mode;
tracker_rendering_window_controls.handles.slider26.Value=tracker_rendering_window_controls.parameter_gamma;

tracker_rendering_window_controls.handles.pushbutton2.BackgroundColor=tracker_rendering_window_controls.color;
%tracker_rendering_window_controls.handles.pushbutton2.BackgroundColor=1-tracker_rendering_window_controls.handles.pushbutton2.ForegroundColor;

tracker_rendering_window_controls.changed=double(0);
tracker_rendering_window_controls.debug_changed=double(0);

% --- Outputs from this function are returned to the command line.
function varargout = mhs_gui_rendering_matlab_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_pixelDensityThreshold=double((get(hObject,'Value')-get(hObject,'Min'))/(get(hObject,'Max')+get(hObject,'Min')));
tracker_rendering_window_controls.changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_pixelDensity=double((get(hObject,'Value')-get(hObject,'Min'))/(get(hObject,'Max')+get(hObject,'Min')));
tracker_rendering_window_controls.changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_bgc = double(uisetcolor(tracker_rendering_window_controls.volume_bgc));
handles.pushbutton1.ForegroundColor=tracker_rendering_window_controls.volume_bgc;
handles.pushbutton1.BackgroundColor=1-handles.pushbutton1.ForegroundColor;
tracker_rendering_window_controls.changed=double(1);


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_lighting(1)=double((get(hObject,'Value')-get(hObject,'Min'))/(get(hObject,'Max')+get(hObject,'Min')));
tracker_rendering_window_controls.changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_lighting(2)=double((get(hObject,'Value')-get(hObject,'Min'))/(get(hObject,'Max')+get(hObject,'Min')));
tracker_rendering_window_controls.changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_lighting(3)=double((get(hObject,'Value')-get(hObject,'Min'))/(get(hObject,'Max')+get(hObject,'Min')));
tracker_rendering_window_controls.changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider6_Callback(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_rendering_window_controls;
tracker_rendering_window_controls.volume_lighting(4)=double(get(hObject,'Value'));
tracker_rendering_window_controls.changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider25_Callback(hObject, eventdata, handles)
% hObject    handle to slider25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_rendering_window_controls;
tracker_rendering_window_controls.color_mode=double(get(hObject,'Value'));
tracker_rendering_window_controls.changed=double(1);

% --- Executes during object creation, after setting all properties.
function slider25_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider26_Callback(hObject, eventdata, handles)
% hObject    handle to slider26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_rendering_window_controls;
tracker_rendering_window_controls.parameter_gamma=double(get(hObject,'Value'));
tracker_rendering_window_controls.changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global tracker_rendering_window_controls;
tracker_rendering_window_controls.color = double(uisetcolor(tracker_rendering_window_controls.color));
handles.pushbutton2.BackgroundColor=tracker_rendering_window_controls.color;
%handles.pushbutton2.BackgroundColor=1-handles.pushbutton2.ForegroundColor;
tracker_rendering_window_controls.changed=double(1);


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton2.
function pushbutton2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


