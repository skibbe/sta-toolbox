function varargout = mhs_gui_tracker_matlab(varargin)
% MHS_GUI_TRACKER_MATLAB MATLAB code for mhs_gui_tracker_matlab.fig
%      MHS_GUI_TRACKER_MATLAB, by itself, creates a new MHS_GUI_TRACKER_MATLAB or raises the existing
%      singleton*.
%
%      H = MHS_GUI_TRACKER_MATLAB returns the handle to a new MHS_GUI_TRACKER_MATLAB or the handle to
%      the existing singleton*.
%
%      MHS_GUI_TRACKER_MATLAB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MHS_GUI_TRACKER_MATLAB.M with the given input arguments.
%
%      MHS_GUI_TRACKER_MATLAB('Property','Value',...) creates a new MHS_GUI_TRACKER_MATLAB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mhs_gui_tracker_matlab_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mhs_gui_tracker_matlab_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mhs_gui_tracker_matlab

% Last Modified by GUIDE v2.5 02-Aug-2015 15:47:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mhs_gui_tracker_matlab_OpeningFcn, ...
                   'gui_OutputFcn',  @mhs_gui_tracker_matlab_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mhs_gui_tracker_matlab is made visible.
function mhs_gui_tracker_matlab_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mhs_gui_tracker_matlab (see VARARGIN)

% Choose default command line output for mhs_gui_tracker_matlab
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mhs_gui_tracker_matlab wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%gexists=exist('tracker_window_controls','var');
gexists=numel(who('tracker_window_controls','global'))>0;


global tracker_window_controls;

if ~gexists
   
    tracker_window_controls.changed=double(1);
    tracker_window_controls.tracker_temp=1;
    tracker_window_controls.tracker_data=double([0,0,0,0,0]);
    tracker_window_controls.debug_changed=double(0);
    tracker_window_controls.edgecost=double([1,1,1,90]);
    tracker_window_controls.bifcost=double([1,1,1,1,1]);
end;

% handles.slider1.Value=tracker_window_controls.volume_pixelDensityThreshold;
% handles.slider2.Value=tracker_window_controls.volume_pixelDensity;
% handles.slider2.Value=tracker_window_controls.volume_pixelDensity;
% handles.pushbutton1.ForegroundColor=tracker_window_controls.volume_bgc;
% handles.pushbutton1.BackgroundColor=1-handles.pushbutton1.ForegroundColor;
% handles.slider3.Value=tracker_window_controls.volume_lighting(1);
% handles.slider4.Value=tracker_window_controls.volume_lighting(2);
% handles.slider5.Value=tracker_window_controls.volume_lighting(3);
% handles.slider6.Value=tracker_window_controls.volume_lighting(4);

tracker_window_controls.handles=handles;
tracker_window_controls.update_func=@update_gui;

tracker_window_controls.update_func();

function update_gui

fprintf('updating gui\n');
global tracker_window_controls;

tracker_window_controls.handles.slider7.Max=max(tracker_window_controls.handles.slider7.Max,2*tracker_window_controls.tracker_temp);

tracker_window_controls.handles.slider8.Max=max(tracker_window_controls.handles.slider8.Max,2*tracker_window_controls.tracker_data(1));
tracker_window_controls.handles.slider9.Max=max(tracker_window_controls.handles.slider9.Max,2*tracker_window_controls.tracker_data(2));
tracker_window_controls.handles.slider10.Max=max(tracker_window_controls.handles.slider10.Max,2*tracker_window_controls.tracker_data(3));
tracker_window_controls.handles.slider11.Max=max(tracker_window_controls.handles.slider11.Max,2*tracker_window_controls.tracker_data(4));
tracker_window_controls.handles.slider12.Max=max(tracker_window_controls.handles.slider12.Max,2*tracker_window_controls.tracker_data(5));


tracker_window_controls.handles.slider7.Value=tracker_window_controls.tracker_temp;
tracker_window_controls.handles.slider8.Value=tracker_window_controls.tracker_data(1);
tracker_window_controls.handles.slider9.Value=tracker_window_controls.tracker_data(2);
tracker_window_controls.handles.slider10.Value=tracker_window_controls.tracker_data(3);
tracker_window_controls.handles.slider11.Value=tracker_window_controls.tracker_data(4);
tracker_window_controls.handles.slider12.Value=tracker_window_controls.tracker_data(5);


tracker_window_controls.handles.slider13.Value=tracker_window_controls.edgecost(1);
tracker_window_controls.handles.slider14.Value=tracker_window_controls.edgecost(2);
tracker_window_controls.handles.slider15.Value=tracker_window_controls.edgecost(3);
tracker_window_controls.handles.slider16.Value=tracker_window_controls.edgecost(4);


tracker_window_controls.handles.slider19.Value=tracker_window_controls.bifcost(1);
tracker_window_controls.handles.slider20.Value=tracker_window_controls.bifcost(2);
tracker_window_controls.handles.slider21.Value=tracker_window_controls.bifcost(3);
tracker_window_controls.handles.slider22.Value=tracker_window_controls.bifcost(4);
tracker_window_controls.handles.slider24.Value=tracker_window_controls.bifcost(5);



tracker_window_controls.changed=double(0);
tracker_window_controls.debug_changed=double(0);

% --- Outputs from this function are returned to the command line.
function varargout = mhs_gui_tracker_matlab_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on slider movement.
function slider7_Callback(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.tracker_temp=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);
fprintf('setting temp to: %f\n',tracker_window_controls.tracker_temp);


% --- Executes during object creation, after setting all properties.
function slider7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider8_Callback(hObject, eventdata, handles)
% hObject    handle to slider8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.tracker_data(1)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider9_Callback(hObject, eventdata, handles)
% hObject    handle to slider9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.tracker_data(2)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider10_Callback(hObject, eventdata, handles)
% hObject    handle to slider10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.tracker_data(3)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider11_Callback(hObject, eventdata, handles)
% hObject    handle to slider11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global tracker_window_controls;
tracker_window_controls.tracker_data(4)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);

% --- Executes during object creation, after setting all properties.
function slider11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider12_Callback(hObject, eventdata, handles)
% hObject    handle to slider12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global tracker_window_controls;
tracker_window_controls.tracker_data(5)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);

% --- Executes during object creation, after setting all properties.
function slider12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider16_Callback(hObject, eventdata, handles)
% hObject    handle to slider16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.edgecost(4)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider15_Callback(hObject, eventdata, handles)
% hObject    handle to slider15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


global tracker_window_controls;
tracker_window_controls.edgecost(3)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider14_Callback(hObject, eventdata, handles)
% hObject    handle to slider14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.edgecost(2)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider13_Callback(hObject, eventdata, handles)
% hObject    handle to slider13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.edgecost(1)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider24_Callback(hObject, eventdata, handles)
% hObject    handle to slider24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.bifcost(5)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider22_Callback(hObject, eventdata, handles)
% hObject    handle to slider22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.bifcost(4)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider21_Callback(hObject, eventdata, handles)
% hObject    handle to slider21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global tracker_window_controls;
tracker_window_controls.bifcost(3)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider20_Callback(hObject, eventdata, handles)
% hObject    handle to slider20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


global tracker_window_controls;
tracker_window_controls.bifcost(2)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);



% --- Executes during object creation, after setting all properties.
function slider20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider19_Callback(hObject, eventdata, handles)
% hObject    handle to slider19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


global tracker_window_controls;
tracker_window_controls.bifcost(1)=double(get(hObject,'Value'));
tracker_window_controls.debug_changed=double(1);


% --- Executes during object creation, after setting all properties.
function slider19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
