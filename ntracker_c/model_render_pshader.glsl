#version 130

varying vec4 vColor;
varying vec3 vNormal;
varying vec3 vPosition;
varying float colorness;
varying float specular_w;
varying float dist_trafo;

uniform float glsl_clipping_plane; 
uniform bool glsl_clipping_hard=true;
uniform float glsl_ambient=0.1; 

uniform int glsl_render_mode=1; 



uniform float glsl_dist_clip=0; 
// uniform bool background_black=false;

void mylit(in float NdotL, in float NdotH, in float m, out vec2 value)
{
  float specular = (NdotL > 0.0) ? pow(max(0.0, NdotH), m) : 0.0;
  value= vec2(max(0.0, NdotL), specular*specular_w);
}


void myreflect( in vec3 i, vec3 n ,out  vec3 value )
{
  value= i - 2.0 * n * dot(n,i);
}



void myrand(in vec3 co, out vec3 value){
    value.x=fract(sin(dot(co.yz ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
    value.y=fract(sin(dot(co.xz ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
    value.z=fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
}


void rgb2gray( in vec3 i, out float value )
{
  value = min(max(0.2989*i.x +  0.5870*i.y + 0.1140*i.z,0),1);
}


void main()
{


      if ((glsl_clipping_hard)&&((vPosition.z+glsl_clipping_plane)<0.0))
      {
       discard;
      }
      
      
      if (glsl_dist_clip>(dist_trafo))
      {
      discard;
      }
      
      
      //float wdist = max(1.0-(dist_trafo - glsl_dist_clip),0.0);
      //float wdist = max(1.0-(dist_trafo - glsl_dist_clip),0.0);
      //wdist  = cos(1-wdist); 

   
      vec3 normal2=vNormal;
      vec3 normal;
      vec3 color;
//      myrand(vNormal,normal);
     //myrand(vNormal+vPosition,normal);
     
//if (glsl_render_mode!=3)
if (glsl_render_mode==0)
{
     myrand(vNormal+normalize(vPosition),normal);
     normal=normalize(vNormal+0.1*normal);
     //normal=vNormal;
     
     
     color.xyz=vColor.xyz+0.1*normal;
}else
{
    
     normal=vNormal;
     color.xyz=vColor.xyz;
}

//color.y = 0.0;
//color.z = 0.0;        

     //color.w=vColor.w;
     
   
//       vec3 normal=normalize(vNormal+0.15*noise3(0.0));
      
      
      const vec3 viewPos=vec3( 0.0,0.0,1.0 ); 
      //const float ambient=0.05;
      float ambient=glsl_ambient;
      
      
      const float  diffuse=1.0;
      const float specular=1.0;
      const float specularN=16.0;
     
      
      const vec3 lightdir=vec3 ( 0.0,0.0,-1.0);
 
 
     
 
      //gl_FragColor = color;
      
      
      
      const float weight=0.97;
      float local_colorness=colorness*weight;
      
      float intensity=max ( color.x, max ( color.y,color.z ) )
            +min ( color.x, min ( color.y,color.z ) );
      
      
      
      float light=0.0;
      
      

      light=dot ( normal,lightdir );
      


      vec3 reflection;
      myreflect ( lightdir,normal,reflection);
      
      float refl=dot ( reflection,viewPos );

      //vec4 l=lit ( light,refl,specularN );
      vec2 l;
      mylit ( light,refl,specularN,l);
      
      
      
      switch (glsl_render_mode)
      {
      
        case 0:
        {
                
        
                color.xyz=((local_colorness+1.0-weight)*color.xyz)+(1.0-(local_colorness+1.0-weight))*intensity;
                gl_FragColor.xyz=((1.0-ambient)*l.x+ambient)*color.xyz+0.25*l.y;
                if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
                {
                gl_FragColor.w=vColor.w;
                }else 
                {
                gl_FragColor.w=0.1;
                }
        }break;
        case 1:
        {
                /*
                gl_FragColor.xyz  = vec3(0.0);
                */
                if (color.x>color.z)
                {
                    gl_FragColor.xyz  = vec3(0.0);
                }else
                {
                    gl_FragColor.xyz  = vec3(0.5);
                }
                
                if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
                {
                gl_FragColor.w=vColor.w;
                }else 
                {
                gl_FragColor.w=0.1;
                }
                
                //vec3 color_back = color;
                //color.xyz=((local_colorness+1.0-weight)*color.xyz)+(1.0-(local_colorness+1.0-weight))*intensity;
                
                //color.xyz = ((1.0-ambient)*l.x+ambient)*color.xyz+0.25*l.y;
                /*
                intensity *= ((1.0-ambient)*l.x+ambient);
                gl_FragColor.x = (1.0 - color.x) * intensity;
                gl_FragColor.y = (1.0 - color.y) * intensity;
                gl_FragColor.z = (1.0 - color.z) * intensity;
                 if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
                {
                gl_FragColor.w=vColor.w;
                }else 
                {
                gl_FragColor.w=0.1;
                }
                */
        }break;
        case 2:
        {      
        
            //float l2=(((1.0-ambient)*l.x+ambient));
                
            //color.xyz = vec3(0.5*(1.0+l2),l2,0.0);
            

        
            color.xyz=((local_colorness+1.0-weight)*color.xyz)+(1.0-(local_colorness+1.0-weight))*intensity;
            float s=l.y;
            if (s>0.2)
            {
            gl_FragColor.xyz=min(color.xyz+0.1,1);
            }else
            {
            float l=(((1.0-ambient)*l.x+ambient));
            if (l > 0.8)
                gl_FragColor.xyz=color.xyz*0.9;
            else if (l > 0.6)
                gl_FragColor.xyz=color.xyz*0.75;
            else if (l > 0.4)
                gl_FragColor.xyz=color.xyz*0.3;
            else
                gl_FragColor.xyz=color.xyz*0.1;  
            }
        
            if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
            {
            gl_FragColor.w=vColor.w;
            }else 
            {
            gl_FragColor.w=0.1;
            }
        }break;
        case 3:
        {   
        
            float l=(((1.0-ambient)*l.x+ambient));
            
            float whiteness = min(min(color.x,color.y),color.z);
            gl_FragColor.xyz=((1.0-whiteness)*vec3(0.5*(1.0+l),l,0.0))+((whiteness));
          
        
            if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
            {
            gl_FragColor.w=vColor.w;
            }else 
            {
            gl_FragColor.w=0.1;
            }
        }break;
      
      }
      /*
      if (glsl_render_mode==1)
      {
	gl_FragColor.xyz=((1.0-ambient)*l.x+ambient)*color.xyz+0.25*l.y;
	
	if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
	{
	  gl_FragColor.w=vColor.w;
	}else 
	{
	  gl_FragColor.w=0.1;
	}
      }else if (glsl_render_mode==2)
      {
      
        gl_FragColor.xyz  = vec3(0.0);
        if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
        {
        gl_FragColor.w=vColor.w;
        }else 
        {
        gl_FragColor.w=0.1;
        }

      }else
      {
	
        float s=l.y;
        if (s>0.2)
        {
        gl_FragColor.xyz=min(color.xyz+0.1,1);
        }else
        {
        float l=(((1.0-ambient)*l.x+ambient));
        if (l > 0.8)
            gl_FragColor.xyz=color.xyz*0.9;
        else if (l > 0.6)
            gl_FragColor.xyz=color.xyz*0.75;
        else if (l > 0.4)
            gl_FragColor.xyz=color.xyz*0.3;
        else
            gl_FragColor.xyz=color.xyz*0.1;  
        }
	
        if ((glsl_clipping_hard)||((vPosition.z+glsl_clipping_plane)>0.0))
        {
        gl_FragColor.w=vColor.w;
        }else 
        {
        gl_FragColor.w=0.1;
        }
      }*/
      
    

    
}
