#include "mex.h"

#include <unistd.h>
#include <complex>
#include <map>

// #define STA_COMMON_ERROR_H

#include "sta_error.h"



#define _SUPPORT_MATLAB_
#include "stafield.h"
#include "../../sta-toolbox/sta_toolbox_matlab/sta_mex_helpfunc.h"
// #define _SUPPORT_MATLAB_ 



#include <complex>
#include <vector>
#include <string>
#include <ctime>
#include <list>
#include <sstream>
#include <string>
#include <limits>
#include <omp.h>
  
// #include "sta_mex_helpfunc.h"
// #include "sta_omp_threads.h"

/*
 mex getLocalMaxC.cpp 
*/

/*
template <typename T>
T compute_weight(int m1, int m2,int l)
{
    return ((m1%2==0) ? 1 : (-1))*hanalysis::clebschGordan ( 1,-m1,1,m2,l,m2-m1 ); 
}
*/

template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    
  try {
	hanalysis::stafield<T> ifield;
        
	ifield=hanalysis::stafield<T>((mxArray *)prhs[0]);   
	
	if (ifield.getType()!=hanalysis::STA_OFIELD_EVEN)
	{
	  throw hanalysis::STAError("stafield type must be STA_OFIELD_EVEN\n"); 
	}
	
	if (ifield.getStorage()!=hanalysis::STA_FIELD_STORAGE_R)
	{
	  throw hanalysis::STAError("stafield storage must be STA_FIELD_STORAGE_R\n"); 
	}
	
	if (ifield.getRank()<2)
	{
	  throw hanalysis::STAError("stafield rank must be 2\n"); 
	}
	  
	hanalysis::stafield<T> ifield_L0;  
	hanalysis::stafield<T> ifield_L2;  
	ifield_L0=ifield.get(0);     
	ifield_L2=ifield.get(2);     
	
	 
	const std::complex<T> * vL0=ifield_L0.getDataConst();
	const std::complex<T> * vL2=ifield_L2.getDataConst();
	
	  
	std::size_t numv=ifield.getNumVoxel();
	std::size_t stride=ifield.getStride();
	
	
	const std::size_t *  shape=ifield.getShape();
	
	mwSize outdim[4];
	outdim[0] = 6;
	outdim[1] = shape[2];
	outdim[2] = shape[1];
	outdim[3] = shape[0];
	
	
	plhs[0] = mxCreateNumericArray(4,outdim,hanalysis::mex_getClassId< T >(),mxREAL);
	T * result = (T *) mxGetData(plhs[0]);
	

	
	  
	const  T s6=std::sqrt(T(6));
	const  T s3=std::sqrt(T(3));
	const  T s2=std::sqrt(T(2));
    
	  
	for (std::size_t z=0;z<numv;z++)
	{
	  
	  const  T  * v0=(const  T  * )(vL0+stride*z);
	  const  T  * v2=(const  T  * )(vL2+stride*z);
	  
	  const T & a=v2[0];
	  const T & b=v2[1];
	  const T & c=v2[2];
	  const T & d=v2[3];
	  const T & e=v2[4];
	  const T & f=v0[0];
	  
	  
	  T *  H=result+6*z; 
	  T & xx=  H[0]; //xx
	  T & yy=  H[1]; //yy
	  T & zz=  H[2]; //zz
	  T & xy=  H[3]; //xy
	  T & yz=  H[4]; //yz
	  T & xz=  H[5]; //xz
	  
	  xx=(a-e/s6-f/s3);
	  yy=(-a-e/s6-f/s3);
	  zz=( (s2*e)-f)/s3;
	  
	  xy=b;
	  yz=d;
	  xz=c;
	}

	
	
  }catch (hanalysis::STAError & error)
  {
      mexErrMsgTxt(error.what());
  }
	
	
	
     
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{    
    if (nrhs<1)
        mexErrMsgTxt("error: expected stafield\n");

    if ((hanalysis::mex_isStaFieldStruct<float>(prhs[0])))
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else
        if ((hanalysis::mex_isStaFieldStruct<double>(prhs[0])))
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");
}

