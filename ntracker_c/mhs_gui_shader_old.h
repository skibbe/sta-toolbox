#ifndef CGCPROGRAM_H
#define CGCPROGRAM_H

#include <Cg/cg.h>
#include <Cg/cgGL.h>

#include <iostream>
#include <stdio.h>
    bool PrintlastCGerror()
    {
	    CGerror a =cgGetError();
	    if (a!=CG_NO_ERROR) {
	      {
	    const char * e = cgGetErrorString(a);
	    printf("Last OpenCG error:%s\n",e);}
	    return true;
	    }
	    return false;
    }

   
   

   class CGCprogram
   {
     
     
     
         typedef struct CONTAINER
         {
            CGcontext cgContext;
            CGprofile cgVertexProfile;
            CGprofile cgFragmentProfile;
         } Cg_container;
      private:
         Cg_container _container;
      public:
	enum class EPROGRAM_TYPE{VERTEX_SHADER=0,PIXEL_SHADER=1};
         void  PrintlastCGerror();
         CGCprogram();
         ~CGCprogram();
         bool Load_Program ( CGprogram & cg_program,std::string filename,EPROGRAM_TYPE type,bool fromDisk=true );
         void enableProfile ( EPROGRAM_TYPE type );
         void disableProfile ( EPROGRAM_TYPE type );
	 void disableProgram(EPROGRAM_TYPE type);
   };

	void CGCprogram::enableProfile(EPROGRAM_TYPE type)
	{
		if (type==EPROGRAM_TYPE::VERTEX_SHADER)
		cgGLEnableProfile(_container.cgVertexProfile);
		else
		cgGLEnableProfile(_container.cgFragmentProfile);
	};

	void CGCprogram::disableProfile(EPROGRAM_TYPE type)
	{
		if (type==EPROGRAM_TYPE::VERTEX_SHADER)
		cgGLDisableProfile(_container.cgVertexProfile);
		else
		cgGLDisableProfile(_container.cgFragmentProfile);
	};
	
	void CGCprogram::disableProgram(EPROGRAM_TYPE type)
	{
		if (type==EPROGRAM_TYPE::VERTEX_SHADER)
		cgGLUnbindProgram(_container.cgVertexProfile);
		else
		cgGLUnbindProgram(_container.cgFragmentProfile);
	};

	CGCprogram::CGCprogram()
	{
	  std::cerr<<"creaint cg context"<<std::endl;
	  
		_container.cgContext=NULL;
		if (_container.cgContext==NULL) _container.cgContext = cgCreateContext();						
		if (_container.cgContext == NULL)
		{
			printf("Failed To Create Cg Context\n");
			throw std::string("Failed To Create Cg Context\n");							
		}
	
		_container.cgVertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX);				
		if (_container.cgVertexProfile == CG_PROFILE_UNKNOWN)
		{
			printf("Invalid profile type\n");
			throw std::string("Invalid profile type\n");								
		}
	
		_container.cgFragmentProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
		if (_container.cgFragmentProfile  == CG_PROFILE_UNKNOWN)
		{
			printf("Invalid profile type\n");
			throw std::string("Invalid profile type\n");						
		}
		printf("vertex profile:   %s\n",
       cgGetProfileString(_container.cgVertexProfile ));
		printf("fragment profile:   %s\n",
       cgGetProfileString(_container.cgFragmentProfile ));
		
// 		cgGLSetOptimalOptions(_container.cgFragmentProfile);
// 		cgGLSetOptimalOptions(_container.cgVertexProfile);
		
		cgGLSetContextOptimalOptions(_container.cgContext, _container.cgFragmentProfile);
		cgGLSetContextOptimalOptions(_container.cgContext, _container.cgVertexProfile);
	}

	CGCprogram::~CGCprogram()
	{
		cgDestroyContext(_container.cgContext);	
	}


	bool CGCprogram::Load_Program(CGprogram & cg_program,std::string filename,EPROGRAM_TYPE type,bool fromDisk)
	{
		if (type==EPROGRAM_TYPE::VERTEX_SHADER)
		{
				if (fromDisk)
				{
				    std::cerr<<"loading : "<<filename.c_str()<<std::endl;
				cg_program = cgCreateProgramFromFile(_container.cgContext, 
								CG_SOURCE, 
								filename.c_str() ,
								_container.cgVertexProfile,
								"main", 0);
				}else
				{
				cg_program = cgCreateProgram(_container.cgContext, 
								CG_SOURCE, 
								filename.c_str() ,
								_container.cgVertexProfile,
								"main", 0);
				}
		}
		else 
		{
				if (fromDisk)
				{
				  std::cerr<<"loading : "<<filename.c_str()<<std::endl;
				cg_program = cgCreateProgramFromFile(_container.cgContext,
								CG_SOURCE, 
								filename.c_str(),
								_container.cgFragmentProfile,
								 "main", 0);
				}else
				{
				cg_program = cgCreateProgram(_container.cgContext,
								CG_SOURCE, 
								filename.c_str(),
								_container.cgFragmentProfile,
								 "main", 0);

				}
		//std::cout<<"loading shader program"<<std::endl;
		}
		if (cg_program == NULL)
		{
			CGerror Error = cgGetError();
			printf("Couldn't load the program '%s': %s\n",filename.c_str(),cgGetErrorString(Error));
			return false;								
		}
		cgGLLoadProgram(cg_program);
	return(true);
	}





	
#endif	