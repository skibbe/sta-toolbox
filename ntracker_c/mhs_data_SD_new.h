#ifndef MHS_DATA_SD_NEW_H
#define MHS_DATA_SD_NEW_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataSD2: public CData<T,TData,Dim>
{
private:
    int scale_power;

    const TData * scales;
    std::size_t num_scales;

    const TData * local_stdv;
    int num_alphas_local_stdv;
    
    const TData * local_mean;
    int num_alphas_local_mean;

    const TData * sd_data;
    int num_samples_sd_data;
    
    

    Vector<T,Dim> boundary;
    bool scale_dependend_boundary;


    
    T min_scale;
    T max_scale;
    T scale_correction;
    

    T DataThreshold;
    T DataScale;
    T StdvEpsilon;
    
    T wxx;
    T wyy;
    T wzz;
    T wxy;
    T wxz;
    T wyz;

    
    Vector<T,3> element_size;
    Vector<std::size_t ,3> feature_shape;
    

public:

    float get_minscale() {
        return min_scale/scale_correction;
    };

    float get_maxscale() {
        return max_scale/scale_correction;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataSD2() {
    }

    ~CDataSD2() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
//                 DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
//                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
//            printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
//        printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
    }
#endif

    void init ( const mxArray * feature_struct ) {

        mhs::dataArray<TData>  tmp0;
        mhs::dataArray<TData>  tmp1;
        mhs::dataArray<TData>  tmp2;
        mhs::dataArray<TData>  tmp3;
	mhs::dataArray<TData>  tmp4;
	
	
        try {
            tmp3=mhs::dataArray<TData> ( feature_struct,"shape_boundary" );
            sta_assert_error ( tmp3.get_num_elements() ==3 );
            boundary[0]=tmp3.data[0];
            boundary[1]=tmp3.data[1];
            boundary[2]=tmp3.data[2];
            scale_dependend_boundary=true;
        } catch ( mhs::STAError error ) {
            boundary[0]=boundary[1]=boundary[2]=T ( 0 );
            scale_dependend_boundary=false;
        }
        
       

        try {


            tmp1=mhs::dataArray<TData> ( feature_struct,"sd_data" );
            sd_data=tmp1.data;
            num_samples_sd_data=tmp1.dim[tmp1.dim.size()-1];
            printf ( "sd data samples: %d \n",num_samples_sd_data );

            tmp2=mhs::dataArray<TData> ( feature_struct,"alphas_sdv" );
            local_stdv=tmp2.data;
            num_alphas_local_stdv=tmp2.dim[tmp2.dim.size()-1];
            printf ( "local stdv pol degree: %d \n",num_alphas_local_stdv-1 );

           sta_assert_error ( tmp2.get_num_elements() /num_alphas_local_stdv==tmp1.get_num_elements() /(num_samples_sd_data*6) );
	   
	    tmp4=mhs::dataArray<TData> ( feature_struct,"alphas_mean" );
            local_mean=tmp4.data;
            num_alphas_local_mean=tmp4.dim[tmp4.dim.size()-1];
            printf ( "local mean pol degree: %d \n",num_alphas_local_mean-1 );

           sta_assert_error ( tmp4.get_num_elements() /num_alphas_local_mean==tmp1.get_num_elements() /(num_samples_sd_data*6) );

            for ( int i=0; i<3; i++ ) {
                feature_shape[i]=tmp1.dim[i];
            }
        } catch ( mhs::STAError error ) {
            throw error;
        }


        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

        std::swap ( this->shape[0],this->shape[2] );

        for ( int i=0; i<3; i++ ) {
            element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
        }

        printf ( "element size data term:" );
        element_size.print();

        scale_correction=1;
        try {
            scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
        } catch ( mhs::STAError error ) {
            throw error;
        }




	sta_assert_error ( num_scales==num_samples_sd_data );

        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"override_scales" ).data;
            min_scale=scales[0]*scale_correction;
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"override_scales" ).dim[0]-1]*scale_correction;
        } catch ( mhs::STAError error ) {
            min_scale=scales[0];
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
        }

    }

    void set_params ( const mxArray * params=NULL ) {
        scale_power=2;
        DataScale=1;
        DataThreshold=1;
        StdvEpsilon=0.001;

        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"Epsilon" ) !=-1 ) {
                    StdvEpsilon=mhs::mex_getParam<T> ( params,"Epsilon",1 ) [0];
                }
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:



public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
      //,const Points<T,Dim> *org_point=NULL
	
    ) {


        try {

	  
	 
	  
            Vector<T,Dim> position=position_org;
            position/=this->element_size;


            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->feature_shape[0] ) || ( Y+1>=this->feature_shape[1] ) || ( X+1>=this->feature_shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }

            T org_scale=radius;

            radius*=scale_correction;

            if ( ( radius<min_scale-0.01 )
                    || ( radius>max_scale+0.01 ) ) {
                printf ( "[%f %f %f]\n",min_scale,radius,max_scale );
                sta_assert_error ( ! ( radius<min_scale ) );
                sta_assert_error ( ! ( radius>max_scale ) );
            }


            radius=std::min ( max_scale,std::max ( radius,min_scale ) );

            const T boundary_scale=2;
            if ( ( scale_dependend_boundary ) &&
                    ( ( position[0]+boundary_scale*org_scale>=this->feature_shape[0] ) ||
                      ( position[1]+boundary_scale*org_scale>=this->feature_shape[1] ) ||
                      ( position[2]+boundary_scale*org_scale>=this->feature_shape[2] ) ||
                      ( position[0]-boundary_scale*org_scale<0 ) ||
                      ( position[1]-boundary_scale*org_scale<0 ) ||
                      ( position[2]-boundary_scale*org_scale<0 ) ) ) {
                return false;
            }

            T stdv=0;
            if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
                return false;
            };
            stdv= ( std::max ( stdv,T ( 0 ) ) );
	    
	    T mean=0;
            if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_mean, num_alphas_local_mean,mean,feature_shape.v ) ) {
                return false;
            };
            


	    T sd_filter=0;
	    T H[6];
	    for ( int i=0; i<6; i++ ) {
		if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,H[i],feature_shape.v,6,i ) ) {
		    return false;
		}
	    }

	    T & xx=  H[0]; //xx
	    T & xy=  H[3]; //xy
	    T & xz=  H[5]; //xz
	    T & yy=  H[1]; //yy
	    T & yz=  H[4]; //yz
	    T & zz=  H[2]; //zz

	    const T & nx=direction[2];
	    const T & ny=direction[1];
	    const T & nz=direction[0];

	    sd_filter= ( nx*nx*xx+ny*ny*yy+nz*nz*zz+
			      +2*nx* ( ny*xy+nz*xz )
			      +2*ny*nz*yz );

	    
	    
// 	    {
// 	    Vector<T,3>   position(15,14,15);
// 	    Vector<T,3>   direction(0,0,1);
// 	    T radius=1.5;
// 	    T sd_filter=0;
// 	    T H[6];
// 	    for ( int i=0; i<6; i++ ) {
// 		if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,H[i],feature_shape.v,6,i,0 ) ) {
// 		    return false;
// 		}
// 	    }
// 
// 	    T & xx=  H[0]; //xx
// 	    T & xy=  H[3]; //xy
// 	    T & xz=  H[5]; //xz
// 	    T & yy=  H[1]; //yy
// 	    T & yz=  H[4]; //yz
// 	    T & zz=  H[2]; //zz
// 
// 	    const T & nx=direction[0];
// 	    const T & ny=direction[1];
// 	    const T & nz=direction[2];
// 	    
// 	    
// 	    printf("%f %f %f %f %f %f\n",H[0],H[1],H[2],H[3],H[4],H[5]);
// 	    
// // 	    std::size_t indx= SUB2IND(15  ,14  ,15  ,feature_shape.v) *6;
// // 	    printf("%f %f %f %f %f %f\n",sd_data[indx],sd_data[indx+1],sd_data[indx+2],sd_data[indx+3],sd_data[indx+4],sd_data[indx+5]);
// 	    
// 
// // 	    sd_filter= ( nx*nx*xx+ny*ny*yy+nz*nz*zz+
// // 			      +2*nx* ( ny*xy+nz*xz )
//   
// 	      
// 	    }

	    
            
//            if ( !data_interpolate_dir ( position,direction,radius,scales,sd_data, num_samples_sd_data,sd_filter ,feature_shape.v,3,0) ) 
// 	   {
//                                         return false;
// 	   }



            T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=org_scale;
                break;
            case 2:
                scale1=org_scale*org_scale;
                break;
            case 3:
                scale1=org_scale*org_scale*org_scale;
                break;
            }
            
// {           
//            T tmp=0;
// 	   T tmp2=0;
// 	   Vector<T,3> position(47,47,47);
// 	   Vector<T,3> direction(1,0,0);
// 	   T radius=2;
//            if ( !data_interpolate_dir ( position,direction,radius,scales,sd_data, num_samples_sd_data,tmp ,feature_shape.v,3,0,1,true) ) 
// 	   {
//                                         return false;
// 	   }
// 	   if ( !data_interpolate_dir ( position,direction,radius,scales,sd_data, num_samples_sd_data,tmp2 ,feature_shape.v,3,0,0,true) ) 
// 	   {
//                                         return false;
// 	   }
//  	   printf("A %f \n",tmp);
//  	   printf("B %f \n",tmp2);
// 	   
// 	    T stdv=0;
//             if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
//                 return false;
//             };
//             stdv= ( std::max ( stdv,T ( 0 ) ) );
// 	    printf("%f\n",stdv);
// 	   
// }           
            


#ifdef D_USE_GUI
            if ( debug!=NULL ) {
                std::stringstream s;
                s.precision ( 3 );
                s<<"------------------";
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<" SD : "<<sd_filter<<" N:" <<(sd_filter-mean)*stdv;
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<"sdv :"<<stdv<< "mean: "<<mean;
                debug->push_back ( s.str() );
		
            }
#endif

	    

	      result=-DataScale*stdv*(sd_filter-mean)*scale1;



            

        } catch ( mhs::STAError error ) {
            throw error;
        }

        return true;
    }
};

#endif
