#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"

#include <stdio.h>
#include <stdlib.h>

#include "mhs_vector.h"



#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>



#include "gpu/mxGPUArray.h"
#include "GL/glu.h"
#include "GL/glext.h"
#include "./ext/helper_math.h"



// template<typename TData>
__global__ void kernel_sfilter( 
    const int3    bb,
    int num_total_voxels,
    cudaTextureObject_t tex,
    float * debug_kernel)
{
  
    
    unsigned int indx=blockIdx.x*blockDim.x+threadIdx.x;

    if (indx<num_total_voxels)
    {
      int z=(indx/(bb.x*bb.y));
      indx%=(bb.y*bb.x);
      int y=(indx/(bb.x));
      int x=(indx%(bb.x));

      
      
      float intensity = tex3D<float>(tex, x+0.5f, y+0.5f, z+0.5f);
      
      int indx2=((z)*bb.y+(y))*bb.x+(x);
      debug_kernel[indx2]=intensity;	
      }
}


typedef  float TData;

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
  
// 	for (int i=0; i<3; i++)
//             shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
//         std::swap(shape[0],shape[2]);

  cudaArray * imgGPU;
    cudaTextureObject_t t_imgGPU;
    int gpu_device;
    cudaDeviceProp gpu_prop;
  
  
	if (nrhs<1)
	  return;
    
	TData * img;
	std::size_t shape[3];

        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(prhs[0]);
            img=tmp.data;
            sta_assert(tmp.dim.size()==3);
	    shape[0]=tmp.dim[0];
	    shape[1]=tmp.dim[1];
	    shape[2]=tmp.dim[2];
        }
        
        printf("SHAPE: %d %d %d\n",shape[0],shape[1],shape[2]);
        
        
	
	cudaError_t cuda_code;
	
	 int nDevices;
	 
	cudaGetDeviceCount(&nDevices);
	sta_assert_error(nDevices>0);
	for (int i = 0; i < nDevices; i++) {
	  cudaDeviceProp prop;
	  cudaGetDeviceProperties(&prop, i);
	  printf("Device %d (%s)\n",i, prop.name);
	  printf("maxGridSize                 %d %d %d\n",prop.maxGridSize[0],prop.maxGridSize[1],prop.maxGridSize[2]);
	  printf("maxThreadDim                %d %d %d\n",prop.maxThreadsDim[0],prop.maxThreadsDim[1],prop.maxThreadsDim[2]);
	  printf("maxThreadsPerBlock          %d \n",prop.maxThreadsPerBlock);
	  printf("maxThreadsPerMultiProcessor %d \n",prop.maxThreadsPerMultiProcessor);
	  printf("sharedMemPerBlock           %d \n",prop.sharedMemPerBlock);
	  printf("multiProcessorCount         %d \n",prop.multiProcessorCount);
	  printf("totalGlobalMem              %d GB \n",prop.totalGlobalMem/1024/1024/1000);
	  printf("l2CacheSize                 %d KB \n",prop.l2CacheSize/1024);
	}

  
	cudaChannelFormatDesc imgData= cudaCreateChannelDesc<TData>();;

	printf("allocating cuda memory ..");
// 	cudaError_t cuda_code=cudaMalloc3DArray(&imgGPU,&imgData,make_cudaExtent(shape[0],shape[1],shape[2]));
// 	cuda_code=cudaMalloc3DArray(&imgGPU,&imgData,make_cudaExtent(shape[0],shape[1],shape[2]));
	cuda_code=cudaMalloc3DArray(&imgGPU,&imgData,make_cudaExtent(shape[2],shape[1],shape[0]));
	sta_assert_error(cuda_code==cudaSuccess);
	printf("done\n"); 
	
	
	printf("img CPU -> GPU");
	std::size_t numv=shape[0]*shape[1]*shape[2];
	cuda_code=cudaHostRegister((void *)img,numv*sizeof(TData),cudaHostRegisterPortable);
	
	printf("cuda code: %s\n",cudaGetErrorString(cuda_code));
	sta_assert_error(cuda_code==cudaSuccess);
	
	
	cudaMemcpy3DParms copyParams = {0};
	copyParams.srcPtr   = make_cudaPitchedPtr((void*)img, shape[2]*sizeof(TData), shape[2],shape[1]);  
	copyParams.dstArray = imgGPU;
//   	copyParams.extent   = make_cudaExtent(shape[0],shape[1],shape[2]);
 	copyParams.extent   = make_cudaExtent(shape[2],shape[1],shape[0]);
	copyParams.kind	 = cudaMemcpyHostToDevice;

  //safecall(cudaMemcpyToArray(Z_d, 0, 0, Z, C*M*N*sizeof(float), cudaMemcpyHostToDevice), "cudaMemcpyToArray" );
	cuda_code=cudaMemcpy3DAsync(&copyParams);
	
	
// 	sta_assert_error(cuda_code==cudaSuccess);
	
	if (cuda_code!=cudaSuccess)
	{
	  cudaHostUnregister((void *)img);
	  cudaFreeArray(imgGPU);
	}
	
 	printf("cuda code: %s\n",cudaGetErrorString(cuda_code));
	sta_assert_error(cuda_code==cudaSuccess);
	printf("done\n"); 
	printf("creating tex object ..");
	
	
	cudaResourceDesc    texRes;
        memset(&texRes, 0, sizeof(cudaResourceDesc));
        texRes.resType = cudaResourceTypeArray;
        texRes.res.array.array  = imgGPU;
	
		
	
        cudaTextureDesc     texDescr;
        memset(&texDescr, 0, sizeof(cudaTextureDesc));
        texDescr.normalizedCoords = false;
        texDescr.filterMode = cudaFilterModeLinear;
        texDescr.addressMode[0] = cudaAddressModeClamp;   // clamp
        texDescr.addressMode[1] = cudaAddressModeClamp;
        texDescr.addressMode[2] = cudaAddressModeClamp;
        texDescr.readMode = cudaReadModeElementType;
        cuda_code=(cudaCreateTextureObject(&t_imgGPU, &texRes, &texDescr, NULL));

	sta_assert_error(cuda_code==cudaSuccess);
	
	printf("done\n"); 
   
	
	
	
	
	
	
	
		int3    GPU_bb;
		GPU_bb.z=shape[0];
		GPU_bb.y=shape[1];
		GPU_bb.x=shape[2];
		
		int GPU_num_total_voxels=shape[0]*shape[1]*shape[2];

		
		int num_threads=gpu_prop.maxThreadsPerBlock;
		int num_total_blocks=GPU_num_total_voxels/num_threads + (GPU_num_total_voxels % num_threads != 0);
		

		
		
		float * GPU_debug_kernel;
		cudaMalloc ( (void **)&GPU_debug_kernel, GPU_num_total_voxels*sizeof(float)) ;
//  		float * CPU_debug_kernel;
//  		CPU_debug_kernel= new float[GPU_num_total_voxels];
// 		
		
// 		printf("GPU %d %d\n",num_total_blocks,num_threads);
		
		 kernel_sfilter<<< num_total_blocks, num_threads >>>( 
		     GPU_bb,
		      GPU_num_total_voxels,
		      t_imgGPU,
    		   GPU_debug_kernel);
		 
		 
		std::size_t ndims[3];
		ndims[2]=shape[0];
		ndims[1]=shape[1];
		ndims[0]=shape[2];
		plhs[0] = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<TData>(),mxREAL );
		TData * debug_data4= ( TData * ) mxGetData (plhs[0] );
		 
		 cudaMemcpy (debug_data4, GPU_debug_kernel, GPU_num_total_voxels*sizeof(float) , cudaMemcpyDeviceToHost ) ;
		
		  
		cudaFree ( GPU_debug_kernel) ;
// 		delete [] CPU_debug_kernel;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
      if (imgGPU)
      {
	cudaError_t cuda_code;
	cuda_code=cudaHostUnregister((void *)img);
	sta_assert_error(cuda_code==cudaSuccess);
	
	cudaDestroyTextureObject(t_imgGPU);
	cuda_code=cudaFreeArray(imgGPU);
	sta_assert_error(cuda_code==cudaSuccess);
      }
  

}