#ifndef GLSL_VERTEX_SHADER_H
#define GLSL_VERTEX_SHADER_H

#include "mhs_error.h"
// #include <GL/glut.h>
#include <GL/glu.h>

  class Shader {
  protected:
  char* textFileRead(const char *fileName) {
	  char* text=NULL;

	  if (fileName != NULL) {
	  FILE *file = fopen(fileName, "rt");
		  if (file != NULL) {
	      fseek(file, 0, SEEK_END);
	      int count = ftell(file);
	      rewind(file);
			  if (count > 0) {
				  text = (char*)malloc(sizeof(char) * (count + 1));
				  count = fread(text, sizeof(char), count, file);
				  text[count] = '\0';
			  }
			  fclose(file);
		  }
	  }
	  return text;
  }  
    
  public:
      Shader(){shader_id=0;};
//       Shader(const char *vsFile, const char *fsFile)
      Shader(std::string vsFile, std::string  fsFile)
      {
	  shader_id=0;
	  try {
	  init(vsFile.c_str(), fsFile.c_str());
	    
	  } catch ( mhs::STAError error ) {

            mhs::STAError error2;
            error2<<error.str() <<"\n"<<"could not create the object";
            throw error2;
        }
      }
      ~Shader()
      {
	if (shader_id!=0)
	{
	    glDetachShader(shader_id, shader_fp);
	    glDetachShader(shader_id, shader_vp);
	    
	    glDeleteShader(shader_fp);
	    glDeleteShader(shader_vp);
	    glDeleteProgram(shader_id);
	}
      }
      
      bool succesfully_compiled(unsigned int ShaderObject)
      {
	GLint compiled;

	glGetShaderiv(ShaderObject, GL_COMPILE_STATUS, &compiled);
	
	if (!compiled)
	{

	 print_log(ShaderObject);
	  return false;
	}
	return true;
      }
      
      void print_log(unsigned int ShaderObject)
      {
	  GLint blen = 0;	
	  GLsizei slen = 0;

	  glGetShaderiv(ShaderObject, GL_INFO_LOG_LENGTH , &blen);       
	  if (blen > 1)
	  {
	  GLchar* compiler_log = (GLchar*)malloc(blen);
	  glGetShaderInfoLog(ShaderObject, blen, &slen, compiler_log);
	  printf("%s\n",compiler_log);
	  free (compiler_log);
	  }
      }
      
      
      GLint get_variable(std::string name)
      {
	GLint location=glGetUniformLocation(shader_id,name.c_str());
	if (location==-1)
	  printf("could'nt get location of  %s\n",name.c_str());
	sta_assert_error(location!=-1);
	return location;
      }
      
      void init(std::string vsFile, std::string fsFile)
      {
	  shader_vp = glCreateShader(GL_VERTEX_SHADER);
	  shader_fp = glCreateShader(GL_FRAGMENT_SHADER);
	  const char* vsText = textFileRead(vsFile.c_str());
	  const char* fsText = textFileRead(fsFile.c_str());  
// 	  printf("%s\n",vsText);
// 	  printf("%s\n",fsText);
	  if (vsText == NULL || fsText == NULL) {
	    throw mhs::STAError("Either vertex shader or fragment shader file not found.");	
	  }
	  
	  glShaderSource(shader_vp, 1, &vsText, 0);
	  glShaderSource(shader_fp, 1, &fsText, 0);

	  printf("compiling  %s vshader .. ",vsFile.c_str());	  
	  glCompileShader(shader_vp);
	  sta_assert_error(succesfully_compiled(shader_vp));
	  printf("done\n");
	  print_log(shader_vp);
	  
	  printf("compiling  %s fshader .. ",fsFile.c_str());	  
	  glCompileShader(shader_fp);
	  sta_assert_error(succesfully_compiled(shader_fp));
 	  print_log(shader_fp);
	  printf("done\n");
	  
	  shader_id = glCreateProgram();
	  glAttachShader(shader_id, shader_fp);
	  glAttachShader(shader_id, shader_vp);
	  glLinkProgram(shader_id);
	  
	  GLint linked;
	  glGetProgramiv(shader_id, GL_LINK_STATUS, &linked);
	  if (!linked)
	  {
	    throw mhs::STAError("could'nt link shader program.");	
	  }       
	  
// 	  char name[255]; 
// 	  GLint count;
// 	  // get count
// 	  glGetProgramiv (shader_id, GL_ACTIVE_ATTRIBUTES, &count);
// 	  // for i in 0 to count:
// 	  for (int i=0;i<count;i++)
// 	  {
// 	    GLsizei length;
// 	    GLint size;
// 	    GLenum type; 
// 	    glGetActiveAttrib (shader_id, i, 255, &length, &size, &type, name);
// 	    printf("%s\n",name);
// 	   }
// 	   
// 	  glGetProgramiv (shader_id, GL_ACTIVE_UNIFORMS, &count);
// 	  // for i in 0 to count:
// 	  for (int i=0;i<count;i++)
// 	  {
// 	    GLsizei length;
// 	    GLint size;
// 	    GLenum type; 
// 	    glGetActiveUniform (shader_id, i, 255, &length, &size, &type, name);
// 	    printf("%s\n",name);
// 	   }
	  
      
      }
      
      void bind()
      {
// 	    printf("using shader %d\n",shader_id);
 	    glUseProgram(shader_id);
// 	    glUseProgram(0);
      }
      
      void unbind()
      {
	    glUseProgram(0);
      }
      
      unsigned int id()
      {
	return shader_id;
      }    
      
  private:
      unsigned int shader_id;
      unsigned int shader_vp;
      unsigned int shader_fp;
  };

#endif