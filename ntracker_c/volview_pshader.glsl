#version 130
// uniform mat4 glsl_worldview;
uniform mat4 glsl_normalmat;
// uniform mat4 glsl_textview;


uniform sampler3D glsl_image_texture;
uniform sampler3D glsl_light_texture;

uniform float glsl_clipping_plane;
uniform vec3  glsl_focuspoint;
uniform float glsl_mode;
uniform vec3  glsl_rescale;
uniform vec3  glsl_texture_size;
uniform float glsl_parameter_pixelDensity=0.5;
uniform float glsl_parameter_pixelDensityThreshold=0.1;
uniform float glsl_ambient=0.01;
uniform float glsl_diffuse=0.5;
uniform float glsl_specular=0.5;
uniform float glsl_specularN=4.0;
uniform int  glsl_color_mode=0;
uniform bool  glsl_is_color_tex=false;
uniform float glsl_gamma=1.0;
uniform vec3 glsl_colorize;


varying vec3 vTextCoord;
varying vec3 vPosition;
varying float clip_depth;
varying vec4 vColor;


// void mylit(in float NdotL, in float NdotH, in float m, out vec4 value)
// {
//   float specular = (NdotL > 0.0) ? pow(max(0.0, NdotH), m) : 0.0;
//   value= vec4(1.0, max(0.0, NdotL), specular, 1.0);
// }


void getIntensity ( in vec3 color, out float intensity )
{
//return max ( color.x, max ( color.y,color.z ) );
   intensity=( max ( color.x, max ( color.y,color.z ) ));
//             +min ( color.x, min ( color.y,color.z ) ) );
}


void mylit(in float NdotL, in float NdotH, in float m, out vec2 value)
{
  float specular = (NdotL > 0.0) ? pow(max(0.0, NdotH), m) : 0.0;
  value= vec2(max(0.0, NdotL), specular);
}

// void mylit(in float NdotL, in float NdotH, in float m, out vec3 value)
// {
//   float specular = (NdotL > 0.0) ? pow(max(0.0, NdotH), m) : 0.0;
//   value= vec3(1.0, max(0.0, NdotL), specular);
// }

// void mylit(in float NdotL, in float NdotH, in float m, out vec4 value)
// {
//   
//   value= lit(NdotL, NdotH, m);
// }

void myreflect( in vec3 i, vec3 n ,out  vec3 value )
{
  value= i - 2.0 * n * dot(n,i);
}

void rand(in vec3 co, out vec3 value){
    value.x=fract(sin(dot(co.yz ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
    value.y=fract(sin(dot(co.xz ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
    value.z=fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
}

// void myreflect( in vec3 i, vec3 n ,out  vec3 value )
// {
//   value=reflect( i, n );
// }


void main (void) {


  const vec3 viewPos=vec3( 0.0,0.0,1.0 );
  
  const vec3 lightdir=vec3( 0.0,0.0,-1.0);


  
//   if ((-vPosition.z+glsl_clipping_plane)<0.0)
  if ((clip_depth+glsl_clipping_plane)<0.0)
  {
    discard;
  }

//    vec3 tex_coord=vTextCoord;
//   
    vec3 tex_coord2;
    
    rand(vTextCoord+vPosition,tex_coord2);
    

    if (glsl_color_mode==0)
    {
    tex_coord2=vTextCoord+vPosition;
    }
    
    vec3 tex_coord=(vTextCoord+0.5*tex_coord2/glsl_texture_size);

      
      //vec3 tex_coord=vTextCoord*glsl_texture_size;
      
  float colorness=1.0;
  float mode_alpha=1.0;
  
  if (abs(glsl_mode-1.0)<0.0001)
  {

      vec3 diff=(tex_coord.xyz-glsl_focuspoint);
      
      vec3 diff2=diff*glsl_rescale.xyz/400.0;
      
      float dist_sqr=sqrt(dot(diff2,diff2));

      colorness=max(0.01,1.0-dist_sqr*10.0);
     
      float v=(dist_sqr*8.0+1.0)/2.0;
      mode_alpha=min(v,1.0);
      
     tex_coord.xyz=diff*min(v,1.0)+glsl_focuspoint;
  }
  
  
    if ((tex_coord.x<0.0)||(tex_coord.y<0.0)||(tex_coord.z<0.0)||(tex_coord.x>1.0)||(tex_coord.y>1.0)||(tex_coord.z>1.0))
    {
      discard;
    }
    


      vec3 color;
      if (glsl_is_color_tex)
      {
	color= texture3D (  glsl_image_texture , tex_coord ).xyz;
      }else
      {
	color.xyz= glsl_colorize*texture3D (  glsl_image_texture , tex_coord ).x;
      }
      
      
      
      if (glsl_gamma<1.0)
      {
	color.x=pow(color.x,glsl_gamma);
	color.y=pow(color.y,glsl_gamma);
	color.z=pow(color.z,glsl_gamma);
      }
      
    
      float intensity;
      getIntensity(color,intensity);
      
//       gl_FragColor.xyz=1-color;
	
	
//       if (intensity<gl_FragColor.w)
//       {
// 	discard;
//       }
//       gl_FragColor.w=intensity;
// 	    
// 	      
//       if (intensity<-1)	
      {
	    gl_FragColor.w=intensity*glsl_parameter_pixelDensity;
	    
	    if ((gl_FragColor.w-glsl_parameter_pixelDensityThreshold*glsl_parameter_pixelDensity)<0.0)
	    {
	    discard;
	    }
      }
      
      
      float light=0.0;
      
      vec3 normal= texture3D ( glsl_light_texture, tex_coord ).xyz;
      
//       vec3 normal;
//       normal.x= texture3D (  glsl_image_texture , tex_coord+vec3(2.0/glsl_texture_size.x,0.0,0.0)).x-color.x;
//       normal.y= texture3D (  glsl_image_texture , tex_coord+vec3(0.0,2.0/glsl_texture_size.y,0.0)).y-color.y;
//       normal.z= texture3D (  glsl_image_texture , tex_coord+vec3(0.0,0.0,2.0/glsl_texture_size.z)).z-color.z;
//       normal=normalize(normal);
      
      normal-=0.5;
      normal*=2.0;
      
      normal=( glsl_normalmat*vec4(normal,1.0) ).xyz;
      
      
//       if (glsl_gamma<0) 
//       {
// 	normal= texture3D ( glsl_light_texture, tex_coord ).xyz;
// 	normal=( glsl_normalmat*vec4(normal,1.0) ).xyz;
//       }
      

      light=dot ( normal,lightdir );

      vec3 reflection;
      myreflect ( lightdir,normal,reflection );
      
      float refl=dot ( reflection,viewPos );

      vec2 l;
      mylit ( light,refl,glsl_specularN,l);
      
      light=glsl_diffuse*l.x+glsl_specular*l.y+glsl_ambient;
     
     if (colorness<1.0)
     {
     color=colorness*color+(1.0-colorness)*vec3(intensity,intensity,intensity);
     }
      
//       switch  (glsl_color_mode)
//       {
// 	case 0:
// 	{
// 	gl_FragColor.x=(1.0-(color.y+color.z)/2.0);
// 	gl_FragColor.y=(1.0-(color.x+color.z)/2.0);
// 	gl_FragColor.z=(1.0-(color.x+color.y)/2.0);
//  	gl_FragColor.xyz*=light+(1.0-mode_alpha);
//  	gl_FragColor.yz*=mode_alpha;
// 
//        }break;
//        default:
//        {
// 	gl_FragColor.xyz=color;
// 	gl_FragColor.xyz+=light;
// 	gl_FragColor.yz*=mode_alpha;
// 	}break;
//       }
      if (glsl_color_mode==0)
      {
     
        /*
      	gl_FragColor.xyz=color.xyz;//(((1.0-glsl_ambient)*l.x+glsl_ambient))*color.xyz+max(0.1-0.1*l.y,0);
        float w=1;//0.5;
        vec3 tmp;
        tmp.x=max(1-w*(gl_FragColor.y+gl_FragColor.z),0);
        tmp.y=max(1-w*(gl_FragColor.x+gl_FragColor.z),0);
        tmp.z=max(1-w*(gl_FragColor.x+gl_FragColor.y),0);

        tmp=tmp*((1.0-glsl_ambient)*l.x+glsl_ambient);
        gl_FragColor.xyz=tmp+glsl_specular*l.y;
        gl_FragColor.yz*=mode_alpha;
        */
        
        /*
        float s=l.y;
        if (s>0.2)
        {
        gl_FragColor.xyz=min(color.xyz+0.1,1);
        }else
        {
        float l=(((1.0-glsl_ambient)*l.x+glsl_ambient));
        if (l > 0.8)
            gl_FragColor.xyz=color.xyz*0.9;
        else if (l > 0.6)
            gl_FragColor.xyz=color.xyz*0.75;
        else if (l > 0.4)
            gl_FragColor.xyz=color.xyz*0.3;
        else
            gl_FragColor.xyz=color.xyz*0.1;  
        }
        */
        if (l.x>0.35)
        {
           gl_FragColor.xyz=color.xyz+glsl_ambient;
        } else
        {
            gl_FragColor.xyz=color.xyz*0.1;
        }
        
        gl_FragColor.yz*=mode_alpha;
        
        //gl_FragColor.xyz*=0;
        
            
       }else 
       {
       
       gl_FragColor.xyz=color;
 	gl_FragColor.xyz+=light;
 	gl_FragColor.yz*=mode_alpha;
 	

      }

      
      
}

