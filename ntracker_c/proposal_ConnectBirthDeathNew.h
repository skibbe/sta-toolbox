#ifndef PROPOSAL_CONNECTBIRTHDEATH_H
#define PROPOSAL_CONNECTBIRTHDEATH_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;


 //#define DEBUG_CBD


	    

template<typename TData,typename T,int Dim>
class CConnectBirth : public CProposal<TData,T,Dim>
{
protected:

    T lambda;
    
    T particle_energy_change_costs;
    
//     mhs::dataArray<TData>  saliency_map;
//      double saliency_correction[3];
     
      bool temp_dependent;
	 bool use_saliency_map;
	  bool consider_birth_death_ratio;
bool sample_scale_temp_dependent;	  
T min_saliency;

public:
      T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,      
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
	return CProposal<TData,T,Dim>::dynamic_weight_terminal(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  
    CConnectBirth(): CProposal<TData,T,Dim>()
    {
        min_saliency = std::numeric_limits<T>::min();
        lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
// 	saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
	
	temp_dependent=true;     
	use_saliency_map=false;
	 consider_birth_death_ratio=false;
	 sample_scale_temp_dependent=false;
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

           if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	   
	   particle_energy_change_costs=update_energy_particle_costs(lambda);
	   
	   	if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	    
	   if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];
	   
	   if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];
	   
	   	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
	    
         if (mhs::mex_hasParam(params,"min_saliency")!=-1)
            min_saliency=mhs::mex_getParam<float>(params,"min_saliency",1)[0];
         
    }
    
    
    

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;
/*
        try {
            saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
        } catch (mhs::STAError error)
        {
            throw error;
        }
        
        saliency_correction[0]=std::numeric_limits< double >::max();
	saliency_correction[1]=std::numeric_limits< double >::min();
	saliency_correction[2]=0;
	CData<T,TData,Dim> & data_fun=*(this->tracker->data_fun);	
	
	std::size_t real_shape[3];
	std::size_t shape[3];
	std::size_t offset[3];
	data_fun.get_real_shape(real_shape);
	data_fun.getshape(shape);
	data_fun.getoffset(offset);
	TData * p_saliency_map=saliency_map.data;

	for (int z=offset[0];z<offset[0]+shape[0];z++)
	{
	  for (int y=offset[1];y<offset[1]+shape[1];y++)
	  {
	    for (int x=offset[2];x<offset[2]+shape[2];x++)
	    {
	      
	      std::size_t index=(z*real_shape[1]+y)*real_shape[2]+x;
	      
	      saliency_correction[0]=std::min(saliency_correction[0],double(p_saliency_map[index]));
	      saliency_correction[1]=std::max(saliency_correction[1],double(p_saliency_map[index]));
	      saliency_correction[2]+=p_saliency_map[index];
	    }
	  }
	}*/




    }

    bool propose()
    {
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			max_collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);

	
	bool debug=false;
	
	if (sample_scale_temp_dependent && (!(temp<maxscale)))
	 {
	      return false; 
	 }
	 




	this->proposal_called++;
	
	/*
	if (sample_scale_temp_dependent && (!(temp<maxscale)))
	 {
	      return false; 
	 }*/
	
	      /// randomly pic a terminal node
	      if (connections.get_num_terminals()>0)
              {
		
		
			
if (debug)		  
  printf("[X");
		
		Points<T,Dim> &  track_point=connections.get_rand_point();
		if (sample_scale_temp_dependent && ((temp>track_point.get_scale())))
		{
		      return false; 
		}
		
		
		T point_cost=-track_point.compute_cost3(temp);
		
		
		//NOTE since frezed points are "single" points not necessary
 		if (track_point.isfreezed())
 		{
 		    return false;
 		}
		
		#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
		  sta_assert_error(track_point.get_num_connections()==1);
		#else
		  sta_assert_error((track_point.endpoint_connections[0]==0)!=(track_point.endpoint_connections[1]==0));
		  int connected_side=(track_point.endpoint_connections[0]==0) ? 1 :0;
		  sta_assert_error(track_point.endpoint_connections[connected_side]<3);
		  sta_assert_error(track_point.endpoint_connections[1-connected_side]==0);
		#endif
		  
		Points<T,Dim> * p_point=particle_pool.create_obj();    
		Points<T,Dim> & point=*p_point;
		point.tracker_birth=1;
		int new_terminal_connection_side=1;
		
 		
	        // compute optimal pos, orientation and scale
		Vector<T,Dim> optimal_pos;
		Vector<T,Dim> optimal_direction;
		const T & optimal_scale=track_point.get_scale();
		
		int terminal_side=1-connected_side;
		
		optimal_direction=track_point.get_direction()*Points<T,Dim>::side_sign[terminal_side];
		
		optimal_pos=track_point.get_position()
                        +optimal_direction*(track_point.get_thickness()*2+optimal_scale/T(2.0));
		
			
			
		T temp_fact_pos=1;
		if (temp_dependent)
		    //temp_fact_pos=std::sqrt(temp);
		  temp_fact_pos=proposal_temp_trafo(temp,optimal_scale,T(0.5));
		
		T temp_fact_scale=temp_fact_pos+2;
		T temp_fact_rot=0.5*temp_fact_pos;
		
		T connection_probability=1;
		
		T position_sigma=temp_fact_pos*(optimal_scale/2);
		/// NEW POSITION
		Vector<T,Dim> randv;
		randv.rand_normal(position_sigma);
		point.set_position(optimal_pos+randv);
		
		//TODO only accept point if before terminal node ?
		
		
if (debug)		  
  printf("Y");		
// 		if ((point.position[0]<0)||
// 		  (point.position[1]<0)||
// 		(point.position[2]<0)||
// 		  (!(point.position[0]<shape[0]))||
// 		  (!(point.position[1]<shape[1]))||
// 		  (!(point.position[2]<shape[2]))
// 		)
		if (point.out_of_bounds(shape))
		{
if (debug)		  
  printf("]\n");		  
		  particle_pool.delete_obj(p_point);
		  return false;
		}
		connection_probability*=normal_dist<T,3>(randv.v,position_sigma);
		
		/// NEW ORIENTATION
 		point.set_direction(random_pic_direction<T,Dim>(
		  optimal_direction,temp_fact_rot));

		connection_probability*=normal_dist_sphere(optimal_direction,point.get_direction(),temp_fact_rot);
		// sign same as terminal node
		
		if (!single_scale)
		{
		
		    /// NEW SCALE
		    T scale_lower;
		    T scale_upper;
		    if (sample_scale_temp_dependent)
		    {
		      
		      scale_lower=std::max(std::max(minscale,temp),optimal_scale/temp_fact_scale);
		      scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
		      sta_assert_debug0(scale_lower<scale_upper);
		    }else
		    {
		      scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
		      scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
		    }
		    
		    
		    point.set_scale(myrand(scale_lower,scale_upper));
		    connection_probability*=1/((scale_upper-scale_lower)+std::numeric_limits<T>::epsilon());
		}else
		{
		    point.set_scale(maxscale);
		}
		
if (debug)		  
  printf("Z");			
		
	      class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	      class Collision<T,Dim>::Candidates candA(query_bufferA);
// 		  class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
// 		  class Collision<T,Dim>::Candidates candB(query_bufferB);

		
		    T max_pt_rad=std::max(maxscale,track_point.predict_thickness_from_scale(maxscale))+0.01;
		    T track_pt_collision_sphere=std::max(track_point.get_scale(),track_point.get_thickness())+max_pt_rad;
		    
		    T pt_collision_sphere=std::max(point.get_scale(),point.get_thickness())+max_pt_rad;
		    
		    sta_assert_debug0(!(max_collision_search_rad<pt_collision_sphere));
	      
		if (!collision_fun_p.is_soft())  
		{
		  T particle_interaction_cost_new=0;
		  //if (collision_fun_p.colliding( particle_interaction_cost_new,tree,point,collision_search_rad,temp,&candB,true,-9))
		  if (collision_fun_p.colliding( particle_interaction_cost_new,tree,point,pt_collision_sphere,temp))
		  {
  if (debug)		  
    printf("]\n");		  
		    particle_pool.delete_obj(p_point);
		    return false;
		  }
		}
		  
		class Connection<T,Dim> new_connection;
		new_connection.pointA=track_point.getfreehub(terminal_side);
		new_connection.pointB=point.getfreehub(new_terminal_connection_side);
		
		T newpointsaliency=0;
// 		if (!data_fun.interp3(newpointsaliency,
// 		  saliency_map.data, 
// 		  point.position,
// 		  shape))
		if ((!data_fun.saliency_get_value(newpointsaliency,point.get_position())) || (newpointsaliency<min_saliency))
		{
		   particle_pool.delete_obj(p_point);
		  return false;
		}
		
// 		if (newpointsaliency<std::numeric_limits<T>::epsilon())
// 		{
// 		    particle_pool.delete_obj(p_point);
// 		    return false;
// 		}
		
		//if (newpointsaliency<)
		
if (debug)		  
  printf("A");			
		/// evaluating data term
		T newenergy_data;	
		if (!data_fun.eval_data(
		      newenergy_data,
		      point))
// 		      point.get_direction(),
// 		      point.get_position(),
// 		       point.get_scale()))
		  {
if (debug)		  
  printf("]\n");		    
			    particle_pool.delete_obj(p_point);
			    return false;
		    }		
		
		    point.point_cost=newenergy_data;
		    point.saliency=newpointsaliency;/// saliency_correction[2];

		  /// computing edge cost
		  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		  
		  T new_edgecost=new_connection.e_cost(
		    edgecost_fun,
		    options.connection_bonus_L,
		    options.bifurcation_bonus_L,
		    inrange
		  );
		    
if (debug)		  
  printf("B");		  
		  
		    //connection.new_cost=connection.connection_new.compute_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,maxendpointdist,inrange);
		  if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		  {
if (debug)		  
  printf("]\n");			    
		    particle_pool.delete_obj(p_point);
		    return false;
		  }		    
		  /// edge energy
		  
		  
		  point_cost=+point.compute_cost3(temp);
		  point_cost=+track_point.compute_cost3(temp);

		  T E=(newenergy_data+point_cost+new_edgecost);

		  /*
		  T E=(newenergy_data)/temp;
		  
		  E+=(point_cost)/temp;
		    
		  E+=new_edgecost/temp;
		  */  
		    
		
		      
		      
			      
		
		
		  T particle_terminalcool_cost_old=0;
		  T particle_terminalcool_cost_new=0;


		  


		  
		    sta_assert_error(!p_point->has_owner());
		
// 		    if (!tree.insert(*p_point))
// 		    {   
// 			particle_pool.delete_obj(p_point);
// 			return false;
// 		    }	
		    

		    
		    
		    if (collision_fun_p.is_soft())    
		    {
		      T tmp=0;
		      			sta_assert_error(!collision_fun_p.colliding(tmp,
								  tree,
								  track_point,
								  track_pt_collision_sphere,
								  temp,&candA,true,-9,1));
			  particle_terminalcool_cost_old+=tmp;
 			
// 			sta_assert_error(!collision_fun_p.colliding(tmp,
// 								  tree,
// 								  *new_connection.pointA->point,
// 								  collision_search_rad,
// 								  temp,&candA,true,-9,1));
// 			  particle_terminalcool_cost_old+=tmp;
// 			  
// 			  
// 			  sta_assert_error(!collision_fun_p.colliding(tmp,
// 								  tree,
// 								  *new_connection.pointB->point,
// 								  collision_search_rad,
// 								  temp,&candB,false,-9,1));
//			  particle_terminalcool_cost_old+=tmp;
		    } 
		
		    
// 		    if (!conn_tree.insert(*p_point))
// 		    {
// 			particle_pool.delete_obj(p_point);
// 			return false;
// 		    }
// 		    track_point.unregister_from_grid(voxgrid_conn_can_id);
		    
		    
	
		      Connection<T,Dim>   *connection_new=NULL;	
		      connection_new=connections.add_connection(new_connection);
		      
		      sta_assert_error(connection_new!=NULL);
		      
		      
		      
		      //NOTE: point is not visible for track_point (not in tree at the moment)
		      
		      if (collision_fun_p.is_soft())    
		      {
			  T tmp=0;
			  sta_assert_error(!collision_fun_p.colliding(tmp,
								    tree,
								    track_point,
								    track_pt_collision_sphere,
								    temp,&candA,false,-10,1));
			    particle_terminalcool_cost_new+=tmp;
			    
			    
			    if(collision_fun_p.colliding(tmp,
								    tree,
								    point,
								    pt_collision_sphere,
								    temp))
			    {
			     		   sta_assert_debug0(connection_new!=NULL);
					    connections.remove_connection(connection_new);
                    
					    particle_pool.delete_obj(p_point); 
					    
					    return false;
			    }
			    particle_terminalcool_cost_new+=tmp;
		      } 
		      
		      
		      if (collision_fun_p.is_soft())   
		      {
			  E+=(particle_terminalcool_cost_new-particle_terminalcool_cost_old);
		      }
		
		
		    T E_debug=E;
		    E/=temp;
		
		
		    T R=mhs_fast_math<T>::mexp(E);
		    
		    
		  R/=connection_probability
		      *(single_scale ? 1 : (maxscale-minscale))
		      +std::numeric_limits<T>::epsilon();
		      
		      
		      if (consider_birth_death_ratio)
		{
		      if (use_saliency_map)
		      {
			  R*=(lambda)*this->call_w_undo;
			  R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*(nvoxel*point.saliency)*this->call_w_do;//*cpoint.saliency;
		      } else
		      {
			  R*=(lambda)*this->call_w_undo;
			  R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*this->call_w_do;//*cpoint.saliency;
		      }
		}	
		      	    
 
		    if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
                    {
		      
		      #ifdef  D_USE_GUI
		      #ifdef  DEBUG_CBD
				  GuiPoints<T> * gps= ( ( GuiPoints<T> * ) (&track_point)) ;
		      #endif
		      #endif
		      
		#ifdef  D_USE_GUI
		      #ifdef  DEBUG_CBD		  
		if (gps->is_selected)
		{		  
		      printf("CD: [E: %f  E0 %f R: %f] [coll %f %f ] [pc %f] [ec %f] [data %f] [%f cp] \n",
			E,
			E_debug,
			R,
			particle_terminalcool_cost_old,
			particle_terminalcool_cost_new,
			point_cost,
			new_edgecost,
			newenergy_data,
			connection_probability
		      );
		      printf("[%e^(-E)] [%f %f] [%f / %f]\n",
			     mhs_fast_math<T>::mexp(E),
			     connection_probability,
			     (single_scale ? 1 : (maxscale-minscale)),
			(lambda)*this->call_w_undo,
			std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*(nvoxel*point.saliency)*this->call_w_do    
		      );
		}
		  #endif
		      #endif    
		      
		     
		      

// 			sta_assert_error(!p_point->has_owner());
// 		
// 			if (!tree.insert(*p_point))
// 			{   
// 			    particle_pool.delete_obj(p_point);
// 			    return false;
// 			    
// 			}	
// 			
// 			if (!conn_tree.insert(*p_point))
// 			{
// 			    particle_pool.delete_obj(p_point);
// 			    return false;
// 			}
// 			track_point.unregister_from_grid(voxgrid_conn_can_id);
// 			
// 			sta_assert_debug0(p_point->has_owner());
// 	
// 		      Connection<T,Dim>   *connection_new=NULL;	
// 		      connection_new=connections.add_connection(new_connection);
// 		      
// 		      sta_assert_error(connection_new!=NULL);
		      
		      if (!conn_tree.insert(*p_point))
		      {
			  connections.remove_connection(connection_new);
			  particle_pool.delete_obj(p_point);
			  return false;
		      }
		    if (!tree.insert(*p_point))
		    {   
		        connections.remove_connection(connection_new);
			particle_pool.delete_obj(p_point);
			return false;
		    }			      
		      sta_assert_debug0(p_point->has_owner());
		      
		      track_point.unregister_from_grid(voxgrid_conn_can_id);
		      
		      
		      p_point->_path_id=track_point._path_id;
		      
		      
		      #ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
		      sta_assert_error(!Constraints::constraint_hasloop((*connection.connection_new.pointA->point),options.constraint_loop_depth));
		      sta_assert_error(!Constraints::constraint_hasloop((*connection.connection_new.pointB->point),options.constraint_loop_depth));
		      sta_assert_error(!Constraints::constraint_hasloop((*connection_new->pointA->point),options.constraint_loop_depth));
		      sta_assert_error(!Constraints::constraint_hasloop((*connection_new->pointB->point),options.constraint_loop_depth));
		      #endif
		
		      this->tracker->update_energy(E+particle_energy_change_costs,static_cast<int>(get_type()));
		      this->proposal_accepted++;
		      
		      
		      #ifdef  D_USE_GUI
// 			(GuiPoints<T>* (&point))->touch(get_type());
// 			(GuiPoints<T>* (&track_point))->touch(get_type());
			point.touch(get_type());
			track_point.touch(get_type());
		      #endif
		      
		      /*
		      if (return_statistic)	
		      {
			  std::size_t center=(std::floor(point.position[0])*shape[1]
			    +std::floor(point.position[1]))*shape[2]
			    +std::floor(point.position[2]);
			static_data[center*2*numproposals+1]+=1;
		      }
		      */
		      
		      this->tracker->update_volume(p_point->get_scale());
	      
      
				return true;
                    }
                    
                   //sta_assert_error(conn_tree.insert(track_point));
		   sta_assert_debug0(connection_new!=NULL);
		  connections.remove_connection(connection_new);
                    
		     particle_pool.delete_obj(p_point);
		}
	
        return false;
    }
};

template<typename TData,typename T,int Dim>
class CConnectDeath : public CProposal<TData,T,Dim>
{
protected:
    T lambda;
T particle_energy_change_costs;
	 bool use_saliency_map;
	  bool consider_birth_death_ratio;
bool temp_dependent;
bool sample_scale_temp_dependent;	
public:
  
  T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_terminal(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  CConnectDeath(): CProposal<TData,T,Dim>()
    {
        lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	temp_dependent=true;
	
	use_saliency_map=false;
	 consider_birth_death_ratio=false;
	 sample_scale_temp_dependent=false;
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

        if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	    
	   if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];
	   
	   if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];
	   
	   	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;
    }

    bool propose()
    {
pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			max_collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);

	this->proposal_called++;	
	
	      /// uniformly select terminal, where after removal remains a terminal
	      if (connections.get_num_terminals()>0)
              {
		
		Points<T,Dim> &  point=connections.get_rand_point();
		
		if (point.isfreezed())
		{
		    return false;
		}
		
		if (point.is_protected_topology())
		{
		    return false;
		}	 
	    
		if (sample_scale_temp_dependent && ((temp>point.get_scale())))
		{
		      return false; 
		}
				    
		#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
		  sta_assert_error(point.get_num_connections()==1);
		#else
		  sta_assert_error((point.endpoint_connections[0]==0)!=(point.endpoint_connections[1]==0));
		  /// check if bifurcation terminal
		  if (point.get_num_connections()>1)
		    return false;
		#endif
		  
		sta_assert_error(point.get_num_connections()==1);
		const int connected_side=(point.endpoint_connections[0]==1) ? 0 : 1;
		
		/*
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
		  static_data[center*2*numproposals+2]+=1;
		}
		*/
		
		/// find the connected edge
		class Connection<T,Dim> * connection=point.endpoints[connected_side][0]->connection;
		Points<T,Dim> *  new_terminal=point.endpoints[connected_side][0]->connected->point;
		int new_terminal_connected_side=point.endpoints[connected_side][0]->connected->side;
		
		//check if connected to a full segment that would become a 
		//terminal in the reverse operation
		if ((new_terminal->endpoint_connections[0]!=1)||
		  (new_terminal->endpoint_connections[1]!=1))
		  return false;
		
		
	      T point_cost=-point.compute_cost3(temp)-new_terminal->compute_cost3(temp);
	      

	      /// computing edge cost
	      typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	      
	      
	      T edge_cost=connection->e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
	      sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),
		(connection->e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange,true,true))
	      );
		
		
	      /// computing optimal pos, scale, orientation
	      Vector<T,Dim> optimal_pos;
	      Vector<T,Dim> optimal_direction;
	      T optimal_scale=new_terminal->get_scale();
	      
	      
	      optimal_direction=new_terminal->get_direction()*Points<T,Dim>::side_sign[new_terminal_connected_side];
	      optimal_pos=new_terminal->get_position()
                        +optimal_direction*(new_terminal->get_thickness()*2+optimal_scale/T(2.0));
	      

			
	      
	      T temp_fact_pos=1;
		if (temp_dependent)
		  temp_fact_pos=proposal_temp_trafo(temp,optimal_scale,T(0.5));
		  //temp_fact_pos=std::sqrt(temp);
	      
	      T temp_fact_scale=temp_fact_pos+2;
	      T temp_fact_rot=0.5*temp_fact_pos;	      
		
	      
	      T position_sigma=temp_fact_pos*(optimal_scale/2);
	      
	      T connection_probability=1;
	      
	      Vector<T,3> displacement;
	      displacement=(point.get_position()-optimal_pos);
	      connection_probability*=normal_dist<T,3>(displacement.v,position_sigma);
	      
// 		//TODO check if old terminal is in front of new terminal?

// 		/// make sure that direction vector points in direction of not-connected endpoint
 	      Vector<T,3> direction=point.get_direction()*Points<T,Dim>::side_sign[1-connected_side];
	      
	      connection_probability*=normal_dist_sphere(optimal_direction,direction,temp_fact_rot);
	      
	      
	      if (!single_scale)
	      {
		T scale_lower;
		T scale_upper;
		if (sample_scale_temp_dependent)
		{
		  scale_lower=std::max(std::max(minscale,temp),optimal_scale/temp_fact_scale);
		  scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
		  sta_assert_debug0(scale_lower<scale_upper);
		}else
		{
		  scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
		  scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
		}
		
		// not in scale interval!
		if ((point.get_scale()<scale_lower)||(point.get_scale()>scale_upper))
		{
		  return false;
		}
		
		connection_probability*=1.0/((scale_upper-scale_lower)+std::numeric_limits<T>::epsilon());
		
	      }
	      
	      
// 	      	      class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
// 	      class Collision<T,Dim>::Candidates candA(query_bufferA);
		  class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
		  class Collision<T,Dim>::Candidates candB(query_bufferB);
		  
	      
	      T particle_interaction_cost_new=0;
	      T particle_interaction_cost_old=0;
	      
	    class Points<T,Dim>    & track_point=*new_terminal;
	    T max_pt_rad=std::max(maxscale,track_point.predict_thickness_from_scale(maxscale))+0.01;
	    T track_pt_collision_sphere=std::max(track_point.get_scale(),track_point.get_thickness())+max_pt_rad;
	    
	    T pt_collision_sphere=std::max(point.get_scale(),point.get_thickness())+max_pt_rad;
	    
	    sta_assert_debug0(!(max_collision_search_rad<pt_collision_sphere));
	      
	      
	      if (collision_fun_p.is_soft())    
	      {
		
		
		  T tmp=0;
		  sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							    point,
							    pt_collision_sphere,
							    temp));
		    particle_interaction_cost_old+=tmp;
		    
		  //NOTE we hide the bridge point from the tree
		    std::size_t  npts_old=tree.get_numpts();
		    point.unregister_from_grid (voxgrid_default_id);
		    std::size_t  npts_new=tree.get_numpts();
		    sta_assert_debug0 ( npts_new+1==npts_old );

		    
		    
		    sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							    *new_terminal,
							    track_pt_collision_sphere,
							    temp,&candB,true,-9,1));
		    particle_interaction_cost_old+=tmp;
	      }
	      
		class Connection<T,Dim> backup_edge;
		backup_edge=*connection;
		
		connections.remove_connection(connection);		
		
		
		if (collision_fun_p.is_soft())    
		{
		    T tmp=0;
		    sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							    *new_terminal,
							    track_pt_collision_sphere,
							    temp,&candB,false,-10,1));
		    particle_interaction_cost_new+=tmp;
		}
	      
	      
	      
// 	      if (collision_fun_p.is_soft())    
// 	      {
// 		sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp));
// 		
// 	      }
	      
	      
	      
	         
	      
		  
	      //NOTE: I added a - here, was + before (Nov, 05)
	      T E=-edge_cost/temp;
	    
	      T newenergy=point.point_cost;
	      E-=newenergy/temp;
	      
	       if (collision_fun_p.is_soft())   
	       {
		E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	       }
	      
	      
	      
// 	       T pointcost=particle_connection_state_cost_scale[1]*point.compute_cost()+(particle_connection_state_cost_offset[1])//+PointScaleCost*point.compute_cost_scale() //0 -> 1 connection
// 				+(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])*new_terminal->compute_cost()+(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]); //1 -> 2
	      
// 	      E-=pointcost/temp;		
	      
	      point_cost+=new_terminal->compute_cost3(temp);
	      E+=point_cost/temp;		
	          
		   
		    
	      T R=mhs_fast_math<T>::mexp(E);
	      	    


		    R*=connection_probability
			*(single_scale ? 1 : (maxscale-minscale));
	    
		if (consider_birth_death_ratio)
		{
		  if (use_saliency_map)
		  {
		      R*=(tree.get_numpts())*(nvoxel*point.saliency)*this->call_w_undo;
		      R/=std::numeric_limits<T>::epsilon()+(lambda)*this->call_w_do;
		  } else
		  {
		      R*=(tree.get_numpts())*this->call_w_undo;
		      R/=(lambda)*this->call_w_do;
		  }
		}
		
		
		/*
		  if (consider_birth_death_ratio)
		{
		      if (use_saliency_map)
		      {
			  R*=(lambda)*this->call_w_undo;
			  R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*(nvoxel*point.saliency)*this->call_w_do;//*cpoint.saliency;
		      } else
		      {
			  R*=(lambda)*this->call_w_undo;
			  R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*this->call_w_do;//*cpoint.saliency;
		      }
		}	
		  */    	    
		
			
	      
		if ((R>=myrand(1)+std::numeric_limits<T>::epsilon())&&((conn_tree.insert(*new_terminal,false,true))))
		{

		#ifdef  D_USE_GUI
		      #ifdef  DEBUG_CBD
				  GuiPoints<T> * gps= ( ( GuiPoints<T> * ) (new_terminal)) ;
		      #endif
		      #endif
		      
		#ifdef  D_USE_GUI
		      #ifdef  DEBUG_CBD		  
		if (gps->is_selected)
		{		  
		      //printf("CB: [E: %f  E0 %f R: %f] [coll %f %f ] [pc %f] [ec %f] [data %f] [%f cp] \n",
		  printf("CD: [E: %f R: %f] [coll %f %f ] [pc %f] [ec %f] [data %f] [%f cp] \n",
			E,
			//E_debug,
			R,
			particle_interaction_cost_old,
			particle_interaction_cost_new,
			point_cost,
			edge_cost,
			newenergy,
			connection_probability
		      );
		      printf("[%e^(-E)] [%f %f] [%f / %f]\n",
			     mhs_fast_math<T>::mexp(E),
			     connection_probability,
			     (single_scale ? 1 : (maxscale-minscale)),
			(tree.get_numpts())*(nvoxel*point.saliency)*this->call_w_undo,
			std::numeric_limits<T>::epsilon()+(lambda)*this->call_w_do  
		      );
		}
		  #endif
		      #endif    		  
		  
		  
		  
		  this->tracker->update_energy(E-particle_energy_change_costs,static_cast<int>(get_type()));
		  this->proposal_accepted++;		  
		  
    
		  
		  
		  
		  
		  Points< T, Dim > * p_point=&point;
		  this->tracker->update_volume(-p_point->get_scale());
		  
		  
		  particle_pool.delete_obj(p_point);
		  sta_assert_debug0(p_point==NULL);
		  
		  if (!conn_tree.insert(*new_terminal))
		  {
		      new_terminal->print();
		      throw mhs::STAError("error insering point\n");
		      return false;
		  }
		  
		  
		 
		  /*   
		  if (return_statistic)	
		  {
		      std::size_t center=(std::floor(point.position[0])*shape[1]
			+std::floor(point.position[1]))*shape[2]
			+std::floor(point.position[2]);
		    static_data[center*2*numproposals+3]+=1;
		  }
		  */
		  
		  #ifdef  D_USE_GUI
// 			(GuiPoints<T>* (new_terminal))->touch(get_type());
			new_terminal->touch(get_type());
		  #endif
		  
		  return true;
		}
		
		
		 if ( collision_fun_p.is_soft() ) {
		      sta_assert_error ( tree.insert ( point) );
		  }
		  connections.add_connection ( backup_edge);

		}



        return false;

    }
};


#endif


