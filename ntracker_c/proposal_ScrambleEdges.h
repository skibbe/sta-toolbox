#ifndef PROPOSAL_SCRABME_H
#define PROPOSAL_SCRABME_H
#include "proposals.h"

 template<typename TData,typename T,int Dim> class CProposal;



template<typename TData,typename T,int Dim>
class CProposalScrambleEdges : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
    T ConnectEpsilon;

    
    unsigned int edge_configs[8][6];	
 
public:
  
  T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_edges(num_edges,
		      num_particles,
		      num_connected_particles,		      
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }



    CProposalScrambleEdges(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
	
	    unsigned  config[6];
	    
	    
	    //c=1;i=0; for j=i+1:5, for k=0:5, if ((k~=i) && (k~=j)) for l=k+1:5 if ((l~=i) && (l~=j)),D=[];for m=1:5, if ((m~=i)&&(m~=j)&&(m~=k)&&(m~=l)) D=[D,m];end;end; fprintf('%d:[%d %d] [%d %d] [%d %d]\n',c,i,j,k,l,D(1),D(2));c=c+1; end; end; end;end;end;
	    //c=1;i=0; for j=i+1:4, for k=0:5, if ((k~=i) && (k~=j)) for l=k+1:5 if ((l~=i) && (l~=j)),D=[];for m=1:5, if ((m~=i)&&(m~=j)&&(m~=k)&&(m~=l)) D=[D,m];end;end; if  (~((k==1)&&((l==2)))) &&  (~((k==3)&&((l==4))))  && (~((D(1)==1)&&((D(2)==2)))) && (~((D(1)==3)&&((D(2)==4)))) fprintf('%d:[%d %d] [%d %d] [%d %d]\n',c,i,j,k,l,D(1),D(2));c=c+1; end; end; end;end;end;end;
	    //c=1;i=0; for j=i+1:4, for k=0:5, if ((k~=i) && (k~=j)) for l=k+1:5 if ((l~=i) && (l~=j)),D=[];for m=1:5, if ((m~=i)&&(m~=j)&&(m~=k)&&(m~=l)) D=[D,m];end;end; if  (~((k==1)&&((l==2)))) &&  (~((k==3)&&((l==4))))  && (~((D(1)==1)&&((D(2)==2)))) && (~((D(1)==3)&&((D(2)==4)))) && (k>D(1)) fprintf('%d:[%d %d] [%d %d] [%d %d]\n',c,i,j,k,l,D(1),D(2));c=c+1; end; end; end;end;end;end;
	    

	    unsigned  config_count=0;
	    const unsigned  int i=0;
	    config[0]=0;
	    for (int j=i+1;j<5;j++)
	    {
	      config[1]=j;
	      for (int k=0;k<6;k++)
	      {
		config[2]=k;
		if ((k!=i) && (k!=j))
		{
		  for (int l=k+1;l<6;l++)
		  {
		    config[3]=l;
		    if ((l!=i) && (l!=j))
		    {
		      int count=4;
		      for (int m=1;m<6;m++)
		      {
			 if ((m!=i)&&(m!=j)&&(m!=k)&&(m!=l)) 
			 {
			   sta_assert_debug0(count<6);
			   config[count++]=m;
			 }
		      }
		      
		      
		      
		      if  ((!((config[2]==1)&&((config[3]==2)))) &&  
			   (!((config[2]==3)&&((config[3]==4)))) &&
			   (!((config[4]==1)&&((config[5]==2)))) &&  
			   (!((config[4]==3)&&((config[5]==4)))) &&
			   (config[2]<config[4])
			  )
		      {
			sta_assert_debug0(config_count<8);		      
			for (int c=0;c<6;c++)
			{
			  edge_configs[config_count][c]=config[c];
			}
			printf("%d [%d %d] [%d %d] [%d %d] \n",config_count,config[0],config[1],config[2],config[3],config[4],config[5]);
			
			config_count++;
		      }
		    }
		  }
		}
	      }
	    }
    }
    
    ~CProposalScrambleEdges()
    {

    }
    

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
    }
    
    class CConfigProposal
    {
      public:
	Points<T,Dim> * points[4];
	class Points< T, Dim >::CEndpoint * endpoints[6];
    };

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	
	this->proposal_called++;
	
	class Connection<T,Dim> * edge;

        /// uniformly pic edge
        edge=connections.get_rand_edge();
        if (edge==NULL)
            return false;
        
        if (edge->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
	  return false;

	
	
	sta_assert_debug0((*edge->pointA->endpoint_connections==1)&&
 			  (*edge->pointB->endpoint_connections==1));
	
	//if (edge->pointA->endpoint_connections[0])
	if ((edge->pointA->point->get_num_connections()!=2)||(edge->pointB->point->get_num_connections()!=2))
	{
	 return false; 
	}
	
	
	class Points< T, Dim >::CEndpoint * epoints[6];
	
// 	epoints[0]=*edge->pointA;
// 	epoints[1]=*edge->pointB;
// 	epoints[2]=epoints[0]->opposite_slots[0];
// 	epoints[3]=epoints[2]->connected;
// 	
// 	epoints[4]=epoints[1]->opposite_slots[0];
// 	epoints[5]=epoints[4]->connected;
	
	epoints[2]=edge->pointA;
	epoints[3]=edge->pointB;
	epoints[1]=epoints[2]->opposite_slots[0];
	epoints[0]=epoints[1]->connected;
	
	epoints[4]=epoints[3]->opposite_slots[0];
	epoints[5]=epoints[4]->connected;
	
	for (int i=0;i<6;i++)
	{
	  sta_assert_debug0(epoints[i]!=NULL);
	  for (int j=i+1;j<6;j++)
	    sta_assert_debug0(epoints[i]!=epoints[j]);
	}
	
	class Connection<T,Dim> * edges[3];
	edges[0]=edge;
// 	edges[1]=epoints[2]->connection;
// 	edges[2]=epoints[4]->connection;
	edges[1]=epoints[0]->connection;
	edges[2]=epoints[5]->connection;
	
	if ((edges[1]->edge_type== ( EDGE_TYPES::EDGE_BIFURCATION ))||(edges[2]->edge_type== ( EDGE_TYPES::EDGE_BIFURCATION )))
	  return false;
	
	sta_assert_debug0(edges[0]!=edges[1]);
	sta_assert_debug0(edges[0]!=edges[2]);
	sta_assert_debug0(edges[1]!=edges[2]);
	
	try {		  

	  int valid_indx[8];
	  int num_valid=0;
	  T costs[8];
	  T prob[8];
	  T prob_acc[8];
	  
	  
// 	  T searchrad=options.connection_candidate_searchrad;
// 	  searchrad*=searchrad;
	  
	  for (int c=0;c<8;c++)
	  {
		//TODO: the intervals [0:4] [5:8] [9:12] [13:16] have the same costs
		typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	       T u=edgecost_fun.e_cost(*epoints[edge_configs[c][0]],
				       *epoints[edge_configs[c][1]],
				       inrange);
	       //
		if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		u+=edgecost_fun.e_cost(*epoints[edge_configs[c][2]],
				       *epoints[edge_configs[c][3]],
				       inrange);
		if (c==0)
		{
		 sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)); 
		}
	       
		if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		u+=edgecost_fun.e_cost(*epoints[edge_configs[c][4]],
				       *epoints[edge_configs[c][5]],
				       inrange);
	       
		if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		valid_indx[num_valid]=c;
		costs[num_valid]=u+options.connection_bonus_L;
		prob[num_valid]=mhs_fast_math<T>::mexp((u+Lprior)/conn_temp)+ConnectEpsilon;;
		
		
		if (num_valid==0)
		{
		    prob_acc[num_valid]=prob[num_valid];
		}
		else
		{
		    prob_acc[num_valid]=prob[num_valid]+prob_acc[num_valid-1];
		}
		
		num_valid++;
	  }
	  
	  sta_assert_debug0(num_valid>0);
	  
	  
/*	  
{
	    
	  	  
	   int valid_indx2[8];
	  int num_valid2=0;
	  T costs2[8];
	  
	  T tmp;
	  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange0;
	 
	  for (int c=0;c<8;c++)
	  {
		
		if (c%2==0)
		{
		  tmp=edgecost_fun.u_cost(*epoints[edge_configs[c][0]],
				       *epoints[edge_configs[c][1]],
				       searchrad,
				       inrange0);
		}
		if (inrange0!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		T u=tmp;
		
// 		T u=edgecost_fun.u_cost(*epoints[edge_configs[c][0]],
// 				       *epoints[edge_configs[c][1]],
// 				       searchrad,
// 				       inrange);
		
		typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		
		if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		u+=edgecost_fun.u_cost(*epoints[edge_configs[c][2]],
				       *epoints[edge_configs[c][3]],
				       searchrad,
				       inrange);
		if (c==0)
		{
		 sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)); 
		}
	       
		if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		u+=edgecost_fun.u_cost(*epoints[edge_configs[c][4]],
				       *epoints[edge_configs[c][5]],
				       searchrad,
				       inrange);
	       
		if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
		{
		  continue;
		}
		
		valid_indx2[num_valid2]=c;
		costs2[num_valid2]=u+options.connection_bonus_L;
		
		num_valid2++;
	  }
	  
	  if (num_valid2!=num_valid)
	  {
	    printf("%d %d\n",num_valid,num_valid2);
	    int m=(num_valid>num_valid2) ? num_valid : num_valid2;
	    for (int a=0;a<m;a++)
	    {
	      printf("%f  \  %f\n",costs[a],costs2[a]);
	    }
	  }
// 	  sta_assert_error_c(num_valid2==num_valid,printf("!! %d %d !!\n",num_valid2,num_valid));
// 	  
// 	  for (int a=0;a<num_valid2;a++)
// 	  {
// 	    sta_assert_error(costs[a]==costs2[a]);
// 	  }
	  
}	 */ 
	  
	  
	  //only the existing configuration is valid -> do nothing
	  if (num_valid==1)
	    return false;
	  
	  std::size_t connection_candidate=rand_pic_array(prob_acc,num_valid,prob_acc[num_valid-1]);
	  
	  sta_assert_debug0(connection_candidate<num_valid);
	  
	  //the new configuration is the old one 
	  if (connection_candidate==0)
	    return false;

	  
	  T E=(costs[connection_candidate]-costs[0])/temp;
	
	  T R=mhs_fast_math<T>::mexp(E);	
	  
	  R*=prob[0]/((prob[connection_candidate])+std::numeric_limits<T>::epsilon());
	  
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	     connections.remove_connection(edges[0]);
	     connections.remove_connection(edges[1]);
	     connections.remove_connection(edges[2]);
	     
	     class Connection<T,Dim>  new_connection;
	     for (int e=0;e<3;e++)
	     {
	      new_connection.pointA= epoints[edge_configs[valid_indx[connection_candidate]][e*2]];
	      new_connection.pointB= epoints[edge_configs[valid_indx[connection_candidate]][e*2+1]];
	      
	      connections.add_connection(new_connection);
	     }
	    
		#ifdef  D_USE_GUI
		    epoints[0]->point->touch(get_type());
		    epoints[1]->point->touch(get_type());
		    epoints[3]->point->touch(get_type());
		    epoints[5]->point->touch(get_type());
		#endif
	    
	    
	      this->tracker->update_energy(E,static_cast<int>(get_type()));
	      this->proposal_accepted++;
		
	      return true;
	  }
	  
	} catch (mhs::STAError error)
	{
	    throw error;
	}
	
	return false;
	
    }
    
//      bool  do_tracking3(
//         class CEdgecost<T,Dim> & edgecost_fun,
// 	CConfigProposal  & proposal,
// 	T & new_cost,
// 	T & new_prop,
//         OctTreeNode<T,Dim> & tree,
//         //class Points<T,Dim>::CEndpoint & slot,
// 	class Points<T,Dim> & terminal_point,
//         T search_connection_point_center_candidate, 
//         T searchrad, 
//         T TempConnect,
//         T L
//     )
//     {
//       bool debug=false;
//       
//       const Vector<T,Dim> & terminal_point_pos=terminal_point.get_position();
//       
//       std::size_t found;
//         class OctPoints<T,Dim> * query_buffer[query_buffer_size];
//         class OctPoints<T,Dim> ** candidates;
//         candidates=query_buffer;
// 
//         try {
//             tree.queryRange(
//                 terminal_point_pos,
//                 search_connection_point_center_candidate,
//                 candidates,
//                 query_buffer_size,
//                 found);
//         } catch (mhs::STAError & error)
//         {
//             throw error;
//         }
//         
//         if (found==1)
//         {
//             if (debug)
//                 printf("no points\n",found);
//             return false;
//         }
//         
//         searchrad*=searchrad;
//         search_connection_point_center_candidate*=search_connection_point_center_candidate;
//         
//       
// 	class CConfigProposal * candidates_buffer[query_buffer_size];
//         class CConfigProposal **candidates=candidates_buffer;
// 	
// 	std::size_t num_candidates=0;
// 	
// 	CConfigProposal  tmp_proposal;
// 	
// 	 int unconnected_side=(terminal_point.endpoint_connections[0]==0) ? 0 : 1;
// 	tmp_proposal.points[2]=&terminal_point;
// 	tmp_proposal.endpoints[4]=&(terminal_point.endpoints_pos[unconnected_side]);
// 	
// 	for (std::size_t a=0; a<found; a++)
// 	{
// 	    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
// 	    
// 	    sta_assert_error(point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
// 	    sta_assert_error(track_point.get_num_connections()==1);
// 	    
// 	    T candidate_center_dist_sq=(Vector<T,Dim>(point->get_position())-terminal_point.get_position()).norm2();
// 	    if (search_connection_point_center_candidate>candidate_center_dist_sq)
// 	    {
// 	      
// 	      
// 	      
// 	    }
// 	}
// 	
// 	
// 	
// 	
//     }
    
    


};

#endif


