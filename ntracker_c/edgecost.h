#ifndef EDGECOST_H
#define EDGECOST_H

#include "global.h"


#define EDGECOST_DEBUG



template<typename T,int Dim> class Points;

template <typename T,int Dim>
class CEdgecost
{
  
private:

protected:


public:
  enum class EDGECOST_STATE : unsigned int {
    EDGECOST_STATE_OUT_OF_RANGE,
    EDGECOST_STATE_VIOLATES_CONSTRAINT,
    EDGECOST_STATE_VIOLATES_CONSTRAINT_ANGLE,
    EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_AREA,
    EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_ANGLE,
    EDGECOST_STATE_IN_RANGE,
  };
  
    static T side_scale;
    static T _max_edge_length[max_tracking_treads];  
    static T max_edge_length_hard;
    
    
    CEdgecost()
    {
     // max_edge_length=-1;
      CEdgecost<T,Dim>:: max_edge_length_hard=-1;
    }


    virtual ~CEdgecost() {};
    
    static void static_init()
    {
      for (std::size_t t=0;t<max_tracking_treads;t++)
      {
	CEdgecost<T,Dim>:: _max_edge_length[t]=-1;
      }
    }
    
    static void set_max_edge_length_soft(const T maxlength, int thread_id=-1)
    {
      sta_assert_error(!(maxlength<0));
      
      if (thread_id<0)
      {
	thread_id=get_thread_id();
      }
      
      //CEdgecost<T,Dim>:: max_edge_length=maxlength*maxlength;

      CEdgecost<T,Dim>:: _max_edge_length[thread_id]=maxlength;
    }
    static void set_max_edge_length_hard(const T maxlength)
    {
      sta_assert_error(!(maxlength<0));
      CEdgecost<T,Dim>:: max_edge_length_hard=maxlength;
    }
    
    static T get_current_edge_length_limit(int thread_id=-1)
    {
      if (thread_id<0)
      {
	thread_id=get_thread_id();
      }
      if (CEdgecost<T,Dim>::_max_edge_length[thread_id]>0)
      {
	return std::min(CEdgecost<T,Dim>::_max_edge_length[thread_id],CEdgecost<T,Dim>::max_edge_length_hard);
      }
      return CEdgecost<T,Dim>::max_edge_length_hard;
    }
    
    static T get_total_edge_length_limit()
    {
      return CEdgecost<T,Dim>::max_edge_length_hard;
    }
    

    virtual T e_cost(
        const class Points< T, Dim >::CEndpoint & endpointA,
        const class Points< T, Dim >::CEndpoint & endpointB,
//         T max_endpoint_dist,
        EDGECOST_STATE & inrange,
	bool check_distance=true,
        bool debug=false
#ifdef EDGECOST_DEBUG        
	, std::stringstream * debugstream=NULL
#endif        
    )=0;
    
    virtual T e_cost(
        const class Points< T, Dim >::CEndpoint & endpointA,
        const class Points< T, Dim >::CEndpoint & endpointB,
	const class Points< T, Dim >::CEndpoint & endpointC,
//         T max_endpoint_dist,
        EDGECOST_STATE & inrange,
	bool check_distance=true,
        bool debug=false
    )=0;

    virtual void set_params(const mxArray * params=NULL)
    {
       try {
	
        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"connection_candidate_searchrad")!=-1)
	    {
                set_max_edge_length_hard(mhs::mex_getParam<T>(params,"connection_candidate_searchrad",1)[0]);
	    }
        }
      }
        catch (mhs::STAError error)
        {
	  throw error;
        }
      
    };
    
    #ifdef D_USE_GUI	        
    virtual void read_controls(const mxArray * handle)=0;
    virtual void set_controls(const mxArray * handle)=0;
    #endif  
    
    
    virtual bool particle_angle_valid(const class Points< T, Dim >::CEndpoint & endpointA,
				      const class Points< T, Dim >::CEndpoint & endpointB,
				     bool debug=false)=0;
				     
				     
  bool valid_triangle(const T & a,const T & b,const T & c,T minratio)
  {
	if (a>b)
	{
	 if (a>c)
	 {
	   if ((((b+c)/a)<minratio)) // a>b,c
	   {
// 	     sta_assert_debug0(a>b);
// 	     sta_assert_debug0(a>c);
	    return false;
	   }
	   sta_assert_debug0(!(a-(b+c)>0));
	 }else
	 {
	   if ((((b+a)/c)<minratio))  // c>a,b
	   {
// 	     sta_assert_debug0(c>b);
// 	     sta_assert_debug0(c>a);
	    return false;
	   }
	   sta_assert_debug0(!(c-(b+a)>0));
	 }
	}else
	{
	 if (b>c) 
	 {
	   if ((((a+c)/b)<minratio)) // b>a,c
	   {
// 	    sta_assert_debug0(b>a);
// 	    sta_assert_debug0(b>c);	     
	   return false;
	   }
	   sta_assert_debug0(!(b-(c+a)>0));
	 }else
	 {
	   if ((((b+a)/c)<minratio)) // c>a,b
	   {
	     /*sta_assert_debug0(c>b);
	     sta_assert_debug0(c>a);*/	     
	   return false;
	   }
	   sta_assert_debug0(!(c-(b+a)>0));
	 } 
	} 
	return true;
  }
    
};


template <typename T,int Dim>
T CEdgecost<T,Dim>::side_scale=2;//std::sqrt(2.0);

template <typename T,int Dim>
T CEdgecost<T,Dim>:: _max_edge_length[max_tracking_treads]={-1};//std::sqrt(2.0);

template <typename T,int Dim>
T CEdgecost<T,Dim>:: max_edge_length_hard=-1;//std::sqrt(2.0);


template <typename T,int Dim>
class CNTrackerScaleInvariantEdgeCosts: public CEdgecost<T,Dim>
{
  
public:

    bool particle_angle_valid(const class Points< T, Dim >::CEndpoint & endpointA,
			      const class Points< T, Dim >::CEndpoint & endpointB,
				     bool debug=false)
    {
	if (AngleScale>-1)
        {
	  
	  Vector<T,Dim> n=endpointB.point->get_position()-endpointA.point->get_position();
	  n.normalize();
	  T angle_cosA=endpointA.get_slot_direction().dot(n);
 	  T angle_cosB=-endpointB.get_slot_direction().dot(n);
	  

	  if ((angle_cosA<AngleScale)||(angle_cosB<AngleScale))
	  {
	      return false;
	  }
        }
        return true;
    }  
  
  


  
  
//     T ConnectionScale;
//     T ConnectionPointWeightScale;
//     T ThicknessScale;
//     T AngleScale;

  
  protected:
  
  bool point_cone_intersect(const Vector<T,3> & x1, const Vector<T,3> & x2,T s2,const Vector<T,3> & x3)
  {
    Vector<T,3> p=x2-x1;
    Vector<T,3> & n=p;
    T l_p=std::sqrt(p.norm2())+std::numeric_limits< T >::epsilon();
    n/=l_p;
    
    Vector<T,3> v=x3-x1;
    
    T vn=(v.dot(n));
    if (vn<0)
      return false;
    
    Vector<T,3> d=n*vn;
    Vector<T,3> h=v-d;
    
    T l_d=std::sqrt(d.norm2());
    T l_h=std::sqrt(h.norm2());
    
    if (l_d<l_p)
      return false;
    
    T s3=s2*l_d/l_p;
    return (s3>l_h);
  }
  
  
  
public:
    T ConnectionScale;
    T BifurcationScale;
    T OrientationScale;
    T BifurcationArea;
    T BifurcationHScale;
    T BifurcationCScale;
    T BifurcationDScale;
//     T ConnectionPointWeightScale;
    T ThicknessScale;
    T AngleScale;
    T AreaEpsilon;
    T DirectionScale;
    T DirectionScaleThreshold;
    T InConePanelty;
    
    T BifurcationByPathID;
    
    T EdgeBonus;
    
    bool id_check;
    
    bool consider_birth_death_ratio;
    
    int point_weight;
    
    T bifurcation_edge_bonusfac;
//     T BifurcationPaneltyFact;

    
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle)
    {
      if (handle!=NULL)
      {
	  const mxArray *
	  parameter= mxGetField(handle,0,(char *)("edgecost"));
	  if (parameter!=NULL) 
	  {
		  ConnectionScale=(*((double*) mxGetPr(parameter)));
		  OrientationScale=*(((double*) mxGetPr(parameter))+1);
		  ThicknessScale=*(((double*) mxGetPr(parameter))+2);
// 		  if ((*(((double*) mxGetPr(parameter))+3))>0)
// 		    AngleScale=std::cos(M_PI*((*(((double*) mxGetPr(parameter))+3))/180.0));
// 		  else 
// 		    AngleScale=-1;	
// 		  DataScaleGrad=*(((double*) mxGetPr(parameter))+4);
	  }
	  printf("Edge: C: %f,  O %f, T %f, A %f\n",ConnectionScale,OrientationScale,ThicknessScale,AngleScale);
	  
	  parameter= mxGetField(handle,0,(char *)("bifcost"));
	  if (parameter!=NULL) 
	  {
		  BifurcationScale=(*((double*) mxGetPr(parameter)));
		  BifurcationHScale=*(((double*) mxGetPr(parameter))+1);
		  BifurcationArea=*(((double*) mxGetPr(parameter))+2);
		  BifurcationCScale=*(((double*) mxGetPr(parameter))+3);
		  BifurcationDScale=*(((double*) mxGetPr(parameter))+4);
		  
// 		  DataScaleGrad=*(((double*) mxGetPr(parameter))+4);
	  }
	  printf("Bif : B: %f,  H %f, A %f, C %f, D %f\n",BifurcationScale,BifurcationHScale,BifurcationArea,BifurcationCScale,BifurcationDScale);
// 	  printf("Th: %f,  Da %f, DaGV %f, DaG %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
	  //printf("Th: %f,  Da %f, DaGV %f\n",DataThreshold,DataScale,DataScaleGradVessel);
      }
    }
    void set_controls(const mxArray * handle)
    {
	  if (handle!=NULL)
	    {
	    const mxArray *
	    parameter= mxGetField(handle,0,(char *)("edgecost"));
	    if (parameter!=NULL) 
	    {
		  *((double*) mxGetPr(parameter))=ConnectionScale;
		  *(((double*) mxGetPr(parameter))+1)=OrientationScale;
		  *(((double*) mxGetPr(parameter))+2)=ThicknessScale;
// 		  if (AngleScale>0)
// 		    *(((double*) mxGetPr(parameter))+3)=180.0*std::acos(AngleScale)/M_PI;
// 		  else
// 		    *(((double*) mxGetPr(parameter))+3)=-1;
// 		  *(((double*) mxGetPr(parameter))+4)=DataScaleGrad;
	    }
	    
	    printf("Edge: C: %f,  O %f, T %f, A %f\n",ConnectionScale,OrientationScale,ThicknessScale,AngleScale);
	    
	    parameter= mxGetField(handle,0,(char *)("bifcost"));
	    if (parameter!=NULL) 
	    {
		  *((double*) mxGetPr(parameter))=BifurcationScale;
		  *(((double*) mxGetPr(parameter))+1)=BifurcationHScale;
		  *(((double*) mxGetPr(parameter))+2)=BifurcationArea;
		  *(((double*) mxGetPr(parameter))+3)=BifurcationCScale;
		  *(((double*) mxGetPr(parameter))+4)=BifurcationDScale;
		  
		  
	    }
	    printf("Bif : B: %f,  H %f, A %f, C %f, D %f\n",BifurcationScale,BifurcationHScale,BifurcationArea,BifurcationCScale,BifurcationDScale);
	    
	    }
    }
    #endif  
  
  
    CNTrackerScaleInvariantEdgeCosts()
    {
        ConnectionScale=1;
        ThicknessScale=1;
        AngleScale=-1;
	OrientationScale=0;
	BifurcationScale=1;
	BifurcationHScale=1;
	BifurcationArea=-1;
	BifurcationCScale=-1;
	BifurcationDScale=-1;
	AreaEpsilon=0.001;
	InConePanelty=-1;
	DirectionScale=-1;
	DirectionScaleThreshold=-1;
	EdgeBonus=1;
	BifurcationByPathID=-1;
	
	point_weight= Points<T,Dim>::point_weight;
	id_check=false;
	consider_birth_death_ratio=false;
	// 	BifurcationPaneltyFact=0.1;
	bifurcation_edge_bonusfac=1.5;

    }

    ~CNTrackerScaleInvariantEdgeCosts() {};

    T e_cost(
        const class Points< T, Dim >::CEndpoint & endpointA,
        const class Points< T, Dim >::CEndpoint & endpointB,
	const class Points< T, Dim >::CEndpoint & endpointC,
//         T max_endpoint_dist,
        typename CEdgecost<T,Dim>::EDGECOST_STATE & inrange,
	bool check_distance=true,
        bool debug=false
    ){
// 	const Vector<T,Dim> & endpoint_posA=*(endpointA.position);
//         const Vector<T,Dim> & endpoint_posB=*(endpointB.position);
// 	const Vector<T,Dim> & endpoint_posC=*(endpointC.position);
      
	const Vector<T,Dim> * endpoint_pos[3];
	endpoint_pos[0]=(endpointA.position);
	endpoint_pos[1]=(endpointB.position);
	endpoint_pos[2]=(endpointC.position);
	
	const class Points< T, Dim >::CEndpoint * endpoints[3];
	endpoints[0]=&endpointA;
	endpoints[1]=&endpointB;
	endpoints[2]=&endpointC;
	
	T endpoint_dist2[3];
	
	
	//sta_assert_error((max_endpoint_dist>0)||(CEdgecost<T,Dim>::max_edge_length<0));
	
	T max_edge_length=CEdgecost<T,Dim>::get_current_edge_length_limit();
	
        if (check_distance&&(max_edge_length>-1))
        {
	  
	  max_edge_length*=max_edge_length;
	  //max_endpoint_dist*=CEdgecost<T,Dim>:: max_edge_length;
	  //T max_endpoint_dist_dynamic=(CEdgecost<T,Dim>::max_edge_length>-1) ? CEdgecost<T,Dim>::max_edge_length : max_endpoint_dist;
	  
// 	  printf("max_endpoint_dist_dynamic %f %f \n",max_endpoint_dist_dynamic,CEdgecost<T,Dim>::max_edge_length);
	  
	  for (int i=0;i<3;i++)
	  {
	    endpoint_dist2[i]=(*endpoint_pos[i]-*endpoint_pos[(i+1)%3]).norm2();
	    T dist2=endpoint_dist2[i];
	    if (!(max_edge_length>dist2))
	    {
	      
	      if (debug)
	      {
			for (int i=0;i<3;i++)
			{
			    printf("%d: %f\n",i,(*endpoint_pos[i]-*endpoint_pos[(i+1)%3]).norm2(),max_edge_length);
			}
			printf("3point-bifurcation-cost: out of range [%d]\n",inrange);
	      }
		      if (!(CEdgecost<T,Dim>::max_edge_length_hard*CEdgecost<T,Dim>::max_edge_length_hard>dist2))
		      {
			inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE;
		      }else
			{
			  inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT;
			}
		      
		      return 1000000000000000;
	    } 
	  }

        }else
	{
	   for (int i=0;i<3;i++)
	    endpoint_dist2[i]=(*endpoint_pos[i]-*endpoint_pos[(i+1)%3]).norm2();
	}
        inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE;
	
	
	
	

	const class Points< T, Dim > * points[3];
	points[0]=endpointA.point;
	points[1]=endpointB.point;
	points[2]=endpointC.point;
	
	
	
	if (id_check)
	{
	  if ((points[0]->_path_id!=points[1]->_path_id)||(points[0]->_path_id!=points[2]->_path_id)
	      ||(points[1]->_path_id!=points[2]->_path_id))
	  {
	    inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT;
	    
	    if (debug)
		      printf("invalid ids\n");
	    
	    return 1000000000000000;
	  }
	}
	
	if (BifurcationByPathID>-1)
	{
// 	  if (points[0]->_path_id>BifurcationByPathID)
// 	    printf("WTF1 %d %f\n",points[0]->_path_id,BifurcationByPathID);
// 	  if (points[0]->_path_id<0)
// 	    printf("WTF2 %d %f\n",points[0]->_path_id,BifurcationByPathID);
// 	  if (points[0]->_path_id==BifurcationByPathID)
// 	    printf("YEA %d %f\n",points[0]->_path_id,BifurcationByPathID);
	  
	  if ((points[0]->_path_id<BifurcationByPathID)&&(points[1]->_path_id<BifurcationByPathID)
	      &&(points[2]->_path_id<BifurcationByPathID))
	  {
	    inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT;
	    
	    if (debug)
		      printf("no valid ids\n");
	    
	    return 1000000000000000;
	  }
	}
	
	
	
	if (AngleScale>-1)
        {
// 	  Vector<T,Dim> n;
	  for (int i=0;i<3;i++)
	  {
	    
		if (!particle_angle_valid(*endpoints[i],*endpoints[(i+1)%3]))
		{
		    inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_ANGLE;
		    if (debug)
		      printf("out of angle\n");
		    
		    return 1000000000000000;
		}
/*	    
	      const class Points<T,Dim> & pointA=*points[i];
	      const class Points<T,Dim> & pointB=*points[(i+1)%3];
	      const Vector<T,Dim> & cenerA=pointA.get_position();
	      const Vector<T,Dim> & cenerB=pointB.get_position();
	      n=cenerB-cenerA;    
	      n.normalize();
	      const T & signA=Points< T, Dim >::side_sign[endpoints[i]->side];
	      const T & signB=Points< T, Dim >::side_sign[endpoints[(i+1)%3]->side];
	      T angle_cosA=signA*pointA.get_direction().dot(n);
	      T angle_cosB=-signB*pointB.get_direction().dot(n);
		if ((angle_cosA<AngleScale)||(angle_cosB<AngleScale))
		{
		    inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_ANGLE;
		    if (debug)
		      printf("out of angle\n");
		    
		    return 1000000000000000;
		}*/
	  }
       }
	
	

        
        const T & a=CEdgecost<T,Dim>::side_scale*points[0]->get_scale();
        const T & b=CEdgecost<T,Dim>::side_scale*points[1]->get_scale();
	const T & c=CEdgecost<T,Dim>::side_scale*points[2]->get_scale();
	
	const T * scales[3];
	scales[0]=&a;
	scales[1]=&b;
	scales[2]=&c;
	
	
	

	
	//#define _MIN_TRIANGLE_RATIO_ T(1.1)
	#define _MIN_TRIANGLE_RATIO_ T(1.01)
	
	if ((!this->valid_triangle(a,b,c,_MIN_TRIANGLE_RATIO_))||
	   (!this->valid_triangle(FAST_SQRT(endpoint_dist2[0]),FAST_SQRT(endpoint_dist2[1]),FAST_SQRT(endpoint_dist2[2]),_MIN_TRIANGLE_RATIO_))
	)
	{
	   if (debug)
		      printf("invalid triangle\n");
	  
 	    inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_AREA;
 	    return 1000000000000000;
	}
	

	
	T costs=0;
	
	
	T w=(b*b+c*c-a*a)/(2*b*c);
	sta_assert_debug0(!(std::abs(w)>1));
	
	
	//NOTE: we cannot determine all necessary points of the triangle in 
	//3D space directly. So we determine baricentric coordinates from a 2D version
	// of the problem
	
	
	// We first construct the outer triangle (in a reference coordinate system)
	Vector<T,2> P1;
	P1[0]=c;
	P1[1]=0;
	Vector<T,2> P2;
	P2[0]=b*w;
	P2[1]=b*FAST_SQRT(1-w*w);
	
	
	// We then construct the inner triangle (has the shape of the triangle defined by the endpoints)
	Vector<T,2> P[3];
	P[0]=(P1+P2)/2; 	// mid of a
	P[1]=P2/2;		// mid of b
	P[2]=P1/2;  		// mid of c
	
	
	
	
	///TODO: note that I turned of direction check in BifurcationDScale
	/// BifurcationScale is sused!
	if (BifurcationScale>0)
	{
	  //center of circumcircle (umkreis) : Schnitpunkt der hoehensenkrechten (-> circumcenter) (nicht hohen/  altitudes )
	  Vector<T,2> C;
	  C[0]=c/2;
	  C[1]=-(-P2[0]*P2[0]-P2[1]*P2[1]+P2[0]*c)/(2*P2[1]);
	  
	  
	  // center off mass: we use it to determine direction of hight
	  Vector<T,2> center_2D=(P[0]+P[1]+P[2])/T(3);
	  
	  int sides[3];
	  sides[0]=endpointA.side;
	  sides[1]=endpointB.side;
	  sides[2]=endpointC.side;
	    
	    
	    // the intersection point of the height is the center of the outer
	    // circle of the big triangle/ we compute the center using baricentric coordinates
	    // -> we evaluate the derivation of this center and the "actual" height endpoints
	    T a2=a*a;
	    T a4=a2*a2;
	    T b2=b*b;
	    T b4=b2*b2;
	    T c2=c*c;
	    T c4=c2*c2;
	    T b2_m_c2_sqr=b2-c2;
	      b2_m_c2_sqr*=b2_m_c2_sqr;
	    T a2_m_c2_sqr=a2-c2;
	      a2_m_c2_sqr*=a2_m_c2_sqr;
	    T b2_m_a2_sqr=b2-a2;
	      b2_m_a2_sqr*=b2_m_a2_sqr;    
	    
	    
	    T w0=a4+b2_m_c2_sqr-2*a2*(b2+c2);
	    T w1=(-a4+b2_m_c2_sqr)/w0;
	    T w2=(-b4+a2_m_c2_sqr)/w0;
	    T w3=(-c4+b2_m_a2_sqr)/w0;
	    
	   Vector<T,3> center_Circle=(*endpoint_pos[0]*w1+*endpoint_pos[1]*w2+*endpoint_pos[2]*w3);
	    
	    
	    Vector<T,3> n;
	    T cost_bifurcation=0;
	    
// 	    T cost_bifurcation_test=0;

	    for (int i=0;i<3;i++)
	    {
	      Vector<T,2> n1=(P[i]-center_2D);//n1.normalize();
	      Vector<T,2> n2=(P[i]-C);//n2.normalize();
	      T l=(n1.dot(n2)>0 ? 1 : -1)*FAST_SQRT(n2.norm2());
	      
	      T scale_inv_norm=1/(*scales[i]);
	      
	      cost_bifurcation+=scale_inv_norm*scale_inv_norm*(center_Circle-(*endpoint_pos[i]+points[i]->get_direction()*Points<T,Dim>::side_sign[sides[i]]*l)).norm2();	      
	      
/*	      
	      T dist=FAST_SQRT((center_Circle-*endpoint_pos[i]).norm2())-std::abs(l);
	      cost_bifurcation_test+=scale_inv_norm*scale_inv_norm*dist*dist;	      */
	    }
	    
// 	    printf("%f %f\n",cost_bifurcation_test,cost_bifurcation);
	    costs+=BifurcationScale*cost_bifurcation; 
	   if (debug)
	     printf("cost bifurcation hight: %f\n",cost_bifurcation);
	}
	
	
	if (BifurcationHScale>0)
	{
	    T ha,hb,hc;
	    {   
	      T nominator=FAST_SQRT(-(a*a*a*a+std::pow(b*b-c*c,2.0)-2*a*a*(b*b+c*c)))/4;
 	      ha=nominator/a;
 	      //printf("Ha: %f %f\n",ha,ha_test);
	      hb=nominator/b;
 	      //printf("Hb: %f %f\n",hb,hb_test);
	      hc=nominator/c;
 	      //printf("Hc: %f %f\n",hc,hc_test);

	      
	    }
	    

	    
	    {
	      Vector<T,Dim> P1,P2;
	      P1=*endpoint_pos[2]-*endpoint_pos[1];
	      T l_2=P1.norm2();
	      P2=*endpoint_pos[0]-*endpoint_pos[1];
	      ha-=FAST_SQRT((P2-P1*((P2).dot(P1)/l_2)).norm2());
	      //Ha=FAST_SQRT((P2-P1*((P2).dot(P1)/l_2)).norm2());
	      l_2=P2.norm2();
	      hc-=FAST_SQRT((P1-P2*(P1).dot(P2)/l_2).norm2());
	      //Hc=FAST_SQRT((P1-P2*(P1).dot(P2)/l_2).norm2());
	      P2=*endpoint_pos[0]-*endpoint_pos[2];
	      l_2=P2.norm2();
	      P1*=-1;
	      hb-=FAST_SQRT((P1-P2*(P1).dot(P2)/l_2).norm2());
	      //Hb=FAST_SQRT((P1-P2*(P1).dot(P2)/l_2).norm2());
	    }
	    
	    costs+=BifurcationHScale*((ha*ha)+(hb*hb)+(hc*hc));
	    
// 	    if (debug)
// 	     printf("cost bifurcation hight: %f\n",cost_bifurcation);
	    
	}
	
	
	if (BifurcationCScale>0)
	{
	    Vector<T,3> center_3D=(*endpoint_pos[0]+*endpoint_pos[1]+*endpoint_pos[2])/T(3);
	    
	    int sides[3];
	    sides[0]=endpointA.side;
	    sides[1]=endpointB.side;
	    sides[2]=endpointC.side;
	    
	    Vector<T,3> n;
	    T cost_bifurcation=0;
	    for (int i=0;i<3;i++)
	    {
	      n=*endpoint_pos[i]-center_3D;
	      
	      
	      T l_is=FAST_SQRT(n.norm2());
	      
	      T l_opt=
 		      FAST_SQRT((((i==0)? -1 : 2)*(*scales[0])*(*scales[0])
 		     +((i==1)? -1 : 2)*(*scales[1])*(*scales[1])
 		     +((i==2)? -1 : 2)*(*scales[2])*(*scales[2])))/6;

		n/=l_is;///TODO if the term below is used, this can be removed
		
		T scale_inv_norm=1/(*scales[i]);
		
		
	      l_is=(l_is-l_opt)* scale_inv_norm;
	      cost_bifurcation+=(l_is*l_is);
	    }
	    costs+=BifurcationCScale*cost_bifurcation;
	}	
	
	
	
	
	
	
	if (BifurcationDScale>0)
	{
	    int sides[3];
	    sides[0]=endpointA.side;
	    sides[1]=endpointB.side;
	    sides[2]=endpointC.side;
	    
	    Vector<T,3> n;
	    
	    T cost_bifurcation=0;
	    for (int i=0;i<3;i++)
	    {
	      
	      //TODO optimal length is actiually half the particle scale!
	      //T l=FAST_SQRT((P[i]-P[(i+1)%3]).norm2());
	      T l=(*scales[(i+2)%3])/2;
	      
	      l-=FAST_SQRT(endpoint_dist2[i]);
		
	      T scale_inv_norm=1/FAST_SQRT((*scales[(i+1)%3])*(*scales[i]));
	      cost_bifurcation+=(l*l)*(scale_inv_norm*scale_inv_norm);
	    }

	    costs+=BifurcationDScale*cost_bifurcation;
	    if (debug)
	     printf("cost bifurcation dist: %f\n",cost_bifurcation);
	}	
	
	

	
	
	if (BifurcationArea>0)
	{
	  T arera_cost=0;
	  T a=(P[0]-P[1]).norm2();
	  T b=(P[0]-P[2]).norm2();
	  T c=(P[1]-P[2]).norm2();
	  T s=(a+b+c);
	  s*=s;
	  s-=2*(a*a+b*b+c*c);
// 	  T s=(a+b+c)/2;
	  sta_assert_debug0(s>0);
 	  T A_opt=FAST_SQRT(s/4)+AreaEpsilon;
	  s=(endpoint_dist2[0]+endpoint_dist2[1]+endpoint_dist2[2]);
	  s*=s;
	  s-=2*(endpoint_dist2[0]*endpoint_dist2[0]
	      +endpoint_dist2[1]*endpoint_dist2[1]
	      +endpoint_dist2[2]*endpoint_dist2[2]);
	  sta_assert_debug0(s>0)
	  T A_is=(FAST_SQRT(s/4))+AreaEpsilon;
	  //arera_cost=FAST_SQRT(A_is)-FAST_SQRT(A_opt);
	  //arera_cost*=arera_cost;
	  
	  arera_cost=std::max(A_is,A_opt)/(std::min(A_is,A_opt)+std::numeric_limits< T >::epsilon());
  	  arera_cost=(arera_cost*arera_cost)-1;
	  
	   if (debug)
	    {
	      printf("Area is: %f  Area opt: %f \n",A_is,A_opt);
	    }
	  
	  
	  costs+=BifurcationArea*arera_cost;
	}
	
	
	if (InConePanelty>0)
	{
	  const Vector<T,3> & posA=points[0]->get_position();
	  const Vector<T,3> & posB=points[1]->get_position();
	  const Vector<T,3> & posC=points[2]->get_position();
	  
	  const T & a=CEdgecost<T,Dim>::side_scale*points[0]->get_scale();
	  const T & b=CEdgecost<T,Dim>::side_scale*points[1]->get_scale();
	  const T & c=CEdgecost<T,Dim>::side_scale*points[2]->get_scale();
	
	  if ((point_cone_intersect(posA,posB,b,posC))
	    ||(point_cone_intersect(posA,posC,c,posB))
	    ||(point_cone_intersect(posB,posA,a,posC))
	    ||(point_cone_intersect(posB,posC,c,posA))
	    ||(point_cone_intersect(posC,posA,a,posB))
	    ||(point_cone_intersect(posC,posB,b,posA))
	  )
	  {
	  //inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT; 
	  //return 1000000000000000; 
	    costs+=InConePanelty;
	  }
	}
	
	
	if (EdgeBonus>-1)
	{
	    costs-=bifurcation_edge_bonusfac*EdgeBonus;
	}

	
	 
	    if (debug)
	    {
	      printf("edgcost :debug\n[%f %f %f]\n",
		     FAST_SQRT((*endpoint_pos[0]-*endpoint_pos[1]).norm2()),
		     FAST_SQRT((*endpoint_pos[1]-*endpoint_pos[2]).norm2()),
		     FAST_SQRT((*endpoint_pos[2]-*endpoint_pos[0]).norm2())
		    );
	      printf("[%f %f %f]\n",a/2,b/2,c/2);
	    }
	  
	  
	  
// 	 switch (point_weight) 
// 	 {
// 	   case 1: 
// 	   {
// 	     costs*=std::min(points[0]->get_scale(),std::min(points[1]->get_scale(),points[2]->get_scale()));
// 	   }break;
// 	   case 2: 
// 	   {
// 	     T power=std::min(points[0]->get_scale(),std::min(points[1]->get_scale(),points[2]->get_scale()));
// 	     costs*=power*power;
// 	   }break;
// 	   case 3: 
// 	   {
// 	     T power=std::min(points[0]->get_scale(),std::min(points[1]->get_scale(),points[2]->get_scale()));
// 	     costs*=power*power*power;
// 	   }break;  
// 	 } 

	 switch (point_weight) 
	 {
	   case 1: 
	   {
	     costs*=(points[0]->get_scale()+points[1]->get_scale()+points[2]->get_scale())/3;
	   }break;
	   case 2: 
	   {
	     T power=(points[0]->get_scale()+points[1]->get_scale()+points[2]->get_scale())/3;
	     //T power=std::min(points[0]->get_scale(),std::min(points[1]->get_scale(),points[2]->get_scale()));
	     costs*=power*power;
	   }break;
	   case 3: 
	   {
	     //T power=std::min(points[0]->get_scale(),std::min(points[1]->get_scale(),points[2]->get_scale()));
	     T power=(points[0]->get_scale()+points[1]->get_scale()+points[2]->get_scale())/3;
	     costs*=power*power*power;
	   }break;  
	 } 
	
	
	return costs;
	//return BifurcationScale*cost_bifurcation+ThicknessScale*thickness_panelty;
    }
    
    T e_cost(
        const class Points< T, Dim >::CEndpoint & endpointA,
        const class Points< T, Dim >::CEndpoint & endpointB,
        //T max_endpoint_dist,
        typename CEdgecost<T,Dim>::EDGECOST_STATE & inrange,
	bool check_distance=true,
        bool debug=false
#ifdef EDGECOST_DEBUG        
	, std::stringstream * debugstream=NULL
#endif          
    )
    {
      
        const Vector<T,Dim> & endpoint_posA=*(endpointA.position);
        const Vector<T,Dim> & endpoint_posB=*(endpointB.position);
	
	

	
	T max_edge_length=CEdgecost<T,Dim>::get_current_edge_length_limit();
	
        if (check_distance&&(max_edge_length>-1))
        {
	  
	 max_edge_length*=max_edge_length;
	
	   
	    //max_endpoint_dist*=CEdgecost<T,Dim>:: max_edge_length;

	    T dist2=(endpoint_posA-endpoint_posB).norm2();
	    
            if (!(max_edge_length>dist2))
            {
		if (debug)
		{
		   printf("2point-edge-cost: out of range [%d]\n",inrange);
		   printf("distances: !([%f] > [%f]) failed\n",max_edge_length,dist2);
		}
                
		if (!(CEdgecost<T,Dim>::max_edge_length_hard*CEdgecost<T,Dim>::max_edge_length_hard>dist2))
		{
		  inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE;
		}else
		{
		   inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT;
		}
		
                return 1000000000000000;
            }
        }
        inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE;

        const class Points< T, Dim > & pointA=*endpointA.point;
        const class Points< T, Dim > & pointB=*endpointB.point;
	
	
	if (id_check)
	{
	  if ((pointA._path_id!=pointA._path_id))
	  {
	    inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT;
	    
	    if (debug)
		      printf("invalid ids\n");
	    
	    return 1000000000000000;
	  }
	}
	
	
// 	sta_assert_debug0(*endpointA.endpoint_connections<2);
// 	sta_assert_debug0(*endpointB.endpoint_connections<2);
	
        const T & lengthA=pointA.get_scale();
        const T & lengthB=pointB.get_scale();
        const Vector<T,Dim> & cenerA=pointA.get_position();
        const Vector<T,Dim> & cenerB=pointB.get_position();



        T u=0;
	
	
	
	
        T dist_cost;
	T orient_cost=0;
	
	T lA=pointA.get_thickness();
        T lB=pointB.get_thickness();
	
	T wa=lB/(lA+lB);
	T wb=lA/(lA+lB);
	Vector<T,3> center;
	center=cenerA*wa+cenerB*wb;

	//distance costs:
	wa=lengthA*lengthA+std::numeric_limits<T>::epsilon();
	wb=lengthB*lengthB+std::numeric_limits<T>::epsilon();;

	
/*	
	T center[3];
	const T * cA=cenerA.v;
	const T * cB=cenerB.v;	
	T sign=(Points<T,Dim>::thickness<0) ? -1 : 1 ; 
	
	T lA=sign*Points<T,Dim>::thickness*lengthA;
        T lB=sign*Points<T,Dim>::thickness*lengthB;
	if (Points<T,Dim>::min_thickness>0)
	{
	    lA=std::max(Points<T,Dim>::min_thickness,lA);
	    lB=std::max(Points<T,Dim>::min_thickness,lB);
	}
	T wa=lB/(lA+lB);
	T wb=lA/(lA+lB);
	center[0]=(wa*(*cA++)+wb*(*cB++));
	center[1]=(wa*(*cA++)+wb*(*cB++));
	center[2]=(wa*(*cA)+wb*(*cB));

	//distance costs:
	wa=lengthA*lengthA+std::numeric_limits<T>::epsilon();
	wb=lengthB*lengthB+std::numeric_limits<T>::epsilon();;
	
	
	
*/	


	


	if (EdgeBonus>-1)
	{
	    u-=EdgeBonus;
	    #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"EdgeBonus : "<<u<<"\n";
	      }
	    #endif          
	}

	if (OrientationScale>0)
	{
	  T distA=FAST_SQRT((cenerA-center).norm2());
	  T distB=FAST_SQRT((cenerB-center).norm2());
	  
	  //curvature costs:
	  const T & signA=Points< T, Dim >::side_sign[endpointA.side];
	  const T & signB=Points< T, Dim >::side_sign[endpointB.side];
 	  orient_cost=((cenerA+(pointA.get_direction()*(signA*distA)))-center).norm2()/wa;
 	  orient_cost+=((cenerB+(pointB.get_direction()*(signB*distB)))-center).norm2()/wb;
	  
	  distA-=lA;
	  distB-=lB;
	  dist_cost=(distA*distA)/wa+(distB*distB)/wb;
	  u+=OrientationScale*orient_cost;
	  if (ConnectionScale>0)
	  {
	    u+=ConnectionScale*dist_cost;
	  }
	  
	    #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(OrientationScale>0) : "<<u<<"\n";
	      }
	    #endif    
	  
	}else
	{
	  if (OrientationScale<0)
	  {
	      Vector<T,3> dA=(endpoint_posA-center);
	      Vector<T,3> dB=(endpoint_posB-center);
	      
	      Vector<T,3> ncenter=cenerB-cenerA;
	      ncenter.normalize();
	      
	      T dist_termA=-ncenter.dot(dA);
	      T dist_termB=ncenter.dot(dB);
	      
// 	      T o_termA=std::sqrt((dA+ncenter*dist_termA).norm2());
// 	      T o_termB=std::sqrt((dB-ncenter*dist_termB).norm2());
	      T o_termA=((dA+ncenter*dist_termA).norm2());
	      T o_termB=((dB-ncenter*dist_termB).norm2());
	      
	      
// 	      sta_assert_debug0(!(dist_termA<0));
// 	      sta_assert_debug0(!(dist_termB<0));
	      
	      
	      T dist2=(dist_termA*dist_termA/wa+dist_termB*dist_termB/wb);
	      
	      
	      //u-=(OrientationScale)*dist2*(o_termA/wa+o_termB/wb);
	      u-=(OrientationScale)*(o_termA/wa+o_termB/wb);

	    #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(OrientationScale<0 O) : "<<u<<"\n";
	      }
	    #endif   
	      
	      
	      if (ConnectionScale>0)
	      {
		u+=(ConnectionScale)*dist2;
	      }
	      
	    #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(OrientationScale<0 D) : "<<u<<"\n";
	      }
	    #endif   
	  }else //OrientationScale==0
	    {
	      T distA=(endpoint_posA-center).norm2();
	      T distB=(endpoint_posB-center).norm2();
	      dist_cost=distA/wa+distB/wb;	  
	      if (ConnectionScale>0)
	      {
		u+=ConnectionScale*dist_cost;
	      }
	      
	      #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(OrientationScale==0) : "<<u<<"\n";
	      }
	    #endif   
	  }
	}
	
	
	
	
	
	
	if (DirectionScale>0)
	{
	  T score=(endpointA.get_slot_direction()).dot(endpointB.get_slot_direction());
	  if (!(score<DirectionScaleThreshold))
	  {   
	    T weight=(pointA.get_position()-pointB.get_position()).norm2()/(wa*wb);
	    u+=DirectionScale*weight*(score-DirectionScaleThreshold);	
	    #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(DirectionScale) : "<<u<<"  "<<score<<"  "<<std::acos(score)<<"\n";
	      }
	    #endif      
	  }
	   #ifdef EDGECOST_DEBUG        
	   else{
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"((score<DirectionScaleThreshold)) : "<<u"\n";
	      }
	   }
	    #endif   
	}else if (DirectionScale<0)
	{
	  T score=(endpointA.get_slot_direction()).dot(endpointB.get_slot_direction());
	  if (!(score<DirectionScaleThreshold))
	  {   
	    u-=DirectionScale*(score-DirectionScaleThreshold);	
	    #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(DirectionScale) : "<<u<<"  "<<score<<"  "<<std::acos(score)<<"\n";
	      }
	    #endif      
	  }
	   #ifdef EDGECOST_DEBUG        
	   else{
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"((score<DirectionScaleThreshold)) : "<<u"\n";
	      }
	   }
	    #endif   
	}
	
	
	
// 	dist_cost=(endpoint_posA-endpoint_posB).norm2();
	
	

        if (debug)
        {
            printf("lengthA %f, length B %f\n",lengthA,lengthB);
            printf("thickness :%f\n",(std::pow(std::max(lengthA,lengthB)/std::min(lengthA,lengthB),T(2))-1)) ;
            printf("dist      :%f\n",dist_cost) ;
        }

        
        

        if (ThicknessScale>0)
	{
	    T thickness_panelty=std::max(lengthA,lengthB)/std::min(lengthA,lengthB);
	    //thickness_panelty=std::abs(thickness_panelty)-1;
	    thickness_panelty=(thickness_panelty*thickness_panelty)-1;
	    
	    u+=ThicknessScale*thickness_panelty;
	    
	     #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(ThicknessScale<0) : "<<u<<"\n";
	      }
	    #endif   
	}
        
        //u=OrientationScale*orient_cost+ConnectionScale*dist_cost+ThicknessScale*thickness_panelty;
	
	
	
	
	if (AngleScale>-1)
        {
	  
	  if (!particle_angle_valid(endpointA,endpointB))
	  {
	      inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_ANGLE;
	      if (debug)
		printf("out of angle\n");
	      
	      
	      #ifdef EDGECOST_DEBUG        
	      if (debugstream!=NULL)
	      {
		  (*debugstream)<<"(Angle invalid!! ) \n";
	      }
	    #endif   
	      
	      return 1000000000000000;
	  }
	  
// 	  Vector<T,Dim> n=cenerB-cenerA;
// 	  n.normalize();
// 	  const T & signA=Points< T, Dim >::side_sign[endpointA.side];
// 	  const T & signB=Points< T, Dim >::side_sign[endpointB.side];
// 	  T angle_cosA=signA*pointA.get_direction().dot(n);
// 	  T angle_cosB=-signB*pointB.get_direction().dot(n);
//             if ((angle_cosA<AngleScale)||(angle_cosB<AngleScale))
//             {
//                 inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_ANGLE;
// 		if (debug)
// 		  printf("out of angle\n");
// 		
//                 return 1000000000000000;
//             }

// 	  //x*y - sqrt((1-x*x)*(1-y*y)) //=acos(x)+acos(y)
// 	  T acosAplusB=angle_cosA*angle_cosB-FAST_SQRT((1-angle_cosA*angle_cosA)*(1-angle_cosB*angle_cosB));
//             if (acosAplusB<AngleScale)
//             {
//                 inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_ANGLE;
//                 return 1000000000000000;
//             }
	  
//             T angle_cos=std::abs(pointA.direction.dot(pointB.direction));
//             if (angle_cos<AngleScale)
//             {
//                 inrange = CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_ANGLE;
//                 return 1000000000000000;
//             }
        }

        
        if (debug)
	  printf("successfully computed the 2point-edge-cost [%d]\n",inrange);
	
	
	switch (point_weight) 
	 {
	   case 1: 
	   {
	     u*=(lengthA+lengthB)/2;
	   }break;
	   case 2: 
	   {
	     u*=(lengthA*lengthA+lengthB*lengthB)/2;
	   }break;
	   case 3: 
	   {
	     u*=(lengthA*lengthA*lengthA+lengthB*lengthB*lengthB)/2;
	   }break;  
	 }
	
	
        return u;

    }

    void set_params(const mxArray * params=NULL)
    {
      try {

	CEdgecost<T,Dim>::set_params(params);
	
        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"ConnectionScale")!=-1)
                ConnectionScale=mhs::mex_getParam<T>(params,"ConnectionScale",1)[0];

	    if (mhs::mex_hasParam(params,"BifurcationScale")!=-1)
                BifurcationScale=mhs::mex_getParam<T>(params,"BifurcationScale",1)[0];
	    
	    if (mhs::mex_hasParam(params,"BifurcationHScale")!=-1)
                BifurcationHScale=mhs::mex_getParam<T>(params,"BifurcationHScale",1)[0];

	    if (mhs::mex_hasParam(params,"BifurcationCScale")!=-1)
                BifurcationCScale=mhs::mex_getParam<T>(params,"BifurcationCScale",1)[0];
	    
	    if (mhs::mex_hasParam(params,"BifurcationDScale")!=-1)
                BifurcationDScale=mhs::mex_getParam<T>(params,"BifurcationDScale",1)[0];
	    
	    if (mhs::mex_hasParam(params,"BifurcationByPathID")!=-1)
                BifurcationByPathID=mhs::mex_getParam<T>(params,"BifurcationByPathID",1)[0];
	    
	   
//             if (mhs::mex_hasParam(params,"ConnectionPointWeightScale")!=-1)
//                 ConnectionPointWeightScale=mhs::mex_getParam<T>(params,"ConnectionPointWeightScale",1)[0];

            if (mhs::mex_hasParam(params,"ThicknessScale")!=-1)
                ThicknessScale=mhs::mex_getParam<T>(params,"ThicknessScale",1)[0];
	    
	    if (mhs::mex_hasParam(params,"BifurcationArea")!=-1)
                BifurcationArea=mhs::mex_getParam<T>(params,"BifurcationArea",1)[0];
	    

            if (mhs::mex_hasParam(params,"AngleScale")!=-1)
	    {
                AngleScale=mhs::mex_getParam<T>(params,"AngleScale",1)[0];
		if (AngleScale>-1)
		AngleScale=(std::cos(AngleScale));
		//AngleScale=std::abs(std::cos(AngleScale));
	    }
// 	    printf("AngleScale %f\n",AngleScale);
                if ( mhs::mex_hasParam ( params,"bifurcation_edge_bonusfac" ) !=-1 ) {
                bifurcation_edge_bonusfac=mhs::mex_getParam<T> ( params,"bifurcation_edge_bonusfac",1 ) [0];
            } 

	    
	    if (mhs::mex_hasParam(params,"OrientationScale")!=-1)
                OrientationScale=mhs::mex_getParam<T>(params,"OrientationScale",1)[0];
	    
	     if (mhs::mex_hasParam(params,"particle_point_weight_exponent")!=-1)
                point_weight=mhs::mex_getParam<int>(params,"particle_point_weight_exponent",1)[0];
	    
	     
	     if (mhs::mex_hasParam(params,"InConePanelty")!=-1)
                InConePanelty=mhs::mex_getParam<T>(params,"InConePanelty",1)[0];
	     
	     if (mhs::mex_hasParam(params,"DirectionScale")!=-1)
                DirectionScale=mhs::mex_getParam<T>(params,"DirectionScale",1)[0];
	     
	     if (mhs::mex_hasParam(params,"DirectionScaleThreshold")!=-1)
                DirectionScaleThreshold=mhs::mex_getParam<T>(params,"DirectionScaleThreshold",1)[0];
	     
	       if (mhs::mex_hasParam(params,"EdgeBonus")!=-1)
                EdgeBonus=mhs::mex_getParam<T>(params,"EdgeBonus",1)[0];
	       
	       
	        if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];
	     
	     
	     
// 	    if (mhs::mex_hasParam(params,"BifurcationPaneltyFact")!=-1)
//                 BifurcationPaneltyFact=mhs::mex_getParam<T>(params,"BifurcationPaneltyFact",1)[0];

	    
        }
      }
        catch (mhs::STAError error)
        {
	  throw error;
        }
        
        
        printf("-------EDCOSTS_SEGMENT-----\n");
	printf("AngleScale 	:  %d    (%f)\n",(AngleScale>-1),AngleScale);
	printf("ThicknessScale	:  %d    (%f)\n",(ThicknessScale>0),ThicknessScale);
	if (DirectionScale>0)
	{
	  printf("DirectionScale D:  %d    (%f)\n",(DirectionScale>0),DirectionScale);
	}else if  (DirectionScale<0)
	{
	  printf("DirectionScale C:  %d    (%f)\n",(DirectionScale<0),-DirectionScale);
	}
	printf("EdgeBonus	:  %d    (%f)\n",(EdgeBonus>-1),EdgeBonus);
	      if (OrientationScale>0)
	      {
		if (ConnectionScale>0)
		{
		  
		  printf("Connection new1	:    (c:%f,o:%f)\n",ConnectionScale,OrientationScale);
		}else
		{
		  printf("Connection new1	:    (c:-,o:%f)\n",OrientationScale);
		}

	      }else
	      {
		if (OrientationScale<0)
		{
		   
		   if (ConnectionScale>0)
		   {
		     printf("Connection new2	:    (c:%f,o:%f)\n",ConnectionScale,-OrientationScale);
		   }else
		   {
		     printf("Connection new2	:    (c:-,o:%f)\n",-OrientationScale);
		   }
		}else
		{
		   if (ConnectionScale>0)
		   {
		     printf("Connection old	:    (c:-,o:%f)\n",ConnectionScale);
		   }else
		   {
		     printf("Connection ---	:   \n");
		   }
		}
	      }	
	printf("-------EDCOSTS_BIFURCA-----\n");	      
	printf("BifurcationScale 	:  %d    (%f)\n",(BifurcationScale>-1),BifurcationScale);      
	printf("BifurcationHScale 	:  %d    (%f)\n",(BifurcationHScale>-1),BifurcationHScale);      
	printf("BifurcationCScale 	:  %d    (%f)\n",(BifurcationCScale>-1),BifurcationCScale);      
	printf("BifurcationDScale 	:  %d    (%f)\n",(BifurcationDScale>-1),BifurcationDScale);      
	printf("BifurcationArea 	:  %d    (%f)\n",(BifurcationArea>-1),BifurcationArea);      
	printf("InConePanelty 	:  %d    (%f)\n",(InConePanelty>-1),InConePanelty);      
	printf("bifurcation_edge_bonusfac 	:  %f\n",bifurcation_edge_bonusfac);      
	printf("BifurcationByPathID 	:  %f\n",BifurcationByPathID);     
	
	
	
	
	printf("-------###############-----\n");
        
              
	 
	    
	    
	    
	    
	    
	    
	  
	    
	      
        
        
        
// 	printf("point weight scale: %f\n",ConnectionPointWeightScale);

    }
};




#endif





