function  FeatureData=create_DTI_features(bTensor,Mask,signal)    

%%
if 0
    %%
 
proposals={...
    'BIRTH',[1,1],...    
    'DEATH',[1,1],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',1,...    
    'MOVE',1,...
    'SCALE',0,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0,...
    'B_DEATHS',0,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    };  

    particle_min_thickness=1/50;
    particle_thickness=-2;
    %particle_thickness=-0.5;
    opt_particles_per_voxel=100;%2*(1/FeatureData.scales(1))^3;
    
    AngleScale=-1;
    ThicknessScale=-1;
    
     DataScale=5; 
     Template_Similarity=1;
     %particle_connection_state_cost_scale=Template_Similarity*DataScale*[1.0,0.9,0.8,0.9,0.8]; 
     %particle_connection_state_cost_scale=[1.0,1.0,1.0,1.0,1.0]; 
     particle_connection_state_cost_scale=[1.0,0.8,0.5,0.8,0.5]; 
     
     lambda=numel(FeatureData.img(:));
     
      opt_temp=5;
     opt_alpha=0.999;
     
     
     bL=0;
        %L=10;
        L=0;
        
        
     particle_point_weight_exponent=0;
      particle_connection_state_cost_offset=1;
      
       ConnectionScale=5;
     OrientationScale=5;
     
      %connection_candidate_searchrad=0.25;
      connection_candidate_searchrad=0.5;
      search_rad_scale=2*pi;
      custom_grid_spacing=0.5;
    
      
      
      no_collision_threshold=-2;
      
    ConnectEpsilon=0.01;
     Epsilon=0.0001;
     temp_dependent=false;
     
     
     
      GlobalDataThreshold=0;
      particle_endpoint_offset=0;
      
      
      soft_collision_cost_scale=100;
      collision_fun_id=1;
      
    options={...
            'custom_grid_spacing',custom_grid_spacing,...
            'search_rad_scale',search_rad_scale,...
            'soft_collision_cost_scale',soft_collision_cost_scale,...
            'collision_fun_id',collision_fun_id,...
            'no_collision_threshold',no_collision_threshold,...
            'ConnectEpsilon',ConnectEpsilon,...
            'Epsilon',Epsilon,...
            'temp_dependent',temp_dependent,...
            'particle_collision_two_steps',0,...
            'opt_numiterations',1000000,...
            'opt_alpha',opt_alpha,...%0.99995,...
            'DataThreshold',GlobalDataThreshold,...%-25,...
            'DataScale',DataScale,...
            'particle_endpoint_offset',particle_endpoint_offset,...
            'particle_connection_state_cost_scale',particle_connection_state_cost_scale,...
            'particle_connection_state_cost_offset',particle_connection_state_cost_offset*[1,1,1,1,1],...-pL*[0,0.25,1],
            'particle_thickness',particle_thickness,...-1,...-1/4,...
            'particle_min_thickness',particle_min_thickness,...
            'connection_candidate_searchrad',connection_candidate_searchrad,...%f15,...%8,...
            'connection_bonus_L',-L,...
            'ConnectionScale',ConnectionScale,...
            'AngleScale',AngleScale,...$deg2rad(60),...
            'ThicknessScale',ThicknessScale,...%,1,...
            'OrientationScale',OrientationScale,...%,1,.....
            'opt_temp',opt_temp,...
            'constraint_loop_depth',25,...
            'scale_power',particle_point_weight_exponent,...
            'particle_point_weight_exponent',particle_point_weight_exponent,...    
            'particle_thickness',particle_thickness,...-1,...-1/4,...
            'particle_min_thickness',particle_min_thickness,...
            'use_saliency_map',true,...
            'proposals',proposals,...
            'opt_particles_per_voxel',opt_particles_per_voxel,...
            %'lambda',lambda,...
            };
        

    
end
%%
       [pathstr, name, ext]=fileparts([mfilename('fullpath'),'.m']);
       sphereInterpolation=load([pathstr,'/sphere_interp.mat']);	
       
       
        
        
       b0indicator = squeeze(squeeze(bTensor(1,1,:)+bTensor(2,2,:)+bTensor(3,3,:)));
       b0indicator = b0indicator/max(b0indicator);
       b0idx = find(b0indicator < 0.101);
       datastruct.b0avg = single(sum(signal(:,:,:,b0idx),4) /length(b0idx));

       %reportstatus('preparing signal');
       didx = setdiff(1:size(bTensor,3),b0idx);
       datastruct.signal = single(zeros(length(didx),size(signal,1),size(signal,2),size(signal,3)));
       if isempty(b0idx),
            datastruct.signal = single(signal);
            datastruct.b0avg = single(ones(size(signal,2),size(signal,3),size(signal,4)));
       else
           for k = 1:length(didx),
                datastruct.signal(k,:,:,:) = single((signal(:,:,:,didx(k)) ./(datastruct.b0avg+0.001))); 
           end;
       end;
       
       %datastruct.original_signal = single(signal);
       %datastruct.original_bTensor = bTensor;

       
     %  datastruct.signal(datastruct.signal>1) = 1;
           
       %reportstatus('correlating with model');          
       frt = FunkRadonTrans(bTensor(:,:,didx),sphereInterpolation.bDir,40,0.0002);      
     %  frt = FunkRadonTrans(bTensor(:,:,didx),sphereInterpolation.bDir,3,0.000);      
       frt = single(frt);
       sz = size(datastruct.signal);
       sz(1) = size(frt,1);
       
       
       
       
       FeatureData.signal = single(reshape(frt*datastruct.signal(:,:),sz));
       FeatureData.sinterpstruct=sphereInterpolation;
       
       FeatureData.saliency_map=(Mask);
       FeatureData.img=single(FeatureData.saliency_map);
       FeatureData.saliency_map=single(FeatureData.saliency_map./sum(FeatureData.saliency_map(:)));
       
       
       cshape=size(Mask);
       FeatureData.cshape=single(cshape);
       FeatureData.datafunc=single(100);
       Scales=single([0.2,0.21]);
       %Scales=single([0.1,0.1000001]);
       FeatureData.scales=single(Scales);
       
       FeatureData.shape_org=FeatureData.cshape;
       FeatureData.shape_new_offset=[0,0,0];
       
      function [odf odfmean] = FunkRadonTrans(pos,posout,L,alpha)

            N = size(pos,3);
            SH = cpmSH(pos,L);
            SH = vertcat(SH{:});
            SHout = cpmSH(posout,L);
            SHout = vertcat(SHout{:});

            for k = 1:2:L
                regu{k} = ones(2*k-1,1)*(k-1)*k;
                Leg = legendre(k-1,0,'norm');
                funkradon{k} =ones(2*k-1,1)*Leg(1)*3^(k-1)/factorial(k-1);         
            end;

            funkradon{1} = 0;

            regu = vertcat(regu{:});
            funkradon = vertcat(funkradon{:});

            odfsh = inv(SH*SH'+alpha*N*diag(regu))*SH;

            FRT = SHout'*diag(funkradon)*odfsh;

            odf = real(FRT);
            odfmean = real(odfsh(1,:,:,:));



            return




function SH = cpmSH(dirin,L)

if size(dirin,3) > 1,
    for k = 1:size(dirin,3),
        [V D] = eigs(dirin(:,:,k));
        dir(:,k) = V(:,3);
    end;
else
    dir = dirin;
end;

N = size(dir,2);   
costheta = (dir(3,:));
xiy = dir(1,:) + i*dir(2,:);
xiy = xiy ./(abs(xiy)+0.0000001);
for k = 1:2:L,
    LegP = legendre(k-1,costheta,'norm');
    for m = -(k-1):(k-1),
        if m < 0 
             SH{k}(m+k,:) = LegP(abs(m)+1,:) .* conj(xiy).^(abs(m)) * (-1)^m;
        else
             SH{k}(m+k,:) = LegP(abs(m)+1,:) .* xiy.^(abs(m));
        end;
    end;  
end;
