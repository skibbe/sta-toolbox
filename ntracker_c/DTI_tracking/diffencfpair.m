function X = opts2(n,finame)

[sx,sy,sz] = sphere(5);
s = 0.1;




X = randn(3,n);
R2 = (sum(power(X,2),1));
X = X./ (ones(3,1)*sqrt(R2));

alpha = 0.05;
for k = 1:100,
    Dmat(1,:,:) = ones(n,1)*X(1,:) - (ones(n,1)*X(1,:))';
    Dmat(2,:,:) = ones(n,1)*X(2,:) - (ones(n,1)*X(2,:))';
    Dmat(3,:,:) = ones(n,1)*X(3,:) - (ones(n,1)*X(3,:))';
    
    Dmat2(1,:,:) = ones(n,1)*X(1,:) + (ones(n,1)*X(1,:))';
    Dmat2(2,:,:) = ones(n,1)*X(2,:) + (ones(n,1)*X(2,:))';
    Dmat2(3,:,:) = ones(n,1)*X(3,:) + (ones(n,1)*X(3,:))';
    
    
    delta = -sum(Dmat./repmat(sum(power(Dmat,2))+permute(eye(n),[3 1 2]),[3 1 1]),3);
    delta = delta  + sum(Dmat2./repmat(sum(power(Dmat2,2)),[3 1 1]),3);
    
    X = X + alpha*delta;
    R = sqrt(sum(power(X,2),1));
    X = X./ (ones(3,1)*R);
    if false,%mod(k,10)==0,
        plot3(X(1,:),X(2,:),X(3,:),'*');  hold on;
        plot3(-X(1,:),-X(2,:),-X(3,:),'*r'); 
        surf(sx*s,sy*s,sz*s);
        axis equal; axis off; axis vis3d; hold off;
        drawnow;
%        [c so] = compCond(X,2);
%        fprintf('%f %i\n',c,so);
    end;
end;
% 
% fi = fopen(finame,'w');
% for k = 1:size(X,2),
%     fprintf(fi,'%f %f %f\n',X(1,k),X(2,k),X(3,k));
% end;
% fclose(fi);
% 



function [c l] = compCond(X,L)

Y(1:2,:) = X(1:2,:)/sqrt(2);
Y(3,:) = X(3,:);
Y(4,:) = 0;

Y = single(Y);
Y = STmult(Y,Y,2);
Yk = Y;

W = Y(1:end-1,:);
normY = sqrt(sum(power(W(1:end-1,:),2))*2 + sum(power(W(end,:),2)));
W = W ./ (ones(size(W,1),1)*normY);
Z = W;
for k = 1:(L-1),
    Yk = STmult(Y,Yk,2*(k+1));
    W = Yk(1:end-1,:);
    normY = sqrt(sum(power(W(1:end-1,:),2))*2 + sum(power(W(end,:),2)));
    W = W ./ (ones(size(W,1),1)*normY);
    Z = [Z; W];
end;
C = Z*Z';
c = (cond(C));
l = size(C,1);
return
