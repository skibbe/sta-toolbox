function stackviewHARDI(mr,varargin)

bgnd = [];
mask = [];
initpos = [];
meanva = 0;
scale = 0;
gamma = 1;
sym = false;
B = [];

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


if not(isfield(mr.user,'bDir'))
   for k=1:size(mr.user.bTensor,3)
       [v,d]= eigs(mr.user.bTensor(:,:,k));
       mr.user.bDir(:,k)=v(:,3)*d(3,3)/1000;
       
   end;
    mr.user.bDir = cat(2,mr.user.bDir,-mr.user.bDir);
    mr.dataAy = cat(4,mr.dataAy,mr.dataAy);
end;


   %else
 bDir = mr.user.bDir;    





sig = permute(mr.dataAy,[4 1 2 3]);

if not(isempty(B)),
    bval = round((mr.user.bTensor(1,1,:)+mr.user.bTensor(2,2,:)+mr.user.bTensor(3,3,:))/10)*10;
    sidx = bval==B;
    bDir = bDir(:,sidx);
    sig = sig(sidx,:,:,:);
end;

if isempty(bgnd),
    bgnd = squeeze(mean(sig));
elseif isstr(bgnd),
    if strcmpi(bgnd,'gfa'),
        bgnd = sqrt(squeeze(var(sig)./sum(sig.^2)*size(sig,1)));
        %bgnd = squeeze(var(sig)./mean(sig));
    end;    
end;

if isempty(mask),
    if isfield(mr.user,'mask')
        mask = mr.user.mask;
    else
        mask = sum(abs(bgnd),4)>0;
    end;
end;

if isfield(mr.user,'sym'),
    sym = mr.user.sym;
end;


if isempty(initpos),
    stackviewODFdirect(sig,bDir,sym,bgnd,mask,[meanva scale gamma]);
else
    stackviewODFdirect(sig,bDir,sym,bgnd,mask,[meanva scale gamma],initpos);
end;