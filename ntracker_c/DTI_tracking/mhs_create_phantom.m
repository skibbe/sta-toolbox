function phantom=mhs_create_phantom(noise)

if nargin<1
    noise=0.01;
end;

[pathstr, name, ext]=fileparts([mfilename('fullpath'),'.m']);
%
if exist([pathstr,'/phantom_dirs.mat'],'file')
    load([pathstr,'/phantom_dirs.mat']);
else
    for k = 6:130, dirs{k} = diffencfpair(k); fprintf('%i,',k); end;
    save([pathstr,'/phantom_dirs.mat']);
end;


%%
clear ten

b1 = 2;
b2 = 1;
b3=3;
nd = 90;
d = [[0 0 0]'  dirs{nd}*sqrt(b1) ]; %dirs{nd}*sqrt(b2) dirs{nd}*sqrt(b3)];% dirs{nd}*sqrt(b2)]; %dirs{nd}*sqrt(b3)];
for k = 1:size(d,2),
    ten(:,:,k) = d(:,k)*d(:,k)';
end;
ten = ten*1000;

hr.user.bTensor = ten;
[dataAy themask] = genPhan(ten/1000,noise);


phantom.Dir=ten;
phantom.Data=dataAy;
phantom.Mask=themask;

