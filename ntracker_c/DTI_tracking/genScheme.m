%%
for k = 6:130, dirs{k} = diffencfpair(k); fprintf('%i,',k); end;
%%
clear ten

b1 = 2;
b2 = 1;
b3=3;
nd = 30;
d = [[0 0 0]'  dirs{nd}*sqrt(b1) ]; %dirs{nd}*sqrt(b2) dirs{nd}*sqrt(b3)];% dirs{nd}*sqrt(b2)]; %dirs{nd}*sqrt(b3)];
for k = 1:size(d,2),
    ten(:,:,k) = d(:,k)*d(:,k)';
end;
ten = ten*1000;

hr.user.bTensor = ten;
[dataAy themask] = genPhan(ten/1000,0.01);

%%

fid = fopen('siemens.txt','w');

for k = 10:length(dirs),
    fprintf(fid,'[directions=%i]\r\n',size(dirs{k},2));
    fprintf(fid,'CoordinateSystem = xyz\r\n');
    fprintf(fid,'Normalisation = none\r\r\n\n');
    for j = 1:size(dirs{k},2),
        fprintf(fid,'%8.3f, %8.3f, %8.3f \r\n',dirs{k}(1,j),dirs{k}(2,j),dirs{k}(3,j));
    end;
    fprintf(fid,'\n\n\r\r');
end;

fclose(fid);

% 
% 
% # DELTA = 0.038, delta = 0.008, b ~ 711	
% [directions=179]	
% CoordinateSystem = xyz	
% Normalisation = none
% 
% Vector [	0	]	=	(	1,	0,	0	)
% Vector [	1	]	=	(	1,	0,	0	)
% Vector [	2	]	=	(	1,	0,	0	)
% Vector [	3	]	=	(	1,	0,	0	)
% Vector [	4	]	=	(	1,	0,	0	)
% Vector [	5	]	=	(	1,	0,	0	)
% Vector [	6	]	=	(	1,	0,	0	)
% Vector [	7	]	=	(	1,	0,	0	)
% Vector [	8	]	=	(	1,	0,	0	)
% Vector [	9	]	=	(	1,	0,	0	)
% Vector [	10	]	=	(	1,	0,	0	)
% Vector [	11	]	=	(	1,	0,	0	)
% Vector [	12	]	=	(	1,	0,	0	)
% Vector [	13	]	=	(	1,	0,	0	)
% Vector [	14	]	=	(	1,	0,	0	)
% Vector [	15	]	=	(	1,	0,	0	)
% Vector [	16	]	=	(	1,	0,	0	)
% Vector [	17	]	=	(	1,	0,	0	)
% Vector [	18	]	=	(	1,	0,	0	)
% Vector [	19	]	=	(	1,	0,	0	)