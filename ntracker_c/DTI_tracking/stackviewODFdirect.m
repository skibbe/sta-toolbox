%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  data  - data array with size [numdirs w h d]
%  dirs - dirs of size [3 numdirs]
%  back - background to be displayed of size [w h d]
%  mask - mask of sz [w h d]
%  params - [mean scaling gamma] (see showODF below)
%  init (optional) - two component vector 
%                 init(1) = 1,2 or 3 (which cutplane to start with x,y,z ~ 1,2,3)
%                 init(2) = which slice to start with
%



function stackviewODFdirect(data,dirs,sym,back,mask,params,init)


ds.signal = data;
ds.dirs = dirs;
ds.back = back;
ds.mask = mask;
ds.sym = sym;

%updirs = load('dirs1024.mat');
%dirout = updirs.dirs1024;
%ds.upsampmat = Upsamp(dirs,dirout,18);
%ds.upsampdir = dirout;



%f = figure('Position',[360,500,750,585]);    
f = gcf;
clf;
set(f,'color',[0 0 0])
% the following is valid while zoom is active
%hManager = uigetmodemanager(f);
%set(hManager.WindowListenerHandles,'Enable','off');
zoom off;



set(f,'WindowButtonDownFcn',{@mousepressed_Callback,ds});
set(f,'ToolBar','figure');

h = uicontrol('Style','slider','String','Surf','Position',[0,0,300,25],'Tag','theslider','Value',0.5,'Callback',{@resolutionslider_Callback,gcbo,[],[],ds,params});
botPanel = uibuttongroup('Units','pixels','Position',[400 0  200 30],'SelectionChangeFcn',{@radiob_Callback,gcbo,ds},'Tag','butgroup');
hX = uicontrol('Style','radiobutton','String','X','Position',[10,1,40,25],'Tag','1','Parent',botPanel);
hY = uicontrol('Style','radiobutton','String','Y','Position',[60,1,40,25],'Tag','2','Parent',botPanel);
hZ = uicontrol('Style','radiobutton','String','Z','Position',[110,1,40,25],'Tag','3','Parent',botPanel);

button = uicontrol('Style','pushbutton','String','nice','Position',[630,1,40,25],'Tag','nicebutton','Callback',{@nicebutton_Callback,gcbo,[],[],ds,params});


coord = uicontrol('Style','text','String','x:1','Tag','coordinfo','Position',[300,0,100,25]);

if exist('init','var'),
    if init(1) == 1,
        set(botPanel,'SelectedObject',hX);
        radiob_Callback(botPanel,[],[],ds);
    elseif init(1) == 2,
        set(botPanel,'SelectedObject',hY);
        radiob_Callback(botPanel,[],[],ds);
        
    elseif init(1) == 3,
        set(botPanel,'SelectedObject',hZ);
        radiob_Callback(botPanel,[],[],ds);

    end;
    if length(init) > 1,
        set(h,'Value',init(2));
    end;
else
    set(botPanel,'SelectedObject',hX);
    radiob_Callback(botPanel,[],[],ds);  
end;

showslice(ds,params);

% --------------------------------------------------------------------
function varargout = mousepressed_Callback(h, eventdata,varargin)
    ds = varargin{1};
%    sz = size(img);
    p = round(get(gca,'CurrentPoint')); p = p(1,1:2);
    hc = findobj(gcf,'Tag','haircross');
    delete(hc);
    rectangle('Position',[p-0.5 1 1],'Tag','haircross','edgecolor',[1 1 0]);

    showTS(p,ds);
    
function showTS(p,ds)
    f = gcf;
    pf = get(f,'Userdata');
    if isempty(pf) || not(ishandle(pf)),
        pf = figure('Toolbar','figure');
        set(f,'Userdata',pf);
    end;
    
    figure(f);
    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos);
    cinfo = findobj(gcf,'Tag','coordinfo');
    
    sz = size(ds.mask);
    
    figure(pf); 
    switch selection,
        case 3,
            if p(2) <= sz(1) && p(2) > 0 && p(1) <= sz(2) && p(1) > 0,
                myplot(squeeze(ds.signal(:,p(1),p(2),idxpos)),ds,3);
                title(sprintf('position (%i,%i,%i)',p(1),p(2),idxpos));
            end;
        case 2,
            if p(1) <= sz(1) && p(1) > 0 && p(2) <= sz(3) && p(1) > 0,            
                myplot(squeeze(ds.signal(:,p(1),idxpos,p(2))),ds,2);
                title(sprintf('position (%i,%i,%i)',p(1),idxpos,p(2)));

            end;
        case 1,
            if p(1) <= sz(2) && p(2) > 0 && p(2) <= sz(3) && p(2) > 0,            
                myplot(squeeze(ds.signal(:,idxpos,p(1),p(2))),ds,1);
                title(sprintf('position (%i,%i,%i)',idxpos,p(1),p(2)));

            end;
            
    end;    
      figure(f);
      
function myplot(f,ds,mode)
    clf; 
    if ds.sym,
        [bDir K] = computeConvHull([ds.dirs'; -ds.dirs'],mode); 
        f = [f(:) ; f(:)];
    else
        [bDir K] = computeConvHull(ds.dirs',mode); 
    end
    n = length(f);

    subplot(2,1,1);

    if sum(f>0) > 0,
        cmap = hot(128);    
        fpos = f.*(f>0);
        valDirPos = bDir.* (fpos*ones(1,3));       
        rgbfpos = cmap(round(127*fpos/max(fpos))+1,:);
        patch('Faces',K,'Vertices',valDirPos,'facecolor','interp','edgecolor','none','FaceVertexCData',rgbfpos);
    end;

    if sum(f<0) > 0,
        cmap = winter(128);
        fneg = -f.*(f<0);
        valDirNeg = bDir.* (fneg*ones(1,3));     
        rgbfneg = cmap(round(127*fneg/max(fneg))+1,:);   
        patch('Faces',K,'Vertices',valDirNeg,'facecolor','interp','edgecolor','none','FaceVertexCData',rgbfneg);
    end;
    
    axis equal; grid on;
    axis([-1 1 -1 1 -1 1]*max(abs(f)));
    view(0,90)
    
    str = sprintf('mean: %.2f  std: %.2f   norm: %.2f   GFA: %.2f   posratio: %.2f  minmax (%.2f,%.2f)',mean(f),std(f),norm(f),std(f)/norm(f), sum(f>0)/length(f),min(f),max(f));
    uicontrol('style','text','position',[0 0 650 20],'string',str,'BackgroundColor',[1 1 1]);
    
    subplot(2,1,2);
    phi = 0:0.05:(2*pi);
    [m idx] = (max((abs(bDir* [cos(phi) ; sin(phi) ; zeros(1,length(phi))]))));
    plot(phi,f(idx)); hold on;
    [m idx] = (max((abs(bDir* [cos(phi) ; zeros(1,length(phi));sin(phi) ]))));
    plot(phi,f(idx),'r');
    [m idx] = (max((abs(bDir* [zeros(1,length(phi)) ; cos(phi) ; sin(phi) ]))));
    plot(phi,f(idx),'g');

  %  axis([0 max(phi) 0 1]); hold off;
   
    grid on;
    return
% --------------------------------------------------------------------
function varargout = resolutionslider_Callback(h, eventdata, handles, varargin)
  
    img = varargin{3};
    showslice(img,varargin{4});
    
    
 % --------------------------------------------------------------------
function varargout = nicebutton_Callback(h, eventdata, handles, varargin)
  
    img = varargin{3};
    showslice(img,varargin{4},1);
       
% --------------------------------------------------------------------
function varargout = radiob_Callback(h, eventdata, handles, varargin)
    data = varargin{1};
     
    radiob = get(h,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    
    mask = data.mask;
    init_slider(size(mask,selection));
 
    slider = findobj(gcf,'Tag','theslider');
    cb = get(slider,'Callback');
    
    showslice(data,cb{6});
    
    
function init_slider(maxs)
    slider = findobj(gcf,'Tag','theslider');
    set(slider,'SliderStep',[1/maxs 0.2]);
    set(slider,'Max',maxs);
    set(slider,'Min',0.99);
    set(slider,'Value',round(maxs/2));

    
    
function showslice(ds,params,nice)

   
    if nargin < 3,
        nice = 0;
    end;
        

    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos);
    cinfo = findobj(gcf,'Tag','coordinfo');
    
    backgr = ds.back;
   
    switch selection,
        case 3,
            showODF(3,ds,squeeze(ds.signal(:,:,:,idxpos)),squeeze(ds.mask(:,:,idxpos)),squeeze(backgr(:,:,idxpos,:)),params(1),params(2),params(3),nice);
             set(cinfo,'String',sprintf('%i/%i',idxpos,size(ds.back,3)));    
         case 2,           
            showODF(2,ds,squeeze(ds.signal(:,:,idxpos,:)),squeeze(ds.mask(:,idxpos,:)),squeeze(backgr(:,idxpos,:,:)),params(1),params(2),params(3),nice);
             set(cinfo,'String',sprintf('%i/%i',idxpos,size(ds.back,2)));    
        case 1,                       
            showODF(1,ds,squeeze(ds.signal(:,idxpos,:,:)),squeeze(ds.mask(idxpos,:,:)),squeeze(backgr(idxpos,:,:,:)),params(1),params(2),params(3),nice);            
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(ds.back,1)));    
    end;
 
    
    set(gca,'Position',[0 0 1 1])
function  [bDir K] = computeConvHull(bDir,mode)

K = convhulln(double(bDir));

switch mode,
    case 1,
        bDir = bDir(:,[2 3 1]);
        %points = single([Z(:)' ; X(:)' ; Y(:)']);
    case 2,
        bDir = bDir(:,[1 3 2]);
        %points = single([X(:)' ; Z(:)' ; Y(:)']);
    case 3,
        bDir = bDir;
       %points = single([X(:)' ; Y(:)' ; Z(:)']);
end;
    
function showODF(mode,ds,data,mask,back,meanval,scval,gamma,nice)

delete(get(gca,'Children'))

if nice == 0,
    if ds.sym,
        [bDir K] = computeConvHull([ds.dirs'; -ds.dirs'],mode);
    else
        [bDir K] = computeConvHull(ds.dirs',mode);
    end;
        
else
    [bDir K] = computeConvHull(ds.upsampdir',mode);
end;

[Xs Ys] = ndgrid(1:size(mask,1)+1,1:size(mask,2)+1);

idx = find(mask>0);
data = data(:,idx);

[ix iy] = ind2sub(size(mask),idx);

data = data-meanval;
%data = data.*(data>0);
data = data.^gamma;

allFaces = [];

for k = 1:length(idx),
    

    
%%% insert here out custom code for displaying the ODF    
    if nice == 0,
        if ds.sym
            C = double([data(:,k) ; data(:,k)]);
        else,
            C = double(data(:,k));
        end;
    else
        C = ds.upsampmat*double(data(:,k));
    end;
    
 %   C = C.*(C>0);
    if scval == 0,
         C = C/(max(abs(C(:)))+eps);
    else
        C = scval*C;    
    end;
   
   R = C;  
    

%    
%     C = C-min(C(:));
%     C = C/max(C(:));
%     R = C;
%     C = C *scval;
%     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    valDir = 0.45*bDir.* (abs(R)*ones(1,3));
    valDir(:,1) = valDir(:,1)+ix(k);
    valDir(:,2) = valDir(:,2)+iy(k);
    valDir(:,3) = valDir(:,3) ;

    allFaces{k} = K+length(C)*(k-1);
    allVerts{k} = valDir;
    allData{k} = C;
    
    %patch('Faces',K,'Vertices',valDir,'facecolor','interp','edgecolor','none','FaceVertexCData',C);     
    %surface(X.*R+x,Y.*R+y,Z.*R,C,'EdgeColor','none'); hold on;
end;

if not(isempty(find(mask(:)>0))),
    toshow = double(back / (eps+max(back(:))));
  %  toshow(mask==0) = min(toshow(mask(:)>0));
    if length(size(toshow)) == 2
        toshow(:,:,2) = toshow(:,:,1);
        toshow(:,:,3) = toshow(:,:,1);
    end;

    surface(Xs-0.5,Ys-0.5,Xs*0-2.99,toshow,'EdgeColor','none', 'FaceLighting', 'none', 'EdgeLighting', 'none','CDataMapping','direct','SpecularStrength',0)



    if not(isempty(allFaces)),
        patch('Faces',cat(1,allFaces{:}),'Vertices',cat(1,allVerts{:}),'facecolor','interp','edgecolor','none','FaceVertexCData',cat(1,allData{:}),...
            'CDataMapping','scaled','hittest','off');
    end

    if nice == 1,
        lighting phong;
   %     camlight;
    end;
end;

%   axis equal    
%axis vis3d
axis off;
%axis([1 size(mask,1) 1 size(mask,2)]);
     









function upsamp = Upsamp(dir,dirout,L)
step = 1;
N = size(dir,2);   
costheta = (dir(3,:));
xiy = dir(1,:) + i*dir(2,:);
xiy = xiy ./(abs(xiy)+0.0001);
costheta_out = (dirout(3,:));
xiy_out = dirout(1,:) + i*dirout(2,:);
xiy_out = xiy_out ./(abs(xiy_out)+0.0001);
for k = 1:step:L,
    LegP = legendre(k-1,costheta,'norm');
    LegP_out = legendre(k-1,costheta_out,'norm');
    for m = -(k-1):(k-1),
        if m < 0 
             SH{k}(m+k,:) = LegP(abs(m)+1,:) .* conj(xiy).^(abs(m)) * (-1)^m;
             SH_out{k}(m+k,:) = LegP_out(abs(m)+1,:) .* conj(xiy_out).^(abs(m)) * (-1)^m;
        else
             SH{k}(m+k,:) = LegP(abs(m)+1,:) .* xiy.^(abs(m));
             SH_out{k}(m+k,:) = LegP_out(abs(m)+1,:) .* xiy_out.^(abs(m));
        end;
        
    end;
    reg{k} = ones(2*(k-1)+1,1)*(k-1).^2;
end;
reg = cat(1,reg{:});
SH = cat(1,SH{:});
SH_out = cat(1,SH_out{:});
upsamp = real(single(SH_out'*pinv(SH*SH'+0.1*diag(reg))*SH));








            