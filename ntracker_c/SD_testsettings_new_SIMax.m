function options=SD_testsettings_new_SIMax(varargin)

%%

%%
if 0
    mh
    img=mhs_local_noramlize_img(img/2^12,'epsilon',0.1);
    img=single(img(301:428,351:478,281:408))
    img2=read_mhd('~/data_ext/nopro_final/elastix/control_05_days/151123/TH-GAL4_UASmCD8GFP_UASsytHA_0days_18C_151123_Channel3_small.mhd');
    img2=mhs_fly_remove_spikes(img2.data);
    img2=mhs_local_noramlize_img(img2/2^12,'epsilon',0.1);
    
    
    %FeatureData=init_mhs_SD_SHorg(img,'use_pyramid',false,'element_size',[1,1,1],'scale_range',[1.15,3],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',0.0001,'maxit',100,'L',4,'Ldata',4);
    
end;

if 0
    %%
    img=read_mhd('~/data_ext/nopro_paper/activity/Tdc2_control-5days_/02/Tdc2-GAL4_UASmCD8GFP_UASsytHA_5days_25C_151118_Channel2_small.mhd');
    %img=single(mhs_local_noramlize_img(img.data/2^12,'epsilon',0.1));
    %img=img(200:end-200,200:end-200,50:end-50);
    img=single(img.data(200:end-200,200:end-200,50:end-50)/2^12);
    %FeatureData=init_mhs_SD_SHorg(img,'use_pyramid',false,'element_size',[1,1,1],'scale_range',[1.15,3],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',0.0001,'maxit',100,'L',4,'Ldata',4);
    
    img=read_mhd('~/data_ext/nopro_paper/activity/Tdc2_Kir-30days_/02/Tdc2-GAL4_UASmCD8GFP_UASsytHA_Kir2-1_tubGAL80_30days_25C_150913_Channel2_small.mhd');
    img_s=(single(img.data(360:560,360:560,150:350))/2^12);
    FeatureData2=init_mhs_SD_SHorg(img_s,'use_pyramid',false,'element_size',[1,1,1],'scale_range',[1.15,3],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.01,0.01,0.01],'maxit',100,'L',4,'Ldata',4);
end;

if 0
    %%
    %img=(1-single(imread('../phantoms/SDphantom.png'))/255);img=mhs_smooth_img(padarray(reshape(img,[size(img),1]),[16,16,16]),1,'normalize',true);
    img=single(((single(single(imread('../phantoms/SDphantom.png'))/255)))<0.25);img=exp(-bwdist(padarray(reshape(img,[size(img),1]),[16,16,16])).^2/(2*1.5^2));
    shape=size(img);
    [X Y Z] = ndgrid(0:(shape(1)-1),0:(shape(2)-1),0:(shape(3)-1));
    %imgd=(img.*(mod(ceil(Y/3),5)>0).*(mod(ceil(X/2),6)>0)+0.1*randn(shape));  
    %imgd=max(0,img-mhs_smooth_img(single((Z==round(shape(3)/2)).*(rand(shape)>0.98)),1))+0.1*randn(shape);
    
    
    %imgd=img./max(img(:))+0.05*randn(shape);
    imgd=(img.*(Y./max(Y(:)*0.5+0.5)))./max(img(:))+0.05*randn(shape);
        
end;

if 0
    %%
    %sigmas=[2,2.5];
    sigmas=[1,1.5];
    offsetA=0;
    imgA=single(((single(single(imread('../phantoms/SDphantom4_1.png'))/255)))<0.25);
    %imgA=exp(-bwdist(padarray(reshape(imgA,[size(imgA),1]),[16,16,16])).^2/(2*sigmas(1)^2));
    imgA=exp(-bwdist(padarray(padarray(reshape(imgA,[size(imgA),1]),[16,16,16]-offsetA,'pre'),[16,16,16]+offsetA,'post')).^2/(2*sigmas(1)^2));
    imgB=single(((single(single(imread('../phantoms/SDphantom4_2.png'))/255)))<0.25);
    imgB=exp(-bwdist(padarray(reshape(imgB,[size(imgB),1]),[16,16,16])).^2/(2*sigmas(2)^2));
    shape=size(imgA);
    [X Y Z] = ndgrid(0:(shape(1)-1),0:(shape(2)-1),0:(shape(3)-1));
    %imgd=(img.*(mod(ceil(Y/3),5)>0).*(mod(ceil(X/2),6)>0)+0.1*randn(shape));  
    %imgd=max(0,img-mhs_smooth_img(single((Z==round(shape(3)/2)).*(rand(shape)>0.98)),1))+0.1*randn(shape);
    %imgd=max(imgA./max(imgA(:)),imgB./max(imgB(:)))+0.05*randn(shape);
    imgd=max(imgA./(max(imgA(:))),imgB./max(imgB(:)))+0.05*randn(shape);
end;



if 0
    %%
    %sigmas=[2,2.5];
    sigmas=[1.5,1.5,1.5];
    offsetA=0;
    imgA=single(((single(single(imread('../phantoms/SDphantom4_1.png'))/255)))<0.25);
    %imgA=exp(-bwdist(padarray(reshape(imgA,[size(imgA),1]),[16,16,16])).^2/(2*sigmas(1)^2));
    imgA=exp(-bwdist(padarray(padarray(reshape(imgA,[size(imgA),1]),[16,16,16]-offsetA,'pre'),[16,16,16]+offsetA,'post')).^2/(2*sigmas(1)^2));
    imgB=single(((single(single(imread('../phantoms/SDphantom4_2.png'))/255)))<0.25);
    imgB=exp(-bwdist(padarray(reshape(imgB,[size(imgB),1]),[16,16,16])).^2/(2*sigmas(2)^2));
    shape=size(imgA);
    
    imgC=single(((single(single(imread('../phantoms/SDphantom4_3.png'))/255)))<0.25);
    imgC=exp(-bwdist(padarray(reshape(imgC,[size(imgC),1]),[16,16,16])).^2/(2*sigmas(3)^2));
    shape=size(imgC);
    
    
    imgd=[max(imgA./(max(imgA(:))),imgB./max(imgB(:)));imgC./max(imgC(:))];
    %imgd=imgd+0.05*randn(size(imgd));
    imgd=imgd+0.01*randn(size(imgd));
    %imgd=mhs_poisson_noise(imgd+0.1+0.1*randn(size(imgd)),12,2^2);
    FeatureData=init_mhs_SD_SHorg(single(imgd),'element_size',[1,1,1],'scale_range',[1.15,2.0],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',10,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1);
    %imgd=max(max(imgA./(max(imgA(:))),imgB./max(imgB(:))),imgC./(max(imgC(:))))+0.05*randn(shape);
end;

%%
if 0
    FeatureData=init_mhs_SD_SHorg(img_s,'element_size',[1,1,1],'scale_range',[1.15,3.0],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.01,0.01],'maxit',100,'L',4,'Ldata',4);
    %FeatureData=init_mhs_SD_SHorg(img_s,'element_size',[1,1,1],'scale_range',[1.15,3.0],'nscales',4,'poldeg',3,'crop',-[10,10,10],'fix_sigma',4.5,'epsilon',0.01,'maxit',100,'L',4,'Ldata',4);
end;


%%
if 0
    img=read_mhd('~/data_ext/nopro_paper/activity/TH_control-0days_/02/TH-GAL4_UASmCD8GFP_UASsytHA_0days_25C_151127_02_Channel2_small.mhd');
    img2=read_mhd('~/data_ext/nopro_paper/activity/TH_control-0days_/02/TH-GAL4_UASmCD8GFP_UASsytHA_0days_25C_151127_02_Channel3_small.mhd');
    p=[439,393,328];
    imgA=img.data(p(1)-64:p(1)+63,p(2)-64:p(2)+63,p(3)-64:p(3)+63);
    imgB=img2.data(p(1)-64:p(1)+63,p(2)-64:p(2)+63,p(3)-64:p(3)+63);

    
end;


%%
        constraint_loop_depth=25;
        the_bif_make_nodata=false;
        bifurcation_particle=false;
        bifurcation_particle_hard=true;
        DataScaleH=-1;

        BifurcationPaneltyFact=0;

        DataThreshold=0;

        temp_dependent=true;
        AngleScale=-1;

        DataScaleGrad=0;
        %lambda=numel(FeatureData.img(:));

        fixed_cooldown=false;

        pL=0;
        bL=0;

        new_version=true;

        DataScaleGradVessel=10;
        DataScaleGradSurface=50;

        ConnectEpsilon=0.01;
        AreaEpsilon=0.001;

        no_collision_threshold=-2;%cos(deg2rad(85));

        directional_weights=false;
        max_vessel_score=-1;

        Epsilon=0.0001;

        opt_particles_per_voxel=0.1;

        terminals_collision=false;
        opt_temp_conn_cost=1;
        opt_temp_conn_cost_dynamic=false;
        
        BifurcationByPathID=-1;

        mode=0;

switch (mode)
   
    case 0   
 proposals={...
     'BIRTH',[2,1]...    
     'DEATH',[1,2],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',2,...    
    'MOVE',2,...
    'SCALE',2,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0,...
    'B_DEATHS',0,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0.01,...
    'SMOOTH',0.01,...
    'FLIPPOS',0.0,...
    };
 time_dependent_scale=true;

  case 1
 proposals={...
     'BIRTH',0,...    
     'DEATH',0,...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',2,...    
    'MOVE',2,...
    'SCALE',2,...    
    'CONNECT',0,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0,...
    'B_DEATHS',0,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH',0,...
    'FLIPPOS',0,...
    };
 time_dependent_scale=false;
  terminals_collision=true;
    case 2
proposals={...
     'BIRTH',0,...    
     'DEATH',0,...
    'C_BIRTH',0,...    
    'C_DEATH',0,...
    'I_BIRTH',0,...    
    'I_DEATH',0,...
    'ROTATE',1,...    
    'MOVE',1,...
    'SCALE',1,...    
    'CONNECT',0,...
    'B_BIRTH',1,...
    'B_DEATH',1,...
    'B_CHANGE',1,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0,...
    'B_DEATHS',0,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH',0,...
    'FLIPPOS',0,...
    };

 time_dependent_scale=false;
 terminals_collision=true;
end;

if false
  proposals={...
     'BIRTH',[10,1]...    
     'DEATH',[1,10],...
    'C_BIRTH',5,...    
    'C_DEATH',5,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',2,...    
    'MOVE',2,...
    'SCALE',2,...    
    'CONNECT',1,...
    'B_BIRTH',0.01,...
    'B_DEATH',0.01,...
    'B_CHANGE',0.01,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0.01,...
    'B_BIRTHL',0.01,...
    'B_DEATHL',0.01,...
    'B_BIRTHX',0.1,...
    'B_DEATHX',0.1,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0.01,...
    'SMOOTH',0.1,...
    'FLIPPOS',0.01,...
    };



end;



 proposals={...
     'BIRTH',[1,1]...    
     'DEATH',[1,1],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',2,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0,...
    'B_DEATHS',0,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0.01,...
    'FLIPPOS',0,...
    'X_BIRTH',0.1,...
    'X_DEATH',0.1,...
    };




 proposals={...
     'BIRTH',[1,1]...    
     'DEATH',[1,1],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',2,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0,...
    'B_DEATHS',0,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0.01,...
    'FLIPPOS',0,...
    'X_BIRTH',0.1,...
    'X_DEATH',0.1,...
    };


 proposals={...
     'BIRTH',[5,1]...    
     'DEATH',[1,5],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',1,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0,...
    'FLIPPOS',0,...
    'X_BIRTH',1,...
    'X_DEATH',1,...
    'BC_BIRTH',0,...%'BC_BIRTH',0.01,...
    };

 proposals={...
     'BIRTH',[1,1]...    
     'DEATH',[1,1],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',0.1,...    
    'I_DEATH',0.1,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',1,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0.1,...
    'B_TDEATH',0.1,...
    'B_BIRTHS',0.1,...
    'B_DEATHS',0.1,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0,...
    'FLIPPOS',0,...
    'X_BIRTH',0.1,...
    'X_DEATH',0.1,...
    'BC_BIRTH',0.01,...%'BC_BIRTH',0.01,...
    'BC_DEATH',0.01,...%'BC_BIRTH',0.01,...
    };


        min_temp=0.00001;

        opt_alpha=0.999;



        BifurcationHScale=-1;

        opt_temp=10;




        particle_point_weight_exponent=1;
        InConePanelty=10;

        semi_soft_collision_power=2;   
        no_bifurcation_self_collision=true;

        


        particle_min_thickness=0.5;
        particle_thickness=-0.25; 

        DataScale=10;
        Template_Similarity=1;

        particle_connection_state_cost_offset=1;

        L=0;


        semi_soft_collision=100;
        soft_collision=true;

        connection_candidate_searchrad=15;
        
        particle_endpoint_offset=0.9;


        no_double_bifurcations=false;
        bifurcation_neighbors=false;



        opt_spawn_blobs=false;
        DistScale=-1;
        
        
         
        particle_connection_state_cost_scale=10*[1,1,1,1,1]; 

        GlobalDataThreshold=-2;
        EdgeBonus=1;
        ThicknessScale=0.1;


        cooldown_threshold=0.01;


        BifurcationArea=-1;
        BifurcationScale=-1;
        BifurcationCScale=1;
        BifurcationDScale=1;

        use_saliency_map=true;



        OrientationScale=-5; 
        ConnectionScale=1;
        DirectionScale=10;
        DirectionScaleThreshold=-1;

        BifurcationArea=-1;
        BifurcationScale=1;%         soft_collision=false;
%         semi_soft_collision=-1;
%         

        BifurcationCScale=10;
        BifurcationDScale=1;
        
        
        semi_soft_collision_power=8;   
        DirectionScale=5;
        
        
        semi_soft_collision_power=8;   
        %clean_term_fact=0.1;
        clean_term_fact=0.1;
        
        semi_soft_collision_power=6;
        semi_soft_collision=100;
        clean_term_fact=0.5;
        max_clean_term_ratio=10;
        opt_temp=10;
        opt_temp_conn_cost_dynamic=true;
        opt_temp_conn_cost=opt_temp;
        opt_temp_conn_cost_dynamic=false;
        
        non_collision_angle=-1;
       
        OrientationScale=-5;
        ConnectionScale=1;
        DirectionScale=5;
        DirectionScaleThreshold=-1;
        EdgeBonus=1;
        opt_temp=5;
        non_edge_prop_fact=1;
        %DataGrad=10;
        DataGrad=-1;
        GlobalDataThreshold=-1;
        
        %semi_soft_collision_power=2;
        %ConnectionScale=1;
        %OrientationScale=-1;
        %DirectionScale=10;
        %clean_term_fact=-1;
        AngleScale=deg2rad(135);
        %AngleScale=deg2rad(75);
        
        consider_birth_death_ratio=true;
        sample_scale_temp_dependent=true;
        connection_candidate_searchrad=10;
        
         opt_alpha=0.999;
         cooldown_threshold=0.000001;
         
         opt_alpha=0.999;
         cooldown_threshold=0.05;
         
         semi_soft_collision_power=6;
         
         
         
         %ConnectionScale=2;
         DirectionScale=1;
         %ConnectionScale=1.5;
         
          GlobalDataThreshold=-0.1;
        %semi_soft_collision_power=4;
        inner_sphere_extra=0.5/semi_soft_collision;
        clean_term_fact=5/semi_soft_collision;
        
        ConnectionScale=2;
        DirectionScale=5;
        OrientationScale=-5;
        inner_sphere_extra=1/semi_soft_collision;
        clean_term_fact=2/semi_soft_collision;
        
        
        particle_thickness=-0.5; 
        
        DataMax=-1;DataMin=1;%off
        
        
         EdgeBonus=2;
         GlobalDataThreshold=-1;
         GlobalDataThreshold=-0.1;
         DataGrad=10;
         
         
        BifurcationArea=-1;
        BifurcationScale=8;
        BifurcationCScale=1;
        BifurcationDScale=1;
         
        % 
        
        %ConnectionScale=0.1;
        %opt_temp=0.0001;
        
        %particle_endpoint_offset=0.9;
        %time_dependent_scale=false;
        
        %soft_collision=false;
        %semi_soft_collision=-1;

        bifurcation_particle_hard=false;
        no_bifurcation_self_collision=true;
        
        proposal_profile=true;
        
        
        
        
if 1        
        
        
        

        bifurcation_particle_soft_scale=1/semi_soft_collision;
        
        BifurcationArea=-1;
        BifurcationScale=4;
        BifurcationCScale=2;
        BifurcationDScale=2;
        
        
         opt_alpha=0.9995;
         cooldown_threshold=0.1;

         bifurcation_edge_bonusfac=1;
         
         
          BifurcationScale=1;
          BifurcationCScale=1;
          BifurcationDScale=1;
          InConePanelty=-1;
          bifurcation_edge_bonusfac=1.5;
          
          %
          bifurcation_panelty=0.1;
          DataGamma=0.25;
           DataGrad=-1;
           bifurcation_edge_bonusfac=1;
           
           
           DataGamma=0.5;
           bifurcation_edge_bonusfac=1.5;
           bifurcation_panelty=1;
           BifurcationScale=0.1;
           BifurcationCScale=2;
           BifurcationDScale=0.1;
           
           
           bifurcation_panelty=0.5;
           BifurcationDScale=2;
           BifurcationCScale=2;
           
           
           %0508
           %AngleScale=deg2rad(75);
           bifurcation_panelty=0.1;
           BifurcationDScale=1;
           BifurcationCScale=1;
           BifurcationScale=1;
           InConePanelty=100;
           bifurcation_edge_bonusfac=1.5;
           %AngleScale=-1;
           
           DataGamma=0.5;
           
           %
            DataScale=10;
            particle_connection_state_cost_scale=10*[1,1,1,1,1]; 
             GlobalDataThreshold=-1;
             
             inner_sphere_extra=0.1/semi_soft_collision;
             clean_term_fact=1/semi_soft_collision;
           
            ConnectionScale=1;
            DirectionScale=0.1;
            OrientationScale=-0.1;%-2;
             %AngleScale=-1;
             AngleScale=deg2rad(135);
             
             BifurcationScale=1;
             DirectionScale=1;
             
             bifurcation_panelty=-1;
             bifurcation_edge_bonusfac=1.5;
             
%before going home             
             %AngleScale=-1;
             AngleScale=deg2rad(135);
             InConePanelty=10;
              
              %samstaeg
              bifurcation_edge_bonusfac=1;
              
              %sonntag
              DirectionScale=2;
              OrientationScale=-1;
              
              
              DirectionScale=-10;
              
              BifurcationDScale=2;
              BifurcationCScale=2;
              BifurcationScale=2;
              
              bifurcation_particle_soft_scale=0.1/semi_soft_collision;
              bifurcation_panelty=0.1;
              

              BifurcationDScale=2;
              BifurcationCScale=2;
              BifurcationScale=0.5;
              
              
              
              
              %MONTAG
              the_bif_make=false;
              %the_bif_make=true;
              
              the_merge_make=false;
              %the_merge_make=true;

              
              % miniDIadem
              GlobalDataThreshold=-0.5;
               ConnectionScale=0.25;
               
               
              GlobalDataThreshold=-1;
               ConnectionScale=1;    
               
               %FREITAG
               AngleScale=-1;
               
               
               nobifurcations=false;
               
               %after gridsearch
if false               
               BifurcationDScale=1.2500;
               BifurcationCScale=1.2500;
               InConePanelty=100;
               BifurcationScale=-1;
               bifurcation_edge_bonusfac=1.1000;               
end;               
               
                %bifurcation_edge_bonusfac=1.5;
               
 %EdgeBonus=2;               
               
               
               %montag
              %bifurcation_edge_bonusfac=1.5;
%               GlobalDataThreshold=-0.5;
%               BifurcationDScale=-1;
%               BifurcationCScale=2;
%               BifurcationScale=0.25;
         
               
                %EdgeBonus=3;
                %GlobalDataThreshold=-2;
                %min_temp=0.0000000001;
                
                %opt_temp=0.0001;
               
               
%                DataGamma=0.25;
%                
%                 DataScale=20;
%             particle_connection_state_cost_scale=DataScale*[1,1,1,1,1]; 
%                ConnectionScale=2;
%                DataGamma=0.5;
%                inner_sphere_extra=0.01/semi_soft_collision;
%                clean_term_fact=0.1/semi_soft_collision;
              
              %img_s=ImgD2.^0.75;
              %img_s=diadem_img.img.^0.75;
              %FeatureData=init_mhs_SD_SHorg(single(img_s),'element_size',[1,1,1],'scale_range',[1,2],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.1,0.1,0.1,0.1],'maxit',50,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1,'adaptive_normalization',false);

              %FeatureData=init_mhs_SD_SHorg(single(img_s),'element_size',[1,1,1],'scale_range',[1,2],'nscales',4,'poldeg',3,'crop',-[10,10,10],'epsilon',[0.05,0.05,0.05,0.05],'maxit',20,'L',4,'Ldata',4,'debug',false,'steerable_conv_fact',1,'adaptive_normalization',false);
              %FeatureData.saliency_map=FeatureData.saliency_map.*(FeatureData.img>0.01);
              
              %Es=extract_paths(mhs_trimtree(E,'min_pathlengt',30),'spacing',15,'smoothing',10,'smoothing_scale',2,'nice_bifucations',true);
              
              %GlobalDataThreshold=-0.1;
              
              
             % clean_term_fact=-1;
            %   inner_sphere_extra=0.5/semi_soft_collision;
              %bifurcation_panelty=1;
              
               
%            DataScale=1;
%            particle_connection_state_cost_scale=1*[1,1,1,1,1]; 
%            EdgeBonus=1;
%            
%            
%            ConnectionScale=1;
%            DirectionScale=1;
%            OrientationScale=-5;
%            
%            bifurcation_particle_soft_scale=1/semi_soft_collision;
            
%            clean_term_fact=0.5/semi_soft_collision;
%            GlobalDataThreshold=-0.1;


DataDistScale=0.1;
DataThickScale=10;

fdata=dbstack;
if numel(fdata)>0
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;



 proposals={...
     'BIRTH',[1,1]...    
     'DEATH',[1,1],...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',0.1,...    
    'I_DEATH',0.1,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',1,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0.1,...
    'B_TDEATH',0.1,...
    'B_BIRTHS',0.1,...
    'B_DEATHS',0.1,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0,...
    'FLIPPOS',0,...
    'X_BIRTH',0.1,...
    'X_DEATH',0.1,...
    'BC_BIRTH',0.01,...%'BC_BIRTH',0.01,...
    'BC_DEATH',0.01,...%'BC_BIRTH',0.01,...
    };



if the_bif_make
    
  BifurcationDScale=-1;
  BifurcationCScale=5;
  BifurcationScale=-1;
  
   bifurcation_edge_bonusfac=2;

  bifurcation_particle_soft_scale=0.1/semi_soft_collision;
  bifurcation_panelty=0.1;

  GlobalDataThreshold=-0.1;    
  InConePanelty=-1;
   AngleScale=-1;
   
  bifurcation_edge_bonusfac=1.5; 
  
  BifurcationCScale=2;
  
  
  BifurcationScale=1;
  BifurcationCScale=1;
  bifurcation_edge_bonusfac=5; 
    
     proposals={...
     'BIRTH',0,...    
     'DEATH',0,...
    'C_BIRTH',0,...    
    'C_DEATH',0,...
    'I_BIRTH',0,...    
    'I_DEATH',0,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',1,...    
    'CONNECT',0,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0,...
    'FLIPPOS',0,...
    'X_BIRTH',0,...
    'X_DEATH',0,...
    'BC_BIRTH',0,...%'BC_BIRTH',0.01,...
    'BC_DEATH',0,...%'BC_BIRTH',0.01,...
    };
end;



if the_bif_make_nodata
    
  BifurcationDScale=-1;
  %BifurcationCScale=5;
  BifurcationScale=-1;
  
  bifurcation_edge_bonusfac=2;

  bifurcation_particle_soft_scale=0.1/semi_soft_collision;
  bifurcation_panelty=-1;

  InConePanelty=-1;
  AngleScale=-1;
   
  bifurcation_edge_bonusfac=1.5; 
  
  BifurcationCScale=5;
  BifurcationScale=0.01;
    
     proposals={...
     'BIRTH',0,...    
     'DEATH',0,...
    'C_BIRTH',0,...    
    'C_DEATH',0,...
    'I_BIRTH',0,...    
    'I_DEATH',0,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',1,...    
    'CONNECT',0,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0,...
    'FLIPPOS',0,...
    'X_BIRTH',0,...
    'X_DEATH',0,...
    'BC_BIRTH',0,...%'BC_BIRTH',0.01,...
    'BC_DEATH',0,...%'BC_BIRTH',0.01,...
    };
end;

if the_merge_make
    
  assert(~the_bif_make)  
  %BifurcationDScale=-1;
  %BifurcationCScale=5;
  %BifurcationScale=-1;
  
  % bifurcation_edge_bonusfac=2;

  %bifurcation_particle_soft_scale=0.1/semi_soft_collision;
  %bifurcation_panelty=0.1;

  %GlobalDataThreshold=-0.1;    
  %InConePanelty=-1;
  % AngleScale=-1;
    
  bifurcation_panelty=-1;
  fixed_cooldown=true;
  
     proposals={...
     'BIRTH',0,...    
     'DEATH',0,...
    'C_BIRTH',0,...    
    'C_DEATH',0,...
    'I_BIRTH',0,...    
    'I_DEATH',0,...
    'ROTATE',0,...    
    'MOVE',0,...
    'SCALE',0,...    
    'MSR',1,...    
    'CONNECT',1,...
    'B_BIRTH',0,...
    'B_DEATH',0,...
    'B_CHANGE',0,...
    'B_TBIRTH',0,...
    'B_TDEATH',0,...
    'B_BIRTHS',0.1,...
    'B_DEATHS',0.1,...
    'B_RECONN',0,...
    'B_BIRTHL',0,...
    'B_DEATHL',0,...
    'B_BIRTHX',0,...
    'B_DEATHX',0,...
    'B_BIRTHC',0,...
    'B_DEATHC',0,...    
    'SCRAMBLE',0,...
    'SMOOTH' ,0,...
    'FLIPPOS',0,...
    'X_BIRTH',0,...
    'X_DEATH',0,...
    'BC_BIRTH',0.1,...%'BC_BIRTH',0.01,...
    'BC_DEATH',0.1,...%'BC_BIRTH',0.01,...
    };
end;




end;          
          
if nobifurcations
    for p=1:2:numel(proposals), 
        if(proposals{p}(1)=='B') && ~strcmpi(proposals{p},'BIRTH'), 
            %fprintf('%s\n',proposals{p});
            proposals{p+1}=0;
        end;
    end;
    for p=1:2:numel(proposals), 
        fprintf('%s -> %d\n',proposals{p},proposals{p+1}(1));
    end;
end


          
lambda=prod(FeatureData.cshape);
use_AODF_min_max=false;

fdata=dbstack;
if numel(fdata)>0
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


if numel(particle_connection_state_cost_offset)==1
    particle_connection_state_cost_offset=particle_connection_state_cost_offset*[1,1,1,1,1];
end


    
    
options={...
            'lambda',lambda,...
            'use_AODF_min_max',use_AODF_min_max,...
            'DataThickScale',DataThickScale,...    
            'DataDistScale',DataDistScale,...    
            'min_temp',min_temp,...
            'bifurcation_panelty',bifurcation_panelty,...
            'DataGamma',DataGamma,...
            'bifurcation_particle_soft_scale',bifurcation_particle_soft_scale,...
            'bifurcation_edge_bonusfac',bifurcation_edge_bonusfac,...
            'proposal_profile',proposal_profile,...
            'DataMax',DataMax,...
            'DataMin',DataMin,...
            'inner_sphere_extra',inner_sphere_extra,...
            'sample_scale_temp_dependent',sample_scale_temp_dependent,...
            'consider_birth_death_ratio',consider_birth_death_ratio,...
            'DataGrad',DataGrad,...
            'non_edge_prop_fact',non_edge_prop_fact,...
            'max_clean_term_ratio',max_clean_term_ratio,...
            'opt_temp_conn_cost',opt_temp_conn_cost,...
            'opt_temp_conn_cost_dynamic',opt_temp_conn_cost_dynamic,...
            'non_collision_angle',non_collision_angle,...
            'clean_term_fact',clean_term_fact,...
            'terminals_collision',terminals_collision,...
            'cooldown_threshold',cooldown_threshold,...
            'time_dependent_scale',time_dependent_scale,...
            'EdgeBonus',EdgeBonus,...
            'soft_collision',soft_collision,...
            'opt_spawn_blobs',opt_spawn_blobs,...
            'DistScale',DistScale,...
            'no_bifurcation_self_collision',no_bifurcation_self_collision,...
            'DirectionScaleThreshold',DirectionScaleThreshold,...
            'DirectionScale',DirectionScale,...
            'InConePanelty',InConePanelty,...
            'semi_soft_collision',semi_soft_collision,...
            'semi_soft_collision_power',semi_soft_collision_power,...
            'opt_particles_per_voxel',opt_particles_per_voxel,...
            'no_double_bifurcations',no_double_bifurcations,...
            'max_vessel_score',max_vessel_score,...
            'directional_weights',directional_weights,...
            'no_collision_threshold',no_collision_threshold,...
            'AreaEpsilon',AreaEpsilon,...
            'ConnectEpsilon',ConnectEpsilon,...
            'Epsilon',Epsilon,...
            'temp_dependent',temp_dependent,...
            'particle_collision_two_steps',0,...
            'opt_numiterations',1000000,...
            'opt_alpha',opt_alpha,...%0.99995,...
            'DataThreshold',GlobalDataThreshold,...%-25,...
            'DataScaleGrad',DataScaleGrad,...
            'DataScaleGradSurface',DataScaleGradSurface,...
            'DataScale',DataScale,...
            'DataScaleH',DataScaleH,...
            'new_version',new_version,...
            'DataScaleGradVessel',DataScaleGradVessel,...    
            'particle_endpoint_offset',particle_endpoint_offset,...
            'particle_connection_state_cost_scale',particle_connection_state_cost_scale,...
            'particle_connection_state_cost_offset',particle_connection_state_cost_offset,...-pL*[0,0.25,1],
            'particle_thickness',particle_thickness,...-1,...-1/4,...
            'particle_min_thickness',particle_min_thickness,...
            'connection_candidate_searchrad',connection_candidate_searchrad,...%f15,...%8,...
            'connection_bonus_L',-L,...
            'bifurcation_bonus_L',-bL,...
            'ConnectionScale',ConnectionScale,...
            'bifurcation_neighbors',bifurcation_neighbors,...
            'AngleScale',AngleScale,...$deg2rad(60),...
            'ThicknessScale',ThicknessScale,...%,1,...
            'OrientationScale',OrientationScale,...%,1,...
            'BifurcationScale',BifurcationScale,...%,1,...
            'BifurcationHScale',BifurcationHScale,...%,1,...
            'BifurcationCScale',BifurcationCScale,...%,1,..
            'BifurcationArea',BifurcationArea,...%,1,...
            'BifurcationDScale',BifurcationDScale,...%,1,...
            'opt_temp',opt_temp,...
            'constraint_loop_depth',constraint_loop_depth,...
            'use_saliency_map',use_saliency_map,...
            'proposals',proposals...
            'BifurcationByPathID',BifurcationByPathID,...
            'scale_power',particle_point_weight_exponent,...
            'particle_point_weight_exponent',particle_point_weight_exponent,...    
            'bifurcation_particle_hard',bifurcation_particle_hard,...
            'BifurcationPaneltyFact',BifurcationPaneltyFact,...
            'fixed_cooldown',fixed_cooldown,...
            };
        
        
 for k = 1:2:numel(options),
    if isnumeric(options{k+1})
    fprintf('%s : %s\n;',options{k},num2str(options{k+1}));
    end
end;
