#ifndef PROPOSAL_SMOOTH_H
#define PROPOSAL_SMOOTH_H
#include "proposals.h"

 template<typename TData,typename T,int Dim> class CProposal;



template<typename TData,typename T,int Dim>
class CProposalSmoothEdges : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
    T ConnectEpsilon;
    bool temp_dependent;
    
    T lambda;		
      bool consider_birth_death_ratio;
      bool use_saliency_map;
		
    
public:
  
  T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_edges(num_edges,
		      num_particles,
		      num_connected_particles,		      
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }



    CProposalSmoothEdges(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
	temp_dependent=true;
	
	lambda=100;
	use_saliency_map=false;
	consider_birth_death_ratio=false;
    }
    
    ~CProposalSmoothEdges()
    {

    }
    

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT_SMOOTH);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_CONNECT_SMOOTH);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	
	if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	
		if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];	
	
	   if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];
	   
	           if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
    }
    
    class CConfigProposal
    {
      public:
	Points<T,Dim> * points[4];
	class Points< T, Dim >::CEndpoint * endpoints[6];
    };

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;
	
	OctPoints<T,Dim> *  cpoint;
	cpoint=tree.get_rand_point();
	if (cpoint==NULL)
	{
	  return false;
	}
	
	Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	
	if (point.isfreezed())
	{
	    return false;
	}
	
	if (point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	  return false;
	
	if (point.get_num_connections()!=2)
	  return false;
	
	bool debug=false;
	
if (debug)	  
  printf("0");	
	
	try {		  

	  
	  
	  class Points< T, Dim >* neigh[2];
	  class Points< T, Dim >::CEndpoint* neighep[2];
	  T thicknes[2];
// 	  T scale[2];
	  
	  for (int i=0;i<2;i++)
	  {
	    neigh[i]=point.endpoints[i][0]->connected->point;
	    neighep[i]=point.endpoints[i][0]->connected;
	    //thicknes[i]=neigh[i]->get_thickness();
	    thicknes[i]=neigh[i]->get_scale();
// 	    scale[i]=neigh[i]->get_scale();
	    sta_assert_debug0(neigh[i]!=NULL);
	  }
	  
	  T w[2];
	  for (int i=0;i<2;i++)
	  {
	    w[i]=thicknes[1-i]/(thicknes[0]+thicknes[1]);
	  }
	  
	  //Vector<T,3> best_pos=(neigh[0]->get_position())*w[0]+(neigh[1]->get_position())*w[1];
	  Vector<T,3> best_pos=(*(neighep[0]->position))*w[0]+(*(neighep[1]->position))*w[1];
	  
	  //Vector<T,3> best_dir=(neigh[0]->get_position()-neigh[1]->get_position())*Points<T,Dim>::side_sign[0];
	  Vector<T,3> best_dir=(*(neighep[0]->position)-*(neighep[1]->position))*Points<T,Dim>::side_sign[0];
	  T sigma=std::sqrt((best_dir).norm2());
	  
	  best_dir/=sigma+std::numeric_limits< T >::epsilon();
	  
	  	  
	  
	  
	  
	  
	  
	  T scale=point.get_scale();
	  
	  
	  Vector<T,Dim> new_pos;
	  
//	  Vector<T,Dim> new_dir;
//	  T temp_fact_rot=2;
	  
	  T temp_fact_pos=std::max(sigma/2,T(1));

	  
	  T tw=1;
	  if (temp_dependent)
	  {
	    tw=proposal_temp_trafo(temp,scale,T(1));;
//	    temp_fact_rot*=tw;
	  }
	  
	  temp_fact_pos*=tw;
	  
	  T new_pos_prob;
	  T old_pos_prob;
	  //
	  if (false)
	  {
	    new_pos.rand_normal(temp_fact_pos);
	    new_pos_prob=normal_dist<T,3>(new_pos.v,temp_fact_pos);

	    Vector<T,Dim> point_shift=point.get_position()-best_pos;
	    old_pos_prob=normal_dist<T,3>(point_shift.v,temp_fact_pos);
	  }else
	  {
	    Vector<T,3> vn[3];
	    vn[0]=best_dir;
	    T aniso=tw*scale/2;
	    mhs_graphics::createOrths(vn[0],vn[1],vn[2]); 	
// 	    vn[0].rand_normal(temp_fact_pos);
// 	    vn[1].rand_normal(aniso);
// 	    vn[2].rand_normal(aniso);
	    
	    vn[0]*=mynormalrand(temp_fact_pos);
	    vn[1]*=mynormalrand(aniso);
	    vn[2]*=mynormalrand(aniso);
	    
	    new_pos=vn[0]+vn[1]+vn[2];
	    
	    new_pos_prob=normal_dist<T,3>(new_pos,best_dir,temp_fact_pos,aniso);
	    //T new_pos_prob=normal_dist<T,3>(new_pos.v,temp_fact_pos);

	    Vector<T,Dim> point_shift=point.get_position()-best_pos;
	    //T old_pos_prob=normal_dist<T,3>(point_shift.v,temp_fact_pos);
	    old_pos_prob=normal_dist<T,3>(point_shift,best_dir,temp_fact_pos,aniso);
	    
// 	    T new_pos_prob2=normal_dist<T,3>(new_pos.v,temp_fact_pos);
// 	    T old_pos_prob2=normal_dist<T,3>(point_shift.v,temp_fact_pos);
// 	    
// 	    
// 	    printf("%f  %f || %f  %f\n",old_pos_prob,old_pos_prob2,new_pos_prob,new_pos_prob2);
	  }
	  
	  
	 
	  
	  
	  new_pos+=best_pos;
	  
	  
	

	  
	  
if (debug)	  
  printf("A");
	  
	  
	if ((new_pos[0]<0)||(new_pos[1]<0)||(new_pos[2]<0)||
	  (!(new_pos[0]<shape[0]))||(!(new_pos[1]<shape[1]))||(!(new_pos[2]<shape[2])))
	{
	    return false;
	}
	
	
	if ((point.get_position()-new_pos).norm2()<0.0000001)
	{
	    return false;
	}
	
	
	
	
	

if (debug)	  
  printf("B");	

	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	T old_cost=point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      inrange);

	sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),
	  (point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      inrange,true))
	);
	
if (debug)	  
  printf("C");
	
	T oldenergy_data=point.point_cost;
	Vector<T,Dim> old_pos=point.get_position();
	
	T particle_interaction_cost_old=0;
	T particle_interaction_cost_new=0;
	
	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	class Collision<T,Dim>::Candidates cand(query_buffer);
	
	if (collision_fun_p.is_soft())    
	{
	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp));
	}
	
if (debug)	  
  printf("D");	
	
	sta_assert_error(point.has_owner(0));
	    
	point.set_position(new_pos);
 	    
	if (!point.update_pos(true))
	{
	  point.set_position(old_pos);
	  //point.set_pos_dir(old_pos,old_dir);
	  
	  return false;
	}
	
	point.update_pos();
	
	
// 	  new_dir=random_pic_direction<T,Dim>(best_dir,temp_fact_rot);
// 	  T new_rot_prob=normal_dist_sphere(best_dir,new_dir,temp_fact_rot);
// 	  
// 	  T old_rot_prob=normal_dist_sphere(best_dir,point.get_direction(),temp_fact_rot);
// 	  
// 	Vector<T,Dim> old_dir=point.get_direction();
// 	point.set_direction(new_dir);
	
	  
if (debug)	  
  printf("E");	

	if (collision_fun_p.colliding(particle_interaction_cost_new, tree,point,collision_search_rad,temp))
	{
	  point.set_position(old_pos);
	  //point.set_pos_dir(old_pos,old_dir);
	  sta_assert_error(point.update_pos());
	  return false;
	}
if (debug)	  
  printf("F");			

	T newpointsaliency=0;
	 if ((!data_fun.saliency_get_value(newpointsaliency,point.get_position()))||((newpointsaliency<std::numeric_limits<T>::min())))
	{
		      point.set_position(old_pos);
		      //point.set_pos_dir(old_pos,old_dir);
		      sta_assert_error(point.update_pos());
		      return false;
	}
	
if (debug)	  
  printf("G");	
	
	T newenergy_data;
	if (!data_fun.eval_data(
	  newenergy_data,
	  point))
	  {
		  point.set_position(old_pos);
		  //point.set_pos_dir(old_pos,old_dir);
		  sta_assert_error(point.update_pos());
	      return false;
	  }

if (debug)	  
  printf("H");	  
		    
	T old_saliency=point.saliency;
	point.saliency=newpointsaliency;///saliency_correction[2];
	T new_cost=point.e_cost(edgecost_fun,
				options.connection_bonus_L,
				options.bifurcation_bonus_L,
				inrange);
	
	
if (debug)	  
  printf("I");

	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	{
	  point.saliency=old_saliency;
	  point.set_position(old_pos);
	  //point.set_pos_dir(old_pos,old_dir);
	  sta_assert_error(point.update_pos());
	  return false;
	}
	
	T E=(new_cost-old_cost)/temp;
	E+=(newenergy_data-oldenergy_data)/temp;	
	
	
	
	if (collision_fun_p.is_soft())   
	{
	    E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	}
	
	
	

	T R=mhs_fast_math<T>::mexp(E);
	
	//R*=(old_pos_prob*old_rot_prob)/((new_pos_prob*new_rot_prob)+std::numeric_limits<T>::epsilon());
	R*=(old_pos_prob)/((new_pos_prob)+std::numeric_limits<T>::epsilon());
	
	
	 if (consider_birth_death_ratio && use_saliency_map)
	{
	      R*=old_saliency/(point.saliency+std::numeric_limits<T>::epsilon());
	}	
	
if (debug)	  
  printf("J");
  
	if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	{
	  
	    this->tracker->update_energy(E,static_cast<int>(get_type()));
	    this->proposal_accepted++;

	    point.point_cost=newenergy_data;
	    
	    #ifdef  D_USE_GUI
	      neigh[0]->touch(get_type());
	      neigh[1]->touch(get_type());
	      point.touch(get_type());
	    #endif

if (debug)	  
  printf("K");	      
	    return true;
	}
	
	point.saliency=old_saliency;
	point.set_position(old_pos);
	//point.set_pos_dir(old_pos,old_dir);
	sta_assert_error(point.update_pos());
if (debug)	  
  printf("L");	
	return false;
    
    
	  
	} catch (mhs::STAError error)
	{
	    throw error;
	}
	
	return false;
	
    }
    


};

#endif


