#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"

#define EPSILON 0.00000000000001


#include "mhs_lapack.h"

//mex CXXFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "  CFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "   sta_EVGSL.cc -l gsl -l gslcblas -l gomp


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

  
    const mxArray *SalTensor;
    SalTensor = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(SalTensor);
    const int *dims = mxGetDimensions(SalTensor);

//     printf("input dims : %d\n",numdim);
if (numdim==4)
{
    int shape[3];  
    shape[0] = dims[3];
    shape[1] = dims[2];
    shape[2] = dims[1];    
    T *saltensor = (T*) mxGetData(SalTensor);
    if (dims[0]!=6)
      mexErrMsgTxt("error: first dim must be 6\n");
    
    int ndims[4];
    ndims[0]=9;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    ndims[3]=dims[3];
    plhs[0] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
    T *eigen_vec = (T*) mxGetData(plhs[0]);

    ndims[0]=3;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    ndims[3]=dims[3];
    plhs[1] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
    T *eigen_val = (T*) mxGetData(plhs[1]);
    
    
    
    
    bool sort=true;
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"sort")!=-1)
            sort=mhs::mex_getParam<bool>(params,"sort",1)[0];
    }
    
    
    

    std::size_t numvoxel=shape[0]*shape[1]*shape[2];
    
    
//     #pragma omp paralell for 4
    for (std::size_t idx= 0; idx < numvoxel; idx++)    
    {
// 	T * p_saltensor=saltensor+idx*6;
// 	T * p_eval=eigen_val+idx*3;
// 	T * p_evec=eigen_vec+idx*9;
// 	eig_3x3_symm(	p_saltensor[0],
// 			p_saltensor[3],
// 			p_saltensor[5],
// 			p_saltensor[1],
// 			p_saltensor[4],
// 			p_saltensor[2],
// 			p_evec, 
// 			p_eval); 
	
	eig_3x3_symm(	saltensor[0],
			saltensor[3],
			saltensor[5],
			saltensor[1],
			saltensor[4],
			saltensor[2],
			eigen_vec, 
			eigen_val);       
	  
	  
	  saltensor+=6;
	  eigen_val+=3;
	  eigen_vec+=9;
  }

		
}else 
  if (numdim==3)
   {
     mexErrMsgTxt("error: 2D not supported yet\n");
   }
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}