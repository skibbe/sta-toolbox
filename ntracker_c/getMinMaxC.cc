#include "mex.h"

#include <unistd.h>
#include <complex>
#include <map>
#include <gsl/gsl_blas.h>
#include "tred33.h"


#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"
// #define _SUPPORT_MATLAB_ 

#include <complex>
#include <vector>
#include <string>
#include <ctime>
#include <list>
#include <sstream>
#include <string>
#include <limits>
#include <omp.h>
#include "mhs_graphics.h"
  
// #include "sta_mex_helpfunc.h"
// #include "sta_omp_threads.h"

/*
 mex getLocalMaxC.cpp 
*/




using namespace std;

template<typename T>
std::complex<T> sh_test(T * direction,std::complex<T> * tensor,int order_sd_data)
{
  {
	      std::complex<T> tmp=0;
	      const T & x=direction[0];
	      const T & y=direction[1];
	      const T & z=direction[2];
	      
	      
	      printf("%f %f %f\n",x,y,z);
	    
	      //std::complex<T> x_myi=std::complex<T>(x,-y);
	      std::complex<T> x_myi=std::complex<T>(x,y); //contravariant
	      std::complex<T> x_myi2=x_myi*x_myi;
	
	      T x2=x*x;
	      T x4=x2*x2;
	      T y2=y*y;
	      T y4=y2*y2;
	      T z2=z*z;
	      T z4=z2*z2;

	     
	      
	      tmp+=tensor[0];
	      
	      if (order_sd_data>=2)
	      {
		const T l_weight=2*2+1;
		tmp+=T(l_weight*std::sqrt(3.0/2.0))*(x_myi2 * tensor[1]);
		tmp+=T(l_weight*(std::sqrt(6.0)*z)) *(x_myi*tensor[2]);
		tmp+=T(l_weight*((-x2-y2)/2+z2))*tensor[3];
	      }
// 	      
	      if (order_sd_data>=4)
	      {
		const T l_weight=4*2+1;
		tmp+=T(l_weight*1.0/4.0 * std::sqrt(35.0/2.0))*(x_myi2*x_myi2*tensor[4]);
		tmp+=T(l_weight*1.0/2.0 * std::sqrt(35.0))*(x_myi2*x_myi*z*tensor[5]);
		tmp+=T(l_weight*-1.0/2.0 * std::sqrt(5.0/2.0)*(x2 + y2 - 6*z2))*(x_myi2*tensor[6]);
		tmp+=T(l_weight*-1.0/2.0*std::sqrt(5.0) * z *(3 * x2+ 3 * y2 - 4 * z2)) *(x_myi*tensor[7]);
		tmp+=T(l_weight*(3* (x4)/8 +  3* (y4)/8 - 3* y2 * z2 + z4 + 3.0/4.0 * x2 *(y2 - 4 * z2)))*tensor[8];
	      }

	      return tmp;
		
		
// 	     sd_filter=tmp.real();
	    }
}


template<typename T>
void blas_matrix_mult(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
  
}

using namespace std;

template<>
void blas_matrix_mult<double>(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    double one[2] = {1,0};
    double zero[2] = {0,0};
    cblas_zgemm (CblasColMajor,
		 CblasTrans,
		 CblasNoTrans,
		 ndir,
		 lp_n,
		 stride,
		 (void*) one,
		 mout,
		 stride,
		 pin,
		 stride,
		 (void*) zero,
		 tmp,
		 ndir);           
}

template<>
void blas_matrix_mult<float>(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    float one[2] = {1,0};
    float zero[2] = {0,0};
    cblas_cgemm (CblasColMajor,
		 CblasTrans,
		 CblasNoTrans,
		 ndir,
		 lp_n,
		 stride,
		 (void*) one,
		 mout,
		 stride,
		 pin,
		 stride,
		 (void*) zero,
		 tmp,
		 ndir);           
}


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    
    
    const mxArray *ST1;
    ST1 = prhs[0];       
    const mwSize *dimsST1 = mxGetDimensions(ST1);
    const mwSize numdimST1 = mxGetNumberOfDimensions(ST1);
    if (numdimST1!=5)  mexErrMsgTxt("error: Array dimension for the first argument must be 5 (2x2L+1xXxYxZ)\n"); 
    std::complex<T> *pin = (std::complex<T> *) (mxGetData(ST1));
  
    const mxArray *BDir = prhs[1];     
    T *bdir = (T *) (mxGetData(BDir));
    long int ndir = mxGetN(BDir);
    
    
    const mxArray *Neighbors= prhs[2];     
    double *neighs = (double *) (mxGetData(Neighbors));
    int numneighs = mxGetM(Neighbors);
    int* Nidx = (int*) malloc(sizeof(int)*numneighs*ndir);
    for (int k = 0; k < numneighs*ndir;k++)
        Nidx[k] = (int) (neighs[k] -1);     
    
    if (ndir != mxGetN(Neighbors))
    {
        mexPrintf("dimension mismatch 1\n");
	free(Nidx);
        return;
    }
    
    const mxArray *Mout = prhs[3];     
    T *mout = (T *) (mxGetData(Mout));
    const mwSize *dimsMout = mxGetDimensions(Mout);
    if (ndir != dimsMout[2])
    {
        mexPrintf("dimension mismatch 2\n");
        return;
    }
   
   
    if (dimsST1[1] != dimsMout[1])
    {
        mexPrintf("dimension mismatch 3\n");
        return;
    }
    
    const mxArray *LocPS = prhs[4];     
    T *locPS = (T *) (mxGetData(LocPS));
    int interp = 1;
    if (locPS == 0)
        interp = 0;
         
    const mxArray *Params = prhs[5];     
    T *params = (T *) (mxGetData(Params));
    
    std::size_t nummax = 2;//(int) params[0];
    
    
    int mode = (int) params[0];
    int sym = (int) params[1];
    
    

    
    std::size_t shape[3];    
    for(int i =2;i <numdimST1;i++)
    {
    	shape[4-i]=dimsST1[i];
    }
    
    
    //std::size_t num_elements=outdim[0] *outdim[1] *outdim[2] *outdim[3];
       
    mwSize outdim[4];
    T *result = NULL;
    T *result_v = NULL;
    
    if (mode==0)
    {
      outdim[0] = 3*nummax;
      outdim[1] = dimsST1[2];
      outdim[2] = dimsST1[3];
      outdim[3] = dimsST1[4];
      
      
      plhs[0] = mxCreateNumericArray(4,outdim,mxGetClassID(ST1),mxREAL);
      result = (T *) mxGetData(plhs[0]);
      
      
      outdim[0] = nummax;
      plhs[1] = mxCreateNumericArray(4,outdim,mxGetClassID(ST1),mxREAL);
      result_v = (T *) mxGetData(plhs[1]);
    }else 
    {
      outdim[0] = 6;
      outdim[1] = dimsST1[2];
      outdim[2] = dimsST1[3];
      outdim[3] = dimsST1[4];
      
      
      plhs[0] = mxCreateNumericArray(4,outdim,mxGetClassID(ST1),mxREAL);
      result = (T *) mxGetData(plhs[0]);
      
      outdim[0] = nummax;
      plhs[1] = mxCreateNumericArray(4,outdim,mxGetClassID(ST1),mxREAL);
      result_v = (T *) mxGetData(plhs[1]);
    }


    std::size_t sz = dimsST1[2]*dimsST1[3]*dimsST1[4];
 
    long int stride = dimsST1[1];
    
 
   

     long int lp_n = 1;
 


//     map<T,int> locmax;
//     T tmp[2*ndir];
//     T tmpV[9];
//     T tmpV2[6];
//     T *V[3];
//     V[0] = &tmpV[0];
//     V[1] = &tmpV[3];
//     V[2] = &tmpV[6];
//     T Gm[numneighs];
//     T dm[3];
//     T em[3];
//     int szpsmat = 6*(numneighs+1);
    
//     printf("%d\n",hanalysis::get_numCPUs()); 
     
     
    #pragma omp parallel for num_threads(omp_get_num_procs())
    for (std::size_t k = 0; k < sz; k++)
    {
      
	T min_max[2];
	int min_max_indx[2];
	T & min_v=min_max[0];
	T & max_v=min_max[1];
        max_v=-std::numeric_limits<T>::max();
	min_v=std::numeric_limits<T>::max();
	int & indx_min=min_max_indx[0];
	int & indx_max=min_max_indx[1];
	
	indx_max=-1;
	indx_min=-1;
	
	T tmp[2*ndir];
	T tmpV[9];
	T tmpV2[6];
	T *V[3];
	V[0] = &tmpV[0];
	V[1] = &tmpV[3];
	V[2] = &tmpV[6];
	T Gm[numneighs];
	T dm[3];
	T em[3];
	int szpsmat = 6*(numneighs+1);
       
         
        //cblas_zgemm (CblasColMajor, CblasTrans, CblasNoTrans, ndir,lp_n,stride, (void*) one, mout, stride,(void*) &( pin[k*stride] ),stride, (void*) zero,tmp,ndir);           
	
	blas_matrix_mult<T>(
		      ndir,
		      lp_n,
		      stride,
		      mout,
		     (void*) &( pin[k*stride] ),
		      tmp);
	
	
	     
	
	 
// 	 if (k%1000000==0)
// 	 { 
// 	   //for (int d=0;d<ndir;d++)
// 	   for (int d=5;d<6;d++)
// 	   {
// 	    std::complex<T> tmp2=sh_test(bdir+3*d,(std::complex<T> *) &(pin[k*stride] ),4);
// 	    std::complex<T> tmp1(tmp[d*2],tmp[d*2+1]);
// 	    printf("[%d] %f %f / %f %f\n",d+100,tmp1.real(),tmp1.imag(),tmp2.real(),tmp2.imag());
// 	    printf("%u, [%f %f %f]",k,*(bdir+3*d),*(bdir+3*d+1),*(bdir+3*d+2));
// 	   }
// 	 }
	
	
      
        int uplim = ndir;
        if (sym)
            uplim = uplim/2;
        
        for(int m = 0; m < uplim;m++)
        {
	      T v=tmp[2*m]; 
	      
	      if (v<min_v)
	      {
		min_v=v;
		indx_min=m;
	      }
	      if (v>max_v)
	      {
		max_v=v;
		indx_max=m;
	      }	      
        }
        
        if ((indx_max<0)||(indx_min<0))
	{
	  printf("error, function undefined \n");
	  continue;
	}
	
	
        
      
//         std::size_t cnt = 0;
        
//         for (typename map<T,int>::iterator it = locmax.end(); it != locmax.begin();)
//         {           
//             it--;
//             if (cnt== nummax)
//                 break;
//             int idx = it->second;
//             T val = it->first;
            
	    
	    for (std::size_t cnt=0;cnt<2;cnt++)
	    {
	
	      const int & idx=min_max_indx[cnt];
	      T & val=min_max[cnt];
	      
	      
	      if (interp)
	      {
				    
		    for (int j = 0; j < numneighs;j++)
		    { 
			Gm[j] = tmp[2*Nidx[j+numneighs*idx]];
		    }
		    Gm[numneighs] = tmp[2*idx];
		    for (int i = 0; i < 6;i++)
		    {
			tmpV2[i] = 0;
			for (int j = 0; j < numneighs+1;j++)                         
			    tmpV2[i] += locPS[6*j+i+idx*szpsmat]*Gm[j];
		    }
		    V[0][0] = tmpV2[0];
		    V[1][1] = tmpV2[1];
		    V[2][2] = tmpV2[2];
		    V[0][1] = tmpV2[3];
		    V[0][2] = tmpV2[4];
		    V[1][2] = tmpV2[5];
		    V[1][0] = tmpV2[3];
		    V[2][0] = tmpV2[4];
		    V[2][1] = tmpV2[5];

		    tred2<T>(V, dm, em);
		    tql2<T>(V, dm, em);
		    
		    int sg = ((V[0][2]*bdir[3*idx] +  V[1][2]*bdir[3*idx+1] + V[2][2]*bdir[3*idx+2])>0)?1:-1;

		    //sta_assert(nummax*3*k + 3*cnt +2<num_elements);
		    
		    if (mode==0)
		    {
		      
			
			result[nummax*3*k + 3*cnt]    =  V[0][2]*dm[2]*sg;
			result[nummax*3*k + 3*cnt +1] =  V[1][2]*dm[2]*sg;
			result[nummax*3*k + 3*cnt +2] =  V[2][2]*dm[2]*sg;
			
			result_v[nummax*k+ cnt]=dm[2];
// 			printf("-\n");
		    }else
		    {
		       
		       //val=(val>0) ? std::abs(dm[2]) : -std::abs(dm[2]);
		       result_v[nummax*k+ cnt]=val;
		       
		       if (cnt==1)
		       {
			Vector<T,3>  vn0(V[0][2]*sg,V[1][2]*sg,V[2][2]*sg);
			//Vector<T,3>  vn0(bdir[3*idx],bdir[3*idx+1],bdir[3*idx+2]);
			Vector<T,3>  vn1;
			Vector<T,3>  vn2;
// 			vn0.normalize();
			mhs_graphics::createOrths(vn0,vn1,vn2);
			
			  T *  H=result+6*k; 
			  T & xx=  H[0]; //xx
			  T & xy=  H[3]; //xy
			  T & xz=  H[5]; //xz
			  T & yy=  H[1]; //yy
			  T & yz=  H[4]; //yz
			  T & zz=  H[2]; //zz
			
			  xx=  max_v * vn0[0]*vn0[0] + min_v *  (vn1[0]*vn1[0] + vn2[0]*vn2[0]); //xx
			  xy=  max_v * vn0[0]*vn0[1] + min_v *  (vn1[0]*vn1[1] + vn2[0]*vn2[1]); //xx; //xy
			  xz=  max_v * vn0[0]*vn0[2] + min_v *  (vn1[0]*vn1[2] + vn2[0]*vn2[2]); //xx; //xz
			  yy=  max_v * vn0[1]*vn0[1] + min_v *  (vn1[1]*vn1[1] + vn2[1]*vn2[1]); //xx; //yy
			  yz=  max_v * vn0[1]*vn0[2] + min_v *  (vn1[1]*vn1[2] + vn2[1]*vn2[2]); //xx; //yz
			  zz=  max_v * vn0[2]*vn0[2] + min_v *  (vn1[2]*vn1[2] + vn2[2]*vn2[2]); //xx; //zz
		       }
		    }
	      }
	      else
	      {     
		  //sta_assert(nummax*3*k + 3*cnt +2<num_elements);
		    if (mode==0)
		    {
			  result[nummax*3*k + 3*cnt]    =  bdir[3*idx]*val;
			  result[nummax*3*k + 3*cnt +1] =  bdir[3*idx+1]*val;
			  result[nummax*3*k + 3*cnt +2] =  bdir[3*idx+2]*val;
			  
			  result_v[nummax*k+ cnt]=val;
		    }else
		    {
		        result_v[nummax*k+ cnt]=val;
		       if (cnt==1)
		       {
			Vector<T,3>  vn0(bdir[3*idx],bdir[3*idx+1],bdir[3*idx+2]);
			//Vector<T,3>  vn0(bdir[3*idx+2],bdir[3*idx+1],bdir[3*idx+0]);
			Vector<T,3>  vn1;
			Vector<T,3>  vn2;
			mhs_graphics::createOrths(vn0,vn1,vn2);
			
			  T *  H=result+6*k; 
			  T & xx=  H[0]; //xx
			  T & xy=  H[3]; //xy
			  T & xz=  H[5]; //xz
			  T & yy=  H[1]; //yy
			  T & yz=  H[4]; //yz
			  T & zz=  H[2]; //zz
			
			  xx=  max_v * vn0[0]*vn0[0] + min_v *  (vn1[0]*vn1[0] + vn2[0]*vn2[0]); //xx
			  xy=  max_v * vn0[0]*vn0[1] + min_v *  (vn1[0]*vn1[1] + vn2[0]*vn2[1]); //xy
			  xz=  max_v * vn0[0]*vn0[2] + min_v *  (vn1[0]*vn1[2] + vn2[0]*vn2[2]); //xz
			  yy=  max_v * vn0[1]*vn0[1] + min_v *  (vn1[1]*vn1[1] + vn2[1]*vn2[1]); //yy
			  yz=  max_v * vn0[1]*vn0[2] + min_v *  (vn1[1]*vn1[2] + vn2[1]*vn2[2]); //yz
			  zz=  max_v * vn0[2]*vn0[2] + min_v *  (vn1[2]*vn1[2] + vn2[2]*vn2[2]); //zz
		       }
		       
		    }
	      }
            
	    }
//                 cnt++;
//         }
    
        
    }
    
    free(Nidx);
     
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{    
  if (nrhs>4)
    {
    if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
        _mexFunction<double> (nlhs, plhs,nrhs, prhs);
    else
        _mexFunction<float> (nlhs, plhs,nrhs, prhs);
    }
  else 
    mexErrMsgTxt("error: unsupported data type\n");
}

