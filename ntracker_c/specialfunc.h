#ifndef SPECIALFUNC_H
#define SPECIALFUNC_H


// #include "gsl/gsl_sf_legendre.h"

// #include "sta_mex_helpfunc.h"
#include "mhs_graphics.h"


template<typename T>
class fast_func_mexp
{
  public :
  static T f(const T & v){return std::exp(-v);};
  
  static T f_min(const T & v){return std::numeric_limits<T>::max();};
  static T f_max(const T & v){return T(0);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_mexp<T>::min_value=T(-150);

template<typename T>
const  T fast_func_mexp<T>::max_value=T(150);

template<typename T>
const  std::size_t fast_func_mexp<T>::accuracy=10000;




template<typename T>
class fast_func_sqrt
{
  public :
  static T f(const T & v){return std::sqrt(v);};
  
  static T f_min(const T & v){return std::sqrt(v);};
  static T f_max(const T & v){return std::sqrt(v);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_sqrt<T>::min_value=T(1);

template<typename T>
const  T fast_func_sqrt<T>::max_value=T(100000);

template<typename T>
const  std::size_t fast_func_sqrt<T>::accuracy=10000;



/*

template<typename T,int l,int m>
class fast_func_sf_legendre_sphPlm
{
  public :
  static T f(const T & v){return std::sqrt(4.0*M_PI/(2.0*l+1.0))*gsl_sf_legendre_sphPlm (l, m, std::cos(v));};
  
  static T f_min(const T & v){T v2=0;printf("fast_func_sf_legendre_sphPlm: %f -> %f\n",v,v2); return f(v2);};
  static T f_max(const T & v){T v2=M_PI;printf("fast_func_sf_legendre_sphPlm: %f -> %f\n",v,v2);  return f(v2);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T,int l,int m>
const  T fast_func_sf_legendre_sphPlm<T,l,m>::min_value=T(0);

template<typename T,int l,int m>
const  T fast_func_sf_legendre_sphPlm<T,l,m>::max_value=T(4*M_PI);

template<typename T,int l,int m>
const  std::size_t fast_func_sf_legendre_sphPlm<T,l,m>::accuracy=10000;*/

template<typename T,unsigned int n>
class fast_func_pow
{
  public :
  static T f(const T & v){return std::pow(v,n);};
  
  static T f_min(const T & v){return f(v);};
  static T f_max(const T & v){return f(v);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T,unsigned int n>
const  T fast_func_pow<T,n>::min_value=T(-10);

template<typename T,unsigned int n>
const  T fast_func_pow<T,n>::max_value=T(10);

template<typename T,unsigned int n>
const  std::size_t fast_func_pow<T,n>::accuracy=10000;


template<typename T>
class fast_func_cos
{
  public :
  static T f(const T & v){return std::cos(v);};
  
  static T f_min(const T & v){T v2=std::abs(v);printf("fast_func_cos: %f -> %f\n",v,v2); return f(v2);};
  static T f_max(const T & v){T v2=v-2*M_PI;printf("fast_func_cos: %f -> %f\n",v,v2);  return f(v2);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_cos<T>::min_value=T(0);

template<typename T>
const  T fast_func_cos<T>::max_value=T(2*M_PI);

template<typename T>
const  std::size_t fast_func_cos<T>::accuracy=10000;


template<typename T>
class fast_func_sin
{
  public :
  static T f(const T & v){return std::sin(v);};
  
  static T f_min(const T & v){T v2=-std::abs(v);;printf("fast_func_cos: %f -> %f\n",v,v2); return f(v2);};
  static T f_max(const T & v){T v2=v-2*M_PI;printf("fast_func_cos: %f -> %f\n",v,v2);  return  f(v2);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_sin<T>::min_value=T(0);

template<typename T>
const  T fast_func_sin<T>::max_value=T(2*M_PI);

template<typename T>
const  std::size_t fast_func_sin<T>::accuracy=10000;



template<typename T>
class fast_func_acos
{
  public :
  static T f(const T & v){return std::acos(v);};
  
  static T f_min(const T & v){printf("fast_func_acos: %f \n",v); return f(-1);};
  static T f_max(const T & v){printf("fast_func_acos: %f \n",v); return f(1);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_acos<T>::min_value=T(-1);

template<typename T>
const  T fast_func_acos<T>::max_value=T(1);

template<typename T>
const  std::size_t fast_func_acos<T>::accuracy=10000;


template<typename T>
class fast_func_atan
{
  public :
  static T f(const T & v){return std::atan(v);};
  
  static T f_min(const T & v){printf("fast_func_atan: %f \n",v);return f(-M_PI/2);};
  static T f_max(const T & v){printf("fast_func_atan: %f \n",v); return f(M_PI/2);};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_atan<T>::min_value=T(-M_PI/2);

template<typename T>
const  T fast_func_atan<T>::max_value=T(M_PI/2);

template<typename T>
const  std::size_t fast_func_atan<T>::accuracy=10000;


template<typename T>
class fast_func_erf
{
  public :
  static T f(const T & v){return std::erf(v);};
  
  static T f_min(const T & v){printf("fast_func_erf: %f \n",v);return 0;};
  static T f_max(const T & v){printf("fast_func_erf: %f \n",v); return 1;};
  static const  T min_value;
  static const  T max_value;
  static const  std::size_t accuracy;
};


template<typename T>
const  T fast_func_erf<T>::min_value=T(0);

template<typename T>
const  T fast_func_erf<T>::max_value=T(5);

template<typename T>
const  std::size_t fast_func_erf<T>::accuracy=10000;


template<typename T,class Func>
T fast_func(T v)
{
    const T min_value=Func::min_value;
    const T max_value=Func::max_value;
    static bool doinit=true;
    const std::size_t accuracy=Func::accuracy;
    static T X[accuracy];    
    static T Y[accuracy];    
    const T range=max_value-min_value;
    
//     if ((v<min_value)||(!(v<max_value)))
//         return (Func::f(v));
    if (v<min_value)
        return (Func::f_min(v));
    if (!(v<max_value))
        return (Func::f_max(v));

    if (doinit)
    {
	
//        printf("init static vars @%u\n",X);
        for (std::size_t i=0; i<accuracy; i++)
        {
            T value=range*T(i)/accuracy+min_value;
	    X[i]=value;
	    Y[i]=(Func::f(value));
        }
        doinit=false;
    }
    
    std::size_t index=std::floor(accuracy*(v-min_value)/range);
    sta_assert_error_c(!(index<0),printf("index: %d\n",index));
    sta_assert_error_c((index)<accuracy,printf("index: %d %d\n",index,accuracy));
    
    
    if ((index+1)<accuracy)
    {
      T w=(v-X[index])/(X[index+1]-X[index]);
      return w*Y[index+1]+(1-w)*Y[index];	
    }
    return Y[index];	
}




template<typename T>
class mhs_fast_math
{
  public:
  static T mexp(const T & v){return fast_func<T,fast_func_mexp<T> >(v);};
  static T sqrt(const T & v){return fast_func<T,fast_func_sqrt<T> >(v);};
  static T cos(const T & v){return fast_func<T,fast_func_cos<T> >(std::abs(v));};
  static T acos(const T & v){return fast_func<T,fast_func_acos<T> >(v);};
  static T atan(const T & v){return fast_func<T,fast_func_atan<T> >(v);};
  static T sin(const T & v){return (v < 0 ? (-1) : 1)*fast_func<T,fast_func_sin<T> >(v < 0 ? -v :v);};
  
  static T atan2(const T & v){return fast_func<T,fast_func_atan<T> >(v);};
  static T erf(const T & v){return   (v < 0 ? (-1) : 1)*fast_func<T,fast_func_erf<T> >(v < 0 ? -v :v);};
  
  
  
  
  static T pow(const T & v,unsigned int n)
  {
    
      switch (n)
      {
	case 0:
	  return 1;
	case 1:
	  return v;
	case 2:
	  return  fast_func<T,fast_func_pow<T,2> >(v);
	case 3:
	  return  fast_func<T,fast_func_pow<T,3> >(v);
	case 4:
	  return  fast_func<T,fast_func_pow<T,4> >(v);    
	case 5:
	  return  fast_func<T,fast_func_pow<T,5> >(v);    
	case 6:
	  return  fast_func<T,fast_func_pow<T,6> >(v);    
	case 7:
	  return  fast_func<T,fast_func_pow<T,7> >(v);    
	case 8:
	  return  fast_func<T,fast_func_pow<T,8> >(v);    
	case 9:
	  return  fast_func<T,fast_func_pow<T,9> >(v);    
	case 10:
	  return  fast_func<T,fast_func_pow<T,10> >(v);
	case 20:
	  return  fast_func<T,fast_func_pow<T,20> >(v);
	case 30:
	  return  fast_func<T,fast_func_pow<T,30> >(v);        
	case 40:
	  return  fast_func<T,fast_func_pow<T,40> >(v);
	case 50:
	  return  fast_func<T,fast_func_pow<T,50> >(v);          
	case 60:
	  return  fast_func<T,fast_func_pow<T,60> >(v);          
	case 70:
	  return  fast_func<T,fast_func_pow<T,70> >(v);          
	case 80:
	  return  fast_func<T,fast_func_pow<T,80> >(v);          
	case 90:
	  return  fast_func<T,fast_func_pow<T,90> >(v);          
	case 100:
	  return  fast_func<T,fast_func_pow<T,100> >(v);          
	default:
	  return std::pow(v,n);
      }
  }
  
  
  
  /*
  
  static T pow2(const T & v){return fast_func<T,fast_func_pow<T,2> >(v);}
  static T pow3(const T & v){return fast_func<T,fast_func_pow<T,3> >(v);}
  static T pow4(const T & v){return fast_func<T,fast_func_pow<T,4> >(v);}
  static T pow5(const T & v){return fast_func<T,fast_func_pow<T,5> >(v);}
  static T pow6(const T & v){return fast_func<T,fast_func_pow<T,6> >(v);}
  static T pow7(const T & v){return fast_func<T,fast_func_pow<T,7> >(v);}
  static T pow8(const T & v){return fast_func<T,fast_func_pow<T,8> >(v);}
  static T pow9(const T & v){return fast_func<T,fast_func_pow<T,9> >(v);}
  static T pow10(const T & v){return fast_func<T,fast_func_pow<T,10> >(v);}
  
  static T pow(const T & v,unsigned int n)
  {
    
    
//     printf("[ %f  ]: %d\n",v,n);
    
      switch (n)
      {
	case 0:
	{
	  return 1;
	} break;
	case 1:
	{
	  return v;
	} break;
	case 2:
	{
	  return  pow2(v);
	} break;	  
	case 3:
	{
	  return   pow3(v);
	} break;
	case 4:
	{
	  return   pow4(v);
	} break;	  
	case 5:
	{
	  return   pow5(v);
	} break;	  
	case 6:
	{
	  return   pow6(v);
	} break;	  
	case 7:
	{
	  return   pow7(v);
	} break;	  
	case 8:
	{
	  return   pow8(v);
	} break;	  
	case 9:
	{
	  return   pow9(v);
	} break;	  
 	case 10:
	  return  fast_func<T,fast_func_pow<T,10> >(v);
	case 20:
	  return  fast_func<T,fast_func_pow<T,20> >(v);
	case 30:
	  return  fast_func<T,fast_func_pow<T,30> >(v);        
	case 40:
	  return  fast_func<T,fast_func_pow<T,40> >(v);
	case 50:
	  return  fast_func<T,fast_func_pow<T,50> >(v);          
	case 60:
	  return  fast_func<T,fast_func_pow<T,60> >(v);          
	case 70:
	  return  fast_func<T,fast_func_pow<T,70> >(v);          
	case 80:
	  return  fast_func<T,fast_func_pow<T,80> >(v);          
	case 90:
	  return  fast_func<T,fast_func_pow<T,90> >(v);          
	case 100:
	  return  fast_func<T,fast_func_pow<T,100> >(v);       
	default:
	  return std::pow(v,n);
      }
  }*/
  
//   static T plm(const T & v,int l,int m){
//     switch (l)
//     {
//       case 0:
// 	return fast_func<T,fast_func_sf_legendre_sphPlm<T,0,0> >(v);
//       case 2:
// 	switch (m)
// 	{
// 	  case 0:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,2,0> >(v);
// 	  case 1:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,2,1> >(v);  
// 	  case 2:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,2,2> >(v);  
// 	  default:
// 	  throw mhs::STAError("unsupported m in fast_func_sf_legendre_sphPlm \n");
// 	}
//       case 4:
// 	switch (m)
// 	{
// 	  case 0:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,4,0> >(v);
// 	  case 1:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,4,1> >(v);  
// 	  case 2:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,4,2> >(v);
// 	  case 3:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,4,3> >(v);  
// 	  case 4:
// 	    return fast_func<T,fast_func_sf_legendre_sphPlm<T,4,4> >(v);  
// 	  default:
// 	  throw mhs::STAError("unsupported m in fast_func_sf_legendre_sphPlm \n");
// 	}
//       default:
// 	throw mhs::STAError("unsupported l in fast_func_sf_legendre_sphPlm \n");
// 	}
//       };
//       
// //     T fast_atan2(T y, T x)
// //     {
// //       
// //     }
//   
//   
//     
//   static void fast_basis_SphericalHarmonics(int l, int m, T theta,T phi, T & real, T & imag)
//   {
//     try{
//       bool sym=false;
//       if (m<0) {
// 	  sym=true;
// 	  m*=-1;
//       }
//       T legendre=mhs_fast_math<T>::plm(theta,l,m);
//       
// //       printf("%f %f\n",theta,phi);
//       
//       if (sym)
//       {
// 	if (m%2==0)
// 	{
// 	  real=legendre*mhs_fast_math<T>::cos(m*phi);
// 	  imag=-legendre*mhs_fast_math<T>::sin(m*phi);
// 	}else
// 	{
// 	  real=-legendre*mhs_fast_math<T>::cos(m*phi);
// 	  imag=legendre*mhs_fast_math<T>::sin(m*phi);
// 	}
//       }else
//       {
// 	real=legendre*mhs_fast_math<T>::cos(m*phi);
// 	imag=legendre*mhs_fast_math<T>::sin(m*phi);
//       }
//     }catch (mhs::STAError error)
//     {
//       throw error;
//     }
//   }
  

  
};







template<typename T>
T mexp2(T v)
{
//     return std::exp(-v);
  
  
    static bool doinit=true;
    static const std::size_t accuracy=10000000;//std::numeric_limits<std::size_t>::max()
    static const T min=-50;
    static const T max=30;
    static const T range=max-min;
    static T data[accuracy];
    if (v<min)
        return std::numeric_limits<T>::max();
    if (v>=max)
        return 0;

    if (doinit)
    {
        printf("init exp ... ");
        for (std::size_t i=0; i<accuracy; i++)
        {
            T value=range*T(i)/accuracy+min;
            data[i]=std::exp(-value);
        }
        printf("done\n");
        doinit=false;
    }
    std::size_t index=std::floor(accuracy*(v-min)/range);
    //if ((index<0)||(index>=accuracy))
    if (index>=accuracy)
    {
      mhs::STAError e;
      e<<"out of bounce (mexp) 0:"<<index<<":"<<accuracy<<" for value "<<v;
      throw e;
    }
    
    return data[index];
}





template<typename T,int Dim>
T normal_dist(const T pos[], T sigma, T mue=0)
{

    T norm=1/std::sqrt(std::pow(sigma*sigma*(2*M_PI),Dim));
    //T norm=1/(sigma*std::sqrt(2*M_PI));

    T v=0;
    T tmp;
    for (int i=0; i<Dim; i++)
    {
        tmp=(pos[i]-mue);
        v+=tmp*tmp;
    }
    
    v=norm*mhs_fast_math<T>::mexp(v/(2*sigma*sigma));
    return std::min(T(1),v);
//   return norm*std::exp(-v/(2*sigma*sigma));
}

template<typename T>
T normal_dist(T x,T y,T z, T sx, T sy, T sz)
{
	sx*=2*sx;
	sy*=2*sy;
	sz*=2*sz;
       T norm=M_PI*M_PI*M_PI;
       norm*=sx*sy*sz;
       
       T g=1.0/std::sqrt(norm)*
 			    mhs_fast_math<T>::mexp( (x*x)/(sx)
				  +(y*y)/(sy)
				  +(z*z)/(sz)
 			    );
	return std::min(T(1),g);
}


template<typename T>
T normal_dist(const Vector<T,3> & pos,T s)
{
	s*=2*s;
       T norm=M_PI*M_PI*M_PI;
       norm*=s*s*s;
       
       const T & x=pos[0];
       const T & y=pos[1];
       const T & z=pos[2];
       
       T g=1.0/std::sqrt(norm)*
 			    mhs_fast_math<T>::mexp( (x*x)/(s)
				  +(y*y)/(s)
				  +(z*z)/(s)
 			    );
	return g;
}

template<typename T,int Dim>
T normal_dist(const Vector<T,3> & x,const Vector<T,3> & n1,T s1, T s2)
{

  


  
    s1*=2*s1;
    s2*=2*s2;
  
    T l=1/std::sqrt(M_PI*M_PI*M_PI*(s1*s2*s2));

    s1=1/(s1);
    s2=1/(s2);
    
//     Vector<T,3> n2;
//     Vector<T,3> n3;
//     mhs_graphics::createOrths(n1,n2,n3); 

    const T & x1=x[0];        
    const T & x2=x[1];        
    const T & x3=x[2];        

    const T & n1x=n1[0];        
    const T & n1y=n1[1];        
    const T & n1z=n1[2];        
	    
//     const T & n2x=n2[1];        
//     const T & n2y=n2[2];        
//     const T & n2z=n2[3];        
// 
//     const T & n3x=n3[1];        
//     const T & n3y=n3[2];        
//     const T & n3z=n3[3];        
    

    T n1yx2n1zx3_2=(n1y*x2+n1z*x3);
    n1yx2n1zx3_2*=n1yx2n1zx3_2;
    
    T G=(
    n1x*n1x*(s1-s2)*x1*x1+
    2*n1x*(s1-s2)*x1*(n1y*x2+n1z*x3)+
    s1*n1yx2n1zx3_2+
    s2*(
      x1*x1+x2*x2-n1y*n1y*x2*x2-2*n1y*n1z*x2*x3+x3*x3-n1z*n1z*x3*x3)
    );    
    
//     G2=(...
// n1x.^2*(s1-s2).*x1.^2+...
// 2*n1x.*(s1-s2).*x1.*(n1y.*x2+n1z.*x3)+...
// s1.*(n1y.*x2+n1z.*x3).^2+...
// s2.*(x1.^2+x2.^2-n1y.^2*x2.^2-2.*n1y.*n1z.*x2.*x3+...
// x3.^2-n1z.^2.*x3.^2));

    G=l*mhs_fast_math<T>::mexp(G);
    /*
    T G=l*mhs_fast_math<T>::mexp((
    n1x*n1x*s1*x1*x1+
    n2x*n2x*s2*x1*x1+
    n3x*n3x*s2*x1*x1+
    2*n3x*n3y*s2*x1*x2+
    n1y*n1y*s1*x2*x2+
    n2y*n2y*s2*x2*x2+
    n3y*n3y*s2*x2*x2+
    2*n3x*n3z*s2*x1*x3+
    2*n1y*n1z*s1*x2*x3+
    2*n2y*n2z*s2*x2*x3+
    2*n3y*n3z*s2*x2*x3+
    n1z*n1z*s1*x3*x3+
    n2z*n2z*s2*x3*x3+
    n3z*n3z*s2*x3*x3+
    2*n1x*s1*x1*(n1y*x2+n1z*x3)+
    2*n2x*s2*x1*(n2y*x2+n2z*x3)));*/
    
    return G;

}
 


template<typename T>
T normal_dist_sphere2(const Vector<T,3> & m,const  Vector<T,3> & n, T sigma)
{

    T dot=(m.dot(n));
    T g = mhs_fast_math<T>::mexp((1- dot*dot)/(2*sigma*sigma));
    T sign=1;
    if (dot<0)
        sign=-1;

    

    T v=(1+sign*std::erf(std::abs(dot)/std::sqrt(2)/sigma))* std::sqrt(M_PI/2)* sigma* g;

    // TODO simplify
    T norm=1/std::sqrt(std::pow(sigma*sigma*(2*M_PI),3));
    return norm*v;
}

template<typename T>
T normal_dist_sphere(const Vector<T,3> & m,const  Vector<T,3> & n, T sigma)
{

    T dot=(m.dot(n));
    T g = mhs_fast_math<T>::mexp((1- dot*dot)/(2*sigma*sigma));
    
    

    T v=(1+mhs_fast_math<T>::erf(dot/std::sqrt(2)/sigma))* std::sqrt(M_PI/2)* sigma* g;

    // TODO simplify
    T norm=1/mhs_fast_math<T>::sqrt(std::pow(sigma*sigma*(2*M_PI),3));
    return norm*v;
}

template<typename T>
T normal_dist_sphere_full(const Vector<T,3> & m,const  Vector<T,3> & n, T sigma)
{

    T dot=(m.dot(n));
    T g = mhs_fast_math<T>::mexp((1- dot*dot)/(2*sigma*sigma));

    T v= std::sqrt(M_PI/2)* sigma* g;

    // TODO simplify
    T norm=1/std::sqrt(std::pow(sigma*sigma*(2*M_PI),3));
    return norm*v;
}


template<typename T>
void compute_inner_circle(
  const Vector<T,3> & endpoint_a, // seitenhalbierende pos
  const Vector<T,3> & endpoint_b,
  const Vector<T,3> & endpoint_c,
  T a, //seiten laenge
  T b,
  T c,
  Vector<T,3> & center,
  T & radius,
  bool debug=false)
{
  T abc=a+b+c;
  T la=(-a+b+c)/abc;
  T lb=(a-b+c)/abc;
  T lc=(a+b-c)/abc;
  center=endpoint_a*la+endpoint_b*lb+endpoint_c*lc;
  if (debug)
  {
   printf("compute_inner_circle:\n"); 
   printf("%f %f %f\n",la,lb,lc); 
   printf("%f\n",abc); 
   endpoint_a.print();
   endpoint_b.print();
   endpoint_c.print();
   printf("%f %f %f\n",a,b,c); 
   center.print();
  }
  
  radius=std::sqrt(la*lb*lc*abc*abc/4);
}



template <typename T,typename TData>
T interp_from_samples(const T & s,
		const TData * S,
                const TData * X,
                std::size_t num_samples,
		bool quad=false,
		bool debug=false
 		    )
{
   
    
    int offset=-1;
    T r;
    for (std::size_t i=0;i<num_samples-1;i++)
    {
      if ((!(s+std::numeric_limits<T>::epsilon()<S[i]))&&(!(s-std::numeric_limits<T>::epsilon()>S[i+1])))
      {
	offset=i;
	break;
      }
    }
    
    if (offset<0)
    {
     printf("%d %d / %f %f %f\n",offset,num_samples,S[0],s,S[num_samples-1] );
    }
    
    sta_assert_error(offset>-1);
    
     sta_assert_error(offset<num_samples);//,(printf("%d %d / %f %f %f\n",offset,num_samples,S[0],s,S[num_samples-1])));
     
     
//     r=X[offset];
//     return r;
    
    if ((!quad)||(num_samples<3))
    {
      T w=(s-S[offset])/(S[offset+1]-S[offset]);
    
//       printf("%f \n",w);
      r=w*X[offset+1]+(1-w)*X[offset];
      
      if (debug)
	{
	  printf("%f, %f %f\n",w,(X[offset+1]),X[offset]);
	}
       //r=X[offset];
    }else 
    {
      if (offset+3>num_samples)
	offset--;
      
      sta_assert_error(offset+2<num_samples);
      const TData & A0=S[offset];
      const TData & B0=X[offset];
      const TData & A1=S[offset+1];
      const TData & B1=X[offset+1];
      const TData & A2=S[offset+2];
      const TData & B2=X[offset+2];
      
      T a = A2 * (B1-B0) + A1 * (B0-B2) + A0*(B2-B1);
	a/= (A0-A1) * (A0-A2)  * (A1-A2) + std::numeric_limits<T>::epsilon();
      T b = A2 * A2 * (B1-B0) + A1 * A1 * (B0-B2) + A0 * A0 * (B2-B1);
	b/= (A0-A1) * (A0-A2)  * (A2-A1)+ std::numeric_limits<T>::epsilon();
      T c = A0 * (A0-A2)* A2 *B1 + A1 *A1 * (A0 * B2 - A2 * B0) + A1 * (A2* A2 *B0 - A0 *A0 *B2);
	c/= (A0-A1) * (A0-A2)  * (A2-A1)+ std::numeric_limits<T>::epsilon();		
	

	if (debug)
	{
	    printf("[%f %f],[%f %f],[%f %f]\n",
	      a*A0*A0+b*A0+c,B0,
	      a*A1*A1+b*A1+c,B1,
	      a*A2*A2+b*A2+c,B2
	      );
	}
      r=a*s*s+b*s+c;
      
    }
    return r;
}


template <typename T,typename TData>
T interp_from_samplesDir(
		const T & s,
		const Vector<T,3> & dir,
		const TData * S,
                const TData * X,
                std::size_t num_samples,
		bool quad=false,
		bool debug=false
 		    )
{
   
    
    int offset=-1;
    T r;
    for (std::size_t i=0;i<num_samples-1;i++)
    {
      if ((!(s+std::numeric_limits<T>::epsilon()<S[i]))&&(!(s-std::numeric_limits<T>::epsilon()>S[i+1])))
      {
	offset=i;
	break;
      }
    }
    
    if (offset<0)
    {
     printf("%d %d / %f %f %f\n",offset,num_samples,S[0],s,S[num_samples-1] );
    }
    
    sta_assert_error(offset>-1);
    
    sta_assert_error(offset<num_samples);
     
    
    if ((!quad)||(num_samples<3))
    {
      T w=(s-S[offset])/(S[offset+1]-S[offset]);
    
      Vector<T,3> f_response;
      T f[2];
      
      for (int p=0;p<2;p++)
      {
	for (int i=0;i<3;i++)
	{
	  f_response[2-i]=X[(offset+p)*3+i];
	}
	      
	f[p]=f_response.dot(dir);
	if (debug)
	{
	 printf("-----\n");
	 printf("%f\n",w);
	 printf("%d\n",offset);
	 f_response.print();
	 dir.print();
	 printf("%f\n",std::sqrt(f_response.norm2()));
	 printf("%f\n",std::sqrt(dir.norm2()));
	}
      }
      
      
      
            
      r=w*(f[1]*f[1])+(1-w)*(f[0]*f[0]);
    }else 
    {
      if (offset+3>num_samples)
	offset--;
      
      sta_assert_error(offset+2<num_samples);
      
      Vector<T,3> f_response;
      T f[3];
      
      for (int p=0;p<3;p++)
      {
	for (int i=0;i<3;i++)
	{
	  f_response[2-i]=X[(offset+p)*3+i];
	}
	      
	f[p]=f_response.dot(dir);
      }
      
      
      const TData  A0=f[0]*f[0];
      const TData  A1=f[1]*f[1];
      const TData  A2=f[2]*f[2];
      
      const TData & B0=X[offset];
      const TData & B1=X[offset+1];
      const TData & B2=X[offset+2];
      
      T a = A2 * (B1-B0) + A1 * (B0-B2) + A0*(B2-B1);
	a/= (A0-A1) * (A0-A2)  * (A1-A2) + std::numeric_limits<T>::epsilon();
      T b = A2 * A2 * (B1-B0) + A1 * A1 * (B0-B2) + A0 * A0 * (B2-B1);
	b/= (A0-A1) * (A0-A2)  * (A2-A1)+ std::numeric_limits<T>::epsilon();
      T c = A0 * (A0-A2)* A2 *B1 + A1 *A1 * (A0 * B2 - A2 * B0) + A1 * (A2* A2 *B0 - A0 *A0 *B2);
	c/= (A0-A1) * (A0-A2)  * (A2-A1)+ std::numeric_limits<T>::epsilon();		
	

	if (debug)
	{
	    printf("[%f %f],[%f %f],[%f %f]\n",
	      a*A0*A0+b*A0+c,B0,
	      a*A1*A1+b*A1+c,B1,
	      a*A2*A2+b*A2+c,B2
	      );
	}
      r=a*s*s+b*s+c;
      
    }
    return r;
}




template <typename T,typename TData>
T interp_from_samples_scalespace(
		const T & s,
		const T * weights,
		const TData * S,
                const TData ** X,
                std::size_t num_samples,
		bool quad=false,
		bool debug=false
 		    )
{
   
    
    int offset=-1;
    T r;
    for (std::size_t i=0;i<num_samples-1;i++)
    {
      if ((!(s+std::numeric_limits<T>::epsilon()<S[i]))&&(!(s-std::numeric_limits<T>::epsilon()>S[i+1])))
      {
	offset=i;
	break;
      }
    }
    
    if (offset<0)
    {
     printf("%d %d / %f %f %f\n",offset,num_samples,S[0],s,S[num_samples-1] );
    }
    
    sta_assert_error(offset>-1);
    
     sta_assert_error(offset<num_samples);//,(printf("%d %d / %f %f %f\n",offset,num_samples,S[0],s,S[num_samples-1])));
     
     
//     r=X[offset];
//     return r;
    
    if ((!quad)||(num_samples<3))
    {
      T w=(s-S[offset])/(S[offset+1]-S[offset]);
    
//       printf("%f \n",w);
      r=w*(*X[offset+1])*weights[offset+1]+(1-w)*(*X[offset])*weights[offset];
      if (debug)
	{
	  printf("%f, %f %f , %f %f\n",w,(*X[offset+1]),*X[offset],weights[offset+1],weights[offset]);
	}
       //r=X[offset];
    }else 
    {
      if (offset+3>num_samples)
	offset--;
      
      sta_assert_error(offset+2<num_samples);
      const TData & A0=S[offset];
      const TData & B0=*X[offset]*weights[offset];
      const TData & A1=S[offset+1];
      const TData & B1=*X[offset+1]*weights[offset+1];
      const TData & A2=S[offset+2];
      const TData & B2=*X[offset+2]*weights[offset+2];
      
      T a = A2 * (B1-B0) + A1 * (B0-B2) + A0*(B2-B1);
	a/= (A0-A1) * (A0-A2)  * (A1-A2) + std::numeric_limits<T>::epsilon();
      T b = A2 * A2 * (B1-B0) + A1 * A1 * (B0-B2) + A0 * A0 * (B2-B1);
	b/= (A0-A1) * (A0-A2)  * (A2-A1)+ std::numeric_limits<T>::epsilon();
      T c = A0 * (A0-A2)* A2 *B1 + A1 *A1 * (A0 * B2 - A2 * B0) + A1 * (A2* A2 *B0 - A0 *A0 *B2);
	c/= (A0-A1) * (A0-A2)  * (A2-A1)+ std::numeric_limits<T>::epsilon();		
	

	if (debug)
	{
	    printf("[%f %f],[%f %f],[%f %f]\n",
	      a*A0*A0+b*A0+c,B0,
	      a*A1*A1+b*A1+c,B1,
	      a*A2*A2+b*A2+c,B2
	      );
	}
      r=a*s*s+b*s+c;
      
    }
    return r;
}


template <typename T,typename TData>
T eval_polynom1(const T & x1,
                const TData * alphas,
                int mode=4)
{

    T r;

    const TData * & a=alphas;


    switch (mode)
    {
      
    case 0: // 2
    {
      r=a[0];
    }break;
      
      
    case 1: // 2
    {
      r=a[0]+a[1]*x1;
    }break;
  
      
    case 2: // 3
    {
      T x2=x1*x1;

      r=a[0]+a[1]*x1+a[2]*x2;
    }break;
    
    case 3: // 4
    {
       T x2=x1*x1;
       T x3=x2*x1;

       r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3;
    }break;

    case 4: // 5
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;

        r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3+a[4]*x4;
// 	printf("alphas: %f %f %f %f %f\n",a[0],a[1],a[2],a[3],a[4]);
//  	printf("x: %f %f %f %f %f\n",1.0f,x1,x2,x3,x4);
    }break;
    case 5: // 6
    {
	T x2=x1*x1;
	T x3=x2*x1;
	T x4=x3*x1;
	T x5=x4*x1;

	r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3+a[4]*x4+a[5]*x5;      
	
    }break;
    
    case 6: // 6
    {
	T x2=x1*x1;
	T x3=x2*x1;
	T x4=x3*x1;
	T x5=x4*x1;
	T x6=x5*x1;

	r=a[0]+a[1]*x1+a[2]*x2+a[3]*x3+a[4]*x4+a[5]*x5+a[6]*x6;      
	
    }break;
    
    default:
      printf("degree is unsupported\n");
    }

    return r;
}

template <typename T,typename TData,int Dim>
T eval_polynom2(Vector<T,Dim>& vector,
                const TData * alphas,
                int mode=4)
{
    sta_assert_error(Dim==2);
    T r;

    const TData * & a=alphas;
    const T & x1=vector.v[1];
    const T & y1=vector.v[0];

    switch (mode)
    {


    case 2: // 4
    {
        T x2=x1*x1;
        T y2=y1*y1;

        r=a[0]+a[1]*x2+a[2]*x1*y1+a[3]*y2;
    }
    break;

    case 4: //9
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x1*y1+a[4]*x3*y1+a[5]*y2+a[6]*x2*y2+a[7]*x1*y3+a[8]*y4;
    }
    break;

    case 6: //16
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T x5=x4*x1;
        T x6=x5*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T y5=y4*y1;
        T y6=y5*y1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x1*y1+a[5]*x3*y1+a[6]*x5*y1+a[7]*y2+a[8]*x2*y2+a[9]*x4*y2+a[10]*x1*y3+a[11]*x3*y3+a[12]*y4+a[13]*x2*y4+a[14]*x1*y5+a[15]*y6;
    }
    break;

    case 8: //25
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T x5=x4*x1;
        T x6=x5*x1;
        T x7=x6*x1;
        T x8=x7*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T y5=y4*y1;
        T y6=y5*y1;
        T y7=y6*y1;
        T y8=y7*y1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x8+a[5]*x1*y1+a[6]*x3*y1+a[7]*x5*y1+a[8]*x7*y1+a[9]*y2+a[10]*x2*y2+a[11]*x4*y2+a[12]*x6*y2+a[13]*x1*y3+a[14]*x3*y3+a[15]*x5*y3+a[16]*y4+a[17]*x2*y4+a[18]*x4*y4+a[19]*x1*y5+a[20]*x3*y5+a[21]*y6+a[22]*x2*y6+a[23]*x1*y7+a[24]*y8;
    }
    break;
    
     default:
      printf("degree is unsupported\n");
    
    }

    return r;
}



template <typename T,typename TData,int Dim>
T eval_polynom3(Vector<T,Dim>& vector,
                const TData * alphas,
                int mode=4)
{
    sta_assert_error(Dim==3);
    T r;

    const   TData * & a=alphas;
    const T & x1=vector.v[2];
    const T & y1=vector.v[1];
    const T & z1=vector.v[0];

    switch (mode)
    {
    case 4: // 4
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T z2=z1*z1;
        T z3=z2*z1;
        T z4=z3*z1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x1*y1+a[4]*x3*y1+a[5]*y2+a[6]*x2*y2+a[7]*x1*y3+a[8]*y4+a[9]*x1*z1+a[10]*x3*z1+a[11]*y1*z1+a[12]*x2*y1*z1+a[13]*x1*y2*z1+a[14]*y3*z1+a[15]*z2+a[16]*x2*z2+a[17]*x1*y1*z2+a[18]*y2*z2+a[19]*x1*z3+a[20]*y1*z3+a[21]*z4;
    }
    break;

    case 6: // 4
    {
        T x2=x1*x1;
        T x3=x2*x1;
        T x4=x3*x1;
        T x5=x4*x1;
        T x6=x5*x1;
        T y2=y1*y1;
        T y3=y2*y1;
        T y4=y3*y1;
        T y5=y4*y1;
        T y6=y5*y1;
        T z2=z1*z1;
        T z3=z2*z1;
        T z4=z3*z1;
        T z5=z4*z1;
        T z6=z5*z1;

        r=a[0]+a[1]*x2+a[2]*x4+a[3]*x6+a[4]*x1*y1+a[5]*x3*y1+a[6]*x5*y1+a[7]*y2+a[8]*x2*y2+a[9]*x4*y2+a[10]*x1*y3+a[11]*x3*y3+a[12]*y4+a[13]*x2*y4+a[14]*x1*y5+a[15]*y6+a[16]*x1*z1+a[17]*x3*z1+a[18]*x5*z1+a[19]*y1*z1+a[20]*x2*y1*z1+a[21]*x4*y1*z1+a[22]*x1*y2*z1+a[23]*x3*y2*z1+a[24]*y3*z1+a[25]*x2*y3*z1+a[26]*x1*y4*z1+a[27]*y5*z1+a[28]*z2+a[29]*x2*z2+a[30]*x4*z2+a[31]*x1*y1*z2+a[32]*x3*y1*z2+a[33]*y2*z2+a[34]*x2*y2*z2+a[35]*x1*y3*z2+a[36]*y4*z2+a[37]*x1*z3+a[38]*x3*z3+a[39]*y1*z3+a[40]*x2*y1*z3+a[41]*x1*y2*z3+a[42]*y3*z3+a[43]*z4+a[44]*x2*z4+a[45]*x1*y1*z4+a[46]*y2*z4+a[47]*x1*z5+a[48]*y1*z5+a[49]*z6;
    }
    break;

    case 2: // 4
    {
        T x2=x1*x1;
        T y2=y1*y1;
        T z2=z1*z1;

        r=a[0]+a[1]*x2+a[2]*x1*y1+a[3]*y2+a[4]*x1*z1+a[5]*y1*z1+a[6]*z2;
    }
    break;

    default:
        sta_assert_error(1==0);

    }
    return r;
}




template <typename T,typename TData,int Dim>
T eval_polynom(
    Vector<T,Dim>& dir,
    T scale,
    const TData * alphas,
    int numalphas)
{
//Vector<T,Dim> vector=dir*std::sqrt(scale+10) ;
    Vector<T,Dim> vector=dir*scale;
    if (Dim==1)
    {
        int mode=-1;
        switch (numalphas)
        {
	case 2:
            mode=1;
            break;  
	case 3:
            mode=2;
            break;  
	case 4:
            mode=3;
            break;  
        case 5:
            mode=4;
            break;
        }

        return eval_polynom1<T,TData>(vector[0],alphas,mode);
    }

    if (Dim==2)
    {
        int mode=-1;
        switch (numalphas)
        {
        case 4:
            mode=2;
            break;

        case 9:
            mode=4;
            break;

        case 16:
            mode=6;
            break;

        case 25:
            mode=8;
            break;
        }

        return eval_polynom2<T,TData,Dim>(vector,alphas,mode);
    }
    if (Dim==3)
    {

        int mode=-1;
        switch (numalphas)
        {
        case 7:
            mode=2;
            break;
        case 22:
            mode=4;
            break;
        case 50:
            mode=6;
            break;
        }
        return eval_polynom3<T,TData,Dim>(vector,alphas,mode);

    }
    sta_assert_error("eval_polynom");
    return 0;
}


template <typename T,typename TData>
T eval_polynom(T scale,
               const TData * alphas,
               int numalphas)
{

    Vector<T,1> vector;
    vector=scale;
    {
        int mode=-1;
        switch (numalphas)
        {
        case 5:
            mode=4;
            break;
        }

        return eval_polynom1<T,TData,1>(vector,alphas,mode);
    }

}


#endif
