#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"

#include <stdio.h>
#include <stdlib.h>

#include "mhs_vector.h"


#include <SDL2/SDL.h>
#include "SDL2/SDL_opengl.h"
#include "mhs_gui_shader_old.h"
//#include "SDL2/SDL_thread.h"


struct TTracker_window
{
    SDL_Window *tracker_window;
    SDL_GLContext glcontext; 
    CGCprogram  *  _cgprogram;
};

static TTracker_window * tracker_data_p=NULL; 
static TTracker_window tracker_data;





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
    if ((nrhs>0)&&((tracker_data_p!=NULL)))
    {
        const mxArray * params=prhs[nrhs-1] ;

        if ((mhs::mex_hasParam(params,"close")!=-1))
	{
	    SDL_GL_DeleteContext(tracker_data_p->glcontext);  
	      //SDL_Delay(50);
	    SDL_HideWindow(tracker_data_p->tracker_window);
	    SDL_DestroyWindow(tracker_data_p->tracker_window);
	    
	    mxArray *arg1 = mxCreateString("global");
	    mxArray *arg2 = mxCreateString("tracker_window_ptr2");
	    mxArray *pargin[2] = {arg1, arg2};
	    mexCallMATLAB(0, NULL,2,pargin, "clear");
	    
	    delete tracker_data_p->_cgprogram;
	    
	    tracker_data_p=NULL;	    
	 return;     
	}      
    }
  
  
  if (tracker_data_p!=NULL)
  {
    mxArray * handle=mexGetVariable("global", "tracker_window_ptr");
    std::size_t * handle_p=(std::size_t  *)mxGetPr(handle);
    printf("%u %u\n",*handle_p,tracker_data_p);
    
    
    int width;
    int height;
    SDL_GetWindowSize(tracker_data_p->tracker_window,&width,&height);
    printf("%d %d\n",width,height);
    
    if (nrhs>0)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"w")!=-1)
            width=mhs::mex_getParam<int>(params,"w",1)[0];
	if (mhs::mex_hasParam(params,"h")!=-1)
            height=mhs::mex_getParam<int>(params,"h",1)[0];
	
	   printf("%d %d\n",width,height);
	if (width<1)
	  return;
	if (height<1)
	  return;
	SDL_SetWindowSize(tracker_data_p->tracker_window,width,height);
    }
    

    
  }else{
   if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr,
                "\nUnable to initialize SDL:  %s\n",
                SDL_GetError()
               );
        return;
    }
    
     int width=1000;
    int height=1000;    
    if (nrhs>0)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"w")!=-1)
            width=mhs::mex_getParam<int>(params,"w",1)[0];
	if (mhs::mex_hasParam(params,"h")!=-1)
            height=mhs::mex_getParam<int>(params,"h",1)[0];
	
	   printf("%d %d\n",width,height);
	if (width<1)
	  return;
	if (height<1)
	  return;
	//SDL_SetWindowSize(tracker_data_p->tracker_window,width,height);
    }
    
    
    // probably SDL_WINDOW_FOREIGN can fix this crash on exit
    tracker_data.tracker_window = SDL_CreateWindow(
       //"OpenGL Tracker Gui", 0, 0, width, height,
      "Tracker Gui", 0, 0, width, height, 
       SDL_WINDOW_OPENGL);
    
    // Create an OpenGL context associated with the window.
    tracker_data.glcontext = SDL_GL_CreateContext(tracker_data.tracker_window);
    
    tracker_data._cgprogram = new CGCprogram();
    //delete _cgprogram;
    
    tracker_data_p=&tracker_data;
    
    int ndim=1;
    mxArray *handle = mxCreateNumericArray(1,&ndim,mxUINT64_CLASS,mxREAL);
     std::size_t * handle_p=(std::size_t  *)mxGetPr(handle);
     *handle_p=(std::size_t)(tracker_data_p);
    mexPutVariable("global", "tracker_window_ptr", handle );
  }
  
  
  
  /*
   TestThread( (void *)NULL);
  SDL_Thread *thread;
    int         threadReturnValue;

    printf("\nSimple SDL_CreateThread test:");

    // Simply create a thread
    thread = SDL_CreateThread(TestThread, "TestThread", (void *)NULL);

    if (NULL == thread) {
        printf("\nSDL_CreateThread failed: %s\n", SDL_GetError());
    } else {
        SDL_WaitThread(thread, &threadReturnValue);
        printf("\nThread returned value: %d", threadReturnValue);
    }
*/
  
//   SDL_Quit();
  
//     atexit(SDL_Quit);
//   SDL_Log("exiting");
  

}