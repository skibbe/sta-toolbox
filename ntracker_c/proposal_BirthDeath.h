#ifndef PROPOSAL_BIRTHDEATH_H
#define PROPOSAL_BIRTHDEATH_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CBirth : public CProposal<TData,T,Dim>
{
protected:
    
    T lambda;
    T particle_energy_change_costs;
//     bool * Mask;
    
    //mhs::dataArray<TData>  saliency_map_acc;
    
    bool use_saliency_map;
    
//     mhs::dataArray<TData>  saliency_map;
//     double saliency_correction[3];
//     
//     double * saliency_map_acc_tmp;
//     double saliency_map_acc_tmp_max;

    bool temp_dependent;
    
    bool sample_scale_temp_dependent;
    
    bool freeze_manual_particles = false;
    
    
public:

  
      T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_volume(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
    CBirth(): CProposal<TData,T,Dim>()
    {
        use_saliency_map=false;
        lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
/*	
	saliency_map_acc_tmp=NULL;
	saliency_correction[0]=std::numeric_limits< double >::max();
	saliency_correction[1]=std::numeric_limits< double >::min();
	saliency_correction[2]=0;*/
	temp_dependent=true;
	sample_scale_temp_dependent=false;
    }
    
    ~CBirth()
    {
//       if (saliency_map_acc_tmp!=NULL)
// 	delete [] saliency_map_acc_tmp;
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIRTH);
    };
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIRTH);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

        if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];

        if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
	printf("(saliency: %s)",
	use_saliency_map ? "yes" : "no");
	
			if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	    
	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
	  
     
     if (mhs::mex_hasParam(params,"freeze_manual_particles")!=-1)
     {
            freeze_manual_particles=mhs::mex_getParam<bool>(params,"freeze_manual_particles",1)[0];
             printf("##################################\n");
            printf("freeze_manual_particles!\n");
            printf("##################################\n");
     }
      

	    
	    printf("BIRTH DEATH %u\n",lambda);
    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

//         try {
//             saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
//             //saliency_map_acc=mhs::dataArray<TData>(feature_struct,"saliency_map_acc");
//         } catch (mhs::STAError error)
//         {
//             throw error;
//         }
        
        /*
        std::size_t numv=saliency_map.dim[0]*saliency_map.dim[1]*saliency_map.dim[2];
        saliency_map_acc_tmp=new double[numv];
	sta_assert_error(numv>1);
	TData * p_saliency_map=saliency_map.data;
	saliency_map_acc_tmp[0]=*p_saliency_map++;
	for (std::size_t i=1;i<numv;i++)
	  saliency_map_acc_tmp[i]=saliency_map_acc_tmp[i-1]+*p_saliency_map++;
	saliency_map_acc_tmp_max=saliency_map_acc_tmp[numv-1];
	*/
	
	
//         CData<T,TData,Dim> & data_fun=*(this->tracker->data_fun);	
// 	
// 	std::size_t real_shape[3];
// 	std::size_t shape[3];
// 	std::size_t offset[3];
//  	data_fun.get_real_shape(real_shape);
//  	data_fun.getshape(shape);
//  	data_fun.getoffset(offset);
// 	
// // 	data_fun.get_region_not_corrected(real_shape, shape, offset);
// 	
// 	std::size_t numv=shape[0]*shape[1]*shape[2];
//         saliency_map_acc_tmp=new double[numv];
// // 	for (std::size_t i=0;i<numv;i++)
// // 	  saliency_map_acc_tmp[i]=0;
// 	
// 	
// 	sta_assert_error(numv>1);
// 	TData * p_saliency_map=saliency_map.data;
// 	
// 	saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
// 	
// 	for (int z=offset[0];z<offset[0]+shape[0];z++)
// 	{
// 	  for (int y=offset[1];y<offset[1]+shape[1];y++)
// 	  {
// 	    for (int x=offset[2];x<offset[2]+shape[2];x++)
// 	    {
// 	      
// 	      std::size_t index=(z*real_shape[1]+y)*real_shape[2]+x;
// 	      
// 	      saliency_correction[0]=std::min(saliency_correction[0],double(p_saliency_map[index]));
// 	      saliency_correction[1]=std::max(saliency_correction[1],double(p_saliency_map[index]));
// 	      saliency_correction[2]+=p_saliency_map[index];
// 	    }
// 	  }
// 	}
// 
// 	
// 	
// 	
// 	std::size_t count=0;
// 	for (int z=offset[0];z<offset[0]+shape[0];z++)
// 	{
// 	  for (int y=offset[1];y<offset[1]+shape[1];y++)
// 	  {
// 	    for (int x=offset[2];x<offset[2]+shape[2];x++)
// 	    {
// 	      
// 	      std::size_t index=(z*real_shape[1]+y)*real_shape[2]+x;
// 	      
// 	      if (count==0)
// 	      {
// 		saliency_map_acc_tmp[0]=p_saliency_map[index];
// 	      }else
// 	      {
// 		saliency_map_acc_tmp[count]=saliency_map_acc_tmp[count-1]+p_saliency_map[index];
// 	      }
// // 	      if (count==0)
// // 	      {
// // 		saliency_map_acc_tmp[0]=1;
// // 	      }else
// // 	      {
// // 		saliency_map_acc_tmp[count]=saliency_map_acc_tmp[count-1]+1;
// // 	      }
// 	      count++;
// 	    }
// 	  }
// 	}
// 	saliency_map_acc_tmp_max=saliency_map_acc_tmp[numv-1];
	
	
	
        
        
//         try {
// 	  Mask=mhs::dataArray<TData>(feature_struct,"Mask");
// 	} catch (mhs::STAError error)
//         {
// 	  Mask=NULL;
//         }



    }
    
    
    
    bool helper_update_particle_saliency(Points<T,Dim> & point)
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
	
	
        
        T newpointsaliency;
	sta_assert_error(data_fun.saliency_get_value(newpointsaliency,point.get_position()));
	point.saliency=newpointsaliency;
	
    }
    
    bool helper_add_particle(const Vector<T,Dim> & pos)
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			max_collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	
	
        Points<T,Dim> * point=particle_pool.create_obj();
        Points<T,Dim> & cpoint=*point;
	
	
	
//         cpoint.setpoint(pos[0],pos[1],pos[2]);
//        
// 	printf("0");
// //         T new_scale=myrand(minscale,maxscale);
//         cpoint.scale=minscale;
        // orientation
	
	Vector<T,3> direction;
	
        do {
            direction=T(0);
            random_pic_direction<T,Dim>(
                direction,
                direction,
                T(1));
        } while (direction[0]+direction[1]+direction[2]==0);

	cpoint.set_pos_dir_scale(pos,direction,minscale);
	
	
	T collision_search_rad=std::max(cpoint.get_scale(),cpoint.get_thickness())+std::max(maxscale,cpoint.predict_thickness_from_scale(maxscale))+0.01;
	sta_assert_debug0(!(max_collision_search_rad<collision_search_rad));
				       
	printf("1");
	
	T dummy_costs=0;
	{
	  if (collision_fun_p.colliding(dummy_costs, tree,cpoint,collision_search_rad,temp))
	  {
	      particle_pool.delete_obj(point);
	      return false;
	  }
	}
	
	printf("2");
	
        T newpointsaliency=0;
//         if (!data_fun.interp3(newpointsaliency,
//                      saliency_map.data,
//                      cpoint.position,
//                      shape))
//         {
//             particle_pool.delete_obj(point);
//             return false;
//         }
	if ((!data_fun.saliency_get_value(newpointsaliency,cpoint.get_position()))|| (newpointsaliency<std::numeric_limits<T>::min()))
        {
            particle_pool.delete_obj(point);
            return false;
        }
        
//         if (newpointsaliency<std::numeric_limits<T>::epsilon())
//         {
//             particle_pool.delete_obj(point);
//             return false;
//         }
        
        printf("3");

        T newenergy_data;
// 		T newpointsaliency;
        if (!data_fun.eval_data(
                    newenergy_data,
		    cpoint))
//                     cpoint.get_direction(),
//                     cpoint.get_position(),
//                     cpoint.get_scale()))
        {
            particle_pool.delete_obj(point);
            return false;
        }
        
        printf("4");

        cpoint.point_cost=newenergy_data;
//         cpoint.saliency=newpointsaliency/saliency_correction[2];
	cpoint.saliency=newpointsaliency;
	
    
    if (freeze_manual_particles)
    {
        cpoint.freezed = true;
    }
	
	
	if (!tree.insert(*point))
	{
	    particle_pool.delete_obj(point);
// 	   /* point->print();
// 	    throw mhs::STAError("error insering point (collision)\n");*/
	    return false;
	}
	
	printf("5");
	if (!conn_tree.insert(*point))
	{
	    particle_pool.delete_obj(point);
// 	    point->print();
// 	    throw mhs::STAError("error insering point (connections)\n");
	    return false;
	}
	
	printf("6");
	return true;
    }
    

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
	//const T & 			no_collision_threshold  =this->tracker->no_collision_threshold
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);

	 this->proposal_called++;
	
        
	 if (sample_scale_temp_dependent && (!(temp<maxscale)))
	 {
	      return false; 
	 }

        Points<T,Dim> * point=particle_pool.create_obj();
        Points<T,Dim> & cpoint=*point;
	
	

	
	Vector<T,Dim> new_pos;
	
        if (use_saliency_map)
        {
            //sta_assert_error(saliency_map_acc.data!=NULL);
// 	  sta_assert_error(saliency_map_acc_tmp!=NULL);
//             T pos[3];
//             //T v= rand_pic_position(saliency_map_acc.data,shape,pos,center,1,0,true);
// 	    T v= rand_pic_position(saliency_map_acc_tmp,shape,pos,center,1,0,true,saliency_map_acc_tmp_max);
// 	    cpoint.setpoint(pos[0],pos[1],pos[2]);
	    std::size_t center;
	    
	    if (!data_fun.saliency_draw(new_pos,center))
	    {
	      particle_pool.delete_obj(point);
	      return false;
	    }
	    //cpoint.set_position(new_pos);
	    
            
        }
        else
        {
            //Vector<T,Dim> randv;
            T maxect[3];
            maxect[0]=shape[0]-1;
            maxect[1]=shape[1]-1;
            maxect[2]=shape[2]-1;
            T maxect_min[3];
            maxect_min[0]=maxect_min[1]=maxect_min[2]=0;
	    
            new_pos.rand(maxect_min,maxect);
            //cpoint.set_position(randv);
//             center=(std::floor(cpoint.position[0])*shape[1]
//                     +std::floor(cpoint.position[1]))*shape[2]
//                    +std::floor(cpoint.position[2]);
        }


        /*
        if (return_statistic)
        {
          static_data[center*2*numproposals+4]+=1;
        }
        */

       

        // uniformly pick scale
        //T new_scale=myrand(maxscale-minscale)+minscale;
        
        T new_scale;
	
//         if (temp_dependent)
// 	{
// 	  T sigma=(maxscale-minscale)/2;
// 	  new_scale=mynormalrand(minscale+temp,sigma);
// 	  new_scale=std::min(std::max(new_scale,minscale),maxscale);
// 	}else
// 	{
// 	  new_scale=myrand(minscale,maxscale);  
// 	}
	if (!single_scale)
	{
	  if (sample_scale_temp_dependent)
	  {
	    new_scale=myrand(std::max(minscale,temp),maxscale);
	  }else
	  {
	    new_scale=myrand(minscale,maxscale);
	  }
	}else
	{
	  new_scale=maxscale;
	}
        
	Vector<T,Dim> new_direction;
//         cpoint.scale=new_scale;

	new_direction=T(0);
        // orientation
        do {
            
            new_direction=random_pic_direction<T,Dim>(new_direction,T(1));
        } while ((!(new_direction[0]!=0))||(!(new_direction[1]!=0))||(!(new_direction[2]!=0)));

	
	cpoint.set_pos_dir_scale(new_pos,new_direction,new_scale);
	
 	T collision_finite_costs=0;
// 	if (Points<T,Dim>::collision_two_steps>0)
// 	{
// 	  if (colliding( tree,cpoint,collision_search_rad,collision_finite_costs))
// 	  {
// 	      particle_pool.delete_obj(point);
// 	      return false;
// 	  }	  
// 	}else
	
	T particle_interaction_cost_new=0;
	{
	  if (collision_fun_p.colliding(particle_interaction_cost_new, tree,cpoint,collision_search_rad,temp))
	  {
	      particle_pool.delete_obj(point);
	      return false;
	  }
	}
	
// 	T E=collision_finite_costs/temp;

        T newpointsaliency;
//         if (!data_fun.interp3(newpointsaliency,
//                      saliency_map.data,
//                      cpoint.position,
//                      shape))
	if (!data_fun.saliency_get_value(newpointsaliency,cpoint.get_position()))
        {
            particle_pool.delete_obj(point);
            return false;
        }
        
        
        

        T newenergy_data;
// 		T newpointsaliency;
        if (!data_fun.eval_data(
                    newenergy_data,
		    cpoint))
//                     cpoint.get_direction(),
//                     cpoint.get_position(),
//                     cpoint.get_scale()))
        {
            particle_pool.delete_obj(point);
            return false;
        }

        cpoint.point_cost=newenergy_data;
//         cpoint.saliency=newpointsaliency/saliency_correction[2];
	cpoint.saliency=newpointsaliency;


        T E=(newenergy_data+particle_interaction_cost_new)/temp;
	
// 	if (particle_interaction_cost_new!=0)
// 	{
// 	  printf("%f\n",particle_interaction_cost_new);
// 	}

	std::size_t id_no_connection=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_NONE);
        // cost for non-connected point
//         T pointcost=particle_connection_state_cost_scale[id_no_connection]*cpoint.compute_cost()
// 		   +(particle_connection_state_cost_offset[id_no_connection]);//+PointScaleCost*cpoint.compute_cost_scale();
		   
	T pointcost=cpoint.compute_cost3(temp);
	
	
        E+=(pointcost)/temp;


        T R=mhs_fast_math<T>::mexp(E);



        //lambda=bin/(1/V)  bin=lambda/V  /// uniform case
        //bin/prob  /// saliency map case
        if (use_saliency_map)
        {
            R*=(lambda)*this->call_w_undo;
            R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*(nvoxel*cpoint.saliency)*this->call_w_do;//*cpoint.saliency;
        } else
        {
            R*=(lambda)*this->call_w_undo;
            R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*this->call_w_do;//*cpoint.saliency;
        }

        if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
        {
            this->tracker->update_energy(E+particle_energy_change_costs,static_cast<int>(get_type()));
            this->proposal_accepted++;

            if (!tree.insert(*point))
            {
                particle_pool.delete_obj(point);
//                 point->print();
//                 throw mhs::STAError("error insering point (collision)\n");
                return false;
            }
            if (point->particle_type!=PARTICLE_TYPES::PARTICLE_BLOB )
	    {
	      if (!conn_tree.insert(*point))
	      {
		  particle_pool.delete_obj(point);
  // 		point->print();
  // 		throw mhs::STAError("error insering point (connections)\n");
		  return false;
	      }
	    }
	    
	    #ifdef  D_USE_GUI
// 	      (GuiPoints<T>* (point))->touch(get_type());
	      point->touch(get_type());
	    #endif
	    
	    
            /*
            if (return_statistic)
            {
              static_data[center*2*numproposals+5]+=1;
            }
            */
	    this->tracker->update_volume(point->get_scale());
	    
// 	    sta_assert_error(point->has_owner(voxgrid_conn_can_id));
            return true;
        }

        particle_pool.delete_obj(point);
        return false;
    }
};

template<typename TData,typename T,int Dim>
class CDeath : public CProposal<TData,T,Dim>
{
protected:
    bool use_saliency_map;
    T lambda;
T particle_energy_change_costs;
bool temp_dependent;    
 bool sample_scale_temp_dependent;

public:
      T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_volume(num_edges,
		      num_particles,
		      num_connected_particles,		      
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
    CDeath(): CProposal<TData,T,Dim>()
    {
      temp_dependent=true;
        use_saliency_map=false;
        lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_DEATH);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_DEATH);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

        if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];

        if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
// 	printf("%s: lambda: %.0f saliency map: %s\n",
// 	       strtrim(get_name()).c_str(),
// 	       lambda,
// 		use_saliency_map ? "yes" : "no");
	
		printf("(saliency: %s)",
	use_saliency_map ? "yes" : "no");
		
				if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	    
	     if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
	  

    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;
    }

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			max_collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
        {
	  
	    this->proposal_called++;


            OctPoints<T,Dim> *  cpoint;
            cpoint=tree.get_rand_point();
            if (cpoint==NULL)
            {
                return false;
            }

            

            Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));

            if (point.isconnected())
            {
                return false;
            }
            
            if (point.isfreezed())
            {
		return false;
	    }
	    
            if (point.is_protected_topology())
            {
		return false;
	    }	 
	    
	    
	    if (sample_scale_temp_dependent && ((temp>point.get_scale())))
	    {
		  return false; 
	    }
	    
	    T particle_interaction_cost_old=0;
	    if (collision_fun_p.is_soft())    
	    {
	      T collision_search_rad=std::max(point.get_scale(),point.get_thickness())+std::max(maxscale,point.predict_thickness_from_scale(maxscale))+0.01;
		sta_assert_debug0(!(max_collision_search_rad<collision_search_rad));
	      sta_assert_error(!(collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp)));	      
	    }
	    
            /*
            if (return_statistic)
            {
                std::size_t center=(std::floor(point.position[0])*shape[1]
                  +std::floor(point.position[1]))*shape[2]
                  +std::floor(point.position[2]);
              static_data[center*2*numproposals+6]+=1;
            }
            */
	    
// 	    T collision_finite_costs=0;
// 	    if (Points<T,Dim>::collision_two_steps>0)
// 	    {
// 	      sta_assert_error((!colliding( tree,point,collision_search_rad,collision_finite_costs)));
// 	    }
// 	    
// 	    T E=-collision_finite_costs/temp;

            T newenergy=point.point_cost;

            T E=-(newenergy+particle_interaction_cost_old)/temp;

	    
// 	    std::size_t id_no_connection=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_NONE);
// 	    
//             T pointcost=particle_connection_state_cost_scale[id_no_connection]*point.compute_cost()+(particle_connection_state_cost_offset[id_no_connection]);////+PointScaleCost*point.compute_cost_scale();
	    
	    T pointcost=point.compute_cost3(temp);
	    
            E-=pointcost/temp;

            T R=mhs_fast_math<T>::mexp(E);



            {

                if (use_saliency_map)
                {
                    R*=(tree.get_numpts())*(nvoxel*point.saliency)*this->call_w_undo;
                    R/=std::numeric_limits<T>::epsilon()+(lambda)*this->call_w_do;
                } else
                {
                    R*=(tree.get_numpts())*this->call_w_undo;
                    R/=(lambda)*this->call_w_do;
                }
            }

            if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
            {

                this->proposal_accepted++;

		this->tracker->update_volume(-point.get_scale());
		
		Points< T, Dim > * p_point=(Points< T, Dim > *)cpoint;
                particle_pool.delete_obj(p_point);
		
		
		
		this->tracker->update_energy(E-particle_energy_change_costs,static_cast<int>(get_type()));

                return true;
            }
        }
        return false;

    }
};


#endif


