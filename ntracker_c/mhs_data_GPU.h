#ifndef MHS_DATA_SIMPLE_H
#define MHS_DATA_SIMPLE_H

// #ifndef __CUDACC__  
//     #define __CUDACC__
// #endif


#include "mhs_data.h"
#include "mhs_graphics.h"



// #ifndef __CUDACC__
// #define __CUDACC__
// #endif

// #include "GL/glu.h"
// #include "GL/glext.h"

#include "gpu/mxGPUArray.h"

#include "cuda_kernels.h"





template <typename T,typename TData,int Dim>
class CDataSimple: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * img;
    
//     const TData * img_smoothed;
    
    cudaArray * imgGPU;
    cudaTextureObject_t t_imgGPU;
    int gpu_device;
    cudaDeviceProp gpu_prop;
    
    const TData * local_stdv;
    int num_alphas_local_stdv;
    
    const TData * img_smooth;
    int num_alphas_img_smooth;
   
    
    class GPU_only_buffer
    {
    public: 
      float * gpu;
      int buffer_size;
      
      GPU_only_buffer()
      {
	gpu=NULL;
	buffer_size=0;
      }
      
      GPU_only_buffer(int buffer_size)
      {
	resize(buffer_size);
      }
      
      void clean_buffer()
      {
	if (gpu!=NULL)
	{
	printf("cleaning up gpu buffer\n");
	cudaFree (gpu) ;
	}
	buffer_size=0;
      }
      
      void resize(int buffer_size)
      {
	clean_buffer();
	printf("allocating gpu buffer (%d)\n",buffer_size);
	cudaMalloc ( (void **)&gpu, buffer_size*sizeof(float)) ;
	this->buffer_size=buffer_size;
      }
      ~GPU_only_buffer()
      {
	clean_buffer();
      }
    };
    
    
    
    std::vector<GPU_CPU_common_buffer> buffer_GPU;
    std::size_t max_buffer_size;
    std::vector<GPU_only_buffer> buffer_only_GPU;
    std::size_t max_buffer_size_gpu;    
    std::size_t max_buffer_size_gpu_vdim;    

    const TData * debug;
    T min_scale;
    T max_scale;

    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
    T DataScaleGrad;
    T Border;
    int debug_id;

//     std::size_t shape[3];

    
    

public:

    float get_minscale()
    {
        return min_scale;
    };
    float get_maxscale()
    {
        return max_scale;
    };
    float get_scalecorrection()
    {
          return 1;
    };


    CDataSimple()
    {
      Border=0.2;
      debug_id=0;
      sta_assert_error(MX_GPU_SUCCESS==mxInitGPU()); 
      imgGPU=NULL;
      gpu_device=0;
      if (std::is_same<TData,double>::value)
      {
	mhs::STAError error;
	error<<"double textures are not supported!";
	throw error;
      }
      max_buffer_size=64;
      max_buffer_size_gpu_vdim=4;
      max_buffer_size_gpu=64*64*64*max_buffer_size_gpu_vdim;
    }

    ~CDataSimple()
    {
      
      if (imgGPU)
      {
	cudaError_t cuda_code;
	cuda_code=cudaHostUnregister((void *)img);
	sta_assert_error(cuda_code==cudaSuccess);
	
	cudaDestroyTextureObject(t_imgGPU);
	cuda_code=cudaFreeArray(imgGPU);
	sta_assert_error(cuda_code==cudaSuccess);
      }
      
// 	for  (int a=0;a<buffer_GPU.size();a++)
// 	 {
// 	   
// 	   cudaFree (buffer_GPU[a]) ;
// 	   
// 	   delete [] buffer_CPU[a];
// 	   
// 	 }
    }
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle)
    {
      if (handle!=NULL)
      {
	  const mxArray *
	  parameter= mxGetField(handle,0,(char *)("tracker_data"));
	  if (parameter!=NULL) 
	  {
		  DataThreshold=-(*((double*) mxGetPr(parameter)));
		  DataScale=*(((double*) mxGetPr(parameter))+2);
		  DataScaleGradVessel=*(((double*) mxGetPr(parameter))+3);
		  DataScaleGrad=*(((double*) mxGetPr(parameter))+4);
	  }
	  printf("Th: %f,  Da %f, DataScaleGradVessel %f, - %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
      }
    }
    void set_controls(const mxArray * handle)
    {
	  if (handle!=NULL)
	    {
	    const mxArray *
	    parameter= mxGetField(handle,0,(char *)("tracker_data"));
	    if (parameter!=NULL) 
	    {
		  *((double*) mxGetPr(parameter))=-DataThreshold;
		  *(((double*) mxGetPr(parameter))+2)=DataScale;
		  *(((double*) mxGetPr(parameter))+3)=DataScaleGradVessel;
		  *(((double*) mxGetPr(parameter))+4)=DataScaleGrad;
	    }
	  }  
	  printf("Th: %f,  Da %f, DataScaleGradVessel %f, - %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
    }
    #endif    
    


    void init(const mxArray * feature_struct)
    {
        for (int i=0; i<3; i++)
            this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
        std::swap(this->shape[0],this->shape[2]);

        mhs::dataArray<TData>  tmp;
	try 
        {
            tmp=mhs::dataArray<TData>(feature_struct,"img");
            img=tmp.data;
            sta_assert(tmp.dim.size()==3);
	    
// 	    tmp=mhs::dataArray<TData>(feature_struct,"img_smoothed");
//             img_smoothed=tmp.data;
//             sta_assert(tmp.dim.size()==3);
        
            tmp=mhs::dataArray<TData>(feature_struct,"alphas_img");
            img_smooth=tmp.data;
            num_alphas_img_smooth=tmp.dim[tmp.dim.size()-1];
            printf("img smoothed pol degree: %d \n",num_alphas_img_smooth-1);
        
            tmp=mhs::dataArray<TData>(feature_struct,"alphas_sdv");
            local_stdv=tmp.data;
            num_alphas_local_stdv=tmp.dim[tmp.dim.size()-1];
            printf("local stdv pol degree: %d \n",num_alphas_local_stdv-1);
        }catch (mhs::STAError error)
	{
	  throw error;
	}
	 
	try {
	  const TData * scales=mhs::dataArray<TData>(feature_struct,"override_scales").data;
	  min_scale=scales[0];
	  max_scale=scales[mhs::dataArray<TData>(feature_struct,"override_scales").dim[0]-1];
        } catch (mhs::STAError error)
        {
	    const TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	    min_scale=scales[0];
	    max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
        }
        printf("scale: [%f %f]\n",min_scale,max_scale);
	
	
	
	cudaError_t cuda_code;
	
	 int nDevices;
	 
	cudaGetDeviceCount(&nDevices);
	sta_assert_error(nDevices>0);
	for (int i = 0; i < nDevices; i++) {
	  cudaDeviceProp prop;
	  cudaGetDeviceProperties(&prop, i);
	  printf("Device %d (%s)\n",i, prop.name);
	  printf("maxGridSize                 %d %d %d\n",prop.maxGridSize[0],prop.maxGridSize[1],prop.maxGridSize[2]);
	  printf("maxThreadDim                %d %d %d\n",prop.maxThreadsDim[0],prop.maxThreadsDim[1],prop.maxThreadsDim[2]);
	  printf("maxThreadsPerBlock          %d \n",prop.maxThreadsPerBlock);
	  printf("maxThreadsPerMultiProcessor %d \n",prop.maxThreadsPerMultiProcessor);
	  printf("sharedMemPerBlock           %d \n",prop.sharedMemPerBlock);
	  printf("multiProcessorCount         %d \n",prop.multiProcessorCount);
	  printf("totalGlobalMem              %d GB \n",prop.totalGlobalMem/1024/1024/1000);
	  printf("l2CacheSize                 %d KB \n",prop.l2CacheSize/1024);
	  printf("canMapHostMemory            %d \n",prop.canMapHostMemory);
	  
	  
	  
// 	  printf("  Memory Clock Rate (KHz): %d\n",
// 		prop.memoryClockRate);
// 	  printf("  Memory Bus Width (bits): %d\n",
// 		prop.memoryBusWidth);
// 	  printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
// 		2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
	}

	
	cuda_code=cudaGetDeviceProperties(&gpu_prop, gpu_device);
	sta_assert_error(cuda_code==cudaSuccess);
	printf("using device %d\n",gpu_device);
	cuda_code=cudaSetDevice(gpu_device);
	sta_assert_error(cuda_code==cudaSuccess);


// 	cuda_code=cudaSetDeviceFlags(cudaDeviceMapHost);
// 	sta_assert_error(cuda_code==cudaSuccess);
	
	
	{
	  int bb[3];
	  float scale_img[3];
	  scale_img[0]=10;
	  scale_img[1]=6;
	  scale_img[2]=5;
	  
	  
	  float scale_kernel[3];
	  float max_sigma=(std::sqrt(gpu_prop.maxThreadsPerBlock)-1)/(3*2);
	  for (int i=0;i<3;i++)
	  {
	    scale_kernel[i]=std::min(scale_img[i],max_sigma);
	  }
	  	  
	  	  
// 	  int max_size=std::sqrt(gpu_prop.maxThreadsPerBlock);
// 	  for (int i=0;i<3;i++)
// 	  {
// 	    t[i]=(std::min((float)max_size,(float)(2*std::ceil(3*sigma[i])+1))-1)/(2*3);
// 	  }
// 	  	  
	  
	  for (int i=0;i<3;i++)
	  {
	   bb[i]=2*std::ceil(3*scale_kernel[i])+1;  
	  }
	  int voxel_per_slice=bb[1]*bb[2];
	  int block_thickness=gpu_prop.maxThreadsPerBlock/voxel_per_slice;
	  sta_assert_error(block_thickness>0);
	  int num_blocks=bb[0]/block_thickness;
	
 	  printf("voxel_per_slice:  %d / block_thickness: %d / num_blocks: %d\n",
 	    voxel_per_slice,
 	  block_thickness,
 	  num_blocks
 	  );
	  printf("bb: %d  x (%d %d)\n",
 	  bb[0],
 	  bb[1],
 	  bb[2]
 	  );
	  
	  
	  int num_total_voxels=bb[0]*bb[1]*bb[2];
	  int num_total_blocks=num_total_voxels/gpu_prop.maxThreadsPerBlock + (num_total_voxels % gpu_prop.maxThreadsPerBlock != 0);
	  printf("num_total_voxels:  %d / num_total_blocks: %d\n",num_total_voxels,num_total_blocks);
	  
	  
	  T box2img[3];
	  T box2kernel[3];	  
// 	  box2kernel[i]=scale_img[i]/t[i];
//  	  box2img[i]=scale_img[i]/t[i];
	}	
	
	
	cudaChannelFormatDesc imgData= cudaCreateChannelDesc<TData>();;
// 	imgData.f=cudaChannelFormatKindFloat;
// 	imgData.x=sizeof(TData);
// 	imgData.y=0;
// 	imgData.z=0;
// 	imgData.w=0;
	
	printf("allocating cuda memory ..");
// 	cudaError_t cuda_code=cudaMalloc3DArray(&imgGPU,&imgData,make_cudaExtent(this->shape[0],this->shape[1],this->shape[2]));
// 	cuda_code=cudaMalloc3DArray(&imgGPU,&imgData,make_cudaExtent(this->shape[0],this->shape[1],this->shape[2]));
	cuda_code=cudaMalloc3DArray(&imgGPU,&imgData,make_cudaExtent(this->shape[2],this->shape[1],this->shape[0]));
	sta_assert_error(cuda_code==cudaSuccess);
	printf("done\n"); 
	printf("img CPU -> GPU");
	std::size_t numv=this->shape[0]*this->shape[1]*this->shape[2];
	cuda_code=cudaHostRegister((void *)img,numv*sizeof(TData),cudaHostRegisterPortable);
	sta_assert_error(cuda_code==cudaSuccess);
	
// 	cudaMemcpy3DParms cpyData;
// 	cpyData.srcArray
	
// 	cuda_code=cudaMemcpy3DAsync();
//  	cuda_code=cudaMemcpyToArray(imgGPU,0,0,img,numv*sizeof(TData),cudaMemcpyHostToDevice);
	
	
	cudaMemcpy3DParms copyParams = {0};
	copyParams.srcPtr   = make_cudaPitchedPtr((void*)img, this->shape[2]*sizeof(TData), this->shape[2],this->shape[1]);  
	copyParams.dstArray = imgGPU;
// 	copyParams.extent   = make_cudaExtent(this->shape[0],this->shape[1],this->shape[2]);
	copyParams.extent   = make_cudaExtent(this->shape[2],this->shape[1],this->shape[0]);
	copyParams.kind	 = cudaMemcpyHostToDevice;

  //safecall(cudaMemcpyToArray(Z_d, 0, 0, Z, C*M*N*sizeof(float), cudaMemcpyHostToDevice), "cudaMemcpyToArray" );
	cuda_code=cudaMemcpy3DAsync(&copyParams);
	
	
// 	printf("cuda code: %s\n",cudaGetErrorString(cuda_code));
	sta_assert_error(cuda_code==cudaSuccess);
	printf("done\n"); 
	printf("creating tex object ..");
	
	
	cudaResourceDesc    texRes;
        memset(&texRes, 0, sizeof(cudaResourceDesc));
        texRes.resType = cudaResourceTypeArray;
        texRes.res.array.array  = imgGPU;
	
		
	
        cudaTextureDesc     texDescr;
        memset(&texDescr, 0, sizeof(cudaTextureDesc));
        texDescr.normalizedCoords = false;
        texDescr.filterMode = cudaFilterModeLinear;
        texDescr.addressMode[0] = cudaAddressModeClamp;   // clamp
        texDescr.addressMode[1] = cudaAddressModeClamp;
        texDescr.addressMode[2] = cudaAddressModeClamp;
        texDescr.readMode = cudaReadModeElementType;
        cuda_code=(cudaCreateTextureObject(&t_imgGPU, &texRes, &texDescr, NULL));

	sta_assert_error(cuda_code==cudaSuccess);
	
	printf("done\n"); 
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGradVessel=1;
        DataScaleGrad=1;
        DataThreshold=1;
	Border=0.2;
	
        if (params!=NULL)
        {
	  try
	  {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGrad")!=-1)
                DataScaleGrad=mhs::mex_getParam<T>(params,"DataScaleGrad",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
	    
	    if (mhs::mex_hasParam(params,"Border")!=-1)
                Border=mhs::mex_getParam<T>(params,"Border",1)[0];
	    } catch(mhs::STAError & error)
	  {
	      throw error;
	  }
        }

    }

private:

  
  void resize_GPU_CPU_buffer(int thread_id,int buffer_size_gpu_cpu,int buffer_size_gpu)
  {
	 while (buffer_GPU.size()<thread_id+1)
	  buffer_GPU.push_back(GPU_CPU_common_buffer());
	 
	 if (buffer_GPU[thread_id].buffer_size<buffer_size_gpu_cpu)
	  buffer_GPU[thread_id].resize(buffer_size_gpu_cpu);
	 
	  while (buffer_only_GPU.size()<thread_id+1)
	  buffer_only_GPU.push_back(GPU_only_buffer());
	 
	 if (buffer_only_GPU[thread_id].buffer_size<buffer_size_gpu)
	  buffer_only_GPU[thread_id].resize(buffer_size_gpu);
  }
  


private:
  
    bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius
		  #ifdef D_USE_GUI        
//         , std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
	, std::list<std::string>  * debug=NULL
#endif     
      
    )
    {
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;

	
	if ((radius<min_scale-0.01)
	  ||(radius>max_scale+0.01))
	{
	  printf("[%f %f %f]\n",min_scale,radius,max_scale);
 	  sta_assert_error(!(radius<min_scale));
 	  sta_assert_error(!(radius>max_scale));
	}
	radius=std::min(max_scale,std::max(radius,min_scale));
	
	#ifdef D_USE_MULTITHREAD
	 int  thread_id= omp_get_thread_num();	
	#else
	 int  thread_id=0;
	#endif
// 	 if (buffer_GPU.size()<thread_id+1)
// 	 {
// 	   printf("resizing GPU buffer: %d\n",thread_id);
// 	    resize_GPUbuffer(thread_id,max_buffer_size);
// 	 }
        
	  resize_GPU_CPU_buffer(thread_id,max_buffer_size,max_buffer_size_gpu);
	
	  
	  
	T multi_scale=std::sqrt(radius);
	
        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);
        T stdv=0;
        {
            stdv+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,local_stdv+(((Z*this->shape[1]+Y)*this->shape[2]+X))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,local_stdv+(((Z*this->shape[1]+Y)*this->shape[2]+(X+1)))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,local_stdv+(((Z*this->shape[1]+(Y+1))*this->shape[2]+X))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,local_stdv+(((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1)))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,local_stdv+((((Z+1)*this->shape[1]+Y)*this->shape[2]+X))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,local_stdv+((((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1)))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,local_stdv+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X))*num_alphas_local_stdv,num_alphas_local_stdv-1);
            stdv+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,local_stdv+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1)))*num_alphas_local_stdv,num_alphas_local_stdv-1);
        }
        stdv=1/(stdv+0.01);
	
	
	Vector<T,3> vn[3];
		    vn[0]=direction;
		    
	mhs_graphics::createOrths(vn[0],vn[1],vn[2]); 	
	  
	T penalty=0;
	Vector<T,3> pos;  
	T grad[2];
	for (int i=0;i<3;i++)
	{
	  
	  for (int d=0;d<2;d++)
	  {
	      
	      if (d==0)
	      {
		pos=position+vn[i];
	      }
	      else
	      {
		pos=position-vn[i];
	      }
	      int Z=std::floor(pos[0]);
	      int Y=std::floor(pos[1]);
	      int X=std::floor(pos[2]);

	      if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
		      (Z<0)||(Y<0)||(X<0))
		  return false;
	      
	      T wz=(pos[0]-Z);
	      T wy=(pos[1]-Y);
	      T wx=(pos[2]-X);
	      T & g=grad[d];
	      g=0;
	      {
  // 		g+=(1-wz)*(1-wy)*(1-wx)*img_smoothed[((Z*this->shape[1]+Y)*this->shape[2]+X)];
  // 		g+=(1-wz)*(1-wy)*(wx)*img_smoothed[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
  // 		g+=(1-wz)*(wy)*(1-wx)*img_smoothed[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
  // 		g+=(1-wz)*(wy)*(wx)*img_smoothed[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
  // 		g+=(wz)*(1-wy)*(1-wx)*img_smoothed[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
  // 		g+=(wz)*(1-wy)*(wx)*img_smoothed[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
  // 		g+=(wz)*(wy)*(1-wx)*img_smoothed[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
  // 		g+=(wz)*(wy)*(wx)*img_smoothed[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
		  
		
		  g+=(1-wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,img_smooth+(((Z*this->shape[1]+Y)*this->shape[2]+X))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(1-wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,img_smooth+(((Z*this->shape[1]+Y)*this->shape[2]+(X+1)))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(1-wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,img_smooth+(((Z*this->shape[1]+(Y+1))*this->shape[2]+X))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(1-wz)*(wy)*(wx)*eval_polynom1(multi_scale,img_smooth+(((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1)))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(wz)*(1-wy)*(1-wx)*eval_polynom1(multi_scale,img_smooth+((((Z+1)*this->shape[1]+Y)*this->shape[2]+X))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(wz)*(1-wy)*(wx)*eval_polynom1(multi_scale,img_smooth+((((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1)))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(wz)*(wy)*(1-wx)*eval_polynom1(multi_scale,img_smooth+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X))*num_alphas_img_smooth,num_alphas_img_smooth-1);
		  g+=(wz)*(wy)*(wx)*eval_polynom1(multi_scale,img_smooth+((((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1)))*num_alphas_img_smooth,num_alphas_img_smooth-1);
	      }
	  }
	  penalty+=(grad[0]-grad[1])*(grad[0]-grad[1]);
	}
 	penalty=stdv*std::sqrt(penalty);
// 	T penalty=std::abs(grad[0]-grad[1]);
	  
	  
// 	 printf("0: %d %d\n",(debug!=NULL),debug->size());
	  
	  
	  
	
	
	  T * debug_data4=NULL;
	  mxArray * pdata4=NULL;  
	  TData * debug_data5=NULL;
	  mxArray * pdata5=NULL;  
	
	  
	  
	    

	  
	  float scale_img[3];
	  scale_img[0]=radius;
	  scale_img[1]=radius;
	  scale_img[2]=radius;	  

	  
	  T kernel_CPU_H=0;  
	  float num_valid_voxels_CPU=0;
	  float mean_intensities_CPU=0;
	  float mean_kernel_CPU=0;	  
// #define _CPU_KERNEL	  
#ifdef _CPU_KERNEL
	  {
	    int bb[3];
	    T s1_1=scale_img[0];
	    T s2_1=scale_img[1];
	    T s3_1=scale_img[2];
	    T s1_2=s1_1*s1_1;
	    T s2_2=s2_1*s2_1;
	    T s3_2=s3_1*s3_1;
	    T s1_4=s1_2*s1_2;
	    T s2_4=s2_2*s2_2;
	    T s3_4=s3_2*s3_2;
	    
	    float scale_kernel[3];
	    float max_sigma=(std::sqrt(gpu_prop.maxThreadsPerBlock)-1)/(3*2);
	    for (int i=0;i<3;i++)
	    {
	      scale_kernel[i]=scale_img[i];
// 	      scale_kernel[i]=std::min(scale_img[i],max_sigma);
// 	      scale_kernel[i]=5;
	    }
	    
	    int offset[3];
	    float box2kernel[3];
	    for (int i=0;i<3;i++)
	    {
	    bb[i]=2*std::ceil(3*scale_kernel[i])+1;  
	    box2kernel[i]=scale_img[i]/scale_kernel[i];
	    offset[i]=std::ceil(3*scale_kernel[i]);
	    }
	    
	    if ((debug!=NULL)&&(debug->size()>0))
	    {
	      {
		std::size_t ndims[3];
		ndims[2]=bb[0];
		ndims[1]=bb[1];
		ndims[0]=bb[2];
		pdata4 = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<T>(),mxREAL );
		debug_data4= ( T * ) mxGetData (pdata4 );
	      }
	    }
	    
	    
	    int num_total_voxels=bb[0]*bb[1]*bb[2];
	    int num_total_blocks=num_total_voxels/gpu_prop.maxThreadsPerBlock + (num_total_voxels % gpu_prop.maxThreadsPerBlock != 0);
	  
	    float scales_img[9];
	    scales_img[0]=scale_img[0];
	    scales_img[1]=scale_img[1];
	    scales_img[2]=scale_img[2];
	    
	    scales_img[3]=scales_img[0]*scales_img[0];
	    scales_img[4]=scales_img[1]*scales_img[1];
	    scales_img[5]=scales_img[2]*scales_img[2];
	    
	    scales_img[6]=scales_img[3]*scales_img[3];
	    scales_img[7]=scales_img[4]*scales_img[4];
	    scales_img[8]=scales_img[5]*scales_img[5];
	  
	   
// 	    printf("CPU %d %d\n",num_total_blocks,gpu_prop.maxThreadsPerBlock);
	    Vector<float,3> p;
	    Vector<float,3> vn0;
	    vn0=direction;
	    Vector<float,3> vn1;
	    Vector<float,3> vn2;
	    Vector<float,3> pos;
	    pos=position;
	    mhs_graphics::createOrths(vn0,vn1,vn2); 
	
	    for (int b=0;b<num_total_blocks;b++)
	    {
	      
	      for (int t=0;t<gpu_prop.maxThreadsPerBlock;t++)
	      {
		unsigned int indx=b*gpu_prop.maxThreadsPerBlock+t;
		float value=0;
		if (indx<num_total_voxels)
		{
		  int z=indx/(bb[1]*bb[2])-offset[0];
		  indx%=(bb[1]*bb[2]);
		  int y=indx/(bb[2])-offset[1];
		  int x=indx%(bb[2])-offset[2];
		  
		  float dist=(z/float(offset[0]));
		  dist*=dist;
		  if (!(dist>1+__FLT_EPSILON__))
		  {
		    float tmp=(y/float(offset[1]));
		    dist+=tmp*tmp;
		    if (!(dist>1+__FLT_EPSILON__))
		    {
		      tmp=(x/float(offset[2]));
		      dist+=tmp*tmp;
		      if (!(dist>1+__FLT_EPSILON__))
		      {
			    float kx=x*box2kernel[0];
			    float ky=y*box2kernel[1];
			    float kz=z*box2kernel[2];
			    
			      
			    float & s1_2=scales_img[3];
			    float & s2_2=scales_img[4];
			    float & s3_2=scales_img[5];
			    float & s2_4=scales_img[7];
			    float & s3_4=scales_img[8];
			      

	    		  p=pos+vn0*kz+vn1*kx+vn2*ky;
			  
// 			  p[0]=45-1;
// 			  p[1]=62-1;
// 			  p[2]=30-1;
// 			  
// 	    		  p=T(0);
			  
	    		  int Z=std::floor(p[0]);
	    		  int Y=std::floor(p[1]);
	    		  int X=std::floor(p[2]);
	    		  
	    		  const int boundary=1;
	    		  if ((Z<0)||(Y<0)||(X<0)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
	    		  {
	    		  return false; 
	    		  }
	    		  
	    		    T intensity=0.0f;
	    		    T wz=(p[0]-Z);
	    		    T wy=(p[1]-Y);
	    		    T wx=(p[2]-X);
	    		    {
	    			intensity+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
	    			intensity+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
	    			intensity+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
	    			intensity+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
	    			intensity+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
	    			intensity+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
	    			intensity+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
	    			intensity+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
	    		    }
	    		    
	    		    

	    		    
	    		    
	    		    
	    		   T h_img=intensity;
			      
			      float pol=0;
			      float pol_t=0;
			      pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
			      pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/2.0f;

// 			      T g=mexp(pol_t);
			      T g=std::exp(-pol_t);

			      T h_kernel=g*pol;
			      
			      value=h_img*h_kernel;
//  			      value=s3_2*s2_4+s3_4 *s2_2 ;;
//  			      value=1;
			      num_valid_voxels_CPU+=1;
			      mean_intensities_CPU+=h_img;
			      mean_kernel_CPU+=h_kernel;
			    
		      }
		    }
		  }
		  	
		  if (debug_data4!=NULL)
		  {
		    int dshape[3];
		    dshape[0]=bb[0];
		    dshape[1]=bb[1];
		    dshape[2]=bb[2];
		    int debug_data_dim=dshape[0]*dshape[1]*dshape[2];
// 		    int indx=(z*dshape[1]+y)*dshape[2]+x;
		    int indx=((z+offset[0])*dshape[1]+(y+offset[1]))*dshape[2]+(x+offset[2]);
		    sta_assert_error(indx>=0);
		    sta_assert_error(indx<debug_data_dim);
		    
		    if ((indx<0)||(indx>debug_data_dim-1))
		    {
		      printf("? %d %d \n",indx,debug_data_dim);
		    }else{
		      debug_data4[indx]=value;
// 		       debug_data4[indx]=h_img;
		    }
		  }
	

// 		 if (t==15)
// 		 {
// // 		    float dist=(z/offset[0]);
// // 		    dist*=dist;
// // 		    float tmp=(y/offset[1]);
// // 		    dist+=tmp*tmp;
// // 		    tmp=(x/offset[2]);
// // 		    dist+=tmp*tmp;
// // 		    value=dist;
// 		   printf("CPU %d, %f\n",b,(float)(z/offset[0]));
// 		   value=dist;
// 		 }
		}
// 		value=indx;
		kernel_CPU_H+=value;
	      }
	    }
	    }
#endif	    

	    

	    
	    T kernel_GPU_H=0;
	    {
		int bb[3];
		float scale_kernel[3];
// 		float max_sigma=(std::sqrt(gpu_prop.maxThreadsPerBlock)-1)/(3*2);
 		float max_sigma=(std::sqrt(gpu_prop.maxThreadsPerBlock)-1)/(3*2);
// 		const float max_sigma=(std::pow(12.0f*1024.0f,1.0/3.0)-1)/6;//12 times 1024 threads
		float min_sigma=2;
		float fix_kernel_Scale=8;
		for (int i=0;i<3;i++)
		{
// 		  scale_kernel[i]=scale_img[i];
//   		  scale_kernel[i]=fix_kernel_Scale;
// 		  scale_kernel[i]=std::max(std::min(scale_img[i],max_sigma),min_sigma);
  		  scale_kernel[i]=std::min(scale_img[i],max_sigma);
//   		  scale_kernel[i]=std::max(std::min(scale_img[i],max_sigma),2.0f);
//  		  scale_kernel[i]=std::max(scale_img[i],2.0f);
// 		  scale_kernel[i]=10;
    // 	      scale_kernel[i]=5;
		}
	      
	        int offset[3];
		float box2kernel[3];
		for (int i=0;i<3;i++)
		{
		bb[i]=2*std::ceil(3*scale_kernel[i])+1;  
		box2kernel[i]=scale_img[i]/scale_kernel[i];
		offset[i]=std::ceil(3*scale_kernel[i]);
		}
	      
		float3  GPU_position;
		GPU_position.z=position[0];
		GPU_position.y=position[1];
		GPU_position.x=position[2];
		float3  GPU_v0,GPU_v1,GPU_v2;
		
		GPU_v0.z=vn[0][0]*box2kernel[0];
		GPU_v0.y=vn[0][1]*box2kernel[0];
		GPU_v0.x=vn[0][2]*box2kernel[0];
		
		GPU_v1.z=vn[1][0]*box2kernel[1];
		GPU_v1.y=vn[1][1]*box2kernel[1];
		GPU_v1.x=vn[1][2]*box2kernel[1];
		
		GPU_v2.z=vn[2][0]*box2kernel[2];
		GPU_v2.y=vn[2][1]*box2kernel[2];
		GPU_v2.x=vn[2][2]*box2kernel[2];
	      
// 		float3  GPU_position;
// 		GPU_position.z=position[2];
// 		GPU_position.y=position[1];
// 		GPU_position.x=position[0];
// 		float3  GPU_v0,GPU_v1,GPU_v2;
// 		
// 		GPU_v0.z=vn0[2];
// 		GPU_v0.y=vn0[1];
// 		GPU_v0.x=vn0[0];
// 		
// 		GPU_v1.z=vn1[2];
// 		GPU_v1.y=vn1[1];
// 		GPU_v1.x=vn1[0];
// 		
// 		GPU_v2.z=vn2[2];
// 		GPU_v2.y=vn2[1];
// 		GPU_v2.x=vn2[0];
		
		
		
		float3  GPU_offset;
		GPU_offset.z=offset[0];
		GPU_offset.y=offset[1];
		GPU_offset.x=offset[2];
		
/*		float3  GPU_box2kernel;
		GPU_box2kernel.z=box2kernel[0];
		GPU_box2kernel.y=box2kernel[1];
		GPU_box2kernel.x=box2kernel[2];	*/	
		
		float3  GPU_scales2;
// 		GPU_scales2.z=scale_img[0]*scale_img[0];
// 		GPU_scales2.y=scale_img[1]*scale_img[1];
// 		GPU_scales2.x=scale_img[2]*scale_img[2];
		GPU_scales2.z=scale_kernel[0]*scale_kernel[0];
		GPU_scales2.y=scale_kernel[1]*scale_kernel[1];
		GPU_scales2.x=scale_kernel[2]*scale_kernel[2];
		

		float2  GPU_scales4;
		GPU_scales4.y=GPU_scales2.y*GPU_scales2.y;
		GPU_scales4.x=GPU_scales2.x*GPU_scales2.x;
		
		int3    GPU_bb;
		GPU_bb.z=bb[0];
		GPU_bb.y=bb[1];
		GPU_bb.x=bb[2];
		
		int GPU_num_total_voxels=bb[0]*bb[1]*bb[2];

		
		int num_threads=gpu_prop.maxThreadsPerBlock;
		int num_total_blocks=GPU_num_total_voxels/num_threads + (GPU_num_total_voxels % num_threads != 0);
		
		{
		 cudaError_t err = cudaGetLastError();
		  if (err != cudaSuccess) 
		  {
		      printf("Error enterig GPU section: %s\n", cudaGetErrorString(err));
		  sta_assert_error(1!=1);
		  }
		 }
		
// 		if (num_total_blocks>buffer_GPU[thread_id].buffer_size)
// 		{
// 		  buffer_GPU[thread_id].resize(num_total_blocks);
// 		}

		// const std::size_t gpu_cpu_buffer_size_fact=3;
		const std::size_t gpu_cpu_buffer_size_fact=1;

		if (num_total_blocks*gpu_cpu_buffer_size_fact>buffer_GPU[thread_id].buffer_size)
		{
		  buffer_GPU[thread_id].resize(num_total_blocks*gpu_cpu_buffer_size_fact);
		}
		
		if (GPU_num_total_voxels*max_buffer_size_gpu_vdim>buffer_only_GPU[thread_id].buffer_size)
		{
		  buffer_only_GPU[thread_id].resize(GPU_num_total_voxels*max_buffer_size_gpu_vdim);
		}
// 	      cudaError_t  cuda_error_code=cudaMemcpy ( buffer_GPU[thread_id].cpu, buffer_GPU[thread_id].gpu, max_buffer_size*sizeof(float) , cudaMemcpyDeviceToHost ) ;
// 		if (cuda_error_code!=cudaSuccess)
// 		{
// 		printf("cuda code: %s\n",cudaGetErrorString(cuda_error_code));
// 		}
	
 		float * GPU_result=buffer_GPU[thread_id].gpu;
 		float * CPU_result=buffer_GPU[thread_id].cpu;
		float * GPU_buffer=buffer_only_GPU[thread_id].gpu;
				
  		float * GPU_debug_kernel=NULL;
		
// printf("1: %d %d\n",(debug!=NULL),debug->size());
		
		if ((debug!=NULL)&&(debug->size()>0))
		{  
		printf("allocating debug kernel GPU memory\n");  
 		cudaMalloc ( (void **)&GPU_debug_kernel, GPU_num_total_voxels*sizeof(float)) ;
		}
		
		{
		 cudaError_t err = cudaGetLastError();
		  if (err != cudaSuccess) 
		  {
		      printf("Error before kernel: %s\n", cudaGetErrorString(err));
		  sta_assert_error(1!=1);
		  }
		 }
/*		 
		 kernel_interpolate<<<num_total_blocks, num_threads, num_threads*3*sizeof(float) >>>( 
			GPU_position,
			GPU_v0,
			GPU_v1,
			GPU_v2,
			GPU_offset,
			GPU_box2kernel,
			GPU_scales2,
			GPU_scales4,
			GPU_num_total_voxels,
			GPU_bb,
			t_imgGPU,
			GPU_buffer,
			GPU_result);

		  buffer_GPU[thread_id].gpu2cpu();
		  
		  float num_valid_voxels=0;
		  float mean_intensities=0;
		  float mean_kernel=0;
		  for ( int i = 0 ; i<num_total_blocks ; i++ )
		  {
// 				printf("GPU %d, %f\n",i,CPU_result[i]);
				num_valid_voxels+=CPU_result[i] ;
				mean_intensities+=CPU_result[i+num_total_blocks] ;
				mean_kernel+=CPU_result[i+2*num_total_blocks] ;
		  }
// 		  printf("%f %f %f\n",num_valid_voxels,mean_intensities,mean_kernel);
// 		  printf("%f %f %f\n",num_valid_voxels_CPU,mean_intensities_CPU,mean_kernel_CPU);
		  
		  sta_assert_error(num_valid_voxels>0);
		  mean_intensities/=num_valid_voxels;
		  mean_kernel/=num_valid_voxels;
		  
		  
		  kernel_var<<<num_total_blocks, num_threads, num_threads*2*sizeof(float) >>>(  
			GPU_num_total_voxels,
			mean_intensities,
			mean_kernel,
			GPU_buffer,
			GPU_result);
		  
		  buffer_GPU[thread_id].gpu2cpu();
		  
		  float var_intensities=0;
		  float var_kernel=0;
		  for ( int i = 0 ; i<num_total_blocks ; i++ )
		  {
// 				printf("GPU %d, %f\n",i,CPU_result[i]);
				var_intensities+=CPU_result[i] ;
				var_kernel+=CPU_result[i+num_total_blocks] ;
		  }
		  var_intensities/=num_valid_voxels;
		  var_kernel/=num_valid_voxels;

		  kernel_evaluate<<<num_total_blocks, num_threads, num_threads*sizeof(float) >>>( 
		      GPU_num_total_voxels,
		      GPU_buffer,
		      GPU_result,
		      GPU_debug_kernel);
*/		      
		  
// 		   kernel_evaluate<<<num_total_blocks, num_threads, num_threads*sizeof(float) >>>( 
// 		      GPU_num_total_voxels,
// 		      GPU_buffer,
// 		      GPU_result,
// 		      GPU_debug_kernel);
		  
/*
		   buffer_GPU[thread_id].gpu2cpu();
		  
		  float debug_v=0;
		  for ( int i = 0 ; i<num_total_blocks ; i++ )
		  {
				debug_v+=CPU_result[i] ;
		  }*/
		  
		
		 kernel_sfilter2<<< num_total_blocks, num_threads, num_threads*sizeof(float) >>>( 
		  GPU_position,
		  GPU_v0,
		  GPU_v1,
		  GPU_v2,
		  GPU_offset,
// 		  GPU_box2kernel,
		  GPU_scales2,
		  GPU_scales4,
		  GPU_num_total_voxels,
		  GPU_bb,
		  t_imgGPU,
		   GPU_result,
		   GPU_debug_kernel);
		 

		 {
		 cudaError_t err = cudaGetLastError();
		  if (err != cudaSuccess) 
		  {
		      sta_assert_error(1!=1);
		      printf("Error after kernel: %s\n", cudaGetErrorString(err));
		  }
		 }
		 sta_assert_error(!(num_total_blocks>(buffer_GPU[thread_id].buffer_size)));
		 
		 cudaError_t  cuda_error_code;
		 
	      
// 	      cuda_error_code=cudaMemcpy ( buffer_GPU[thread_id].cpu, buffer_GPU[thread_id].gpu, num_total_blocks*sizeof(float) , cudaMemcpyDeviceToHost ) ;
			
		buffer_GPU[thread_id].gpu2cpu();
		
		      
		 
//  		 cudaMemcpy ( CPU_result , GPU_result, num_total_blocks*sizeof(float) , cudaMemcpyDeviceToHost ) ;
// 		  cudaMemcpy ( buffer_CPU[thread_id], buffer_GPU[thread_id], num_total_blocks*sizeof(float) , cudaMemcpyDeviceToHost ) ;

		  for ( int i = 0 ; i<num_total_blocks ; i++ )
		  {
// 				printf("GPU %d, %f\n",i,CPU_result[i]);
				kernel_GPU_H+=CPU_result[i] ;
		  }
		  
//  		  printf("%f %f %f\n",kernel_GPU_H,stdv,radius);
   		  kernel_GPU_H*=stdv/(2*M_PI*scale_kernel[0]);
//  		  kernel_GPU_H*=stdv/(2*M_PI*fix_kernel_Scale);
		   
  		  /*printf("%f %f %f %f\n",kernel_GPU_H,stdv,radius,fix_kernel_Scale);*/ 
// 		   kernel_GPU_H*=1/(2*M_PI*radius);
		  
// 		  printf("%f %f %f\n",debug_v,kernel_GPU_H,kernel_CPU_H);
		
		
		
		//if ((debug!=NULL)&&(debug->size()>0))
		{
		  if (GPU_debug_kernel!=NULL){
		    std::size_t ndims[3];
		    ndims[2]=bb[0];
		    ndims[1]=bb[1];
		    ndims[0]=bb[2];
		    mxArray * pdata = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<TData>(),mxREAL );
		    TData * debug_data= ( TData * ) mxGetData (pdata );
		    cudaMemcpy ( debug_data, GPU_debug_kernel, GPU_num_total_voxels*sizeof(float) , cudaMemcpyDeviceToHost ) ;
// 		    mexPutVariable("global","kernel_debug",pdata);
		    mexPutVariable("base","kernel_debug_base",pdata);
		    printf("debug-kernel 2 global workspace: kernel_debug \n");
		  }
// 		  if (GPU_buffer!=NULL)
// 		  {
// 		    std::size_t ndims[4];
// 		    ndims[3]=bb[0];
// 		    ndims[2]=bb[1];
// 		    ndims[1]=bb[2];
// 		    ndims[0]=4;
// 		    pdata5 = mxCreateNumericArray ( 4,ndims,mhs::mex_getClassId<TData>(),mxREAL );
// 		    debug_data5= ( TData * ) mxGetData (pdata5 );
// 		    cudaMemcpy ( debug_data5 , GPU_buffer, max_buffer_size_gpu_vdim*GPU_num_total_voxels*sizeof(float) , cudaMemcpyDeviceToHost ) ;
// 		  }
		  
		}
		
		
		
		if (GPU_debug_kernel!=NULL)
		cudaFree ( GPU_debug_kernel) ;
		
		 
		
/*		
		float * GPU_result;
		float * CPU_result;
		cudaMalloc ( (void **)&GPU_result, num_total_blocks*sizeof(float)) ;
		CPU_result= new float[num_total_blocks];
		
		
 		float * GPU_debug_kernel=NULL;
// 		cudaMalloc ( (void **)&GPU_debug_kernel, GPU_num_total_voxels*sizeof(float)) ;
		
		 kernel_sfilter<<< num_total_blocks, num_threads, num_threads*sizeof(float) >>>( 
		  GPU_position,
		  GPU_v0,
		  GPU_v1,
		  GPU_v2,
		  GPU_offset,
		  GPU_box2kernel,
		  GPU_scales2,
		  GPU_scales4,
		  GPU_num_total_voxels,
		  GPU_bb,
		  t_imgGPU,
		  GPU_result,
		   GPU_debug_kernel);
		 
		 cudaMemcpy ( CPU_result , GPU_result, num_total_blocks*sizeof(float) , cudaMemcpyDeviceToHost ) ;
		  

		  for ( int i = 0 ; i<num_total_blocks ; i++ )
		  {
// 				printf("GPU %d, %f\n",i,CPU_result[i]);
				kernel_GPU_H+=CPU_result[i] ;
		  }
		  
		cudaFree ( GPU_result) ;
		delete [] CPU_result;
		
		

		
		
		
// 		if ((debug!=NULL)&&(debug->size()>0))
// 		{
// 		  {
// 		    std::size_t ndims[3];
// 		    ndims[2]=bb[0];
// 		    ndims[1]=bb[1];
// 		    ndims[0]=bb[2];
// 		    pdata5 = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<TData>(),mxREAL );
// 		    debug_data5= ( TData * ) mxGetData (pdata5 );
// 		    cudaMemcpy ( debug_data5 , GPU_debug_kernel, GPU_num_total_voxels*sizeof(float) , cudaMemcpyDeviceToHost ) ;
// 		    
// 		  }
// 		}
		
		
		
		if (GPU_debug_kernel!=NULL)
		cudaFree ( GPU_debug_kernel) ;
		
*/		
// 		kernel_GPU_H=0;
		/*delete [] CPU_debug_kernel;  */    
	    }
	    
	
	  
	  
	  T score=kernel_GPU_H;
	  
// 	  if (std::abs(kernel_CPU_H-kernel_GPU_H)>100*__FLT_EPSILON__)
// 	  {
// 	    printf("CPU vs GPU differ:  %f %f\n",kernel_CPU_H,kernel_GPU_H);
// 	  }
	  
	  
	  if (debug_data4!=NULL)
	  {
	  
	    printf("##############\n");
	    printf("kernel->global %f\n",radius);
	    sta_assert_error(pdata4!=NULL);
	    mexPutVariable("global","kernel4",pdata4);
	    
// 	    if (debug_data5!=NULL)
// 	    mexPutVariable("global","kernel5",pdata5);
// 	    printf("##############\n");
	     debug_id++;
	  }
	  
        if (debug!=NULL)
	{
	  debug->clear(); 
// 	  class CData<T,TData,Dim>::CDebugInfo dbinfo;
// 	  dbinfo.value=score;
// 	  dbinfo.name="score";
//  	  debug->push_back(dbinfo);
// // 	  
// // 	  dbinfo.value=sum;
// // 	  dbinfo.name="sum";
// //  	  debug->push_back(dbinfo);
// 	  
//  	  dbinfo.value=penalty;
// 	  dbinfo.name="penalty";
//  	  debug->push_back(dbinfo);	  
// // 	  
// 	  dbinfo.value=radius;
// 	  dbinfo.name="radius";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=stdv;
// 	  dbinfo.name="stdv";
//  	  debug->push_back(dbinfo);
//  
// 	  dbinfo.value=grad[0];
// 	  dbinfo.name="grad[0]";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=grad[1];
// 	  dbinfo.name="grad[1]";
//  	  debug->push_back(dbinfo);
	  
	  
// 
// 	  
// 	  dbinfo.value=tsum;
// 	  dbinfo.name="tsum";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=inhomog;
// 	  dbinfo.name="inhomog";
//  	  debug->push_back(dbinfo);

	  
// 	  dbinfo.value=min_max_box_radius_scale[0];
// 	  dbinfo.name="min_max_box_radius_scale[0]";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=min_max_box_radius_scale[1];
// 	  dbinfo.name="min_max_box_radius_scale[1]";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=min_max_box_radius_scale[2];
// 	  dbinfo.name="min_max_box_radius_scale[2]";
//  	  debug->push_back(dbinfo);
	}
  
	T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }
	
        result=-DataScale*score*scale1
	       +DataScaleGradVessel*penalty
               -DataThreshold;
	
	
	
	      
	       
	       
        return true;
    }
    
    

  

};

#endif
