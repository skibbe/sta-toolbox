#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"

#define EPSILON 0.00000000000001

#define SUB2IND(X, Y, Z, shape)  (((Z)*(shape[1])+(Y)) *(shape[2])+(X)) 
#define SUB2IND_2D(Y, Z, shape)  (((Z)*(shape[1])+(Y))) 
#include "mhs_vector.h"

 //mex mhs_simulate_dmri.cc -lgomp CXXFLAGS=" -O3   -Wfatal-errors  -std=c++11 -fopenmp-simd -fopenmp  -fPIC -march=native"

template<typename D,typename T,typename TData>
bool data_interpolate (
    const Vector<T,3> & pos,
    D & value,
    const TData  *  img,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode=1)
{

    
switch (mode)
{
   case 0:
    {
	int Z=std::floor ( pos[0]+T(0.5) );
	int Y=std::floor ( pos[1]+T(0.5) );
	int X=std::floor ( pos[2]+T(0.5) );

	if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}
	value=img[SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx];
	
    }break;
  case 1:
    {
      
   	T iz=pos[0];
	T iy=pos[1];
	T ix=pos[2];
	
	int Z=std::floor ( iz );
	int Y=std::floor ( iy );
	int X=std::floor ( ix );

	if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}

	D wz= ( iz-Z );
	D wy= ( iy-Y );
	D wx= ( ix-X );
	D & g=value;
	g=0;
        g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
    }break;
}

    return true;
}

/*
int ndirs = 64;
float dirs [] = {0.957082,0.901696,0.248801, 
0.542149,-0.679332,-0.878034, 
0.066274,-0.434525,0.064405, 
-0.078518,-0.916714,0.539551, 
-0.722351,-0.193440,0.096481, 
0.519871,-0.777143,-0.003657, 
-0.076887,0.713000,0.595818, 
0.653077,-0.289975,-0.526906, 
-0.740479,0.378118,0.251431, 
-0.224171,-0.472579,-0.251896, 
0.821934,-0.235974,-0.880742, 
-0.811401,0.832012,-0.950932, 
-0.747267,-0.923155,0.591710, 
0.450482,-0.553914,0.694784, 
-0.299979,-0.414526,0.979359, 
-0.118878,0.631821,0.073482, 
0.793682,-0.408694,-0.275679, 
0.338728,0.384929,0.529935, 
0.992022,0.180622,-0.130146, 
0.391437,-0.593921,-0.234747, 
0.834976,-0.402984,0.056797, 
0.681161,-0.077835,0.431846, 
0.854823,0.763682,0.176035, 
0.431082,-0.856087,-0.894811, 
-0.211115,-0.892810,-0.247955, 
-0.457508,-0.688112,0.371088, 
0.473206,-0.848395,-0.523725, 
0.993804,0.078281,0.334446, 
0.145223,-0.165586,-0.943219, 
0.805488,0.668981,-0.202863, 
0.090437,0.890058,0.396671, 
-0.674815,0.049481,-0.667079, 
0.445109,0.303220,-0.316273, 
0.081242,-0.341989,-0.254296, 
0.601777,0.432141,-0.115405, 
-0.660039,0.950378,-0.391179, 
-0.202000,-0.383606,-0.539819, 
-0.724689,0.543312,0.817651, 
-0.204773,-0.669432,0.809637, 
0.596623,0.125660,-0.957243, 
-0.972716,-0.705797,-0.779467, 
0.491778,0.039863,0.089266, 
-0.639883,-0.591230,-0.279171, 
0.021290,0.455386,0.350521, 
0.712405,-0.207904,-0.512565, 
-0.102479,0.975337,0.443538, 
-0.313296,0.706804,-0.068639, 
0.908226,-0.875653,-0.099803, 
-0.348943,-0.111083,0.993962, 
0.616260,-0.789881,0.738967, 
0.162026,-0.271216,-0.064470, 
0.903257,0.963641,-0.396917, 
0.786970,0.693667,0.567430, 
-0.706627,0.161773,-0.499685, 
-0.455771,-0.298544,0.569768, 
0.288301,-0.536417,-0.781230, 
-0.824537,-0.285697,-0.082428, 
-0.821673,0.007209,-0.915813, 
-0.556236,0.685147,-0.273644, 
0.405482,0.939185,-0.661154, 
-0.443078,0.602669,0.010139, 
-0.225967,-0.192058,0.590447, 
0.199221,-0.838480,-0.548841, 
0.910843,-0.766370,0.431818};
*/


int ndirs = 30;
float dirs [] = {0.231033,0.044775,0.971915, 
-0.253819,0.180376,0.950284, 
-0.118215,-0.313659,0.942148, 
0.336252,-0.379082,0.862109, 
0.243317,0.461612,0.853060, 
-0.186844,0.588051,0.786947, 
-0.527216,-0.168503,0.832857, 
-0.463819,-0.591655,0.659406, 
0.013046,-0.712591,0.701458, 
0.707691,-0.219655,0.671509, 
0.659146,0.221436,0.718674, 
0.102148,0.844197,0.526210, 
-0.559318,0.599952,0.572033, 
-0.725951,0.215301,0.653177, 
-0.837022,-0.232879,0.495138, 
0.066256,-0.942950,0.326276, 
0.506532,-0.668558,0.544477, 
0.943509,-0.048353,0.327800, 
0.854669,0.425959,0.296816, 
0.526385,0.656409,0.540413, 
-0.299857,0.907974,0.292692, 
-0.729483,0.663391,0.166634, 
-0.938122,0.263361,0.224872, 
-0.785015,-0.581423,0.213772, 
-0.393033,-0.861877,0.320457, 
0.462813,-0.874533,0.144897, 
0.840095,-0.484393,0.244137, 
0.984120,0.160544,-0.075723, 
0.568774,0.812348,0.128791, 
0.129253,0.987494,0.090278};


/*
int ndirs = 128;
float dirs [] = {0.140172,-0.016072,0.989997, 
-0.083378,0.032827,0.995977, 
-0.030411,-0.194607,0.980410, 
0.374817,-0.049230,0.925791, 
0.079931,0.214485,0.973451, 
-0.145629,0.267416,0.952513, 
-0.299452,0.081581,0.950617, 
-0.242980,-0.162660,0.956296, 
0.079560,-0.373950,0.924030, 
0.233021,-0.222001,0.946793, 
0.294910,0.174496,0.939457, 
0.236215,0.407453,0.882148, 
0.004105,0.421757,0.906700, 
-0.356335,0.302282,0.884110, 
-0.443037,-0.068585,0.893876, 
-0.324799,-0.351279,0.878128, 
-0.126908,-0.436968,0.890479, 
0.297534,-0.443540,0.845426, 
0.448642,-0.271240,0.851557, 
0.517138,0.101371,0.849878, 
0.451259,0.313316,0.835583, 
0.097021,0.590761,0.800992, 
-0.193558,0.502438,0.842669, 
-0.423336,0.469408,0.774882, 
-0.520691,0.152963,0.839930, 
-0.514453,-0.263462,0.816043, 
-0.321842,-0.552114,0.769148, 
-0.111141,-0.642084,0.758536, 
0.097590,-0.566739,0.818097, 
0.501186,-0.466277,0.728971, 
0.592675,-0.115834,0.797069, 
0.645540,0.288843,0.706999, 
0.406894,0.508413,0.758916, 
0.267856,0.666163,0.696046, 
-0.089276,0.680565,0.727228, 
-0.335278,0.642154,0.689367, 
-0.591581,0.350969,0.725846, 
-0.637232,-0.055287,0.768686, 
-0.690433,-0.270945,0.670740, 
-0.522411,-0.461561,0.716971, 
-0.287804,-0.726768,0.623680, 
0.092618,-0.735560,0.671099, 
0.311318,-0.633281,0.708545, 
0.656104,-0.311785,0.687254, 
0.772372,-0.107350,0.626033, 
0.698967,0.077161,0.710979, 
0.596969,0.487161,0.637418, 
0.460222,0.676510,0.574917, 
0.095913,0.779893,0.618521, 
-0.219521,0.787224,0.576272, 
-0.568199,0.562771,0.600366, 
-0.747814,0.354966,0.561046, 
-0.705062,0.148018,0.693526, 
-0.795193,-0.063678,0.603004, 
-0.684068,-0.476406,0.552348, 
-0.494412,-0.643726,0.584101, 
-0.077759,-0.820319,0.566596, 
0.262855,-0.796052,0.545168, 
0.502718,-0.637514,0.583824, 
0.679662,-0.496629,0.539833, 
0.812615,-0.287163,0.507143, 
0.825170,0.152418,0.543933, 
0.769071,0.373859,0.518421, 
0.654104,0.603832,0.455560, 
0.288332,0.822049,0.491020, 
-0.048698,0.874057,0.483376, 
-0.442294,0.742744,0.502700, 
-0.733449,0.518678,0.439347, 
-0.845131,0.156238,0.511217, 
-0.904829,-0.089009,0.416368, 
-0.820555,-0.296006,0.488948, 
-0.653545,-0.641320,0.401980, 
-0.461004,-0.788744,0.406644, 
-0.260982,-0.859118,0.440232, 
0.089499,-0.896843,0.433200, 
0.447754,-0.784639,0.428786, 
0.635984,-0.673089,0.377458, 
0.815253,-0.453488,0.360153, 
0.893555,-0.034443,0.447631, 
0.901767,0.242705,0.357647, 
0.809145,0.487117,0.328635, 
0.493246,0.783125,0.378713, 
0.125403,0.923090,0.363566, 
-0.279453,0.882868,0.377425, 
-0.475817,0.837094,0.269948, 
-0.617330,0.692120,0.373997, 
-0.874991,0.349817,0.334691, 
-0.936751,0.125839,0.326593, 
-0.915518,-0.291875,0.276830, 
-0.808134,-0.484947,0.334284, 
-0.590645,-0.773970,0.228273, 
-0.331385,-0.916358,0.224660, 
-0.100971,-0.943183,0.316560, 
0.288839,-0.904765,0.313004, 
0.502874,-0.836740,0.216759, 
0.762613,-0.609360,0.217030, 
0.919293,-0.238369,0.313177, 
0.968309,-0.010507,0.249535, 
0.906079,0.395869,0.149360, 
0.678127,0.690354,0.252103, 
0.510215,0.845028,0.160025, 
0.318274,0.912189,0.258094, 
-0.092938,0.960616,0.261877, 
-0.300517,0.942546,0.145935, 
-0.661637,0.734909,0.148812, 
-0.797292,0.558157,0.229754, 
-0.973146,0.192422,0.126337, 
-0.977406,-0.068994,0.199792, 
-0.883294,-0.459238,0.094295, 
-0.757693,-0.632283,0.161617, 
-0.485972,-0.871244,0.069030, 
-0.119499,-0.987977,0.098087, 
0.100377,-0.975151,0.197498, 
0.318547,-0.943715,0.089055, 
0.654063,-0.751269,0.088297, 
0.897044,-0.419918,0.137772, 
0.972679,-0.211528,0.095659, 
0.970263,0.184369,0.156837, 
0.806114,0.584563,0.092014, 
0.665767,0.745965,0.017063, 
0.306116,0.951597,0.027500, 
0.104297,0.985159,0.136321, 
-0.104303,0.994204,0.026073, 
-0.497200,0.866932,0.034950, 
-0.805776,0.592177,0.007110, 
-0.911482,0.399633,0.097434, 
-0.999928,-0.003833,-0.011358, 
-0.965689,-0.253511,0.056359};
*/
//#include "mhs_lapack.h"

//mex CXXFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "  CFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "   sta_EVGSL.cc -l gsl -l gslcblas -l gomp


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

  
    Vector<std::size_t,2>  target_shape;
    target_shape=std::size_t(0);
    
    Vector<std::size_t,3>  skip;
    skip=std::size_t(1);
    
    float min_sig = 0;
    
    int slice = -1;
      
    const mxArray *img;
    img = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(img);
    const int *dims = mxGetDimensions(img);

    bool normalize = true;
    
    float z_scale = 10;
    
    const mxArray * params=prhs[nrhs-1];
     if ( mxIsCell ( params  )  )
	  {
	      if ( mhs::mex_hasParam ( params,"shape" ) !=-1 )
		      {
		      target_shape=mhs::mex_getParam<T> ( params,"shape",2 ) ;
		      }
		      else return;
              
            if ( mhs::mex_hasParam ( params,"skip" ) !=-1 )
		      {
		      skip=mhs::mex_getParam<T> ( params,"skip",3 ) ;
		      }
               if ( mhs::mex_hasParam ( params,"slice" ) !=-1 )
		      {
		      slice=mhs::mex_getParam<int> ( params,"slice")[0] ;
		      }
		      
		      if ( mhs::mex_hasParam ( params,"min_sig" ) !=-1 )
		      {
		      min_sig=mhs::mex_getParam<float> ( params,"min_sig")[0] ;
		      }
		      if ( mhs::mex_hasParam ( params,"normalize" ) !=-1 )
		      {
		      normalize=mhs::mex_getParam<bool> ( params,"normalize")[0] ;
		      }      
		      if ( mhs::mex_hasParam ( params,"z_scale" ) !=-1 )
		      {
		      z_scale=mhs::mex_getParam<float> ( params,"z_scale")[0] ;
		      }      
      }

    
    printf("z-scale %f\n",z_scale);
    
    if (numdim==3)
    {   
        {
                    T *p_img = (T*) mxGetData(img);
                    std::size_t shape[3];  
                    shape[0] = dims[2];
                    shape[1] = dims[1];
                    shape[2] = dims[0];    
                    std::size_t numvoxel=shape[0]*shape[1]*shape[2];
                    
                    int ndims[3];
                    ndims[0]=ndirs;
                    ndims[1]=target_shape[0];
                    ndims[2]=target_shape[1];
                    plhs[0] = mxCreateNumericArray(3,ndims,mxGetClassID(img),mxREAL);
                    T *result = (T*) mxGetData(plhs[0]);
                    
                    
                    int ndims2[2];
                    ndims2[0]=target_shape[0];
                    ndims2[1]=target_shape[1];
                    plhs[1] = mxCreateNumericArray(2,ndims2,mxGetClassID(img),mxREAL);
                    T *result2 = (T*) mxGetData(plhs[1]);
                    
                    std::swap(target_shape[0],target_shape[1]);
                    
                    std::size_t numvoxel_tgt=target_shape[0]*target_shape[1];
                    
                    float scale_x = float(shape[2])/float(target_shape[1]);
                    float scale_y = float(shape[1])/float(target_shape[0]);
                    //float scale_z = float(shape[0])/float(target_shape[0]);
                    
                    //int bb_x_w = std::ceil(scale_x/2);
                    //int bb_y_w = std::ceil(scale_y/2);
                    float bb_x_w = (scale_x/2.0f);
                    float bb_y_w = (scale_y/2.0f);
                    
                    printf("%d %d %d\n",shape[0],shape[1],shape[2]);
                    printf("%d %d \n",target_shape[0],target_shape[1]);
                    printf("%f %f \n",bb_x_w,bb_y_w);
                    
                    printf("%d %d -> %d %d\n",(target_shape[1]-1),(target_shape[0]-1),std::max(int(std::floor((target_shape[1]-1) *scale_x)),0),std::max(int(std::floor((target_shape[0]-1) *scale_y)),0));
                    
                    
                    //return;
                    
                    #pragma omp parallel for num_threads(omp_get_num_procs())
                    for (std::size_t Y= 0; Y < target_shape[0]; Y++)    
                    {
                        for (std::size_t X= 0; X < target_shape[1]; X++)    
                        {
                            std::size_t indx_trget = SUB2IND_2D(X, Y, target_shape); 
                            
                            //std::size_t bby_l = std::max(int(Y*scale_y)-bb_y_w,0);
                            //std::size_t bby_r = std::min(int(Y*scale_y)+bb_y_w,int(shape[1]));
                            //std::size_t bbx_l = std::max(int(X*scale_x)-bb_x_w,0);
                            //std::size_t bbx_r = std::min(int(X*scale_x)+bb_x_w,int(shape[2]));
                            
                            std::size_t bby_l = std::max(int(std::floor(Y*scale_y)),0);
                            std::size_t bby_r = std::min(int(std::ceil(Y*scale_y+2*bb_y_w)),int(shape[1]));
                            std::size_t bbx_l = std::max(int(std::floor(X*scale_x)),0);
                            std::size_t bbx_r = std::min(int(std::ceil(X*scale_x+2*bb_x_w)),int(shape[2]));
                            
                            //for (std::size_t sZ= 0; sZ < shape[0]; sZ+=skip[2])
                            
                            //std::size_t sZ= 3;
                            for (std::size_t sZ= 0; sZ < shape[0]; sZ+=skip[2])
                            for (std::size_t sY= bby_l; sY < bby_r; sY+=skip[1])
                            for (std::size_t sX= bbx_l; sX < bbx_r; sX+=skip[0])
                            //std::size_t  sX = X;
                            //std::size_t  sY = Y;
                            {
                                
                            float d = 1.5;
                            std::size_t indx_src = SUB2IND(sX, sY, sZ, shape); 
                            /*
                            if (indx_src>=numvoxel)
                            {
                            printf("E01\n");
                                
                            }
                            if (indx_src<0)
                            {
                            printf("E02\n");
                                
                            }
                            */
                            float center =  p_img[indx_src];
                            
                            if (center>min_sig)
                            {
                                for (int nd=0;nd<ndirs;nd++)
                                {
                                    float * dirs_ = dirs + 3*nd;
                                    //Vector d(dirs_[0],dirs_[1],dirs_[2]);
                                    for (int t=1;t<3;t++)
                                    {
                                        for (int s=0;s<2;s++)
                                        {   
                                            float offset = (d*t) * (-1+s*2);
                                            //float offset = 2 * (-1+s*2);
                                            //float offset = 1;
                                            
                                            Vector<T,3> pos(dirs_[2]*offset,dirs_[1]*offset,dirs_[0]*offset);
                                            pos[0]/=z_scale;
                                            
                                            Vector<T,3> target_center(sZ,sY,sX);
                                            pos += target_center;
                                            
                                            float value;
                                            
                                            if (data_interpolate (pos,
                                                    value,
                                                    p_img,
                                                    shape,1,0,1))
                                                {
                                                    if (indx_trget<numvoxel_tgt)
                                                    {
                                                        //result[indx_trget*ndirs + nd]+=  std::max(center-value,0.0f);
                                                        
                                                        float v = center*std::max(center-value,0.0f);
                                                        result[indx_trget*ndirs + nd]+=  v;
                                                        result2[indx_trget]+=v;
                                                        //result2[indx_trget]+=center;
                                                    }
                                                    
                                                }
                                        }
                                    }
                                }
                            }
                            
                            }
                            
                        }
                    }
                    if (normalize)
                    {
                        printf("normalizing signal\n");
                        #pragma omp parallel for num_threads(omp_get_num_procs())
                        for (std::size_t idx= 0; idx < numvoxel_tgt; idx++)
                        {
                            if (result2[idx]>0)
                            {
                                for (int dim=0;dim<ndirs;dim++)
                                {
                                    result[idx*ndirs+dim]/=result2[idx];
                                }
                            }
                            
                        }
                    }else{
                        printf("not normalizing signal\n");
                    }
                    
        }
        
    }
        
        
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}
