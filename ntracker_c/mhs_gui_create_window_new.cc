#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"

#include <stdio.h>
#include <stdlib.h>

#include "mhs_vector.h"


#include <SDL2/SDL.h>
#include "SDL2/SDL_opengl.h"

 #ifdef _USE_CGC_
  #include "mhs_gui_shader_old.h"
#endif  
//#include "SDL2/SDL_thread.h"


struct TTracker_window
{
    SDL_Window *tracker_window;
    SDL_GLContext glcontext; 
 #ifdef _USE_CGC_    
    CGCprogram  *  _cgprogram;
#endif    
    std::string variable_name;
    bool active;
};

//static TTracker_window * tracker_data_p=NULL; 
static std::list<TTracker_window> tracker_data_list;
static int variable_count=0;


void  destroy_window(TTracker_window * tracker_data_p)
{
  

	    printf("SDL_GL_DeleteContext\n");
	    SDL_GL_DeleteContext(tracker_data_p->glcontext);  
	      //SDL_Delay(50);
	    printf("SDL_HideWindow\n");
	    SDL_HideWindow(tracker_data_p->tracker_window);
	    
	    printf("SDL_DestroyWindow\n");
	    SDL_DestroyWindow(tracker_data_p->tracker_window);
	    
 #ifdef _USE_CGC_	    
	    delete tracker_data_p->_cgprogram;
#endif	    
	    mxArray *arg1 = mxCreateString("global");
	    mxArray *arg2 = mxCreateString(tracker_data_p->variable_name.c_str());
	    mxArray *pargin[2] = {arg1, arg2};
	      
	    //clear('global','tracker_window_ptr2')
	    mexCallMATLAB(0, NULL,2,pargin, "clear");
	    
	    printf("done\n");
}
// 	    

//NOTE otherwise matlab dies
bool keep_last_window_alive=true;


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
  //cleanupObj = onCleanup(cleanupFun) 
  
  
//     if ((nrhs>0)&&((tracker_data_p!=NULL)))
    if (nrhs>0)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if ((mhs::mex_hasParam(params,"close")!=-1))
	{
	   std::size_t pointer=mhs::mex_getParam<uint64_t>(params,"close",1)[0];
	   
	   TTracker_window * tracker_data_p=NULL;
	   std::list<TTracker_window>::iterator del_iter;
	   for (std::list<TTracker_window>::iterator iter=tracker_data_list.begin();iter!=tracker_data_list.end();iter++)
	   {
	    TTracker_window  & tdata=*iter;
	    if (std::size_t(&tdata)==pointer)
	    {
	      tracker_data_p=&tdata;
	      del_iter=iter;
	    }
	   }
	   
	   if (tracker_data_p==NULL)
	   {
	    printf("not a valid window handle: %u\n",pointer); 
	    return;
	   }
	   
	   
	    
	   
	   if ((tracker_data_list.size()>1)||(!keep_last_window_alive))
	   {
	   
	   destroy_window(tracker_data_p);
	    
	    tracker_data_list.erase (del_iter);
	   }
	   else
	   {
	      tracker_data_p->active=false;
	      
	        for (int i=0;i<2;i++)
		{
		  SDL_Event event;
		  while ( SDL_PollEvent ( &event ) ) {
		  }
		  SDL_Delay(100);
		}
	      
	      SDL_FlushEvents(SDL_KEYDOWN,SDL_KEYUP);
	      SDL_HideWindow(tracker_data_p->tracker_window);
	      //printf("For m 2: SDL events might immediately close the window next time by sending Q key\n");
	   }
	  
	 return;     
	}
	
	
	if ((mhs::mex_hasParam(params,"handles")!=-1))
	{
	  //printf("listing all %d window handles\n",tracker_data_list.size());
	  int ndim=tracker_data_list.size();
	  plhs[0] = mxCreateNumericArray(1,&ndim,mxUINT64_CLASS,mxREAL);
	  std::size_t * handle_p=(std::size_t  *)mxGetPr(plhs[0]);
	  
	  for (std::list<TTracker_window>::iterator iter=tracker_data_list.begin();iter!=tracker_data_list.end();iter++)
	   {
	    TTracker_window  & tdata=*iter;
	    *handle_p=(std::size_t)(&tdata);
	    handle_p++;
	   }
	  
	return;
	}
	
	if ((mhs::mex_hasParam(params,"handle")!=-1))
	{
	  
	  std::size_t pointer=mhs::mex_getParam<uint64_t>(params,"handle",1)[0];
	  
	   TTracker_window * tracker_data_p=NULL;
	   std::list<TTracker_window>::iterator del_iter;
	   for (std::list<TTracker_window>::iterator iter=tracker_data_list.begin();iter!=tracker_data_list.end();iter++)
	   {
	    TTracker_window  & tdata=*iter;
	    if (std::size_t(&tdata)==pointer)
	    {
	      tracker_data_p=&tdata;
	      del_iter=iter;
	    }
	   }
	   
	   if (tracker_data_p==NULL)
	   {
	    printf("not a valid window handle: %u\n",pointer); 
	    return;
	   }
	  
	  int width;
	  int height;
	  SDL_GetWindowSize(tracker_data_p->tracker_window,&width,&height);
	  printf("%d %d\n",width,height);
	  
	  if (nrhs>0)
	  {
	      const mxArray * params=prhs[nrhs-1] ;

	      if (mhs::mex_hasParam(params,"w")!=-1)
		  width=mhs::mex_getParam<int>(params,"w",1)[0];
	      if (mhs::mex_hasParam(params,"h")!=-1)
		  height=mhs::mex_getParam<int>(params,"h",1)[0];
	      
		printf("%d %d\n",width,height);
	      if (width<1)
		return;
	      if (height<1)
		return;
	  SDL_SetWindowSize(tracker_data_p->tracker_window,width,height);
	 
	  return;
	}
	}
    }
    
    
    
  {
    
   if (((tracker_data_list.size()==1)&&(!tracker_data_list.back().active))&&(keep_last_window_alive))
     
   {
    
      TTracker_window * tracker_data_p=&(tracker_data_list.back());
      SDL_ShowWindow(tracker_data_p->tracker_window);
      tracker_data_p->active=true;
       {
	 int ndim=1;
	plhs[0] = mxCreateNumericArray(1,&ndim,mxUINT64_CLASS,mxREAL);
      std::size_t * handle_p=(std::size_t  *)mxGetPr(plhs[0]);
      *handle_p=(std::size_t)(tracker_data_p);
      }
     return;
   }
    
    
    
   if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr,
                "\nUnable to initialize SDL:  %s\n",
                SDL_GetError()
               );
        return;
    }
    
    
//     SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
//     SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
//     SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
//     SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
//     
//     SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    
    if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1)<0)
      printf("SDL_GL_DOUBLEBUFFER failed\n");
    
    if (SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1)<0)
      printf("SDL_GL_MULTISAMPLEBUFFERS failed\n");
      
    if (SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4))
      printf("SDL_GL_MULTISAMPLESAMPLES failed\n");
    
     int width=1000;
    int height=1000;    
    if (nrhs>0)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"w")!=-1)
            width=mhs::mex_getParam<int>(params,"w",1)[0];
	if (mhs::mex_hasParam(params,"h")!=-1)
            height=mhs::mex_getParam<int>(params,"h",1)[0];
	
	   printf("%d %d\n",width,height);
	if (width<1)
	  return;
	if (height<1)
	  return;
	//SDL_SetWindowSize(tracker_data_p->tracker_window,width,height);
    }
    
    
    TTracker_window tracker_data;
    
    std::stringstream title;
    title<<"GL window "<<variable_count;
    
    // probably SDL_WINDOW_FOREIGN can fix this crash on exit
    tracker_data.tracker_window = SDL_CreateWindow(
       //"OpenGL Tracker Gui", 0, 0, width, height,
      title.str().c_str(), 0, 0, width, height, 
       SDL_WINDOW_OPENGL);
    
    
    
    
    // Create an OpenGL context associated with the window.
    tracker_data.glcontext = SDL_GL_CreateContext(tracker_data.tracker_window);

#ifdef _USE_CGC_    
    tracker_data._cgprogram = new CGCprogram();
#endif    
    //delete _cgprogram;
    
//     tracker_data_p=&tracker_data;
    
    std::stringstream vname; 
    vname<<"GL_window_ptr"<<variable_count++;
    tracker_data.variable_name=vname.str();
    
    tracker_data.active=true;
    
    
    tracker_data_list.push_back(tracker_data);
    
    TTracker_window * tracker_data_p=&(tracker_data_list.back());
    
    int ndim=1;
    mxArray *handle = mxCreateNumericArray(1,&ndim,mxUINT64_CLASS,mxREAL);
     std::size_t * handle_p=(std::size_t  *)mxGetPr(handle);
     *handle_p=(std::size_t)(tracker_data_p);
     
    mexPutVariable("global", tracker_data.variable_name.c_str(), handle );
    
    {
      plhs[0] = mxCreateNumericArray(1,&ndim,mxUINT64_CLASS,mxREAL);
     std::size_t * handle_p=(std::size_t  *)mxGetPr(plhs[0]);
     *handle_p=(std::size_t)(tracker_data_p);
    }
    
  }
  


}