#ifndef MHS_DATA_SIMPLE_H
#define MHS_DATA_SIMPLE_H


#include "mhs_data.h"
#include "mhs_graphics.h"


template <typename T,typename TData,int Dim>
class CDataSimple: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * img;

    const TData * debug;
    T min_scale;
    T max_scale;

    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
    T DataScaleGrad;
    T Border;
    int debug_id;

//     std::size_t shape[3];


public:

    float get_minscale()
    {
        return min_scale;
    };
    float get_maxscale()
    {
        return max_scale;
    };
    float get_scalecorrection()
    {
          return 1;
    };


    CDataSimple()
    {
      Border=0.2;
      debug_id=0;
    }

    ~CDataSimple()
    {
    }
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle)
    {
      if (handle!=NULL)
      {
	  const mxArray *
	  parameter= mxGetField(handle,0,(char *)("tracker_data"));
	  if (parameter!=NULL) 
	  {
		  DataThreshold=-(*((double*) mxGetPr(parameter)));
		  DataScale=*(((double*) mxGetPr(parameter))+2);
		  DataScaleGradVessel=*(((double*) mxGetPr(parameter))+3);
		  DataScaleGrad=*(((double*) mxGetPr(parameter))+4);
	  }
	  printf("Th: %f,  Da %f, Sigma %f, - %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
      }
    }
    void set_controls(const mxArray * handle)
    {
	  if (handle!=NULL)
	    {
	    const mxArray *
	    parameter= mxGetField(handle,0,(char *)("tracker_data"));
	    if (parameter!=NULL) 
	    {
		  *((double*) mxGetPr(parameter))=-DataThreshold;
		  *(((double*) mxGetPr(parameter))+2)=DataScale;
		  *(((double*) mxGetPr(parameter))+3)=DataScaleGradVessel;
		  *(((double*) mxGetPr(parameter))+4)=DataScaleGrad;
	    }
	  }  
	  printf("Th: %f,  Da %f, Sigma %f, - %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
    }
    #endif    
    


    void init(const mxArray * feature_struct)
    {
        for (int i=0; i<3; i++)
            this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
        std::swap(this->shape[0],this->shape[2]);

        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"img");
            img=tmp.data;
            sta_assert(tmp.dim.size()==3);
        }

	 
	try {
	  const TData * scales=mhs::dataArray<TData>(feature_struct,"override_scales").data;
	  min_scale=scales[0];
	  max_scale=scales[mhs::dataArray<TData>(feature_struct,"override_scales").dim[0]-1];
        } catch (mhs::STAError error)
        {
	    const TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	    min_scale=scales[0];
	    max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
        }
        printf("scale: [%f %f]\n",min_scale,max_scale);
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGradVessel=1;
        DataScaleGrad=1;
        DataThreshold=1;
	Border=0.2;
	
        if (params!=NULL)
        {
	  try
	  {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGrad")!=-1)
                DataScaleGrad=mhs::mex_getParam<T>(params,"DataScaleGrad",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
	    
	    if (mhs::mex_hasParam(params,"Border")!=-1)
                Border=mhs::mex_getParam<T>(params,"Border",1)[0];
	    } catch(mhs::STAError & error)
	  {
	      throw error;
	  }
        }

    }

private:



private:
  
    bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius
		  #ifdef D_USE_GUI        
        //, std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
        , std::list<std::string>  * debug=NULL
#endif     
      
    )
    {
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;

	
	if ((radius<min_scale-0.01)
	  ||(radius>max_scale+0.01))
	{
	  printf("[%f %f %f]\n",min_scale,radius,max_scale);
 	  sta_assert_error(!(radius<min_scale));
 	  sta_assert_error(!(radius>max_scale));
	}
	radius=std::min(max_scale,std::max(radius,min_scale));
	
	
	
	
	Vector<T,3> vn0=direction;
	Vector<T,3> vn1;
	Vector<T,3> vn2;
	mhs_graphics::createOrths(vn0,vn1,vn2); 
    
// 	T sigma=DataScaleGradVessel*radius;
// 	sigma*=sigma;
	
 	T  minv=std::numeric_limits<T>::max();
	T  maxv=std::numeric_limits<T>::min();
	
	int numsteps=3;
	
	T s[3];
	s[0]=radius;
	s[1]=radius;
	s[2]=radius;
	
	T t[3];
// 	t[0]=s[0];
// 	t[1]=s[1];
// 	t[2]=s[2];
// 	t[0]=std::min(std::max(s[0],T(2)),T(3));
// 	t[1]=std::min(std::max(s[1],T(2)),T(3));
// 	t[2]=std::min(std::max(s[2],T(2)),T(3));
	t[0]=2;
	t[1]=2;
	t[2]=2;
	
 	int bb[3];
// 	b[0]=t[0]*3;
// 	b[1]=t[1]*3;
// 	b[2]=t[2]*3;
	
	int  kernel=0;
	
	T border=3;
	T box2img[3];
	T box2kernel[3];
	
	
	switch (kernel)
	{
	  case 0:
	  {
	    border=3;
	  }break;
	  case 1:
	  {
	    border=2.1;
	  }break;
	}
// 	for (int i=0;i<3;i++)
// 	{
// // 	  min_max_box_radius_scale[i]=std::min(std::max(radius,T(3)),T(6));
// 	  box2kernel[i]=t[i];
// 	  bb[i]=std::ceil(border*box2kernel[i]);
// // 	  box2img[i]=t[i]/box2kernel[i];
// 	  box2img[i]=1;
// 	  box2kernel[i]=1;
// 	}
	for (int i=0;i<3;i++)
	{
//   	  T b=std::min(std::max(radius,T(2)),T(3));
// 	  box2kernel[i]=t[i];
	  
	  bb[i]=std::ceil(border*t[i]);
 	  box2kernel[i]=s[i]/t[i];
 	  box2img[i]=s[i]/t[i];
// 	  box2img[i]=1;
// 	  box2kernel[i]=1;
	}

	T s1_1=s[0];
	T s2_1=s[1];
	T s3_1=s[2];
	T s1_2=s1_1*s1_1;
	T s2_2=s2_1*s2_1;
	T s3_2=s3_1*s3_1;
	T s1_4=s1_2*s1_2;
	T s2_4=s2_2*s2_2;
	T s3_4=s3_2*s3_2;
	
	
	
	T * debug_data=NULL;
	 mxArray * pdata;
	 T * debug_data2=NULL;
	 mxArray * pdata2;
	int debug_data_dim=0;
// 	if (debug_id==0)
	if ((debug!=NULL)&&(debug->size()>0))
	{
	  debug->clear();
// 	  if (radius>5)
	  {
	    std::size_t ndims[3];
	    ndims[2]=bb[0]*2+1;
	    ndims[1]=bb[1]*2+1;
	    ndims[0]=bb[2]*2+1;
	    debug_data_dim=ndims[0]*ndims[1]*ndims[2];
	    pdata = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    debug_data= ( T * ) mxGetData (pdata );
	    pdata2 = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<T>(),mxREAL );
	    debug_data2= ( T * ) mxGetData (pdata2 );
	  }/*else{
	   printf("%f\n",radius); 
	  }*/
	}
	
	
	T n_k=0;
	T n_i=0;
	T mean_k=0;
	T mean_i=0;

	T score=0;
	T sum=0;
	int tsum=0;
	Vector<T,3> p;
	
	
	n_k=1;
	n_i=1;
	
	  for (int z=-bb[0];z<=bb[0];z++)
	    {
	      T pz=z*box2img[0];
 	      T kz=z*box2kernel[0];
	      T tmp=T(z)/bb[0];
	      T rz=tmp*tmp;
   	    if (rz>1)
   	      continue;

	      for (int y=-bb[1];y<=bb[1];y++)
	      {
		T py=y*box2img[1];
 		T ky=y*box2kernel[1];
// 		T ky=py;
		T tmp=T(y)/bb[1]; 
		T ry=tmp*tmp;
 		 if (ry+rz>1)
 		  continue;
		for (int x=-bb[2];x<=bb[2];x++)
		{
		    T px=x*box2img[2];
 		    T kx=x*box2kernel[2];
// 		    T kx=px;
		    T tmp=T(x)/bb[2]; 
		    T rx=tmp*tmp;
		    if (rx+ry+rz>1)
		      continue;
		    p=position+vn0*pz+vn1*px+vn2*py;
		    
		    int Z=std::floor(p[0]);
		    int Y=std::floor(p[1]);
		    int X=std::floor(p[2]);
		    
		    const int boundary=1;
		    if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
		    {
		    return false; 
		    }
		    
		    T center=0.0f;
		    T wz=(p[0]-Z);
		    T wy=(p[1]-Y);
		    T wx=(p[2]-X);
		    {
			center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
			center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
			center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
			center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
			center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
			center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
			center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
			center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
		    }
		    
		    
		    T pol=0;
		    T pol_t=0;
		    pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
		    pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/T(2);
	    
		    T g=mexp(pol_t);
		    
		    
		    
		    T h_kernel=g*pol;
		    T h_img=center;
// 			T h_img=center;
		    
		    sum+=std::abs(center);
		    score+=h_img*h_kernel;
// 			score+=center*h_kernel;
			
			if (debug_data!=NULL)
			{
			  int dshape[3];
			  dshape[0]=bb[0]*2+1;
			  dshape[1]=bb[1]*2+1;
			  dshape[2]=bb[2]*2+1;
			  int indx=((bb[0]+z)*dshape[1]+(bb[1]+y))*dshape[2]+(bb[2]+x);
			  sta_assert_error(indx>=0);
			  sta_assert_error(indx<debug_data_dim);
			  
			  if ((indx<0)||(indx>debug_data_dim-1))
			  {
			    printf("? %d %d \n",indx,debug_data_dim);
			  }else{
			    debug_data[indx]=h_kernel;
			    debug_data2[indx]=h_img;
			  }
			}
		    
		}
	      }
	    }

	
	
	
// 	for (int mode=0;mode<2;mode++)
// 	{
// 	   switch (mode)
// 		    {
// 		      case 0:
// 		      {
// 		      }break;
// 		      
// 		      case 1:
// 		      {
// 			mean_k/=tsum;
// 			mean_i/=tsum;
// 		      }break;
// 		    }
// 	
// 	  for (int z=-bb[0];z<=bb[0];z++)
// 	    {
// 	      T pz=z*box2img[0];
//  	      T kz=z*box2kernel[0];
// // 	      T kz=pz;
// 	      T tmp=T(z)/bb[0];
// 	      T rz=tmp*tmp;
//    	    if (rz>1)
//    	      continue;
// 
// 	      for (int y=-bb[1];y<=bb[1];y++)
// 	      {
// 		T py=y*box2img[1];
//  		T ky=y*box2kernel[1];
// // 		T ky=py;
// 		T tmp=T(y)/bb[1]; 
// 		T ry=tmp*tmp;
//  		 if (ry+rz>1)
//  		  continue;
// 		for (int x=-bb[2];x<=bb[2];x++)
// 		{
// 		    T px=x*box2img[2];
//  		    T kx=x*box2kernel[2];
// // 		    T kx=px;
// 		    T tmp=T(x)/bb[2]; 
// 		    T rx=tmp*tmp;
// 		    if (rx+ry+rz>1)
// 		      continue;
// //    		  if (r+tmp*tmp>1)
// //    		    continue;
// 		  
//   // 		  if (px*px+py*py+pz*pz>3)
//   // 		  {
//   // 		   continue; 
//   // 		  }
// 		    
// 		    p=position+vn0*pz+vn1*px+vn2*py;
// 		    
// 		    int Z=std::floor(p[0]);
// 		    int Y=std::floor(p[1]);
// 		    int X=std::floor(p[2]);
// 		    
// 		    const int boundary=1;
// 		    if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 		    {
// 		    return false; 
// 		    }
// 		    
// 		    T center=0.0f;
// 		    T wz=(p[0]-Z);
// 		    T wy=(p[1]-Y);
// 		    T wx=(p[2]-X);
// 		    {
// 			center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 			center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 			center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 			center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 			center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 			center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 			center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 			center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		    }
// 		    
// 
// // 		    if (std::abs(z)>0.25*box2kernel[0])
// // 		      continue;
// //   		    T pol=(kx*kx/s3_2 + ky*ky/s2_2 )/T(2);
// //   		    pol=(0.5-pol*pol)/(std::pow(M_PI,3.0/2.0)*s3_1*s2_1);
// //      		    T pol_t=(kx*kx/s3_2 + ky*ky/s2_2)/T(2);
// //   		    pol_t*=pol_t;
// 
// 		    
// //     		    T pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
// //    		    T pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/T(2);
// 
// //  		    T pol=(kx*kx/s3_2 + ky*ky/s2_2 )/T(2);
// //  		    pol=(0.5-pol*pol)/(std::pow(M_PI,3.0/2.0)*s3_1*s2_1);
// //     		    T pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/T(2);
// //  		    pol_t*=pol_t;
// 		    
// 		    T pol=0;
// 		    T pol_t=0;
// 		    switch (kernel)
// 		    {
// 		      case 0:
// 		      {
// 			   pol=(  s3_2*s2_4  -  s2_4*kx*kx + s3_4 *s2_2 - s3_4*ky*ky  )/(s2_4*s3_4);
// 			   pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/T(2);
// 		      }break;
// 		      case 1:
// 		      {
// 			    pol=(kx*kx/s3_2 + ky*ky/s2_2 )/T(2);
// 			    pol=(0.5-pol*pol)/(std::pow(M_PI,3.0/2.0)*s3_1*s2_1);
//           		    pol_t=(kx*kx/s3_2 + ky*ky/s2_2 +kz*kz/s1_2)/T(2);
// 			    pol_t*=pol_t;
// 		      }break;
// 		    }
// 		    
// 		    
// 		    /*T g=std::exp(-pol_t)*/;
// 		    T g=mexp(pol_t);
// 		    
// 		    
// 		    switch (mode)
// 		    {
// 		      case 0:
// 		      {
// 			mean_k+=pol*g;
// 			mean_i+=center;
// 			tsum++;
// 		      }break;
// 		      
// 		      case 1:
// 		      {
// 			T h_kernel=g*pol-mean_k;
//  			T h_img=center-mean_i;
// // 			T h_img=center;
// 			
// 			sum+=std::abs(center);
// 			
// 			n_k+=h_kernel*h_kernel;
// 			n_i+=h_img*h_img;
// // 			sum+=std::abs(g);
// 			
//  			score+=h_img*h_kernel;
// // 			score+=center*h_kernel;
// 			
// 			if (debug_data!=NULL)
// 			{
// 			  int dshape[3];
// 			  dshape[0]=bb[0]*2+1;
// 			  dshape[1]=bb[1]*2+1;
// 			  dshape[2]=bb[2]*2+1;
// 			  int indx=((bb[0]+z)*dshape[1]+(bb[1]+y))*dshape[2]+(bb[2]+x);
// 			  sta_assert_error(indx>=0);
// 			  sta_assert_error(indx<debug_data_dim);
// 			  
// 			  if ((indx<0)||(indx>debug_data_dim-1))
// 			  {
// 			    printf("? %d %d \n",indx,debug_data_dim);
// 			  }else{
// 			    debug_data[indx]=h_kernel;
// 			    debug_data2[indx]=h_img;
// 			  }
// 			}
// 		      }break;
// 		    }
// 		}
// 	      }
// 	    }
// 	}
	
// 	  if (sum<std::numeric_limits<T>::epsilon())
	  if (sum/tsum<0.001)
	    return false;
	  
	  
	  
	
	
	T inhomog=0;	
// 	{
// 	  
// 	  int N=std::floor(2*M_PI*radius+1);
// 	  int r_steps=std::ceil(radius);
// 	  for (int R=0;R<r_steps;R++)
// 	  {
// 	    T grad_max=0;
// 	    T r=(radius*R)/r_steps;
// 	    T center_old;
// 	    for (int n=0; n<N+1; n++)
// 	    {
// 	      T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
//   	      p=position+vn1*(x*r)+vn2*(y*r);
// 	      T center=0.0f;  
// 	      
// 	      int Z=std::floor(p[0]);
// 	      int Y=std::floor(p[1]);
// 	      int X=std::floor(p[2]);
// 	      
// 	      const int boundary=1;
// 	      if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	      {
// 	      return false; 
// 	      }
// 	      
// 	      
// 	      T wz=(p[0]-Z);
// 	      T wy=(p[1]-Y);
// 	      T wx=(p[2]-X);
// 	      {
// 		  center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	      
// 	      if (n>0)
// 	      {
// 		T grad=center-center_old;
// 		grad_max=std::max(grad_max,std::abs(grad));
// 	      }
// 	      
// 	      center_old=center;
// 	    }
// 	    inhomog+=grad_max;
// 	  }
// 	  inhomog/=r_steps;
// 	  inhomog/=std::sqrt(n_i)+0.000001;;
// 	}
	  
	  if (debug_data!=NULL)
	  {
	    
	    printf("##############\n");
	    printf("kernel->global %f\n",radius);
	    mexPutVariable("global","kernel",pdata);
	    mexPutVariable("global","kernel2",pdata2);
	    printf("##############\n");
	     debug_id++;
	  }
	  
// 	  return false;

// 	  printf("%f\n",mean_k);
//    	  score/=sum;
// 	  printf("%f %f %f\n",std::sqrt(n_k),std::sqrt(n_i),score);
	  score/=std::sqrt(n_k*n_i)+0.000001;
//     	  score*=radius;
//  	  score*=radius;
		
	  
	 
	  
	#ifdef D_USE_GUI        
        if (debug!=NULL)
	{
	  class CData<T,TData,Dim>::CDebugInfo dbinfo;
	  dbinfo.value=score;
	  dbinfo.name="score";
 	  debug->push_back(dbinfo);
	  
	  dbinfo.value=sum;
	  dbinfo.name="sum";
 	  debug->push_back(dbinfo);
	  
	  
	  dbinfo.value=radius;
	  dbinfo.name="radius";
 	  debug->push_back(dbinfo);

	  
	  dbinfo.value=tsum;
	  dbinfo.name="tsum";
 	  debug->push_back(dbinfo);
	  
	  dbinfo.value=inhomog;
	  dbinfo.name="inhomog";
 	  debug->push_back(dbinfo);

	  
// 	  dbinfo.value=min_max_box_radius_scale[0];
// 	  dbinfo.name="min_max_box_radius_scale[0]";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=min_max_box_radius_scale[1];
// 	  dbinfo.name="min_max_box_radius_scale[1]";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=min_max_box_radius_scale[2];
// 	  dbinfo.name="min_max_box_radius_scale[2]";
//  	  debug->push_back(dbinfo);
	}
	#endif    
	
	
        result=-DataScale*score
               -DataThreshold*radius;
	
	
	
// 	T radius_outer_ends=radius*T(1.5);
// 	T radius_outer_starts=radius*T(1.2);
// 	
// 	int bb_r=radius_outer_ends;
// // 	int bb_v=3;//radius*T(0.25);
// 	int bb_v=1+radius*T(0.5);
// 	
// 	T vote_neg=0;
// 	T vote_pos=0;
// 	T vote_neg_c=0;
// 	T vote_pos_c=0;
//  	T vote_sum=0;
// 	T mean=0;
// 	T stddev=0;
// 	T sum=0;
// 	
// 	for (int step=0;step<3;step++)
// 	{
// 	  switch (step)
// 		  {
// 		    case 1:
// 		    {
// 		    mean/=sum;
// 		    } break;
// 		    case 2:
// 		    {
// 		    stddev/=sum;
// 		    stddev=std::sqrt(stddev)+0.01;
// 		    } break;
// 		  }
// 	  for (int z=-bb_v;z<=bb_v;z++)
// 	  {
// 	    for (int y=-bb_r;y<=bb_r;y++)
// 	    {
// 	      for (int x=-bb_r;x<=bb_r;x++)
// 	      {
// 		  p=position+vn0*z+vn1*x+vn2*y;
// 		  
// 		  int Z=std::floor(p[0]);
// 		  int Y=std::floor(p[1]);
// 		  int X=std::floor(p[2]);
// 		  
// 		  const int boundary=1;
// 		  if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 		  {
// 		  return false; 
// 		  }
// 		  
// 		  T center=0.0f;
// 		  T wz=(p[0]-Z);
// 		  T wy=(p[1]-Y);
// 		  T wx=(p[2]-X);
// 		  {
// 		      center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		      center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		      center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		      center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		      center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		      center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		      center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		      center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  }
// 		  T rad=std::sqrt(x*x+y*y);
// 
// 		  switch (step)
// 		  {
// 		    case 0:
// 		    {
// 		      if (rad<radius)
// 		      {
// 			
// 			mean+=center;
// 			sum++;
// 		      }
// 		    } break;
// 		    case 1:
// 		    {
// 		      if (rad<radius)
// 		      {
// 			stddev+=(center-mean)*(center-mean);
// 		      }
// 		    } break;
// 		    case 2:
// 		    {
// 		      center/=stddev;
// 		      if (rad<radius)
// 		      {
// 		      vote_pos+=center; 
// 		      vote_pos_c++;
// 		      vote_sum+=center*center;
// 		      }else if ((rad>radius_outer_starts)&&(rad<radius_outer_ends))
// 		      {
// 			vote_neg+=center; 
// 			vote_neg_c++;
// 			vote_sum+=center*center;
// 		     
// 		      }
// 		    } break;
// 		  }
// 		  
// 		  
// 	      }
// 	    }
// 	  }
// 	}
// 	
// 	if (vote_pos_c<1)
// 	  return false;
// 	if (vote_neg_c<1)
// 	  return false;
// 	
// 	vote_pos/=vote_pos_c;
// 	vote_neg/=vote_neg_c;
// // 	vote_sum=std::sqrt(vote_sum);
// 	T score=radius*(vote_pos-DataScaleGrad*vote_neg);///(vote_sum+0.01);
// 	
// 	
// 	#ifdef D_USE_GUI        
//         if (debug!=NULL)
// 	{
// 	  class CData<T,TData,Dim>::CDebugInfo dbinfo;
// 	  dbinfo.value=score;
// 	  dbinfo.name="score";
//  	  debug->push_back(dbinfo);
// 
// 	  dbinfo.value=vote_pos_c;
// 	  dbinfo.name="vote_pos_c";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=vote_neg_c;
// 	  dbinfo.name="vote_neg_c";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=vote_neg_c/vote_pos_c;
// 	  dbinfo.name="vote n/p ratio";
//  	  debug->push_back(dbinfo);
// 	  
// 	  dbinfo.value=stddev;
// 	  dbinfo.name="stddev";
//  	  debug->push_back(dbinfo);	  
// 
// 	  dbinfo.value=mean;
// 	  dbinfo.name="mean";
//  	  debug->push_back(dbinfo);		  
// 	  
// 	  dbinfo.value=sum;
// 	  dbinfo.name="sum";
//  	  debug->push_back(dbinfo);		  
// 	}
// 	#endif    
// 	
	
// 	vote_sum=std::sqrt(vote_sum);
// 	T score=radius*(vote_pos-vote_neg)/(std::sqrt(vote_neg_c+vote_pos_c)*vote_sum+0.01);
	
	
// 	int N=std::floor(2*M_PI*radius+1);
// 	
// 
// 	Vector<T,3> dir[3];
// 	Vector<T,3> gp;
// 	dir[0]=direction;
// 	
// 	T mean=0;
// 	T var=0;
// 	//for (int z=0;z<numsteps;z++)
// 	{
// 	  int votes=0;  
// 	  Vector<T,3> pos=position;
// 	  
// 	  for (int n=0; n<N; n++)
// 	  {
// 	      T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      
// 	      dir[1]=vn1*x+vn2*y;
// 	      dir[2]=dir[1].cross(dir[0]);
//   	      p=pos+dir[1]*radius;
// 	      
//   	      int Z=std::floor(p[0]);
// 	      int Y=std::floor(p[1]);
// 	      int X=std::floor(p[2]);
// 	      
// 	      const int deriv_skip=1;
// 	      const int boundary=deriv_skip+1;
// 	      if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	      {
// 	      return false; 
// 	      }
// 	      
// 	      T center;
// 	      T wz=(p[0]-Z);
// 	      T wy=(p[1]-Y);
// 	      T wx=(p[2]-X);
// 	      {
// 		  center=0.0f;
// 		  center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	      mean+=center;
// 	      votes++;
// 	  }
// 	  mean/=votes;
// 	  
// 	  for (int n=0; n<N; n++)
// 	  {
// 	      T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      
// 	      dir[1]=vn1*x+vn2*y;
// 	      dir[2]=dir[1].cross(dir[0]);
//   	      p=pos+dir[1]*radius;
// 	      
//   	      int Z=std::floor(p[0]);
// 	      int Y=std::floor(p[1]);
// 	      int X=std::floor(p[2]);
// 	      
// 	      const int deriv_skip=1;
// 	      const int boundary=deriv_skip+1;
// 	      if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	      {
// 	      return false; 
// 	      }
// 	      
// 	      T center;
// 	      T wz=(p[0]-Z);
// 	      T wy=(p[1]-Y);
// 	      T wx=(p[2]-X);
// 	      {
// 		  center=0.0f;
// 		  center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	      var+=(center-mean)*(center-mean);
// 	  }	  
// 	  var/=votes;
// 	}
// 	
// 	
// 	
// 	int votes=0;
// 	T score=0;
// 	T thick_2length=1/4.0;
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	  
//  	Vector<T,3> pos=position+direction*((z-numsteps/2)*thick_2length);
// // 	  Vector<T,3> pos=position;
// 	
// 
// 	Vector<T,3> grad;
// 	for (int n=0; n<N; n++)
// 	{
// 	    T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	    T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	    
// 	    dir[1]=vn1*x+vn2*y;
// 	    dir[2]=dir[1].cross(dir[0]);
// // 	    printf("%f %f\n",dir[1].norm2(),dir[2].norm2());
// 	    
// 	    p=pos+dir[1]*radius;
// 	    
// // 	    dir[1].print();
// // 	    ((p-pos)/std::sqrt((p-pos).norm2())).print();
// 	    
// 	    int Z=std::floor(p[0]);
// 	    int Y=std::floor(p[1]);
// 	    int X=std::floor(p[2]);
// 	    
// 	    const int deriv_skip=1;
// 	    const int boundary=deriv_skip+1;
// 	    if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	    {
// 	     return false; 
// 	    }
// 	    
// 	    T center;
// 	    T wz=(p[0]-Z);
// 	    T wy=(p[1]-Y);
// 	    T wx=(p[2]-X);
// 	    {
// 		
// 		center=0.0f;
// 		center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	    }
// 	    
// 	    if (center<0.01)
// 	      return false;
// 	    
// 	    for (int d=0;d<3;d++)
// 	    {
// 	      gp=p+dir[d]*deriv_skip;
// 	      
// 	      Z=std::floor(gp[0]);
// 	      Y=std::floor(gp[1]);
// 	      X=std::floor(gp[2]);
// 
// 	      
// 	       const int boundary=3;
// 		if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 		{
// // 		  printf("THIS SHOULDNT HAPPEN %f %f %f\n",Z,Y,X);
// 		return false; 
// 		}
// 		
// 	      wz=(gp[0]-Z);
// 	      wy=(gp[1]-Y);
// 	      wx=(gp[2]-X);
// 	      {
// 		  T & g =grad[d];
// 		  g=0.0f;
// 		  g+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  g+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  g+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  g+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  g+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  g+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  g+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  g+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	    }
// 	    grad-=center;
// 	    grad=dir[0]*grad[0]+dir[1]*grad[1]+dir[2]*grad[2];
// 	    
// 	    //T v=1;//std::sqrt(grad.norm2());
// // 	    T v=(center-mean);
// // 	    v=std::exp(-v*v/(DataScaleGrad*var));
// 	    
// 	    T v=std::sqrt(grad.norm2())/(std::sqrt(var)+0.00000001);
// 	    
// 	    grad.normalize();
// 	    T dist2=(p+(grad)*radius-pos).norm2();
// 	    score+=std::exp(-dist2/(2*sigma))*v;
// 	    votes++;
// 	}
// 	} 
// 	score/=votes;
// 	score*=radius;
// 	
// 	
	
	
        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }

        
 

        
        result=-DataScale*score*scale1
               -DataThreshold;
	       
	      
	       
	       
        return true;
    }
    
    

  
// /*
//     bool eval_data(
//         T & result,
//         Vector<T,Dim>& direction,
//         Vector<T,Dim>& position,
//         T radius)
//     {
//         int Z=std::floor(position[0]);
//         int Y=std::floor(position[1]);
//         int X=std::floor(position[2]);
// 
//         if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
//                 (Z<0)||(Y<0)||(X<0))
//             return false;
// 
// 	
// 	if ((radius<min_scale-0.01)
// 	  ||(radius>max_scale+0.01))
// 	{
// 	  printf("[%f %f %f]\n",min_scale,radius,max_scale);
//  	  sta_assert_error(!(radius<min_scale));
//  	  sta_assert_error(!(radius>max_scale));
// 	}
// 	radius=std::min(max_scale,std::max(radius,min_scale));
// 
// 	
// 	
// 	
// // 	  Vector<T,Dim> vn0;
// // 	  vn0[0]=direction[2];
// // 	  vn0[1]=direction[1];
// // 	  vn0[2]=direction[0];
// 	
// 	
// 	
// 	  Vector<T,3> vn1;
// 	  Vector<T,3> vn2;
// 	  Vector<T,3> p;
// 	  mhs_graphics::createOrths(direction,vn1,vn2); 
// // 	  vn1*=radius;
// // 	  vn2*=radius;
//     
// 	Border=DataScaleGradVessel;
//  
// 	T border=std::max(Border*radius,T(1.5));  
// border=1;
// 	int res=std::ceil((border+radius)*2);
// 	
// 	border+=radius;
// 	
// 	T pos_votes=0;
// 	T neg_votes=0;
// 	T pos_count=0;
// 	T neg_count=0;
// 	
// 	T  minv=std::numeric_limits<T>::max();
// 	T  maxv=std::numeric_limits<T>::min();
// 	
// // 	T pos_mean=0;
// // 	T neg_mean=0;
// // 	int numsteps=1;
// // 	
// // 	for (int z=0;z<numsteps;z++)
// // 	{
// // 	  
// // 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// // 	for (int x=0;x<res;x++)
// // 	{
// // 	 for (int y=0;y<res;y++) 
// // 	 {
// // 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// // 	   
// // 	   if (true)
// // 	   {
// // 		int Z=std::floor(p[0]+0.5f);
// // 		int Y=std::floor(p[1]+0.5f);
// // 		int X=std::floor(p[2]+0.5f);
// // 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
// //                  (Z<0)||(Y<0)||(X<0))
// // 		 {
// // 		  return false;
// // 		 }else
// // 		 {
// // 		  T dist=(pos-p).norm2();
// // 		  //T pw=border-dist;
// // 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// // 		  if (dist<radius*radius)
// // 		  {
// // 		    pos_mean+=value;
// // 		     pos_count++;
// // 		  }else if (dist<border*border)
// // 		  {
// // 		    neg_mean+=value;		    
// // 		     neg_count++;
// // 		  }
// // 		}
// // 	   }
// // 	 }
// // 	}
// // 	}
// 	
// /*
// 	
// 	neg_mean/=neg_count;
// 	pos_mean/=pos_count;
// 	
// 	T pos_var=0;
// 	T neg_var=0;	
// 	
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  if (dist<radius*radius)
// 		  {
// 		    pos_var+=(value-pos_mean)*(value-pos_mean);
// 		  }else if (dist<border*border)
// 		  {
// 		    neg_var+=(value-neg_mean)*(value-neg_mean);
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	neg_var/=neg_count;
// 	pos_var/=pos_count;
// 	
// 	
// 	T bonus=0;
// 	T penalty1=0;	
// 	T penalty2=0;
// 	
// 	T myeps=0.001;
// 	
// 	T pos_sig=pos_var*2+myeps;
// 	T pos_norm=1/std::sqrt(2*M_PI*pos_var+myeps);
// 	T neg_sig=neg_var*2+myeps;
// 	T neg_norm=1/std::sqrt(2*M_PI*neg_var+myeps);
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  if (dist<radius*radius)
// 		  {
// 		    T tmp=(value-pos_mean);
// 		    bonus+=pos_norm*std::exp(-(tmp*tmp)/(pos_sig));
// 		    tmp=(value-neg_mean);
// 		    penalty1+=neg_norm*std::exp(-(tmp*tmp)/(neg_sig));
// 		    
// 		  }else if (dist<border*border)
// 		  {
// 		    T tmp=(value-pos_mean);
// 		    penalty2+=pos_norm*std::exp(-(tmp*tmp)/(pos_sig));
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	bonus/=pos_count;
// 	penalty1/=pos_count;	
// 	penalty2/=neg_count;
// 
// 	if (neg_count<0.00001)
// 	  return false;
// 	if (pos_count<0.00001)
// 	  return false;
// 	
// 	T score=bonus-DataScaleGradVessel*penalty1-DataScaleGrad*penalty2;
// 	score*=radius;
// 	
// 	
// 	if (score<-10)
// 	  return false;*/
// 	
// 	
// 	
// // 	pos_threshold=(maxv-minv)/2+minv;
// // 	minv=std::numeric_limits<T>::max();
// // 	maxv=std::numeric_limits<T>::min();
// // 	
// // 	for (int z=0;z<3;z++)
// // 	{
// // 	  
// // 	Vector<T,3> pos=position+direction*((-1+z)*radius);
// // 	for (int x=0;x<res;x++)
// // 	{
// // 	 for (int y=0;y<res;y++) 
// // 	 {
// // 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// // 	   
// // 	   if (true)
// // 	   {
// // 		int Z=std::floor(p[0]+0.5f);
// // 		int Y=std::floor(p[1]+0.5f);
// // 		int X=std::floor(p[2]+0.5f);
// // 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
// //                  (Z<0)||(Y<0)||(X<0))
// // 		 {
// // 		  return false;
// // 		 }else
// // 		 {
// // 		  T dist=(pos-p).norm2();
// // 		  //T pw=border-dist;
// // 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// // 		  minv=std::min(minv,value);
// // 		  maxv=std::max(maxv,value);
// // 		  
// // 		  if (dist<border*border)
// // 		  {
// // 		      if (dist<radius*radius)
// // 		      {
// // 			 if (value<pos_threshold)
// // 			{
// // // 			    pos_votes-=value;
// // 			    pos_count++;
// // 			}else
// // 			{
// // 			    pos_votes+=value;
// // 			    pos_count++;
// // 			}
// // 		      }else 
// // 		      {
// // 			neg_votes+=value;
// // 			neg_count++;
// // 		      }
// // 		  }
// // 		  
// // 		}
// // 	   }
// // 	 }
// // 	}
// // 	}
// 	
// // 	for (int z=0;z<3;z++)
// // 	{
// // 	  
// // 	Vector<T,3> pos=position+direction*((-1+z)*radius);
// // 	for (int x=0;x<res;x++)
// // 	{
// // 	 for (int y=0;y<res;y++) 
// // 	 {
// // 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// // 	   
// // 	   if (true)
// // 	   {
// // 		int Z=std::floor(p[0]+0.5f);
// // 		int Y=std::floor(p[1]+0.5f);
// // 		int X=std::floor(p[2]+0.5f);
// // 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
// //                  (Z<0)||(Y<0)||(X<0))
// // 		 {
// // 		  return false;
// // 		 }else
// // 		 {
// // 		  T dist=(pos-p).norm2();
// // 		  //T pw=border-dist;
// // 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// // 		  minv=std::min(minv,value);
// // 		  maxv=std::max(maxv,value);
// // 		  if (dist<radius*radius)
// // 		  {
// // 		    pos_votes+=value;
// // 		    pos_count++;
// // 		  }else if (dist<border*border)
// // 		  {
// // 		    neg_votes+=value;
// // 		    neg_count++;
// // 		  }
// // 		  
// // 		}
// // 	   }else
// // 	   {
// // // 	 	int Z=std::floor(p[0]);
// // // 		int Y=std::floor(p[1]);
// // // 		int X=std::floor(p[2]);
// // // 		  if ((Z>=shapei[0])||(Y>=shapei[1])||(X>=shapei[2])||
// // //                   (Z<0)||(Y<0)||(X<0))
// // // 		 {
// // // 		  img_slice[y*res+x]=0.0f;
// // // 		 }else
// // // 		 {
// // // 		  
// // // 		T wz=(p[0]-Z);
// // // 	        T wy=(p[1]-Y);
// // // 	        T wx=(p[2]-X);
// // // 	        {
// // // 	            float & g=img_slice[y*res+x];
// // // 		    g=0.0f;
// // // 	            g+=(1-wz)*(1-wy)*(1-wx)*img[((Z*shapei[1]+Y)*shapei[2]+X)];
// // // 	            g+=(1-wz)*(1-wy)*(wx)*img[((Z*shapei[1]+Y)*shapei[2]+(X+1))];
// // // 	            g+=(1-wz)*(wy)*(1-wx)*img[((Z*shapei[1]+(Y+1))*shapei[2]+X)];
// // // 	            g+=(1-wz)*(wy)*(wx)*img[((Z*shapei[1]+(Y+1))*shapei[2]+(X+1))];
// // // 	            g+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*shapei[1]+Y)*shapei[2]+X)];
// // // 	            g+=(wz)*(1-wy)*(wx)*img[(((Z+1)*shapei[1]+Y)*shapei[2]+(X+1))];
// // // 	            g+=(wz)*(wy)*(1-wx)*img[(((Z+1)*shapei[1]+(Y+1))*shapei[2]+X)];
// // // 	            g+=(wz)*(wy)*(wx)*img[(((Z+1)*shapei[1]+(Y+1))*shapei[2]+(X+1))];
// // // 	        }
// // // 		}
// // 	   }
// // 	 }
// // 	}
// // 	}
// 	
//  	
// 	
// // 	if (neg_count<0.00001)
// // 	  return false;
// // 	if (pos_count<0.00001)
// // 	  return false;
// // 	
// // 	T scale=(maxv-minv)+0.000001;
// // 	T score=(pos_votes-minv)/pos_count-2*(neg_votes-minv)/neg_count;
// // 	score/=scale;
// // 	score*=radius;
// 	
// 	
// // 	T score=(pos_votes)/pos_count-2*(neg_votes)/neg_count;
// // 	score*=radius;
// //  	if (score>0)
// //   	printf("%f [%f %f] [%f %f] %f\n",radius,pos_votes,pos_count,neg_votes,neg_count,score);
// 	
// 	
// 	T mean=0;
// 	int mean_count=0;
// 	int numsteps=2;
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	  
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  
// 		  if (dist<border*border)
// 		  {
// 		    T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		    mean+=value;
// 		    mean_count++;
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	mean/=mean_count;
// 	T var=0;
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	  
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  
// 		  if (dist<border*border)
// 		  {
// 		    T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)]-mean;
// 		    var=(value)*(value);
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	var/=mean_count;
// 	T stdv=std::sqrt(var)+0.001;
// 	
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  if (dist<border*border)
// 		  {
// 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		    if (dist<radius*radius)
// 		    {
// 		      pos_count++;
// 		      pos_votes+=value/stdv;
// 		      
// 		    }else
// 		    {
// 		      neg_count++;
// 		      neg_votes+=value/stdv;
// 		    }
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	
// 	if (neg_count<0.00001)
//  	  return false;
//  	if (pos_count<0.00001)
//  	  return false;
// 	
// // 	T score=radius*(pos_votes/pos_count-DataScaleGrad*neg_votes/neg_count);
// 	T score=radius*(pos_votes-DataScaleGrad*neg_votes);
// 	
//         T scale1=1;
//         switch(scale_power)
//         {
//         case 1:
//             scale1=radius;
//             break;
//         case 2:
//             scale1=radius*radius;
//             break;
//         case 3:
//             scale1=radius*radius*radius;
//             break;
//         }
// 
// 
// //         printf("%f %f %f %f \n",DataScale,score,scale1,DataThreshold);
//         
//         result=-DataScale*score*scale1
//                -DataThreshold;
// 	       
// 	      
// 	       
// 	       
//         return true;
//     }*/
    
    

  

};

#endif
