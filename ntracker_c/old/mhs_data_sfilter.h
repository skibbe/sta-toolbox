#ifndef MHS_DATA_SFILTER_H
#define MHS_DATA_SFILTER_H


#include "mhs_data.h"


template <typename T,typename TData,int Dim>
class CDataSFilter: public CData<T,TData,Dim>
{
private:
    int scale_power;
    T logscalefac;
//     const TData * 	saliency_map;
    const TData * 	alphas_vesselness;
    int 		num_alphas_vesselness;
    const TData * 	alphas_gradient;
    int 		num_alphas_gradient;
    const TData * 	alphas_gradient_vessel;
    int 		num_alphas_gradient_vessel;
    T DataThreshold;
    T DataScale;
    T DataScaleGrad;
    T DataScaleGradVessel;
//     std::size_t shape[3];
    T min_scale;
    T max_scale;

public:
  float get_minscale()
    {
      return min_scale;
    };
    float get_maxscale()
    {
      return max_scale;
    };
    
    float get_scalecorrection()
    {
	return 1;
    };    
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle){};
    void set_controls(const mxArray * handle){};
    #endif    

      
      CDataSFilter()
      {
// 	shape[0]=shape[1]=shape[2]=0;
      }
	
      
    void init(const mxArray * feature_struct)
    {
//         for (int i=0; i<3; i++)
//             this->shape[i]=shape[i];
      	for (int i=0; i<3; i++)	
	  this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
	std::swap(this->shape[0],this->shape[2]);
	printf("shape %d %d %d\n",this->shape[0],this->shape[1],this->shape[2]);

        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"Alphas");
            alphas_vesselness=tmp.data;
            num_alphas_vesselness=tmp.dim[tmp.dim.size()-1];
            printf("num alphas %d\n",num_alphas_vesselness);
        }
//         {
//             tmp=mhs::dataArray<TData>(feature_struct,"saliency_map");
//             saliency_map=tmp.data;
//         }
        
        {
            tmp=mhs::dataArray<TData>(feature_struct,"AlphasCurvature");
            alphas_gradient=tmp.data;
            num_alphas_gradient=tmp.dim[tmp.dim.size()-1];
            printf("num grad alphas %d\n",num_alphas_gradient);
        }
        {
            tmp=mhs::dataArray<TData>(feature_struct,"AlphasCurvatureVDir");
            alphas_gradient_vessel=tmp.data;
            num_alphas_gradient_vessel=tmp.dim[tmp.dim.size()-1];
            printf("num grad vessel alphas %d\n",num_alphas_gradient_vessel);
        }
        {
            tmp=mhs::dataArray<TData>(feature_struct,"Scales_fac");
            logscalefac=tmp.data[0];
            printf("scalefac: %f\n",logscalefac);
            sta_assert(logscalefac>0);
            logscalefac=std::log(logscalefac);
        }
        TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	min_scale=scales[0];
	//max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim.size()-1];
	max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGrad=1;
        DataScaleGradVessel=1;
        DataThreshold=1;

        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];
            if (mhs::mex_hasParam(params,"DataScaleGrad")!=-1)
                DataScaleGrad=mhs::mex_getParam<T>(params,"DataScaleGrad",1)[0];
            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];
            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
        }
         printf("data vessel scale  = vesselness*scale^%d\n",scale_power);
    }

private:


//     template <typename T,typename TData,int Dim>
    T compute_vesselness(
        Vector<T,Dim>& dir,
        T scale,
        const TData * alphas,
        int numalphas)
    {
        T v=eval_polynom(
                dir,
                scale,
                alphas,
                numalphas);
        return v;
    }

//     template <typename T,typename TData,int Dim>
    T compute_grad(
        Vector<T,Dim>& dir,
        T scale,
        const TData * alphas,
        int numalphas)
    {
        T v=eval_polynom(
                dir,
                scale,
                alphas,
                numalphas);
        return v;
    }

//     template <typename T,typename TData,int Dim>
    T compute_vessel_grad(
        Vector<T,Dim>& dir,
        T scale,
        const TData * alphas,
        int numalphas)
    {
        T v=eval_polynom(
                dir,
                scale,
                alphas,
                numalphas);
        return v;
    }





    /*!
     *  energy based on vesselness
     */
//     template <typename T>
    T dataterm(
        int scale_power,
        T scale,
        T vesselness,
        T threshold,
        T vessel_scale,
        T gradient,
        T gscale,
        T gradient_vessel,
        T gvscale
    )
    {

        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=scale;
            break;
        case 2:
            scale1=scale*scale;
            break;
        case 3:
            scale1=scale*scale*scale;
            break;
        }
        return -(
                   vesselness*vessel_scale*scale1
                   +threshold
                   -gradient*gscale
                   -gradient_vessel*gvscale
               );


    }


    bool eval_data(
        T & result,
// 	T & saliency,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T scale
		  #ifdef D_USE_GUI        
//         , std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
	  , std::list<std::string>  * debug=NULL
#endif     
      
    )
    {
        T vesselness=0;
        T gradient=0;
        T gradient_vessel=0;
// 	 saliency=0;

        T multi_scale=std::sqrt(mylog(scale)/logscalefac);


//         std::size_t numvoxel=shape[0]*shape[1]*shape[2];
        {
            std::size_t  indexes[8];
            T weights[8];

            if (!sub2indInter(
                        this->shape.v,
                        position,
                        indexes,
                        weights))
            {
                return false;
            }



            for (int a=0; a<8; a++)
            {
                {
                    {
                        vesselness+=weights[a]*
                                    compute_vesselness(
                                        direction,
                                        multi_scale,
                                        alphas_vesselness+(indexes[a]*num_alphas_vesselness),
                                        num_alphas_vesselness);
                    }
                    {
                        gradient+=weights[a]*compute_grad(
                                      direction,
                                      multi_scale,
                                      alphas_gradient+(indexes[a]*num_alphas_gradient),
                                      num_alphas_gradient);
                    }
                    {
                        gradient_vessel+=weights[a]*compute_vessel_grad(
                                             direction,
                                             T(1),
                                             alphas_gradient_vessel+(indexes[a]*num_alphas_gradient_vessel),
                                             num_alphas_gradient_vessel);
                    }
                    {
// 		      {
// 			saliency+=weights[a]*saliency_map[indexes[a]];
// 		     }
		    }
                }
            }

            result=dataterm(
                       scale_power,
                       scale,
                       vesselness,
                       DataThreshold,
                       DataScale,
                       gradient,
                       DataScaleGrad,
                       gradient_vessel,
                       DataScaleGradVessel);


            return true;
        }


    }
};

#endif
