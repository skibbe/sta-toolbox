#ifndef MHS_DATA_SIMPLE_H
#define MHS_DATA_SIMPLE_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataSimple: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * img;

    const TData * debug;
    T min_scale;
    T max_scale;

    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
    T DataScaleGrad;
    T Border;

//     std::size_t shape[3];


public:

    float get_minscale()
    {
        return min_scale;
    };
    float get_maxscale()
    {
        return max_scale;
    };
    float get_scalecorrection()
    {
          return 1;
    };


    CDataSimple()
    {
      Border=0.2;
    }

    ~CDataSimple()
    {
    }
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle)
    {
      if (handle!=NULL)
      {
	  const mxArray *
	  parameter= mxGetField(handle,0,(char *)("tracker_data"));
	  if (parameter!=NULL) 
	  {
		  DataThreshold=-(*((double*) mxGetPr(parameter)));
		  DataScale=*(((double*) mxGetPr(parameter))+2);
		  DataScaleGradVessel=*(((double*) mxGetPr(parameter))+3);
		  DataScaleGrad=*(((double*) mxGetPr(parameter))+4);
	  }
	  printf("Th: %f,  Da %f, Sigma %f, - %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
      }
    }
    void set_controls(const mxArray * handle)
    {
	  if (handle!=NULL)
	    {
	    const mxArray *
	    parameter= mxGetField(handle,0,(char *)("tracker_data"));
	    if (parameter!=NULL) 
	    {
		  *((double*) mxGetPr(parameter))=-DataThreshold;
		  *(((double*) mxGetPr(parameter))+2)=DataScale;
		  *(((double*) mxGetPr(parameter))+3)=DataScaleGradVessel;
		  *(((double*) mxGetPr(parameter))+4)=DataScaleGrad;
	    }
	  }  
	  printf("Th: %f,  Da %f, Sigma %f, - %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
    }
    #endif    
    


    void init(const mxArray * feature_struct)
    {
        for (int i=0; i<3; i++)
            this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
        std::swap(this->shape[0],this->shape[2]);

        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"img");
            img=tmp.data;
            sta_assert(tmp.dim.size()==3);
        }

	 
	try {
	  const TData * scales=mhs::dataArray<TData>(feature_struct,"override_scales").data;
	  min_scale=scales[0];
	  max_scale=scales[mhs::dataArray<TData>(feature_struct,"override_scales").dim[0]-1];
        } catch (mhs::STAError error)
        {
	    const TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	    min_scale=scales[0];
	    max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
        }
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGradVessel=1;
        DataScaleGrad=1;
        DataThreshold=1;
	Border=0.2;
	
        if (params!=NULL)
        {
	  try
	  {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGrad")!=-1)
                DataScaleGrad=mhs::mex_getParam<T>(params,"DataScaleGrad",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
	    
	    if (mhs::mex_hasParam(params,"Border")!=-1)
                Border=mhs::mex_getParam<T>(params,"Border",1)[0];
	    } catch(mhs::STAError & error)
	  {
	      throw error;
	  }
        }

    }

private:



private:
  
    bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius
		  #ifdef D_USE_GUI        
        //, std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
        , std::list<std::string>  * debug=NULL
#endif     
      
    )
    {
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;

	
	if ((radius<min_scale-0.01)
	  ||(radius>max_scale+0.01))
	{
	  printf("[%f %f %f]\n",min_scale,radius,max_scale);
 	  sta_assert_error(!(radius<min_scale));
 	  sta_assert_error(!(radius>max_scale));
	}
	radius=std::min(max_scale,std::max(radius,min_scale));
	
	
	
	
	Vector<T,3> vn0=direction;
// 	std::swap(vn0[0],vn0[2]);
	Vector<T,3> vn1;
	Vector<T,3> vn2;
	Vector<T,3> p;
	mhs_graphics::createOrths(vn0,vn1,vn2); 
    
	T sigma=DataScaleGradVessel*radius;
	sigma*=sigma;
	
 	T  minv=std::numeric_limits<T>::max();
	T  maxv=std::numeric_limits<T>::min();
	
	int numsteps=3;
	
	
	T radius_outer_ends=radius*T(1.5);
	T radius_outer_starts=radius*T(1.2);
	
	int bb_r=radius_outer_ends;
// 	int bb_v=3;//radius*T(0.25);
	int bb_v=1+radius*T(0.5);
	
	T vote_neg=0;
	T vote_pos=0;
	T vote_neg_c=0;
	T vote_pos_c=0;
 	T vote_sum=0;
	T mean=0;
	T stddev=0;
	T sum=0;
	
	for (int step=0;step<3;step++)
	{
	  switch (step)
		  {
		    case 1:
		    {
		    mean/=sum;
		    } break;
		    case 2:
		    {
		    stddev/=sum;
		    stddev=std::sqrt(stddev)+0.01;
		    } break;
		  }
	  for (int z=-bb_v;z<=bb_v;z++)
	  {
	    for (int y=-bb_r;y<=bb_r;y++)
	    {
	      for (int x=-bb_r;x<=bb_r;x++)
	      {
		  p=position+vn0*z+vn1*x+vn2*y;
		  
		  int Z=std::floor(p[0]);
		  int Y=std::floor(p[1]);
		  int X=std::floor(p[2]);
		  
		  const int boundary=1;
		  if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
		  {
		  return false; 
		  }
		  
		  T center=0.0f;
		  T wz=(p[0]-Z);
		  T wy=(p[1]-Y);
		  T wx=(p[2]-X);
		  {
		      center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
		      center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
		      center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
		      center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
		      center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
		      center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
		      center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
		      center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
		  }
		  T rad=std::sqrt(x*x+y*y);

		  switch (step)
		  {
		    case 0:
		    {
		      if (rad<radius)
		      {
			
			mean+=center;
			sum++;
		      }
		    } break;
		    case 1:
		    {
		      if (rad<radius)
		      {
			stddev+=(center-mean)*(center-mean);
		      }
		    } break;
		    case 2:
		    {
		      center/=stddev;
		      if (rad<radius)
		      {
		      vote_pos+=center; 
		      vote_pos_c++;
		      vote_sum+=center*center;
		      }else if ((rad>radius_outer_starts)&&(rad<radius_outer_ends))
		      {
			vote_neg+=center; 
			vote_neg_c++;
			vote_sum+=center*center;
		     
		      }
		    } break;
		  }
		  
		  
	      }
	    }
	  }
	}
	
	if (vote_pos_c<1)
	  return false;
	if (vote_neg_c<1)
	  return false;
	
	vote_pos/=vote_pos_c;
	vote_neg/=vote_neg_c;
// 	vote_sum=std::sqrt(vote_sum);
	T score=radius*(vote_pos-DataScaleGrad*vote_neg);///(vote_sum+0.01);
	
	
	#ifdef D_USE_GUI        
        if (debug!=NULL)
	{
	  class CData<T,TData,Dim>::CDebugInfo dbinfo;
	  dbinfo.value=score;
	  dbinfo.name="score";
 	  debug->push_back(dbinfo);

	  dbinfo.value=vote_pos_c;
	  dbinfo.name="vote_pos_c";
 	  debug->push_back(dbinfo);
	  
	  dbinfo.value=vote_neg_c;
	  dbinfo.name="vote_neg_c";
 	  debug->push_back(dbinfo);
	  
	  dbinfo.value=vote_neg_c/vote_pos_c;
	  dbinfo.name="vote n/p ratio";
 	  debug->push_back(dbinfo);
	  
	  dbinfo.value=stddev;
	  dbinfo.name="stddev";
 	  debug->push_back(dbinfo);	  

	  dbinfo.value=mean;
	  dbinfo.name="mean";
 	  debug->push_back(dbinfo);		  
	  
	  dbinfo.value=sum;
	  dbinfo.name="sum";
 	  debug->push_back(dbinfo);		  
	}
	#endif    
	
	
// 	vote_sum=std::sqrt(vote_sum);
// 	T score=radius*(vote_pos-vote_neg)/(std::sqrt(vote_neg_c+vote_pos_c)*vote_sum+0.01);
	
	
// 	int N=std::floor(2*M_PI*radius+1);
// 	
// 
// 	Vector<T,3> dir[3];
// 	Vector<T,3> gp;
// 	dir[0]=direction;
// 	
// 	T mean=0;
// 	T var=0;
// 	//for (int z=0;z<numsteps;z++)
// 	{
// 	  int votes=0;  
// 	  Vector<T,3> pos=position;
// 	  
// 	  for (int n=0; n<N; n++)
// 	  {
// 	      T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      
// 	      dir[1]=vn1*x+vn2*y;
// 	      dir[2]=dir[1].cross(dir[0]);
//   	      p=pos+dir[1]*radius;
// 	      
//   	      int Z=std::floor(p[0]);
// 	      int Y=std::floor(p[1]);
// 	      int X=std::floor(p[2]);
// 	      
// 	      const int deriv_skip=1;
// 	      const int boundary=deriv_skip+1;
// 	      if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	      {
// 	      return false; 
// 	      }
// 	      
// 	      T center;
// 	      T wz=(p[0]-Z);
// 	      T wy=(p[1]-Y);
// 	      T wx=(p[2]-X);
// 	      {
// 		  center=0.0f;
// 		  center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	      mean+=center;
// 	      votes++;
// 	  }
// 	  mean/=votes;
// 	  
// 	  for (int n=0; n<N; n++)
// 	  {
// 	      T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	      
// 	      dir[1]=vn1*x+vn2*y;
// 	      dir[2]=dir[1].cross(dir[0]);
//   	      p=pos+dir[1]*radius;
// 	      
//   	      int Z=std::floor(p[0]);
// 	      int Y=std::floor(p[1]);
// 	      int X=std::floor(p[2]);
// 	      
// 	      const int deriv_skip=1;
// 	      const int boundary=deriv_skip+1;
// 	      if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	      {
// 	      return false; 
// 	      }
// 	      
// 	      T center;
// 	      T wz=(p[0]-Z);
// 	      T wy=(p[1]-Y);
// 	      T wx=(p[2]-X);
// 	      {
// 		  center=0.0f;
// 		  center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	      var+=(center-mean)*(center-mean);
// 	  }	  
// 	  var/=votes;
// 	}
// 	
// 	
// 	
// 	int votes=0;
// 	T score=0;
// 	T thick_2length=1/4.0;
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	  
//  	Vector<T,3> pos=position+direction*((z-numsteps/2)*thick_2length);
// // 	  Vector<T,3> pos=position;
// 	
// 
// 	Vector<T,3> grad;
// 	for (int n=0; n<N; n++)
// 	{
// 	    T x=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	    T y=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
// 	    
// 	    dir[1]=vn1*x+vn2*y;
// 	    dir[2]=dir[1].cross(dir[0]);
// // 	    printf("%f %f\n",dir[1].norm2(),dir[2].norm2());
// 	    
// 	    p=pos+dir[1]*radius;
// 	    
// // 	    dir[1].print();
// // 	    ((p-pos)/std::sqrt((p-pos).norm2())).print();
// 	    
// 	    int Z=std::floor(p[0]);
// 	    int Y=std::floor(p[1]);
// 	    int X=std::floor(p[2]);
// 	    
// 	    const int deriv_skip=1;
// 	    const int boundary=deriv_skip+1;
// 	    if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 	    {
// 	     return false; 
// 	    }
// 	    
// 	    T center;
// 	    T wz=(p[0]-Z);
// 	    T wy=(p[1]-Y);
// 	    T wx=(p[2]-X);
// 	    {
// 		
// 		center=0.0f;
// 		center+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		center+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		center+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		center+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		center+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		center+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		center+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		center+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	    }
// 	    
// 	    if (center<0.01)
// 	      return false;
// 	    
// 	    for (int d=0;d<3;d++)
// 	    {
// 	      gp=p+dir[d]*deriv_skip;
// 	      
// 	      Z=std::floor(gp[0]);
// 	      Y=std::floor(gp[1]);
// 	      X=std::floor(gp[2]);
// 
// 	      
// 	       const int boundary=3;
// 		if ((Z<boundary)||(Y<boundary)||(X<boundary)||(!(Z+boundary<this->shape[0]))||(!(Y+boundary<this->shape[1]))||(!(X+boundary<this->shape[2])))
// 		{
// // 		  printf("THIS SHOULDNT HAPPEN %f %f %f\n",Z,Y,X);
// 		return false; 
// 		}
// 		
// 	      wz=(gp[0]-Z);
// 	      wy=(gp[1]-Y);
// 	      wx=(gp[2]-X);
// 	      {
// 		  T & g =grad[d];
// 		  g=0.0f;
// 		  g+=(1-wz)*(1-wy)*(1-wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  g+=(1-wz)*(1-wy)*(wx)*img[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  g+=(1-wz)*(wy)*(1-wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  g+=(1-wz)*(wy)*(wx)*img[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 		  g+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)];
// 		  g+=(wz)*(1-wy)*(wx)*img[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))];
// 		  g+=(wz)*(wy)*(1-wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)];
// 		  g+=(wz)*(wy)*(wx)*img[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))];
// 	      }
// 	    }
// 	    grad-=center;
// 	    grad=dir[0]*grad[0]+dir[1]*grad[1]+dir[2]*grad[2];
// 	    
// 	    //T v=1;//std::sqrt(grad.norm2());
// // 	    T v=(center-mean);
// // 	    v=std::exp(-v*v/(DataScaleGrad*var));
// 	    
// 	    T v=std::sqrt(grad.norm2())/(std::sqrt(var)+0.00000001);
// 	    
// 	    grad.normalize();
// 	    T dist2=(p+(grad)*radius-pos).norm2();
// 	    score+=std::exp(-dist2/(2*sigma))*v;
// 	    votes++;
// 	}
// 	} 
// 	score/=votes;
// 	score*=radius;
// 	
// 	
	
	
        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }

        
 

        
        result=-DataScale*score*scale1
               -DataThreshold;
	       
	      
	       
	       
        return true;
    }
    
    

  
// /*
//     bool eval_data(
//         T & result,
//         Vector<T,Dim>& direction,
//         Vector<T,Dim>& position,
//         T radius)
//     {
//         int Z=std::floor(position[0]);
//         int Y=std::floor(position[1]);
//         int X=std::floor(position[2]);
// 
//         if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
//                 (Z<0)||(Y<0)||(X<0))
//             return false;
// 
// 	
// 	if ((radius<min_scale-0.01)
// 	  ||(radius>max_scale+0.01))
// 	{
// 	  printf("[%f %f %f]\n",min_scale,radius,max_scale);
//  	  sta_assert_error(!(radius<min_scale));
//  	  sta_assert_error(!(radius>max_scale));
// 	}
// 	radius=std::min(max_scale,std::max(radius,min_scale));
// 
// 	
// 	
// 	
// // 	  Vector<T,Dim> vn0;
// // 	  vn0[0]=direction[2];
// // 	  vn0[1]=direction[1];
// // 	  vn0[2]=direction[0];
// 	
// 	
// 	
// 	  Vector<T,3> vn1;
// 	  Vector<T,3> vn2;
// 	  Vector<T,3> p;
// 	  mhs_graphics::createOrths(direction,vn1,vn2); 
// // 	  vn1*=radius;
// // 	  vn2*=radius;
//     
// 	Border=DataScaleGradVessel;
//  
// 	T border=std::max(Border*radius,T(1.5));  
// border=1;
// 	int res=std::ceil((border+radius)*2);
// 	
// 	border+=radius;
// 	
// 	T pos_votes=0;
// 	T neg_votes=0;
// 	T pos_count=0;
// 	T neg_count=0;
// 	
// 	T  minv=std::numeric_limits<T>::max();
// 	T  maxv=std::numeric_limits<T>::min();
// 	
// // 	T pos_mean=0;
// // 	T neg_mean=0;
// // 	int numsteps=1;
// // 	
// // 	for (int z=0;z<numsteps;z++)
// // 	{
// // 	  
// // 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// // 	for (int x=0;x<res;x++)
// // 	{
// // 	 for (int y=0;y<res;y++) 
// // 	 {
// // 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// // 	   
// // 	   if (true)
// // 	   {
// // 		int Z=std::floor(p[0]+0.5f);
// // 		int Y=std::floor(p[1]+0.5f);
// // 		int X=std::floor(p[2]+0.5f);
// // 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
// //                  (Z<0)||(Y<0)||(X<0))
// // 		 {
// // 		  return false;
// // 		 }else
// // 		 {
// // 		  T dist=(pos-p).norm2();
// // 		  //T pw=border-dist;
// // 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// // 		  if (dist<radius*radius)
// // 		  {
// // 		    pos_mean+=value;
// // 		     pos_count++;
// // 		  }else if (dist<border*border)
// // 		  {
// // 		    neg_mean+=value;		    
// // 		     neg_count++;
// // 		  }
// // 		}
// // 	   }
// // 	 }
// // 	}
// // 	}
// 	
// /*
// 	
// 	neg_mean/=neg_count;
// 	pos_mean/=pos_count;
// 	
// 	T pos_var=0;
// 	T neg_var=0;	
// 	
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  if (dist<radius*radius)
// 		  {
// 		    pos_var+=(value-pos_mean)*(value-pos_mean);
// 		  }else if (dist<border*border)
// 		  {
// 		    neg_var+=(value-neg_mean)*(value-neg_mean);
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	neg_var/=neg_count;
// 	pos_var/=pos_count;
// 	
// 	
// 	T bonus=0;
// 	T penalty1=0;	
// 	T penalty2=0;
// 	
// 	T myeps=0.001;
// 	
// 	T pos_sig=pos_var*2+myeps;
// 	T pos_norm=1/std::sqrt(2*M_PI*pos_var+myeps);
// 	T neg_sig=neg_var*2+myeps;
// 	T neg_norm=1/std::sqrt(2*M_PI*neg_var+myeps);
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		  if (dist<radius*radius)
// 		  {
// 		    T tmp=(value-pos_mean);
// 		    bonus+=pos_norm*std::exp(-(tmp*tmp)/(pos_sig));
// 		    tmp=(value-neg_mean);
// 		    penalty1+=neg_norm*std::exp(-(tmp*tmp)/(neg_sig));
// 		    
// 		  }else if (dist<border*border)
// 		  {
// 		    T tmp=(value-pos_mean);
// 		    penalty2+=pos_norm*std::exp(-(tmp*tmp)/(pos_sig));
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	bonus/=pos_count;
// 	penalty1/=pos_count;	
// 	penalty2/=neg_count;
// 
// 	if (neg_count<0.00001)
// 	  return false;
// 	if (pos_count<0.00001)
// 	  return false;
// 	
// 	T score=bonus-DataScaleGradVessel*penalty1-DataScaleGrad*penalty2;
// 	score*=radius;
// 	
// 	
// 	if (score<-10)
// 	  return false;*/
// 	
// 	
// 	
// // 	pos_threshold=(maxv-minv)/2+minv;
// // 	minv=std::numeric_limits<T>::max();
// // 	maxv=std::numeric_limits<T>::min();
// // 	
// // 	for (int z=0;z<3;z++)
// // 	{
// // 	  
// // 	Vector<T,3> pos=position+direction*((-1+z)*radius);
// // 	for (int x=0;x<res;x++)
// // 	{
// // 	 for (int y=0;y<res;y++) 
// // 	 {
// // 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// // 	   
// // 	   if (true)
// // 	   {
// // 		int Z=std::floor(p[0]+0.5f);
// // 		int Y=std::floor(p[1]+0.5f);
// // 		int X=std::floor(p[2]+0.5f);
// // 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
// //                  (Z<0)||(Y<0)||(X<0))
// // 		 {
// // 		  return false;
// // 		 }else
// // 		 {
// // 		  T dist=(pos-p).norm2();
// // 		  //T pw=border-dist;
// // 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// // 		  minv=std::min(minv,value);
// // 		  maxv=std::max(maxv,value);
// // 		  
// // 		  if (dist<border*border)
// // 		  {
// // 		      if (dist<radius*radius)
// // 		      {
// // 			 if (value<pos_threshold)
// // 			{
// // // 			    pos_votes-=value;
// // 			    pos_count++;
// // 			}else
// // 			{
// // 			    pos_votes+=value;
// // 			    pos_count++;
// // 			}
// // 		      }else 
// // 		      {
// // 			neg_votes+=value;
// // 			neg_count++;
// // 		      }
// // 		  }
// // 		  
// // 		}
// // 	   }
// // 	 }
// // 	}
// // 	}
// 	
// // 	for (int z=0;z<3;z++)
// // 	{
// // 	  
// // 	Vector<T,3> pos=position+direction*((-1+z)*radius);
// // 	for (int x=0;x<res;x++)
// // 	{
// // 	 for (int y=0;y<res;y++) 
// // 	 {
// // 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// // 	   
// // 	   if (true)
// // 	   {
// // 		int Z=std::floor(p[0]+0.5f);
// // 		int Y=std::floor(p[1]+0.5f);
// // 		int X=std::floor(p[2]+0.5f);
// // 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
// //                  (Z<0)||(Y<0)||(X<0))
// // 		 {
// // 		  return false;
// // 		 }else
// // 		 {
// // 		  T dist=(pos-p).norm2();
// // 		  //T pw=border-dist;
// // 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// // 		  minv=std::min(minv,value);
// // 		  maxv=std::max(maxv,value);
// // 		  if (dist<radius*radius)
// // 		  {
// // 		    pos_votes+=value;
// // 		    pos_count++;
// // 		  }else if (dist<border*border)
// // 		  {
// // 		    neg_votes+=value;
// // 		    neg_count++;
// // 		  }
// // 		  
// // 		}
// // 	   }else
// // 	   {
// // // 	 	int Z=std::floor(p[0]);
// // // 		int Y=std::floor(p[1]);
// // // 		int X=std::floor(p[2]);
// // // 		  if ((Z>=shapei[0])||(Y>=shapei[1])||(X>=shapei[2])||
// // //                   (Z<0)||(Y<0)||(X<0))
// // // 		 {
// // // 		  img_slice[y*res+x]=0.0f;
// // // 		 }else
// // // 		 {
// // // 		  
// // // 		T wz=(p[0]-Z);
// // // 	        T wy=(p[1]-Y);
// // // 	        T wx=(p[2]-X);
// // // 	        {
// // // 	            float & g=img_slice[y*res+x];
// // // 		    g=0.0f;
// // // 	            g+=(1-wz)*(1-wy)*(1-wx)*img[((Z*shapei[1]+Y)*shapei[2]+X)];
// // // 	            g+=(1-wz)*(1-wy)*(wx)*img[((Z*shapei[1]+Y)*shapei[2]+(X+1))];
// // // 	            g+=(1-wz)*(wy)*(1-wx)*img[((Z*shapei[1]+(Y+1))*shapei[2]+X)];
// // // 	            g+=(1-wz)*(wy)*(wx)*img[((Z*shapei[1]+(Y+1))*shapei[2]+(X+1))];
// // // 	            g+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*shapei[1]+Y)*shapei[2]+X)];
// // // 	            g+=(wz)*(1-wy)*(wx)*img[(((Z+1)*shapei[1]+Y)*shapei[2]+(X+1))];
// // // 	            g+=(wz)*(wy)*(1-wx)*img[(((Z+1)*shapei[1]+(Y+1))*shapei[2]+X)];
// // // 	            g+=(wz)*(wy)*(wx)*img[(((Z+1)*shapei[1]+(Y+1))*shapei[2]+(X+1))];
// // // 	        }
// // // 		}
// // 	   }
// // 	 }
// // 	}
// // 	}
// 	
//  	
// 	
// // 	if (neg_count<0.00001)
// // 	  return false;
// // 	if (pos_count<0.00001)
// // 	  return false;
// // 	
// // 	T scale=(maxv-minv)+0.000001;
// // 	T score=(pos_votes-minv)/pos_count-2*(neg_votes-minv)/neg_count;
// // 	score/=scale;
// // 	score*=radius;
// 	
// 	
// // 	T score=(pos_votes)/pos_count-2*(neg_votes)/neg_count;
// // 	score*=radius;
// //  	if (score>0)
// //   	printf("%f [%f %f] [%f %f] %f\n",radius,pos_votes,pos_count,neg_votes,neg_count,score);
// 	
// 	
// 	T mean=0;
// 	int mean_count=0;
// 	int numsteps=2;
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	  
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  
// 		  if (dist<border*border)
// 		  {
// 		    T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		    mean+=value;
// 		    mean_count++;
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	mean/=mean_count;
// 	T var=0;
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	  
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  
// 		  if (dist<border*border)
// 		  {
// 		    T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)]-mean;
// 		    var=(value)*(value);
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	var/=mean_count;
// 	T stdv=std::sqrt(var)+0.001;
// 	
// 	
// 	for (int z=0;z<numsteps;z++)
// 	{
// 	Vector<T,3> pos=position+direction*((z-numsteps/2)*radius);
// 	for (int x=0;x<res;x++)
// 	{
// 	 for (int y=0;y<res;y++) 
// 	 {
// 	   p=pos+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
// 	   
// 	   if (true)
// 	   {
// 		int Z=std::floor(p[0]+0.5f);
// 		int Y=std::floor(p[1]+0.5f);
// 		int X=std::floor(p[2]+0.5f);
// 		 if ((Z>=this->shape[0])||(Y>=this->shape[1])||(X>=this->shape[2])||
//                  (Z<0)||(Y<0)||(X<0))
// 		 {
// 		  return false;
// 		 }else
// 		 {
// 		  T dist=(pos-p).norm2();
// 		  //T pw=border-dist;
// 		  if (dist<border*border)
// 		  {
// 		  T value=img[((Z*this->shape[1]+Y)*this->shape[2]+X)];
// 		    if (dist<radius*radius)
// 		    {
// 		      pos_count++;
// 		      pos_votes+=value/stdv;
// 		      
// 		    }else
// 		    {
// 		      neg_count++;
// 		      neg_votes+=value/stdv;
// 		    }
// 		  }
// 		}
// 	   }
// 	 }
// 	}
// 	}
// 	
// 	
// 	if (neg_count<0.00001)
//  	  return false;
//  	if (pos_count<0.00001)
//  	  return false;
// 	
// // 	T score=radius*(pos_votes/pos_count-DataScaleGrad*neg_votes/neg_count);
// 	T score=radius*(pos_votes-DataScaleGrad*neg_votes);
// 	
//         T scale1=1;
//         switch(scale_power)
//         {
//         case 1:
//             scale1=radius;
//             break;
//         case 2:
//             scale1=radius*radius;
//             break;
//         case 3:
//             scale1=radius*radius*radius;
//             break;
//         }
// 
// 
// //         printf("%f %f %f %f \n",DataScale,score,scale1,DataThreshold);
//         
//         result=-DataScale*score*scale1
//                -DataThreshold;
// 	       
// 	      
// 	       
// 	       
//         return true;
//     }*/
    
    

  

};

#endif
