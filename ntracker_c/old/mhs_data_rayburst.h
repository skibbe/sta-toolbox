#ifndef MHS_DATA_RAY_H
#define MHS_DATA_RAY_H


#include "mhs_data.h"
#include "mhs_graphics.h"
#include <algorithm>


template <typename T,typename TData,int Dim>
class CDataRay: public CData<T,TData,Dim>
{
private:
    
    const TData * img;
    const TData * gimg;
//     const TData * norm_img;

    
    int scale_power;
    
    T min_scale;
    T max_scale;


    int vessel_steps;
//     int vessel_sample_res;
    int circ_sample_res;
    Vector<T,Dim> *** circle_pts;
    
    T DataScale;
    T DataScaleGradVessel;
    T DataThreshold;
    int MaxRad;

public:
    float get_minscale()
    {
        return min_scale;
    };
    float get_maxscale()
    {
        return max_scale;
    };
    float get_scalecorrection()
    {
          return 1;
    };

    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle){};
    void set_controls(const mxArray * handle){};
    #endif    

    CDataRay()
    {
      DataScale=1;
      vessel_steps=3;
      circ_sample_res=4;
//       vessel_sample_res=5;
      MaxRad=10;
      
      sta_assert_error((vessel_steps>0)&&(vessel_steps%2==1));
//       sta_assert_error(vessel_sample_res>vessel_steps);
      sta_assert_error(vessel_steps>0);
      int range=(vessel_steps-1)/2;
     
      
	circle_pts=new Vector<T,Dim>**[MaxRad];
	for (int r=0;r<MaxRad;r++)
	{
	  
	  int numpts=std::ceil(r+0.5)*circ_sample_res;
	  printf("r: %d, %f  num pts: %d\n",r,std::ceil(r+0.5),numpts);
	  
	  circle_pts[r]=new Vector<T,Dim>*[vessel_steps];
	  for (int v=-range; v<=range; v++)
	  {
	    
	    
	    //WARNING
	    //circ_sample_res:= sampling resolution on unit circle
	    
	      //T offset=std::real(std::exp(std::complex<T>(0,2*M_PI*(T(circ_sample_res)/2.0+v)/((T)circ_sample_res*2))));
	      T shift=0.5*(v%2);
  // 	    printf("%d %d\n",v+range,vessel_steps);
	      circle_pts[r][v+range]=new Vector<T,Dim>[numpts];
	      for (int n=0; n<numpts; n++)
	      {
		  
		sta_assert_error(v+range>=0);
		sta_assert_error(v+range<vessel_steps);
		sta_assert_error(n>=0);
		sta_assert_error(n<numpts);
		
		  circle_pts[r][v+range][n].set(std::real(std::exp(std::complex<T>(0,2*M_PI*(shift+n)/((T)numpts)))),
					    std::imag(std::exp(std::complex<T>(0,2*M_PI*(shift+n)/((T)numpts)))),
						0);
					    //offset);
		  circle_pts[r][v+range][n].normalize();
	      }
	  }
	}
        
        for (int r=0;r<MaxRad;r++)
	{
	 int numpts=std::ceil(r+0.5)*circ_sample_res; 
	  printf("R{%d}=[",r+1);
	  for (int v=-range; v<=range; v++)
	  {
	    for (int n=0; n<numpts; n++)
	    {
	      printf(" %f %f %f; ",circle_pts[r][v+range][n][0],circle_pts[r][v+range][n][1],r);
	    }
	  }
	  printf("];\n");
	}
    }

    ~CDataRay()
    {
      for (int r=0;r<MaxRad;r++)
	{
	  for (int v=0; v<vessel_steps; v++)
	  {
	      delete [] circle_pts[r][v];
	  }
	  delete [] circle_pts[r];
	}
	delete [] circle_pts;
    }

    void init(const mxArray * feature_struct)
    {
//         for (int i=0; i<3; i++)
//             this->this->shape[i]=this->shape[i];

        for (int i=0; i<3; i++)
            this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
        std::swap(this->shape[0],this->shape[2]);

	
        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"img");
            img=tmp.data;
        }
        {
            tmp=mhs::dataArray<TData>(feature_struct,"gimg");
            gimg=tmp.data;
        }
//         {
//             tmp=mhs::dataArray<TData>(feature_struct,"stdv");
//             norm_img=tmp.data;
//         }
        
	try {
	  const TData * scales=mhs::dataArray<TData>(feature_struct,"override_scales").data;
	  min_scale=scales[0];
	  max_scale=scales[mhs::dataArray<TData>(feature_struct,"override_scales").dim[0]-1];
        } catch (mhs::STAError error)
        {
	    const TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	    min_scale=scales[0];
	    max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
        }
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
	DataThreshold=1;
	DataScaleGradVessel=0;
        
        if (params!=NULL)
        {
	  try
	  {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];
	    
	    if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];
	    
	    
	    if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];

	    } catch(mhs::STAError & error)
	  {
	      throw error;
	  }
        }

    }

private:



private:

    bool eval_data(
        T & result,
         Vector<T,Dim>& direction2,
         Vector<T,Dim>& position2,
	//WARNING
        T radius
		  #ifdef D_USE_GUI        
//         , std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
	, std::list<std::string>  * debug=NULL
#endif     
      
    )
    {
      
     Vector<T,Dim> direction;
     direction=direction2;
     Vector<T,Dim> position;
     position=position2;
      
	 
//       position[0]=21;
//       position[1]=53;
//       position[2]=53;
//       
//       direction[0]=0;
//       direction[1]=1;
//       direction[2]=0;
//       
//       radius=6;
//       
      
	
      
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;

	if ((radius<min_scale-0.01)
	  ||(radius>max_scale+0.01))
	{
	  printf("[%f %f %f]\n",min_scale,radius,max_scale);
 	  sta_assert_error(!(radius<min_scale));
 	  sta_assert_error(!(radius>max_scale));
	}
	radius=std::min(max_scale,std::max(radius,min_scale));
	
	T multi_scale=std::sqrt(radius);

        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);
	
	
	Vector<T,3> vn1;
        Vector<T,3> vn2;

        const  Vector<T,Dim> & vn0=direction;
        mhs_graphics::createOrths(vn0,vn1,vn2);
	
	int offset[3];
	offset[0]=offset[1]=offset[2]=0;
	
	int r=std::min(MaxRad-1,int(std::ceil(radius)));
	
	int numpts=std::ceil(r+0.5)*circ_sample_res; 
	
	
	int num_votes=vessel_steps*numpts;
	T votes[num_votes];
	int votes_count=0;
	
	
	sta_assert_error(r>=0);
	sta_assert_error(r<MaxRad);
	
// 	circle_pts[r][0][0].print();
// 	circle_pts[r][0][numpts-1].print();
// 	circle_pts[r][vessel_steps-1][0].print();
// 	circle_pts[r][vessel_steps-1][numpts-1].print();
// 	
// 	return false;
// 	
	bool out_of_image=false;
	T vote=0;
	
	//int spawn_threads=(radius>5) ? vessel_steps : 1;
	
	#pragma omp parallel for num_threads(vessel_steps) //shared(vote,votes,votes_count)
	for (int v=0;v<vessel_steps;v++)
	{
	  int local_votes_count=0;
	  T * local_votes=votes+v*numpts;
	  Vector<T,Dim> grad;
	  
	  for (int d=0;d<numpts;d++)
	  {
	    if (out_of_image)
	      break;
	    
	    Vector<T,Dim> dir;
	    T vessel_dir_displacement=(-(vessel_steps-1)/2+v)*(1+radius/2);
	    
	    dir=(vn0*vessel_dir_displacement+
		vn1*circle_pts[r][v][d][0]*radius+
		vn2*circle_pts[r][v][d][1]*radius);
	      
	     Vector<T,Dim> sample_pt=position+dir; 
	     Vector<T,Dim> center=position+vn0*vessel_dir_displacement;
	     
	     if ((sample_pt[0]+2>=this->shape[0])||
	         (sample_pt[1]+2>=this->shape[1])||
	         (sample_pt[2]+2>=this->shape[2])||
		 (sample_pt[0]<1)||
		 (sample_pt[1]<1)||
		 (sample_pt[2]<1))  
	     {
	       result=0;
	       out_of_image=true;
	       break;
	       //return false;
	     }
	      
	     int Z=std::floor(sample_pt[0]);
	     int Y=std::floor(sample_pt[1]);
	     int X=std::floor(sample_pt[2]);
	     
	      T wz=(sample_pt[0]-Z);
	      T wy=(sample_pt[1]-Y);
	      T wx=(sample_pt[2]-X);	      
	      
	      
	      
	      for (int dim=0;dim<3;dim++)
	      {
	      
	      T & g=grad[dim];
	      g=0;
	      
	      g+=(1-wz)*(1-wy)*(1-wx)*gimg[(((Z)*this->shape[1]+(Y))*this->shape[2]+(X))*3+dim];
	      g+=(1-wz)*(1-wy)*(wx)*gimg[(((Z)*this->shape[1]+(Y))*this->shape[2]+((X)+1))*3+dim];
	      g+=(1-wz)*(wy)*(1-wx)*gimg[(((Z)*this->shape[1]+((Y)+1))*this->shape[2]+(X))*3+dim];
	      g+=(1-wz)*(wy)*(wx)*gimg[(((Z)*this->shape[1]+((Y)+1))*this->shape[2]+((X)+1))*3+dim];
	      g+=(wz)*(1-wy)*(1-wx)*gimg[((((Z)+1)*this->shape[1]+(Y))*this->shape[2]+(X))*3+dim];
	      g+=(wz)*(1-wy)*(wx)*gimg[((((Z)+1)*this->shape[1]+(Y))*this->shape[2]+((X)+1))*3+dim];
	      g+=(wz)*(wy)*(1-wx)*gimg[((((Z)+1)*this->shape[1]+((Y)+1))*this->shape[2]+(X))*3+dim];
	      g+=(wz)*(wy)*(wx)*gimg[((((Z)+1)*this->shape[1]+((Y)+1))*this->shape[2]+((X)+1))*3+dim];
	      }
	      
	      std::swap(grad[0],grad[2]);
	      
	     
	      T vote_value=std::sqrt(grad.norm2());
	      grad/=vote_value+std::numeric_limits<T>::epsilon();
	      
	      Vector<T,Dim> vote_pt=sample_pt+grad*radius;
	      
	      //T contribution=std::exp(-(vote_pt-center).norm2()/(radius*radius));
	      T contribution=mexp((vote_pt-center).norm2()/(radius*radius));
 	      //T contribution=std::exp(-(vote_pt-center).norm2()/(radius*radius/4));
	      //T contribution=std::exp(-(vote_pt-center).norm2()/(radius*radius/2));
	      //T contribution=std::exp(-(vote_pt-center).norm2()/(radius*radius/(1*1)));
	      
	      
//  	      T panelty=1-std::exp(-(vote_pt-center).norm2()/(radius*radius/(0.5*4)));
//  	      vote+=contribution-panelty;
//  	      votes[votes_count++]=contribution-panelty;
// 	      
	      
  	      //vote+=2*contribution-1;
  	      //votes[votes_count++]=2*contribution-1;
	      
	      //votes[votes_count++]=std::pow(vote_value,T(0.01))*(2*contribution-1);
	      
	      
	      local_votes[local_votes_count++]=vote_value*(2*contribution-1);
	      
// 	      T norm_fact=0;
// 	      norm_fact+=(1-wz)*(1-wy)*(1-wx)*norm_img[(((Z)*this->shape[1]+(Y))*this->shape[2]+(X))];
// 	      norm_fact+=(1-wz)*(1-wy)*(wx)*norm_img[(((Z)*this->shape[1]+(Y))*this->shape[2]+((X)+1))];
// 	      norm_fact+=(1-wz)*(wy)*(1-wx)*norm_img[(((Z)*this->shape[1]+((Y)+1))*this->shape[2]+(X))];
// 	      norm_fact+=(1-wz)*(wy)*(wx)*norm_img[(((Z)*this->shape[1]+((Y)+1))*this->shape[2]+((X)+1))];
// 	      norm_fact+=(wz)*(1-wy)*(1-wx)*norm_img[((((Z)+1)*this->shape[1]+(Y))*this->shape[2]+(X))];
// 	      norm_fact+=(wz)*(1-wy)*(wx)*norm_img[((((Z)+1)*this->shape[1]+(Y))*this->shape[2]+((X)+1))];
// 	      norm_fact+=(wz)*(wy)*(1-wx)*norm_img[((((Z)+1)*this->shape[1]+((Y)+1))*this->shape[2]+(X))];
// 	      norm_fact+=(wz)*(wy)*(wx)*norm_img[((((Z)+1)*this->shape[1]+((Y)+1))*this->shape[2]+((X)+1))];
// 	      
// 	      local_votes[local_votes_count++]=norm_fact*vote_value*(2*contribution-1);
	      
	      
	      //v*numpts+d;
// 	      #pragma omp critical
// 	      {
// 	      votes[votes_count++]=norm_fact*vote_value*(2*contribution-1);
// 	      }
	  }
	}
	if (out_of_image)
	  return false;
	
	vote/=(vessel_steps*numpts);	
	
	std::sort(votes,votes+num_votes);
	//vote=votes[int(std::floor(3.0*num_votes/4.0))];
	vote=votes[int(std::floor(num_votes/2))];
	
/*	
	for (int i=0;i<num_votes;i++)
	  printf("%f ",votes[i]);
	printf("\n");*/
	
	//vote=votes[int(std::floor(3.0*num_votes/4.0))];
	//vote=votes[int(std::floor(1.0*num_votes/4.0))];
	sta_assert_error(!(votes[0]>votes[vessel_steps*numpts-1]));
	
	
// 	//WARNING
// 	vote=0;
	
	
	
	
/*	int Z=std::floor(position[0]);
	int Y=std::floor(position[1]);
	int X=std::floor(position[2]);
	
	T wz=(position[0]-Z);
	T wy=(position[1]-Y);
	T wx=(position[2]-X);	   */   
	
	Vector<T,Dim> vessel_grad;
	for (int dim=0;dim<3;dim++)
	{
	T & g=vessel_grad[dim];
	g=0;
	
	g+=(1-wz)*(1-wy)*(1-wx)*gimg[(((Z)*this->shape[1]+(Y))*this->shape[2]+(X))*3+dim];
	g+=(1-wz)*(1-wy)*(wx)*gimg[(((Z)*this->shape[1]+(Y))*this->shape[2]+((X)+1))*3+dim];
	g+=(1-wz)*(wy)*(1-wx)*gimg[(((Z)*this->shape[1]+((Y)+1))*this->shape[2]+(X))*3+dim];
	g+=(1-wz)*(wy)*(wx)*gimg[(((Z)*this->shape[1]+((Y)+1))*this->shape[2]+((X)+1))*3+dim];
	g+=(wz)*(1-wy)*(1-wx)*gimg[((((Z)+1)*this->shape[1]+(Y))*this->shape[2]+(X))*3+dim];
	g+=(wz)*(1-wy)*(wx)*gimg[((((Z)+1)*this->shape[1]+(Y))*this->shape[2]+((X)+1))*3+dim];
	g+=(wz)*(wy)*(1-wx)*gimg[((((Z)+1)*this->shape[1]+((Y)+1))*this->shape[2]+(X))*3+dim];
	g+=(wz)*(wy)*(wx)*gimg[((((Z)+1)*this->shape[1]+((Y)+1))*this->shape[2]+((X)+1))*3+dim];
	}
	      
	T vessel_grad_l=std::sqrt(vessel_grad.norm2());

        vessel_grad/=vessel_grad_l+std::numeric_limits<T>::epsilon();
        T vessel_grad_p=(direction.dot(vessel_grad));
        vessel_grad_l*=vessel_grad_p*vessel_grad_p;
	
	
	

        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }


        
        result=-DataScale*vote*scale1
	       +DataScaleGradVessel*std::abs(vessel_grad_l)
               -DataThreshold;
        return true;
    }
    
    
public:
  

};

#endif
