#ifndef MHS_DATA_HOUGH_H
#define MHS_DATA_HOUGH_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataHough: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * gm;
    const TData * gmv;
//     const TData * gm_scales;
//    int num_alphas;

    T logscalefac;

    T HoughGamma;
    T HoughSigma;
    T HoughhomogeneitySigma;
    T HoughMaxMagnitude;
    int HoughTubeSteps;
    T HoughTubeLengths;
    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
//     std::size_t shape[3];
    T ** circle_pts;
    const int maxrad=15;
    
    T min_scale;
    T max_scale;    

    T * tube_step_pts;
    T * tube_step_weights;

public:
//  template<typename S>
//     void setshape(S shape[])
//       {
// 	for (int i=0;i<Dim;i++)
// 	{
// 	  sta_assert(this->shape[i]>0);
// 	  shape[i]=this->shape[i];
// 	}
// 
//       }
  
    float get_minscale()
    {
      return min_scale;
    };
    float get_maxscale()
    {
      return max_scale;
    };
    
    float get_scalecorrection()
    {
	return 1;
    };
  
    CDataHough() :  CData<T,TData,Dim>()
    {
//       shape[0]=shape[1]=shape[2]=0;
        circle_pts=new T*[maxrad];
        for (int r=0; r<maxrad; r++)
        {
            int N=std::floor(2*M_PI*r+1);
            circle_pts[r]=new T[2*N];
            for (int n=0; n<N; n++)
            {
                circle_pts[r][2*n]=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
                circle_pts[r][2*n+1]=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
            }
        }
        tube_step_pts=NULL;
        tube_step_weights=NULL;


    }

    ~CDataHough()
    {
        for (int r=0; r<maxrad; r++)
        {
            delete [] circle_pts[r];
        }
        delete [] circle_pts;
        if (tube_step_pts!=NULL)
        {
            delete [] tube_step_pts;
            delete [] tube_step_weights;
        }
    }
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle){};
    void set_controls(const mxArray * handle){};
    #endif    

    void init(const mxArray * feature_struct)
    {
//         for (int i=0; i<3; i++)
//             this->shape[i]=shape[i];
	for (int i=0; i<3; i++)	
	this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
	std::swap(this->shape[0],this->shape[2]);
  
        gm=mhs::dataArray<TData>(feature_struct,"gradient_img").data;

        try {
            gmv=mhs::dataArray<TData>(feature_struct,"vgradient_img").data;
        } catch (mhs::STAError error)
        {
            printf("no vgradient_img field, using gradient_img field instead\n");
            gmv=gm;
        }
        
        TData* scales=mhs::dataArray<TData>(feature_struct,"scales").data;
	min_scale=scales[0];
	//max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim.size()-1];        
	max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
	if (max_scale>maxrad)
	  max_scale=maxrad;

//         gm_scales=mhs::dataArray<TData>(feature_struct,"scales").data;
        logscalefac=mhs::dataArray<TData>(feature_struct,"Scales_fac").data[0];
        printf("scalefac: %f\n",logscalefac);
        sta_assert(logscalefac>0);
        logscalefac=std::log(logscalefac);
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleGradVessel=1;
        DataThreshold=1;
        HoughGamma=0.1;

        HoughSigma=1;
        HoughhomogeneitySigma=-1;
        HoughMaxMagnitude=-1;

        HoughTubeLengths=1;
        HoughTubeSteps=1;

        /*	HoughTubeLengths=1;
        	HoughTubeSteps=3;*/

        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];

            if (mhs::mex_hasParam(params,"HoughGamma")!=-1)
                HoughGamma=mhs::mex_getParam<T>(params,"HoughGamma",1)[0];

            if (mhs::mex_hasParam(params,"HoughhomogeneitySigma")!=-1)
                HoughhomogeneitySigma=mhs::mex_getParam<T>(params,"HoughhomogeneitySigma",1)[0];

            if (mhs::mex_hasParam(params,"HoughMaxMagnitude")!=-1)
                HoughMaxMagnitude=mhs::mex_getParam<T>(params,"HoughMaxMagnitude",1)[0];

            if (mhs::mex_hasParam(params,"HoughSigma")!=-1)
                HoughSigma=mhs::mex_getParam<T>(params,"HoughSigma",1)[0];

            if (mhs::mex_hasParam(params,"HoughTubeLengths")!=-1)
                HoughTubeLengths=mhs::mex_getParam<T>(params,"HoughTubeLengths",1)[0];

            if (mhs::mex_hasParam(params,"HoughTubeSteps")!=-1)
                HoughTubeSteps=mhs::mex_getParam<int>(params,"HoughTubeSteps",1)[0];


            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];

            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
        }

        HoughSigma=2*HoughSigma*HoughSigma;

        tube_step_pts=new T[HoughTubeSteps];
        tube_step_weights=new T[HoughTubeSteps];
        if (HoughTubeSteps==1)
        {
            tube_step_pts[0]=0;
            tube_step_weights[0]=1;
        }
        else
        {
            T sum=0;
            const T tube_length=1;
            for (int a=0; a<HoughTubeSteps; a++)
            {
                tube_step_pts[a]=-tube_length+a*2*tube_length/(HoughTubeSteps-1);
                tube_step_weights[a]=1-std::abs(0.5*tube_step_pts[a]/tube_length);
                sum+=tube_step_weights[a];
                printf("%f ",tube_step_pts[a]);
            }
            printf("\n");
            for (int a=0; a<HoughTubeSteps; a++)
            {
                tube_step_weights[a]/=sum;
                printf("%f ",tube_step_weights[a]);
            }
            printf("\n");
        }
    }

private:


    bool eval_data(
        T & result,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius
	#ifdef D_USE_GUI        
//         , std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
	  , std::list<std::string>  * debug=NULL
	  #endif     
    )
    {


        int r=std::floor(radius+0.5);
        int N=std::floor(2*M_PI*r+1);
        sta_assert(r<maxrad);
        sta_assert(r>-1);

        Vector<T,3> vn1;
        Vector<T,3> vn2;

        const  Vector<T,Dim> & direction2=direction;
//   	  std::swap(direction.v[0],direction.v[2]);
        mhs_graphics::createOrths(direction2,vn1,vn2);


        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;


        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);

        Vector<T,3> vessel_grad=T(0);
        for (int i=0; i<3; i++)
        {
            T & g=vessel_grad.v[i];
            g+=(1-wz)*(1-wy)*(1-wx)*gmv[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(1-wz)*(1-wy)*(wx)*gmv[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(1-wz)*(wy)*(1-wx)*gmv[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(1-wz)*(wy)*(wx)*gmv[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(1-wy)*(1-wx)*gmv[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(wz)*(1-wy)*(wx)*gmv[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(wy)*(1-wx)*gmv[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(wz)*(wy)*(wx)*gmv[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
        }
        std::swap(vessel_grad.v[0],vessel_grad.v[2]);

        T vessel_grad_l=std::abs(direction2.dot(vessel_grad));



        result=0;

        //#pragma omp parallel for num_threads(HoughTubeSteps)
        //#pragma omp parallel for num_threads(std::min(HoughTubeSteps,omp_get_num_procs()))
        for (int s=0; s<HoughTubeSteps; s++)
        {
            Vector<T,3> sample_p;
            Vector<T,3> sample_dir;
            Vector<T,3> grad;
            Vector<T,3> voting_pt;

            T vessel_offset=HoughTubeLengths*tube_step_pts[s];
            Vector<T,3> P_vessel=position+direction2*vessel_offset;


            T mean=0;
            if (HoughhomogeneitySigma>0)
            {
                for (int n=0; n<N; n++)
                {
                    sample_dir=vn1*(circle_pts[r][2*n])
                               +vn2*(circle_pts[r][2*n+1]);

                    sample_p=P_vessel+sample_dir*radius;

                    int Z=std::floor(sample_p[0]);
                    int Y=std::floor(sample_p[1]);
                    int X=std::floor(sample_p[2]);

                    if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                            (Z<0)||(Y<0)||(X<0))
                        continue;

                    T wz=(sample_p[0]-Z);
                    T wy=(sample_p[1]-Y);
                    T wx=(sample_p[2]-X);

                    grad=T(0);
                    for (int i=0; i<3; i++)
                    {
                        T & g=grad.v[i];
                        g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                        g+=(1-wz)*(1-wy)*(wx)*gm[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                        g+=(1-wz)*(wy)*(1-wx)*gm[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                        g+=(1-wz)*(wy)*(wx)*gm[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                        g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                        g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                        g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                        g+=(wz)*(wy)*(wx)*gm[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                    }
                    std::swap(grad.v[0],grad.v[2]);

                    {
                        T gm=std::sqrt(grad.norm2());
                        if (HoughGamma>0)
                            gm=std::pow(gm,HoughGamma);

                        grad.normalize();
                        voting_pt=sample_p+grad*radius;

                        if (sample_dir.dot(grad)<0)
                        {

                            if (HoughGamma==-1)
                                gm=1;
                            else if (HoughMaxMagnitude>0)
                                gm=std::min(HoughMaxMagnitude,gm);
                            mean+=gm*std::exp(-(P_vessel-voting_pt).norm2()/(HoughSigma));
                        }
                    }
                }
                mean=mean/N+std::numeric_limits<T>::epsilon();
            }

            T v=0;
            for (int n=0; n<N; n++)
            {
                sample_dir=vn1*(circle_pts[r][2*n])
                           +vn2*(circle_pts[r][2*n+1]);

                sample_p=P_vessel+sample_dir*radius;

                int Z=std::floor(sample_p[0]);
                int Y=std::floor(sample_p[1]);
                int X=std::floor(sample_p[2]);

                if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                        (Z<0)||(Y<0)||(X<0))
                    continue;

                T wz=(sample_p[0]-Z);
                T wy=(sample_p[1]-Y);
                T wx=(sample_p[2]-X);

                grad=T(0);
                for (int i=0; i<3; i++)
                {
                    T & g=grad.v[i];
                    g+=(1-wz)*(1-wy)*(1-wx)*gm[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                    g+=(1-wz)*(1-wy)*(wx)*gm[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                    g+=(1-wz)*(wy)*(1-wx)*gm[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                    g+=(1-wz)*(wy)*(wx)*gm[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                    g+=(wz)*(1-wy)*(1-wx)*gm[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                    g+=(wz)*(1-wy)*(wx)*gm[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                    g+=(wz)*(wy)*(1-wx)*gm[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                    g+=(wz)*(wy)*(wx)*gm[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                }
                std::swap(grad.v[0],grad.v[2]);

                {
                    T gm=std::sqrt(grad.norm2());
//                         T gmorg=gm;
                    if (HoughGamma>0)
                        gm=std::pow(gm,HoughGamma);

                    grad.normalize();
                    voting_pt=sample_p+grad*radius;

                    if (sample_dir.dot(grad)<0)
                    {

                        if (HoughGamma==-1)
                            gm=1;
                        else if (HoughMaxMagnitude>0)
                            gm=std::min(HoughMaxMagnitude,gm);

                        if (HoughhomogeneitySigma>0)
                        {
                            T tmp2=(1-gm/mean);
                            tmp2*=tmp2;
                            gm*=std::exp(-tmp2/(HoughhomogeneitySigma));
                        }


                        v+=gm*std::exp(-(P_vessel-voting_pt).norm2()/(HoughSigma));
                    }
                }
            }
            //#pragma omp critical
            {
                result+=tube_step_weights[s]*v/N;
            }
        }




        switch(scale_power)
        {
        case 1:
            result*=radius;
            break;
        case 2:
            result*=radius*radius;
            break;
        case 3:
            result*=radius*radius*radius;
            break;
        }

        //result*=DataScale;
        result=-DataScale*result
               -DataThreshold
               +DataScaleGradVessel*vessel_grad_l;
        return true;
    }
};

#endif
