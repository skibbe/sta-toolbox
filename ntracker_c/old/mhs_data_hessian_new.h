#ifndef MHS_DATA_HESSIANL_NEW_H
#define MHS_DATA_HESSIANL_NEW_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataHessianNew: public CData<T,TData,Dim>
{
private:
    int scale_power;
    const TData * hessian;
//     const TData * gradient;
//     const TData * gradient_v;
    int num_alphas_hessian;
//     int num_alphas_gradient;
//     bool soverride=false;
    
    const TData * scales;
    std::size_t num_scales;

    const TData * local_stdv;
    int num_alphas_local_stdv;

    const TData * img_smooth;
    int num_alphas_img_smooth;
    bool img_smooth_is_pol;
    
    
    Vector<T,Dim> boundary;
    bool scale_dependend_boundary;


    const TData * debug;
    T min_scale;
    T max_scale;
    T scale_correction;
//     T logscalefac;
    bool true_SF;

    T DataThreshold;
    T DataScale;
    T DataScaleGradVessel;
    T StdvEpsilon;

    T wxx;
    T wyy;
    T wzz;
    T wxy;
    T wxz;
    T wyz;

//     T w_grad[3];
    T DataScaleGradSurface;


    Vector<T,3> element_size;
    Vector<std::size_t ,3> feature_shape;

//     std::size_t shape[3];


public:
//     template<typename S>
//     void setshape(S  shape[])
//       {
// 	for (int i=0;i<Dim;i++)
// 	{
// 	  sta_assert(this->shape[i]>0);
// 	  shape[i]=this->shape[i];
// 	}
//       }

    float get_minscale() {
//        if (soverride)
//  	 return min_scale;
        return min_scale/scale_correction;
    };
    float get_maxscale() {
//       if (soverride)
//  	 return max_scale;
        return max_scale/scale_correction;
    };
    float get_scalecorrection() {
        //return 1.0/scale_correction;
        //return scale_correction;
        return 1;
    };


    CDataHessianNew() {
//       shape[0]=shape[1]=shape[2]=0;
        true_SF=false;
	img_smooth_is_pol=true;
    }

    ~CDataHessianNew() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
                DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
                DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
// 	  printf("Th: %f,  Da %f, DaGV %f, DaG %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGrad);
            printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
        printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
    }
#endif

    void init ( const mxArray * feature_struct ) {

// 	CData<T,TData,Dim>::init(feature_struct);
//         for (int i=0; i<3; i++)
//             this->this->shape[i]=this->shape[i];



        mhs::dataArray<TData>  tmp0;
        mhs::dataArray<TData>  tmp1;
        mhs::dataArray<TData>  tmp2;

        try {
            tmp0=mhs::dataArray<TData> ( feature_struct,"alphas_hessian" );
            hessian=tmp0.data;
            sta_assert_error ( tmp0.dim.size() ==5 );
            sta_assert_error ( tmp0.dim[3]==6 );
            num_alphas_hessian=tmp0.dim[4];
            printf ( "hessian  pol degree: %d \n",num_alphas_hessian-1 );

        } catch ( mhs::STAError error ) {
            hessian=NULL;
        }
        
        mhs::dataArray<TData>  tmp3;
        try {
            tmp3=mhs::dataArray<TData> ( feature_struct,"shape_boundary" );
	    sta_assert_error(tmp3.get_num_elements()==3);
	    boundary[0]=tmp3.data[0];
	    boundary[1]=tmp3.data[1];
	    boundary[2]=tmp3.data[2];
	    scale_dependend_boundary=true;
        } catch ( mhs::STAError error ) {
            boundary[0]=boundary[1]=boundary[2]=T(0);
	    scale_dependend_boundary=false;
        }

        try {


            tmp1=mhs::dataArray<TData> ( feature_struct,"alphas_img" );
            img_smooth=tmp1.data;
            num_alphas_img_smooth=tmp1.dim[tmp1.dim.size()-1];
            printf ( "img smoothed pol degree: %d \n",num_alphas_img_smooth-1 );

            tmp2=mhs::dataArray<TData> ( feature_struct,"alphas_sdv" );
            local_stdv=tmp2.data;
            num_alphas_local_stdv=tmp2.dim[tmp2.dim.size()-1];
            printf ( "local stdv pol degree: %d \n",num_alphas_local_stdv-1 );
//             tmp=mhs::dataArray<TData>(feature_struct,"alphas_gradient");
//             gradient=tmp.data;
//             sta_assert(tmp.dim.size()==5);
//             sta_assert(tmp.dim[3]==3);
//             num_alphas_gradient=tmp.dim[4];
//             printf("gradient pol degree: %d \n",num_alphas_gradient-1);
// 	    for (int i=0; i<3; i++)
// 	    {
// 	      printf("%d %d %d\n",tmp0.dim[i],tmp1.dim[i],tmp2.dim[i]);
// 	    }
            if ( hessian!=NULL ) {
                sta_assert_error ( tmp0.get_num_elements() / ( num_alphas_hessian*6 ) ==tmp1.get_num_elements() /num_alphas_img_smooth );
                sta_assert_error ( tmp0.get_num_elements() / ( num_alphas_hessian*6 ) ==tmp2.get_num_elements() /num_alphas_local_stdv );
            }
            sta_assert_error ( tmp2.get_num_elements() /num_alphas_local_stdv==tmp1.get_num_elements() /num_alphas_img_smooth );

            for ( int i=0; i<3; i++ ) {
//  	      sta_assert_error(tmp0.dim[i]==tmp1.dim[i]);
// 	      sta_assert_error(tmp1.dim[i]==tmp2.dim[i]);
// 	      sta_assert_error(tmp0.dim[i]==tmp2.dim[i]);
                feature_shape[i]=tmp1.dim[i];
            }
        } catch ( mhs::STAError error ) {
            throw error;
        }


        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

// 	 printf("data shape: %d %d %d\n",this->shape[0],this->shape[1],this->shape[2]);
// 	 feature_shape.print();
        std::swap ( this->shape[0],this->shape[2] );

        for ( int i=0; i<3; i++ ) {
            element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
        }

// 	wxx=1/(element_size[0]*element_size[0]);
// 	wyy=1/(element_size[1]*element_size[1]);
// 	wzz=1/(element_size[2]*element_size[2]);
// 	wxy=1/(element_size[0]*element_size[1]);
// 	wxz=1/(element_size[0]*element_size[2]);
// 	wyz=1/(element_size[1]*element_size[2]);

        wxx=1/ ( element_size[2]*element_size[2] );
        wyy=1/ ( element_size[1]*element_size[1] );
        wzz=1/ ( element_size[0]*element_size[0] );
        wxy=1/ ( element_size[2]*element_size[1] );
        wxz=1/ ( element_size[2]*element_size[0] );
        wyz=1/ ( element_size[1]*element_size[0] );

// 	w_grad[0]=1/(element_size[0]);
// 	w_grad[1]=1/(element_size[1]);
// 	w_grad[2]=1/(element_size[2]);

// 	std::swap(this->feature_shape[0],this->feature_shape[2]);
        //std::swap(this->element_size[0],this->element_size[2]);

        printf ( "element size data term:" );
        element_size.print();

//         gradient_v=mhs::dataArray<TData>(feature_struct,"gradient_vessel").data;

//  	 scale_correction=1.0/std::sqrt(2.0);
// 	 scale_correction=std::sqrt(2.0);
        scale_correction=1;
	try {
	    scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
	    num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
	}catch ( mhs::STAError error ) {
            throw error;
        }
        
        
        try {
	  const TData * is_pol=mhs::dataArray<TData> ( feature_struct,"img_smooth_is_pol" ).data;
	  img_smooth_is_pol=(is_pol[0]>0);
	  
	}catch ( mhs::STAError error ) {
            img_smooth_is_pol=true;
        }
        
        if (!img_smooth_is_pol)
	{
	 sta_assert_error(num_scales==num_alphas_img_smooth); 
	 printf("using local interpolation for smoothed img\n");
	}
	
        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"override_scales" ).data;
            min_scale=scales[0]*scale_correction;
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"override_scales" ).dim[0]-1]*scale_correction;
// 	  soverride=true;
        } catch ( mhs::STAError error ) {
            min_scale=scales[0];
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
        }

        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"trueSF" ).data;
            true_SF= ( *scales ) ==1;
            printf ( "found SF settings: %d\n",true_SF );
        } catch ( mhs::STAError error ) {
            throw error;
        }

// 	try {
// 	  mhs::dataArray<TData> element_size_array=mhs::dataArray<TData>(feature_struct,"element_size");
//
// 	  sta_assert_error(element_size_array.get_num_elements()==3);
//
// 	  this->element_size[0]=element_size_array.data[0];
// 	  this->element_size[1]=element_size_array.data[1];
// 	  this->element_size[2]=element_size_array.data[2];
// 	  printf("found element size \n");
// 	  this->element_size.print();
//
//
//         } catch (mhs::STAError error)
//         {
//
//         }


//         logscalefac=mhs::dataArray<TData>(feature_struct,"Scales_fac").data[0];
//         printf("scalefac: %f\n",logscalefac);
//         //sta_assert(logscalefac>0);
// 	sta_assert_error(!(logscalefac<1));
//         logscalefac=std::log(logscalefac);
    }

    void set_params ( const mxArray * params=NULL ) {
        scale_power=2;
        DataScale=1;
        DataScaleGradVessel=1;
//         DataScaleGrad=1;

        DataScaleGradSurface=1;
        DataThreshold=1;
        StdvEpsilon=0.001;


        if ( params!=NULL ) {
            try {

// 	    if (mhs::mex_hasParam(params,"particle_point_weight_exponent")!=-1)
//                 scale_power=mhs::mex_getParam<int>(params,"particle_point_weight_exponent",1)[0];

                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

//             if (mhs::mex_hasParam(params,"DataScaleGrad")!=-1)
//                 DataScaleGrad=mhs::mex_getParam<T>(params,"DataScaleGrad",1)[0];

                if ( mhs::mex_hasParam ( params,"DataScaleGradSurface" ) !=-1 ) {
                    DataScaleGradSurface=mhs::mex_getParam<T> ( params,"DataScaleGradSurface",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScaleGradVessel" ) !=-1 ) {
                    DataScaleGradVessel=mhs::mex_getParam<T> ( params,"DataScaleGradVessel",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"Epsilon" ) !=-1 ) {
                    StdvEpsilon=mhs::mex_getParam<T> ( params,"Epsilon",1 ) [0];
                }
                
//                 if ( mhs::mex_hasParam ( params,"img_smooth_is_pol" ) !=-1 ) {
//                     img_smooth_is_pol=mhs::mex_getParam<bool> ( params,"img_smooth_is_pol",1 ) [0];
//                 }
                
                

            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:


    //*6+i
    template<typename D>
    inline bool interpolate2 ( const Vector<T,3> & pos,const T & multi_scale,const TData  *  alphas, int num_alphas, D & value,std::size_t stride=1,std::size_t indx=0 ) {
        int Z=std::floor ( pos[0] );
        int Y=std::floor ( pos[1] );
        int X=std::floor ( pos[2] );

        if ( ( Z+1>=feature_shape[0] ) || ( Y+1>=feature_shape[1] ) || ( X+1>=feature_shape[2] ) ||
                ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
            return false;
        }

        D wz= ( pos[0]-Z );
        D wy= ( pos[1]-Y );
        D wx= ( pos[2]-X );
        D & g=value;
        g=0;
        {
            g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( Z*feature_shape[1]+Y ) *feature_shape[2]+X ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( 1-wz ) * ( 1-wy ) * ( wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( Z*feature_shape[1]+Y ) *feature_shape[2]+ ( X+1 ) ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( 1-wz ) * ( wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( Z*feature_shape[1]+ ( Y+1 ) ) *feature_shape[2]+X ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( 1-wz ) * ( wy ) * ( wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( Z*feature_shape[1]+ ( Y+1 ) ) *feature_shape[2]+ ( X+1 ) ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( wz ) * ( 1-wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( ( Z+1 ) *feature_shape[1]+Y ) *feature_shape[2]+X ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( wz ) * ( 1-wy ) * ( wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( ( Z+1 ) *feature_shape[1]+Y ) *feature_shape[2]+ ( X+1 ) ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( wz ) * ( wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( ( Z+1 ) *feature_shape[1]+ ( Y+1 ) ) *feature_shape[2]+X ) *stride+indx ) *num_alphas,num_alphas-1 );
            g+= ( wz ) * ( wy ) * ( wx ) *eval_polynom1 ( multi_scale,alphas+ ( ( ( ( Z+1 ) *feature_shape[1]+ ( Y+1 ) ) *feature_shape[2]+ ( X+1 ) ) *stride+indx ) *num_alphas,num_alphas-1 );
        }

        return true;

    }


public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
#ifdef D_USE_GUI
//         ,std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
        ,std::list<std::string>  * debug=NULL
#endif
    ) {
      
      
      try 
      {

        Vector<T,Dim> position=position_org;
        position/=this->element_size;

// 	position[0]=std::round(position[0]);
// 	position[1]=std::round(position[1]);
// 	position[2]=std::round(position[2]);

        int Z=std::floor ( position[0] );
        int Y=std::floor ( position[1] );
        int X=std::floor ( position[2] );

        if ( ( Z+1>=this->feature_shape[0] ) || ( Y+1>=this->feature_shape[1] ) || ( X+1>=this->feature_shape[2] ) ||
                ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
            return false;
        }

        T org_scale=radius;

        radius*=scale_correction;

        if ( ( radius<min_scale-0.01 )
                || ( radius>max_scale+0.01 ) ) {
            printf ( "[%f %f %f]\n",min_scale,radius,max_scale );
            sta_assert_error ( ! ( radius<min_scale ) );
            sta_assert_error ( ! ( radius>max_scale ) );
            //printf("%f %f %f\n",min_scale,radius,max_scale);
            //radius=std::min(max_scale,std::max(radius,min_scale));
        }


        radius=std::min ( max_scale,std::max ( radius,min_scale ) );
	
	const T boundary_scale=2;
	if ( (scale_dependend_boundary) &&
	  ( ( position[0]+boundary_scale*org_scale>=this->feature_shape[0] ) ||
	     ( position[1]+boundary_scale*org_scale>=this->feature_shape[1] ) ||
	     ( position[2]+boundary_scale*org_scale>=this->feature_shape[2] ) ||
             ( position[0]-boundary_scale*org_scale<0 ) ||
             ( position[1]-boundary_scale*org_scale<0 ) ||
	     ( position[2]-boundary_scale*org_scale<0 ) ))
	     {
	     
            return false;
        }
	
	
//         if (radius<min_scale)
//             radius=min_scale;


        T multi_scale=radius;
// 	T multi_scale=std::sqrt(radius);
        //T multi_scale=std::sqrt(mylog(radius+1)/logscalefac);
        //T multi_scale=std::sqrt(std::log(radius+1)/logscalefac);
        //T multi_scale=std::sqrt(mylog(radius)/logscalefac);
        //T multi_scale=radius;

        T wz= ( position[0]-Z );
        T wy= ( position[1]-Y );
        T wx= ( position[2]-X );


        T stdv=0;
        {
            stdv+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( Z*this->feature_shape[1]+Y ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( 1-wz ) * ( 1-wy ) * ( wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( Z*this->feature_shape[1]+Y ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( 1-wz ) * ( wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( Z*this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( 1-wz ) * ( wy ) * ( wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( Z*this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( wz ) * ( 1-wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+Y ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( wz ) * ( 1-wy ) * ( wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+Y ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( wz ) * ( wy ) * ( 1-wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
            stdv+= ( wz ) * ( wy ) * ( wx ) *eval_polynom1 ( multi_scale,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
        }
//         if (stdv<0)
// 	  printf("%f\n",stdv);


//         stdv=T(1.0)/(std::max(stdv,T(0))+StdvEpsilon);
        stdv= ( std::max ( stdv,T ( 0 ) ) );



        T steer_filter=0;

        T steer_filter2=0;

        T steer_filter3=0;

        if ( ( false ) && ( hessian!=NULL ) ) {

            T H[6];
// 	    T H2[6];
            for ( int i=0; i<6; i++ ) {

	      
//                 if ( !data_interpolate_polynom<T,T,TData> ( position,multi_scale,hessian, num_alphas_hessian,H[i],feature_shape.v,6,i ) ) {
//                     return false;
//                 }
		if ( !interpolate2 ( position,multi_scale,hessian, num_alphas_hessian,H[i],6,i ) ) {
                    return false;
                }

            }

            T & xx=  H[0]; //xx
            T & xy=  H[3]; //xy
            T & xz=  H[5]; //xz
            T & yy=  H[1]; //yy
            T & yz=  H[4]; //yz
            T & zz=  H[2]; //zz

            const T & nx=direction[2];
            const T & ny=direction[1];
            const T & nz=direction[0];

            steer_filter2= ( nx*nx*xx+ny*ny*yy+nz*nz*zz+
                             +2*nx* ( ny*xy+nz*xz )
                             +2*ny*nz*yz );

        }
//  	else
        if ( true ) {
            {
		  T h=std::max(radius/T(2),T(1));
          	  //T h=1;
		  T hz=h/element_size[0];
		  T hy=h/element_size[1];
		  T hx=h/element_size[2];
//                 T hz=h;
//                 T hy=h;
//                 T hx=h;
                //T h=1;

                int count=0;
                Vector<T,3> pos;
                bool valid[3];
                T img_patch[3][3][3];
                for ( int z=0; z<3; z++ ) {
                    valid[0] = ( z==1 );
                    pos[0]=position[0]+ ( z-1 ) *hz;
                    for ( int y=0; y<3; y++ ) {
                        valid[1] = ( y==1 );
                        pos[1]=position[1]+ ( y-1 ) *hy;
                        for ( int x=0; x<3; x++ ) {
                            img_patch[x][y][z]=0;
                            valid[2] = ( x==1 );
                            if ( valid[0]||valid[1]||valid[2] ) {
                                count++;
                                pos[2]=position[2]+ ( x-1 ) *hx;
				 if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,img_patch[x][y][z] ,feature_shape.v) ) {
				      return false;
				 }
                            }
                        }
                    }
                }


                T dxx= ( img_patch[0][1][1]-2*img_patch[1][1][1]+img_patch[2][1][1] );
                T dyy= ( img_patch[1][0][1]-2*img_patch[1][1][1]+img_patch[1][2][1] );
                T dzz= ( img_patch[1][1][0]-2*img_patch[1][1][1]+img_patch[1][1][2] );

                T trace=dxx+dyy+dzz;
                dxx-=trace;
                dyy-=trace;
                dzz-=trace;

                T dxy=T ( 0.25 ) * ( ( img_patch[0][0][1]+img_patch[2][2][1] )- ( img_patch[0][2][1]+img_patch[2][0][1] ) );
                T dyz=T ( 0.25 ) * ( ( img_patch[1][0][0]+img_patch[1][2][2] )- ( img_patch[1][0][2]+img_patch[1][2][0] ) );
                T dxz=T ( 0.25 ) * ( ( img_patch[0][1][0]+img_patch[2][1][2] )- ( img_patch[0][1][2]+img_patch[2][1][0] ) );

                T N=radius*radius/ ( h*h );

                const  T & nx=direction[2];
                const  T & ny=direction[1];
                const  T & nz=direction[0];
//
                steer_filter=N* ( nx*nx*dxx+ny*ny*dyy+nz*nz*dzz+
                                  +2*nx* ( ny*dxy+nz*dxz )
                                  +2*ny*nz*dyz );
                sta_assert_error ( count==19 );
            }
        }


        
//         T steer_filter4=0;
// 	T steer_filter5=0;
	if (false)
        {
	  T steer_filter3_test=0;
	   {

                Vector<T,3> vn[2];

                mhs_graphics::createOrths ( direction,vn[0],vn[1] );

                vn[0]/=element_size;
                vn[1]/=element_size;

                T h=std::max(T(1),radius/2);
                T Lap=0;
                Vector<T,3> pos;


                    T lap=0;
                    T lap2=0;
		    T lap3=0;
		    
                    T v;
                    bool valid[2];

                    for ( int x=0; x<3; x++ ) {
                        valid[0]= ( x==1 );
                        for ( int y=0; y<3; y++ ) {
                            valid[1]= ( y==1 );
                            
                                pos=position+vn[0]* ( x-1 ) *h+vn[1]* ( y-1 ) *h;
                                v=0;
				 if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,v ,feature_shape.v) ) {
				      return false;
				  }
                            if ( ( valid[0] ) || ( valid[1] ) ) {    
                                if ( ( x!=1 ) || ( y!=1 ) ) {
                                    lap+=v;
                                } else {
                                    lap2+=v;
                                }
                            } else 
			    {
				    lap3+=v;
			    }
                        }
                    }


                    T N=radius*radius/ ( h*h );
                    const T gamma=1.0/3.0;
                    Lap+=N* ( ( 1.0-gamma ) * ( lap-4*lap2 ) +gamma* ( 0.5*lap3-2*lap2 ) );

                    steer_filter3= ( -Lap );
// 		   steer_filter3_test= ( -Lap );
            }
//             {
// 
//                 Vector<T,3> vn[2];
// 
//                 mhs_graphics::createOrths ( direction,vn[0],vn[1] );
// 
//                 vn[0]/=element_size;
//                 vn[1]/=element_size;
// 
//                 const int samples=1;
//                 T h[samples];
// 		
// 		
//                 h[0]=1;
//                 h[1]=radius/2;
// 		
// 		h[0]=std::max(T(1),radius/2);
// 
//                 T Lap=0;
// 
// 
//                 Vector<T,3> pos;
// 
// 
//                 for ( int s=0; s<samples; s++ ) {
//                     T lap=0;
//                     T lap2=0;
// 
//                     T v;
//                     bool valid[2];
// 
//                     for ( int x=0; x<3; x++ ) {
//                         valid[0]= ( x==1 );
//                         for ( int y=0; y<3; y++ ) {
//                             valid[1]= ( y==1 );
//                             if ( ( valid[0] ) || ( valid[1] ) ) {
//                                 pos=position+vn[0]* ( x-1 ) *h[s]+vn[1]* ( y-1 ) *h[s];
//                                 v=0;
// 				if (img_smooth_is_pol)
// 				{
// 				  if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth,v ,feature_shape.v) ) {
// 				      return false;
// 				  }
// 				}
//                                 else 
// 				{
// 				  if ( !data_interpolate_local_polynom  ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,v,feature_shape.v ) ) {
// 				      return false;
// 				  }
// 				 
// 				}
//                                 
//                                 if ( ( x!=1 ) || ( y!=1 ) ) {
//                                     lap+=v;
//                                 } else {
//                                     lap2+=v;
//                                 }
//                             }
//                         }
//                     }
// 
//                     T lap3=0;
//                     for ( int x=0; x<3; x++ ) {
//                         valid[0]= ( x!=1 );
//                         for ( int y=0; y<3; y++ ) {
//                             valid[1]= ( y!=1 );
//                             if ( ( valid[0] ) && ( valid[1] ) ) {
//                                 pos=position+vn[0]* ( x-1 ) *h[s]+vn[1]* ( y-1 ) *h[s];
//                                 v=0;
// 				
// 				
//                               if (img_smooth_is_pol)
// 				{
// 				  if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth,v ,feature_shape.v) ) {
// 				      return false;
// 				  }
// 				}
//                                 else 
// 				{
// 				  if ( !data_interpolate_local_polynom  ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,v,feature_shape.v ) ) {
// 				      return false;
// 				  }
// 				 
// 				}
//                                 lap3+=v;
//                             }
//                         }
//                     }
// 
//                     T N=radius*radius/ ( h[s]*h[s]*samples );
//                     const T gamma=1.0/3.0;
//                     Lap+=N* ( ( 1.0-gamma ) * ( lap-4*lap2 ) +gamma* ( 0.5*lap3-2*lap2 ) );
//                 }
// 
//                 steer_filter3= ( -Lap );
//             }
//             
//             printf("%f %f\n",steer_filter3,steer_filter3_test);
        }







        T eigen_values[2];
        eigen_values[0]=eigen_values[1]=0;
	if (false)
        {
            Vector<T,3> vn[2];
            mhs_graphics::createOrths ( direction,vn[0],vn[1] );

            T h=radius;

            int count=0;
            Vector<T,3> pos;
            bool valid[3];
            T img_patch[3][3];


            for ( int y=0; y<3; y++ ) {

                for ( int x=0; x<3; x++ ) {
                    pos=position+vn[0]* ( x-1 ) *h+vn[1]* ( y-1 ) *h;
//                     if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth,img_patch[x][y],feature_shape.v ) ) {
//                         return false;
//                     }
		    if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,img_patch[x][y] ,feature_shape.v) ) {
				      return false;
				  }
				  
//                     if (img_smooth_is_pol)
// 		    {
// 		      if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth, img_patch[x][y],feature_shape.v) ) {
// 			  return false;
// 		      }
// 		    }
// 		    else 
// 		    {
// 		      if ( !data_interpolate_local_polynom  ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,img_patch[x][y],feature_shape.v ) ) {
// 			  return false;
// 		      }
// 		      
// 		    }
                }
            }
            
         

            T dxx= ( img_patch[0][1]-2*img_patch[1][1]+img_patch[2][1] );
            T dyy= ( img_patch[1][0]-2*img_patch[1][1]+img_patch[1][2] );
            T dxy=T ( 0.25 ) * ( ( img_patch[0][0]+img_patch[2][2] )- ( img_patch[0][2]+img_patch[2][0] ) );


            T rootsqr=dxx*dxx + 4*dxy*dxy - 2*dxx*dyy + dyy*dyy;

            if ( rootsqr>0 ) {
                T root=std::sqrt ( rootsqr );
                eigen_values[0]=0.5* ( dxx+dyy-root );
                eigen_values[1]=0.5* ( dxx+dyy+root );

            }
        }


        T local_Grad=0;
	if (false)
        {
            Vector<T,3> vn[2];
            mhs_graphics::createOrths ( direction,vn[0],vn[1] );

// 	  T h=radius;
            T h=1;

            int count=0;
            Vector<T,3> pos;
            bool valid[3];
            T img_patch[3][3];

            for ( int y=0; y<3; y++ ) {
                for ( int x=0; x<3; x++ ) {
                    pos=position+vn[0]* ( x-1 ) *h+vn[1]* ( y-1 ) *h;
//                     if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth,img_patch[x][y],feature_shape.v ) ) {
//                         return false;
//                     }
		    if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,img_patch[x][y] ,feature_shape.v) ) {
		      return false;
		  }
		    
// 		    if (img_smooth_is_pol)
// 		    {
// 		      if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth, img_patch[x][y],feature_shape.v) ) {
// 			  return false;
// 		      }
// 		    }
// 		    else 
// 		    {
// 		      if ( !data_interpolate_local_polynom  ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,img_patch[x][y],feature_shape.v ) ) {
// 			  return false;
// 		      }
// 		      
// 		    }
                }
            }

            for ( int y=0; y<3; y++ ) {
                for ( int x=0; x<3; x++ ) {
                    if ( ( x!=1 ) || ( y!=1 ) ) {
                        local_Grad=std::max ( ( img_patch[x][y]-img_patch[1][1] ),local_Grad );
                    }
                }
            }
            local_Grad*=radius/h;
        }





        T vessel_grad_l=0;
        if ( false ) {

            Vector<T,3> vn[3];
            vn[0]=direction;
            mhs_graphics::createOrths ( vn[0],vn[1],vn[2] );
            Vector<T,3> pos;
            T grad[2];
            T h=radius/2;

            vn[0]/=element_size;
            vn[1]/=element_size;
            vn[2]/=element_size;

            for ( int i=0; i<3; i++ ) {

                for ( int d=0; d<2; d++ ) {

                    if ( d==0 ) {
                        pos=position+vn[i]* ( h );
                    } else {
                        pos=position-vn[i]* ( h );
                    }

//                     if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth,grad[d],feature_shape.v) ) {
//                         return false;
//                     }
		    if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,grad[d] ,feature_shape.v) ) {
				      return false;
				  }
//                     if (img_smooth_is_pol)
// 		    {
// 		      if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth, grad[d],feature_shape.v) ) {
// 			  return false;
// 		      }
// 		    }
// 		    else 
// 		    {
// 		      if ( !data_interpolate_local_polynom  ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,grad[d],feature_shape.v ) ) {
// 			  return false;
// 		      }
// 		      
// 		    }

                }
                vessel_grad_l+= ( ( grad[0]-grad[1] ) * ( grad[0]-grad[1] ) ) / ( h*h );
            }
            vessel_grad_l=radius*std::sqrt ( vessel_grad_l );
        } else {


	    //T h=T(0.5);
	    T h=radius/T(2);
	  
            Vector<T,3> pos;
            T grad[2];

            Vector<T,3> dir;
            dir=direction/element_size;


            for ( int d=0; d<2; d++ ) {

                if ( d==0 ) {
                    pos=position+dir*h;
                } else {
                    pos=position-dir*h;
                }
                
		  if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,grad[d] ,feature_shape.v) ) {
		    return false;
		}
		
//                     if (img_smooth_is_pol)
// 		    {
// 		      if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth, num_alphas_img_smooth, grad[d],feature_shape.v) ) {
// 			  return false;
// 		      }
// 		    }
// 		    else 
// 		    {
// 		      if ( !data_interpolate_local_polynom  ( pos,multi_scale,scales,img_smooth, num_alphas_img_smooth,grad[d],feature_shape.v ) ) {
// 			  return false;
// 		      }
// 		      
// 		    }

            }
            vessel_grad_l+= ( ( grad[0]-grad[1] ) * ( grad[0]-grad[1] ) ) /(4*h*h);

//  	vessel_grad_l=radius*std::sqrt(vessel_grad_l);
            vessel_grad_l=std::sqrt ( vessel_grad_l );

        }


        
        //T stdv_fixed=0;
        
        T rad_grad=0;
        if ((radius>1.5) &&( DataScaleGradSurface>0 )) {
	  
	  
	  
//         if (stdv<0)
// 	  printf("%f\n",stdv);


//         stdv=T(1.0)/(std::max(stdv,T(0))+StdvEpsilon);
        



            Vector<T,3> vn[3];
            vn[0]=direction;
            mhs_graphics::createOrths ( vn[0],vn[1],vn[2] );
            vn[1]/=element_size;
            vn[2]/=element_size;

            int N=std::floor ( 2*M_PI*radius+1 );
	    
// 	    T radial_edge_smooth=multi_scale;
	    T radial_edge_smooth=std::max(T(1.5),min_scale);
		
// 		{
// 		stdv_fixed+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( Z*this->feature_shape[1]+Y ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( 1-wz ) * ( 1-wy ) * ( wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( Z*this->feature_shape[1]+Y ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( 1-wz ) * ( wy ) * ( 1-wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( Z*this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( 1-wz ) * ( wy ) * ( wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( Z*this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( wz ) * ( 1-wy ) * ( 1-wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+Y ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( wz ) * ( 1-wy ) * ( wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+Y ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( wz ) * ( wy ) * ( 1-wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+X ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 		stdv_fixed+= ( wz ) * ( wy ) * ( wx ) *eval_polynom1 ( radial_edge_smooth,local_stdv+ ( ( ( ( Z+1 ) *this->feature_shape[1]+ ( Y+1 ) ) *this->feature_shape[2]+ ( X+1 ) ) ) *num_alphas_local_stdv,num_alphas_local_stdv-1 );
// 	      }
//	    stdv_fixed= ( std::max (stdv_fixed,T ( 0 ) ) );

            Vector<T,3> dir;
            Vector<T,3> pos;
	    
	    //int radial_samples=1+(radius>3);
	    int radial_samples=1;
	    
	    {
                T mean=0;
                T var=0;
		
                for ( int n=0; n<N; n++ ) {
                    std::complex<T> d=std::exp ( std::complex<T> ( 0,2*M_PI*n/ ( ( T ) N ) ) );
                    T x=std::real ( d );
                    T y=std::imag ( d );


                    dir=vn[1]*x+vn[2]*y;
		    
		    		    
		    for (int s=0;s<radial_samples;s++)
		    {
		      T step=T(s+1)/radial_samples;
		      pos=position+dir*radius*step;
		      
		      T g=0;
		      if ( !data_interpolate_polynom ( pos,radial_edge_smooth,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,g ,feature_shape.v) ) 
		      {
			return false;
		      }
		      mean+=g;
		    }
                }

                mean/=N*radial_samples;
                for ( int n=0; n<N; n++ ) {
                    std::complex<T> d=std::exp ( std::complex<T> ( 0,2*M_PI*n/ ( ( T ) N ) ) );
                    T x=std::real ( d );
                    T y=std::imag ( d );
                    dir=vn[1]*x+vn[2]*y;
                    

		    for (int s=0;s<radial_samples;s++)
		    {
		      T step=T(s+1)/radial_samples;
		      pos=position+dir*radius*step;
		      
		      T g=0;
		      if ( !data_interpolate_polynom ( pos,radial_edge_smooth,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,g ,feature_shape.v) ) {
			return false;
		      }
		      var+= ( g-mean ) * ( g-mean );
		    }
                }
                var/=N*radial_samples;

                rad_grad=std::sqrt ( var );
            }
//             {
//                 T mean=0;
//                 T var=0;
//                 for ( int n=0; n<N; n++ ) {
//                     std::complex<T> d=std::exp ( std::complex<T> ( 0,2*M_PI*n/ ( ( T ) N ) ) );
//                     T x=std::real ( d );
//                     T y=std::imag ( d );
// 
// 
//                     dir=vn[1]*x+vn[2]*y;
//                     pos=position+dir*radius;
// 		    
// 		    T g=0;
// 		    if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,g ,feature_shape.v) ) {
// 		      return false;
// 		}
//                     mean+=g;
//                 }
// 
//                 mean/=N;
//                 for ( int n=0; n<N; n++ ) {
//                     std::complex<T> d=std::exp ( std::complex<T> ( 0,2*M_PI*n/ ( ( T ) N ) ) );
//                     T x=std::real ( d );
//                     T y=std::imag ( d );
//                     dir=vn[1]*x+vn[2]*y;
//                     pos=position+dir*radius;
// 
// 		    T g=0;
// 		    if ( !data_interpolate_polynom ( pos,multi_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,g ,feature_shape.v) ) {
// 		      return false;
// 		  }
//                     var+= ( g-mean ) * ( g-mean );
//                 }
//                 var/=N;
// 
//                 rad_grad=std::sqrt ( var );
//             }
        }



        T scale1=1;
        switch ( scale_power ) {
        case 1:
            scale1=org_scale;
            break;
        case 2:
            scale1=org_scale*org_scale;
            break;
        case 3:
            scale1=org_scale*org_scale*org_scale;
            break;
        }


#ifdef D_USE_GUI
        if ( debug!=NULL ) {
            std::stringstream s;
            s.precision ( 3 );
            s<<"------------------";
            debug->push_back ( s.str() );

//             s.str ( "" );
//             s<<"EV :"<<eigen_values[0]<<" : "<<eigen_values[1];
//             debug->push_back ( s.str() );
// 
//             s.str ( "" );
//             s<<"local_Grad :"<<local_Grad<<" : "<<local_Grad*stdv;
//             debug->push_back ( s.str() );
// 
// 
// 
//             s.str ( "" );
//             s<<"nsteer_filter :"<<stdv*steer_filter<<" / true : "<<stdv*steer_filter2<<" / new :"<<stdv*steer_filter3<<" / new2 :"<<stdv*steer_filter4<<" / new3 :"<<stdv*(steer_filter5-steer_filter4);
//             debug->push_back ( s.str() );
	    




            s.str ( "" );
            s<<" true : "<<stdv*steer_filter2<<" / new :"<<stdv*steer_filter;
            debug->push_back ( s.str() );

            s.str ( "" );
            s<<"ngrad :"<<stdv*std::abs ( vessel_grad_l );
            debug->push_back ( s.str() );

            s.str ( "" );
            s<<"rad_grad :"<<stdv*rad_grad;
            debug->push_back ( s.str() );

            s.str ( "" );
            s<<"sdv :"<<stdv;
            debug->push_back ( s.str() );


        }
#endif


        result=-DataScale*stdv*steer_filter*scale1
               +DataScaleGradVessel*stdv*std::abs ( vessel_grad_l ) *scale1;

        if ( DataScaleGradSurface>0 ) {
            result+=DataScaleGradSurface*stdv*rad_grad*scale1;
        }
// 	  result=-DataScale*stdv*steer_filter2*scale1;
        
      }
	catch ( mhs::STAError error ) {
            throw error;
        }

        return true;
    }
};

#endif
