#ifndef MHS_DATA_GVF_FILTER_H
#define MHS_DATA_GVF_FILTER_H


#include "mhs_data.h"


template <typename T,typename TData,int Dim>
class CDataGVFFilter: public CData<T,TData,Dim>
{
private:
    int scale_power;


    const TData * 	normalized_gvf;
    const TData * 	vesselness_gvf_based;

    T DataThreshold;

    T DataScaleH;
    T DataScale;
    T DataScaleGradVessel;

    T min_scale;
    T max_scale;


    T ** circle_pts;
    const int maxrad=15;

public:
    float get_minscale()
    {
        return min_scale;
    };
    float get_maxscale()
    {
        return max_scale;
    };

    float get_scalecorrection()
    {
        return 1;
    };
    
    
    #ifdef D_USE_GUI	    
    void read_controls(const mxArray * handle){};
    void set_controls(const mxArray * handle){};
    #endif    
    


    CDataGVFFilter()  :  CData<T,TData,Dim>()
    {
        circle_pts=new T*[maxrad];
        for (int r=0; r<maxrad; r++)
        {
            int N=std::floor(2*M_PI*r+1);
            circle_pts[r]=new T[2*N];
            for (int n=0; n<N; n++)
            {
                circle_pts[r][2*n]=std::real(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
                circle_pts[r][2*n+1]=std::imag(std::exp(std::complex<T>(0,2*M_PI*n/((T)N))));
            }
        }

    }

    ~CDataGVFFilter()
    {
        for (int r=0; r<maxrad; r++)
        {
            delete [] circle_pts[r];
        }
        delete [] circle_pts;
    }


    void init(const mxArray * feature_struct)
    {
//         for (int i=0; i<3; i++)
//             this->shape[i]=shape[i];
        for (int i=0; i<3; i++)
            this->shape[i]=mhs::dataArray<TData>(feature_struct,"cshape").data[i];
        std::swap(this->shape[0],this->shape[2]);
        printf("shape %d %d %d\n",this->shape[0],this->shape[1],this->shape[2]);

        mhs::dataArray<TData>  tmp;
        {
            tmp=mhs::dataArray<TData>(feature_struct,"NGVF");
            normalized_gvf=tmp.data;
        }
        {
            tmp=mhs::dataArray<TData>(feature_struct,"H");
            vesselness_gvf_based=tmp.data;
        }
        TData * scales=mhs::dataArray<TData>(feature_struct,"scales").data;
        min_scale=scales[0];
        max_scale=scales[mhs::dataArray<TData>(feature_struct,"scales").dim[0]-1];
    }

    void set_params(const mxArray * params=NULL)
    {
        scale_power=1;
        DataScale=1;
        DataScaleH=1;
        DataThreshold=1;
        DataScaleGradVessel=1;

        if (params!=NULL)
        {
            if (mhs::mex_hasParam(params,"scale_power")!=-1)
                scale_power=mhs::mex_getParam<int>(params,"scale_power",1)[0];
            if (mhs::mex_hasParam(params,"DataScale")!=-1)
                DataScale=mhs::mex_getParam<T>(params,"DataScale",1)[0];
            if (mhs::mex_hasParam(params,"DataScaleH")!=-1)
                DataScaleH=mhs::mex_getParam<T>(params,"DataScaleH",1)[0];
            if (mhs::mex_hasParam(params,"DataThreshold")!=-1)
                DataThreshold=mhs::mex_getParam<T>(params,"DataThreshold",1)[0];
            if (mhs::mex_hasParam(params,"DataScaleGradVessel")!=-1)
                DataScaleGradVessel=mhs::mex_getParam<T>(params,"DataScaleGradVessel",1)[0];

        }
    }

private:
  
  

    bool eval_data(
        T & result,
// 	T & saliency,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius
		  #ifdef D_USE_GUI        
//         , std::list<class CData<T,TData,Dim>::CDebugInfo>  * debug=NULL
	, std::list<std::string>  * debug=NULL
	#endif     
    )
    {
//       return false;
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;



        if ((radius<min_scale-0.01)
                ||(radius>max_scale+0.01))
        {
            printf("[%f %f %f]\n",min_scale,radius,max_scale);
            sta_assert_error(!(radius<min_scale));
            sta_assert_error(!(radius>max_scale));
        }
        radius=std::min(max_scale,std::max(radius,min_scale));


        T multi_scale=std::sqrt(radius);

        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);

        Vector<T,Dim> gvf_n=T(0);
        for (int i=0; i<3; i++)
        {
            T & g=gvf_n.v[i];
            g+=(1-wz)*(1-wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(1-wz)*(1-wy)*(wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(1-wz)*(wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(1-wz)*(wy)*(wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(1-wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(wz)*(1-wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(wz)*(wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
        }
        std::swap(gvf_n[2],gvf_n[0]);


        T vessel_grad_l=std::sqrt(gvf_n.norm2());

        gvf_n/=vessel_grad_l+std::numeric_limits<T>::epsilon();
        T vessel_grad_p=(direction.dot(gvf_n));
        vessel_grad_l*=vessel_grad_p*vessel_grad_p;




        Vector<T,3> vn1;
        Vector<T,3> vn2;
        const  Vector<T,Dim> & direction2=direction;
        mhs_graphics::createOrths(direction2,vn1,vn2);

        Vector<T,3> sample_p;
        Vector<T,3> sample_dir;
        Vector<T,3> grad;
	Vector<T,3> voting_pt;

        int r=std::floor(radius+0.5);
        int N=std::floor(2*M_PI*r+1);
        sta_assert(r<maxrad);
        sta_assert(r>-1);
	
// 	direction.print();
// 	vn1.print();
// 	vn2.print();
// 	printf("P=[%f %f %f]\n",position[2],position[1],position[0]);
// 	printf("C=[...\n");
//  	//printf("-----------\n");	
	T vote=0;
        for (int n=0; n<N; n++)
        {
            sample_dir=vn1*(circle_pts[r][2*n])
                       +vn2*(circle_pts[r][2*n+1]);

		       
	    
            sample_p=position+sample_dir*radius;
	    //printf("dist: %f\n",std::sqrt((sample_p-position).norm2()));

            int Z=std::floor(sample_p[0]);
            int Y=std::floor(sample_p[1]);
            int X=std::floor(sample_p[2]);

            if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                    (Z<0)||(Y<0)||(X<0))
                continue;
	    
	    
// 	    for (int i=0;i<3;i++) 
// 	      sample_p[i]=std::floor(sample_p[i]);

            T wz=(sample_p[0]-Z);
            T wy=(sample_p[1]-Y);
            T wx=(sample_p[2]-X);
	    
	    
	    

            grad=T(0);
            for (int i=0; i<3; i++)
            {
                T & g=grad.v[i];
                g+=(1-wz)*(1-wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                g+=(1-wz)*(1-wy)*(wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                g+=(1-wz)*(wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                g+=(1-wz)*(wy)*(wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                g+=(wz)*(1-wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                g+=(wz)*(1-wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                g+=(wz)*(wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                g+=(wz)*(wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
            }
            
            
            
            std::swap(grad.v[0],grad.v[2]);
	    
	     grad.normalize();
            voting_pt=sample_p+grad*radius;

	    //if (sample_dir.dot(grad)<0)
	    {
		T HoughSigma=radius;//*radius*radius; 
		vote+=std::exp(-(position-voting_pt).norm2()/(HoughSigma));
	    }
	    //printf("%f %f %f;...\n",voting_pt[2],voting_pt[1],voting_pt[0]);
	    //printf("%f %f %f;...\n",sample_p[2],sample_p[1],sample_p[0]);
// 	    printf("%f %f %f %f %f %f %f %f %f %f %f %f;...\n",sample_p[2],sample_p[1],sample_p[0],
// 		   grad[2],grad[1],grad[0],
// 	    sample_dir[2],sample_dir[1],sample_dir[0],
// 	    sample_dir.dot(grad),sample_dir.norm2(),grad.norm2());
	
	
	    //printf("%1.2f %1.2f %1.2f | %1.2f %1.2f %1.2f\n",grad.dot(Vector<T,3>(1,0,0)),grad.dot(Vector<T,3>(0,1,0)),grad.dot(Vector<T,3>(0,0,1)),(position-voting_pt).norm2(),radius,vote);	
	    
	    
// 	    if (std::abs(direction[0])>0.8)
// 	    {
//   	    printf("--------\n");
 	    //printf("%1.2f %1.2f %1.2f\n",(position-voting_pt).norm2(),radius,vote);	
// 	    position.print();
// 	    sample_p.print();
//  	    grad.print();
// 	    printf("%f %f %f\n",normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+0],
// 		    normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+1],
// 		    normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+2]);
// 	    
//  	    sample_dir.print();
// 	    T v=std::sqrt((position-voting_pt).norm2());
// 	    printf("%f / %f\n",grad.dot(sample_dir),v);
// 	    }
//             vote+=std::max(-sample_dir.dot(grad),T(0));
        }
//         printf("];\n");
	//img=squeeze(FeatureData.img(:,:,12));img(round(P(1)),round(P(2)))=0;img(sub2ind(shape(1:2),floor(1+C(:,1)),floor(1+C(:,2))))=0;shape=size(img);[X,Y]=meshgrid(1:shape(2),1:shape(1));figure(123);clf;hold on; imagesc(img);quiver(C(:,2)+0.5,C(:,1)+0.5,C(:,4),C(:,5));
	
	//img=squeeze(FeatureData.img(:,:,12));img(round(P(1)),round(P(2)))=0;img(sub2ind(shape(1:2),floor(1+C(:,1)),floor(1+C(:,2))))=0;shape=size(img);[X,Y]=meshgrid(1:shape(2),1:shape(1));figure(123);clf;hold on; imagesc(img);quiver(X,Y,squeeze(FeatureData.NGVF(1,:,:,12)),squeeze(FeatureData.NGVF(2,:,:,12)));
	
	
        vote/=N;
	//if (vote>0.25)
	
	

//         T vote=0;
//         for (int n=0; n<N; n++)
//         {
//             sample_dir=vn1*(circle_pts[r][2*n])
//                        +vn2*(circle_pts[r][2*n+1]);
// 
//             sample_p=position+sample_dir*radius;
// 
//             int Z=std::floor(sample_p[0]);
//             int Y=std::floor(sample_p[1]);
//             int X=std::floor(sample_p[2]);
// 
//             if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
//                     (Z<0)||(Y<0)||(X<0))
//                 continue;
// 
//             T wz=(sample_p[0]-Z);
//             T wy=(sample_p[1]-Y);
//             T wx=(sample_p[2]-X);
// 
//             grad=T(0);
//             for (int i=0; i<3; i++)
//             {
//                 T & g=grad.v[i];
//                 g+=(1-wz)*(1-wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
//                 g+=(1-wz)*(1-wy)*(wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
//                 g+=(1-wz)*(wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
//                 g+=(1-wz)*(wy)*(wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
//                 g+=(wz)*(1-wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
//                 g+=(wz)*(1-wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
//                 g+=(wz)*(wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
//                 g+=(wz)*(wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
//             }
//             
//             
//             
//             std::swap(grad.v[0],grad.v[2]);
// 	    
// 	    printf("%f %f %f\n",grad.dot(Vector<T,3>(1,0,0)),grad.dot(Vector<T,3>(0,1,0)),grad.dot(Vector<T,3>(0,0,1)));
// 	    
// // 	    if (std::abs(direction[0])>0.8)
// // 	    {
// // 	    printf("--------\n");
// // 	    grad.print();
// // 	    sample_dir.print();
// // 	    }
//             vote+=std::max(-sample_dir.dot(grad),T(0));
//         }
//         vote/=N;

// 	printf("%1.2f %1.2f %1.2f | %1.2f\n",direction[0],direction[1],direction[2],vote);



        T H[6];
        for (int i=0; i<6; i++)
        {
            T & g=H[i];
            g=0;
            g+=(1-wz)*(1-wy)*(1-wx)*vesselness_gvf_based[((Z*this->shape[1]+Y)*this->shape[2]+X)*6+i];
            g+=(1-wz)*(1-wy)*(wx)*vesselness_gvf_based[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i];
            g+=(1-wz)*(wy)*(1-wx)*vesselness_gvf_based[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i];
            g+=(1-wz)*(wy)*(wx)*vesselness_gvf_based[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i];
            g+=(wz)*(1-wy)*(1-wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*6+i];
            g+=(wz)*(1-wy)*(wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i];
            g+=(wz)*(wy)*(1-wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i];
            g+=(wz)*(wy)*(wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i];
        }

        T & xx=  H[0]; //xx
        T & xy=  H[3]; //xy
        T & xz=  H[5]; //xz
        T & yy=  H[1]; //yy
        T & yz=  H[4]; //yz
        T & zz=  H[2]; //zz

        T & nx=direction[2];
        T & ny=direction[1];
        T & nz=direction[0];

        T steer_filter=nx*nx*xx+ny*ny*yy+nz*nz*zz+
                       +2*nx*(ny*xy+nz*xz)
                       +2*ny*nz*yz;



        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }


      result=-DataScale*vote*radius*radius;
//         result=-DataScale*steer_filter*vote*scale1
                -DataThreshold;
//                -DataScaleH*steer_filter
//                +DataScaleGradVessel*std::abs(vessel_grad_l);
        return true;
    }

    
    

/*
    bool eval_data(
        T & result,
// 	T & saliency,
        Vector<T,Dim>& direction,
        Vector<T,Dim>& position,
        T radius)
    {
//       return false;
        int Z=std::floor(position[0]);
        int Y=std::floor(position[1]);
        int X=std::floor(position[2]);

        if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                (Z<0)||(Y<0)||(X<0))
            return false;



        if ((radius<min_scale-0.01)
                ||(radius>max_scale+0.01))
        {
            printf("[%f %f %f]\n",min_scale,radius,max_scale);
            sta_assert_error(!(radius<min_scale));
            sta_assert_error(!(radius>max_scale));
        }
        radius=std::min(max_scale,std::max(radius,min_scale));


        T multi_scale=std::sqrt(radius);

        T wz=(position[0]-Z);
        T wy=(position[1]-Y);
        T wx=(position[2]-X);

        Vector<T,Dim> gvf_n=T(0);
        for (int i=0; i<3; i++)
        {
            T & g=gvf_n.v[i];
            g+=(1-wz)*(1-wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(1-wz)*(1-wy)*(wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(1-wz)*(wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(1-wz)*(wy)*(wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(1-wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
            g+=(wz)*(1-wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
            g+=(wz)*(wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
            g+=(wz)*(wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
        }
        std::swap(gvf_n[2],gvf_n[0]);


        T vessel_grad_l=std::sqrt(gvf_n.norm2());

        gvf_n/=vessel_grad_l+std::numeric_limits<T>::epsilon();
        T vessel_grad_p=(direction.dot(gvf_n));
        vessel_grad_l*=vessel_grad_p*vessel_grad_p;




        Vector<T,3> vn1;
        Vector<T,3> vn2;
        const  Vector<T,Dim> & direction2=direction;
        mhs_graphics::createOrths(direction2,vn1,vn2);

        Vector<T,3> sample_p;
        Vector<T,3> sample_dir;
        Vector<T,3> grad;

        int r=std::floor(radius+0.5);
        int N=std::floor(2*M_PI*r+1);
        sta_assert(r<maxrad);
        sta_assert(r>-1);

        T vote=0;
        for (int n=0; n<N; n++)
        {
            sample_dir=vn1*(circle_pts[r][2*n])
                       +vn2*(circle_pts[r][2*n+1]);

            sample_p=position+sample_dir*radius;

            int Z=std::floor(sample_p[0]);
            int Y=std::floor(sample_p[1]);
            int X=std::floor(sample_p[2]);

            if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
                    (Z<0)||(Y<0)||(X<0))
                continue;

            T wz=(sample_p[0]-Z);
            T wy=(sample_p[1]-Y);
            T wx=(sample_p[2]-X);

            grad=T(0);
            for (int i=0; i<3; i++)
            {
                T & g=grad.v[i];
                g+=(1-wz)*(1-wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                g+=(1-wz)*(1-wy)*(wx)*normalized_gvf[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                g+=(1-wz)*(wy)*(1-wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                g+=(1-wz)*(wy)*(wx)*normalized_gvf[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
                g+=(wz)*(1-wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
                g+=(wz)*(1-wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
                g+=(wz)*(wy)*(1-wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
                g+=(wz)*(wy)*(wx)*normalized_gvf[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
            }
            
            
            
            std::swap(grad.v[0],grad.v[2]);
	    
	    printf("%f %f %f\n",grad.dot(Vector<T,3>(1,0,0)),grad.dot(Vector<T,3>(0,1,0)),grad.dot(Vector<T,3>(0,0,1)));
	    
// 	    if (std::abs(direction[0])>0.8)
// 	    {
// 	    printf("--------\n");
// 	    grad.print();
// 	    sample_dir.print();
// 	    }
            vote+=std::max(-sample_dir.dot(grad),T(0));
        }
        vote/=N;

// 	printf("%1.2f %1.2f %1.2f | %1.2f\n",direction[0],direction[1],direction[2],vote);



        T H[6];
        for (int i=0; i<6; i++)
        {
            T & g=H[i];
            g=0;
            g+=(1-wz)*(1-wy)*(1-wx)*vesselness_gvf_based[((Z*this->shape[1]+Y)*this->shape[2]+X)*6+i];
            g+=(1-wz)*(1-wy)*(wx)*vesselness_gvf_based[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i];
            g+=(1-wz)*(wy)*(1-wx)*vesselness_gvf_based[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i];
            g+=(1-wz)*(wy)*(wx)*vesselness_gvf_based[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i];
            g+=(wz)*(1-wy)*(1-wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*6+i];
            g+=(wz)*(1-wy)*(wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*6+i];
            g+=(wz)*(wy)*(1-wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*6+i];
            g+=(wz)*(wy)*(wx)*vesselness_gvf_based[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*6+i];
        }

        T & xx=  H[0]; //xx
        T & xy=  H[3]; //xy
        T & xz=  H[5]; //xz
        T & yy=  H[1]; //yy
        T & yz=  H[4]; //yz
        T & zz=  H[2]; //zz

        T & nx=direction[2];
        T & ny=direction[1];
        T & nz=direction[0];

        T steer_filter=nx*nx*xx+ny*ny*yy+nz*nz*zz+
                       +2*nx*(ny*xy+nz*xz)
                       +2*ny*nz*yz;



        T scale1=1;
        switch(scale_power)
        {
        case 1:
            scale1=radius;
            break;
        case 2:
            scale1=radius*radius;
            break;
        case 3:
            scale1=radius*radius*radius;
            break;
        }


      result=-vote;
//         result=-DataScale*steer_filter*vote*scale1
//                -DataThreshold;
//                -DataScaleH*steer_filter
//                +DataScaleGradVessel*std::abs(vessel_grad_l);
        return true;
    }*/

};

#endif
