//#define _POOL_TEST
//#define _NUM_THREADS_ 4

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_quat.h"


	    
/*!
 *
 *  MAIN FUNCTION
 *
 * */
template <typename T>
//template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
      
    
    try 
    {
    
      if (nrhs<2)
	return;
      
      mhs::dataArray<T> quatdata=mhs::dataArray<T>(prhs[0]);
      std::size_t number_of_new_samples=mhs::dataArray<T>(prhs[1]).data[0];
      
      sta_assert_error(quatdata.dim.size()==2);
      sta_assert_error(quatdata.dim[1]==4);
      sta_assert_error(quatdata.dim[0]>1);
      
      std::size_t number_of_old_samples=quatdata.dim[0];
      printf("number of existing samples %d\n",quatdata.dim[0]);
      printf("number of new      samples %d\n",number_of_new_samples);
  
      
      std::size_t num_attributes =4;
      std::size_t ndims[2];
      ndims[1]=number_of_new_samples;
      ndims[0]=num_attributes;
      plhs[0] = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
      
      {
	std::size_t num_attributes =1;
	std::size_t ndims[2];
	ndims[1]=number_of_new_samples;
	ndims[0]=num_attributes;
	plhs[1] = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
      }
      
      T * quat_Data=quatdata.data;
      T * quat_Data_out= ( T * ) mxGetData (plhs[0] );
      T * valid_Data_out= ( T * ) mxGetData (plhs[1] );
      
      
      bool * vdata= new bool[number_of_old_samples];
      bool * vdata2= new bool[number_of_old_samples];
      
      for (std::size_t j=0;j<number_of_old_samples;j++)
      {
	
	bool valid=true;
	
	if (j<number_of_new_samples-1)
	{
	  Quaternion<T> cpQ1(quat_Data[(j)*4],quat_Data[(j)*4+1],quat_Data[(j)*4+2],quat_Data[(j)*4+3]);
	  Quaternion<T> cpQ2(quat_Data[(j+1)*4],quat_Data[(j+1)*4+1],quat_Data[(j+1)*4+2],quat_Data[(j+1)*4+3]);
	  
	  T dot=cpQ1.dot(cpQ2);
	  if (dot<0.9)
	    valid=false;
	}
	
	vdata[j]=valid;
	vdata2[j]=valid;
      }
      
      
      for (std::size_t i=0;i<number_of_old_samples;i++)
      {
	
	if (!vdata[i])
	    vdata2[i]=false;
	if (i<number_of_new_samples-1)
	{
	  if (!vdata[i+1])
	    vdata2[i]=false;
	}
	if (i>0)
	{
	  if (!vdata[i-1])
	    vdata2[i]=false;
	}
	
      }
      
      
      valid_Data_out= ( T * ) mxGetData (plhs[1] );
      
      for (std::size_t i=0;i<number_of_new_samples;i++)
      {
	double progress=double(number_of_old_samples*i)/double(number_of_new_samples-1);
	
// 	bool valid=true;
	
	std::size_t j=std::floor(progress);
//  	printf("%f\n",progress);
// 	
// 	sta_assert_error(j<number_of_old_samples);
	
	double t=progress-j;
	
	Quaternion<T> rot;
	
	if (i==number_of_new_samples-1)
	{
	  j=number_of_old_samples-1;
	  Quaternion<T> cpQ1(quat_Data[(j)*4],quat_Data[(j)*4+1],quat_Data[(j)*4+2],quat_Data[(j)*4+3]);
	  rot=cpQ1;
	}
	else if ((j>0)&&(j<number_of_old_samples-2))
	{
	  Quaternion<T> cpQ0(quat_Data[(j-1)*4],quat_Data[(j-1)*4+1],quat_Data[(j-1)*4+2],quat_Data[(j-1)*4+3]);
	  Quaternion<T> cpQ1(quat_Data[(j)*4],quat_Data[(j)*4+1],quat_Data[(j)*4+2],quat_Data[(j)*4+3]);
	  Quaternion<T> cpQ2(quat_Data[(j+1)*4],quat_Data[(j+1)*4+1],quat_Data[(j+1)*4+2],quat_Data[(j+1)*4+3]);
	  Quaternion<T> cpQ3(quat_Data[(j+2)*4],quat_Data[(j+2)*4+1],quat_Data[(j+2)*4+2],quat_Data[(j+2)*4+3]);
	  
	  rot=Quaternion<T>::squad(cpQ1,cpQ2,t,cpQ0,cpQ3);
	}
	else if (j<number_of_new_samples-1)
	{
	  Quaternion<T> cpQ1(quat_Data[(j)*4],quat_Data[(j)*4+1],quat_Data[(j)*4+2],quat_Data[(j)*4+3]);
	  Quaternion<T> cpQ2(quat_Data[(j+1)*4],quat_Data[(j+1)*4+1],quat_Data[(j+1)*4+2],quat_Data[(j+1)*4+3]);
	  
// 	  T dot=cpQ1.dot(cpQ2);
// 	  if (dot<0.75)
// 	    valid=false;
	  
	  rot=Quaternion<T>::slerp(cpQ1,cpQ2,t);
	} else
	{
	  Quaternion<T> cpQ1(quat_Data[(j)*4],quat_Data[(j)*4+1],quat_Data[(j)*4+2],quat_Data[(j)*4+3]);
	  rot=cpQ1;
	}
	
// 	*valid_Data_out=valid;
// 	valid_Data_out++;

	valid_Data_out[i]=vdata2[j];
	
	quat_Data_out[0]=rot[0];
	quat_Data_out[1]=rot[1];
	quat_Data_out[2]=rot[2];
	quat_Data_out[3]=rot[3];
	quat_Data_out+=4;
      }
      
      
      delete []vdata; 
      delete []vdata2; 
      
      
      
    
    } catch (mhs::STAError & error)
    {
       printf("%s\n",error.str().c_str());
    
    }

    

}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  
}







