#ifndef MHS_INTERACTIVE_VIEWER_H
#define MHS_INTERACTIVE_VIEWER_H


#include "mhs_gui_basic_viewer.h"
#include "mhs_cam.h"
#include "mhs_data.h"


template<typename TData,typename T, int Dim>
class CTrackerRenderer;

// class CMovie
//     {
//     public:
//       
//       double last_frame_timestamp;
//       std::size_t particle_number_changed;
//       std::size_t edge_number_changed;
//       std::size_t last_particle_number;
//       std::size_t last_edge_number;
//       mhs::CtimeStopper timer;
//       
//       CMovie()
//       {
// 	last_frame_timestamp=0;
// 	particle_number_changed=0;
// 	edge_number_changed=0;
// 	last_particle_number=0;
// 	last_edge_number=0;	
//       }
//       void check(CTracker & tracker)
//       {
// 	 //std::size_t n_bifurcations=tracker.connections.pool->get_num_bifurcation_centers();
// 	 //std::size_t n_points=tracker.particle_pool->get_numpts()-n_bifurcations;
// 	 std::size_t n_points=tracker.particle_pool->get_numpts();		
// 	 std::size_t n_edges=tracker.connections.pool->get_numpts();
// 	 if (last_particle_number!=n_points) 
// 	 {
// 	   particle_number_changed++;
// 	   last_particle_number=n_points;
// 	 }
// 	 if (last_edge_number!=n_edges) 
// 	 {
// 	   edge_number_changed++;
// 	   last_edge_number=n_edges;
// 	 }
// 	 bool create_frame=false;
// 	 double tstamp=timer.get_total_runtime();
// 	 
// 	 if (n_edges<20)
// 	 {
// 	   if (edge_number_changed>1)
// 	   {
// 	     create_frame=true;
// 	   }
// 	 }else if (n_edges<50)
// 	 {
// 	   if (edge_number_changed>2)
// 	   {
// 	     create_frame=true;
// 	   }
// 	 }else
// 	 {
// 	   if (edge_number_changed>((float)n_points/5))
// 	   {
// 	     create_frame=true;
// 	   }
// 	 }
// 	 
// 	 if (tstamp-last_frame_timestamp>1.0)
// 	 {
// 	    create_frame=true;
// 	 }
// 	 
// 	 if (n_points<1)
// 	   create_frame=false;
// 	 
// 	 if (create_frame)
// 	 {
// 	   last_edge_number=n_edges;
// 	   last_particle_number=n_points;
// 	   last_frame_timestamp=tstamp;
// 	   edge_number_changed=0;
// 	   particle_number_changed=0;
// // 	   printf("creating snapshot\n");
// 	     try {
// 	   tracker.save_video_snapshot();
// 	   } catch ( mhs::STAError error ) {
//                 printf ( "could'nt save video frame \n");
//             }
// 	   tracker.state_movie_frame++;
// 	 }
// 
//       }
//     };


class CParticleMeshes
{
protected:

    static const unsigned int num_particle_types=4;
    CTriangleObject * particles[num_particle_types];

    GLuint vertex_buffer;
    GLuint normal_buffer;
    GLuint index_buffer;

    struct CMesh {
        std::size_t i_offset;
        std::size_t i_length;
    };

//     std::size_t num_faces;

    CMesh * buffered_objects;
public:



    CParticleMeshes() {
        printf ( "init mesh " );

        std::size_t v_buffer_size=0;
        std::size_t i_buffer_size=0;
        for ( int i=0; i<num_particle_types; i++ ) {
            printf ( "%d ",i );
            particles[i]=new CTriangleObject ( i );
            //particles[i]=new CTriangleObject(2);
            v_buffer_size+=particles[i]->getNumVerts();
            i_buffer_size+=particles[i]->getNumIndeces();
            printf ( "object with %d verts and %d faces\n",particles[i]->getNumVerts(),particles[i]->getNumIndeces() );
        }
        printf ( "done\n" );
//         num_faces=i_buffer_size;

        mhs_check_gl;

        printf ( "buffer size %d\n",v_buffer_size );
        CTriangleObject::CVertex * tmp_buffer=new CTriangleObject::CVertex[v_buffer_size];
        CTriangleObject::CVertex * tmp_nbuffer=new CTriangleObject::CVertex[v_buffer_size];
        {
            for ( std::size_t i=0; i<v_buffer_size; i++ ) {
                tmp_buffer[i].x=-1000000;
                tmp_buffer[i].y=-1000000;
                tmp_buffer[i].z=-1000000;
            }
            CTriangleObject::CVertex * tmp_buffer_p= tmp_buffer;
            CTriangleObject::CVertex * tmp_nbuffer_p= tmp_nbuffer;
            for ( int i=0; i<num_particle_types; i++ ) {
                particles[i]->cpy2VertexBuffer ( tmp_buffer_p,tmp_nbuffer_p );
                tmp_buffer_p+=particles[i]->getNumVerts();
                tmp_nbuffer_p+=particles[i]->getNumVerts();
            }
            glGenBuffers ( 1,&vertex_buffer );
            glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
            glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_buffer, GL_STATIC_DRAW );

            glGenBuffers ( 1,&normal_buffer );
            glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
            glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_nbuffer, GL_STATIC_DRAW );

            for ( std::size_t i=0; i<v_buffer_size; i++ ) {
                if ( tmp_buffer[i].x==-1000000 ) {
                    printf ( "ERROR at x  %d\n",i );
                }
                if ( tmp_buffer[i].y==-1000000 ) {
                    printf ( "ERROR at y  %d\n",i );
                }
                if ( tmp_buffer[i].z==-1000000 ) {
                    printf ( "ERROR at z  %d\n",i );
                }
            }
        }




        sta_assert_error ( sizeof ( CTriangleObject::CVertex ) ==sizeof ( float ) *3 );

        printf ( "buffer size %d\n",i_buffer_size );
        {
            glGenBuffers ( 1,&index_buffer );
            mhs_check_gl;

            ushort * face_buffer=new ushort[i_buffer_size*3];
            for ( std::size_t i=0; i<i_buffer_size*3; i++ ) {
                face_buffer[i]=10000;
            }


            ushort *  tmp_buffer_p= face_buffer;
            std::size_t index_offset=0;
            for ( int i=0; i<num_particle_types; i++ ) {
                particles[i]->cpy2IndexBuffer ( tmp_buffer_p,index_offset );
                tmp_buffer_p+=particles[i]->getNumIndeces() *3;
                index_offset+=particles[i]->getNumVerts();
            }

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
            mhs_check_gl;
            glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 3*sizeof ( ushort ) *i_buffer_size, face_buffer, GL_STATIC_DRAW );
            mhs_check_gl;

            delete [] face_buffer;


            glEnable ( GL_CULL_FACE );
            glCullFace ( GL_BACK );
        }

        delete [] tmp_buffer;
        delete [] tmp_nbuffer;
        mhs_check_gl;

        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindBuffer ( GL_ARRAY_BUFFER, 0 );


        buffered_objects=new CMesh[num_particle_types];

        std::size_t offset_i=0;
        for ( int i=0; i<num_particle_types; i++ ) {
            buffered_objects[i].i_offset=offset_i;//*sizeof(GLushort);
            buffered_objects[i].i_length=particles[i]->getNumIndeces() *3;

            offset_i+=particles[i]->getNumIndeces() *3;
        }

        for ( int i=0; i<num_particle_types; i++ ) {
            printf ( "obj %d: offset %d length %d\n",i,buffered_objects[i].i_offset,buffered_objects[i].i_length );
        }

        mhs_check_gl;

    }

    ~CParticleMeshes() {
        for ( int i=0; i<num_particle_types; i++ ) {
            delete particles[i];
        }

        delete [] buffered_objects;

        mhs_check_gl;
        glDeleteBuffers ( 1, &index_buffer );
        mhs_check_gl;
        glDeleteBuffers ( 1, &vertex_buffer );
        mhs_check_gl;
        glDeleteBuffers ( 1, &normal_buffer );
        mhs_check_gl;

    }

    void enable_VBO() {
        glEnableClientState ( GL_VERTEX_ARRAY );
        glEnableClientState ( GL_NORMAL_ARRAY );

        glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
        glVertexPointer ( 3, GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );

        glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
        glNormalPointer ( GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );

        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
    }

    void disable_VBO() {
        glDisableClientState ( GL_NORMAL_ARRAY );
        glDisableClientState ( GL_VERTEX_ARRAY );

        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindBuffer ( GL_ARRAY_BUFFER, 0 );


    }


};


template<typename TData,typename T, int Dim>
class MyMouse: public Mouse<TData,T,Dim>
{
public:

    enum class RENDERER_MODE : unsigned int
    {
        RENDERER_MODE_DEFAULT=0,
        RENDERER_MODE_CONNECTED=2,
        RENDERER_MODE_LINE=4,
        RENDERER_MODE_WORKER=8,
        RENDERER_MODE_ONLINE=16,
	RENDERER_MODE_ACTIVITY=32,
	RENDERER_MODE_GSEARCH=64,
	
    };


    bool do_liveview;
    bool pause_program;


    unsigned int render_mode;
    
    //float dist_clip = 0;
//     unsigned int mode_activity_view;


    MyMouse ( CTrackerRenderer<TData,T,Dim> * viewer=NULL ) : Mouse<TData,T,Dim> ( viewer ) {

        do_liveview=true;
        pause_program=false;

        render_mode=static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_DEFAULT );
        //dist_clip = 0;
// 	mode_activity_view=0;

    }


    bool  ui() {


        if ( !Mouse<TData,T,Dim>::ui() ) {
            return false;
        }

        CTrackerRenderer<TData,T,Dim> & V=* ( ( CTrackerRenderer<TData,T,Dim> * ) ( Mouse<TData,T,Dim>::viewer ) );

        //processing keyboard input
        {
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_L]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_L] ) ) {
                do_liveview=!do_liveview;
                if ( do_liveview ) {
                    V.add_info ( "observer: on" );
                } else {
                    V.add_info ( "observer: off" );
                }
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_I]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_I] ) ) {
                Vector<float,3> realpoint=Mouse<TData,T,Dim>::focus_point_img;

                for ( int a=0; a<V.tracker_p.size(); a++ ) {
                    sta_assert_error ( V.tracker_p[a] );
                    CTracker<TData,T,Dim> & tracker=*V.tracker_p[a];
                    Vector<float,3> offset;
                    offset=tracker.get_world_offset();
                    Vector<float,3> realpoint_offset=realpoint-offset;
                    bool found=false;
                    if ( tracker.is_in_bb ( realpoint_offset.v ) ) {
                        class CTracker<TData,T,Dim>::User_action * action=new class CTracker<TData,T,Dim>::User_action_add_particle();
                        ( ( class CTracker<TData,T,Dim>::User_action_add_particle * ) action )->position=realpoint_offset;
                        tracker.execute ( action );
                        found=true;
                    }
                    if ( found ) {
                        realpoint.print();
                    } else {
                        printf ( "warning , point out of all tracker regions!\n" );
                    }

                }
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_P]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_P] ) ) {
                pause_program=!pause_program;
                if ( pause_program ) {
                    V.add_info ( "pausing" );
                } else {
                    V.add_info ( "continuing" );
                }
            }

            for ( int k=0; k<12; k++ ) {

                if ( Mouse<TData,T,Dim>::keys[Mouse<TData,T,Dim>::Fkeys[k]]&& ( !Mouse<TData,T,Dim>::keystates[Mouse<TData,T,Dim>::Fkeys[k]] ) ) {
                    unsigned int selection=k+12*Mouse<TData,T,Dim>::keys[SDL_SCANCODE_SPACE];
                    if ( selection<nproposal_names ) {
                        for ( int i=0; i<V.tracker_p.size(); i++ ) {
                            sta_assert_error ( V.tracker_p[i] );
                            CTracker<TData,T,Dim> & tracker=*V.tracker_p[i];
                            tracker. energy_gradient_stat2[1].clear();
                            tracker.selective_stat2[selection]=!tracker.selective_stat2[selection];
                            if ( i==0 ) {
                                if ( tracker.selective_stat2[selection] ) {
                                    printf ( "adding %s to stats\n",proposal_names[selection] );
                                } else {
                                    printf ( "removing %s to stats\n",proposal_names[selection] );
                                }
                            }
                        }
                    }
                }
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_1]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_1] ) ) {
                render_mode=static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_DEFAULT );
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_2]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_2] ) ) {
                render_mode=render_mode^static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_CONNECTED );
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_3]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_3] ) ) {
                render_mode=render_mode^static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_LINE );
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_4]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_4] ) ) {
                render_mode=render_mode^static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_WORKER );
            }

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_5]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_5] ) ) {
                render_mode=render_mode^static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_ONLINE );
            }
            
            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_6]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_6] ) ) {
                render_mode=render_mode^static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_ACTIVITY );
            }
            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_7]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_7] ) ) {
                 GuiPoints<T>::activity_mode++;
 		 //GuiPoints<T>::activity_mode%=6;
		 GuiPoints<T>::activity_mode%=9;
            }
            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_8]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_8] ) ) {
                render_mode=render_mode^static_cast<unsigned int> ( RENDERER_MODE::RENDERER_MODE_GSEARCH );
            }
            

            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_S]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_S] ) ) {
                V.debug_point.clear();
            }
            
            
            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_Z]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_Z] ) ) {
                V.debug_patch_mode++;
// 		V.debug_patch_mode%=6;
		V.debug_patch_mode%=6;
		
		printf("debug patch mode: %d\n",V.debug_patch_mode);
            }
            
            

            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_D]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_D] ) ) {
                for ( int i=0; i<V.tracker_p.size(); i++ ) {
                    sta_assert_error ( V.tracker_p[i]!=NULL );
                    CTracker<TData,T,Dim> & tracker=*V.tracker_p[i];
                    class CTracker<TData,T,Dim>::User_action * action=new class CTracker<TData,T,Dim>::User_action_clear();
                    tracker.execute ( action );

                }
            }
            
            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_O]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_O] ) ) {
                if ( Mouse<TData,T,Dim>::call_from_main==0 ) {
                    V.add_info ( "showing matlab GUI" );
                    Mouse<TData,T,Dim>::call_from_main=1;
                }
            }


            if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_K]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_K] ) ) {
                if ( Mouse<TData,T,Dim>::call_from_main==0 ) {
                    V.add_info ( "writing debug data 2 matlab" );
                    Mouse<TData,T,Dim>::call_from_main=10;
                }
            }
            
            
		    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_W]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_W] ) )
                    {
		      if (V.cam->cam_is_flying())
			V.cam->stop_flight(); 
			else
			  V.cam->start_flight(); 
                    }
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_E]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_E] ) )
                    {
		      if (V.cam->cam_is_flying())
			V.cam->stop_flight(); 
			else
			  V.cam->start_flight(false); 
                    }
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_Y]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_Y] ) )
                    {
		      if (V.movie_folder.length()==0)
		      {
			printf("no movie folder set!\n");
		      }else 
		      {
			if (V.get_movie_state()!=CTrackerRenderer<TData,T,Dim>::MOVIE_SATE::MOVIE_NOT_RECORDING)
			{
			    V.cam->stop_flight();
			    V.set_movie_state(CTrackerRenderer<TData,T,Dim>::MOVIE_SATE::MOVIE_NOT_RECORDING);
			    printf("stop recording!\n");
			}else
			{
			  printf("start recording!\n");
			  V.cam->stop_flight();
			  V.cam->start_flight(false); 
  // 			V.recording_flight=true;
			  V.frame_flight=0;
			  pause_program=false;
			  V.set_movie_state(CTrackerRenderer<TData,T,Dim>::MOVIE_SATE::MOVIE_RECORDING);
			}
		      }
                    }    
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_U]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_U] ) )
                    {
		      if (V.movie_folder.length()==0)
		      {
			printf("no movie folder set!\n");
		      }else
			V.recording_user=!V.recording_user;
			if (V.recording_user)
			{
			  printf("recording user interaction\n");
			}
			else 
			  printf("stopped recording user interaction\n");
                    }    
                    
                    
//                     if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_Y]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_Y] ) )
//                     {
// 		      if (V.movie_folder.length()==0)
// 		      {
// 			printf("no movie folder set!\n");
// 		      }else 
// 		      {
// 			V.cam->stop_flight();
// 			V.cam->start_flight(false); 
// 			V.recording_flight=true;
// 		      }
//                     }    
//                     
//                     if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_U]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_U] ) )
//                     {
// 		      if (V.movie_folder.length()==0)
// 		      {
// 			printf("no movie folder set!\n");
// 		      }else
// 			V.recording_user=!V.recording_user;
// 			if (V.recording_user)
// 			  printf("recording user interaction\n");
// 			else 
// 			  printf("stopped recording user interaction\n");
//                     }    
//                     
            
        }


        /// mouse interaction
        if ( ( !Mouse<TData,T,Dim>::transition_on ) ) {
            for ( int b=0; b<3; b++ ) {
                if ( Mouse<TData,T,Dim>::b_down[b] ) {
                    if ( !Mouse<TData,T,Dim>::b_down_old[b] ) { // mouse_down_event
                        if ( !Mouse<TData,T,Dim>::is_busy() ) {
                            switch ( b ) {
                            case 0: {
                                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_LCTRL] ) {
                                    V.add_info ( "particle cursor: on" );
                                    Mouse<TData,T,Dim>::point_select_mode=0;
                                    Mouse<TData,T,Dim>::point_select_on=true;
                                    Mouse<TData,T,Dim>::mouse_pos_old[0]=-1;
                                    Mouse<TData,T,Dim>::mouse_pos_old[1]=-1;
                                }

                            }
                            break;
                            }
                        }
                    } else { //  mouse_is_down
                        if ( ( Mouse<TData,T,Dim>::point_select_on ) && ( !Mouse<TData,T,Dim>::arcball_on ) && ( !Mouse<TData,T,Dim>::trackball_on ) && ( !Mouse<TData,T,Dim>::trackballZ_on ) ) {

                            if ( Mouse<TData,T,Dim>::point_select_mode==0 ) {

                                if ( ( Mouse<TData,T,Dim>::mouse_pos_old[0]!=Mouse<TData,T,Dim>::mouse_pos[0] ) && ( Mouse<TData,T,Dim>::mouse_pos_old[1]!=Mouse<TData,T,Dim>::mouse_pos[1] ) ) {

                                    V.particle_hittest ( Mouse<TData,T,Dim>::mouse_pos[0],Mouse<TData,T,Dim>::mouse_pos[1],Mouse<TData,T,Dim>::focus_point,Mouse<TData,T,Dim>::focus_point_img );
                                    Mouse<TData,T,Dim>::mouse_pos_old[0]=Mouse<TData,T,Dim>::mouse_pos[0];
                                    Mouse<TData,T,Dim>::mouse_pos_old[1]=Mouse<TData,T,Dim>::mouse_pos[1];
                                }
                            }
                        }
                    }
                } else if ( Mouse<TData,T,Dim>::b_down_old[b] ) { // mouse_up_event
                    if ( b==0 ) {
                        if ( ( Mouse<TData,T,Dim>::point_select_on ) ) {
                            if ( Mouse<TData,T,Dim>::point_select_mode==0 ) {
                                V.add_info ( "particle cursor: off" );
                            }

                        }
                    }

                }
            }
        }

        return true;
    }
};
















template<typename TData,typename T, int Dim>
class CTrackerRenderer: public CViewer<TData,T,Dim>, public CParticleMeshes
{

    friend   MyMouse<TData,T, Dim>;
//     friend   CTracker<TData,T, Dim>;
    
public:
  
   enum  MOVIE_SATE : std::size_t {
      MOVIE_NOT_RECORDING=0,
      MOVIE_RECORDING=1,
      MOVIE_RECORDING_WAITING_FOR_CAPTURE=2
    };
    
private:

    float activity_delay;
  
    const TData * img_smooth;
    int num_alphas_img_smooth;
    const TData * scales;
    std::size_t num_scales;    
     bool img_smooth_is_pol;
    
    const TData * hessian;
    int num_alphas_hessian;
    
    int debug_patch_mode;

    std::vector<CTracker<TData,T,Dim>* >  tracker_p;


    GLuint sliceview_textureID;
    static const int sliceview_extents=64;
    float sliceview_slice[sliceview_extents*sliceview_extents];
    MOVIE_SATE movie_state;

    class CVertex
    {
    public:
        Vector<float,3> pos;
        Vector<float,3> color;
    };

    class C_debug_pt
    {
    public:
        Points<T,Dim>  * debug_point;
        int debug_life;
        int debug_point_tracker_id;

        void clear() {
            if ( debug_point!=NULL ) {
                debug_point->clear_selection();
            }
            debug_point=NULL;
        }

        void assign ( Points<T,Dim>  * p, int tracker_id ) {
            clear();
            debug_point =p;
            p->is_selected=true;
            debug_point_tracker_id=tracker_id;
            debug_life=p->get_life();
            sta_assert_error ( tracker_id>-1 );
// 	  sta_assert_error(tracker_id<tracker_p.size());
        }

        C_debug_pt() {
            debug_point=NULL;
            debug_life=-1;
            debug_point_tracker_id=-1;
        }

    };

    C_debug_pt debug_point;
    Vector<T,3> debug_poit_world_offset;

    mhs::CtimeStopper timer;
    std::size_t sync_tick;
    double last_tick;
    
    CCam<TData,T,Dim> * cam;
    std::string movie_folder;
    std::size_t frame_flight;
    std::size_t frame_user;
    
    double scalethreshold = -1;
    double colorcooldown = 1;
//     bool recording_flight;
    bool recording_user;
    double prev_user_recording_time;

    float constraint_loop_depth;

public:
  
    MOVIE_SATE get_movie_state(){return movie_state;}; 
    void set_movie_state(MOVIE_SATE  state){movie_state=state;}; 
    
  
    void pause() {
      ( ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state )->pause_program=true;
    };


    void save_state_in_struct ( mxArray * tracker_state ) {
        CViewer<TData,T,Dim>:: save_state_in_struct ( tracker_state );
        if ( tracker_state!=NULL ) {
            try {
                float * data_p=mhs::mex_getFieldPtrCreate<float> ( tracker_state,1,"observer_rendermode",0 );
                *data_p= ( ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state )->render_mode;

                data_p=mhs::mex_getFieldPtrCreate<float> ( tracker_state,1,"observer_show_tracking",0 );
                *data_p= ( ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state )->do_liveview;

            } catch ( mhs::STAError error ) {
                printf ( "could'nt save oberser state :%s\n",error.str().c_str() );
            }
        }
    }


    void load_state_from_struct ( const mxArray * tracker_state ) {
        CViewer<TData,T,Dim>:: load_state_from_struct ( tracker_state );
        if ( tracker_state!=NULL ) {
            try {
                float * data_p =mhs::mex_getFieldPtr<float> ( tracker_state,"observer_rendermode" );
                ( ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state )->render_mode=*data_p;

                data_p=mhs::mex_getFieldPtr<float> ( tracker_state,"observer_show_tracking" );
                ( ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state )->do_liveview=*data_p;

            } catch ( mhs::STAError error ) {
                printf ( "could'nt find oberser state :%s\n",error.str().c_str() );
            }
        }
    }






    void do_render() {
      
        CViewer<TData,T,Dim>::do_render();

        MyMouse<TData,T,Dim> * mouse_state= ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state;

        bool data_kernel_Debug=false;
        if ( mouse_state->call_from_main>0 ) {
            switch ( mouse_state->call_from_main ) {
            case 1: {
                mexCallMATLAB ( 0, NULL,0,NULL, "start_gui" );
                update_tracker_gui();
            }
            break;
// 	    case 2:
// 	    {
// 		  mexCallMATLAB(0, NULL,0,NULL, "start_gui2");
// 	    }
	    break;
            case 10: {
                data_kernel_Debug=true;
            }
            break;

            }
            mouse_state->call_from_main=0;
        }

        if ( SDL_GL_MakeCurrent ( CSceneRenderer::tracker_data_p->tracker_window,CSceneRenderer::tracker_data_p->glcontext ) !=0 ) {
            printf ( "error setting opengl context to current window\n" );
            return;
        }

	bool cam_is_progressing=false;
	
	if ((movie_state==MOVIE_SATE::MOVIE_NOT_RECORDING)||(movie_state==MOVIE_SATE::MOVIE_RECORDING_WAITING_FOR_CAPTURE))
	{
	  cam_is_progressing=cam->fly();
 	  //printf("flying %d\n",cam_is_progressing);
	}
	
	
	
        CViewer<TData,T,Dim>::render_init_default(!cam->cam_is_flying());        
        
//         CViewer<TData,T,Dim>::render_init_default();
	
	
	glDisable(GL_MULTISAMPLE);
// 	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint(GL_POLYGON_SMOOTH_HINT, GL_FASTEST );
 
// 	glEnable(GL_LINE_SMOOTH);
	glDisable(GL_POLYGON_SMOOTH);

	
	 Matrix<float,4>  trafo=CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform;
	 
	  Vector<float,3> center=this->new_center*T ( -1 );
	  Matrix<float,4>  modelview_mat=trafo*Matrix<float,4>::OpenGL_glTranslate(center);
// 	  Matrix<float,4>  normal_mat=trafo.transpose().OpenGL_invert();


        double total_time=timer.get_total_runtime();
        if ( total_time-last_tick>0.001 ) {
            last_tick=total_time;
            sync_tick++;
        }
        
        double global_time=global_timer.get_total_runtime();

        CViewer<TData,T,Dim>::enable_model_shader();
	
#ifdef  _USE_CGC_    	
	cgGLSetParameter3fv ( this->_focuspointV,mouse_state->focus_point.v );
        cgGLSetParameter2fv ( this->_vertex_mode,this->shader_options.v );
        cgGLSetParameter1f ( this->_clipping_plane,this->clip_plane[0] );
	     cgGLSetStateMatrixParameter ( this->_projection,  CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY );

	
	 
#else
	    glUniform3fv(this->glsl_focuspoint, 1, ( GLfloat * )( mouse_state->focus_point.v));
	    glUniform2fv(this->glsl_vertex_mode, 1, ( GLfloat * )( this->shader_options.v));
	    glUniform1f(this->glsl_clipping_plane, CViewer<TData,T,Dim>::clip_plane_dist);
	    glUniformMatrix4fv(this->glsl_projection,1,GL_FALSE, ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v);
	    
	    glUniform1i(this->glsl_render_mode, this->render_mode);
        
        
	    
	    mhs_check_gl
	
#endif

	  
        float & rescale=CViewer<TData,T,Dim>::rescale;
        Vector<float,3> & font_color=CViewer<TData,T,Dim>::font_color;



        this->enable_VBO();
        mhs_check_gl;


//         Vector<T,Dim> rot_ax;
//         rot_ax=T ( 0 );
	Vector<float,Dim> rot_ax;
        rot_ax=float ( 0 );


        bool no_single= ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_CONNECTED ) );

// 	{
// 	  std::size_t maxpart=-1;
// 	  int maxpart_indx=-1;
// 	  for ( int i=0; i<tracker_p.size(); i++ ) {
// 	    CTracker<TData,T,Dim> & tracker=*tracker_p[i];
// 	    std::size_t n_points=tracker.particle_pool->get_numpts();
// 	    tracker.is_movie_maker=false;
// 	    if (n_points>maxpart)
// 	    {
// 	      maxpart=n_points;
// 	      maxpart_indx=i;
// 	    }
// 	  }
// 	  if (maxpart_indx>0)
// 	  {
// 	    tracker_p[maxpart_indx]->is_movie_maker=true;
// 	  }
// 	}
	//NOTE now fixed. dynamic better of more than one trac\ker
	tracker_p[0]->is_movie_maker=true;
	
        for ( int i=0; i<tracker_p.size(); i++ ) {
            sta_assert_error ( tracker_p[i]!=NULL );
            CTracker<TData,T,Dim> & tracker=*tracker_p[i];
	    int tracker_thread_id=tracker.current_thread_id;
	    sta_assert_error(tracker_thread_id>-1);
	    sta_assert_error(tracker_thread_id<max_tracking_treads);
	    
	    //TODO  i-> tracker_id 
	    int tracker_id=i;

            if ( ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ONLINE ) ) && ( tracker.is_offline() ) ) {
                continue;
            }



            Vector<float,3> worker_color;
            Vector<float,3> worker_color_select;
            Vector<float,3> worker_color_bifurcation;
            bool is_offline=tracker.is_offline();
            float offline_fact_h= ( 1-is_offline );
            float offline_fact_v=0.5*is_offline+ ( 1-is_offline );
	    //if ENDERER_MODE_ACTIVITY
	    
	    
	    
            hsv2rgb<float> ( float ( i*360.0f ) /tracker_p.size(),0.75f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2] );
            hsv2rgb<float> ( int ( float ( ( ( i*360 ) ) /tracker_p.size() +10 ) ) %360,1.00f*offline_fact_h,0.5f*offline_fact_v,worker_color_bifurcation[0],worker_color_bifurcation[1],worker_color_bifurcation[2] );
            hsv2rgb<float> ( int ( float ( ( ( i*360 ) ) /tracker_p.size() +180 ) ) %360,0.50f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2] );

	    
#ifndef  _USE_CGC_    		    
// 	Matrix<float,3> model_trafo_translate;
	Matrix<float,4> model_trafo_rotate;   
	bool no_rotation;
// 	Matrix<float,3> model_trafo; 
// 	Matrix<float,3> model_trafo_scale;   
// 	Matrix<float,3> normal_trafo;
#endif
            Vector<T,3> world_offset=tracker.get_world_offset();

            const class Points<T,Dim> ** particle_ptr=tracker.particle_pool->getMem();
            std::size_t n_points=tracker.particle_pool->get_numpts();

            // 	if (false)
            if ( mouse_state->do_liveview && true)
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> * point_p= ( ( Points<T,Dim>* ) particle_ptr[i] );
                    if ( point_p==NULL ) {
                        continue;
                    }
                    

                    if ( !tracker.particle_pool->is_valid_adress ( point_p ) ) {
                        SDL_Log ( "\ninvalid addr\n" );
                        continue;
                    }

                    Points<T,Dim>  point= ( *point_p );

                    if ( ( no_single ) && ( !point.isconnected() ) ) {
                        continue;
                    }
                    
//                        if (i<3)
//                         {
//                             printf("vis %u\n",point_p);
//                         }
//                     if (i<3)
//                     {
//                         printf("vis %u\n",&point);
//                     }


                    float tx= ( point.get_position()[0]+world_offset[0] ) *rescale;
                    float ty= ( point.get_position()[1]+world_offset[1] ) *rescale;
                    float tz= ( point.get_position()[2]+world_offset[2] ) *rescale;
                    if ( ( tx<0 ) || ( tx>1 ) ) {
                        continue;
                    }
                    if ( ( ty<0 ) || ( ty>1 ) ) {
                        continue;
                    }
                    if ( ( tz<0 ) || ( tz>1 ) ) {
                        continue;
                    }


                    float p_scale=point.get_scale();
                    float scale=p_scale*rescale;
                    float thickness=point.get_thickness(tracker_thread_id) *rescale;
                    
                    
                    //printf("%f\n",p_scale);
                    if (!point.isconnected() && (p_scale<scalethreshold))
                    {
                        continue;
                    }
		    
// 		    if (i==0)
// 		    printf("%f %f %f\n",scale,thickness,Points<T,Dim>::dynamic_thicknes_fact);
		    
                    if ( ! ( scale>0 ) ||! ( scale<50 ) ) {
                        continue;
                    }
                    if ( ! ( thickness>0 ) ||! ( thickness<50 ) ) {
                        continue;
                    }

                    
//                     if (scale>50)
// 		      printf("uhhhhh\n");


#ifdef  _USE_CGC_    		   
                    glPushMatrix();
                    glTranslatef ( tx,ty,tz );
#endif		    
                    int particle_typ=0;
                    int color_id=0;
                    switch ( point.tracker_birth ) {
                    case 0:
                        color_id=CSceneRenderer::Cred;
                        break;
                    case 1:
                        color_id=CSceneRenderer::Cgreen;
                        break;
                    case 2:
                        color_id=CSceneRenderer::Cblue;
                        break;
                    case 3:
                        color_id=CSceneRenderer::Cyellow;
                        break;
		    case 4:
                        color_id=CSceneRenderer::Ctur;
                        break;
                    }

                    no_rotation=false;
                    switch ( point.particle_type ) {
                    case PARTICLE_TYPES::PARTICLE_SEGMENT: {
                        const Vector<T,Dim> & n=point.get_direction();
                        if ( ! ( n.norm1() >0 ) ) {
#ifdef  _USE_CGC_    		   			  
                            glPopMatrix();
#endif			    
                            continue;
                        }
                        sta_assert_error ( n.norm2() >0 );
                        rot_ax[0]=n[1];
                        rot_ax[1]=-n[0];
                        rot_ax.normalize();
                        T angle=-180*std::acos ( n[2] ) /M_PI;
			
			
			
#ifdef  _USE_CGC_    		   			
			glRotatef ( angle,rot_ax[0],rot_ax[1],rot_ax[2] );
#else
// 			model_trafo_translate=Matrix<4,float>::OpenGL_glTranslate(tx,ty,tz);
			model_trafo_rotate=Matrix<float,4>::OpenGL_glRotate(angle,rot_ax);    
#endif
                        particle_typ=1;
			
			//if (std::abs(Points<T,Dim>::thickness)>p_scale)
			//NOTE switch between flat and round
// 			if ((std::abs(Points<T,Dim>::thickness)>p_scale)||(!(Points<T,Dim>::thickness>-1))||(Points<T,Dim>::dynamic_thicknes_fact>0))
// 			{
// 			  particle_typ=0;
// 			}
			if ((scale/thickness)<3)
			{
			  particle_typ=0;
			}

                    }
                    break;
                    case PARTICLE_TYPES::PARTICLE_BIFURCATION: {
                        const Vector<T,Dim> & n=point.get_direction();
                        if ( ! ( n.norm1() >0 ) ) {
#ifdef  _USE_CGC_    		   						  
                            glPopMatrix();
#endif			    
                            continue;
                        }
                        sta_assert_error ( n.norm2() >0 );
                        rot_ax[0]=n[1];
                        rot_ax[1]=-n[0];
                        rot_ax.normalize();
                        T angle=-180*std::acos ( n[2] ) /M_PI;
#ifdef  _USE_CGC_    		   						
                        glRotatef ( angle,rot_ax[0],rot_ax[1],rot_ax[2] );
#else
// 			model_trafo_translate=Matrix<4,float>::OpenGL_glTranslate(tx,ty,tz);
			/*model_trafo_rotate=Matrix<4,float>::OpenGL_glRotate(angle,rot_ax[0],rot_ax[1],rot_ax[2]);*/    
			model_trafo_rotate=Matrix<float,4>::OpenGL_glRotate(angle,rot_ax);       
#endif			
                        particle_typ=1;
			
			if ((scale/thickness)<3)
			{
			  particle_typ=0;
			}

                    }
                    break;
                    case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER: {
                        color_id=CSceneRenderer::Cmag;
			no_rotation=true;
                    }
                    break;
		    case PARTICLE_TYPES::PARTICLE_BLOB: {
                        color_id=CSceneRenderer::Cred;
			no_rotation=true;
                    }
                    break;
                    }

                    if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_LINE ) ) {
                        if (( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER )&&( point.particle_type!=PARTICLE_TYPES::PARTICLE_BLOB )) {
			  
			   int nc=point.get_num_connections();
			    switch (nc)
			    {
			      case 0:
			      color_id=CSceneRenderer::Clightgray;
			      break;
			      case 1:
			      color_id=CSceneRenderer::Clightyello;
			      break;
			      default:
			      color_id=CSceneRenderer::Cyellow;
			    }

                            scale*=0.4;
                            thickness=scale;
			    no_rotation=true;

                            //thickness*=1.25;
                            //scale=thickness;

                            particle_typ=0;
                        }
                    }

                    if ( point_p->is_selected ) {
                        if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_WORKER ) ) {
                            glColor3fv ( worker_color_select.v );
                        } else {
                            color_id=CSceneRenderer::Corange;
                            glColor3fv ( this->particle_colors[color_id] );
                        }
                        
			    
			    
// 		  glColor3fv(blinking_color.v);
                    } else {
		      if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ACTIVITY) ) {
			float diff=(global_time-point.last_accepted)* colorcooldown;
			
			
	      
            
			
// 			switch (mouse_state->mode_activity_view)
// 			{
// 			  case 1:
// 			  {
// 			      point
// 			    
// 			  }break;
// 			}
			
			//float activity=std::max(1.0-diff/5.0,0.0);
			//float activity2=std::max(1.0-diff/2.0,0.0);
			
			float activity=std::max(1.0-diff/(activity_delay*5.0),0.0);
			float activity2=std::max(1.0-diff/(activity_delay*2.0),0.0);
			int color_temp=(260+int(160*activity2))%360;
			
// 			hsv2rgb<float> ( float ( tracker_id*360.0f ) /tracker_p.size(),0.75f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
			hsv2rgb<float> ( float ( color_temp) ,0.75f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
			//hsv2rgb<float> (0.0f,0.75f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
			glColor3fv ( worker_color.v );
		      }
		      else
		      {
			  if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_WORKER ) ) {
			      if ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {
				  glColor3fv ( worker_color.v );
			      } else {
				  glColor3fv ( worker_color_bifurcation.v );
			      }
			  } else {
			      if ( is_offline ) {
				  color_id=CSceneRenderer::Cdarkgray;
			      }
			      glColor3fv ( this->particle_colors[color_id] );
			  }
		      }
                    }
                    
                    
                    if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_GSEARCH) ) {
// 		      if (track_unique_id[tracker_id]<10+con->track_me_id)
// 		      {
// 			float color=float(track_unique_id[tracker_id]-con->track_me_id)/10.0f;
// 			glColor3f (color,color,color  );
// 		      }
		      //float color=std::min(1.0f-point.depth/20.0f,1.0f);
		      float color=std::min(point.depth/constraint_loop_depth,1.0f);
		      
		      if (point.isconnected())
		      {
			glColor3f (color,1.0f-color,0.5f);
		      }else
		      {
			glColor3f (0.7f,0.7f,0.7f);
		      }
		    }



#ifdef  _USE_CGC_  
                    glScalef ( scale,scale,thickness );

                    // 	      cgGLSetStateMatrixParameter( _worldview,  CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
                    // 		cgGLSetStateMatrixParameter(  _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_TRANSPOSE);

                    cgGLSetStateMatrixParameter ( this->_modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY );
                    cgGLSetStateMatrixParameter ( this->_normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE );
#else
if (no_rotation)
{
		      glUniformMatrix4fv(this->glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(Vector<float,3>(tx,ty,tz))*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,thickness))).v));
// 		      glUniformMatrix4fv(this->glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (trafo).transpose().OpenGL_invert().v));
		      glUniformMatrix4fv(this->glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (trafo).v));
}else
{
		      glUniformMatrix4fv(this->glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(Vector<float,3>(tx,ty,tz))*model_trafo_rotate*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,thickness))).v));
// 		      glUniformMatrix4fv(this->glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (trafo*model_trafo_rotate).transpose().OpenGL_invert().v));
		      glUniformMatrix4fv(this->glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (trafo*model_trafo_rotate).v));
}
		    
#endif		    
		    

                    std::size_t offset=this->buffered_objects[particle_typ].i_offset;
                    glDrawElements ( GL_TRIANGLES, this->buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT, ( void* ) ( offset * sizeof ( ushort ) ) );
#ifdef  _USE_CGC_		    
                    glPopMatrix();
#endif		    
                    mhs_check_gl;
                }


            //






            std::size_t numconnections=tracker.connections.pool->get_numpts();

            int particle_typ=2;
            int color_id=CSceneRenderer::Cyellow;
            Vector<float,Dim> positionn;
            Vector<T,Dim> n;

            if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_WORKER ) ) {
                hsv2rgb<float> ( float ( i*360.0f ) /tracker_p.size(),0.25f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2] );
                glColor3fv ( worker_color.v );

            } else {
                if ( is_offline ) {
                    color_id=CSceneRenderer::Cdarkgray;
                }
                glColor3fv ( this->particle_colors[color_id] );
            }


            const Connection<T,Dim> ** conptr=tracker.connections.pool->getMem();
            if ( mouse_state->do_liveview  && true)
                for ( std::size_t i=0; i<numconnections; i++ ) {
                    const Connection<T,Dim> * con=conptr[i];

                    if ( con==NULL ) {
                        continue;
                    }
                    if ( !tracker.connections.pool->is_valid_adress ( con ) ) {
                        SDL_Log ( "\ninvalid addr\n" );
                        continue;
                    }
                    class Points< T, Dim >::CEndpoint * epointA=con->pointA;
                    class Points< T, Dim >::CEndpoint * epointB=con->pointB;
                    if ( ( epointA==NULL ) || ( epointB==NULL ) ) {
                        continue;
                    }

                    Points<T,Dim> * pointA=epointA->point;
                    Points<T,Dim> * pointB=epointB->point;

                    if ( ( pointA==NULL ) || ( pointB==NULL ) ) {
                        continue;
                    }


                    Vector<T,Dim>  pA=pointA->get_position()+world_offset;
                    Vector<T,Dim>  pB=pointB->get_position()+world_offset;



                    n=pA-pB;
                    T length=std::sqrt ( n.norm2() );

                    if ( length<0.0001 ) {
                        continue;
                    }

                    n/=length+std::numeric_limits<T>::epsilon();

                    length*=0.5*rescale;

                    // probably conected with a "not initialized point"
                    if ( length>0.25 ) {
                        continue;
                    }
                    
                    
                    
		    
		    
                    float s=std::min ( pointA->get_scale(),pointB->get_scale() );
                    if ( ( s<0.0001 ) || ( s>100 ) ) {
                        continue;
                    }
                    //s=0.55*std::max(1.0f,s);
                    if ( mouse_state->render_mode!=static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_LINE ) ) {
                        s*=0.3f;
                        s=std::max ( std::min ( 0.5f,s ),s );
// 		  s*=0.3;
                    } else {
                        s*=0.3;
                    }

                     positionn= ( pA+pB ) /2;
#ifdef  _USE_CGC_  
                    glPushMatrix();
                    glTranslatef ( positionn[0]*rescale,positionn[1]*rescale,positionn[2]*rescale );
#endif		    
		    

                    rot_ax[0]=n[1];
                    rot_ax[1]=-n[0];

                    sta_assert_error ( n.norm2() >0 );
                    if ( ! ( n.norm1() >0 ) ) {
#ifdef  _USE_CGC_  		      
                        glPopMatrix();
#endif			
                        continue;
                    }
                    rot_ax.normalize();

                    T angle=-180*std::acos ( n[2] ) /M_PI;
		    
#ifdef  _USE_CGC_    		   						
                        glRotatef ( angle,rot_ax[0],rot_ax[1],rot_ax[2] );
#else
			model_trafo_rotate=Matrix<float,4>::OpenGL_glRotate(angle,rot_ax);    
#endif				    
//                     glRotatef ( angle,rot_ax[0],rot_ax[1],rot_ax[2] );
                    particle_typ=2;

                    if ( length<0.00001 ) {
                        //printf("ha? skipping edges: lenght %f\n",length);
#ifdef  _USE_CGC_		      
                        glPopMatrix();
#endif			
                        continue;
                    }
                    if ( ! ( length>0 ) ) {
                        printf ( "ha? skipping edges: neg (%d) or zero (%d) lenght: %f\n",length<0,! ( length<0 ),length );
#ifdef  _USE_CGC_		      			
                        glPopMatrix();
#endif			
                        continue;
                    }
                    
		    if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ACTIVITY) ) {
// 		      float activity=std::max(1.0-(global_time-std::max(pointA->last_accepted,pointB->last_accepted))/5.0,0.0);
		      
			float diff=(global_time-std::max(pointA->last_accepted,pointB->last_accepted)) * colorcooldown;
			float activity=std::max(1.0-diff/5.0,0.0);
			float activity2=std::max(1.0-diff/2.0,0.0);
			int color_temp=(260+int(160*activity2))%360;
// 			hsv2rgb<float> ( float ( tracker_id*360.0f ) /tracker_p.size(),0.75f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
			hsv2rgb<float> ( float ( color_temp) ,0.25f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
		      
		      //hsv2rgb<float> ( float ( tracker_id*360.0f ) /tracker_p.size(),0.25f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
		      //hsv2rgb<float> (0.0f,0.25f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
		      //hsv2rgb<float> ( float ( i*360.0f ) /tracker_p.size(),0.75f*activity,1.0f*activity,worker_color[0],worker_color[1],worker_color[2] );
		       glColor3fv ( worker_color.v );
		    }
		    
		    if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_GSEARCH) ) {
		      float color1=std::min(pointA->depth/constraint_loop_depth,1.0f);
		      float color2=std::min(pointB->depth/constraint_loop_depth,1.0f);
		      float color=(color1+color2)/2;
		      
		      glColor3f (color,1.0f-color,0.75f);
		      
		    }
		    

                    
                    
#ifdef  _USE_CGC_		      			                    
                    glScalef ( s*rescale,s*rescale,length );
                    cgGLSetStateMatrixParameter ( this->_modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY );
                    cgGLSetStateMatrixParameter ( this->_normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE );
#else
		      glUniformMatrix4fv(this->glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(positionn*rescale)*model_trafo_rotate*Matrix<float,4>::OpenGL_glScale(Vector<float,3>( s*rescale,s*rescale,length ))).v));
// 		      glUniformMatrix4fv(this->glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (trafo*model_trafo_rotate).transpose().OpenGL_invert().v));
		      glUniformMatrix4fv(this->glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (trafo*model_trafo_rotate).v));
#endif		    
		    
                    std::size_t offset=this->buffered_objects[particle_typ].i_offset;
                    glDrawElements ( GL_TRIANGLES, this->buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT, ( void* ) ( offset * sizeof ( ushort ) ) );
#ifdef  _USE_CGC_		      			                    		    
                    glPopMatrix();
#endif		    		    
                }
        }




        std::list<std::string >  messages;





        CViewer<TData,T,Dim>::disable_model_shader();




         CViewer<TData,T,Dim>::render_outline();
//         CViewer<TData,T,Dim>::render_focuspoint();

if (!cam->cam_is_flying())
{
        render_outline();
        render_focuspoint();
}

        this->disable_VBO();
        mhs_check_gl;


        glDisable ( GL_BLEND );


        CViewer<TData,T,Dim>::render_volume();


        mhs_check_gl



        CViewer<TData,T,Dim>::text_HUD->InitHUD();




        int top_row=0;
        glPushMatrix();
        glLoadIdentity();

if (!cam->cam_is_flying())
{	


        //glColor3f(0,0,0);

// 			text_HUD->Stringoutxyz(s.str(),Vector<T,3>(-0.95,0.95,0));
// 			text_HUD->Stringoutxyz(s.str(),Vector<T,3>(0.05,0.95,0));

        Vector<float,3> worker_color;

        glLineWidth ( 1 );

	

	
	
        if ( true ) {
            for ( int i=0; i<tracker_p.size(); i++ ) {
                sta_assert_error ( tracker_p[i] );
                CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                //if (mouse_state->render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER))


                {
                    bool is_offline=tracker.is_offline();
                    float offline_fact_h= ( 1-is_offline );
                    float offline_fact_v=0.5*is_offline+ ( 1-is_offline );
                    hsv2rgb<float> ( float ( i*360.0f ) /tracker_p.size(),0.75f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2] );
                    glColor3fv ( worker_color.v );
                }
// 				float t=0.8;
// 				float offset=0.01;
// 				float t=0.0;
// 				float h=0.1;
// 				float l=0.7-offset;
// 				float w=0.3;



                float offset=CViewer<TData,T,Dim>::text_HUD->p2gl_h ( 5 );
                float t=0;
                float h=CViewer<TData,T,Dim>::text_HUD->p2gl_w ( 80 );
                float w=CViewer<TData,T,Dim>::text_HUD->p2gl_w ( 300 );
                float l=1-w-offset;



                float level=0;//tracker.state_energy_grad_mean;
// 				tracker.energy_gradient_stat->print_hist(t, l,w, h,0.001);
                tracker.energy_gradient_stat2[0].print_hist ( t+offset, l,w, h,0 );
                glColor3fv ( worker_color.v );
                tracker.energy_gradient_stat2[2].print_hist ( t+h+offset*2, l,w, h,0 );

                if ( tracker_p.size() ==1 ) {
                    glColor3f ( 1,1,0 );
                    //tracker.energy_gradient_stat2[1].print_hist(t+h+offset*2, l,w, h,0);
                    tracker.energy_gradient_stat2[1].print_hist ( t+offset, l,w, h,0 );
                    glColor3f ( 0,1,1 );
                    tracker.edge2particle_stat.print_hist ( t+h+offset*2, l,w, h,1 );
                    //tracker.edge2particle_stat.print_hist(t+h, l,w, h,0);
                }

                /*
                for (int proposal_id=0;proposal_id<tracker.proposals.size();proposal_id++)
                {

                  if (tracker.state_energy_grad_proposal_type[proposal_id]==static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_BIRTH))
                  {
                	glColor3f(1,1,0);
                	tracker.energy_gradient_stat_per_proposal[proposal_id].print_hist(t, l,w, h,0);
                  }
                  if (tracker.state_energy_grad_proposal_type[proposal_id]==static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_DEATH))
                  {
                	glColor3f(1,0,1);
                	tracker.energy_gradient_stat_per_proposal[proposal_id].print_hist(t, l,w, h,0);
                  }

                }
                */

//
            }

            // 	 glBegin(GL_LINES);
            //
            // 		glColor3f(1,1,1);
            // 		glVertex3f(0,0,0);
            // 		glVertex3f(1,1,0);
            // 	  glEnd();

        }








        std::stringstream s;
        s.precision ( 2 );


        std::size_t total_n_bif=0;
        std::size_t total_n_points=0;
        std::size_t total_n_connections=0;
	std::size_t total_n_segments=0;
        std::size_t max_proposals=0;
        std::size_t max_accepted_proposals=0;
        std::size_t max_accepted_proposals_selected=0;
        std::size_t max_particle_changed_per_seconds=0;
	
	
        for ( int i=0; i<tracker_p.size(); i++ ) {



            sta_assert_error ( tracker_p[i] );
            CTracker<TData,T,Dim> & tracker=*tracker_p[i];
            std::size_t n_points=tracker.particle_pool->get_numpts();
            total_n_points+=n_points;
            std::size_t n_connections=tracker.connections.pool->get_numpts();
            total_n_connections+=n_connections;
            std::size_t n_bif=tracker.connections.num_bifurcation_center_points;
            total_n_bif+=n_bif;
	    std::size_t n_segments=tracker.get_num_graph_segments();
            total_n_segments+=n_segments;
            max_proposals=std::max ( tracker.proposals_per_seconds, ( double ) max_proposals );
            max_accepted_proposals=std::max ( tracker.accepted_proposals_per_seconds, ( double ) max_accepted_proposals );
            max_accepted_proposals_selected=std::max ( tracker.accepted_proposals_per_seconds_selected, ( double ) max_accepted_proposals_selected );
            max_particle_changed_per_seconds=std::max ( tracker.particle_changed_per_seconds, ( double ) max_particle_changed_per_seconds );
	    
        }
        int l_p=std::ceil ( std::log10 ( total_n_points ) );
        int l_e=std::ceil ( std::log10 ( total_n_connections ) );
        int l_b=std::ceil ( std::log10 ( total_n_bif ) );
	int l_s=std::ceil ( std::log10 ( total_n_segments ) );
        int l_pt=std::ceil ( std::log10 ( max_proposals ) );
        int l_pa=std::ceil ( std::log10 ( max_accepted_proposals ) );
        int l_ps=std::ceil ( std::log10 ( max_accepted_proposals_selected ) );
        int l_np=std::ceil ( std::log10 ( max_particle_changed_per_seconds ) );

        glColor4f ( font_color[0],font_color[1],font_color[2],1 );

        for ( int i=0; i<tracker_p.size(); i++ ) {

            sta_assert_error ( tracker_p[i] );
            CTracker<TData,T,Dim> & tracker=*tracker_p[i];
            std::size_t n_points=tracker.particle_pool->get_numpts();
// 			      total_n_points+=n_points;

            std::size_t n_connections=tracker.connections.pool->get_numpts();
// 			      total_n_connections+=n_connections;
            std::size_t n_bif=tracker.connections.num_bifurcation_center_points;
	    
	    std::size_t n_seg=tracker.get_num_graph_segments();

            T temp=tracker.opt_temp;
            /*				s<<"["<<n_points<<" / "<<n_connections<<"] ";    */
            s.precision ( 3 );
            s.str ( "" );
            s<<"p:"<<std::setfill ( ' ' ) << std::setw ( l_p ) <<n_points<<" ";
            s<<"e:"<<std::setfill ( ' ' ) << std::setw ( l_e ) <<n_connections<<" ";
            s<<"b:"<<std::setfill ( ' ' ) << std::setw ( l_b ) <<n_bif<<" ";
	    s<<"s:"<<std::setfill ( ' ' ) << std::setw ( l_s ) <<n_seg<<" ";
            s<<"T:"<< std::fixed<<temp;
            s<<" (worker "<<std::setfill ( ' ' ) << std::setw ( 2 ) <<i+1<<")";
            if ( tracker.accepted_proposals_per_seconds>0 ) {
                s<<" "<< std::setw ( l_pa ) <<std::size_t ( tracker.accepted_proposals_per_seconds );
            }
            if ( tracker.proposals_per_seconds>0 ) {
                s<<" "<< std::setw ( l_pt ) <<std::size_t ( tracker.proposals_per_seconds );
            }
            if ( tracker.accepted_proposals_per_seconds_selected>0 ) {
                s<<" "<< std::setw ( l_ps ) <<std::size_t ( tracker.accepted_proposals_per_seconds_selected );
            }
            if ( tracker.particle_changed_per_seconds>0 ) {
                s<<" "<< std::setw ( l_np ) <<std::size_t ( tracker.particle_changed_per_seconds );
            }
//             s.precision ( 3 );
//             s<<"   "<<tracker.state_energy_grad2[0]<<"   "<<tracker.state_energy_grad2[1];
	    
	    s.precision ( 3 );
            s<<" ["<<tracker.state_particle_number_activity<<" "<<tracker.state_particle_number_activity_max<<"]";

	    
	    s<<"("<<((tracker.active_scheduler==CTracker<TData,T,Dim>::COOL_DOWN_SCHEDULER::COOL_DOWN_BIRTH_DEATH) ? "bd" : "i")<<")";
	    
	    s<<" ["<<tracker.get_E_mean()<<":"<<tracker.get_E_std()<<"]";
	    
            if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_WORKER ) ) {
                bool is_offline=tracker.is_offline();
                float offline_fact_h= ( 1-is_offline );
                float offline_fact_v=0.5*is_offline+ ( 1-is_offline );
                hsv2rgb<float> ( float ( i*360.0f ) /tracker_p.size(),0.75f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2] );

                glColor3fv ( worker_color.v );
            }
            CViewer<TData,T,Dim>::text_HUD->StringoutLine ( s.str(),i+1 );


// 				 state_energy_grad,
// 				opt_temp,
// 				iteration/1000000,
// 				options.opt_numiterations/1000000,
// 				  tree->get_numpts(),
// 				  connections.pool->get_numpts(),
// 				  connections.num_terminals,
// 				  connections.num_bifurcation_center_points
// 				  );


        }
        glColor4f ( font_color[0],font_color[1],font_color[2],1 );
        //text_HUD->StringoutLine(s.str(),1);
        s.str ( "" );
        s<<"p:"<<std::setfill ( ' ' ) << std::setw ( l_p ) <<total_n_points<<" ";
        s<<"e:"<<std::setfill ( ' ' ) << std::setw ( l_e ) <<total_n_connections<<" ";
	s<<"b:"<<std::setfill ( ' ' ) << std::setw ( l_b) <<total_n_bif<<" ";
	s<<"s:"<<std::setfill ( ' ' ) << std::setw ( l_s) <<total_n_segments<<" (total)";
        CViewer<TData,T,Dim>::text_HUD->StringoutLine ( s.str(),0 );

        top_row+=tracker_p.size() +1;


        




	    if ( mouse_state->render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ACTIVITY) ) {			

		  glColor4f ( 1,1,1,1 );
		    s.str ( "" );
		  switch(GuiPoints<T>::activity_mode)
		  {
		      case 1:
			{
			s<< ("activity: connect/ bif reconn + change");
		      }break;
		      case 2:
		      {
			s<<("activity: particle birth");
		      }break;
		      case 3:
		      {
			s<<("activity: bif ");
		      }break;
		      case 4:
		      {
			s<<("activity: all - (move,rot,scale)");
		      }break;
		       case 5:
		      {
			s<<("activity: move");
		      }break;
		      case 6:
		      {
			s<<("activity: scramble");
		      }break;
		      case 7:
		      {
			s<<("activity: smooth");
		      }break;
		       case 8:
		      {
			s<<("activity: bif bridge");
		      }break;
		      default:
		      {
			s<<("activity: all");
		      }break;
		      }
		      CViewer<TData,T,Dim>::text_HUD->StringoutLine ( s.str(),top_row );
		      top_row++;
	    }
	    
	    
	    CViewer<TData,T,Dim>::render_print_info_stack();
            

        {


//   for (int l=0;l<num_debug_points;l++)
//   {
//     if (debug_point_list[l].debug_point!=NULL)
//     {
//     if (debug_point_list[l].debug_life!=debug_point_list[l].debug_point->get_life())
//     {
//       debug_point_list[l].debug_point->clear_selection();
//       debug_point_list[l].debug_point=NULL;
//     }
//     }
//   }



            if ( debug_point.debug_point!=NULL ) {
                if ( debug_point.debug_life!=debug_point.debug_point->get_life() ) {
//       debug_point.debug_point->clear_selection();
//      debug_point.debug_point=NULL;
                    debug_point.clear();
                } else {
                    const Vector<T,Dim> & n=debug_point.debug_point->get_direction();
                    T scale=debug_point.debug_point->get_scale();
                    if ( ( n.norm1() >0 ) && ( scale>0 ) && ( scale<1000 ) ) {

                        glColor4f ( font_color[0],font_color[1],font_color[2],1 );
                        top_row++;


                        sta_assert_error ( tracker_p.size() >=debug_point.debug_point_tracker_id );
                        sta_assert_error ( tracker_p[debug_point.debug_point_tracker_id]!=NULL );
                        try {


			    T result=0;
                            if ( debug_point.debug_point->particle_type!= ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
			      
			      
			        T scale_min=tracker_p[debug_point.debug_point_tracker_id]->data_fun->get_minscale();
				T scale_max=tracker_p[debug_point.debug_point_tracker_id]->data_fun->get_maxscale();
				s.str ( "[" );
 				  for (int a=0;a<6;a++)
				  {
				    T scale=scale_min+(a*(scale_max-scale_min))/(5);
// 				      tracker_p[debug_point.debug_point_tracker_id]->data_fun->data_score( result,
// 					debug_point.debug_point->get_direction(),
// 					debug_point.debug_point->get_position(),
// 					scale,
//                                         NULL);
				      tracker_p[debug_point.debug_point_tracker_id]->data_fun->data_score( result,
					debug_point.debug_point->get_direction(),
					debug_point.debug_point->get_position(),
					scale,
                                         debug_point.debug_point);
					
					s<<" "<<result;
				  }
				  s<<"]";
				  messages.push_back ( s.str() );
			      
			      
                                
                                std::list<std::string>  debug_data;
                                if ( data_kernel_Debug ) {
                                    debug_data.push_back ( "" );
                                }
                                tracker_p[debug_point.debug_point_tracker_id]->data_fun->eval_data ( result,
					*debug_point.debug_point,
                                        &debug_data );

                                messages.splice ( messages.end(), debug_data );



// 	  for (typename std::list<std::string>::iterator iter=debug_data.begin();iter!=debug_data.end();iter++)
// 	  {
// 	    text_HUD->StringoutLine(*iter,top_row++);
// 	  }
                                s.str ( "" );
                                s<<"filter response: "<<result;
                                messages.push_back ( s.str() );
// 	  text_HUD->StringoutLine(s.str(),top_row++);
                            }
                            
                            s.str ( "" );
			    s<<"position:"<<debug_point.debug_point->get_position()[0]<<":"<<debug_point.debug_point->get_position()[1]<<":"<<debug_point.debug_point->get_position()[2];
                                messages.push_back ( s.str() );




// 	if (false)
                            {

                                s.precision ( 3 );
                                s.str ( "" );
                                s<<"# scale: "<<debug_point.debug_point->get_scale();
                                messages.push_back ( s.str() );
// 	  text_HUD->StringoutLine(s.str(),top_row++);

                                s.precision ( 3 );
                                s.str ( "" );
                                s<<"# point dcost: "<<debug_point.debug_point->point_cost;
                                messages.push_back ( s.str() );
// 	  text_HUD->StringoutLine(s.str(),top_row++);

                                s.precision ( 20 );
                                s.str ( "" );
                                s<<"# point  sail: "<<debug_point.debug_point->saliency;
                                messages.push_back ( s.str() );
// 	  text_HUD->StringoutLine(s.str(),top_row++);

				if ( debug_point.debug_point->particle_type!= ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
				  s.precision ( 3 );
				  s.str ( "" );
				  T temp=tracker_p[debug_point.debug_point_tracker_id]->opt_temp;
				  s<<"# point scost: "<<debug_point.debug_point->compute_cost3 ( temp );
				  messages.push_back ( s.str() );
				}
				
				if ( debug_point.debug_point->particle_type!= ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
				  s.precision ( 3 );
				  s.str ( "" );
// 				  T scale_min=tracker_p[debug_point.debug_point_tracker_id]->data_fun->get_minscale();
// 				  T scale_max=tracker_p[debug_point.debug_point_tracker_id]->data_fun->get_maxscale();
// 				  for (int a=0;a<)
				  s<<"# costs (in scale): "<<debug_point.debug_point->compute_cost3 ( debug_point.debug_point->get_scale() )/debug_point.debug_point->get_scale()<<"  "<<result/debug_point.debug_point->get_scale();
				  messages.push_back ( s.str() );
				}

// 	  text_HUD->StringoutLine(s.str(),top_row++);

// 	  typename CEdgecost<T,Dim>::EDGECOST_STATE  inrange;
// 	   T ecost=debug_point.debug_point->edge_energy(
// 	    *tracker_p[debug_point.debug_point_tracker_id]->edgecost_fun,
// 	    0,
// 	    0,
// 	    tracker_p[debug_point.debug_point_tracker_id]->maxendpointdist,
// 	    inrange);
// 	  s.str("");
// 	  s<<"# point ecost: "<<ecost<<" valid:"<<(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	  text_HUD->StringoutLine(s.str(),top_row++);



                                s.str ( "" );
                                s<<"# grid normal:"<<debug_point.debug_point->has_owner ( 0 ) <<" con:"<<debug_point.debug_point->has_owner ( 1 );
                                messages.push_back ( s.str() );
// 	  text_HUD->StringoutLine(s.str(),top_row++);
				
				s.str ( "" );
                                s<<"# edges: ("<<debug_point.debug_point->endpoint_connections[0]<<" : "<<debug_point.debug_point->endpoint_connections[1]<<")";
                                messages.push_back ( s.str() );


                                messages.push_back ( "--snapshots--" );
                                s.precision ( 3 );
                                s.str ( "" );
                                s<<"# edgecost: "<<debug_point.debug_point->snapshot_connected_egde_cost;
                                messages.push_back ( s.str() );

				s.precision ( 6 );
                                s.str ( "" );
                                s<<"# proposed edge: "<<debug_point.debug_point->snapshot_edge_proposed_costs[0]
                                 <<" -> "<<debug_point.debug_point->snapshot_edge_proposed_costs[1]
                                 <<" : "<<debug_point.debug_point->snapshot_edge_proposed_costs[3]
                                 <<" ? "<< ( debug_point.debug_point->snapshot_edge_proposed_costs[2]>0 )
                                 <<" ("<<debug_point.debug_point->snapshot_edge_proposed_costs[4]<<"|"<<debug_point.debug_point->snapshot_edge_proposed_costs[5]<<")";
                                messages.push_back ( s.str() );

                                s.precision ( 2 );
                            }



                            for ( std::list<std::string>::iterator iter=messages.begin(); iter!=messages.end(); iter++ ) {
                                CViewer<TData,T,Dim>::text_HUD->StringoutLine ( ( *iter ),top_row++ );
                            }

                        } catch ( ... ) {

                        }
                        top_row++;






                        sta_assert_error ( n.norm2() >0 );

	  if (img_smooth!=NULL)
	  {
			
			switch (debug_patch_mode)
			{
			 
			 
			 case 1:
			 {
			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents ,debug_point.debug_point->get_scale(),0 );
			 }
			  break;
			 case 2:
			 {
			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents,debug_point.debug_point->get_scale(),1 );
			 }break;
			 case 3:
			 {
			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents,debug_point.debug_point->get_scale(),2 );
			 }break;
			 case 4:
			 {
			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents,debug_point.debug_point->get_scale(),3 );
			 }break;
			 case 5:
			 {
			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents,debug_point.debug_point->get_scale(),4 );
			 }break;
// 			 case 4:
// 			 {
// 			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents,debug_point.debug_point->get_scale(),4 );
// 			 }break;
// 			 case 5:
// 			 {
// 			    get_patch_scale ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents,debug_point.debug_point->get_scale(),5 );
// 			 }break;
			    
			 default:
			    get_patch ( sliceview_slice,debug_point.debug_point->get_position()+debug_poit_world_offset,n,sliceview_extents, ( scale*3.0f ) /sliceview_extents );
			}
			
                        
                        mhs_check_gl
                        glDeleteTextures ( 1,&sliceview_textureID );
                        sliceview_textureID=GenerateTextureRectangle ( sliceview_slice,sliceview_extents,sliceview_extents );
                        mhs_check_gl

                        glDisable ( GL_CULL_FACE );
                        glEnable ( GL_COLOR_MATERIAL );
                        glMatrixMode ( GL_TEXTURE );
                        glLoadIdentity();
                        glMatrixMode ( GL_MODELVIEW );
                        glPushMatrix();
                        glLoadIdentity();
                        //    glEnable(GL_BLEND);
                        glColor4f ( 1,1,1,1 );
                        glActiveTextureARB ( GL_TEXTURE0_ARB );
                        mhs_check_gl
                        glEnable ( GL_TEXTURE_2D );
                        mhs_check_gl
                        glBindTexture ( GL_TEXTURE_2D, sliceview_textureID );
                        mhs_check_gl




                        const float w=1;
                        const float h=1;
                        float pixel_offset_y=5+13;
                        float pixel_offset_x=5+8;
                        float row=top_row+1;
                        float x=float ( pixel_offset_x ) /this->width;
                        float y=float ( this->height- ( row*pixel_offset_y ) ) /this->height;
// 	float xw=float(sliceview_extents)/width;
// 	float yw=float(sliceview_extents)/height;
                        float xw=float ( 128 ) /this->width;
                        float yw=float ( 128 ) /this->height;
                        glBegin ( GL_QUADS );
                        {
                            glTexCoord2f ( w,h );
                            glVertex3f ( x+xw,y-yw,0 );

                            glTexCoord2f ( 0,h );
                            glVertex3f ( x,y-yw,0 );

                            glTexCoord2f ( 0,0 );
                            glVertex3f ( x,y,0 );

                            glTexCoord2f ( w,0 );
                            glVertex3f ( x+xw,y,0 );
                        }
                        glEnd();

                        glBindTexture ( GL_TEXTURE_2D, 0 );
                        mhs_check_gl
                        glDisable ( GL_TEXTURE_2D );
                        glPopMatrix();
			
	  }			
                    } else {
                        sta_assert_error ( debug_point.debug_point!=NULL );
                        debug_point.clear();
                    }

                }
            }
        }



} else 
{
		    try {
		      for ( std::list<std::string>::iterator iter=messages.begin(); iter!=messages.end(); iter++ ) {
                                CViewer<TData,T,Dim>::text_HUD->StringoutLine ( ( *iter ),top_row++ );
                            }

                        } catch ( ... ) {

                        }
}  
 
        glPopMatrix();
	
        CViewer<TData,T,Dim>::text_HUD->DeinitHUD();

        mhs_check_gl
        glFinish();
	
	
	if ((movie_folder.length()>0))
	{
	  double current_time=timer.get_total_runtime();
	  if ((recording_user)&&((current_time-prev_user_recording_time)>1.0/24.0))
	  {
	    prev_user_recording_time=current_time;
	    std::stringstream filename;
	    filename<<movie_folder<<"/"<<"tracker_movie_UI"<<10000+frame_user++<<".jpg";
	    this->save_image_to_file2(filename.str()); 
	    printf("writing frame %d to disk\n",frame_user);
	  }
	  
// 	  printf("%d %d\n",movie_state==MOVIE_SATE::MOVIE_RECORDING_WAITING_FOR_CAPTURE, (cam_is_progressing));
	  
	  
	  if ((movie_state==MOVIE_SATE::MOVIE_RECORDING_WAITING_FOR_CAPTURE)&&(cam_is_progressing))
	  {
	    printf("waiting for trackers to pause\n");
	    pause_tracker_and_wait();
	    printf("taking screenshot\n");
	    std::stringstream filename;
	    filename<<movie_folder<<"/"<<"tracker_movie_FLIGHT"<<10000+frame_flight++<<".jpg";
	    this->save_image_to_file2(filename.str()); 
	    if (!cam->cam_is_flying())
	      movie_state=MOVIE_SATE::MOVIE_NOT_RECORDING;
	    else 
	      movie_state=MOVIE_SATE::MOVIE_RECORDING;
	    printf("writing frame %d to disk\n",frame_flight);
	    
	     mouse_state->pause_program=false;
	    
	  }

	}
	
	
	
        SDL_GL_SwapWindow ( CSceneRenderer::tracker_data_p->tracker_window );
	
	
	
	
	{
	 CTracker<TData,T,Dim> & tracker=*tracker_p[0];
	 
	if (tracker.options.opt_create_movie)	
	{
	  if (!mouse_state->pause_program)
	  {
	    pause_tracker_and_wait();
	  
  // 	  mxArray *arg2 = mhs::mex_string2mex("hellp");
  // 	  mxDestroyArray(arg2);
	    
	    for ( int i=0; i<tracker_p.size(); i++ ) {
		sta_assert_error ( tracker_p[i] );
		CTracker<TData,T,Dim> & tracker=*tracker_p[i];
		sta_assert_error(tracker.is_pausing)
		{
// 		  tracker.movie.check(tracker);
		}
	    }
	    mouse_state->pause_program=false;
	  }
	  }
	}

    }

    
    void pause_tracker_and_wait()
    {
	MyMouse<TData,T,Dim> * mouse_state= ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state;
	bool is_pausing=false;
	mouse_state->pause_program=true;
	while (!is_pausing)
	{
	  is_pausing=true;
	  for ( int i=0; i<tracker_p.size(); i++ ) {
	      sta_assert_error ( tracker_p[i] );
	      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
	      if (!tracker.is_pausing)
	      {
		is_pausing=false;
	      }
	  }
	}
    }
    
   
    



    void register_tracker ( CTracker<TData,T,Dim> & tracker ) {
        tracker_p.push_back ( &tracker );
    }



protected:



    GLuint GenerateTextureRectangle ( const float * slice,int w, int h ) {
        GLenum internalFormat=GL_LUMINANCE8;//GL_LUMINANCE16
        GLuint textureID;

        glGenTextures ( 1, &textureID );
        glBindTexture ( GL_TEXTURE_2D, textureID );

        glTexParameteri ( GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST );
        glTexParameteri ( GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );

        glTexImage2D ( GL_TEXTURE_2D, 0, internalFormat,
                       h,w, 0,GL_LUMINANCE, getFormat<float>(), slice );
        return textureID;
    }


    
   
    

    bool get_patch ( float * img_slice,const Vector<T,3> & position,const Vector<T,3> & dir,int res,float scale ) {

        Vector<T,3> vn1;
        Vector<T,3> vn2;
        Vector<T,3> p;
        mhs_graphics::createOrths ( dir,vn1,vn2 );
        std::size_t shapei[3];
        shapei[0]=CSceneRenderer::shape[0];
        shapei[1]=CSceneRenderer::shape[1];
        shapei[2]=CSceneRenderer::shape[2];

        vn1*=scale;
        vn2*=scale;

        if ( CViewer<TData,T,Dim>::img!=NULL ) {



            for ( int x=0; x<res; x++ ) {
                for ( int y=0; y<res; y++ ) {
                    p=position+vn1* ( x-0.5* ( res-1 ) ) +vn2* ( y-0.5* ( res-1 ) );

                    if ( true ) {
		       if (!data_interpolate(
			  p,
			  img_slice[y*res+x],
			  CViewer<TData,T,Dim>::img,
			  shapei,1,0,0))
			    img_slice[y*res+x]=0.0f;
//                         int Z=std::floor ( p[0]+0.5f );
//                         int Y=std::floor ( p[1]+0.5f );
//                         int X=std::floor ( p[2]+0.5f );
//                         if ( ( Z>=shapei[0] ) || ( Y>=shapei[1] ) || ( X>=shapei[2] ) ||
//                                 ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
//                             img_slice[y*res+x]=0.0f;
//                         } else {
//                             img_slice[y*res+x]=CViewer<TData,T,Dim>::img[ ( ( Z*shapei[1]+Y ) *shapei[2]+X )];
//                         }
                    } else {
			 if (!data_interpolate(
			  p,
			  img_slice[y*res+x],
			  CViewer<TData,T,Dim>::img,
			  shapei))
			    img_slice[y*res+x]=0.0f;
                    }
                }
            }


            float  minv=std::numeric_limits<float>::max();
            float  maxv=std::numeric_limits<float>::min();

            for ( int x=0; x<res*res; x++ ) {
                minv=std::min ( minv,img_slice[x] );
                maxv=std::max ( maxv,img_slice[x] );
            }
            float scale= ( maxv-minv ) +0.000001;
            for ( int x=0; x<res*res; x++ ) {
                img_slice[x]= ( img_slice[x]-minv ) /scale;
            }

            return true;
        }
        return false;
    }

    bool get_patch_scale ( float * img_slice,const Vector<T,3> & position,const Vector<T,3> & dir,int res,float scale, float point_scale,int mode=0) {

        Vector<T,3> vn1;
        Vector<T,3> vn2;
        Vector<T,3> p;
        mhs_graphics::createOrths ( dir,vn1,vn2 );
        std::size_t shapei[3];
        shapei[0]=CSceneRenderer::shape[0];
        shapei[1]=CSceneRenderer::shape[1];
        shapei[2]=CSceneRenderer::shape[2];

	Vector<T,3> vn[2];
	vn[0]=vn1;
	vn[1]=vn2;	
	
        vn1*=scale;
        vn2*=scale;

	
	point_scale=std::max(float(scales[0]),std::min((float)(scales[num_scales-1]),point_scale));
	

	
        if ( img_smooth!=NULL ) {



            for ( int x=0; x<res; x++ ) {
                for ( int y=0; y<res; y++ ) {
                    p=position+vn1* ( x-0.5* ( res-1 ) ) +vn2* ( y-0.5* ( res-1 ) );

switch(mode)		    
{
		    
		    case 0:
		    {
			 if (!data_interpolate_polynom<float,T,TData>(
			  p,
			  point_scale,
			  img_smooth_is_pol ? NULL : scales,
			  img_smooth,
			  num_alphas_img_smooth,
			  img_slice[y*res+x],
			  shapei,1,0,0))
			   img_slice[y*res+x]=0.0f;
// 		       if (!data_interpolate(
// 			  p,
// 			  img_slice[y*res+x],
// 			  CViewer<TData,T,Dim>::img,
// 			  shapei,1,0,1))
// 		      
// 			   img_slice[y*res+x]=0.0f;
			                         
		    }break;
		    
		      case 1:
		    {
// 			 if (!data_interpolate_polynom<float,T,TData>(
// 			  p,
// 			  point_scale,
// 			  img_smooth_is_pol ? NULL : scales,
// 			  img_smooth,
// 			  num_alphas_img_smooth,
// 			  img_slice[y*res+x],
// 			  shapei,1,0,2))
// 			   img_slice[y*res+x]=0.0f;
// 		      if (!data_interpolate(
// 			  p,
// 			  img_slice[y*res+x],
// 			  CViewer<TData,T,Dim>::img,
// 			  shapei,1,0,2))
// 		      
// 			   img_slice[y*res+x]=0.0f;
		      if (!data_interpolate_polynom<float,T,TData>(
			  p,
			  point_scale,
			  img_smooth_is_pol ? NULL : scales,
			  img_smooth,
			  num_alphas_img_smooth,
			  img_slice[y*res+x],
			  shapei,1,0,1))
			                    img_slice[y*res+x]=0.0f;      
		    }break;
		    
		    case 2:
		     {
		       
				img_slice[y*res+x]=0.0f;


				int samples=1;
				T h[samples];
				
				h[0]=std::max(point_scale/2,float(1));
				h[1]=1;
				
// 				h[0]=point_scale/2;
// 				h[1]=1;
// 				h[1]=point_scale/2;
// 				h[0]=point_scale;
				
				
				T Lap=0;
				
				
				Vector<T,3> pos;
				
				
				for (int s=0;s<samples;s++)
				{
				    T lap=0;
				    T lap2=0;
				    
				    T v;
				    bool valid[2];
				    int count0=0;
				    int count1=0;
				    for (int x=0;x<3;x++)
				    {
				      valid[0]=(x==1);
				      for (int y=0;y<3;y++)
				      {
					valid[1]=(y==1);
					if ((valid[0])||(valid[1]))
					{
					  pos=p+vn[0]*(x-1)*h[s]+vn[1]*(y-1)*h[s];
					  v=0;
//  					    data_interpolate_polynom<T,T,TData> ( pos,point_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,v ,shapei,1,0,2); 
					  data_interpolate_polynom<T,T,TData> ( pos,point_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,v ,shapei); 
				
				
					if ((x!=1)||(y!=1))
					  {
					  lap+=v; 
					  count0++;
					  }else 
					  {
					  lap2+=v; 
					  count1++;
					  }
					  }
				      }
				    }
				    
				    if (count0!=4)
				      printf("count0\n");
				    if (count1!=1)
				      printf("count1\n");
				    
				    count0=0;
				    
				    T lap3=0;
				    for (int x=0;x<3;x++)
				    {
				      valid[0]=(x!=1);
				      for (int y=0;y<3;y++)
				      {
					valid[1]=(y!=1);
					if ((valid[0])&&(valid[1]))
					{
					  pos=p+vn[0]*(x-1)*h[s]+vn[1]*(y-1)*h[s];
					  v=0;
//  					  data_interpolate_polynom<T,T,TData> ( pos,point_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,v ,shapei,1,0,2); 
					  data_interpolate_polynom<T,T,TData> ( pos,point_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth,v ,shapei); 
// 					
					 
					  lap3+=v; 
					  count0++;
					}
				      }
				    }
				    
				    if (count0!=4)
				      printf("count0_2\n");

				    T N=point_scale*point_scale/(h[s]*h[s]*samples);
				    const T gamma=1.0/3.0;
  				    Lap+=N*((1.0-gamma)*(lap-4*lap2)+gamma*(0.5*lap3-2*lap2));
//      				    Lap+=(lap-4*lap2);
//				    Lap+=(lap2);
// 				    printf("%f %f / %f\n",lap,4*lap2,h[0]);
//  				    Lap+=(0.5*lap3-2*lap2);
			      
				}
 				img_slice[y*res+x]=(-Lap);
// 				img_slice[y*res+x]=x*res+y;
			}break;
			
// 			case 2:
// 			{
// 			 
// 			  
// 			  	T local_Grad=0;
// 				  {
// 				    
// 				    
// 			  // 	  T h=radius;
// 				    T h=1;
// 				    
// 				    int count=0;   
// 				    Vector<T,3> pos;
// 				    bool valid[3];
// 				    T img_patch[3][3];
// 				    
// 				    for (int y=0;y<3;y++)
// 				    {
// 				      for (int x=0;x<3;x++)
// 				      {
// 					pos=p+vn[0]*(x-1)*h+vn[1]*(y-1)*h;
// 					data_interpolate_polynom<T,T,TData>(
// 						pos,
// 						point_scale,
// 						img_smooth,
// 						num_alphas_img_smooth,
// 						img_patch[x][y],
// 						shapei);	
// 				      }
// 				    }
// 				    
// 				    for (int y=0;y<3;y++)
// 				    {
// 				      for (int x=0;x<3;x++)
// 				      {
// 					if ((x!=1)||(y!=1))
// 					{
// 					  local_Grad=std::max((img_patch[x][y]-img_patch[1][1]),local_Grad);
// 					}
// 				      }
// 				    }
// // 				    local_Grad*=radius/h;
// 				    
// 
// 				    }
// 				    
// 				    img_slice[y*res+x]=local_Grad;
// 			}break;
			
			case 3:
			{
			  
/*			      Vector<T,3> X;
			      X[0]=std::round(p[0]);
			      X[1]=std::round(p[1]);
			      X[2]=std::round(p[2]);	*/		 
			  
   				    T h=std::max(point_scale/2,1.0f);
//   				    T h=1;
				    T hz=h;
				    T hy=h;
				    T hx=h;
				    
				    int count=0;   
				    Vector<T,3> pos;
				    bool valid[3];
				    T img_patch[3][3][3];
				    
				    
				    for (int z=0;z<3;z++)
				    {
				      valid[0] = (z==1);
				      pos[0]=p[0]+(z-1.0)*hz;
				      for (int y=0;y<3;y++)
				      {
					valid[1] = (y==1);
					pos[1]=p[1]+(y-1.0)*hy;
					for (int x=0;x<3;x++)
					{
					  img_patch[x][y][z]=0;
					  valid[2] = (x==1);
					  if (valid[0]||valid[1]||valid[2])
					  {
					    count++;
					    pos[2]=p[2]+(x-1.0)*hx;
					    
// 					    data_interpolate_polynom<T,T,TData>(
// 						pos,
// 						point_scale,
// 						img_smooth,
// 						num_alphas_img_smooth,
// 						img_patch[x][y][z],
// 						shapei);	
					    data_interpolate_polynom<T,T,TData> ( pos,point_scale,img_smooth_is_pol ? NULL : scales,img_smooth, num_alphas_img_smooth, img_patch[x][y][z] ,shapei); 
					    
// 					    if (img_smooth_is_pol)
// 					    {
// 					      data_interpolate_polynom2<T,T,TData>(
// 						  pos,
// 						  point_scale,
// 						  img_smooth,
// 						  num_alphas_img_smooth,
// 						  img_patch[x][y][z],
// 						  shapei);
// 					    }else 
// 					    {
// 					       data_interpolate_local_polynom<T,T,TData>(
// 						  pos,
// 						  point_scale,
// 						  scales,
// 						  img_smooth,
// 						  num_alphas_img_smooth,
// 						  img_patch[x][y][z],
// 						  shapei);
// 					    }
					    
					  }
					}
				      } 
				    }
				    
				    
				    T dxx=(img_patch[0][1][1]-2*img_patch[1][1][1]+img_patch[2][1][1]);
				    T dyy=(img_patch[1][0][1]-2*img_patch[1][1][1]+img_patch[1][2][1]);
// 				    T dzz=(img_patch[1][1][0]-2*img_patch[1][1][1]+img_patch[1][1][2]);
				    T dzz=((img_patch[1][1][0]+img_patch[1][1][2])-img_patch[1][1][1]-img_patch[1][1][1]);
				    
				    T trace=dxx+dyy+dzz;
				    dxx-=trace;
				    dyy-=trace;
				    dzz-=trace;
				    
				    T dxy=T(0.25)*((img_patch[0][0][1]+img_patch[2][2][1])-(img_patch[0][2][1]+img_patch[2][0][1]));
				    T dyz=T(0.25)*((img_patch[1][0][0]+img_patch[1][2][2])-(img_patch[1][0][2]+img_patch[1][2][0]));
				    T dxz=T(0.25)*((img_patch[0][1][0]+img_patch[2][1][2])-(img_patch[0][1][2]+img_patch[2][1][0]));	  
				    
				    T N=point_scale*point_scale/(h*h);
				    
				    const  T & nx=dir[2];
				    const  T & ny=dir[1];
				    const  T & nz=dir[0];
			   
				      img_slice[y*res+x]=N*(nx*nx*dxx+ny*ny*dyy+nz*nz*dzz+
						    +2*nx*(ny*dxy+nz*dxz)
						    +2*ny*nz*dyz); 
				    
// 				    img_slice[y*res+x]=dzz; 
			  
			}break;
			
			case 4:
			{
			  if ( hessian!=NULL ) 
			  
			  {
			  T H[6];
			 
// 			      Vector<T,3> X;
// 			      X[0]=std::round(p[0]);
// 			      X[1]=std::round(p[1]);
// 			      X[2]=std::round(p[2]);
			  
			      for (int i=0; i<6; i++)
			      {
// 				data_interpolate_polynom<T,T,TData>(
// 						X,
// 						point_scale,
// 						hessian,
// 						num_alphas_hessian,
// 						H[i],
// 						shapei,6,i);	
				
				data_interpolate_polynom<T,T,TData> ( p,point_scale,NULL,hessian,
						num_alphas_hessian,
						H[i],
						shapei,6,i);	
				
			      }

			      T & xx=  H[0]; //xx
			      T & xy=  H[3]; //xy
			      T & xz=  H[5]; //xz
			      T & yy=  H[1]; //yy
			      T & yz=  H[4]; //yz
			      T & zz=  H[2]; //zz

			      const T & nx=dir[2];
			      const T & ny=dir[1];
			      const T & nz=dir[0];

			      img_slice[y*res+x]=(nx*nx*xx+ny*ny*yy+nz*nz*zz+
					    +2*nx*(ny*xy+nz*xz)
					    +2*ny*nz*yz);
// 			      img_slice[y*res+x]=zz;
			  }else 
			  {
			    img_slice[y*res+x]=0;
			  }
			  }break;
			  
// 			case 5:
// 			{
// 			  
// 			      Vector<T,3> X;
// 			      X[0]=std::round(p[0]);
// 			      X[1]=std::round(p[1]);
// 			      X[2]=std::round(p[2]);			 
// 			  
// //  				    T h=point_scale/2;
//  				    T h=1;
// 				    T hz=h;
// 				    T hy=h;
// 				    T hx=h;
// 				    
// 				    int count=0;   
// 				    Vector<T,3> pos;
// 				    bool valid[3];
// 				    T img_patch[3][3][3];
// 				    
// 				    
// 				    for (int z=0;z<3;z++)
// 				    {
// 				      valid[0] = (z==1);
// 				      pos[0]=X[0]+(z-1.0)*hz;
// 				      for (int y=0;y<3;y++)
// 				      {
// 					valid[1] = (y==1);
// 					pos[1]=X[1]+(y-1.0)*hy;
// 					for (int x=0;x<3;x++)
// 					{
// 					  img_patch[x][y][z]=0;
// 					  valid[2] = (x==1);
// 					  if (valid[0]||valid[1]||valid[2])
// 					  {
// 					    count++;
// 					    pos[2]=X[2]+(x-1.0)*hx;
// 					    
// 					    if (img_smooth_is_pol)
// 					    {
// 					      data_interpolate_polynom2<T,T,TData>(
// 						  pos,
// 						  point_scale,
// 						  img_smooth,
// 						  num_alphas_img_smooth,
// 						  img_patch[x][y][z],
// 						  shapei);
// 					    }else 
// 					    {
// 					       data_interpolate_local_polynom<T,T,TData>(
// 						  pos,
// 						  point_scale,
// 						  scales,
// 						  img_smooth,
// 						  num_alphas_img_smooth,
// 						  img_patch[x][y][z],
// 						  shapei);
// 					    }
// 					  }
// 					}
// 				      } 
// 				    }
// 				    
// 				    
// 				    T dxx=(img_patch[0][1][1]-2*img_patch[1][1][1]+img_patch[2][1][1]);
// 				    T dyy=(img_patch[1][0][1]-2*img_patch[1][1][1]+img_patch[1][2][1]);
// // 				    T dzz=(img_patch[1][1][0]-2*img_patch[1][1][1]+img_patch[1][1][2]);
// 				    T dzz=((img_patch[1][1][0]+img_patch[1][1][2])-img_patch[1][1][1]-img_patch[1][1][1]);
// 				    
// 				    T trace=dxx+dyy+dzz;
// 				    dxx-=trace;
// 				    dyy-=trace;
// 				    dzz-=trace;
// 				    
// 				    T dxy=T(0.25)*((img_patch[0][0][1]+img_patch[2][2][1])-(img_patch[0][2][1]+img_patch[2][0][1]));
// 				    T dyz=T(0.25)*((img_patch[1][0][0]+img_patch[1][2][2])-(img_patch[1][0][2]+img_patch[1][2][0]));
// 				    T dxz=T(0.25)*((img_patch[0][1][0]+img_patch[2][1][2])-(img_patch[0][1][2]+img_patch[2][1][0]));	  
// 				    
// 				    T N=point_scale*point_scale/(h*h);
// 				    
// 				    const  T & nx=dir[2];
// 				    const  T & ny=dir[1];
// 				    const  T & nz=dir[0];
// 			   
//  				      img_slice[y*res+x]=N*(nx*nx*dxx+ny*ny*dyy+nz*nz*dzz+
//  						    +2*nx*(ny*dxy+nz*dxz)
//  						    +2*ny*nz*dyz); 
// 				    
// // 				    img_slice[y*res+x]=dzz; 
// 			  
// 			}break;
					  
		      }			
					
				      
					  
				      
				  }
			      }


			      float  minv=std::numeric_limits<float>::max();
			      float  maxv=std::numeric_limits<float>::min();

			      for ( int x=0; x<res*res; x++ ) {
				  minv=std::min ( minv,img_slice[x] );
				  maxv=std::max ( maxv,img_slice[x] );
			      }
			      float scale= ( maxv-minv ) +0.00000001;
			      for ( int x=0; x<res*res; x++ ) {
				  img_slice[x]= ( img_slice[x]-minv ) /scale;
			      }

            return true;
        }
        return false;
    }

    



    bool particle_hittest ( GLdouble  winX,  GLdouble  winY,
                            Vector<float,3> & focus_point,
                            Vector<float,3> & focus_point_img ) {
        //TODO ; consider cliplane


        unsigned int render_mode= ( ( MyMouse<TData,T,Dim>* ) this->mouse_state )->render_mode;

        Matrix<double,4> projection;
        Matrix<double,4> modelview;

        Vector<double,3> hit0;
        Vector<double,3> hit1;

        Vector<int,4> viewport;
        viewport[0]=0;
        viewport[1]=0;
        viewport[2]=this->width;
        viewport[3]=this->height;


        projection=CViewer<TData,T,Dim>::projection_mat;
        modelview= ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform );

        float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );

        Vector<float,3>new_center;
        new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
        new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
        new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;
        Matrix<float,4> trafo;
        trafo=modelview;

        gluUnProject ( winX,this->height-winY,0.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit0.v[0],&hit0.v[1],&hit0.v[2] );
        gluUnProject ( winX,this->height-winY,1.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit1.v[0],&hit1.v[1],&hit1.v[2] );

        Vector<float,3> ray_start;
        Vector<float,3> ray_dir;

        //without clipping
        ray_start=hit0;
        ray_dir= ( hit1-hit0 );
        ray_dir.normalize();

        {
            Vector<float,3> selection;
            Vector<float,3> selection_img;
            float dist=std::numeric_limits< float >::max();

            float eye_dist=std::numeric_limits< float >::max();
            float min_dist_sq=rescale*rescale*25*25;
// 	float min_dist_sq=0.05*0.05;


//             bool no_single= ( render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_CONNECTED ) )
//                             | ( render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_LINE ) );
	    bool no_single= ( render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_CONNECTED ) )
                            ;


            for ( int a=0; a<tracker_p.size(); a++ ) {

                sta_assert_error ( tracker_p[a] );
                CTracker<TData,T,Dim> & tracker=*tracker_p[a];

                if ( ( render_mode&static_cast<unsigned int> ( MyMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ONLINE ) ) && ( tracker.is_offline() ) ) {
                    continue;
                }

                Vector<T,3> world_offset=tracker.get_world_offset();

                const class Points<T,Dim> ** particle_ptr=tracker.particle_pool->getMem();
                std::size_t n_points=tracker.particle_pool->get_numpts();



                Vector<float,Dim> new_pos;
                Points<T,Dim>  point;
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> * point_p= ( ( Points<T,Dim>* ) particle_ptr[i] );
                    if ( point_p==NULL ) {
                        continue;
                    }

                    if ( !tracker.particle_pool->is_valid_adress ( point_p ) ) {
                        continue;
                    }

                    point=*point_p;

                    if ( ( no_single ) && ( !point.isconnected() ) ) {
                        continue;
                    }

                    Vector<float,3> position;

                    //new_pos=point.position+world_offset;
                    position=point.get_position()+world_offset;

                    new_pos= ( position*rescale-new_center );

                    float ray_proj_dist= ( ( new_pos-ray_start ).dot ( ray_dir ) );

                    //TODO : consier distance to viewer
                    //IDEA : include scale of particel in collision

                    //if (ray_proj_dist<0) continue; //behind eye

                    Vector<float,3> closet_ray_pt=ray_start+ray_dir*ray_proj_dist;


                    /*
                    float tmp=(closet_ray_pt[0]-new_pos[0]);
                    float new_dist=tmp*tmp;
                    if (min_dist_sq<new_dist) continue;
                    tmp=(closet_ray_pt[1]-new_pos[1]);
                    new_dist+=tmp*tmp;
                    if (min_dist_sq<new_dist) continue;
                    tmp=(closet_ray_pt[2]-new_pos[2]);
                    new_dist+=tmp*tmp;
                    if (min_dist_sq<new_dist) continue;
                    if (eye_dist>ray_proj_dist)
                    {
                      debug_point=point_p;
                      debug_life=point.get_life();
                      dist=new_dist;
                      selection=new_pos;
                    }
                    */



                    float tmp= ( closet_ray_pt[0]-new_pos[0] );
                    float new_dist=tmp*tmp;
                    if ( dist<new_dist ) {
                        continue;
                    }
                    tmp= ( closet_ray_pt[1]-new_pos[1] );
                    new_dist+=tmp*tmp;
                    if ( dist<new_dist ) {
                        continue;
                    }
                    tmp= ( closet_ray_pt[2]-new_pos[2] );
                    new_dist+=tmp*tmp;
                    if ( dist>new_dist ) {
                        debug_point.assign ( point_p,a );
                        dist=new_dist;
                        selection=new_pos;
                        debug_poit_world_offset=world_offset;
                        selection_img=position;
                    }

                }
            }
            if ( dist<min_dist_sq ) {
                focus_point=trafo.multv3 ( selection );
                focus_point_img=selection_img;
                return true;
            }
            debug_point.clear();
        }

        return false;

    }

    void render_outline() {
        MyMouse<TData,T,Dim> * mouse_state= ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state;
        if ( ( mouse_state->is_busy() ) && ( ! ( mouse_state->point_select_on ) ) ) {
            float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );


            for ( int z=0; z<2; z++ )
                for ( int y=0; y<2; y++ )
                    for ( int x=0; x<2; x++ ) {
                        float scale=0.01;
                        glPushMatrix();
                        glTranslatef ( z*CSceneRenderer::shape[0]*rescale,y*CSceneRenderer::shape[1]*rescale,x*CSceneRenderer::shape[2]*rescale );
                        int particle_typ=0;
                        //int color_id=CSceneRenderer::Clightgray;
                        //int color_id=CSceneRenderer::Cred;
                        int color_id=CSceneRenderer::Corange;
                        glColor3fv ( this->particle_colors[color_id] );
                        glScalef ( scale,scale,scale );
                        std::size_t offset=this->buffered_objects[particle_typ].i_offset;
#ifdef  _USE_CGC_			
                        cgGLSetStateMatrixParameter ( this->_modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY );
                        cgGLSetStateMatrixParameter ( this->_normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE );
#endif			
                        glDrawElements ( GL_TRIANGLES, this->buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT, ( void* ) ( offset * sizeof ( ushort ) ) );
                        glPopMatrix();
                    }
                    
                     CViewer<TData,T,Dim>::render_outline();

            //if (!(mouse_state.point_select_on))
//             {
//                 float left=0;
//                 float right=CSceneRenderer::shape[0]*rescale;
//                 float top=0;
//                 float bottom=CSceneRenderer::shape[1]*rescale;
//                 float front=0;
//                 float back=CSceneRenderer::shape[2]*rescale;
//                 glLineWidth ( 1 );
// 
// 
//                 glDisable ( GL_CULL_FACE );
//                 glLineStipple ( 1, ( short ) 0x00FF );
//                 glEnable ( GL_LINE_STIPPLE );
//                 glPolygonMode ( GL_FRONT_AND_BACK,GL_LINE );
//                 //glColor3f(0.0f,0.0f,0.7f);
//                 glColor3fv ( this->particle_colors[CSceneRenderer::Corange] );
//                 glBegin ( GL_QUADS );
//                 glVertex3f ( left,top,back );
//                 glVertex3f ( left,top,front );
//                 glVertex3f ( left,bottom,front );
//                 glVertex3f ( left,bottom,back );
//                 glVertex3f ( right,top,back );
//                 glVertex3f ( right,top,front );
//                 glVertex3f ( right,bottom,front );
//                 glVertex3f ( right,bottom,back );
//                 glVertex3f ( left,bottom,back );
//                 glVertex3f ( right,bottom,back );
//                 glVertex3f ( right,top,back );
//                 glVertex3f ( left,top,back );
//                 glVertex3f ( left,bottom,front );
//                 glVertex3f ( right,bottom,front );
//                 glVertex3f ( right,top,front );
//                 glVertex3f ( left,top,front );
//                 glVertex3f ( left,top,front );
//                 glVertex3f ( left,top,back );
//                 glVertex3f ( right,top,back );
//                 glVertex3f ( right,top,front );
//                 glVertex3f ( left,bottom,front );
//                 glVertex3f ( left,bottom,back );
//                 glVertex3f ( right,bottom,back );
//                 glVertex3f ( right,bottom,front );
//                 glEnd();
//                 glPolygonMode ( GL_FRONT_AND_BACK,GL_FILL );
//                 glDisable ( GL_LINE_STIPPLE );
//                 glEnable ( GL_CULL_FACE );
//             }

        }
    }

    void render_focuspoint() {
        {
            MyMouse<TData,T,Dim> * mouse_state= ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state;
            glPushMatrix();
            glLoadIdentity();

            glTranslatef ( mouse_state->focus_point[0],mouse_state->focus_point[1],mouse_state->focus_point[2] );

            float scale=0.02*CViewer<TData,T,Dim>::zoom;
            int particle_typ=0;
            int color_id=CSceneRenderer::Corange;
            glColor3fv ( this->particle_colors[color_id] );
            glScalef ( scale,scale,scale );
            std::size_t offset=this->buffered_objects[particle_typ].i_offset;
// 	      cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
// 	      cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
            glDrawElements ( GL_TRIANGLES, this->buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT, ( void* ) ( offset * sizeof ( ushort ) ) );
            glPopMatrix();
        }
    }


public:



    void set_params(const mxArray * params=NULL)
    {

        
        if (params==NULL)
            return;
	try {
	  
	  
	MyMouse<TData,T,Dim> * mouse_state= ( MyMouse<TData,T,Dim> * ) CViewer<TData,T,Dim>::mouse_state;  
        if (mhs::mex_hasParam(params,"movie_folder")!=-1)
	{
            movie_folder=mhs::mex_getParamStr(params,"movie_folder");
	    printf("setting movie output folder to %s\n",movie_folder.c_str());
	    
	}
	if (mhs::mex_hasParam(params,"pausing")!=-1)
	{
            mouse_state->pause_program=mhs::mex_getParam<bool>(params,"pausing",1)[0];
	    
	}
	
	if (mhs::mex_hasParam(params,"scalethreshold")!=-1)
	{
            scalethreshold=mhs::mex_getParam<double>(params,"scalethreshold",1)[0];
            printf("##################");
            printf("##################");
            printf("##################");
	    printf("vis scale particle threshold to %f\n",scalethreshold);
            
	}
	
	if (mhs::mex_hasParam(params,"colorcooldown")!=-1)
	{
            colorcooldown=mhs::mex_getParam<double>(params,"colorcooldown",1)[0];
           
            
	}
	
	
	if (mhs::mex_hasParam(params,"constraint_loop_depth")!=-1)
	{
            constraint_loop_depth=mhs::mex_getParam<float>(params,"constraint_loop_depth",1)[0];
	}
	
	if (mhs::mex_hasParam(params,"activity_delay")!=-1)
	{
            activity_delay=mhs::mex_getParam<float>(params,"activity_delay",1)[0];
	}
		    

	
	
	 }catch  (mhs::STAError & error)
	  {
	  
	    throw error;
	  }

	
    }



    CTrackerRenderer ( std::size_t * existing_handle=NULL ) :  CViewer<TData,T,Dim> ( existing_handle ) {

        scalethreshold = -1;
        colorcooldown = 1;
        sta_assert_error ( ( CViewer<TData,T,Dim>::mouse_state ) !=NULL );
        delete CViewer<TData,T,Dim>::mouse_state;
        CViewer<TData,T,Dim>::mouse_state= new MyMouse<TData,T,Dim> ( this );

        sync_tick=0;
        last_tick=0;
	
	activity_delay=1;

        for ( int i=0; i<sliceview_extents*sliceview_extents; i++ ) {
            sliceview_slice[i]= ( i%2 ) ==0;
        }

        sliceview_textureID=GenerateTextureRectangle ( sliceview_slice,sliceview_extents,sliceview_extents );
	cam = NULL;
	movie_state=MOVIE_SATE::MOVIE_NOT_RECORDING;
	frame_flight=0;
	frame_user=0;
// 	recording_flight=false;
	recording_user=false;
	prev_user_recording_time=0;
	img_smooth=NULL;
	hessian=NULL;
	debug_patch_mode=0;
	constraint_loop_depth=25;
    }

    ~CTrackerRenderer() {

	if (cam!=NULL)
	  delete cam;

        glDeleteTextures ( 1,&sliceview_textureID );
    }


    bool is_pause_program() {
        return ( ( MyMouse<TData,T,Dim>* ) ( CViewer<TData,T,Dim>::mouse_state ) )->pause_program;
    }


      void load_cam_path(const mxArray * data)
      {
	  sta_assert_error(cam!=NULL);
	  try 
	  {
	  if (data==NULL)
	    return;
	  
	  mxArray * struct_array=mxGetField(data,0,"cam");
	  if (struct_array!=NULL)
	  {
	    printf("loading cam data\n");
	    cam->load(struct_array);
	  }
	 } catch (mhs::STAError & error)
            {
                throw error;
            }
      }


    void init ( const mxArray * feature_struct ) {
        if ( feature_struct==NULL ) {
            return;
        }

        printf ( "init CViewer\n" );
        CViewer<TData,T,Dim>::init ( feature_struct );

	if (cam!=NULL)
	  delete cam;
	cam = new CCam<TData,T,Dim>(CSceneRenderer::shape.v,*this);
	
	
	try 
        {
	    mhs::dataArray<TData> tmp1;
	    tmp1=mhs::dataArray<TData>(feature_struct,"alphas_img");
            img_smooth=tmp1.data;
            num_alphas_img_smooth=tmp1.dim[tmp1.dim.size()-1];
            printf("img smoothed pol degree: %d \n",num_alphas_img_smooth-1);

        }catch (mhs::STAError error)
        {
	  img_smooth=NULL;
        }
        
        
        try {
	    scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
	    num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
	}catch ( mhs::STAError error ) {
            scales=NULL;
        }
        
        try {
	  const TData * is_pol=mhs::dataArray<TData> ( feature_struct,"img_smooth_is_pol" ).data;
	  img_smooth_is_pol=(is_pol[0]>0);
	  
	}catch ( mhs::STAError error ) {
            img_smooth_is_pol=true;
        }
        
        if (scales==NULL)
	{
	 img_smooth_is_pol=true; 
	}
        
        try 
        {
	  mhs::dataArray<TData> tmp0;
	    tmp0=mhs::dataArray<TData>(feature_struct,"alphas_hessian");
            hessian=tmp0.data;
            sta_assert_error(tmp0.dim.size()==5);
            sta_assert_error(tmp0.dim[3]==6);
            num_alphas_hessian=tmp0.dim[4];
            printf("hessian  pol degree: %d \n",num_alphas_hessian-1);
	    
	} catch (mhs::STAError error)
        {
	   hessian=NULL;
        } 
        
    }



    void update_tracker_controls ( bool force_update=false ) {
      
#ifndef _OCTAVE_          
	CViewer< TData,T, Dim > :: update_tracker_controls (force_update);
        mhs::mex_dumpStringNOW();
        const mxArray * handle=mexGetVariablePtr ( "global", "tracker_window_controls" );
        if ( handle!=NULL ) {
            const mxArray * changed= mxGetField ( handle,0, ( char * ) ( "debug_changed" ) );
            if ( changed!=NULL ) {
                double * handle_p= ( double  * ) mxGetPr ( changed );
                if ( ( *handle_p ) ==1 ) {
                    ( *handle_p ) =2;
                    printf ( "debug controls have been updated\n" );


                    const mxArray *
                    parameter= mxGetField ( handle,0, ( char * ) ( "tracker_temp" ) );
                    if ( parameter!=NULL ) {
                        for ( int i=0; i<tracker_p.size(); i++ ) {
                            sta_assert_error ( tracker_p[i]!=NULL );
                            CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                            tracker.opt_temp=* ( double* ) mxGetPr ( parameter );
			    //tracker.dynamic_point_scale();
			    tracker.update_dynamic_point=true;
                        }
                    }




                    parameter= mxGetField ( handle,0, ( char * ) ( "edgecost" ) );
                    if ( parameter!=NULL ) {
                        for ( int i=0; i<tracker_p.size(); i++ ) {
                            sta_assert_error ( tracker_p[i]!=NULL );
                            CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                            sta_assert_error ( tracker.data_fun!=NULL );
                            tracker.edgecost_fun->read_controls ( handle );
                        }
                    }




                    parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
                    if ( parameter!=NULL ) {
                        for ( int i=0; i<tracker_p.size(); i++ ) {
                            sta_assert_error ( tracker_p[i]!=NULL );
                            CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                            sta_assert_error ( tracker.data_fun!=NULL );
                            tracker.data_fun->read_controls ( handle );
                        }
                    }
                }
            }


        }
#endif        
    }

    void update_tracker_gui ( int depth=0 ) {
        if ( depth>1 ) {
            printf ( "called to often\n" );
            return;
        }
#ifndef _OCTAVE_          
        mhs::mex_dumpStringNOW();
        const mxArray * handle=mexGetVariablePtr ( "global", "tracker_window_controls" );
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_temp" ) );
            if ( parameter!=NULL ) {
                double tmp=0;
                for ( int i=0; i<tracker_p.size(); i++ ) {
                    sta_assert_error ( tracker_p[i]!=NULL );
                    CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                    tmp=std::max ( tracker.opt_temp,tmp );
                }
                * ( double* ) mxGetPr ( parameter ) =tmp;
            }


            for ( int i=0; i<tracker_p.size(); i++ ) {
                sta_assert_error ( tracker_p[i]!=NULL );
                CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                sta_assert_error ( tracker.data_fun!=NULL );
                tracker.data_fun->set_controls ( handle );
            }


            for ( int i=0; i<tracker_p.size(); i++ ) {
                sta_assert_error ( tracker_p[i]!=NULL );
                CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                sta_assert_error ( tracker.edgecost_fun!=NULL );
                tracker.edgecost_fun->set_controls ( handle );
            }

            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                for ( int i=0; i<tracker_p.size(); i++ ) {
                    sta_assert_error ( tracker_p[i]!=NULL );
                    CTracker<TData,T,Dim> & tracker=*tracker_p[i];
                    sta_assert_error ( tracker.data_fun!=NULL );
// 	      printf("-> costs %f %f %f\n",tracker.options.particle_connection_state_cost_scale[0],
// 		      tracker.options.particle_connection_state_cost_scale[1],
// 		      tracker.options.particle_connection_state_cost_scale[2]
// 		    );
// 	      *(((double*) mxGetPr(parameter))+1)=tracker.options.particle_connection_state_cost_scale[0];

// 	      tracker.particle_connection_state_cost_scale[1]=tracker.particle_connection_state_cost_scale[1];
// 	      tracker.particle_connection_state_cost_scale[2]=tracker.particle_connection_state_cost_scale[1];
                }
            }


            mexCallMATLAB ( 0, NULL,0,NULL, "mhs_update_gui" );
        } else {
            printf ( "no tracker gui data avaliable, starting gui" );
            mexCallMATLAB ( 0, NULL,0,NULL, "start_gui" );
            update_tracker_gui ( depth++ );
        }
#endif        

    }

};



















#endif
