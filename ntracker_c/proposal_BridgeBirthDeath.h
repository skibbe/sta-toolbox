#ifndef PROPOSAL_BRIDGETBIRTHDEATH_H
#define PROPOSAL_BRIDGETBIRTHDEATH_H

#include "proposals.h"

#define BRIDGETBIRTHDEATH_SINGLE_PART


template<typename TData,typename T,int Dim> class CProposal;


// #define DEBUG_BRIDGE_CONNECT

template<typename TData,typename T,int Dim>
class CBridgeBirth : public CProposal<TData,T,Dim>
{
protected:

    T lambda;
    T particle_energy_change_costs;

    bool temp_dependent;
    bool use_saliency_map;
    bool consider_birth_death_ratio;
    bool sample_scale_temp_dependent;

public:
    T dynamic_weight (
        std::size_t num_edges,
        std::size_t num_particles,
        std::size_t num_connected_particles,
        std::size_t num_bifurcations,
        std::size_t num_terminals,
        std::size_t num_segments,
        T volume_img,
        T volume_particles
    ) {
        return CProposal<TData,T,Dim>::dynamic_weight_terminal ( num_edges,
                num_particles,
                num_connected_particles,
                num_bifurcations,
                num_terminals,
                num_segments,
                volume_img,
                volume_particles );
    }


    CBridgeBirth () : CProposal<TData,T,Dim>() {
        lambda=100;
        particle_energy_change_costs=update_energy_particle_costs ( lambda );
        temp_dependent=true;
        use_saliency_map=false;
        consider_birth_death_ratio=false;
        sample_scale_temp_dependent=false;
    }

    std::string get_name() {
        return enum2string ( PROPOSAL_TYPES::PROPOSAL_BRIDGE_BIRTH );
    };

    PROPOSAL_TYPES get_type() {
        return ( PROPOSAL_TYPES::PROPOSAL_BRIDGE_BIRTH );
    }


    void set_params ( const mxArray * params=NULL ) {
        sta_assert_error ( this->tracker!=NULL );

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
        particle_energy_change_costs=update_energy_particle_costs ( lambda );
        if ( params==NULL ) {
            return;
        }

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

        if ( mhs::mex_hasParam ( params,"lambda" ) !=-1 ) {
            lambda=mhs::mex_getParam<T> ( params,"lambda",1 ) [0];
        }

        particle_energy_change_costs=update_energy_particle_costs ( lambda );

        if ( mhs::mex_hasParam ( params,"temp_dependent" ) !=-1 ) {
            temp_dependent=mhs::mex_getParam<bool> ( params,"temp_dependent",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }

    }




    // saliency map
    void init ( const mxArray * feature_struct ) {
        sta_assert_error ( this->tracker!=NULL );
        if ( feature_struct==NULL ) {
            return;
        }

    }

    bool propose() {
        pool<class Points<T,Dim> > &	particle_pool		=* ( this->tracker->particle_pool );
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=* ( this->tracker->data_fun );
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=* ( this->tracker->tree );
        OctTreeNode<T,Dim> &		conn_tree		=* ( this->tracker->connecion_candidate_tree );
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=* ( this->tracker->edgecost_fun );
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
        Collision<T,Dim> &   collision_fun_p=* ( this->tracker->collision_fun );
	bool  single_scale		=(this->tracker->single_scale);


        bool debug=false;

        if ( sample_scale_temp_dependent && ( ! ( temp<maxscale ) ) ) {
            return false;
        }



        this->proposal_called++;


        /// randomly pic a terminal node
        if ( connections.get_num_terminals() >0 ) {


            Points<T,Dim> &  bridge_start_pt=connections.get_rand_point();
	    
	    

	    

            //T bridge_start_pt_cost=-bridge_start_pt.compute_cost3 ( temp );

            int connected_side= ( bridge_start_pt.endpoint_connections[0]==0 ) ? 1 :0;

#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
            sta_assert_error ( bridge_start_pt.get_num_connections() ==1 );
#else
            sta_assert_error ( ( bridge_start_pt.endpoint_connections[0]==0 ) != ( bridge_start_pt.endpoint_connections[1]==0 ) );
            sta_assert_error ( bridge_start_pt.endpoint_connections[connected_side]<3 );
            sta_assert_error ( bridge_start_pt.endpoint_connections[1-connected_side]==0 );
#endif

            int bridge_start_pt_terminal_side=1-connected_side;

            const class Points< T, Dim >::CEndpoint & bridge_start=*bridge_start_pt. endpoints[bridge_start_pt_terminal_side][0];

            class Points< T, Dim >::CEndpoint * bridge_end=NULL;
            T proposed_prob=-1;

            if ( debug ) {
                printf ( "0" );
            }

            if ( !do_tracking3 (
                        bridge_start,
                        conn_tree,
                        search_connection_point_center_candidate,
                        bridge_end,
                        proposed_prob
                    ) ) {
                if ( debug ) {
                    printf ( "\n" );
                }
                return false;
            }

            sta_assert_error ( proposed_prob>-1 );

            Points<T,Dim> &  bridge_end_pt=* ( bridge_end->point );

            Points<T,Dim> * bridge_pt_prt=particle_pool.create_obj();
            Points<T,Dim> & bridge_pt=*bridge_pt_prt;
            bridge_pt.tracker_birth=4;


            // compute optimal pos, orientation and scale
            Vector<T,Dim> 	optimal_pos= ( bridge_start_pt.get_position() +bridge_end_pt.get_position() ) /2;
            Vector<T,Dim> 	optimal_direction= ( bridge_end_pt.get_position()-bridge_start_pt.get_position() );
            optimal_direction.normalize();
            T 			optimal_scale= ( bridge_start_pt.get_scale() +bridge_end_pt.get_scale() ) /2;



            T temp_fact_pos=1;
            if ( temp_dependent )
                //temp_fact_pos=std::sqrt(temp);
            {
                temp_fact_pos=proposal_temp_trafo ( temp,optimal_scale,T ( 0.5 ) );
            }

            T temp_fact_scale=temp_fact_pos+1;
            T temp_fact_rot=0.5*temp_fact_pos;

            T point_create_prob=1;




            T position_sigma=temp_fact_pos* ( optimal_scale/2 );
            /// NEW POSITION
            Vector<T,Dim> randv;
            randv.rand_normal ( position_sigma );

            bridge_pt.set_position ( optimal_pos+randv );

            if ( debug ) {
                printf ( "1" );
            }


            if ( bridge_pt.out_of_bounds ( shape ) ) {
                particle_pool.delete_obj ( bridge_pt_prt );
                if ( debug ) {
                    printf ( "\n" );
                }
                return false;
            }


            // prepare edges
            class Connection<T,Dim> new_connections[2];
            class Connection<T,Dim> *  new_connection_pt[2];
            new_connections[0].pointA=bridge_start_pt.getfreehub ( bridge_start.side );
            new_connections[0].pointB=bridge_pt.getfreehub ( 1 );
            new_connections[0].edge_type=EDGE_TYPES::EDGE_SEGMENT;


            new_connections[1].pointA=bridge_end_pt.getfreehub ( bridge_end->side );
            new_connections[1].pointB=bridge_pt.getfreehub ( 0 );
            new_connections[1].edge_type=EDGE_TYPES::EDGE_SEGMENT;

            T max_edge_length=CEdgecost<T,Dim>::get_current_edge_length_limit();
            max_edge_length*=max_edge_length;

            if ( debug ) {
                printf ( "2" );
            }



            for ( int i=0; i<2; i++ ) {
                T dist2= ( ( *new_connections[i].pointA->position )- ( *new_connections[i].pointB->position ) ).norm2();
                if ( ! ( dist2<max_edge_length ) ) {
                    particle_pool.delete_obj ( bridge_pt_prt );
                    if ( debug ) {
                        printf ( "\n" );
                    }
                    return false;
                }
            }



            point_create_prob*=normal_dist<T,3> ( randv.v,position_sigma );

            /// NEW ORIENTATION
            bridge_pt.set_direction ( random_pic_direction<T,Dim> (
                                          optimal_direction,temp_fact_rot ) );

            point_create_prob*=normal_dist_sphere ( optimal_direction,bridge_pt.get_direction(),temp_fact_rot );
            // sign same as terminal node

	    if (!single_scale)
	    {
	      /// NEW SCALE
	      T scale_lower;
	      T scale_upper;
// 	      if ( sample_scale_temp_dependent ) {
// 
// 		  scale_lower=std::max ( std::max ( minscale,temp ),optimal_scale/temp_fact_scale );
// 		  scale_upper=std::min ( maxscale,optimal_scale*temp_fact_scale );
// 		  sta_assert_debug0 ( scale_lower<scale_upper );
// 	      } else {
// 		  scale_lower=std::max ( minscale,optimal_scale/temp_fact_scale );
// 		  scale_upper=std::min ( maxscale,optimal_scale*temp_fact_scale );
// 	      }
	      if ( sample_scale_temp_dependent ) {

		  scale_lower= std::max ( minscale,temp );
		  scale_upper=maxscale;
		  sta_assert_debug0 ( scale_lower<scale_upper );
	      } else {
		  scale_lower=minscale;
		  scale_upper=maxscale;
	      }


	      bridge_pt.set_scale ( myrand ( scale_lower,scale_upper ) );
	      point_create_prob*=1/ ( ( scale_upper-scale_lower ) +std::numeric_limits<T>::epsilon() );
	      
	    }else
	    {
	       bridge_pt.set_scale ( maxscale );
	    }


            T particle_interaction_cost_old=0;
            T particle_interaction_cost_new=0;


#ifdef  D_USE_GUI
#ifdef  DEBUG_BRIDGE_CONNECT
            //GuiPoints<T> * gps=(GuiPoints<T> *)&bridge_pt;
            GuiPoints<T> * gps= ( ( ( GuiPoints<T> * ) &bridge_end_pt )->is_selected ) ? ( ( GuiPoints<T> * ) &bridge_end_pt ) : ( ( GuiPoints<T> * ) &bridge_start_pt );
// 		  if ( gps->is_selected)
// 		  {
// 		  printf("pointcosts : %f\n",pointcosts);
// 		  }
#endif
#endif



            typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;


            T new_edgecost=0;

            if ( debug ) {
                printf ( "3" );
            }

            for ( int i=0; i<2; i++ ) {
                new_edgecost+=new_connections[i].e_cost (
                                  edgecost_fun,
                                  options.connection_bonus_L,
                                  options.bifurcation_bonus_L,
                                  inrange
                              );
                //connection.new_cost=connection.connection_new.compute_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,maxendpointdist,inrange);
                if ( inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) {
                    particle_pool.delete_obj ( bridge_pt_prt );
                    if ( debug ) {
                        printf ( "\n" );
                    }
                    return false;
                }
            }


            // at this point, the bridge_pt is invisibe
            // for the start and endpoint
            if ( debug ) {
                printf ( "4" );
            }

            if ( !collision_fun_p.is_soft() ) {
                if ( collision_fun_p.colliding (
                            particle_interaction_cost_new,
                            tree,
                            bridge_pt,
                            collision_search_rad,
                            temp ) ) {
		  
                    particle_pool.delete_obj ( bridge_pt_prt );
                    if ( debug ) {
                        printf ( "\n" );
                    }
                    return false;
                }
            }

            class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
            class Collision<T,Dim>::Candidates candA ( query_bufferA );

            class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
            class Collision<T,Dim>::Candidates candB ( query_bufferB );


// 	      class OctPoints<T,Dim> * query_bufferC[query_buffer_size];
// 	      class Collision<T,Dim>::Candidates candC(query_bufferC);

            int mode=1; // only special collision costs

            //############################
            // before adding edges
            //############################

            T point_cost_old=0;
            point_cost_old+=bridge_start_pt.compute_cost3 ( temp );
            point_cost_old+=bridge_end_pt.compute_cost3 ( temp );

            if ( collision_fun_p.is_soft() ) {
                T tmp;
                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       bridge_start_pt,
                                       collision_search_rad,
                                       temp,
                                       &candA,true,
                                       -9,mode  // collect colliding pts
                                   ) );
                particle_interaction_cost_old+=tmp;

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       bridge_end_pt,
                                       collision_search_rad,
                                       temp,
                                       &candB,true,
                                       -9,mode  // collect colliding pts
                                   ) );
                particle_interaction_cost_old+=tmp;
            }



            //adding edges, yea
            for ( int i=0; i<2; i++ ) {
                new_connection_pt[i]=NULL;
                new_connection_pt[i]=connections.add_connection ( new_connections[i] );
                sta_assert_debug0 ( new_connection_pt[i]!=NULL );
            }



            //############################
            // after adding edges
            //############################
            
            //NOTE bridge_pt is not in tree now, so only visible in his has_own
            // collision check
      

            T point_cost_new=0;
            point_cost_new+=bridge_start_pt.compute_cost3 ( temp );
            point_cost_new+=bridge_end_pt.compute_cost3 ( temp );
            point_cost_new+=bridge_pt.compute_cost3 ( temp );

            if ( debug ) {
                printf ( "5" );
            }
            if ( collision_fun_p.is_soft() ) {
                T tmp;
                if ( collision_fun_p.colliding (
                            tmp,
                            tree,
                            bridge_pt,
                            collision_search_rad,
                            temp ) ) {
		  
		  goto cleanup_particle_and_edges;
                }
                particle_interaction_cost_new+=tmp;
// 	      #ifdef  D_USE_GUI
// 		#ifdef  DEBUG_BRIDGE_CONNECT
//     		  if ( gps->is_selected)
//     		  {
// 		    printf("interaction bridge: %f\n",tmp);
//     		  }
// 		#endif
// 	      #endif



                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       bridge_start_pt,
                                       collision_search_rad,
                                       temp,
                                       &candA,false,
                                       -10,mode  // do not collect
                                   ) );
                particle_interaction_cost_new+=tmp;

// 	      #ifdef  D_USE_GUI
// 		#ifdef  DEBUG_BRIDGE_CONNECT
//     		  if ( gps->is_selected)
//     		  {
// 		    printf("interaction start: %f\n",tmp);
//     		  }
// 		#endif
// 	      #endif

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       bridge_end_pt,
                                       collision_search_rad,
                                       temp,
                                       &candB,false,
                                       -10,mode  // do not collect
                                   ) );
                particle_interaction_cost_new+=tmp;

// 	      	      #ifdef  D_USE_GUI
// 		#ifdef  DEBUG_BRIDGE_CONNECT
//     		  if ( gps->is_selected)
//     		  {
// 		    printf("interaction end: %f\n",tmp);
//     		  }
// 		#endif
// 	      #endif

            }

            {
            
            if ( debug ) {
                printf ( "6" );
            }
            T newpointsaliency=0;
            T newenergy_data=0;
            if	(
                (
                    ( !data_fun.saliency_get_value (
                          newpointsaliency,
                          bridge_pt.get_position() ) )
                    ||
                    ( newpointsaliency<std::numeric_limits<T>::min() )
                ) ||
                (
                    !data_fun.eval_data (
                        newenergy_data,
                        bridge_pt )
                )
            ) {
		goto cleanup_particle_and_edges;
	    }



            bridge_pt.point_cost=newenergy_data;
            bridge_pt.saliency=newpointsaliency;

	    {

            /// edge energy

            T  Eold= ( point_cost_old
                       +particle_interaction_cost_old

                     ) ;
            T  Enew= ( point_cost_new
                       +particle_interaction_cost_new
                       +new_edgecost
                       +newenergy_data
                     ) ;

            //if (debug)
            /*#ifdef  D_USE_GUI
            #ifdef  DEBUG_BRIDGE_CONNECT
            	  if ( gps->is_selected)
            	  {
            //
                 printf("( %f %f  pc[%f %f] pi[%f %f] {ec: %f , data: %f})\n",
            	    Eold,Enew,
            	    point_cost_old,point_cost_new,
            	    particle_interaction_cost_old,particle_interaction_cost_new,
            	    new_edgecost,newenergy_data);
            	  }
            #endif
                 #endif		*/
            //

            T E= ( Enew-Eold ) /temp;

            T R=mhs_fast_math<T>::mexp ( E );

            R*= ( connections.get_num_terminals() +T ( 2 ) ) / ( tree.get_numpts() +T ( 1 ) );

            R/=point_create_prob*proposed_prob
               * ( maxscale-minscale )
               +std::numeric_limits<T>::epsilon();

#ifdef  D_USE_GUI
#ifdef  DEBUG_BRIDGE_CONNECT
            if ( gps->is_selected ) {
                //if ( Enew<Eold ) 
		{
                    printf ( "( %f %f  pc[%f %f] pi[%f %f] {ec: %f , data: %f})\n",
                             Eold,Enew,
                             point_cost_old,point_cost_new,
                             particle_interaction_cost_old,particle_interaction_cost_new,
                             new_edgecost,newenergy_data );

                    T f0= ( connections.get_num_terminals() +T ( 2 ) ) / ( tree.get_numpts() +T ( 1 ) );

                    printf ( "R:%f  [%f / (%f %f %f )]\n",R,f0,proposed_prob,point_create_prob,( maxscale-minscale ) );
		    printf ( "E0 %f E1 %f\n",mhs_fast_math<T>::mexp ( E ),std::exp ( -E ));

                }
            }
#endif
#endif


            if ( consider_birth_death_ratio ) {
                if ( use_saliency_map ) {
                    R*= ( lambda ) *this->call_w_undo;
                    R/=std::numeric_limits<T>::epsilon() + ( tree.get_numpts() +1 ) * ( nvoxel*bridge_pt.saliency ) *this->call_w_do; //*cpoint.saliency;
                } else {
                    R*= ( lambda ) *this->call_w_undo;
                    R/=std::numeric_limits<T>::epsilon() + ( tree.get_numpts() +1 ) *this->call_w_do; //*cpoint.saliency;
                }
            }


            if ( R>=myrand ( 1 ) +std::numeric_limits<T>::epsilon() ) {

                if ( debug ) {
                    printf ( "7" );
                }
                if ( Constraints::constraint_hasloop_follow ( bridge_start_pt,new_connection_pt[0],options.constraint_loop_depth ) ) {
                    goto cleanup_particle_and_edges;
                }


                sta_assert_error ( !bridge_pt.has_owner() );


                sta_assert_debug0 ( bridge_start_pt.is_segment() );
		
#ifndef BRIDGETBIRTHDEATH_SINGLE_PART  		
                sta_assert_debug0 ( bridge_end_pt.is_segment() );
#endif		
                //TODO check not required, must be a segment
                if ( ( bridge_start_pt.endpoint_connections[0]>0 ) && ( bridge_start_pt.endpoint_connections[1]>0 ) ) {
                    bridge_start_pt.unregister_from_grid ( voxgrid_conn_can_id );
                }
                if ( ( bridge_end_pt.endpoint_connections[0]>0 ) && ( bridge_end_pt.endpoint_connections[1]>0 ) ) {
                    bridge_end_pt.unregister_from_grid ( voxgrid_conn_can_id );
                }
                if ( debug ) {
                    printf ( "8" );
                }
                if ( !tree.insert ( bridge_pt ) ) {
                   goto cleanup_particle_and_edges;
                }

                sta_assert_debug0 ( bridge_end_pt.has_owner() );



                this->tracker->update_energy ( E+particle_energy_change_costs,static_cast<int> ( get_type() ) );
                this->proposal_accepted++;


#ifdef  D_USE_GUI
                bridge_pt.touch ( get_type() );
                bridge_end_pt.touch ( get_type() );
                bridge_start_pt.touch ( get_type() );
#endif
		
		bridge_pt._path_id=bridge_end_pt._path_id;

                this->tracker->update_volume ( bridge_pt.get_scale() );

                if ( debug ) {
                    printf ( "accept\n" );
                }

                return true;
            }
	    }
	    }
            
            
            if ( debug ) {
                printf ( "9" );
            }

cleanup_particle_and_edges:	            
            for ( int i=0; i<2; i++ ) {
                connections.remove_connection ( new_connection_pt[i] );
            }
            particle_pool.delete_obj ( bridge_pt_prt );
            if ( debug ) {
                printf ( "\n" );
            }
            return false;
        }
        if ( debug ) {
            printf ( "10\n" );
        }
        return false;
    }


public:


    static bool  do_tracking3 (
        const class Points< T, Dim >::CEndpoint & bridge_start,
        OctTreeNode<T,Dim> & conn_tree,
        T search_connection_point_center_candidate,
        class Points< T, Dim >::CEndpoint * & proposed_bridge_end,
        T & proposed_prob,
	bool bridge_pt=true,
	bool debug=false
	
        
// 	,
// 	const class Points< T, Dim >::CEndpoint *  existing_bridge_end=NULL,
// 	T * existing_prob=NULL
    ) {

      
        T search_rad_scale= bridge_pt ? 2 : 1;
        const bool consider_dist=false;
        //bool debug=false;
	
	
// 	debug=true;
        if ( debug ) {
            printf ( "do_tracking\n" );
        }


        search_connection_point_center_candidate*=search_rad_scale;

        class Points<T,Dim> & bridge_start_point=*bridge_start.point;

        /*!
         collect all points in search rectangle
         */
        const Vector<T,Dim> & endpoint=*bridge_start.position;

        std::size_t found;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        try {
            conn_tree.queryRange (
                endpoint,
                search_connection_point_center_candidate,
                candidates,
                query_buffer_size,
                found );
        } catch ( mhs::STAError & error ) {
            throw error;
        }

        sta_assert_debug0 ( bridge_start_point.maxconnections>0 );

        /// no points nearby. no existing bridge, do nothing
        if ( ( found==1 ) && ( query_buffer[0]==&bridge_start_point ) && ( proposed_bridge_end==NULL ) ) {
            if ( debug ) {
                printf ( "no points\n",found );
            }
            return false;
        }


        T bridge_start_scale=bridge_start_point.get_scale();

        T current_edge_lengths=CEdgecost<T,Dim>::get_current_edge_length_limit();

        T searchrad=CEdgecost<T,Dim>::get_total_edge_length_limit();


        searchrad*=search_rad_scale;

        if ( debug ) {
            printf ( "found %d initial candidates in radius %f\n",found,searchrad );
        }


        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        if ( debug ) {
            printf ( "collecting endpoints in circle\n" );
        }


        class Points< T, Dim >::CEndpoint * connet_candidates_buffer[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** connet_candidates=connet_candidates_buffer;

        T connet_prop[query_buffer_size];
        T connet_prop_acc[query_buffer_size];

        std::size_t num_connet_candidates=0;

        //if (existing_bridge_end!=NULL)
        if ( proposed_bridge_end!=NULL ) {
	  
	    if ( debug ) {
	      printf ( "computing prob for existing edge\n" );
	    }
	  
            const class Points< T, Dim >::CEndpoint *  existing_bridge_end=proposed_bridge_end;
            if ( found> ( query_buffer_size-2 ) ) {
	      proposed_prob=-1;
                return false;
            }
            
#ifndef BRIDGETBIRTHDEATH_SINGLE_PART
            //NOTE for a bridge, was a terminal before, right?
             sta_assert_debug0((proposed_bridge_end->point->is_segment()));
#endif	    

            Vector<T,Dim> & candidate_endpoint=*existing_bridge_end->position;

            T facing_pts=1- ( bridge_start.get_slot_direction().dot ( existing_bridge_end->get_slot_direction() ) );

            if ( facing_pts>0 ) {
                T candidate_dist_sq= ( endpoint-candidate_endpoint ).norm2();
                if ( searchrad>candidate_dist_sq ) {
                    T bridge_end_scale=existing_bridge_end->point->get_scale();
                    T avg_scale= ( bridge_end_scale+bridge_start_scale ) /2;
                    T bridge_thickness=Points<T,Dim>::predict_thickness_from_scale ( avg_scale );

                    //if ( bridge_thickness*bridge_thickness>candidate_dist_sq ) {
		    //if ((!bridge_pt)||( ! ( bridge_thickness*bridge_thickness>candidate_dist_sq )) ) {
		    if ((bridge_pt)&&( ! ( bridge_thickness*bridge_thickness>candidate_dist_sq )) ) {
  
		      
                        //*existing_prob=-1;
                        proposed_prob=-1;
                        return false;
                    }


                    T score;
                    if ( consider_dist ) {

                        T optimal_length=2* ( current_edge_lengths+bridge_thickness );
                        T dist= ( mhs_fast_math<T>::sqrt ( candidate_dist_sq )- ( optimal_length ) ) /avg_scale;
                        score=facing_pts*mhs_fast_math<T>::mexp ( dist*dist );
                    } else {
                        score=facing_pts;
                    }
                    connet_prop[num_connet_candidates]=score;
                    connet_prop_acc[num_connet_candidates]=score;
                    num_connet_candidates++;
		    
		    if ( debug ) {
		    printf ( "adding existing candidate (total number now is  %d) \n" ,num_connet_candidates );
		  }
                } else
		{
		  if ( debug ) {
		    printf ( "out of range!!!!\n");
		  } 
		  
		  proposed_prob=-1;
		  return false;
// 		 sta_assert_error_c(1==0,
// 				    {printf("ERROR :: ! (%f  > %f )\n",searchrad,candidate_dist_sq);}
// 		); 
		}
            } else {
	       if ( debug ) {
		printf ( "non facing pts (%f)\n",facing_pts );
	      }
                //*existing_prob=-1;
                proposed_prob=-1;
                return false;
            }
        }





        {

            {
                /*!
                check for all candidates if endpoints are within the circle
                    with radius "searchrad"
                */
                for ( std::size_t a=0; a<found; a++ ) {
                    Points<T,Dim> * point= ( Points<T,Dim> * ) candidates[a];

                    if ( point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {
                        continue;
                    }
                    
#ifndef BRIDGETBIRTHDEATH_SINGLE_PART                    
                    if ( !point->is_terminal()) {
                        continue;
                    }
#endif

//                     if ((point!=&npoint) // no self connections ;-)
// 		       &&((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))) // no bifurcation poins
#ifdef _BIF_CD_REQUIRES_2_ALL_CONNECT_
                    if ( ( point!=&bridge_start_point ) // no self connections ;-)
                            && ( ( point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION ) ) ) // no bifurcation poins
#else
                    if ( point!=&bridge_start_point ) // no self connections ;-)
#endif
                    {
                        //TODO might take into coinsideration:
                        //would always happensif I would use endpoint position
                        //instead of point posiiton because queryRange already supports sphere selection
                        //
                        T candidate_center_dist_sq= ( Vector<T,Dim> ( point->get_position() )-bridge_start_point.get_position() ).norm2();
                        if ( search_connection_point_center_candidate>candidate_center_dist_sq ) {
			  
			  if ((!bridge_start_point.is_connected_with(point)))
			  {
                            for ( int pointside=0; pointside<=1; pointside++ ) {
                                // do not connect with particle sides which are modeling a bifurcation
			      
                                // or are already occupied with a connection
                                if (( point->endpoint_connections[pointside]==0 )) {
                                    Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
                                    ///! updating endpoints (not done automatically)
// 				  point->update_endpoint(pointside);

                                    class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub ( pointside );

                                    T facing_pts=1- ( bridge_start.get_slot_direction().dot ( freehub->get_slot_direction() ) );
// 				  T facing_pts=point->get_direction().dot(bridge_start_point.get_direction());
// 				  facing_pts*=-(bridge_start.get_slot_sign()*candidate_endpoint.get_slot_sign());
// 				  facing_pts+=1;

                                    if ( facing_pts>0 ) {
                                        T candidate_dist_sq= ( endpoint-candidate_endpoint ).norm2();
                                        if ( debug ) {
                                            printf ( "endpointdist: %f\n",std::sqrt ( candidate_dist_sq ) );
                                        }
                                        /*!
                                        if not in radius remove
                                          point from further consideration
                                             */
                                        if ( searchrad>candidate_dist_sq ) {

                                            /*!
                                              if there are no free connections, remove
                                            endpoint from further consideration
                                            */
                                            if ( freehub!=NULL ) {
                                                T bridge_end_scale=freehub->point->get_scale();
                                                T avg_scale= ( bridge_end_scale+bridge_start_scale ) /2;
                                                T bridge_thickness=Points<T,Dim>::predict_thickness_from_scale ( avg_scale );
                                                if ((!bridge_pt)||( ! ( bridge_thickness*bridge_thickness>candidate_dist_sq )) ) {




                                                    sta_assert_debug0 ( num_connet_candidates<query_buffer_size );
                                                    ( *connet_candidates++ ) =freehub;
                                                    T score;

                                                    if ( consider_dist ) {

                                                        T optimal_length=2* ( current_edge_lengths+bridge_thickness );
                                                        T dist= ( mhs_fast_math<T>::sqrt ( candidate_dist_sq )- ( optimal_length ) ) /avg_scale;
                                                        score=facing_pts*mhs_fast_math<T>::mexp ( dist*dist );

                                                    } else {
                                                        score=facing_pts;
                                                    }
                                                    
                                                    if (debug)
						    {
						      printf("%d %f\n",a,score);
						    }
                                                    connet_prop[num_connet_candidates]=score;

                                                    if ( num_connet_candidates==0 ) {
                                                        connet_prop_acc[num_connet_candidates]=score;
                                                    } else {
                                                        connet_prop_acc[num_connet_candidates]=connet_prop_acc[num_connet_candidates-1]+score;
                                                    }


                                                    num_connet_candidates++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
		    }
                    }
                }





// 		if (existing_bridge_end!=NULL)
                if ( proposed_bridge_end  !=NULL ) {
                    sta_assert_debug0 ( num_connet_candidates>0 );
                    //*existing_prob=connet_prop[0]/connet_prop_acc[num_connet_candidates-1];
                    proposed_prob=connet_prop[0]/connet_prop_acc[num_connet_candidates-1];


                } else {

                    if ( num_connet_candidates<1 ) {
                        return false;
                    }

                    std::size_t connection_candidate=rand_pic_array ( connet_prop_acc,num_connet_candidates,connet_prop_acc[num_connet_candidates-1] );

                    if ( debug ) {
                        printf ( "choosing %d\n",connection_candidate );
                    }

                    proposed_prob=connet_prop[connection_candidate]/connet_prop_acc[num_connet_candidates-1];
                    proposed_bridge_end=connet_candidates_buffer[connection_candidate];
		    
		    
		    //NOTE we did a lazy check before
		    if (Connections<T,Dim>::connection_exists(bridge_start_point,*(proposed_bridge_end->point)))
		    {
		      return false;
		    }

                }

                return true;
            }
        }


    };


};

template<typename TData,typename T,int Dim>
class CBridgeDeath : public CProposal<TData,T,Dim>
{
protected:
    T lambda;
    T particle_energy_change_costs;
    bool use_saliency_map;
    bool consider_birth_death_ratio;
    bool temp_dependent;
    bool sample_scale_temp_dependent;
public:

    T dynamic_weight (
        std::size_t num_edges,
        std::size_t num_particles,
        std::size_t num_connected_particles,
        std::size_t num_bifurcations,
        std::size_t num_terminals,
        std::size_t num_segments,
        T volume_img,
        T volume_particles
    ) {
        return CProposal<TData,T,Dim>::dynamic_weight_terminal ( num_edges,
                num_particles,
                num_connected_particles,
                num_bifurcations,
                num_terminals,
                num_segments,
                volume_img,
                volume_particles );
    }

    CBridgeDeath() : CProposal<TData,T,Dim>() {
        lambda=100;
        particle_energy_change_costs=update_energy_particle_costs ( lambda );
        temp_dependent=true;

        use_saliency_map=false;
        consider_birth_death_ratio=false;
        sample_scale_temp_dependent=false;
    }

    std::string get_name() {
        return enum2string ( PROPOSAL_TYPES::PROPOSAL_BRIDGE_DEATH );
    };

    PROPOSAL_TYPES get_type() {
        return ( PROPOSAL_TYPES::PROPOSAL_BRIDGE_DEATH );
    }


    void set_params ( const mxArray * params=NULL ) {
        sta_assert_error ( this->tracker!=NULL );

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
        particle_energy_change_costs=update_energy_particle_costs ( lambda );
        if ( params==NULL ) {
            return;
        }

        // check if proposal parameters exist
        // if (mhs::mex_hasParam(params,"randwalk")!=-1)
        // randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

        if ( mhs::mex_hasParam ( params,"lambda" ) !=-1 ) {
            lambda=mhs::mex_getParam<T> ( params,"lambda",1 ) [0];
        }

        particle_energy_change_costs=update_energy_particle_costs ( lambda );

        if ( mhs::mex_hasParam ( params,"temp_dependent" ) !=-1 ) {
            temp_dependent=mhs::mex_getParam<bool> ( params,"temp_dependent",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }
    }

    // saliency map
    void init ( const mxArray * feature_struct ) {
        sta_assert_error ( this->tracker!=NULL );
        if ( feature_struct==NULL ) {
            return;
        }
    }

    bool propose() {
        pool<class Points<T,Dim> > &	particle_pool		=* ( this->tracker->particle_pool );
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=* ( this->tracker->data_fun );
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=* ( this->tracker->tree );
        OctTreeNode<T,Dim> &		conn_tree		=* ( this->tracker->connecion_candidate_tree );
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=* ( this->tracker->edgecost_fun );
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
        Collision<T,Dim> &   collision_fun_p=* ( this->tracker->collision_fun );
	bool  single_scale		=(this->tracker->single_scale);
	

        this->proposal_called++;


        OctPoints<T,Dim> *  cpoint;

        ///randomly choose a point
        cpoint=tree.get_rand_point();

        if ( cpoint==NULL ) {
            return false;
        }


        Points<T,Dim> &  bridge_pt= * ( ( Points<T,Dim>* ) ( cpoint ) );

        if ( bridge_pt.isfreezed() ) {
            return false;
        }

        if ( bridge_pt.is_protected_topology() ) {
            return false;
        }

        /// check if point has exactly one connection / side
        if ( !bridge_pt.is_segment() ) {
            return false;
        }

        //only segments
        if ( bridge_pt.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT ) {
            return false;
        }
        
	if (sample_scale_temp_dependent && ((temp>bridge_pt.get_scale())))
	{
	      return false; 
	}

        class Connection<T,Dim> *  old_connection_pt[2];
        class Connection<T,Dim> old_connections[2];
        for ( int i=0; i<2; i++ ) {
            sta_assert_debug0 ( bridge_pt.endpoints[i][0]->connected!=NULL );
            old_connection_pt[i]=bridge_pt.endpoints[i][0]->connection;
            if ( old_connection_pt[i]->is_protected_topology() ) {
                return false;
            }
            old_connections[i]=*old_connection_pt[i];
        }

        class Points< T, Dim >::CEndpoint & bridge_start=*bridge_pt.endpoints[0][0]->connected;
        class Points< T, Dim >::CEndpoint & bridge_end=*bridge_pt.endpoints[1][0]->connected;

        Points<T,Dim> &  bridge_start_pt=* ( bridge_start.point );
        Points<T,Dim> &  bridge_end_pt=* ( bridge_end.point );

#ifndef BRIDGETBIRTHDEATH_SINGLE_PART  		
	if ( ( !bridge_start_pt.is_segment() ) || ( !bridge_end_pt.is_segment() ) ) {
            return false;
        }
#else 
	if  ( !bridge_start_pt.is_segment() )  {
            return false;
        }
#endif	

      



        class Points< T, Dim >::CEndpoint * bridge_end_ptr=&bridge_end;
        T proposed_prob=-1;

        if ( !CBridgeBirth<TData,T,Dim>::do_tracking3 (
                    bridge_start,
                    conn_tree,
                    search_connection_point_center_candidate,
                    bridge_end_ptr,
                    proposed_prob
                ) ) {
            sta_assert_debug0 ( bridge_end_ptr==&bridge_end );
            return false;
        }

        sta_assert_debug0 ( bridge_end_ptr==&bridge_end );



        // compute optimal pos, orientation and scale
        Vector<T,Dim> 	optimal_pos= ( bridge_start_pt.get_position() +bridge_end_pt.get_position() ) /2;
        Vector<T,Dim> 	optimal_direction= ( bridge_end_pt.get_position()-bridge_start_pt.get_position() );
        optimal_direction.normalize();
        T 			optimal_scale= ( bridge_start_pt.get_scale() +bridge_end_pt.get_scale() ) /2;



        T temp_fact_pos=1;
        if ( temp_dependent )
            //temp_fact_pos=std::sqrt(temp);
        {
            temp_fact_pos=proposal_temp_trafo ( temp,optimal_scale,T ( 0.5 ) );
        }

        T temp_fact_scale=temp_fact_pos+1;
        T temp_fact_rot=0.5*temp_fact_pos;

        T point_create_prob=1;

        T position_sigma=temp_fact_pos* ( optimal_scale/2 );

        point_create_prob*=normal_dist<T,3> ( ( bridge_pt.get_position()-optimal_pos ).v ,position_sigma );
        point_create_prob*=normal_dist_sphere ( optimal_direction,bridge_pt.get_direction(),temp_fact_rot );

	
	if (!single_scale)
	{
	  /// NEW SCALE
	  T scale_lower;
	  T scale_upper;
// 	  if ( sample_scale_temp_dependent ) {
// 
// 	      scale_lower=std::max ( std::max ( minscale,temp ),optimal_scale/temp_fact_scale );
// 	      scale_upper=std::min ( maxscale,optimal_scale*temp_fact_scale );
// 	      sta_assert_debug0 ( scale_lower<scale_upper );
// 	  } else {
// 	      scale_lower=std::max ( minscale,optimal_scale/temp_fact_scale );
// 	      scale_upper=std::min ( maxscale,optimal_scale*temp_fact_scale );
// 	  }
	      if ( sample_scale_temp_dependent ) {
		  scale_lower= std::max ( minscale,temp );
		  scale_upper=maxscale;
		  sta_assert_debug0 ( scale_lower<scale_upper );
	      } else {
		  scale_lower=minscale;
		  scale_upper=maxscale;
	      }

	  point_create_prob*=1/ ( ( scale_upper-scale_lower ) +std::numeric_limits<T>::epsilon() );
	}


        T particle_interaction_cost_old=0;
        T particle_interaction_cost_new=0;



        class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
        class Collision<T,Dim>::Candidates candA ( query_bufferA );

        class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
        class Collision<T,Dim>::Candidates candB ( query_bufferB );


        int mode=1; // only special collision costs


        //############################
        // before removing edges
        //############################

        T point_cost_old=0;
        point_cost_old+=bridge_start_pt.compute_cost3 ( temp );
        point_cost_old+=bridge_end_pt.compute_cost3 ( temp );
        point_cost_old+=bridge_pt.compute_cost3 ( temp );


        if ( collision_fun_p.is_soft() ) {

            T tmp;
            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   bridge_pt,
                                   collision_search_rad,
                                   temp ) );

            particle_interaction_cost_old+=tmp;

	    //NOTE we hide the bridge point from the tree
	    
	    
            std::size_t  npts_old=tree.get_numpts();
            bridge_pt.unregister_from_grid ();
            std::size_t  npts_new=tree.get_numpts();
            sta_assert_debug0 ( npts_new+1==npts_old );


            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   bridge_start_pt,
                                   collision_search_rad,
                                   temp,
                                   &candA,true,
                                   -9,mode  // collect colliding pts
                               ) );
            particle_interaction_cost_old+=tmp;

            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   bridge_end_pt,
                                   collision_search_rad,
                                   temp,
                                   &candB,true,
                                   -9,mode  // collect colliding pts
                               ) );
            particle_interaction_cost_old+=tmp;
        }

        T old_energy_data=bridge_pt.point_cost;


        typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;

        T old_edgecost=0;

        for ( int i=0; i<2; i++ ) {
            old_edgecost+=old_connections[i].e_cost (
                              edgecost_fun,
                              options.connection_bonus_L,
                              options.bifurcation_bonus_L,
                              inrange
                          );
            //connection.new_cost=connection.connection_new.compute_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,maxendpointdist,inrange);
            sta_assert_error ( ( inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );

        }


        //removing edges, yea
        for ( int i=0; i<2; i++ ) {
            connections.remove_connection ( old_connection_pt[i] );
        }

        T point_cost_new=0;
        point_cost_new+=bridge_start_pt.compute_cost3 ( temp );
        point_cost_new+=bridge_end_pt.compute_cost3 ( temp );


        if ( collision_fun_p.is_soft() ) {
            T tmp;
            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   bridge_start_pt,
                                   collision_search_rad,
                                   temp,
                                   &candA,false,
                                   -10,mode  // do not collect
                               ) );
            particle_interaction_cost_new+=tmp;

            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   bridge_end_pt,
                                   collision_search_rad,
                                   temp,
                                   &candB,false,
                                   -10,mode  // do not collect
                               ) );
            particle_interaction_cost_new+=tmp;
        }



        //ADD BRIDGET to tree if failes

        T  Eold= ( point_cost_old
                   +particle_interaction_cost_old
                   +old_edgecost
                   +old_energy_data
                 ) ;
        T  Enew= ( point_cost_new
                   +particle_interaction_cost_new
                 ) ;


        T E= ( Enew-Eold ) /temp;
        T R=mhs_fast_math<T>::mexp ( E );

        R*=T ( tree.get_numpts() + ( ( collision_fun_p.is_soft() ) ) ) /T ( connections.get_num_terminals() );

        R*=point_create_prob*proposed_prob
           * ( maxscale-minscale );

        if ( consider_birth_death_ratio ) {
            if ( use_saliency_map ) {
                R*= ( tree.get_numpts() + ( collision_fun_p.is_soft() ) ) * ( nvoxel*bridge_pt.saliency ) *this->call_w_undo;
                R/=std::numeric_limits<T>::epsilon() + ( lambda ) *this->call_w_do;
            } else {
                R*= ( tree.get_numpts() + ( collision_fun_p.is_soft() ) ) *this->call_w_undo;
                R/= ( lambda ) *this->call_w_do;
            }
        }


        if ( R>=myrand ( 1 ) +std::numeric_limits<T>::epsilon() ) {
            this->tracker->update_energy ( E-particle_energy_change_costs,static_cast<int> ( get_type() ) );
            this->proposal_accepted++;

            this->tracker->update_volume ( -bridge_pt.get_scale() );


            Points<T,Dim> * bridge_pt_prt=&bridge_pt;
            particle_pool.delete_obj ( bridge_pt_prt );
	    
	    
	    Points<T,Dim> * bridge_pt_end_prt=&bridge_end_pt;
	    Points<T,Dim> * bridge_pt_start_prt=&bridge_start_pt;
	    
#ifndef BRIDGETBIRTHDEATH_SINGLE_PART  		    
	    if ((!conn_tree.insert(*bridge_pt_end_prt))||(!conn_tree.insert(*bridge_pt_start_prt)))
	    {
		throw mhs::STAError("error insering points\n");
		return false;
	    } 
#else
	    if ((!conn_tree.insert(*bridge_pt_end_prt,true))||(!conn_tree.insert(*bridge_pt_start_prt)))
	    {
		throw mhs::STAError("error insering points\n");
		return false;
	    } 

#endif

#ifdef  D_USE_GUI
            bridge_end_pt.touch ( get_type() );
            bridge_start_pt.touch ( get_type() );
#endif

            //this->tracker->update_energy(E-particle_energy_change_costs,static_cast<int>(get_type()));
            //this->proposal_accepted++;

            return true;
        }

        if ( collision_fun_p.is_soft() ) {
            sta_assert_error ( tree.insert ( bridge_pt ) );
        }
        for ( int i=0; i<2; i++ ) {
            connections.add_connection ( old_connections[i] );
        }

        return false;

    }
};


#endif


