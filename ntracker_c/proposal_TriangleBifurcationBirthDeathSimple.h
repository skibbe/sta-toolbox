#ifndef PROPOSAL_BIFURCATION_BD_SIMPLE_H
#define PROPOSAL_BIFURCATION_BD_SIMPLE_H

#include "proposals.h"
#include "edges.h"
#include "mhs_graphics.h"

//FIXME: constraint_hasloop_follow: ensure that bifurcation point is pointB in edge


template<typename TData,typename T,int Dim> class CProposal;

template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathSimple;
template<typename TData,typename T,int Dim>
class CProposalBifurcationReconn;
template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathSplit;
template<typename TData,typename T,int Dim>
 class CProposalBifurcationCrossingDeath;





template<typename TData,typename T,int Dim>
class CProposalBifurcationBirthSplit : public CProposal<TData,T,Dim>
{
    friend class CProposalBifurcationDeathSplit<TData,T,Dim>;

protected:
    T Lprior;
    T ConnectEpsilon;

    const static int max_stat=10;
    int statistic[max_stat];
public:
  
     T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }

    CProposalBifurcationBirthSplit(): CProposal<TData,T,Dim>()
    {
	ConnectEpsilon=0.0001;
        Lprior=-10;
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
    }

    ~CProposalBifurcationBirthSplit()
    {
//  printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("(%d)%d ",i,statistic[i]);
// 	printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X);
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
         pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
      
        this->proposal_called++;
      
	int stat_count=0;
	//0
	statistic[stat_count++]++;
	 
	class Connection<T,Dim> * edge;
	
        /// uniformly pic edge
        edge=connections.get_rand_edge();
        if (edge==NULL)
            return false;
        
        if (edge->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
	  return false;
	
	int side=std::rand()%2;// NOTE not necessary I guess
	
	//class Points< T, Dim >::CEndpoint
	class Points<T,Dim>::CEndpoint * bifurcation_candidate_epoints[3];
	
	bifurcation_candidate_epoints[0]=edge->points[side];
	bifurcation_candidate_epoints[1]=edge->points[1-side];
	
	// check if points are within a segment
	for (int a=0;a<2;a++)
	if (bifurcation_candidate_epoints[a]->point->get_num_connections()!=2)
	  return false;
	
	// check if point is part of a bifurcation
	for (int a=0;a<2;a++)
	{
	    if (bifurcation_candidate_epoints[a]->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	      return false;
	
	    if (!options.bifurcation_neighbors)
	    {
	      if (bifurcation_candidate_epoints[a]->opposite_slots[0]->connected->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
		return false;
	    }
	}
	
	

	
	
	try 
	{
	  //1
	  statistic[stat_count++]++;
	  //new terminal particle connected side
	  
	  
	   
	    
	    
	  
	  
	 //class Points<T,Dim>::CEndpoint * 
	 bifurcation_candidate_epoints[2]=NULL;
	 T new_edgecost;
	 T new_edge_prop;
	 
	  
 
	  if (!do_tracking(
        *bifurcation_candidate_epoints[0],
	*bifurcation_candidate_epoints[1],
	shape,
        tree,
        collision_search_rad,
	maxendpointdist,
        nvoxel,
	search_connection_point_center_candidate,
        edgecost_fun,
        options,
	Lprior,
	ConnectEpsilon,
	&bifurcation_candidate_epoints[2],
        &new_edgecost,
	&new_edge_prop))
	    return false;
	    
	    
	/*if  (bifurcation_candidate_epoints[2]->connection->is_protected_topology())
	{
	  return false;
	}*/	    
	    
	  sta_assert_debug0(bifurcation_candidate_epoints[2]->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);	    
	 if (!options.bifurcation_neighbors)
	    {
	      sta_assert_debug0(bifurcation_candidate_epoints[2]->opposite_slots[0]->connected!=NULL);
	      if (bifurcation_candidate_epoints[2]->opposite_slots[0]->connected->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
		return false;
	    }    

	    

	   if ((Connections<T,Dim>::connection_exists(*(bifurcation_candidate_epoints[0]->point),*(bifurcation_candidate_epoints[2]->point)))||
	      (Connections<T,Dim>::connection_exists(*(bifurcation_candidate_epoints[1]->point),*(bifurcation_candidate_epoints[2]->point)))
	    )
	    {
		return false;
	    }
	    
	    
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;    
	T old_edgecost=bifurcation_candidate_epoints[0]->connection->e_cost(
	  edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	old_edgecost+=bifurcation_candidate_epoints[2]->connection->e_cost(
	  edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));    
	
	class Points<T,Dim>::CEndpoint * new_terminal_ep=bifurcation_candidate_epoints[2]->connected;
	
	sta_assert_debug0(!new_terminal_ep->point->is_terminal());
	
	
	T particle_costs=0;
	particle_costs-=bifurcation_candidate_epoints[0]->point->compute_cost3(temp); //old segment point A
	particle_costs-=bifurcation_candidate_epoints[1]->point->compute_cost3(temp); //old segment point B
	particle_costs-=bifurcation_candidate_epoints[2]->point->compute_cost3(temp); //old segment point C
	particle_costs-=new_terminal_ep->point->compute_cost3(temp); //old segment point C
	
	
	class Connection<T,Dim> old_connections_backup[2];
	// remove bifurcation connections
	for (int slot=0;slot<2;slot++)
	{
	  Connection<T,Dim> * delete_me=bifurcation_candidate_epoints[slot+1]->connection;
	  old_connections_backup[slot]=*delete_me;
	  connections.remove_connection(delete_me);
	}
	
	Points<T,Dim> * p_b_particle=particle_pool.create_obj();    
	Points<T,Dim> & b_particle=*p_b_particle;
	b_particle.tracker_birth=3;
	b_particle.set_direction(Vector<T,Dim>(1,0,0));
	
	class Connection<T,Dim> * new_connections[3];
    
	sta_assert_debug0(*bifurcation_candidate_epoints[0]->endpoint_connections==0);
	sta_assert_debug0(*bifurcation_candidate_epoints[1]->endpoint_connections==0);
	sta_assert_debug0(*bifurcation_candidate_epoints[2]->endpoint_connections==0);
	
	class Connection<T,Dim> a_new_connection;
	a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
	
	///create the three edges
	for (int i=0;i<3;i++)
	{
	  a_new_connection.pointA=b_particle.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	  Points<T,Dim> * pointB=bifurcation_candidate_epoints[i]->point;
	  a_new_connection.pointB=pointB->getfreehub(bifurcation_candidate_epoints[i]->side);
	  sta_assert_debug0(a_new_connection.pointB->point!=a_new_connection.pointA->point);
	  new_connections[i]=connections.add_connection(a_new_connection);
	}
	
	
/*printf("2");*/	
	
	bool new_terminal_is_registered_to_conn_tree=new_terminal_ep->point->has_owner(voxgrid_conn_can_id);
	
	if (!new_terminal_is_registered_to_conn_tree)
	{
	  if (!conn_tree.insert(*new_terminal_ep->point))
	  {
	      connections.remove_connection(new_connections[0]);
	      connections.remove_connection(new_connections[1]);
	      connections.remove_connection(new_connections[2]);
	      connections.add_connection(old_connections_backup[0]);
	      connections.add_connection(old_connections_backup[1]);
	      particle_pool.delete_obj(p_b_particle);
// 	      new_terminal_ep->point->print();
// 	      throw mhs::STAError("error insering point\n");
	      return false;
	  }
	  
// 	  if (!conn_tree.insert(*new_terminal_ep->point,true))
// 	  {
// 	      connections.remove_connection(new_connections[0]);
// 	      connections.remove_connection(new_connections[1]);
// 	      connections.remove_connection(new_connections[2]);
// 	      connections.add_connection(old_connections_backup[0]);
// 	      connections.add_connection(old_connections_backup[1]);
// 	      new_terminal_ep->point->print();
// 	      throw mhs::STAError("error insering point\n");
// 	      return false;
// 	  }
	}
	
	
	
	
	
	sta_assert_debug0(new_terminal_ep->point->get_num_connections()==1);
        sta_assert_debug0(new_terminal_ep->point->is_terminal());
	
	
	typename CProposalBifurcationDeathSplit<TData,T,Dim>::T_Edge_candidate existing_connections;
	existing_connections.seg_A1=new_terminal_ep;
	existing_connections.seg_A2=bifurcation_candidate_epoints[2];
	existing_connections.seg_B1=bifurcation_candidate_epoints[0];
	existing_connections.seg_B2=bifurcation_candidate_epoints[1];
	T new_edgecost_2;
	T old_edgecost_2;
	T old_edge_prop;

// printf("3");	
// 	new_terminal_ep->point->update_endpoints();
// 	new_terminal_ep->point->update_endpoints();
// 	printf("%f %f");
	
	
// 	{
// 	  std::size_t found;
// 	  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
// 	  class OctPoints<T,Dim> ** candidates;
// 	  candidates=query_buffer;
// 
// 	  try {
// 	      conn_tree.queryRange(
// 		  b_particle.position,
// 		  search_connection_point_center_candidate,
// 		  candidates,
// 		  query_buffer_size,
// 		  found);
// 	  } catch (mhs::STAError & error)
// 	  {
// 	      throw error;
// 	  }
// 	  
// 	}
	
//       {
// 	  connections.remove_connection(new_connections[0]);
// 		  connections.remove_connection(new_connections[1]);
// 		  connections.remove_connection(new_connections[2]);
// 		  connections.add_connection(old_connections_backup[0]);
// 		  connections.add_connection(old_connections_backup[1]);
// 		  particle_pool.delete_obj(p_b_particle);
// 		  // remove terminal particle from connection candidate list
// 		  new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
// printf("4");		  
// 		    return false;	
// 	}
	
	
/*{
		connections.remove_connection(new_connections[0]);
		  connections.remove_connection(new_connections[1]);
		  connections.remove_connection(new_connections[2]);
		  connections.add_connection(old_connections_backup[0]);
		  connections.add_connection(old_connections_backup[1]);
		  particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
		  new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
// 		  printf("BB################################################\n")	;
		    return false; 
  
}*/	
	
 	if (!CProposalBifurcationDeathSplit<TData,T,Dim>::do_tracking(
	  b_particle,
	  shape,
	  tree,
	  collision_search_rad,
	  maxendpointdist,
	  nvoxel,
	  search_connection_point_center_candidate,
	  edgecost_fun,
	  options,
	  Lprior,
	  ConnectEpsilon,
	  &new_edgecost_2,
	  &existing_connections, 
	  &old_edgecost_2,
	  &old_edge_prop,
	  false))
	{
// 	  printf("AA################################################\n")	;
	  connections.remove_connection(new_connections[0]);
		  connections.remove_connection(new_connections[1]);
		  connections.remove_connection(new_connections[2]);
		  connections.add_connection(old_connections_backup[0]);
		  connections.add_connection(old_connections_backup[1]);
		  particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
		  if (!new_terminal_is_registered_to_conn_tree)
			{
			  new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
			}
		  //new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
// 		  printf("BB################################################\n")	;
		    return false;
	}
	

	
	
	//TODO doppelt gemoppelt berechnet!
	//printf("A: %f %f\n",new_edgecost_2,new_edgecost);
	//printf("B: %f %f\n",old_edgecost_2,old_edgecost);

	particle_costs+=b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point->compute_cost3(temp); //new bif point
	particle_costs+=b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->point->compute_cost3(temp); //new bif point
	particle_costs+=b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->point->compute_cost3(temp); //new bif point
	particle_costs+=new_terminal_ep->point->compute_cost3(temp); //old segment point C
	
	
// 	T terminal_point_cost_change=new_terminal_ep->point->compute_cost()*(particle_connection_state_cost_scale[1]-particle_connection_state_cost_scale[2])
// 	 +(particle_connection_state_cost_offset[1]-particle_connection_state_cost_offset[2]);

		  
		T E=(new_edgecost-old_edgecost)/temp;
// 		  E+=(terminal_point_cost_change)/temp;
		E+=(particle_costs)/temp;

		  T R=mhs_fast_math<T>::mexp(E);
/*printf("3\n");*/		  


	  
//
 		T create_bif_prop=new_edge_prop/(connections.pool->get_numpts()-1); 
 		T remove_bif_prop=old_edge_prop/(connections.get_num_bifurcation_centers());
		
 		//printf("%f %f\n",new_edge_prop,old_edge_prop);
		//printf("%f %f\n",create_bif_prop,remove_bif_prop);
 		R*=remove_bif_prop/((create_bif_prop)+std::numeric_limits<T>::epsilon());
		
		
statistic[stat_count++]++;	
	
// 	  printf("%f %f %f | %f %f\n",terminal_point_cost_change,new_edgecost,old_edgecost,create_bif_prop,remove_bif_prop);
// 	  printf("%d\n",connections.get_num_bifurcation_centers());

 	  //if (true)
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
		//printf("eWARNING, BIRTHX accepted, but proposal pro are currently ignored!\n");
		
		
		
		
		sta_assert_debug0(*bifurcation_candidate_epoints[0]->endpoint_connections==1);
		sta_assert_debug0(*bifurcation_candidate_epoints[1]->endpoint_connections==1);
		sta_assert_debug0(*bifurcation_candidate_epoints[2]->endpoint_connections==1);
		sta_assert_debug0(b_particle.get_num_connections()==3);
		sta_assert_debug0(b_particle.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		
		
		if (options.no_double_bifurcations)
		{
		    if (p_b_particle->connects_two_bif())
		    {
			sta_assert_debug0(b_particle.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_connections[2]);
			connections.add_connection(old_connections_backup[0]);
			connections.add_connection(old_connections_backup[1]);
			particle_pool.delete_obj(p_b_particle);
			// remove terminal particle from connection candidate list
			//new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
			if (!new_terminal_is_registered_to_conn_tree)
			{
			  new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
			}
			
			return false;
		    }
		}
		
		
		bool out_of_bounce=false;
		b_particle.bifurcation_center_update(shape,out_of_bounce);
		
		if (out_of_bounce)
		{
		    sta_assert_debug0(b_particle.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_connections[2]);
			connections.add_connection(old_connections_backup[0]);
			connections.add_connection(old_connections_backup[1]);
			particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
			//new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
			if (!new_terminal_is_registered_to_conn_tree)
			{
			  new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
			}
			
		    return false;
		}

		if (options.bifurcation_particle_hard)	
		{
		  if (collision_fun_p.colliding( tree,b_particle,collision_search_rad,temp))
		  {
  // 		  printf("PUFF!! Bif Burth!\n");
			sta_assert_debug0(b_particle.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_connections[2]);
			connections.add_connection(old_connections_backup[0]);
			connections.add_connection(old_connections_backup[1]);
			    particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
			//new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
			    
			  if (!new_terminal_is_registered_to_conn_tree)
			  {
			    new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
			  }
			    
			    return false;
		  }
		}

		// check for loops by following new edge
		if (Constraints::constraint_hasloop_follow(b_particle,new_connections[2],options.constraint_loop_depth+1))
		{
		  sta_assert_debug0(b_particle.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		  connections.remove_connection(new_connections[0]);
		  connections.remove_connection(new_connections[1]);
		  connections.remove_connection(new_connections[2]);
		  connections.add_connection(old_connections_backup[0]);
		  connections.add_connection(old_connections_backup[1]);
		  particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
		  //new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
		  if (!new_terminal_is_registered_to_conn_tree)
		  {
		    new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
		  }
		  
		  return false;
		}
		
		if (options.bifurcation_particle_hard)	
		{
		  if(!tree.insert(b_particle))
		  {
		    connections.remove_connection(new_connections[0]);
		    connections.remove_connection(new_connections[1]);
		    connections.remove_connection(new_connections[2]);
		    connections.add_connection(old_connections_backup[0]);
		    connections.add_connection(old_connections_backup[1]);
		    particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
		  //new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
		    if (!new_terminal_is_registered_to_conn_tree)
		    {
		      new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
		    }
		    return false;
		    //throw mhs::STAError("error insering point\n");
		  }
		}
		
		if(!bifurcation_tree.insert(b_particle))
		{
		  connections.remove_connection(new_connections[0]);
		    connections.remove_connection(new_connections[1]);
		    connections.remove_connection(new_connections[2]);
		    connections.add_connection(old_connections_backup[0]);
		    connections.add_connection(old_connections_backup[1]);
		    particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
		  //new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
		    if (!new_terminal_is_registered_to_conn_tree)
		    {
		      new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
		    }
		    return false;
// 		  particle_pool.delete_obj(p_b_particle);
// 		  throw mhs::STAError("error insering point\n");
		}
		
		
		
// 		sta_assert_error(center_point.get_num_connections()==0);
// 		
		
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		 #ifdef BIFURCATION_DEBUG_EDGECHECK
		  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;    
// 		    for (int i=0;i<3;i++)
// 		    {
// 		    new_connections[i]->compute_cost(
// 				edgecost_fun,
// 			  options.connection_bonus_L,
// 			  options.bifurcation_bonus_L,
// 			  maxendpointdist,
// 			  inrange,false,bifurcation_edgecheck_update);
// 			
// 			/// of course the old edge should be in range (valid)
// 			sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 		    }
			  (b_particle.e_cost(
			      edgecost_fun,
			      options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      inrange,true,false,bifurcation_edgecheck_update));
		    
		 sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    
		    
		  #endif
		 
		 #ifdef  D_USE_GUI
		    b_particle.touch(get_type());
		    for (int i=0;i<3;i++)
		    {
			b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		    }
		  #endif	
		
		
	    
	    return true;
	  }
	  
	  connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_connections[2]);
			connections.add_connection(old_connections_backup[0]);
			connections.add_connection(old_connections_backup[1]);
			particle_pool.delete_obj(p_b_particle);
		  // remove terminal particle from connection candidate list
		  //new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);	
			
		  if (!new_terminal_is_registered_to_conn_tree)
		  {
		    new_terminal_ep->point->unregister_from_grid(voxgrid_conn_can_id);
		  }
			
		    return false;
	
	
	
	  
	}catch(mhs::STAError & error)
        {
            throw error;
        }
      
      
	return false;
    }
    
     protected:
    static bool do_tracking(
        class Points<T,Dim>::CEndpoint & bifurcation_candidate_epointA,
	class Points<T,Dim>::CEndpoint & bifurcation_candidate_epointB,
	const std::size_t *shape,
        OctTreeNode<T,Dim> &	tree,
        const T & collision_search_rad,
	const T & maxendpointdist,
        const std::size_t & nvoxel,
	T & search_connection_point_center_candidate,
        CEdgecost<T,Dim>  & edgecost_fun,
        class CTracker<TData,T,Dim>::COptions & options,
	T Lprior,
	T ConnectEpsilon,
	class Points<T,Dim>::CEndpoint **  bifurcation_candidate_epointC,
        T * new_edgecost,
	T * new_edge_prop)
    {
      
           //if new_connections.seg_A1!=NULL, then we search for an existing configuration
	  bool prob_request=(bifurcation_candidate_epointC!=NULL)&&(*bifurcation_candidate_epointC!=NULL);
	  
      
      
      	  //searching for double connected particles for building bifurcation (candidates)
	  // -> point must be in range of bifurcation_candidate_epoints[0,1]
	  // we check for 0
	  std::size_t found;
	  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	  class OctPoints<T,Dim> ** candidates;
	  candidates=query_buffer;
	  
	  try {
	      tree.queryRange(
		  bifurcation_candidate_epointA.point->get_position(),
		  search_connection_point_center_candidate,
		  candidates,
		  query_buffer_size,
		  found);
	  } catch (mhs::STAError & error)
	  {
	      throw error;
	  }
	  
	  sta_assert_debug0(found>2);
	  // only the segment has been found (itself+2 particles)
	  if (found==3)
	    return false;
	  
	  //T searchrad_sq=options.connection_candidate_searchrad;
	   T searchrad_sq=CEdgecost<T,Dim>::get_current_edge_length_limit();
	  searchrad_sq*=searchrad_sq;
	  
	  
// 	  bifurcation_candidate_epointA.point->update_endpoint(bifurcation_candidate_epointA.side);
// 	  bifurcation_candidate_epointB.point->update_endpoint(bifurcation_candidate_epointB.side);
	  
	  class Points<T,Dim>::CEndpoint * new_bifurcation_candidates[query_buffer_size];
	  
	  

//  printf("A\n");
	  
	  // collect all possible bifurcation points 
        std::size_t num_candidates=0;
        {
                for (std::size_t a=0; a<found; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    if (  (point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT)
			&&(point->get_num_connections()==2)
			&&(point!= bifurcation_candidate_epointA.point)
		        &&(point!= bifurcation_candidate_epointB.point))
		    {
		      for (int side=0;side<2;side++)
		      {
			
			class Points<T,Dim>::CEndpoint * ep=point->endpoints[side][0];
			
			// check if new terminal candidate is connected yet 
			// (later a valid terminal)
			if (ep->connected->point->get_num_connections()==2)
			{
// 			  point->update_endpoint(side);
			  Vector<T,Dim> & candidate_endpoint=*ep->position;
			 
			    if ((searchrad_sq>((bifurcation_candidate_epointA.point->get_position())-candidate_endpoint).norm2())&&
			      (searchrad_sq>((bifurcation_candidate_epointB.point->get_position())-candidate_endpoint).norm2()))
			    {
				new_bifurcation_candidates[num_candidates]=ep;
				num_candidates++;
			    }
			}
		      }
		    }
		}
	}
	
// printf("B\n");
	
	if (prob_request)
	{
	  sta_assert_error((((*bifurcation_candidate_epointC)->point->get_num_connections())==1));
	  sta_assert_error(num_candidates+2<query_buffer_size);
	  new_bifurcation_candidates[num_candidates]=*bifurcation_candidate_epointC;
	  num_candidates++;
	}
	
	
	
	
	//no terminal candidates nearby
	  if (num_candidates==0)
	    return false;
	  
	//compute edge costs (for valid candidates only)
	int valid_candidates=0;
	class Points<T,Dim>::CEndpoint ** valid_new_bifurcation_candidates[query_buffer_size];
	T candidate_costs[query_buffer_size];
// 	T candidate_costs[query_buffer_size];
// 	T_Edge_candidate  * valid_terminal_candidates_buffer[query_buffer_size];
// 	
	
// 	printf("a");
	
	
// 	bool prob_request_valid=false;
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	//for (std::size_t a=0; a<num_candidates; a++)
	for (std::size_t a=num_candidates-1; a>0; a--)
	{
	   T costs_edge =edgecost_fun.e_cost
	   (bifurcation_candidate_epointA,
	    bifurcation_candidate_epointB,
	    *new_bifurcation_candidates[a],
	    //(CEdgecost<T,Dim>::max_edge_length>-1) ? -1 : maxendpointdist, //-1,
	    inrange,false);
	    //maxendpointdist,inrange);
	  if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	  {
	    //printf("reason %d\n",static_cast<int>(inrange));
	    //checking existing configuration
	    if (prob_request&&(a==num_candidates-1))
	      return false;
	    
	    continue;
	  }
	  candidate_costs[valid_candidates]=costs_edge;
	  valid_new_bifurcation_candidates[valid_candidates]=&new_bifurcation_candidates[a];
	  valid_candidates++;
	}  
	
	  if (valid_candidates==0)
	    return false;
	
// 	printf("b");
	
	T candidate_prop_accu[query_buffer_size];
	T candidate_prop[query_buffer_size];
	
	candidate_prop_accu[0]=mhs_fast_math<T>::mexp(candidate_costs[0]+Lprior)+ConnectEpsilon;
	candidate_prop[0]=candidate_prop_accu[0];
	for (std::size_t a=1; a<valid_candidates; a++)
	{
	  candidate_prop[a]=mhs_fast_math<T>::mexp(candidate_costs[a]+Lprior)+ConnectEpsilon;
	  candidate_prop_accu[a]=candidate_prop_accu[a-1]+candidate_prop[a];
	}
	
	//no valid candidates nearby
	  if (valid_candidates==0)
	    return false;
	
	
	sta_assert_error(!(candidate_prop_accu[valid_candidates-1]<0));
	
	if (std::abs(candidate_prop_accu[valid_candidates-1])<0.00000001)
	{
	  return false;
	}
	
	std::size_t connection_candidate=rand_pic_array(candidate_prop_accu,valid_candidates,candidate_prop_accu[valid_candidates-1]);

	if (prob_request)
	{
	  sta_assert_error(valid_candidates>0);
	  //connection_candidate=valid_candidates-1;
	  connection_candidate=0;
	  sta_assert_error(*valid_new_bifurcation_candidates[connection_candidate]==*bifurcation_candidate_epointC);
	}
	

	  
	  
	  
	  
	*bifurcation_candidate_epointC=*valid_new_bifurcation_candidates[connection_candidate];
	*new_edgecost=candidate_costs[connection_candidate]+options.bifurcation_bonus_L;//2*options.connection_bonus_L;
	*new_edge_prop=candidate_prop[connection_candidate]/candidate_prop_accu[valid_candidates-1];

	   
	  
	  return true;
    }
    
};




template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathSplit : public CProposal<TData,T,Dim>
{
    friend class CProposalBifurcationBirthSplit<TData,T,Dim>;
    

protected:
    typedef struct 
    {
      class Points<T,Dim>::CEndpoint * seg_A1;
      class Points<T,Dim>::CEndpoint * seg_A2;
      class Points<T,Dim>::CEndpoint * seg_B1;
      class Points<T,Dim>::CEndpoint * seg_B2;
    } T_Edge_candidate;
  
  
    T Lprior;
         T ConnectEpsilon;

    const static int max_stat=10;
    int statistic[max_stat];
public:
  
       T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }

    CProposalBifurcationDeathSplit(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;	
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
    }

    ~CProposalBifurcationDeathSplit()
    {
//  printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("(%d)%d ",i,statistic[i]);
// 	printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X);
    };
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	 if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	
    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
         pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;

        this->proposal_called++;


        int stat_count=0;
	
	//0
        statistic[stat_count++]++;

        bool debug=false;


	
	if (connections.get_num_bifurcation_centers()<1)
	  return false;
	//1
	statistic[stat_count++]++;
	
	try
	{
	  /// randomly pic a bifurcation node
	  Points<T,Dim> &  center_point=connections.get_rand_bifurcation_center_point();
	  
	  /// check if bifurcation has no terminal
	  for (int a=0;a<3;a++)
	  if (center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][a]->connected->point->get_num_connections()!=2)
	    return false;
	  

	  T_Edge_candidate new_connections;
	  new_connections.seg_A1=NULL;
	  T new_edgecost;
	  T new_edge_prop;
	  T old_edgecost;
	  
	
	  
	  
	if (!do_tracking(
	      center_point,
	      shape,
	      conn_tree,
	      collision_search_rad,
	      maxendpointdist,
	      nvoxel,
	      search_connection_point_center_candidate,
	      edgecost_fun,
	      options,
	      Lprior,
	      ConnectEpsilon,
	      &old_edgecost,
	      &new_connections,
	      &new_edgecost,
	      &new_edge_prop))
	  return false;
	 
	  
	  //it may happen (particularlyu at high temp) that there alrerady exists a connection between A1 and A2
	 {
	  class Connection<T,Dim> a_new_connection;
	  a_new_connection.edge_type=EDGE_TYPES::EDGE_SEGMENT;
	  a_new_connection.pointA=new_connections.seg_A1;
	  a_new_connection.pointB=new_connections.seg_A2;
	  if (Connections<T,Dim>::connection_exists(a_new_connection))
	    return false;
	 } 
	  
	
	  
	  class Points<T,Dim>::CEndpoint * bifurcation_candidate_epoints[3];
	  bifurcation_candidate_epoints[0]=new_connections.seg_B1; //(initial edge 
	  bifurcation_candidate_epoints[1]=new_connections.seg_B2; //   role  in birth)
	  bifurcation_candidate_epoints[2]=new_connections.seg_A1; //(terminal role in birth)
	  
	  T old_edgecost2;
	  T old_edge_prop;
	  
	 
	  
	   if (!CProposalBifurcationBirthSplit<TData,T,Dim>::do_tracking(
        *bifurcation_candidate_epoints[0],
	*bifurcation_candidate_epoints[1],
	shape,
        tree,
        collision_search_rad,
	maxendpointdist,
        nvoxel,
	search_connection_point_center_candidate,
        edgecost_fun,
        options,
	Lprior,
	ConnectEpsilon,
	&bifurcation_candidate_epoints[2],
        &old_edgecost2,
	&old_edge_prop))
	    return false;
	    
	    
	    
	    
	
	
	
	T particle_costs=0;
	particle_costs-=new_connections.seg_A2->point->compute_cost3(temp); 
	particle_costs-=new_connections.seg_A1->point->compute_cost3(temp); 
	particle_costs-=new_connections.seg_B2->point->compute_cost3(temp); 
	particle_costs-=new_connections.seg_B1->point->compute_cost3(temp); 
	
	
	
// 	std::size_t particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_SEGMENT);
// 	particle_costs+=new_connections.seg_A2->point->compute_cost3(temp,particle_conn_id); 
// 	particle_costs+=new_connections.seg_A1->point->compute_cost3(temp,particle_conn_id); 
// 	particle_costs+=new_connections.seg_B2->point->compute_cost3(temp,particle_conn_id); 
// 	particle_costs+=new_connections.seg_B1->point->compute_cost3(temp,particle_conn_id); 
	
	
	std::size_t particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_SEGMENT);
	
// 	int num_conn=new_connections.seg_A2->point->get_num_connections();
// 	sta_assert_error(num_conn<=2);
// 	if (num_conn>1)
// 	{
// 	    
// 	    new_connections.seg_A2->point->endpoints[0][0]->connected!=NULL
// 	    
// 	}
	
	std::size_t new_particle_id=(new_connections.seg_A1->point->particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION)) ? static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT) : particle_conn_id;
	particle_costs+=new_connections.seg_A1->point->compute_cost3(temp,new_particle_id); 
	    
	new_particle_id=(new_connections.seg_A2->point->get_num_conneced_bifurcations()==2) ? static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT) : particle_conn_id;
	particle_costs+=new_connections.seg_A2->point->compute_cost3(temp,new_particle_id); 
	
// 	if (new_particle_id==static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT))
// 	  printf("yupp\n");

	new_particle_id=(new_connections.seg_B2->point->get_num_conneced_bifurcations()==2) ? static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT) : particle_conn_id;
	particle_costs+=new_connections.seg_B2->point->compute_cost3(temp,new_particle_id); 
	
// 	if (new_particle_id==static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT))
// 	  printf("yapp\n");
	
	new_particle_id=(new_connections.seg_B1->point->get_num_conneced_bifurcations()==2) ? static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT) : particle_conn_id;
	particle_costs+=new_connections.seg_B1->point->compute_cost3(temp,new_particle_id); 
	
// 	if (new_particle_id==static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT))
// 	  printf("yipp\n");
	    
	
	  //the terminal gets connected, yea!
// 	  T terminal_point_cost_change=new_connections.seg_A1->point->compute_cost()*(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])
// 	  +(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]);
	  
	  
	  T E=(new_edgecost-old_edgecost)/temp;
// 	    E+=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;
	  
	  
	  T R=mhs_fast_math<T>::mexp(E);
	  
	  
	  T create_bif_prop=new_edge_prop/(connections.pool->get_numpts()-1); 
 	  T remove_bif_prop=old_edge_prop/(connections.get_num_bifurcation_centers()-1+std::numeric_limits< T >::epsilon());	  
	  R*=create_bif_prop/((remove_bif_prop)+std::numeric_limits<T>::epsilon());
	  
// 	  T flatten_bif_prop=new_edge_prop/(connections.get_num_bifurcation_centers());
// 	  // randomly select edge -> uniformly pick one side particle as center candidate -> uniformly pick a particle side
// 	  T bif_create_prop=1.0/(connections.pool->get_numpts()-1)*(0.5)*(0.5); 
// 	  R*=bif_create_prop/((flatten_bif_prop)+std::numeric_limits<T>::epsilon());

	  
	 // printf("%.2f %.2f | %.5f %.5f  | \n",new_edgecost,old_edgecost,flatten_bif_prop,bif_create_prop);
	  
	//5
	statistic[stat_count++]++;			
	
// 	  return false;
	
	
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
		//printf("eWARNING, DEATHX accepted, but proposal pro are currently ignored!\n");
	    
	    /*
	    for (int i=0;i<3;i++)
	    {
	        class Points<T,Dim>::CEndpoint * opposite_endpt=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected->opposite_slots[0];
	      if (opposite_endpt->connected!=NULL)
	      {
		  printf ("ep %d: %u \n",i,opposite_endpt->connected->point);
	      }
	    }
	    */
	    
	      
	    
	    

		class Connection<T,Dim> old_connections_backup[3];
		// remove bifurcation connections
		for (int slot=0;slot<3;slot++)
		{
		  class Connection<T,Dim> *  delete_me=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection;
		  sta_assert_debug0(delete_me!=NULL);
		  old_connections_backup[slot]=*delete_me;
		  connections.remove_connection(delete_me);
		}
		
		sta_assert_error(center_point.get_num_connections()==0);
		
		
		
		//printf("%u %u %u %u\n",new_connections.seg_A1->point,new_connections.seg_A2->point,new_connections.seg_B1->point,new_connections.seg_B2->point);
		
		
		class Connection<T,Dim> *  new_connection_backup[2];	
	      class Connection<T,Dim> a_new_connection;
	      a_new_connection.edge_type=EDGE_TYPES::EDGE_SEGMENT;
	      
	      try {
	      a_new_connection.pointA=new_connections.seg_B2;
	      a_new_connection.pointB=new_connections.seg_B1;
	      new_connection_backup[1]=connections.add_connection(a_new_connection);
	      }catch(mhs::STAError & error)
	      {
		  mhs::STAError error2;
		  error2<<error.str().c_str();
		  error2<<"\naccepted but could not add edge B";
		  throw error2;
	      }

	      try {
	      a_new_connection.pointA=new_connections.seg_A2;
	      a_new_connection.pointB=new_connections.seg_A1;
	      new_connection_backup[0]=connections.add_connection(a_new_connection);
	      }catch(mhs::STAError & error)
	      {
		  mhs::STAError error2;
		  error2<<error.str().c_str();
		  error2<<"\naccepted but could not add edge A";
		  throw error2;
	      }
	      
	      
	      if (Constraints::constraint_hasloop_follow(*new_connection_backup[0]->pointA->point,new_connection_backup[0],options.constraint_loop_depth+1))
		{
		  connections.remove_connection(new_connection_backup[0]);
		  connections.remove_connection(new_connection_backup[1]);
		  connections.add_connection(old_connections_backup[0]);
		  connections.add_connection(old_connections_backup[1]);
		  connections.add_connection(old_connections_backup[2]);
		  return false;
		}
	      
		#ifdef  D_USE_GUI
		for (int i=0;i<2;i++)
		{
		  new_connection_backup[i]->pointA->point->touch(get_type());
		  new_connection_backup[i]->pointB->point->touch(get_type());
		}
		#endif	
	      
   
	      // remove bifurcation center particle
	      
	        Points<T,Dim> * p_center_point=&center_point;
		particle_pool.delete_obj(p_center_point);
		sta_assert_error(p_center_point==NULL);

		
		// remove terminal particle from connection candidate list
		new_connections.seg_A1->point->unregister_from_grid(voxgrid_conn_can_id);
		
		
		
	
		
			

	      
	      this->tracker->update_energy(E,static_cast<int>(get_type()));
	      this->proposal_accepted++;
	      
	       #ifdef BIFURCATION_DEBUG_EDGECHECK
		  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;    
		    for (int i=0;i<2;i++)
		    {
		    new_connection_backup[i]->e_cost(
				edgecost_fun,
			  options.connection_bonus_L,
			  options.bifurcation_bonus_L,
			  inrange,true,false,bifurcation_edgecheck_update);
			
			/// of course the old edge should be in range (valid)
			sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    }
// 			  (b_particle.edge_energy(
// 			      edgecost_fun,
// 			      options.connection_bonus_L,
// 			      options.bifurcation_bonus_L,
// 			      maxendpointdist,
// 			      inrange,false,bifurcation_edgecheck_update));
// 		    
// 		 sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    
		    
		  #endif
	      
	      
	      
	    return true;
	  }
	//6
	statistic[stat_count++]++;
	return false;
	  

	} catch(mhs::STAError & error)
        {
            throw error;
        }
	
	
      
      
	return false;
    }
    
  protected:
    static bool do_tracking(
        Points<T,Dim> &  center_point,
	const std::size_t *shape,
        OctTreeNode<T,Dim> &	conn_tree,
        const T & collision_search_rad,
	const T & maxendpointdist,
        const std::size_t & nvoxel,
	T & search_connection_point_center_candidate,
        CEdgecost<T,Dim>  & edgecost_fun,
        class CTracker<TData,T,Dim>::COptions & options,
	T Lprior,
	T ConnectEpsilon,
	T * old_edgecost,
        T_Edge_candidate * new_connections,
	T * new_edgecost,
	T * new_edge_prop,
	bool check_edge_in_range=true)
    {
      
           //if new_connections.seg_A1!=NULL, then we search for an existing configuration
	  bool prob_request=(new_connections->seg_A1!=NULL);
	  int prob_request_request_valide=-1;
     
	  bool out_of_bounce=false;
	  center_point.bifurcation_center_update(shape,out_of_bounce);
	  
	  //NOTE might be slighlty out of image
	  if (out_of_bounce)
	  {
	    /*
// 	    printf("\n");
	    bool out_of_bounce=false;
	    center_point.bifurcation_center_update(shape,out_of_bounce,true);
	    //NOTE might be slighlty out of image
	    printf("CProposalBifurcationDeathSplit: out_of_bounce A\n");
 	    printf("%d\n",out_of_bounce);
 	    printf("%d %d %d\n",shape[0],shape[1],shape[2]);
 	    center_point.position.print();
	    printf("CProposalBifurcationDeathSplit: out_of_bounce B\n");
	    */
	    return false;
	  }
	  
	  
	  sta_assert_debug0(!out_of_bounce);
	  
	  
	  // we are searching for terminals,
	  // -> hence first search for particles that are not fully connected
	  std::size_t found;
	  class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	  class OctPoints<T,Dim> ** candidates;
	  candidates=query_buffer;

	  try {
	      conn_tree.queryRange(
		  center_point.get_position(),
		  search_connection_point_center_candidate+options.connection_candidate_searchrad,
		  candidates,
		  query_buffer_size,
		  found);
	  } catch (mhs::STAError & error)
	  {
	      throw error;
	  }
	  
	  //no terminals nearby (all bifurcation points are NO terminals in this case)
	  if (found==0)
	    return false;
	  
	  
	
	T searchrad_sq=options.connection_candidate_searchrad;
	searchrad_sq*=searchrad_sq;
	

	
	
	 typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	 // this will also update bifurcation point positions
	  *old_edgecost=center_point.e_cost(
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange
	  );
	  
	  
	  //check if constellation is valid
	  if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	  {
// 	    center_point.edge_energy(
// 	    edgecost_fun,
// 	    options.connection_bonus_L,
// 	    options.bifurcation_bonus_L,
// 	    maxendpointdist,
// 	    inrange,
// 	    true
// 	      );
	    if (check_edge_in_range)
	    {
	     sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)); 
	    } else
	    {
	     return false; 
	    }
	  }
	  
	
	
	sta_assert_error(found<query_buffer_size);
	T_Edge_candidate  terminal_candidates_buffer[query_buffer_size];
	

	
	
	// collect all possible bifurcation points <-> terminal edges
        std::size_t num_terminals=0;
        {
                for (std::size_t a=0; a<found; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    
// 		     	if (prob_request&&(point==new_connections->seg_A1->point))
// 		       {
// 			 printf ("BLING BLIGN!\n");
// 		       }
		    
		    if (  (point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT)
			&&(point->get_num_connections()==1)  )
		    {
		       int free_side=(point->endpoint_connections[0]==0) ? 0 : 1;
		       
		    
		       class Points<T,Dim>::CEndpoint * ep=point->endpoints[free_side][0];
// 		       point->update_endpoint(free_side);
		       
		       
// 		       if (prob_request&&(ep==new_connections->seg_A1))
// 		       {
// 			 printf ("BLING BLIGN!\n");
// 		       }
		       
		         Vector<T,Dim> & candidate_endpoint=*ep->position;
			 for (int s=0;s<3;s++)
			 {
			    if ((searchrad_sq>((*center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][s]->connected->position)-candidate_endpoint).norm2()))
			    {
			        // if we search for a specific constellation....
			        if (prob_request&&(ep==new_connections->seg_A1))
				{
				  prob_request_request_valide=num_terminals;
				}
			      
				terminal_candidates_buffer[num_terminals].seg_A1=ep;
				terminal_candidates_buffer[num_terminals].seg_A2=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][s]->connected;
				
				terminal_candidates_buffer[num_terminals].seg_B1=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][(s+1)%3]->connected;
				terminal_candidates_buffer[num_terminals].seg_B2=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][(s+2)%3]->connected;
				
				sta_assert_debug0(terminal_candidates_buffer[num_terminals].seg_A1->point!=terminal_candidates_buffer[num_terminals].seg_A2->point);
				sta_assert_debug0(terminal_candidates_buffer[num_terminals].seg_B1->point!=terminal_candidates_buffer[num_terminals].seg_B2->point);
				sta_assert_debug0(terminal_candidates_buffer[num_terminals].seg_A1->point!=terminal_candidates_buffer[num_terminals].seg_B1->point);
				sta_assert_debug0(terminal_candidates_buffer[num_terminals].seg_A2->point!=terminal_candidates_buffer[num_terminals].seg_B2->point);
				sta_assert_debug0(terminal_candidates_buffer[num_terminals].seg_A1->point!=terminal_candidates_buffer[num_terminals].seg_B2->point);
				sta_assert_debug0(terminal_candidates_buffer[num_terminals].seg_A2->point!=terminal_candidates_buffer[num_terminals].seg_B1->point);
				num_terminals++;
			    }
			 }
		    }
		}
	}
	
	
	
	//no terminals nearby
	  if (num_terminals==0)
	    return false;
	  
	  if (prob_request&&(prob_request_request_valide<0))
	  {
// 	    printf("not found :(\n");
	    return false;
	  }
	

	
	//compute edge costs (for valid candidates only)
	int valid_candidates=0;
	T candidate_costs[query_buffer_size];
	T_Edge_candidate  * valid_terminal_candidates_buffer[query_buffer_size];
	
	bool prob_request_valid=false;
	
	
	for (std::size_t a=0; a<num_terminals; a++)
	{
// 	   if (prob_request&&(ep==new_connections->seg_A1))
// 				  prob_request_request_valide=true;
	  
	  //sta_assert_error(inrange[0]==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	  T costs_edge =edgecost_fun.e_cost(*terminal_candidates_buffer[a].seg_A1,*terminal_candidates_buffer[a].seg_A2,inrange);
	  if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	  {
	    //printf("reason %d\n",static_cast<int>(inrange));
	    continue;
	  }
	  costs_edge+=edgecost_fun.e_cost(*terminal_candidates_buffer[a].seg_B1,*terminal_candidates_buffer[a].seg_B2,inrange);
	  if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	  {
	    //printf("reason %d\n",static_cast<int>(inrange));
	    continue;
	  }
	  if ((!prob_request_valid)&&(a==prob_request_request_valide))
	  {
	    prob_request_valid=true;
	    prob_request_request_valide=valid_candidates;
	  }
	  candidate_costs[valid_candidates]=costs_edge;
	  valid_terminal_candidates_buffer[valid_candidates]=terminal_candidates_buffer+a;
	  valid_candidates++;
	}
	
	//printf("nt %d %d:\n",valid_candidates,num_terminals);
	
	//no valid candidates nearby
	  if (valid_candidates==0)
	    return false;
	
	if (prob_request&&(!prob_request_valid))
	    return false;
	
	
	
	  
	T candidate_prop_accu[query_buffer_size];
	T candidate_prop[query_buffer_size];
	
	candidate_prop_accu[0]=mhs_fast_math<T>::mexp(candidate_costs[0]+Lprior)+ConnectEpsilon;
	candidate_prop[0]=candidate_prop_accu[0];
	for (std::size_t a=1; a<valid_candidates; a++)
	{
	  candidate_prop[a]=mhs_fast_math<T>::mexp(candidate_costs[a]+Lprior)+ConnectEpsilon;
	  candidate_prop_accu[a]=candidate_prop_accu[a-1]+candidate_prop[a];
	}
	
	sta_assert_error(!(candidate_prop_accu[valid_candidates-1]<0));
	
	if (std::abs(candidate_prop_accu[valid_candidates-1])<0.00000001)
	{
	  return false;
	}
	
	std::size_t connection_candidate=rand_pic_array(candidate_prop_accu,valid_candidates,candidate_prop_accu[valid_candidates-1]);
	
	if (prob_request)
	  connection_candidate=prob_request_request_valide;
	
	*new_connections=*valid_terminal_candidates_buffer[connection_candidate];
	*new_edgecost=candidate_costs[connection_candidate]+2*options.connection_bonus_L;
	*new_edge_prop=candidate_prop[connection_candidate]/candidate_prop_accu[valid_candidates-1];
      
      return true;
    }
    
};




template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathLine : public CProposal<TData,T,Dim>
{
    friend class CProposalBifurcationDeathSimple<TData,T,Dim>;
    friend class CProposalBifurcationReconn<TData,T,Dim>;

protected:
    T Lprior;
    const static int max_stat=10;
    int statistic[max_stat];
public:

  
       T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
    
    
    CProposalBifurcationDeathLine(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
    }

    ~CProposalBifurcationDeathLine()
    {
//  printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("(%d)%d ",i,statistic[i]);
// 	printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L);
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;

        this->proposal_called++;


        int stat_count=0;
	
	//0
        statistic[stat_count++]++;

        bool debug=false;


	
	if (connections.get_num_bifurcation_centers()<1)
	  return false;
	//1
	statistic[stat_count++]++;
	
	try
	{
	  /// randomly pic a bifurcation node
	  Points<T,Dim> &  center_point=connections.get_rand_bifurcation_center_point();
	  
	  
	  class Points<T,Dim>::CEndpoint * candidates[3]; //class Points<T,Dim>::CEndpoint * candidates[2]; // NOTE AUG 17: why not lonlely bifurcations?
	  int count=0;
	  for (int slot=0;slot<3;slot++)
	  {
	    class Points<T,Dim>::CEndpoint * ep=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][slot]->connected;
	    if (ep->point->get_num_connections()==1)
	    {
// 	      if (count==2) // found lonely bifurcation    // NOTE AUG 17: why not lonlely bifurcations?
// 		return false;
	      candidates[count++]=ep;
	    }
	  }
	  //2
	  statistic[stat_count++]++;
	  
	    if (count==0) // no open endpoint
	      return false;
	    
	    
	    //3
	  statistic[stat_count++]++;  
	    
	  count=std::rand()%(count);
	  
	  sta_assert_error(count<3);//sta_assert_error(count<2);// NOTE AUG 17: why not lonlely bifurcations?
	  
	  class Points<T,Dim>::CEndpoint * new_center=candidates[count];
	  class Points<T,Dim>::CEndpoint * configuration[4];
	  configuration[0]=configuration[1]=configuration[2]=configuration[3]=NULL;
	  
	  count =0;
	  for (int slot=0;slot<3;slot++)
	  { 
	    class Points<T,Dim>::CEndpoint * ep=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][slot]->connected;
	    if (ep!=new_center)
	    {
// 	      if (count==2)
// 	      {
// 		for (int a=0;a<3;a++)
// 		{
// 		  printf("%u %u\n",center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][a]->connected,new_center);
// 		}
// 	      }
	      sta_assert_error(count<2);
	      configuration[count*3]=ep;
	      count++;
	    } 
	  }
	  sta_assert_error(configuration[0]!=NULL);
	  sta_assert_error(configuration[3]!=NULL);
	  sta_assert_error(configuration[3]!=configuration[0]);
	  
	  configuration[1]=&(new_center->point->endpoints_[0][0]);
	  configuration[2]=&(new_center->point->endpoints_[1][0]);
	  
	  
	  // this endpoint won't be updated during bifurcation edge cost computation
// 	  new_center->point->update_endpoint(new_center->opposite_slots[0]->side);
	  
	  
	  //configuration[0] first outer endpoint
	  //configuration[1] first center endpoint
	  //configuration[2] second center endpoint
	  //configuration[3] second outer endpoint
	    
	  
	  //test for configuration
	
	  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange[2];
	  
	  
	  //compute old costs and update endpoint positions
	  // TODO if endpos are updated automatically, move below
	  T old_edgecost=center_point.e_cost(
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange[0]
	  );
	  sta_assert_error(inrange[0]==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	  
	  
	  T cost_config[2];
	  T connet_prop[2];connet_prop[0]=connet_prop[1]=0;
//	  	  T testcost= edgecost_fun.u_cost( *bifurcation_endpoints[0],
// 				      *bifurcation_endpoints[1],
// 				      *bifurcation_endpoints[2],
// 				      maxendpointdist,
// 				      inrange
// 				    )+options.bifurcation_bonus_L;
						   
	    cost_config[0] =edgecost_fun.e_cost(*configuration[0],*configuration[1],inrange[0]);
	    if (inrange[0]==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		cost_config[0]+=edgecost_fun.e_cost(*configuration[2],*configuration[3],inrange[0]);
	    if (inrange[0]==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	    {
	      connet_prop[0]=mhs_fast_math<T>::mexp(cost_config[0]+Lprior);
	      cost_config[0]+=2*options.connection_bonus_L;
	    }
	  
	  
	  std::swap(configuration[1],configuration[2]);
	  
	  //test for second configuration
	  
	   cost_config[1] =edgecost_fun.e_cost(*configuration[0],*configuration[1],inrange[1]);
	   if (inrange[1]==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	      cost_config[1]+=edgecost_fun.e_cost(*configuration[2],*configuration[3],inrange[1]);
	   if (inrange[1]==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	   {
	       connet_prop[1]=mhs_fast_math<T>::mexp(cost_config[1]+Lprior);
	      cost_config[1]+=2*options.connection_bonus_L;
	   }
	   
// 	   printf("%f %f\n",cost_config[0],cost_config[1]);
	   
	   
	  //randomly choose a candidate
	   
	   T connet_prop_acc[2];
	  connet_prop_acc[0]=connet_prop[0];
	  connet_prop_acc[1]=connet_prop[0]+connet_prop[1];
	  
	  std::size_t connection_candidate=rand_pic_array(connet_prop_acc,2,connet_prop_acc[1]);
	  
	  if (inrange[connection_candidate]!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	    return false;
	  
	  T new_edgecost=cost_config[connection_candidate];
	  T new_edge_prop=connet_prop[connection_candidate]/(connet_prop[0]+connet_prop[1]+std::numeric_limits< T >::epsilon());
	  
	  if (connection_candidate==0)
	    std::swap(configuration[1],configuration[2]);	    
	  
	  
	  
	      
	
	
	T particle_costs=0;
	particle_costs-=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point->compute_cost3(temp); 
	particle_costs-=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->point->compute_cost3(temp); 
	particle_costs-=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->point->compute_cost3(temp);
	
	
	
// 	particle_costs+=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->point->compute_cost3(temp,particle_conn_id); 
// 	particle_costs+=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->point->compute_cost3(temp,particle_conn_id); 
// 	particle_costs+=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->point->compute_cost3(temp,particle_conn_id); 
	
	
	std::size_t particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_SEGMENT);
	
	for (int i=0;i<3;i++)
	{
	  class Points<T,Dim> * point= center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected->point;
	  try {
	  std::size_t new_particle_id=(point->get_num_conneced_bifurcations()==2) ? static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT) : particle_conn_id;
	  particle_costs+=point->compute_cost3(temp,new_particle_id); 
	  } catch (mhs::STAError error)
	  {
	   printf("particle %i is hen\n",i); 
	   throw error; 
	  }
	  
	}
	

	
	
	  
	  
	  //the center point's reward increases (two sides conected)  
// 	  T terminal_point_cost_change=center_point.compute_cost()*(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])
// 	  +(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]);

	  
	  T E=(new_edgecost-old_edgecost)/temp;
// 	    E+=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;
	  
	  
	  T R=mhs_fast_math<T>::mexp(E);
	  
	  
	  T flatten_bif_prop=new_edge_prop/(connections.get_num_bifurcation_centers());
	  // randomly select edge -> uniformly pick one side particle as center candidate -> uniformly pick a particle side
	  T bif_create_prop=1.0/(connections.pool->get_numpts()-1)*(0.5)*(0.5); 
	  R*=bif_create_prop/((flatten_bif_prop)+std::numeric_limits<T>::epsilon());

	  
	 // printf("%.2f %.2f | %.5f %.5f  | \n",new_edgecost,old_edgecost,flatten_bif_prop,bif_create_prop);
	  
	//4
	statistic[stat_count++]++;			
	
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;


		#ifdef  D_USE_GUI
// 		  for (int i=0;i<3;i++)
// 		  {
// 		    old_connections_backup[i].pointA->point->touch(get_type());
// 		    old_connections_backup[i].pointB->point->touch(get_type());
// 		  }
		
		  //center_point.touch(get_type());
		  for (int i=0;i<3;i++)
		  {
			center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		  }
		#endif	

		
		// remove bifurcation connections
		for (int slot=0;slot<3;slot++)
		{
		  class Connection<T,Dim> *  delete_me=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection;
		  sta_assert_error(delete_me!=NULL);
		  connections.remove_connection(delete_me);
		  
		}
		
		
		
		
		sta_assert_error(center_point.get_num_connections()==0);
		
		// remove bifurcation center particle
		 Points<T,Dim> * p_center_point=&center_point;
		particle_pool.delete_obj(p_center_point);
		sta_assert_error(p_center_point==NULL);
		
		// remove new center particle from connection candidate list
		new_center->point->unregister_from_grid(voxgrid_conn_can_id);
		
		
	      class Connection<T,Dim> a_new_connection;
	      a_new_connection.edge_type=EDGE_TYPES::EDGE_SEGMENT;
	      a_new_connection.pointA=configuration[0]->point->getfreehub(configuration[0]->side);
	      a_new_connection.pointB=configuration[1]->point->getfreehub(configuration[1]->side);
	      connections.add_connection(a_new_connection);
	      
	       #ifdef BIFURCATION_DEBUG_EDGECHECK
		  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;    
		    {
		    a_new_connection.e_cost(
				edgecost_fun,
			  options.connection_bonus_L,
			  options.bifurcation_bonus_L,
			  inrange,true,false,bifurcation_edgecheck_update);
			
			sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    }
		  #endif
	      
	      
	      a_new_connection.pointA=configuration[2]->point->getfreehub(configuration[2]->side);
	      a_new_connection.pointB=configuration[3]->point->getfreehub(configuration[3]->side);
	      connections.add_connection(a_new_connection);
	      
		  #ifdef BIFURCATION_DEBUG_EDGECHECK
		    
		    {
		    a_new_connection.e_cost(
				edgecost_fun,
			  options.connection_bonus_L,
			  options.bifurcation_bonus_L,
			  inrange,true,false,bifurcation_edgecheck_update);
			
			/// of course the old edge should be in range (valid)
			sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    }
		  #endif	      
	      
	      
	      
	    return true;
	  }
	//5
	statistic[stat_count++]++;
	return false;
	} catch(mhs::STAError & error)
        {
            throw error;
        }
	
    }
    
};






template<typename TData,typename T,int Dim>
class CProposalBifurcationBirthLine : public CProposal<TData,T,Dim>
{
    friend class CProposalBifurcationBirthSimple<TData,T,Dim>;
    friend class CProposalBifurcationReconn<TData,T,Dim>;

protected:
    T Lprior;
    const static int max_stat=10;
    int statistic[max_stat];
public:
     T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }



    CProposalBifurcationBirthLine(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
    }



    ~CProposalBifurcationBirthLine()
    {
//         printf("\n");
//         for (int i=0; i<max_stat; i++)
//             printf("%d ",statistic[i]);
//         printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_L);
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_L);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	
	
        this->proposal_called++;
      
	int stat_count=0;
	statistic[stat_count++]++;
	 
	class Connection<T,Dim> * edge;
	
        /// uniformly pic edge
        edge=connections.get_rand_edge();
        if (edge==NULL)
            return false;
        
        if (edge->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
	  return false;
	
	int side=std::rand()%2;
	
	//class Points< T, Dim >::CEndpoint
	Points< T, Dim > * terminal_candidate_point=edge->points[side]->point;
	
	// check if point is within a segment
	if (terminal_candidate_point->get_num_connections()!=2)
	  return false;
	
	// check if point is part of a bifurcation
	if (terminal_candidate_point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	  return false;
	
	
	sta_assert_debug0(terminal_candidate_point->endpoint_connections[0]==1);
	sta_assert_debug0(terminal_candidate_point->endpoint_connections[1]==1);
	
	
	try 
	{
	  statistic[stat_count++]++;
	  
	  //new terminal particle connected side
	  side=std::rand()%2;
	  
	  class Points<T,Dim>::CEndpoint * involved_epts[3];			  
	  involved_epts[0]=terminal_candidate_point->endpoints[0][0]->connected;
	  involved_epts[1]=terminal_candidate_point->endpoints[1][0]->connected;
	  involved_epts[2]=terminal_candidate_point->endpoints[side][0];
	  
	  if (!options.bifurcation_neighbors)
	  {
	      if ((involved_epts[0]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)||
		(involved_epts[1]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)
	      )
	      {
		return false;
	      }
	    
	  }
	  
	  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	  
	  // compute old edgecosts and update endpoint positions
	  // TODO if endpos are updated automatically, move below
	  T old_edgecost=terminal_candidate_point->e_cost(
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange
	  );
	  sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	  
	  T new_edgecost=edgecost_fun.e_cost(*terminal_candidate_point->endpoints[0][0]->connected,
				  *terminal_candidate_point->endpoints[1][0]->connected,
				  *terminal_candidate_point->endpoints[side][0],
				  inrange
				  )+options.bifurcation_bonus_L;
	  
	
	
				  
	  if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	  {  
	    return false;
	  }
	  
// 	  {
// 	    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange2;
// 	    T new_edgecost=edgecost_fun.u_cost(*terminal_candidate_point->endpoints[0][0]->connected,
// 				  *terminal_candidate_point->endpoints[1][0]->connected,
// 				  *terminal_candidate_point->endpoints[side][0],
// 				  maxendpointdist, 
// 				  inrange2
// 				  );		    
// 	    sta_assert_error(inrange2==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	    }
	  
	  
	  

// 	  {
// 	    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange2;
// 	    T new_edgecost=edgecost_fun.u_cost(*terminal_candidate_point->endpoints[0][0]->connected,
// 				  *terminal_candidate_point->endpoints[1][0]->connected,
// 				  *terminal_candidate_point->endpoints[side][0],
// 				  maxendpointdist, 
// 				  inrange2
// 				  );		    
// 	    sta_assert_error(inrange2==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	    }	  
// 	  
	  //only used to compute the probability
	  T old_edgecost_alternative=
				  edgecost_fun.e_cost(*terminal_candidate_point->endpoints[0][0],
				  *terminal_candidate_point->endpoints[1][0]->connected,
				  inrange
				  )+options.bifurcation_bonus_L;
	  if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	  {
	    old_edgecost_alternative=0;
	  } else 
	  {
	    old_edgecost_alternative+=
				  edgecost_fun.e_cost(*terminal_candidate_point->endpoints[1][0],
				  *terminal_candidate_point->endpoints[0][0]->connected,
				  inrange
				  )+options.bifurcation_bonus_L;
	    if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		old_edgecost_alternative=0;  
	  }
	  
	  
// 	  {
// 	    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange2;
// 	    T new_edgecost=edgecost_fun.u_cost(*terminal_candidate_point->endpoints[0][0]->connected,
// 				  *terminal_candidate_point->endpoints[1][0]->connected,
// 				  *terminal_candidate_point->endpoints[side][0],
// 				  maxendpointdist, 
// 				  inrange2
// 				  );		    
// 	    sta_assert_error(inrange2==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	    }	  
	  
	  T conn_prop=mhs_fast_math<T>::mexp(old_edgecost+Lprior-options.bifurcation_bonus_L);
	  conn_prop/=conn_prop+mhs_fast_math<T>::mexp(old_edgecost_alternative+Lprior-options.bifurcation_bonus_L)+std::numeric_limits< T >::epsilon();
	  
	  
	  T particle_costs=0;
	  particle_costs-=involved_epts[0]->point->compute_cost3(temp); 
	  particle_costs-=involved_epts[1]->point->compute_cost3(temp); 
	  particle_costs-=involved_epts[2]->point->compute_cost3(temp);
	  
	  std::size_t particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT);
	  particle_costs+=involved_epts[0]->point->compute_cost3(temp,particle_conn_id); 
	  particle_costs+=involved_epts[1]->point->compute_cost3(temp,particle_conn_id); 
	  particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL);
	  particle_costs+=involved_epts[2]->point->compute_cost3(temp,particle_conn_id);
	  
	  
	
	  
	  
	  //the new terminal point's reward decreases (two sides conected -> only one side)  
// 	  T terminal_point_cost_change=terminal_candidate_point->compute_cost()*(particle_connection_state_cost_scale[1]-particle_connection_state_cost_scale[2])
// 	  +(particle_connection_state_cost_offset[1]-particle_connection_state_cost_offset[2]);
	  
	  T E=(new_edgecost-old_edgecost)/temp;
// 	  E+=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;

	  T R=mhs_fast_math<T>::mexp(E);
	  
	  
	  T flatten_bif_prop=conn_prop/(connections.get_num_bifurcation_centers()-1+std::numeric_limits< T >::epsilon());
	  // randomly select edge -> uniformly pick one side particle as center candidate -> uniformly pick a particle side
	  T bif_create_prop=1.0/(connections.pool->get_numpts())*(0.5)*(0.5); 
	  R*=flatten_bif_prop/((bif_create_prop)+std::numeric_limits<T>::epsilon());

	  
	  
	
	
	
	

	statistic[stat_count++]++;			
	
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	
// 	    {
// 	    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange2;
// 	    T new_edgecost=edgecost_fun.u_cost(*terminal_candidate_point->endpoints[0][0]->connected,
// 				  *terminal_candidate_point->endpoints[1][0]->connected,
// 				  *terminal_candidate_point->endpoints[side][0],
// 				  maxendpointdist, 
// 				  inrange2
// 				  );		    
// 	    sta_assert_error(inrange2==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	    }

	  
		
		// remove bifurcation connections
		class Connection<T,Dim> old_connections_backup[2];
		class Points<T,Dim>::CEndpoint * endpts[3]; 
		for (int slot=0;slot<2;slot++)
		{
		  endpts[slot]=terminal_candidate_point->endpoints[slot][0]->connected;
		  class Connection<T,Dim>  * delete_me=(terminal_candidate_point->endpoints[slot][0]->connection);
		  sta_assert_debug0(delete_me!=NULL);
		  old_connections_backup[slot]=*delete_me;
		  connections.remove_connection(delete_me);
		}
		endpts[2]=terminal_candidate_point->getfreehub(side);
		
		/*
		endpts_debug[0]=terminal_candidate_point->endpoints[0][0]->connected;
		endpts_debug[1]=terminal_candidate_point->endpoints[1][0]->connected;
		endpts_debug[2]=terminal_candidate_point->endpoints[side][0];*/
				  
				
		Points<T,Dim> * p_bif_center_point=particle_pool.create_obj();    
		Points<T,Dim> & bif_center_point=*p_bif_center_point;
		bif_center_point.set_direction(Vector<T,Dim>(1,0,0));
		bif_center_point.tracker_birth=3;	
		
		
		
		
		
		class Connection<T,Dim> a_new_connection;
		class Connection<T,Dim> * new_connections[3];
		a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
		for (int slot=0;slot<3;slot++)
		{
		  a_new_connection.pointA=bif_center_point.getfreehub(Points<T,Dim>::bifurcation_center_slot);
		  a_new_connection.pointB=endpts[slot];
		  new_connections[slot]=connections.add_connection(a_new_connection);
		}
		
		sta_assert_error((bif_center_point.endpoint_connections[Points<T,Dim>::bifurcation_center_slot])==3);
		
		sta_assert_error(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		
		bool out_of_bounce=false;
		bif_center_point.bifurcation_center_update(shape,out_of_bounce);
		
		
		
		if (out_of_bounce)
		{
		    sta_assert_debug0(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		    connections.remove_connection(new_connections[0]);
		    connections.remove_connection(new_connections[1]);
		    connections.remove_connection(new_connections[2]);
		    connections.add_connection(old_connections_backup[0]);
		    connections.add_connection(old_connections_backup[1]);
		    particle_pool.delete_obj(p_bif_center_point);
		    return false;
		  }

		  
		if (options.no_double_bifurcations)
		{
		    if (p_bif_center_point->connects_two_bif())
		    {
			  sta_assert_debug0(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			  connections.remove_connection(new_connections[0]);
			  connections.remove_connection(new_connections[1]);
			  connections.remove_connection(new_connections[2]);
			  connections.add_connection(old_connections_backup[0]);
			  connections.add_connection(old_connections_backup[1]);
			  particle_pool.delete_obj(p_bif_center_point);
			  return false;
		    }
		}
		  
		  
		  
		  p_bif_center_point->e_cost(
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange
	  );
// 		  printf("%u %u %u\n",endpts[0],endpts[1],endpts[2]);
// 		  printf("%u %u %u\n",endpts_debug[0],endpts_debug[1],endpts_debug[2]);
	 sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));

		
		
	      if (options.bifurcation_particle_hard)	
	      {
		if ((collision_fun_p.colliding( tree,bif_center_point,collision_search_rad,temp))
		  ||(!tree.insert(*p_bif_center_point)))
		{
// 		  printf("PUFF!! Bif Track Burth!\n");
		  sta_assert_debug0(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		  connections.remove_connection(new_connections[0]);
		  connections.remove_connection(new_connections[1]);
		  connections.remove_connection(new_connections[2]);
		    connections.add_connection(old_connections_backup[0]);
		    connections.add_connection(old_connections_backup[1]);
		    particle_pool.delete_obj(p_bif_center_point);
		    return false;
		}
	      }
	      
		
		
// 		if (options.bifurcation_particle_hard)	
// 		{
// 		 if(!tree.insert(*p_bif_center_point))
// 		 {
// 		   particle_pool.delete_obj(p_bif_center_point);
// 		    p_bif_center_point->print();
// 		    throw mhs::STAError("error insering point\n");
// 		 } 
// 		}
		
		if ((!conn_tree.insert(*terminal_candidate_point))||(!bifurcation_tree.insert(*p_bif_center_point)))
		{
		    sta_assert_debug0(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		    connections.remove_connection(new_connections[0]);
		    connections.remove_connection(new_connections[1]);
		    connections.remove_connection(new_connections[2]);
		    connections.add_connection(old_connections_backup[0]);
		    connections.add_connection(old_connections_backup[1]);
		    particle_pool.delete_obj(p_bif_center_point);
		    return false;
		}
		
// 		if(!bifurcation_tree.insert(*p_bif_center_point))
// 		{
// 		  particle_pool.delete_obj(p_bif_center_point);
// 		  throw mhs::STAError("error insering point\n");
// 		}
		
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		  #ifdef BIFURCATION_DEBUG_EDGECHECK
		// 	for (int i=0;i<3;i++)
		// 	{
		// 	new_connections[i]->compute_cost(
		// 		    edgecost_fun,
		// 	      options.connection_bonus_L,
		// 	      options.bifurcation_bonus_L,
		// 	      maxendpointdist,
		// 	      inrange,false,bifurcation_edgecheck_update);
		// 	    
		// 	    /// of course the old edge should be in range (valid)
		// 	    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		// 	}
			  (p_bif_center_point->e_cost(edgecost_fun,
					options.connection_bonus_L,
				      options.bifurcation_bonus_L,
				      inrange,true,false,bifurcation_edgecheck_update));
			    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		      #endif
		
		#ifdef  D_USE_GUI
		  p_bif_center_point->touch(get_type());
		  for (int i=0;i<3;i++)
		  {
		      p_bif_center_point->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		  }
		  terminal_candidate_point->touch(get_type());
		#endif	
		
	    return true;
	  }

	statistic[stat_count++]++;
	return false;
	
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
	
	
      
      
      return false;
    }
};
































template<typename TData,typename T,int Dim>
class CProposalBifurcationReconn : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
      T ConnectEpsilon;
    const static int max_stat=10;
    int statistic[max_stat];
public:

     T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }


    CProposalBifurcationReconn(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
    }

    ~CProposalBifurcationReconn()
    {
//         printf("%s: ",enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN).c_str());
//         for (int i=0; i<max_stat; i++)
//             printf("%d ",statistic[i]);
//         printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN);
   };
   
   
   PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	

        this->proposal_called++;
	
	
	bool debug=false;
	
	/// no bifurcations?
	if (connections.get_num_bifurcation_centers()<1)
	  return false;


	int stat_count=0;
//0	
statistic[stat_count++]++;	
	
	try
	{
if (debug)	
  printf("[");	  
	/// randomly pic a bifurcation center node
	Points<T,Dim> &  point=connections.get_rand_bifurcation_center_point();	  
	
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
	//uniformly pick an edge that is supposed to be changed
	int remove_edge=std::rand()%3;
	
	class Points<T,Dim> * new_terminal_point=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connected->point;
	
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	
	
	class Points<T,Dim>::CEndpoint * remaining_endpoints[2];
	
	
	if (!options.bifurcation_neighbors)
	{
	  class Points<T,Dim>::CEndpoint * opposite_endpt=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connected->opposite_slots[0];
	  
	  
	  
	  if (opposite_endpt->connected!=NULL)
	  {
	    sta_assert_error(opposite_endpt->connection->edge_type==EDGE_TYPES::EDGE_SEGMENT);
	  }
	  //NOTE the statements below make no sense I guess. So I added the assertion above
	 /* 
	 if ((opposite_endpt->connected!=NULL)&&(opposite_endpt->connection->edge_type!=EDGE_TYPES::EDGE_SEGMENT))
	 return false; 
	 */
	 
	}
	
if (debug)	
  printf("@");		
	
	int j=0;
	for (int i=0;i<3;i++)
	{
	  if (i!=remove_edge)
	  {
	    sta_assert_debug0(j<2);
	    remaining_endpoints[j]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected;
	    j++;
	  }
	}

	
	class Points<T,Dim>::CEndpoint * old_and_proposed_endpoint[2];
	old_and_proposed_endpoint[0]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connected;

	T connection_prob[2];
	T connection_costs[2];

	
	 
	
//1	
statistic[stat_count++]++;

if (debug)	
  printf("*");	

	if (!CProposalBifurcationBirthSimple<TData,T,Dim>::do_tracking3(
		    edgecost_fun,
		    old_and_proposed_endpoint,
		    connection_prob,
		    connection_costs,
		    conn_tree,
		    options.bifurcation_neighbors,
		    remaining_endpoints[0],
		    remaining_endpoints[1],
		    search_connection_point_center_candidate,
		    //options.connection_candidate_searchrad,
		    conn_temp,
		    options.bifurcation_bonus_L,
		    Lprior,
		     ConnectEpsilon,
		    2))
	{
	  sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	    return false;
	}
	
	
	
	
	

if (debug)	
  printf("^");		
//2	
statistic[stat_count++]++;	
	
// 	printf("%f %f\n",connection_costs,old_edgecost);
	T & old_edgecost=connection_costs[0];
	T & new_edgecost=connection_costs[1];
	class Points<T,Dim> * new_bif_point=old_and_proposed_endpoint[1]->point;
	
	sta_assert_debug0((point.endpoint_connections[Points<T,Dim>::bifurcation_center_slot])==3);
	
	
if (debug)	
  printf("^");	
	  T particle_costs=0;
	  particle_costs-=old_and_proposed_endpoint[0]->point->compute_cost3(temp);  //old bif point
if (debug)	
  printf("^");	  
	  particle_costs-=old_and_proposed_endpoint[1]->point->compute_cost3(temp);  //new bif point
	  
	  
if (debug)	
  printf("^");		
	
	//backup old edge 
	class Connection< T, Dim >  backup_old_edge;
	class Connection< T, Dim > * delete_me=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connection;
	backup_old_edge=*delete_me;
	
	//remove old edge
	connections.remove_connection(delete_me);
	
if (debug)	
  printf("^");		
	
	//add new edge
	class Connection<T,Dim> a_new_connection;
	a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
	a_new_connection.pointA=point.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	a_new_connection.pointB=old_and_proposed_endpoint[1];
	      
	sta_assert_debug0(a_new_connection.pointB->point!=a_new_connection.pointA->point);

	sta_assert_debug0((point.endpoint_connections[Points<T,Dim>::bifurcation_center_slot])==2);
	
	    if (Connections<T,Dim>::connection_exists(*(a_new_connection.pointA->point),*(a_new_connection.pointB->point)))
	    {
		connections.add_connection(backup_old_edge);
		return false;
	    }
	
	
	class Connection< T, Dim > * p_new_edge;      
	p_new_edge=connections.add_connection(a_new_connection);
	
	sta_assert_debug0((point.endpoint_connections[Points<T,Dim>::bifurcation_center_slot])==3);
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
if (debug)	
  printf("^");	

	  particle_costs+=old_and_proposed_endpoint[0]->point->compute_cost3(temp);  //old bif point
	  particle_costs+=old_and_proposed_endpoint[1]->point->compute_cost3(temp);  //new bif point
	  
	/*std::size_t particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT);
	  particle_costs+=involved_epts[0]->point->compute_cost3(temp,particle_conn_id); 
	  particle_costs+=involved_epts[1]->point->compute_cost3(temp,particle_conn_id); 
	  particle_conn_id=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL);
	  particle_costs+=involved_epts[2]->point->compute_cost3(temp,particle_conn_id);*/  
	  
	  //T terminal_point_cost_change=(new_terminal_point->compute_cost()-new_bif_point->compute_cost())*(particle_connection_state_cost_scale[1]-particle_connection_state_cost_scale[2]);

	if (debug)	
  printf("^");	  
	  
	T E=(new_edgecost-old_edgecost)/temp;
	  //E+=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;

          T R=mhs_fast_math<T>::mexp(E);
	
	T create_connection_prob=connection_costs[1]/connections.pool->get_numpts(); 
	T remove_connection_prob=connection_costs[0]/(connections.get_num_bifurcation_centers()-1);
	R*=create_connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());
if (debug)	
  printf("y");
	
//3
statistic[stat_count++]++;		

	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
		bool out_of_bounce=false;
		
		
		if (options.no_double_bifurcations)
		{
		    if (point.connects_two_bif())
		    {
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(p_new_edge);
			connections.add_connection(backup_old_edge);
			point.bifurcation_center_update(shape,out_of_bounce);
			sta_assert_error(!out_of_bounce);
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			  return false;
		    }
		}
	    
	    
		
		point.bifurcation_center_update(shape,out_of_bounce);
	    
		
		if (out_of_bounce)
		{
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(p_new_edge);
			connections.add_connection(backup_old_edge);
			point.bifurcation_center_update(shape,out_of_bounce);
			sta_assert_error(!out_of_bounce);
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		    return false;
		}
		
		
		if (options.bifurcation_particle_hard)	
		{
		  if (collision_fun_p.colliding( tree,point,collision_search_rad,temp))
		  {
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(p_new_edge);
			connections.add_connection(backup_old_edge);
			point.bifurcation_center_update(shape,out_of_bounce);
			sta_assert_error(!out_of_bounce);
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			    return false;
		  }
		}

		// check for loops by following new edge
		if (Constraints::constraint_hasloop_follow(point,p_new_edge,options.constraint_loop_depth+1))
		{
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(p_new_edge);
			connections.add_connection(backup_old_edge);
			point.bifurcation_center_update(shape,out_of_bounce);
			sta_assert_error(!out_of_bounce);
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			    return false;
		}
		
		
	    
		 
		if (!conn_tree.insert(*new_terminal_point,true))
		{
		    new_terminal_point->print();
		    throw mhs::STAError("error insering point\n");
		    return false;
		}
		if ((new_bif_point->endpoint_connections[0]>0)&&
		(new_bif_point->endpoint_connections[1]>0))
		{
		    new_bif_point->unregister_from_grid(voxgrid_conn_can_id);
		    sta_assert_debug0(new_bif_point->endpoint_connections[0]>0);
		    sta_assert_debug0(new_bif_point->endpoint_connections[1]>0);
		}
		sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);

		
		#ifdef  D_USE_GUI
		point.touch(get_type());
		for (int i=0;i<3;i++)
		{
		      point.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		}
		new_terminal_point->touch(get_type());
		#endif
		
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;

		
		  #ifdef BIFURCATION_DEBUG_EDGECHECK
// 			for (int i=0;i<3;i++)
// 			{
// 			p_new_edge->compute_cost(
// 				    edgecost_fun,
// 			      options.connection_bonus_L,
// 			      options.bifurcation_bonus_L,
// 			      maxendpointdist,
// 			      inrange,false,bifurcation_edgecheck_update);
// 			    
// 			    /// of course the old edge should be in range (valid)
// 			    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 			}
			(point.e_cost(edgecost_fun,
				      options.connection_bonus_L,
				    options.bifurcation_bonus_L,
				    inrange,true,false,bifurcation_edgecheck_update));
			  sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    #endif
		
		
if (debug)	
  printf("]\n");		
	    return true;
	  }
//4	  
statistic[stat_count++]++;		  
	

	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			connections.remove_connection(p_new_edge);
			connections.add_connection(backup_old_edge);
			bool out_of_bounce=false;
			point.bifurcation_center_update(shape,out_of_bounce);
			sta_assert_error(!out_of_bounce);
			   
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
if (debug)	
  printf("0]\n");	
	
	
	
	return false;
	
	
	
	
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }  
	
	
	
    }

};








































template<typename TData,typename T,int Dim>
class CProposalBifurcationCrossingBirth: public CProposal<TData,T,Dim>
{

protected:
    T Lprior;
    const static int max_stat=10;
    int statistic[max_stat];
      T ConnectEpsilon;
public:


       T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  
    typedef class CProposalBifurcationCrossingDeath<TData,T,Dim>::Bif2Conn Bif2Conn;

    CProposalBifurcationCrossingBirth(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
	
	ConnectEpsilon=0.0001;
    }



    ~CProposalBifurcationCrossingBirth()
    {
//         printf("\n");
//         for (int i=0; i<max_stat; i++)
//             printf("%d ",statistic[i]);
//         printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_C);
    };
    
    T get_default_weight() {
        return 0;
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_C);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
		 if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;
	
	try {
	  
	    
	    sta_assert_error(options.bifurcation_neighbors);
	  
	    class Connection<T,Dim> * existing_connection;

	    /// randomly choose an edge
	    existing_connection=connections.get_rand_edge();
	    if (existing_connection==NULL)
		return false;

	    sta_assert_debug0(existing_connection->pointA->connected!=NULL);
	    sta_assert_debug0(existing_connection->pointB->connected!=NULL);
	    
	    if (existing_connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
	      return false;
	    
	    
	    Bif2Conn   proposed_endpoint;
	    
	    proposed_endpoint.endptsA[0]=existing_connection->pointA;
	    proposed_endpoint.endptsA[1]=NULL;
	    proposed_endpoint.endptsA[2]=NULL;
	    
	    proposed_endpoint.endptsB[0]=existing_connection->pointB;
	    proposed_endpoint.endptsB[1]=NULL;
	    proposed_endpoint.endptsB[2]=NULL;
	    proposed_endpoint.center_ptB=NULL;
	      
	    
// 	     printf("Search\n");
	    
	    T connection_prob_new;
	    if (!search_candidates(
		      edgecost_fun,
		      &proposed_endpoint,
		      &connection_prob_new,
		      tree,
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad
				  ))
	  {
	    
	      return false;
	  }
	  
	  
// 	  return false;
// 	  printf("Success\n");
	  
// 	  printf("%f \n",search_connection_point_center_candidate);

            if (connection_prob_new<std::numeric_limits<T>::epsilon())
                return false;

	    
	    if (
	      (Connections<T,Dim>::connection_exists(*(proposed_endpoint.endptsA[1]->point),*(proposed_endpoint.endptsA[2]->point)))||
	      (Connections<T,Dim>::connection_exists(*(proposed_endpoint.endptsA[0]->point),*(proposed_endpoint.endptsA[1]->point)))||
	      (Connections<T,Dim>::connection_exists(*(proposed_endpoint.endptsA[0]->point),*(proposed_endpoint.endptsA[2]->point)))||
	      (Connections<T,Dim>::connection_exists(*(proposed_endpoint.endptsB[0]->point),*(proposed_endpoint.endptsB[1]->point)))||
	      (Connections<T,Dim>::connection_exists(*(proposed_endpoint.endptsB[0]->point),*(proposed_endpoint.endptsB[2]->point)))||
	      (Connections<T,Dim>::connection_exists(*(proposed_endpoint.endptsB[1]->point),*(proposed_endpoint.endptsB[2]->point))))
	    {
		return false;
	    }
	    
	
	  
	   T particle_cost_new=0;
	   T particle_cost_old=0;
	   
	   for (int slot=0;slot<3;slot++)
	   {
	    particle_cost_old+=proposed_endpoint.endptsA[slot]->point->compute_cost3(temp);
	    particle_cost_old+=proposed_endpoint.endptsB[slot]->point->compute_cost3(temp);
	  }
	  
	 
// 	  printf("Cleaning\n");
	     
	  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;  
// 	  T  connection_costs_old=0;
	   class Connection<T,Dim> old_connections[3];
	   
	  for (int slot=0;slot<3;slot++)
	  {
	    class Connection<T,Dim> * delete_me=(proposed_endpoint.endptsA[slot]->connection);
	    sta_assert_debug0(delete_me!=NULL);
	    old_connections[slot]=*delete_me;
	    connections.remove_connection(delete_me);
	  }
	  
	  
// 	  printf("creating\n");
	  
	  class Connection<T,Dim> * new_connections[2][3];
	  class Connection<T,Dim> a_new_connection;
	  a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
	  
	  // create two bifurcation particles
	  Points<T,Dim> * p_b_particles[2];
	  for (int p=0;p<2;p++)
	  {
	    p_b_particles[p]=particle_pool.create_obj();    
	    p_b_particles[p]->tracker_birth=3;
	    p_b_particles[p]->set_direction(Vector<T,Dim>(1,0,0));
	  }

	  sta_assert_debug0(proposed_endpoint.endptsA[0]->point!=proposed_endpoint.endptsA[1]->point);
	  sta_assert_debug0(proposed_endpoint.endptsA[2]->point!=proposed_endpoint.endptsA[1]->point);
	  sta_assert_debug0(proposed_endpoint.endptsA[2]->point!=proposed_endpoint.endptsA[0]->point);
	  sta_assert_debug0(proposed_endpoint.endptsB[0]->point!=proposed_endpoint.endptsB[1]->point);
	  sta_assert_debug0(proposed_endpoint.endptsB[2]->point!=proposed_endpoint.endptsB[1]->point);
	  sta_assert_debug0(proposed_endpoint.endptsB[2]->point!=proposed_endpoint.endptsB[0]->point);
	  
	  for (int edge=0;edge<3;edge++)
	  {
	     sta_assert_debug0(proposed_endpoint.endptsA[edge]->connected==NULL);
	     a_new_connection.pointB=p_b_particles[0]->getfreehub(Points<T,Dim>::bifurcation_center_slot);
	     a_new_connection.pointA=proposed_endpoint.endptsA[edge]->point->getfreehub(proposed_endpoint.endptsA[edge]->side);
	     sta_assert_debug0(a_new_connection.pointA->point->endpoint_connections[proposed_endpoint.endptsA[edge]->side]==0);
	     
	     // after adding and removing edges we need to update the pointers (probably not neccessary)
	     proposed_endpoint.endptsA[edge]=a_new_connection.pointA;
//  	     printf("A %d\n",edge);
	     new_connections[0][edge]=connections.add_connection(a_new_connection);
	     
	     sta_assert_debug0(a_new_connection.pointA->point->endpoint_connections[proposed_endpoint.endptsA[edge]->side]==1);
	     
#ifdef _DEBUG_CHECKS_0_
	      switch(edge) 
	      {
		case 0:
		  sta_assert_error(p_b_particles[0]->particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
		  break;
		case 1:
		  sta_assert_error(p_b_particles[0]->particle_type==(PARTICLE_TYPES::PARTICLE_SEGMENT));
		  break;
		case 2:
		  sta_assert_error(p_b_particles[0]->particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
		  break;  
	      }
    #endif	
	  }


	  
	  for (int edge=0;edge<3;edge++)
	  {
	     sta_assert_debug0(proposed_endpoint.endptsB[edge]->connected==NULL);	     
	     
	     // setting bifurcation particle to B allows us to use loop follow
	     a_new_connection.pointB=p_b_particles[1]->getfreehub(Points<T,Dim>::bifurcation_center_slot);
	     a_new_connection.pointA=proposed_endpoint.endptsB[edge]->point->getfreehub(proposed_endpoint.endptsB[edge]->side);
	     
	     sta_assert_debug0(a_new_connection.pointA->point->endpoint_connections[proposed_endpoint.endptsB[edge]->side]==0);
	     
	     // after adding and removing edges we need to update the pointers (probably not neccessary)
	     proposed_endpoint.endptsB[edge]=a_new_connection.pointA;
//  	     printf("B %d\n",edge);
	     new_connections[1][edge]=connections.add_connection(a_new_connection);
	     
	     sta_assert_debug0(a_new_connection.pointA->point->endpoint_connections[proposed_endpoint.endptsB[edge]->side]==1);
	     #ifdef _DEBUG_CHECKS_0_
	      switch(edge) 
	      {
		case 0:
		  sta_assert_error(p_b_particles[1]->particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
		  break;
		case 1:
		  sta_assert_error(p_b_particles[1]->particle_type==(PARTICLE_TYPES::PARTICLE_SEGMENT));
		  break;
		case 2:
		  sta_assert_error(p_b_particles[1]->particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
		  break;  
	      }
	        #endif
	  }
// 	  printf("OK\n");
	  
	  	  
	  #ifdef _DEBUG_CHECKS_0_
	      for (int i=0;i<3;i++)
		sta_assert_debug0((p_b_particles[0]->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->particle_type)==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
	      for (int i=0;i<3;i++)
		sta_assert_debug0((p_b_particles[1]->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->particle_type)==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
	    #endif
/*	  printf("Undo 1\n");
		    connections.remove_connection(new_connections[0][0]);
		    connections.remove_connection(new_connections[0][1]);
		    connections.remove_connection(new_connections[0][2]);
		    connections.remove_connection(new_connections[1][0]);
		    connections.remove_connection(new_connections[1][1]);
		    connections.remove_connection(new_connections[1][2]);
	printf("Undo 2\n");	    
		    connections.add_connection(old_connections[0]);
		    connections.add_connection(old_connections[1]);
		    connections.add_connection(old_connections[2]);
	printf("Undo 3\n");	    		    
		    particle_pool.delete_obj(p_b_particles[0]);
		    particle_pool.delete_obj(p_b_particles[1]);
	printf("Undo 4\n");	    		    
		    return false;*/	      
	      
	      
	      
	  for (int p=0;p<2;p++)
	  {
	  	bool out_of_bounce=false;
		p_b_particles[p]->bifurcation_center_update(shape,out_of_bounce);
		if (out_of_bounce)
		{
		    connections.remove_connection(new_connections[0][0]);
		    connections.remove_connection(new_connections[0][1]);
		    connections.remove_connection(new_connections[0][2]);
		    connections.remove_connection(new_connections[1][0]);
		    connections.remove_connection(new_connections[1][1]);
		    connections.remove_connection(new_connections[1][2]);
		    
		    connections.add_connection(old_connections[0]);
		    connections.add_connection(old_connections[1]);
		    connections.add_connection(old_connections[2]);
		    particle_pool.delete_obj(p_b_particles[0]);
		    particle_pool.delete_obj(p_b_particles[1]);
		    return false;
		}
	  }
	  
//  printf("POPE 01\n");	   
	  
	   for (int slot=0;slot<3;slot++)
	   {
	    particle_cost_new+=proposed_endpoint.endptsA[slot]->point->compute_cost3(temp);
	    particle_cost_new+=proposed_endpoint.endptsB[slot]->point->compute_cost3(temp);
	   }

	   
/*printf("POPE 02\n");*/	   	   
	  T connection_costs_new=0;
	  for (int p=0;p<2;p++)
	  {
	    connection_costs_new+=p_b_particles[p]->e_cost(
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange);
	    
	    if (inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE)
	    {
	      printf("%f\n",(*p_b_particles[p]->endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->position-*p_b_particles[p]->endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected->position).norm2()); 
	      printf("%f\n",(*p_b_particles[p]->endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->position-*p_b_particles[p]->endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected->position).norm2()); 
	      printf("%f\n",(*p_b_particles[p]->endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected->position-*p_b_particles[p]->endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected->position).norm2()); 
	      printf("%f\n",maxendpointdist); 
	    }
	    
	    sta_assert_debug0((inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
	    
	    if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	    {
	          connections.remove_connection(new_connections[0][0]);
		    connections.remove_connection(new_connections[0][1]);
		    connections.remove_connection(new_connections[0][2]);
		    connections.remove_connection(new_connections[1][0]);
		    connections.remove_connection(new_connections[1][1]);
		    connections.remove_connection(new_connections[1][2]);
		    
		    connections.add_connection(old_connections[0]);
		    connections.add_connection(old_connections[1]);
		    connections.add_connection(old_connections[2]);
		    particle_pool.delete_obj(p_b_particles[0]);
		    particle_pool.delete_obj(p_b_particles[1]);
		    
// 		    printf("POPE 02 failed\n");	
		    return false;
	    }
	  }
	   
	   proposed_endpoint.center_ptB=p_b_particles[1];
	   T connection_prob_old;
	 T connection_costs_old=0;
	 
/*printf("OK 1\n");*/	 
	    if (!CProposalBifurcationCrossingDeath<TData,T,Dim>::search_candidates(
		      edgecost_fun,
		      &proposed_endpoint,
		      &connection_prob_old,
		      &connection_costs_old,
		      bifurcation_tree,
		      *p_b_particles[0],
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
 		      conn_temp,
		      options.connection_bonus_L,
		      Lprior,ConnectEpsilon,
		      1))
	  {
	      sta_assert_error(false);
	      return false;
	  }    
	   
/*printf("OK 2\n");*/	  
 	      T E=(connection_costs_new-connection_costs_old)/temp;
 
 	      E+=(particle_cost_new-particle_cost_old)/temp;
 	      
 	      T R=mhs_fast_math<T>::mexp(E);
	      
	      
	      T BifurcationCrossingDeath_prop=connection_prob_old/(connections.get_num_bifurcation_centers()+std::numeric_limits< T >::epsilon());
	      T BifurcationCrossingBirth_prop=connection_prob_new/(connections.pool->get_numpts()-3+std::numeric_limits< T >::epsilon());
		  
// 	      printf("%f %f %f %f\n",
// 		BifurcationCrossingDeath_prop,
// 		BifurcationCrossingBirth_prop,
// 		connection_prob_old,
// 		connection_prob_new
// 	      );
	      
// 	      printf("%f %f | %f %f | %f %f ^ %f %f \n",
// 		     E,R,
// 	      (connection_costs_new-connection_costs_old),
// 		     (particle_cost_new-particle_cost_old),
// 		     connection_costs_new,connection_costs_old,
// 		     particle_cost_new,particle_cost_old
// 	      );
	      
	      R*=BifurcationCrossingDeath_prop/(BifurcationCrossingBirth_prop+std::numeric_limits<T>::epsilon());
	      
	      
	      
	      
	      if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	      {

/*printf("OK A00\n");*/		
		for (int b=0;b<2;b++)
		{
		  /*printf("%d\n",b);*/		
		  //if ((Constraints::constraint_hasloop_follow(*p_b_particles[b],new_connections[b][0],options.constraint_loop_depth+1))||
		  //if ((Constraints::constraint_hasloop_follow(*(new_connections[b][0]->pointA->point),new_connections[b][0],options.constraint_loop_depth+1))||
		  if ((Constraints::constraint_hasloop(*p_b_particles[b],options.constraint_loop_depth+1))||
		    ((options.bifurcation_particle_hard)&&(collision_fun_p.colliding( tree,*p_b_particles[b],collision_search_rad,temp))||
		      ((options.no_double_bifurcations)&&(p_b_particles[b]->connects_two_bif()))
		    )
		  )
		  {
		/*    printf("fail\n");	*/			    
		    connections.remove_connection(new_connections[0][0]);
		    connections.remove_connection(new_connections[0][1]);
		    connections.remove_connection(new_connections[0][2]);
		    connections.remove_connection(new_connections[1][0]);
		    connections.remove_connection(new_connections[1][1]);
		    connections.remove_connection(new_connections[1][2]);
		    
		    connections.add_connection(old_connections[0]);
		    connections.add_connection(old_connections[1]);
		    connections.add_connection(old_connections[2]);
		    particle_pool.delete_obj(p_b_particles[0]);
		    particle_pool.delete_obj(p_b_particles[1]);
// 		    printf("return\n");
		    return false;
		  }

		}
		
// 		for (int b=0;b<2;b++)
// 		{
// 		  /*printf("%d\n",b);*/		
// 		  //if ((Constraints::constraint_hasloop_follow(*p_b_particles[b],new_connections[b][0],options.constraint_loop_depth+1))||
// 		 if ((Constraints::constraint_hasloop(*(new_connections[b][0]->pointB->point),5)))
// 		 {
// 		   bool checkA1=Constraints::constraint_hasloop_follow(*(new_connections[b][0]->pointA->point),new_connections[b][0],5);
// 		   bool checkA2=Constraints::constraint_hasloop_follow(*(new_connections[b][1]->pointA->point),new_connections[b][1],5);
// 		   bool checkA3=Constraints::constraint_hasloop_follow(*(new_connections[b][2]->pointA->point),new_connections[b][2],5);
// 		   
// 		   bool checkB1=Constraints::constraint_hasloop_follow(*(new_connections[1-b][0]->pointA->point),new_connections[b][0],5);
// 		   bool checkB2=Constraints::constraint_hasloop_follow(*(new_connections[1-b][1]->pointA->point),new_connections[b][1],5);
// 		   bool checkB3=Constraints::constraint_hasloop_follow(*(new_connections[1-b][2]->pointA->point),new_connections[b][2],5);
// 
// 		   printf("%d %d %d %d %d %d\n",checkA1,checkA2,checkA2,checkB1,checkB2,checkB2);   
// 		 }
// 		}
		
/*printf("OK A0\n");*/		
		
		  for (int b=0;b<2;b++)
		  {
		    Points<T,Dim> & b_particle=*p_b_particles[b];
// 		    if (options.bifurcation_particle_hard)	
// 		    {
// 		      if(!tree.insert(b_particle))
// 		      {
// 			particle_pool.delete_obj(p_b_particles[0]);
// 			particle_pool.delete_obj(p_b_particles[1]);
// 			throw mhs::STAError("error insering point\n");
// 		      }
// 		    }
// 		    if(!bifurcation_tree.insert(b_particle))
// 		    {
// 		      particle_pool.delete_obj(p_b_particles[0]);
// 		      particle_pool.delete_obj(p_b_particles[1]);
// 		      throw mhs::STAError("error insering point\n");
// 		    }
		    if (((options.bifurcation_particle_hard)&&(!tree.insert(b_particle))) || (!bifurcation_tree.insert(b_particle)))
		    {
		      connections.remove_connection(new_connections[0][0]);
		      connections.remove_connection(new_connections[0][1]);
		      connections.remove_connection(new_connections[0][2]);
		      connections.remove_connection(new_connections[1][0]);
		      connections.remove_connection(new_connections[1][1]);
		      connections.remove_connection(new_connections[1][2]);
		      
		      connections.add_connection(old_connections[0]);
		      connections.add_connection(old_connections[1]);
		      connections.add_connection(old_connections[2]);
		      particle_pool.delete_obj(p_b_particles[0]);
		      particle_pool.delete_obj(p_b_particles[1]);
		      return false;
		    }
		}
		
		
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		#ifdef  D_USE_GUI
		for (int b=0;b<2;b++)
		  {
		    Points<T,Dim> & b_particle=*p_b_particles[b];
		    b_particle.touch(get_type());
		    for (int i=0;i<3;i++)
		    {
			b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		    }
		  }
		#endif	
		
/*printf("OK A\n");*/		
		return true;
	      }
	      
/*printf("OK B\n");*/	      
	  connections.remove_connection(new_connections[0][0]);
	  connections.remove_connection(new_connections[0][1]);
	  connections.remove_connection(new_connections[0][2]);
	  connections.remove_connection(new_connections[1][0]);
	  connections.remove_connection(new_connections[1][1]);
	  connections.remove_connection(new_connections[1][2]);
	  
	  connections.add_connection(old_connections[0]);
	  connections.add_connection(old_connections[1]);
	  connections.add_connection(old_connections[2]);
	  particle_pool.delete_obj(p_b_particles[0]);
	  particle_pool.delete_obj(p_b_particles[1]);
      	      
/*printf("OK C\n");*/	  
	      return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
	
    }
    
    
    static bool search_candidates(
       class CEdgecost<T,Dim> & edgecost_fun,
        Bif2Conn   * proposed_endpoint, 
        T *  connection_prob,
        OctTreeNode<T,Dim> & tree,
        T search_connection_point_center_candidate, 
        T searchrad, 
//         T TempConnect,
//         T lineL,
//         T Lprior,
// 	T ConnectEpsilon,
        int mode=0)
    {
      
	std::size_t total_candidates;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;
	
 	Vector<T,Dim> edge_center=((*proposed_endpoint->endptsA[0]->position)+(*proposed_endpoint->endptsB[0]->position))*T(0.5);
	T edge_length=std::sqrt(((*proposed_endpoint->endptsA[0]->position)-(*proposed_endpoint->endptsB[0]->position)).norm2());
	
        try {
            tree.queryRange(
                edge_center,
                search_connection_point_center_candidate+edge_length*T(0.5),
                candidates,
                query_buffer_size,
                total_candidates);
        } catch (mhs::STAError & error)
        {
            throw error;
        }

        sta_assert_debug0(total_candidates>1);
        /// no enough points for two bifurcations
        if (total_candidates<6)
        {
//             if (debug)
//                 printf("no points\n",found);
            return false;
        }
        
        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;

	 #ifdef D_USE_MULTITHREAD
	  int thread_id= omp_get_thread_num();
	 #else
	  int thread_id= 0;
	 #endif
	track_unique_id[thread_id]++;
	
	
	class Points<T,Dim>::CEndpoint * enpts[2];
	enpts[0]=proposed_endpoint->endptsA[0];
	enpts[1]=proposed_endpoint->endptsB[0];
	
	class Points<T,Dim> * pts[2];
	pts[0]=proposed_endpoint->endptsA[0]->point;
	pts[1]=proposed_endpoint->endptsB[0]->point;
	
	Connection<T,Dim> * setup_candidates_buffer[2][query_buffer_size];
	std::size_t setup_candidates_founds[2];
	setup_candidates_founds[0]=setup_candidates_founds[1]=0;
// 	Connection<T,Dim> * setup_candidates=setup_candidates_buffer;
/*printf("0");  */      
        //search bifurcation2edges configurations
// 	std::size_t num_point_candidates=0;
        {
	    for (std::size_t a=0; a<total_candidates; a++)
	    {
		Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		if ((point!=pts[0])&&
		    (point!=pts[1])&&
		    (point->get_num_connections()>1)&&
		    (point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		   )
		{
		  
		  
		  
		  for (int pointside=0; pointside<2; pointside++)
		  {
			class Points<T,Dim>::CEndpoint * endpointsA=point->endpoints[pointside][0];
			class Connection<T,Dim> * connection=endpointsA->connection;
			
			  
			
			  // no edge
			  if (point->endpoint_connections[pointside]==0)  
			    continue;
			  
			  if ((connection->track_me_id==track_unique_id[thread_id])||
			    (connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
			    continue;
			  
			  if ((connection->points[0]->point==pts[0])||
			      (connection->points[0]->point==pts[1])||
			    (connection->points[1]->point==pts[0])||
			      (connection->points[1]->point==pts[1])
			  )
			    continue;
			    
			  
			  connection->track_me_id=track_unique_id[thread_id];
			  
			  // an edge must be there
			  sta_assert_debug0(point->endpoint_connections[pointside]==1);		
			  sta_assert_debug0((endpointsA->connected!=NULL));			
			  sta_assert_debug0((connection!=NULL));	
			  bool edge_in_Range=true;
			  for (int enpt=0; enpt<2; enpt++)
			  {
			    
			      for (int connection_side=0; connection_side<2; connection_side++)
			      {   
				T dist_pts=((enpts[enpt]->point->get_position())-(connection->points[connection_side]->point->get_position())).norm2();
				if (!(search_connection_point_center_candidate>dist_pts))
				{
				  edge_in_Range=false;
				  break;
				}
				T dist_epts=((*enpts[enpt]->position)-(*connection->points[connection_side]->position)).norm2();
				if (!(searchrad>dist_epts))
				{
				  edge_in_Range=false;
				  break;
				}
			      }
			      if (!edge_in_Range)
				break;
			  }
			    
			  if (edge_in_Range)
			  {
			    setup_candidates_buffer[pointside][setup_candidates_founds[pointside]]=connection;
			    setup_candidates_founds[pointside]++;
			  }
		  }
		}
	    }
	}
/*printf("1");  */      	
	if (mode==1)
	{
	  connection_prob[0]=T(1)/(T((1+setup_candidates_founds[0])*(1+setup_candidates_founds[1])));  
	  return true;
	}
	
	
	if ((setup_candidates_founds[0]<1)||(setup_candidates_founds[1]<1))
	  return false;
	
	
	
	std::size_t edge_candidateA=std::rand()%setup_candidates_founds[0];
	std::size_t edge_candidateB=std::rand()%setup_candidates_founds[1];
	
	if (setup_candidates_buffer[0][edge_candidateA]==setup_candidates_buffer[1][edge_candidateB])
	  return false;
	
	connection_prob[0]=T(1)/(T(setup_candidates_founds[0]*setup_candidates_founds[1]));
	
/*printf("2"); */       	
	
	proposed_endpoint->endptsA[1]=setup_candidates_buffer[0][edge_candidateA]->pointA;
	proposed_endpoint->endptsA[2]=setup_candidates_buffer[1][edge_candidateB]->pointA;
	proposed_endpoint->endptsB[1]=setup_candidates_buffer[0][edge_candidateA]->pointB;
	proposed_endpoint->endptsB[2]=setup_candidates_buffer[1][edge_candidateB]->pointB;
	
	
	//we haven't test for valid distances of the newedges
	if (!(((*proposed_endpoint->endptsA[1]->position-*proposed_endpoint->endptsA[2]->position).norm2())<searchrad))
	  return false;
	if (!(((*proposed_endpoint->endptsB[1]->position-*proposed_endpoint->endptsB[2]->position).norm2())<searchrad))
	  return false;
	
	
	sta_assert_debug0(
	      ((proposed_endpoint->endptsB[1]->point
	      !=proposed_endpoint->endptsA[1]->point)&&
		(proposed_endpoint->endptsB[1]->point
	      !=proposed_endpoint->endptsA[2]->point))||
	      ((proposed_endpoint->endptsB[2]->point
	      !=proposed_endpoint->endptsA[1]->point)&&
		(proposed_endpoint->endptsB[2]->point
	      !=proposed_endpoint->endptsA[2]->point)));
	
	
	//if (Connections<T,Dim>::connection_exists(*proposed_endpoint->endptsA[1]))
	
	
	try {
	  
// 	  sta_assert_error();

/*printf("3");*/	  
	if ((!(proposed_endpoint->endptsA[1]->point
	      !=proposed_endpoint->endptsA[2]->point))
	   )
	{
	  sta_assert_debug0(proposed_endpoint->endptsA[1]->side!=proposed_endpoint->endptsA[2]->side);
/*printf("4");	*/  	  
	  return false;
	}

/*printf("5");	*/  	  	
	if ((!(proposed_endpoint->endptsB[1]->point
	      !=proposed_endpoint->endptsB[2]->point))
	   )
	{
	  
	  sta_assert_debug0(proposed_endpoint->endptsB[1]->side!=proposed_endpoint->endptsB[2]->side);
/*printf("6");	  	  	  

printf("7");*/	  	  
	  return false;
	}
	
	 } catch (mhs::STAError & error)
        {
            throw error;
        }
	
/*printf("end");*/        	
// 	if ((proposed_endpoint->endptsA[1]->opposite_slots[0]->connected!=NULL)&&
// 	    (proposed_endpoint->endptsA[2]->opposite_slots[0]->connected!=NULL)&&
// 	    (!(proposed_endpoint->endptsA[1]->opposite_slots[0]->connected->point
// 	      !=proposed_endpoint->endptsA[2]->opposite_slots[0]->connected->point))
// 	   )
// 	{
// 	  return false;
// 	}
// 	
// 	if ((proposed_endpoint->endptsB[1]->opposite_slots[0]->connected!=NULL)&&
// 	    (proposed_endpoint->endptsB[2]->opposite_slots[0]->connected!=NULL)&&
// 	    (!(proposed_endpoint->endptsB[1]->opposite_slots[0]->connected->point
// 	      !=proposed_endpoint->endptsB[2]->opposite_slots[0]->connected->point))
// 	   )
// 	{
// 	  return false;
// 	}
	
	return true;
    }
};





template<typename TData,typename T,int Dim>
class CProposalBifurcationCrossingDeath: public CProposal<TData,T,Dim>
{

protected:
    T Lprior;
    const static int max_stat=10;
    int statistic[max_stat];
    T ConnectEpsilon;
public:


	class Bif2Conn
	{
	  public:
	  class Points< T, Dim >::CEndpoint * endptsA[3];
	  class Points< T, Dim >::CEndpoint * endptsB[3];
	  class Points< T, Dim > * center_ptB;  
	};
	
	
	     T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }


    CProposalBifurcationCrossingDeath(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
        for (int i=0; i<max_stat; i++)
            statistic[i]=0;
	ConnectEpsilon=0.0001;
    }

    T get_default_weight() {
        return 0;
    };


    ~CProposalBifurcationCrossingDeath()
    {
//         printf("\n");
//         for (int i=0; i<max_stat; i++)
//             printf("%d ",statistic[i]);
//         printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_C);
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_C);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
		 if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;
	
	sta_assert_error(options.bifurcation_neighbors);
	
	if (connections.get_num_bifurcation_centers()<1)
	  return false;
	
	try {
	
	   /// randomly pic a bifurcation node
	  Points<T,Dim> &  center_pointA=connections.get_rand_bifurcation_center_point();
	  
	  Bif2Conn   proposed_endpoint; 
	  proposed_endpoint.center_ptB=NULL;
	  T connection_prob_new;
          T connection_costs_new;
	    
	  if (!search_candidates(
		      edgecost_fun,
		      &proposed_endpoint,
		      &connection_prob_new,
		      &connection_costs_new,
		      bifurcation_tree,
		      center_pointA,
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
		      conn_temp,
		      options.connection_bonus_L,
		      Lprior,ConnectEpsilon))
	  {
	      return false;
	  }

            if (connection_prob_new<std::numeric_limits<T>::epsilon())
                return false;
	  
// Bif2Conn   proposed_endpoint_check; 	    
// proposed_endpoint_check=proposed_endpoint;
	    
	 T connection_prob_old;
	 T connection_costs_old;
	    if (!CProposalBifurcationCrossingBirth<TData,T,Dim>::search_candidates(
		      edgecost_fun,
		      &proposed_endpoint,
		      &connection_prob_old,
		      tree,
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
		      1))
	  {
	      sta_assert_error(false);
	      return false;
	  }   
	  
	  
// for (int i=0;i<3;i++)	  
// {
//  sta_assert_error(proposed_endpoint.endptsA[i]==proposed_endpoint.endptsA[i]); 
//  sta_assert_error(proposed_endpoint.endptsB[i]==proposed_endpoint.endptsB[i]); 
// }
	  
	  
	  
	  	  sta_assert_debug0(proposed_endpoint.center_ptB!=NULL)
	  Points<T,Dim> &  center_pointB=*proposed_endpoint.center_ptB;
	  sta_assert_debug0(&center_pointB!=&center_pointA);
	  
	  sta_assert_debug0(proposed_endpoint.endptsA[0]->connected->point==&center_pointA);
	  sta_assert_debug0(proposed_endpoint.endptsA[1]->connected->point==&center_pointA);
	  sta_assert_debug0(proposed_endpoint.endptsA[2]->connected->point==&center_pointB);
	  
	  sta_assert_debug0(proposed_endpoint.endptsB[0]->connected->point==&center_pointB);
	  sta_assert_debug0(proposed_endpoint.endptsB[1]->connected->point==&center_pointA);
	  sta_assert_debug0(proposed_endpoint.endptsB[2]->connected->point==&center_pointB);
	  
	  sta_assert_debug0(proposed_endpoint.endptsA[0]->point!=proposed_endpoint.endptsB[0]->point);
	  sta_assert_debug0(proposed_endpoint.endptsA[1]->point!=proposed_endpoint.endptsB[1]->point);
	  sta_assert_debug0(proposed_endpoint.endptsA[2]->point!=proposed_endpoint.endptsB[2]->point);
	  

	  
	  
	  
	  typename CEdgecost<T,Dim>::EDGECOST_STATE  inrange;
	  connection_costs_old=center_pointA.e_cost (
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange);
	  
	  sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	  
	  connection_costs_old+=center_pointB.e_cost (
	    edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange);
	  
	  sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	  
	  
	  //connection_costs_old=center_pointA.com
	  
	   T particle_cost_new=0;
	   T particle_cost_old=0;
	   
	   for (int slot=0;slot<3;slot++)
	   {
	    particle_cost_old+=proposed_endpoint.endptsA[slot]->point->compute_cost3(temp);
	    particle_cost_old+=proposed_endpoint.endptsB[slot]->point->compute_cost3(temp);
	   }
	  
	  

	  
	  class Connection<T,Dim> old_bifurcation_connectionsA[3];
	  class Connection<T,Dim> old_bifurcation_connectionsB[3];
// 	  class Connection<T,Dim> * remove_connectionsA[3];
// 	  class Connection<T,Dim> * remove_connectionsB[3];
	  
	  
	  

	  sta_assert_debug0(center_pointA.get_num_connections()==3);
	  sta_assert_debug0(center_pointB.get_num_connections()==3);


 	  
	  for (int slot=0;slot<3;slot++)
	  {
  	    
	    class Connection<T,Dim> * delete_me=(center_pointA.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection);
	    sta_assert_debug0(delete_me!=NULL);
	    old_bifurcation_connectionsA[slot]=*delete_me;
	    connections.remove_connection(delete_me,false); 
	    
	    delete_me=(center_pointB.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection);
	    sta_assert_debug0(delete_me!=NULL);
	    old_bifurcation_connectionsB[slot]=*delete_me;
	    connections.remove_connection(delete_me); 
	  }

	  
	  class Connection<T,Dim> * new_connections[3];
	  class Connection<T,Dim> a_new_connection;
	  a_new_connection.edge_type=EDGE_TYPES::EDGE_SEGMENT;
	  
	  for (int edge=0;edge<3;edge++)
	  {
	     sta_assert_debug0(proposed_endpoint.endptsA[edge]->connected==NULL);
	     sta_assert_debug0(proposed_endpoint.endptsB[edge]->connected==NULL);
// 	     a_new_connection.pointA=proposed_endpoint.endptsA[edge]->point->getfreehub(proposed_endpoint.endptsA[edge]->side);
// 	     a_new_connection.pointB=proposed_endpoint.endptsB[edge]->point->getfreehub(proposed_endpoint.endptsB[edge]->side);
	     a_new_connection.pointA=proposed_endpoint.endptsA[edge]->getfreehub();
	     a_new_connection.pointB=proposed_endpoint.endptsB[edge]->getfreehub();
// 	     printf("adde %d ",edge);
	     new_connections[edge]=connections.add_connection(a_new_connection);
	     
	     
// 	     printf("ok  \n");
	  }
	  
	  
// 	  for (int edge=0;edge<3;edge++)
// 	  {
// 	  if (!edgecost_fun.particle_angle_valid(*(new_connections[edge]->pointA),*(new_connections[edge]->pointB)))
// 	      {
// 		
// 		for (int edge=0;edge<3;edge++)
// 		{
// 			bool check=edgecost_fun.particle_angle_valid(*(new_connections[edge]->pointA),*(new_connections[edge]->pointB));
// 			sta_assert_error(new_connections[edge]->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
// 			sta_assert_error(new_connections[edge]->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
// 			printf("AHHH :%d -> %d\n",edge,check);
// 		}
// 			mhs::STAError error;
// // 			      error<<"error checking edge angle: edge is bif ?:"<< (con->edge_type==EDGE_TYPES::EDGE_BIFURCATION)<<"\n";
// 			error<<"CDEATH: error checking edge angle "<<edge<<"\n";
// 			throw error;
// 	      }
// 	  }
	  
	  
	   for (int slot=0;slot<3;slot++)
	   {
	    particle_cost_new+=proposed_endpoint.endptsA[slot]->point->compute_cost3(temp);
	    particle_cost_new+=proposed_endpoint.endptsB[slot]->point->compute_cost3(temp);
	   }
	  
	  
	    T E=(connection_costs_new-connection_costs_old)/temp;

	      E+=(particle_cost_new-particle_cost_old)/temp;
	      
	      T R=mhs_fast_math<T>::mexp(E);
	  
	  
	      //2+ because bifurcation points have benn degenerated to ordinate points after removing edges
	  T BifurcationCrossingDeath_prop=connection_prob_new/(2+connections.get_num_bifurcation_centers()+std::numeric_limits< T >::epsilon());
	  T BifurcationCrossingBirth_prop=connection_prob_old/(connections.pool->get_numpts()+std::numeric_limits< T >::epsilon());
	      
	  R*=BifurcationCrossingBirth_prop/(BifurcationCrossingDeath_prop+std::numeric_limits<T>::epsilon());

	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
// 	  if (false)
	  {
		// check for loops by following new edge
		if (Constraints::constraint_hasloop_follow(*new_connections[0]->pointA->point,new_connections[0],options.constraint_loop_depth+1))
		{
		    for (int edge=0;edge<3;edge++)
		    {
		      connections.remove_connection(new_connections[edge]);
		    }
		    for (int edge=0;edge<3;edge++)
		    {
		      connections.add_connection(old_bifurcation_connectionsA[edge]);
		    }
		    for (int edge=0;edge<3;edge++)
		    {
		      connections.add_connection(old_bifurcation_connectionsB[edge]);
		    }
		    return false;
		}
		
		
		#ifdef  D_USE_GUI
		  for (int i=0;i<3;i++)
		  {
		    new_connections[i]->pointA->point->touch(get_type());
		    new_connections[i]->pointB->point->touch(get_type());
		  }
		
		  //center_point.touch(get_type());
// 		  for (int i=0;i<3;i++)
// 		  {
// 			center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
// 		  }
		#endif	

		
		sta_assert_debug0(center_pointA.get_num_connections()==0);
		sta_assert_debug0(center_pointB.get_num_connections()==0);

		Points< T, Dim > * p_pointA=&center_pointA;
		sta_assert_debug0(p_pointA!=NULL);
	        particle_pool.delete_obj(p_pointA);
		sta_assert_debug0(p_pointA==NULL);
		
		Points< T, Dim > * p_pointB=&center_pointB;
		sta_assert_debug0(p_pointB!=NULL);
		particle_pool.delete_obj(p_pointB);
		sta_assert_debug0(p_pointB==NULL);
		
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		return true;
	  }
	  
	for (int edge=0;edge<3;edge++)
	{
	  connections.remove_connection(new_connections[edge]);
	}
	for (int edge=0;edge<3;edge++)
	{
	  connections.add_connection(old_bifurcation_connectionsA[edge]);
	}
	for (int edge=0;edge<3;edge++)
	{
	  connections.add_connection(old_bifurcation_connectionsB[edge]);
	}	
      
      
      return false;
	  
	  
	  
	} catch(mhs::STAError & error)
        {
            throw error;
        }
	

    }
    
      
	  
    
    static bool search_candidates(
       class CEdgecost<T,Dim> & edgecost_fun,
        Bif2Conn   * proposed_endpoint, 
        T *  connection_prob,
        T *  connection_cost,
        OctTreeNode<T,Dim> & tree,
	const Points<T,Dim> & b_particle,
        T search_connection_point_center_candidate, 
        T searchrad, 
        T TempConnect,
        T lineL,
        T Lprior,
	T ConnectEpsilon,
        int mode=0)
    {
	
      
	std::size_t total_candidates=0;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        T point_dist=0;
	
	for (int i=0;i<3;i++)
	{
	  T tmp=(b_particle.get_position()-*(b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected->position)).norm2();
	  point_dist=std::max(point_dist,tmp);
	}

	//search for nearby bifurcation points
        try {
            tree.queryRange(
                b_particle.get_position(),
                search_connection_point_center_candidate+point_dist,
                candidates,
                query_buffer_size,
                total_candidates);
        } catch (mhs::STAError & error)
        {
            throw error;
        }
        
          /// no bifurcation points nearby. do nothing
        //if ((total_candidates==3)&&((mode!=1)))
        if ((total_candidates==1)&&(mode!=1))
        {
//             if (debug)
//                 printf("no points\n",total_candidates);
            return false;
        }
        
        sta_assert_error((total_candidates>1)||((mode!=0)));
	
        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;

	
	
	Bif2Conn  setup_candidates_buffer[query_buffer_size];
	Bif2Conn * setup_candidates=setup_candidates_buffer;

	
	//search bifurcation2edges configurations
	std::size_t num_point_candidates=0;
        {
	    for (std::size_t a=0; a<total_candidates; a++)
	    {
		Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		if (point!=&b_particle)
		{
		
		  sta_assert_error(point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		  for (int pidA=0;pidA<3;pidA++)
		  {
		    class Points< T, Dim >::CEndpoint *epA=(b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][pidA]->connected);
		    for (int pidB=0;pidB<3;pidB++)
		    {
		      class Points< T, Dim >::CEndpoint *epB=(point->endpoints[Points<T,Dim>::bifurcation_center_slot][pidB]->connected);
		      
		      //the two bifurcations may share epA and epB point: exclude this
		      if (epA->point!=epB->point)
		      {
			//epA and epB may be connected already
			if ((epA->opposite_slots[0]->connected==NULL)||(epA->opposite_slots[0]->connected==NULL)||
			  (epA->opposite_slots[0]->connected->point!=epB->point))
			{
			if (search_connection_point_center_candidate>(epA->point->get_position()-epB->point->get_position()).norm2())
			{
// 			  class Points< T, Dim >::CEndpoint *epA1=(b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][pidA+1]->connected);
// 			  class Points< T, Dim >::CEndpoint *epA2=(b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][pidA+2]->connected);
// 			  class Points< T, Dim >::CEndpoint *epB1=point->endpoints[Points<T,Dim>::bifurcation_center_slot][(pidB+1)%3]->connected;
// 			  class Points< T, Dim >::CEndpoint *epB2=point->endpoints[Points<T,Dim>::bifurcation_center_slot][(pidB+2)%3]->connected;
			 
			if (searchrad>(*epA->position-*epB->position).norm2())
			{
			  if (!(Connections<T,Dim>::connection_exists(*(epA->point),*(epB->point))))
			  {
			    setup_candidates->endptsA[0]=epA;
			    setup_candidates->endptsA[1]=b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][(pidA+1)%3]->connected;
			    setup_candidates->endptsA[2]=point->endpoints[Points<T,Dim>::bifurcation_center_slot][(pidB+1)%3]->connected;
			    setup_candidates->endptsB[0]=epB;
			    setup_candidates->endptsB[1]=b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][(pidA+2)%3]->connected;
			    setup_candidates->endptsB[2]=point->endpoints[Points<T,Dim>::bifurcation_center_slot][(pidB+2)%3]->connected;
			    setup_candidates->center_ptB=point;
			    setup_candidates++;
			    num_point_candidates++;
			  }
			}
			}
			}
		      }
		    }
		  }
		}
	    }
        }
        
        
                // if proposed_endpoint
        if (mode>0)
	{
	   sta_assert_error(num_point_candidates+1<query_buffer_size);
	    *setup_candidates=proposed_endpoint[0];
	    setup_candidates++;
	   num_point_candidates++;
	}else
	{
	  if (num_point_candidates<1)
	  {
	   return false; 
	  }
	}
        
        
	T connet_cost[query_buffer_size];
        T connet_prop[query_buffer_size];
        T connet_prop_acc[query_buffer_size];



// 	setup_candidates=setup_candidates_buffer;	
        int candidates_count=0;
	int not_valid=0;
	std::size_t searched_edges_found=0;
        for (std::size_t i=0; i<num_point_candidates; i++)
        {

            Bif2Conn & candidate_endpoint=setup_candidates_buffer[i];
	    
	    sta_assert_debug0(candidate_endpoint.endptsA[0]!=NULL);
	    sta_assert_debug0(candidate_endpoint.endptsA[1]!=NULL);
	    sta_assert_debug0(candidate_endpoint.endptsA[2]!=NULL);
	    sta_assert_debug0(candidate_endpoint.endptsB[0]!=NULL);
	    sta_assert_debug0(candidate_endpoint.endptsB[1]!=NULL);
	    sta_assert_debug0(candidate_endpoint.endptsB[2]!=NULL);
	    
            typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;


//  	    int check_range=-1;
	    int check_range=searchrad;
	    
            T u=edgecost_fun.e_cost(*candidate_endpoint.endptsA[0],
                                    *candidate_endpoint.endptsB[0],
                                    //check_range, ///NOTE we sort out all edges not in distance already
                                    inrange
                                   );
	    
/*if (u<0)
{
  printf("1 cost: %f\n",u);
  sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
}*/	    

	    

	    sta_assert_debug0(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
	    
	    
	    if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	    {
// 	      if ((mode>0)&&((i+1)==num_point_candidates))
// 	      {
// 		printf("0 ahhhhhh :\n",inrange);
// 	      }
	      
	      not_valid++;
	      continue;
	    }
	    
	    u+=edgecost_fun.e_cost(*candidate_endpoint.endptsA[1],
                                    *candidate_endpoint.endptsB[1],
                                    //check_range, ///NOTE we sort out all edges not in distance already
                                    inrange
	    );
	    
// if (u<0)	    
// {
//   printf("2 cost: %f\n",u);	    
//   sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// }
	    sta_assert_debug0(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
	    
	    if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	    {
// 	      if ((mode>0)&&((i+1)==num_point_candidates))
// 	      {
// 		printf("1 ahhhhhh :\n",inrange);
// 	      }
	      
	      not_valid++;
	      continue;
	    }
	    
	    u+=edgecost_fun.e_cost(*candidate_endpoint.endptsA[2],
                                    *candidate_endpoint.endptsB[2],
                                    //check_range, ///NOTE we sort out all edges not in distance already
                                    inrange
                                   );	    

	      
// if (u<0)	    
// {
//   printf("3 cost: %f\n",u);	    
//   sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// }
            sta_assert_debug0(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));


	    
	    if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	    {
// 	      if ((mode>0)&&((i+1)==num_point_candidates))
// 	      {
// 		printf("2 ahhhhhh :\n",inrange);
// 	      }
	      
	      not_valid++;
	      continue;
	    }
//             if (debug)
//             {
//                 printf("u:  %f\n",u);
//             }

            

            //if (inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	    {
		connet_cost[candidates_count]=u+3*lineL;
                connet_prop[candidates_count]=mhs_fast_math<T>::mexp((u+Lprior)/TempConnect)+ConnectEpsilon;
	    }
//             else
// 	    {
// 	      not_valid++;
// 	      continue;
//                 //connet_prop[candidates_count]=0;
// 	    }

            if (candidates_count==0)
            {
                connet_prop_acc[candidates_count]=connet_prop[candidates_count];
            }
            else
            {
                connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
            }
            
            
            //move valid candidates to the front 
            setup_candidates_buffer[candidates_count]=candidate_endpoint;
            
            candidates_count++;
	    
	    searched_edges_found=std::max(searched_edges_found,i);
        }
        
        
        sta_assert_error(candidates_count+not_valid==num_point_candidates);
	
	
        
        if (mode>0)
        {
	    sta_assert_error((searched_edges_found+1)==num_point_candidates);
	    sta_assert_error(candidates_count>0);
            connection_prob[0]=connet_prop[candidates_count-1]/(connet_prop_acc[candidates_count-1]+std::numeric_limits< T >::epsilon());
            connection_cost[0]=connet_cost[candidates_count-1];
// 	    printf("cost: %f %f\n",connection_cost[0],lineL);
	    if (mode==1)
	      return true;
        }
        
        
        
        
        /*!
          now we choose a point candidate with probability connet_prop
          using connet_prop_acc from the edge candidate list
        */
        {
	  
	  
	    unsigned int proposed_indx=(mode>0) ? 1 : 0;
	    
	    if (candidates_count<1) 
	    {
	      connection_prob[mode]=0;
	      return false;
	    }
	  
            // pic candidate with propability connect_prop
            std::size_t connection_candidate=rand_pic_array(connet_prop_acc,candidates_count,connet_prop_acc[candidates_count-1]);
	    
	    sta_assert_debug0(connection_candidate>=0);
            sta_assert_debug0(connection_candidate<candidates_count);


//             if (debug)
//                 printf("connection proposal\n");

            sta_assert_debug0(connection_candidate>=0);
            sta_assert_debug0(connection_candidate<candidates_count);

            if (connet_prop_acc[candidates_count-1]<std::numeric_limits<T>::epsilon())
            {
                connection_prob[proposed_indx]=0;
                return false;
            }
            else
            {
                connection_prob[proposed_indx]=connet_prop[connection_candidate]/(connet_prop_acc[candidates_count-1]);
            }



            connection_cost[proposed_indx]=connet_cost[connection_candidate];

            proposed_endpoint[proposed_indx]=setup_candidates_buffer[connection_candidate];



//             if (debug)
//                 printf("conn index %d\n",connection_candidate);
            return true;
	
	}
	
	return false;
	
      
    }
    
};







#endif
