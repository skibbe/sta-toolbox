#ifndef PROPOSAL_CONNECTBIRTHDEATH_H
#define PROPOSAL_CONNECTBIRTHDEATH_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CConnectBirth : public CProposal<TData,T,Dim>
{
protected:

    T lambda;
    mhs::dataArray<TData>  saliency_map;

public:
    CConnectBirth(): CProposal<TData,T,Dim>()
    {
        lambda=100;
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH);
    };


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;

        if (params==NULL)
            return;

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

           if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

        try {
            saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
        } catch (mhs::STAError error)
        {
            throw error;
        }



    }

    bool propose()
    {
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
        const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;

	
	bool debug=false;


	this->proposal_called++;
	      /// randomly pic a terminal node
	      if (connections.get_num_terminals()>0)
              {
		
		
			
if (debug)		  
  printf("[X");
		
		Points<T,Dim> &  track_point=connections.get_rand_point();
		
		#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
		  sta_assert_error(track_point.get_num_connections()==1);
		#else
		  sta_assert_error((track_point.endpoint_connections[0]==0)!=(track_point.endpoint_connections[1]==0));
		  int connected_side=(track_point.endpoint_connections[0]==0) ? 1 :0;
		  sta_assert_error(track_point.endpoint_connections[connected_side]<3);
		  sta_assert_error(track_point.endpoint_connections[1-connected_side]==0);
		#endif
		  
		Points<T,Dim> * p_point=particle_pool.create_obj();    
		Points<T,Dim> & point=*p_point;
		point.tracker_birth=1;
		
		
 		
	        // compute optimal pos, orientation and scale
		Vector<T,Dim> optimal_pos;
		Vector<T,Dim> optimal_direction;
		T & optimal_scale=track_point.scale;
		
		int optimal_sideA;
		int optimal_sideB;
		track_point.optimal_sucessor(
		  optimal_pos,
		  optimal_direction,
		  optimal_sideA,
		  optimal_sideB);
		
		
		sta_assert_error(track_point.endpoint_connections[optimal_sideA]==0);
		
		optimal_sideB=1;
		
		/// DEBUG
// 		{
// 		  track_point.update_endpoint(optimal_sideA);
// 		  
// 		  Vector<T,Dim> tmp;
// 		  tmp=track_point.endpoints_pos[optimal_sideA]-track_point.position;
// 		  tmp.normalize();
// 		  sta_assert_error(optimal_direction.dot(tmp)>0.999);
// 		  sta_assert_error(track_point.endpoint_connections[optimal_sideA]==0);
// 		  sta_assert_error(track_point.endpoint_connections[1-optimal_sideA]==1);
// 		}
		

		T temp_fact_pos=std::sqrt(temp);
		T temp_fact_scale=temp_fact_pos+1;
		T & temp_fact_rot=temp_fact_pos;
		
		T connection_probability=1;
		
		T position_sigma=temp_fact_pos*(optimal_scale/2);
		/// NEW POSITION
		Vector<T,Dim> randv;
		randv.rand_normal(position_sigma);
		point.position=optimal_pos+randv;
		
		/// only accept point if before terminal node
		Vector<T,Dim> tmp;
		tmp=randv;
		tmp.normalize();
if (debug)		  		
printf("X");
		if ((optimal_direction.dot(tmp)<0))
		{
		  particle_pool.delete_obj(p_point);
if (debug)		  		  
printf("]\n");
		  return false;
		}
		
		if ((point.position[0]<0)||
		  (point.position[1]<0)||
		(point.position[2]<0)||
		  (point.position[0]-1>shape[0])||
		  (point.position[1]-1>shape[1])||
		  (point.position[2]-1>shape[2])
		){
		  particle_pool.delete_obj(p_point);
if (debug)		  		  
printf("]\n");		  
		  return false;
		}
		connection_probability*=normal_dist<T,3>(randv.v,position_sigma);
		
		/*
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
		  static_data[center*2*numproposals]+=1;
		}
		*/
		
		/// NEW ORIENTATION
 		random_pic_direction<T,Dim>(
		  optimal_direction,
 		  point.direction,temp_fact_rot);

		connection_probability*=normal_dist_sphere_full(optimal_direction,point.direction,temp_fact_rot);
		// sign same as terminal node
		if ((optimal_direction.dot(point.direction)<0))
		  point.direction*=-1;
		
		/// NEW SCALE
		T scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
		T scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale);
		point.scale=myrand(scale_lower,scale_upper);
		connection_probability*=1/(scale_upper-scale_lower+std::numeric_limits<T>::epsilon());
		
		/// Collision check
		T collision_finite_costs=0;
		if (Points<T,Dim>::collision_two_steps>0)
		{
		  if (colliding( tree,point,collision_search_rad,collision_finite_costs))
		  {
		      particle_pool.delete_obj(p_point);
if (debug)		  		      
printf("]\n");		      
		      return false;
		  }	  
		}else
		{
		  if (colliding( tree,point,collision_search_rad))
		  {
		    particle_pool.delete_obj(p_point);
if (debug)		  		    
printf("]\n");		    
		    return false;
		  }
		}
		
		
		class Connection<T,Dim> new_connection;
		new_connection.pointA=track_point.getfreehub(optimal_sideA);
		new_connection.pointB=point.getfreehub(optimal_sideB);
		
		/// new connection
// 		class CProposal<TData,T,Dim>::TConnecTProposal connection;
// 		connection.old_connection_exists=false;  
// 		connection.connection_new.pointA=track_point.getfreehub(optimal_sideA);
// 		connection.connection_new.pointB=point.getfreehub(optimal_sideB);
// 		sta_assert_error(connection.connection_new.pointA!=NULL);
// 		sta_assert_error(connection.connection_new.pointB!=NULL);
// 		connection.new_prob=1;
// 		connection.new_connection_exists=true;
		

		
		
		T newpointsaliency;
		if (!interp3(newpointsaliency,
		saliency_map.data, 
		point.position,
		shape))
		{
		   particle_pool.delete_obj(p_point);
if (debug)		  		   
printf("]\n");		   
		  return false;
		}
		
		/// evaluating data term
		T newenergy_data;	
		if (!data_fun.eval_data(
		      newenergy_data,
		      point.direction,
		      point.position,
		       point.scale))
			  {
			    particle_pool.delete_obj(p_point);
if (debug)		  			    
printf("]\n");			    
			    return false;
			    }		
		
		    point.point_cost=newenergy_data;
		    point.saliency=newpointsaliency;

		    /// computing edge cost
		    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		    
		    T new_edgecost=new_connection.compute_cost(
		      edgecost_fun,
		      options.connection_bonus_L,
		      options.bifurcation_bonus_L,
		      maxendpointdist,
		      inrange
		    );
		    
		    //connection.new_cost=connection.connection_new.compute_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,maxendpointdist,inrange);
		    if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		    {
		      particle_pool.delete_obj(p_point);
if (debug)		  		      
printf("]\n");		      
		      return false;
		    }		    
		    
		    T E=collision_finite_costs/temp;
		    /// edge energy
		    E+=newenergy_data/temp;
		    
		    /// updating point costs
		    T pointcost=particle_connection_state_cost_scale[1]*point.compute_cost()+(particle_connection_state_cost_offset[1])//+PointScaleCost*point.compute_cost_scale() //0 -> 1 connection
				+(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])*track_point.compute_cost()+(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]); //1 -> 2 connection
		    
		    E+=(pointcost)/temp;
		    
		    /// edge costs
		    //T edge_cost=connection.new_cost;
		    //E+=(edge_cost)/temp;
		    E+=new_edgecost/temp;
		    
		    
		    T R=mexp(E);
		    
		    
		    R/=connection_probability
			*(maxscale-minscale)
			+std::numeric_limits<T>::epsilon();
			
		      	    
 
		    if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
                    {

			sta_assert_error(!p_point->has_owner());
		
			if (!tree.insert(*p_point))
			{
			    particle_pool.delete_obj(p_point);
			    p_point->print();
			    printf("error insering point\n");
			    return false;
			}	
			sta_assert_error(p_point->has_owner());
		      
			//sta_assert_error((connection.new_connection_exists));
		      
// 		      #ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
// 		      sta_assert_error(!Constraints::constraint_hasloop((*connection.connection_new.pointA->point),options.constraint_loop_depth));
// 		      sta_assert_error(!Constraints::constraint_hasloop((*connection.connection_new.pointB->point),options.constraint_loop_depth));
// 		      #endif		
			
		      Connection<T,Dim>   *connection_new=NULL;	
		      connection_new=connections.add_connection(new_connection);
		      //connection_new=connections.add_connection(connection.connection_new);
		      
		      sta_assert_error(connection_new!=NULL);
		      
		      
		      #ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
		      sta_assert_error(!Constraints::constraint_hasloop((*connection.connection_new.pointA->point),options.constraint_loop_depth));
		      sta_assert_error(!Constraints::constraint_hasloop((*connection.connection_new.pointB->point),options.constraint_loop_depth));
		      sta_assert_error(!Constraints::constraint_hasloop((*connection_new->pointA->point),options.constraint_loop_depth));
		      sta_assert_error(!Constraints::constraint_hasloop((*connection_new->pointB->point),options.constraint_loop_depth));
		      #endif
		
		      this->tracker->update_energy(E);
		      this->proposal_accepted++;
		      
		      /*
		      if (return_statistic)	
		      {
			  std::size_t center=(std::floor(point.position[0])*shape[1]
			    +std::floor(point.position[1]))*shape[2]
			    +std::floor(point.position[2]);
			static_data[center*2*numproposals+1]+=1;
		      }
		      */
if (debug)		  		      
printf("]\n");		      
				return true;
                    }
                    
		     particle_pool.delete_obj(p_point);
		}
if (debug)		  		
printf("]\n");		
        return false;
    }
};

template<typename TData,typename T,int Dim>
class CConnectDeath : public CProposal<TData,T,Dim>
{
protected:
    T lambda;

public:
    CConnectDeath(): CProposal<TData,T,Dim>()
    {
        lambda=100;
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH);
    };


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;

        if (params==NULL)
            return;

        // check if proposal parameters exist

//        if (mhs::mex_hasParam(params,"randwalk")!=-1)
//             randwalk=mhs::mex_getParam<bool>(params,"randwalk",1)[0];

        if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;
    }

    bool propose()
    {
pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
        const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;

	this->proposal_called++;	
	
	      /// uniformly select terminal, where after removal remains a terminal
	      if (connections.get_num_terminals()>0)
              {
		
		Points<T,Dim> &  point=connections.get_rand_point();
				    
		#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
		  sta_assert_error(point.get_num_connections()==1);
		#else
		  sta_assert_error((point.endpoint_connections[0]==0)!=(point.endpoint_connections[1]==0));
		  /// check if bifurcation terminal
		  if (point.get_num_connections()>1)
		    return false;
		#endif
		  
		sta_assert(point.get_num_connections()==1);
		
		const int connected_side=(point.endpoint_connections[0]==1) ? 0 : 1;
		
		/*
		if (return_statistic)	
		{
		    std::size_t center=(std::floor(point.position[0])*shape[1]
		      +std::floor(point.position[1]))*shape[2]
		      +std::floor(point.position[2]);
		  static_data[center*2*numproposals+2]+=1;
		}
		*/
		
		
		/// find the connected edge
		class Connection<T,Dim> * connection;
		
		
		Points<T,Dim> *  new_terminal=NULL;
// 		new_terminal=point.endpoints[connected_side][0]->connected->point;
// 		int found=point.endpoints[connected_side][0]->connected->side;
		
		//TODO THIS CAN BE SIMPLIFIED WHEN ASSUMING THE SINGLE EDGE THEN FIRST POLICY!!
		int found=-1;			
		for (int s=0;s<2;s++)
		for (int c=0;c<Points<T,Dim>::maxconnections;c++)
		{
		  if (point.endpoints_[s][c].connected!=NULL)
		  {
		    connection=point.endpoints_[s][c].connection;
		    new_terminal=point.endpoints_[s][c].connected->point;
		    found=point.endpoints_[s][c].connected->side;
		    break;
		  }
		}
		sta_assert_error(found!=-1);		
		
		//check if connected to a full segment that would become a 
		//terminal in the reverse operation
// 		#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
// 		if ((new_terminal->endpoint_connections[0]!=1)||
// 		  (new_terminal->endpoint_connections[1]!=1))
// 		  return false;
// 		#else
// 		if (point.endpoints[connected_side][0]->connected->endpoint_connections[0]!=1)
// 		  return false;
// 		#endif
		if ((new_terminal->endpoint_connections[0]!=1)||
		  (new_terminal->endpoint_connections[1]!=1))
		  return false;
		
	      

	      /// computing edge cost
	      typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	      
	      
	      T edge_cost=connection->compute_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,maxendpointdist,inrange);
	      sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		
		
	      /// computing optimal pos, scale, orientation
	      Vector<T,Dim> optimal_pos;
	      Vector<T,Dim> optimal_direction;
	      int optimal_sideA;
	      int optimal_sideB;
	      new_terminal->optimal_sucessor(
		optimal_pos,
		optimal_direction,
		optimal_sideA,
		optimal_sideB,
		found);
	      T optimal_scale=new_terminal->scale;

	      T temp_fact_pos=std::sqrt(temp);
	      T temp_fact_scale=temp_fact_pos+1;
	      T & temp_fact_rot=temp_fact_pos;	      
		
	      
	      T position_sigma=temp_fact_pos*optimal_scale/2;
	      
	      T connection_probability=1;
	      
	      Vector<T,3> displacement;
	      displacement=(point.position-optimal_pos);
	      connection_probability*=normal_dist<T,3>(displacement.v,position_sigma);
	      
		/// check if old terminal is in front of new terminal
		Vector<T,3> tmp=(point.position-new_terminal->position);
		tmp.normalize();
		if (optimal_direction.dot(tmp)<0)
		{
		  return false;	  
		}	      

		/// make sure that direction vector points in direction of not-connected endpoint
	      Vector<T,3> direction=point.direction;
	      if (point.endpoint_connections[1]==0)
		direction*=-1;
	      	
	      /// this could not have been created via birth-track
	      if ((optimal_direction.dot(direction)<0))
		{
		return false;		  
		}	
	      
	      connection_probability*=normal_dist_sphere_full(optimal_direction,direction,temp_fact_rot);
	      //connection_probability*=normal_dist_sphere(optimal_direction,direction,temp_fact_rot);
	      
	      
	      T scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
	      T scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale);
	      
	      if ((point.scale<scale_lower)||(point.scale>scale_upper))
	      {
		return false;
	      }
	      
	      connection_probability*=1.0/(scale_upper-scale_lower+std::numeric_limits<T>::epsilon());
	      
	      T collision_finite_costs=0;
	      if (Points<T,Dim>::collision_two_steps>0)
	      {
		sta_assert_error((!colliding( tree,point,collision_search_rad,collision_finite_costs)));
	      }
	    
	      T E=-collision_finite_costs/temp;	      
	      
		  
	      E=-edge_cost/temp;
	    
	      T newenergy=point.point_cost;
	      E-=newenergy/temp;
	      
	      
	       T pointcost=particle_connection_state_cost_scale[1]*point.compute_cost()+(particle_connection_state_cost_offset[1])//+PointScaleCost*point.compute_cost_scale() //0 -> 1 connection
				+(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])*new_terminal->compute_cost()+(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]); //1 -> 2
	      E-=pointcost/temp;		    
		   
		    
	      T R=mexp(E);
	      	    


		    R*=connection_probability
			*(maxscale-minscale);
	    
			    

			
	      
		if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
		{

		  this->tracker->update_energy(E);
		  this->proposal_accepted++;		  
		  
		  connections.remove_connection(connection);		    
		    
		  
		  particle_pool.delete_obj(&point);
		  /*   
		  if (return_statistic)	
		  {
		      std::size_t center=(std::floor(point.position[0])*shape[1]
			+std::floor(point.position[1]))*shape[2]
			+std::floor(point.position[2]);
		    static_data[center*2*numproposals+3]+=1;
		  }
		  */
		  return true;
		}
		

		}



        return false;

    }
};


#endif


