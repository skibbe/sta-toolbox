#ifndef PROPOSAL_BIFURCATION_H
#define PROPOSAL_BIFURCATION_H

#include "proposals.h"

template<typename TData,typename T,int Dim> class CProposal;

template<typename TData,typename T,int Dim> class CProposalBifurcationDeath;


template<typename TData,typename T,int Dim>
class CProposalBifurcationBirth : public CProposal<TData,T,Dim>
{
  friend class CProposalBifurcationDeath<TData,T,Dim>;
  
protected:
    T Lprior;
    
    const static int max_stat=10;
    int statistic[max_stat];
public:




    CProposalBifurcationBirth(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
    }
    
    ~CProposalBifurcationBirth()
    {
        printf("\n");
	for (int i=0;i<max_stat;i++)
	  printf("%d ",statistic[i]);
	printf("\n");
	
    }

//     void set_tracker(typename CProposal<TData,T,Dim>::Ctrack & tracker)
//     {
//         CProposal<TData,T,Dim>::CProposal(tracker);
//     }
    std::string get_name() {
        return "BIF_BIRTH";
    };

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
        const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	
	this->proposal_called++;
	
	/// no terminals?
	if (connections.get_num_terminals()<1)
	  return false;
 int stat_count=0;	
 statistic[stat_count++]++;	
	  
	try {
	
	/// randomly pic a terminal node
	Points<T,Dim> &  point=connections.get_rand_point();
	
	
	

	/// point is not a bifurcation?
	if (point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	  return false;
	


	
	  
	
	class Connection<T,Dim> connection;
	T connection_prob;
	if (!do_tracking3(
		      edgecost_fun,
		      connection,
		      connection_prob,
		      tree,
		      point,
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
		      conn_temp,
		      options.connection_bonus_L,
		      Lprior
		      ))
	{
	  return false;
	}
	//printf("%f \n",connection_prob);
 statistic[stat_count++]++;	
	sta_assert_error(!(connection_prob<0));
	

	
	  
	  Points<T,Dim> &  bifurcation_point=*connection.pointB->point;
	  T old_scale=bifurcation_point.scale;
	  T new_scale=myrand(maxscale-minscale)+minscale;
	  
	  
	  /// check if point would collide if bifurcation point
// 	  bifurcation_point.scale=new_scale;
// 	  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;
// 	  if (colliding( tree,bifurcation_point,collision_search_rad))
// 	  {
// 	    bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;  
// 	    bifurcation_point.scale=old_scale;
// 	    return false;
// 	  }
//	  bifurcation_point.scale=old_scale;
//	  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;	  
	  

 statistic[stat_count++]++;	  
	  
	    
	  int inrange;
	  T old_cost=bifurcation_point.edge_energy(edgecost_fun,
				options.connection_bonus_L,
				  maxendpointdist,
				inrange,true);
	  sta_assert_error(inrange>0);
	
	
	  
// T oldenergy_data_test;
// 	  if (!data_fun.eval_data(
// 		oldenergy_data_test,
// 		bifurcation_point.direction,
// 		bifurcation_point.position,
// 		bifurcation_point.scale))
// 	      {
// 		  
// 		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT; 
// 
// 		  return false;
// 		}
		
	  
	  bifurcation_point.scale=new_scale;
	  Connection<T,Dim>   *connection_new=NULL;
	  connection_new=connections.add_connection(connection);
	  
	  bifurcation_point.scale=new_scale;
	  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;


	  
if (colliding( tree,bifurcation_point,collision_search_rad))
{
  connections.remove_connection(connection_new);
  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;  
  bifurcation_point.scale=old_scale;
  return false;
}	  
	  
	  
	  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;
	  
	  bool inner_intersection; 
	  T bifurcation_collision_costs=colliding_bifurcation_costs(point,inner_intersection);
	  if (inner_intersection)
	  {
		  connections.remove_connection(connection_new);
		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT; 
		  bifurcation_point.scale=old_scale;
		  return false;
	    }	  
	  
	  T new_cost=bifurcation_point.edge_energy(edgecost_fun,
				options.connection_bonus_L,
				  maxendpointdist,
				inrange);
	  
	
	  T terminal_point_cost_change=point.compute_cost()*(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])
	  +(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]);

	  
	  
	  T oldenergy_data=bifurcation_point.point_cost;
	  
	  
		
	  T newenergy_data;
	  if (!data_fun.eval_data(
		newenergy_data,
		bifurcation_point.direction,
		bifurcation_point.position,
		bifurcation_point.scale))
	      {
		  connections.remove_connection(connection_new);
		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT; 
		  bifurcation_point.scale=old_scale;
		  return false;
		}
 statistic[stat_count++]++;	  
	 

	  //sta_assert_error(!inner_intersection)
	  
	  
	  T E=(new_cost-old_cost)/temp;
	    E+=(terminal_point_cost_change)/temp;
	    E+=(newenergy_data-oldenergy_data)/temp;	
	    E+=bifurcation_collision_costs/temp;


          T R=exp(-E);
	  
	  
// 	  if (newenergy_data<oldenergy_data)
// 	    printf("\n(%f) %f %f / %f / %f %f / %f \n",R,new_cost,old_cost,terminal_point_cost_change,newenergy_data,oldenergy_data,bifurcation_collision_costs);
	  
	T remove_connection_prob=1.0/3.0*1.0/(tree.get_numpts()); 
	connection_prob*=1.0/connections.get_num_terminals();
	R*=remove_connection_prob/((connection_prob)+std::numeric_limits<T>::epsilon());

// 	if (newenergy_data<oldenergy_data)
// 	printf("%f // %d %f %f\n",R,connections.get_num_terminals(),remove_connection_prob,connection_prob);

  	 //if (false)
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	
	    printf("%f [%f %f %f %f]\n",E,new_cost,old_cost,terminal_point_cost_change,bifurcation_collision_costs);
	    
		if (Constraints::constraint_hasloop_follow(point,connection_new,options.constraint_loop_depth))
		{
		  connections.remove_connection(connection_new);
		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT; 
		   bifurcation_point.scale=old_scale;
		  return false;
		}
	
		
		bifurcation_point.edge_energy_update_from_tmp();
		sta_assert_error(bifurcation_point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT);
		
		
// 		if (bifurcation_point.get_num_connections()==2)
// 		{
// 		  
// 		}
	      
		this->tracker->update_energy(E);
		this->proposal_accepted++;
	    
	    return true;
	  }
	  
	  
        connections.remove_connection(connection_new);
        bifurcation_point.scale=old_scale;
	bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT; 
        return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
    }
    
    
    

    // searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
    static bool  do_tracking3(
        class CEdgecost<T,Dim> & edgecost_fun,
	class Connection<T,Dim>   & proposed_connection,
	T & connection_prob,
        OctTreeNode<T,Dim> & tree,
        Points<T,Dim> & npoint,
        T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
        T searchrad, // TODO currently ignored. using point scale instead
        T TempConnect,
        T L,
	T Lprior,
	class Points<T,Dim>::CEndpoint * compute_probability_for=NULL
    )
    {
      

      
        bool debug=false;
        if (debug)
            printf("do_tracking birfurcation\n");

	
	/// select a free hub a the non-connected endpoint
	const int side=(npoint.endpoint_connections[0]==1) ? 1 : 0;
	class Points<T,Dim>::CEndpoint 	* enpt=npoint.getfreehub(side);
	sta_assert_error(enpt!=NULL);
	sta_assert_error(npoint.endpoint_connections[side]==0);
	sta_assert_error(npoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
	

	/// find the connected point. We want to exclude it from the 
	/// bifurcationcandidate list
	class Points<T,Dim> * connected_point=NULL;
	
	if (compute_probability_for==NULL)
	{
	  for (int a=0; a<Points<T,Dim>::maxconnections; a++)
	  {
	      if (npoint.endpoints[1-side][a].connected!=NULL)
		  connected_point=npoint.endpoints[1-side][a].connected->point;
	  }
	  sta_assert_error(connected_point!=NULL);
	}
//         class Points< T, Dim >::CEndpoint 	* hub=NULL;


        /*!
         collect all points in search rectangle
         */
        npoint.update_endpoint(side);

        //const Vector<T,Dim> & endpoint=npoint.endpoints_pos[side]; // REF OK?
        Vector<T,Dim> & endpoint=*enpt->position; // REF OK?



        std::size_t found;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        try {
            tree.queryRange(
                endpoint,
                search_connection_point_center_candidate,
                candidates,
                query_buffer_size,
                found);
        } catch (mhs::STAError & error)
        {
            throw error;
        }

        sta_assert_error(npoint.maxconnections>0);

        /// no points nearby. do nothing
        if (found==1)
        {
            if (debug)
                printf("no points\n",found);
            return false;
        }


        if (debug)
            printf("found %d initial candidates in radius %f\n",found,searchrad);


        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        if (debug)
            printf("collecting endpoints in circle\n");


        class Points< T, Dim >::CEndpoint * connet_candidates_buffer[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** connet_candidates=connet_candidates_buffer;

	
	int compute_probability_for_indx=-1;
	
// 	if (compute_probability_for!=NULL)
// 	  printf("found %d initial candidates in radius %f\n",found,searchrad);

	
        std::size_t num_connet_candidates=0;
        {

            {
                /*!
                check for all candidates in the rectangle
                  if one of the endpoints lies within the circle
                    with radius "searchrad"
                */
                for (std::size_t a=0; a<found; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    
		    
                    if ((point!=&npoint) // no self connections ;-)
		       &&((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))
		       &&(point->endpoint_connections[0]==1)&&
		       (point->endpoint_connections[1]==1)&&// no bifurcation poins
		       (connected_point!=point)) // already connected with terminal
                    {
                        //TODO might take into coinsideration:
                        //would always happensif I would use endpoint position
                        //instead of point posiiton because queryRange already supports sphere selection
                        //
                        bool not_connected_with_bifurcation_already=true;
                        for (int end_id=0;end_id<2;end_id++)
			{
			    class Points<T,Dim>::CEndpoint * endpointsA=point->endpoints[end_id];
			    
			    for (int i=0;i<Points<T,Dim>::maxconnections;i++)
			    {
			      if ((endpointsA[i].connected!=NULL)&&(endpointsA[i].connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION))
			      {
				not_connected_with_bifurcation_already=false;
			      }
			    }
			}
                        
                        if (not_connected_with_bifurcation_already)
			{
                        T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-npoint.position).norm2();
			
			
                        if (search_connection_point_center_candidate>candidate_center_dist_sq)
                        {
                            for (int pointside=0; pointside<=1; pointside++)
                            {
                                ///! updating endpoints (not done automatically)
                                //point->update_endpoint(pointside);
                                //Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
                                //T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
				
				Vector<T,Dim> bif_surf_pos=endpoint-(point->position);
				bif_surf_pos.normalize();
				T candidate_dist_sq;
				//maxdist: endpunkt zu punkt auf INNEREN/KLEINEN kreis
				if (Points<T,Dim>::thickness>0)
				  {
				      candidate_dist_sq=((point->position-bif_surf_pos*Points<T,Dim>::thickness)-(endpoint)).norm2();
				  }
				  else
				  {
				      //candidate_center_dist_sq=((bif_surf_pos*Points<T,Dim>::thickness)-(npoint.position)).norm2();
				      T  fact=(std::max(Points<T,Dim>::min_thickness,-Points<T,Dim>::thickness*point->scale));
				      candidate_dist_sq=((point->position-bif_surf_pos*fact)-(endpoint)).norm2();
				  }					
				
				
				
				
                                //if (debug)
                                   // printf("endpointdist: %f %f \n",std::sqrt(candidate_center_dist_sq),std::sqrt(candidate_dist_sq));

                                /*!
                                  if not in radius remove
                                    point from further consideration
                                */
                                if (searchrad>candidate_dist_sq)
                                {
                                    class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
                                    /*!
                                      if there are no free connections, remove
                                    endpoint from further consideration
                                    */
                                    if (freehub!=NULL)
                                    {
                                        sta_assert_error(num_connet_candidates<query_buffer_size);
                                        (*connet_candidates++)=freehub;
					
					if ((compute_probability_for!=NULL))
					{
					  if ((compute_probability_for->point==point)&&
					      (compute_probability_for->side==freehub->side))
					  //if (!(compute_probability_for!=freehub))
					  {
					    compute_probability_for_indx=num_connet_candidates;
					  }
					}
					
                                        num_connet_candidates++;
                                    }
                                }
                            }
                        }
		    }


                    }
                    /*  iter++;	*/
                }
                
//                 if (compute_probability_for!=NULL)
// 		  printf("found %d initial candidates in radius %f\n",found,searchrad);

		if ((compute_probability_for!=NULL)&&(compute_probability_for_indx<0))
		{
		  printf("couldn't find point\n");
		  return false;
		}
		//printf("could find point\n");
                
		/// no points nearby -> do nothing
		if ((num_connet_candidates==0))
		{
		    if (debug)
			printf("no endpoints %d\n",num_connet_candidates);

		    return false;
		}

		//printf("endpoint nearby: %d\n",num_connet_candidates);

                if (debug)
                    printf("computing probabilities for %d points\n",num_connet_candidates);



                T connet_cost[query_buffer_size];
                T connet_prop[query_buffer_size];
                T connet_prop_acc[query_buffer_size];


                class Points< T, Dim >::CEndpoint * connet_candidates_array[query_buffer_size];


                connet_candidates=connet_candidates_buffer;
                int candidates_count=0;
		
                for (std::size_t i=0; i<num_connet_candidates; i++)
                {


                    class Points< T, Dim >::CEndpoint * candidate_hub=connet_candidates[i];
		
		    
                    connet_candidates_array[candidates_count]=candidate_hub;

                    Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
                    Points<T,Dim> * candidate_point=candidate_hub->point;

                    Vector<T,Dim> & candidate_point_pos=candidate_point->position;
                    T   candidate_point_size=candidate_point->scale;


                    int inrange;
		    candidate_hub->point->particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;
                    T u=edgecost_fun.u_cost(*enpt,
                                            *candidate_hub,
                                            searchrad,
                                            inrange,
					    false
 					  )+L;
		    candidate_hub->point->particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;

                    sta_assert_error(inrange>-2);


                    if (debug)
                    {
                        printf("u:  %f\n",u);
                        printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
                        printf("(%f %f)\n",endpoint[0],endpoint[1]);
                        printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
                        printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// 		      sta_assert_error(false);
                    }

                    connet_cost[candidates_count]=u;
		    


                    if (inrange>0)
                        connet_prop[candidates_count]=mexp((u+Lprior-L)/TempConnect);
                    else
                        connet_prop[candidates_count]=0;

		    //printf("[%d %d] p(u):  %f\n",i,num_connet_candidates,connet_prop[candidates_count]);
		    
                    if (candidates_count==0)
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count];
                    }
                    else
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
                    }
                    candidates_count++;
                }
                
                if (compute_probability_for!=NULL)
		{
		  
		  if (connet_prop_acc[candidates_count-1]<std::numeric_limits<T>::epsilon())
		    connection_prob=0;
		    else
		  connection_prob=connet_prop[compute_probability_for_indx]/(connet_prop_acc[candidates_count-1]);
		  return true;
		}




                if (debug)
                    printf("creating proposal\n");

                /*!
                  now we choose a candidate with probability connet_prop
                  using connet_prop_acc

                  and we randomliy (uniformly) choose one port from the current point
                  (if there is no port, we create one)
                */
                {

                    // pic candidate with propability connect_prop
                    std::size_t connection_candidate=rand_pic_array(connet_prop_acc,num_connet_candidates,connet_prop_acc[candidates_count-1]);


                    if (debug)
                    {
                        for (int g=0; g<candidates_count; g++)
                        {
                            printf("%f,",connet_prop_acc[g]);
                        }
                        printf("\n");
                        for (int g=0; g<candidates_count-1; g++)
                        {
                            printf("%f,",connet_cost[g]);
                        }
                        printf("\n");
                        for (int g=0; g<candidates_count; g++)
                        {
                            printf("%f,",connet_prop[g]-1);
                        }
                        printf("\n");
                    }


                    if (debug)
                        printf("choosing %d\n",connection_candidate);


                    sta_assert_error(connection_candidate>=0);
                    sta_assert_error(connection_candidate<num_connet_candidates);


         

                    /*########################################
                     * New Connnection Cases
                    *#######################################*/

         
		    /*!
		      * propose a connection
		      */
                    {
			
		      
                        if (debug)
                            printf("connection proposal\n");

                        sta_assert_error(connection_candidate>=0);
                        sta_assert_error(connection_candidate<num_connet_candidates);

			if (connet_prop_acc[candidates_count-1]<std::numeric_limits<T>::epsilon())
			connection_prob=0;
			else
		      connection_prob=connet_prop[connection_candidate]/(connet_prop_acc[candidates_count-1]);
		  
			//connection_prob=connet_prop[connection_candidate]/connet_prop_acc[candidates_count-1];
			
                        proposed_connection.pointA=enpt;

                        sta_assert_error(connet_candidates_array[connection_candidate]!=NULL);
                        proposed_connection.pointB=connet_candidates_array[connection_candidate];

                        sta_assert_error(proposed_connection.pointA!=NULL);
                        sta_assert_error(proposed_connection.pointB!=NULL);

                        if (debug)
                            printf("conn index %d\n",connection_candidate);
                        return true;
                    }

                }

            }
        }

        throw mhs::STAError("should not be reached!");
        return true;
    }


};





template<typename TData,typename T,int Dim>
class CProposalBifurcationDeath : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
      const static int max_stat=10;
    int statistic[max_stat];
public:




    CProposalBifurcationDeath(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
    }
    
      ~CProposalBifurcationDeath()
    {
        printf("\n");
	for (int i=0;i<max_stat;i++)
	  printf("%d ",statistic[i]);
	printf("\n");
	
    }

//     void set_tracker(typename CProposal<TData,T,Dim>::Ctrack & tracker)
//     {
//         CProposal<TData,T,Dim>::CProposal(tracker);
//     }
    std::string get_name() {
        return "BIF_DEATH";
    };

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
        const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	
	this->proposal_called++;
	
	OctPoints<T,Dim> *  cpoint;
	cpoint=tree.get_rand_point();
	if (cpoint==NULL)
	{
	  return false;
	}
	
	try {
	
	Points<T,Dim> &  bifurcation_point= *((Points<T,Dim>*)(cpoint));
	
int stat_count=0;	
 statistic[stat_count++]++;	
	// point is not a bifurcation?
	if (bifurcation_point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION)
	  return false;
	
	
	bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;  
	if (colliding( tree,bifurcation_point,collision_search_rad))
	{
	  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;  
	  return false;
	}
	bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;  

statistic[stat_count++]++;	
	 
	  int inrange;
	  T old_cost=bifurcation_point.edge_energy(edgecost_fun,
				options.connection_bonus_L,
				  maxendpointdist,
				inrange,true);
	  sta_assert_error(inrange>0);
	  
	  
	  bool inner_intersection;
	  T bifurcation_collision_cost_old=colliding_bifurcation_costs(bifurcation_point,inner_intersection);
	if (inner_intersection)
	{
	  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;  
	  return false;
	}
	

	class Points<T,Dim>::CEndpoint * bifurcation_connections[3];
	int connected_sides[3];
	int conn_counter=0;
	
	// uniformly pick an edge 	
	for (int end_id=0;end_id<2;end_id++)
	{
	    class Points<T,Dim>::CEndpoint * endpointsA=bifurcation_point.endpoints[end_id];
	    
	    for (int i=0;i<Points<T,Dim>::maxconnections;i++)
	    {
	      if (endpointsA[i].connected!=NULL)
	      {
		bifurcation_connections[conn_counter]=&endpointsA[i];//.connection;
		connected_sides[conn_counter]=endpointsA[i].side;
		conn_counter++;
		sta_assert_error(conn_counter<4);
	      }
	    }
	}
	int rand_edge=std::rand()%conn_counter;
	sta_assert_error(bifurcation_connections[rand_edge]->connected!=NULL);
	sta_assert_error(bifurcation_connections[rand_edge]->connection!=NULL);
	
	
	
	
	
	
	
	
	
	
	

	

	
	// compute probability for selecting this edge
	
	// bifurcations can only be created from particles having
	// connections to both sides of a particle
	//check (and if necessary temporarily correct)
	class Points<T,Dim>::CEndpoint * segment_connection[2];
	int segment_connected_sides[2];
	int segment_counter=0;
	for (int a=0;a<3;a++)
	{
	 if (a!=rand_edge) 
	 {
	   segment_connection[segment_counter]=bifurcation_connections[a];
	   segment_connected_sides[segment_counter]=connected_sides[a];
	   segment_counter++;
	   sta_assert_error(segment_counter<3);
	 }
	}
	
	sta_assert_error(segment_counter==2);
	
	
 	
	
	
// 	Connection<T,Dim>  connection_before_switch=*(segment_connection[0]->connection);
// 	connections.remove_connection(segment_connection[0]->connection);
// 	int edge_side=(connection_before_switch.pointB=!segment_connection[0]) ? 0 : 1;
// 	Connection<T,Dim>  new_connection=connection_before_switch;
// 	connection_before_switch
// 	Connection<T,Dim>  connection_after_switch=connections.add_connection(connection_old);
	
// 	printf("-> switch 1\n");
// 	printf("%d %d %d\n",segment_connection[0]->point->endpoint_connections[0],segment_connection[0]->point->endpoint_connections[1],segment_connection[0]->side);
// 	segment_connection[0]=segment_connection[0]->connection->switch_pole(segment_connection[0]->point);
// 	sta_assert_error(segment_connection[0]!=NULL);
// 	printf("%d %d %d\n",segment_connection[0]->point->endpoint_connections[0],segment_connection[0]->point->endpoint_connections[1],segment_connection[0]->side);
// 	printf("-> switch 2\n");
// 	segment_connection[0]=segment_connection[0]->connection->switch_pole(segment_connection[0]->point);
// 	sta_assert_error(segment_connection[0]!=NULL);
// 	printf("%d %d %d\n",segment_connection[0]->point->endpoint_connections[0],segment_connection[0]->point->endpoint_connections[1],segment_connection[0]->side);
// 	printf("-> switch DONE\n");
// 	
// 	Connection<T,Dim>  connection_old=*(bifurcation_connections[rand_edge]->connection);
//  	connections.remove_connection(bifurcation_connections[rand_edge]->connection);
//  	  connections.add_connection(connection_old);
//  	  printf("-> edge added\n");
// 		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION; 
// 	return false;
	
	
	
	
	if (segment_connected_sides[0]==segment_connected_sides[1])
	{
	  segment_connection[0]=segment_connection[0]->connection->switch_pole(segment_connection[0]->point);
   	  sta_assert_error(segment_connection[0]!=NULL);
	  //sta_assert_error(segment_connection[0]->connection->switch_pole(segment_connection[0]->point));
	    //sta_assert_error(segment_connection[0]->connection->switch_pole(segment_connection[0]->point));
// 	     connections.add_connection(connection_old);
// 		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION; 
// 	  return false;
	}
	
	class Points<T,Dim>::CEndpoint * compute_probability_for=bifurcation_connections[rand_edge];
	//printf("00");
	class Points<T,Dim> * new_terminal_point_candidate=
	new_terminal_point_candidate=(bifurcation_connections[rand_edge]->connected->point);
	sta_assert_error(new_terminal_point_candidate!=NULL);
	
	
	//printf("11");
	//printf("%d %d ->",new_terminal_point_candidate->endpoint_connections[0],new_terminal_point_candidate->endpoint_connections[1]);
	Connection<T,Dim>  connection_old=*(bifurcation_connections[rand_edge]->connection);
	connections.remove_connection(bifurcation_connections[rand_edge]->connection);
	//printf("%d %d\n",new_terminal_point_candidate->endpoint_connections[0],new_terminal_point_candidate->endpoint_connections[1]);
	//printf("22");
      
	///DEBUG///
// 	  if (segment_connected_sides[0]==segment_connected_sides[1])
// 		sta_assert_error(segment_connection[0]->connection->switch_pole(segment_connection[0]->point));	  
// 	   connections.add_connection(connection_old);
// 		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION; 
// 	  return false;		
	///DEBUG///
	
	bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT; 
	
	T connection_prob;
	class Connection<T,Dim> connection;
	if ((!CProposalBifurcationBirth<TData,T,Dim>::do_tracking3(
		      edgecost_fun,
		      connection,
		      connection_prob,
		      tree,
		      *new_terminal_point_candidate,
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
		      conn_temp,
		      options.connection_bonus_L,
		      Lprior,
		      compute_probability_for
		      )))
	{
	  	if (segment_connected_sides[0]==segment_connected_sides[1])
		sta_assert_error(segment_connection[0]->connection->switch_pole(segment_connection[0]->point));	  

	   connections.add_connection(connection_old);
		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION; 
		  
		  // should never be reached because edge should be in range!!
		  sta_assert_error(0!=0);
	  return false;
	}
	
	sta_assert_error(!(connection_prob<0));
statistic[stat_count++]++;	
	
	///DEBUG///
	/*  if (segment_connected_sides[0]==segment_connected_sides[1])
		sta_assert_error(segment_connection[0]->connection->switch_pole(segment_connection[0]->point));	  
	   connections.add_connection(connection_old);
		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION; 
	  return false;	*/	
	///DEBUG///

	//bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
	
	  T new_cost=bifurcation_point.edge_energy(edgecost_fun,
				options.connection_bonus_L,
				  maxendpointdist,
				inrange);
// 	  if (inrange<0)
// 	    printf("(%f)",new_cost);
	  sta_assert_error(inrange>0);
	
	
	  T terminal_point_cost_change=bifurcation_connections[rand_edge]->point->compute_cost()*(particle_connection_state_cost_scale[1]-particle_connection_state_cost_scale[2])
	  +(particle_connection_state_cost_offset[1]-particle_connection_state_cost_offset[2]);
	  
	  
	  
	  
	  
	  
	    
	    
	  
	  T E=(new_cost-old_cost)/temp;
	    E+=(terminal_point_cost_change)/temp;
	    E-=bifurcation_collision_cost_old/temp;
	
// 	printf("%f [%f %f %f]\n",E,new_cost,old_cost,terminal_point_cost_change);
          T R=exp(-E);
	  
// 	  printf("[%f]\n",connection_prob);
	  
    
	//printf("%f ",R);  
	T remove_connection_prob=1.0/3.0*1.0/(tree.get_numpts()); 
	connection_prob*=1.0/connections.get_num_terminals();
	R*=connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());
	
     // printf("%f \n",R);
	
//  	 if (false)
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
		
	    printf("%f [%f %f %f %f]\n",E,new_cost,old_cost,terminal_point_cost_change,bifurcation_collision_cost_old);
	    
		bifurcation_point.edge_energy_update_from_tmp();
		this->tracker->update_energy(E);
		this->proposal_accepted++;
	    
	    return true;
	  }
	  
		if (segment_connected_sides[0]==segment_connected_sides[1])
	  sta_assert_error(segment_connection[0]->connection->switch_pole(segment_connection[0]->point));	  
  
       connections.add_connection(connection_old);
		  bifurcation_point.particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION; 
        return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
      
    }
};

#endif


