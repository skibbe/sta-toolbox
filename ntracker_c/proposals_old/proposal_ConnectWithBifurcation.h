#ifndef PROPOSAL_CONNECT_H
#define PROPOSAL_CONNECT_H

#include "proposals.h"

template<typename TData,typename T,int Dim> class CProposal;



template<typename TData,typename T,int Dim>
class CProposalConnect : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
public:




    CProposalConnect(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
    }

//     void set_tracker(typename CProposal<TData,T,Dim>::Ctrack & tracker)
//     {
//         CProposal<TData,T,Dim>::CProposal(tracker);
//     }
    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT);
    };

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
        const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	
	this->proposal_called++;
	
	OctPoints<T,Dim> *  cpoint;
	cpoint=tree.get_rand_point();
	if (cpoint==NULL)
	  return false;

	  
	try {		  
	
	Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	
	if (point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	  return false;
	
	class CProposal<TData,T,Dim>::TConnecTProposal connection;

/*printf("[0");*/	
	if (!do_tracking3(
		      edgecost_fun,
		      connection,
		      tree,
		      point,
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
		      conn_temp,
		      options.connection_bonus_L
		      ))
	{
	  return false;
	}

		
	
	T old_prob=connection.old_prob;
	T new_prob=connection.new_prob;
	
	T old_cost=connection.old_cost;
	T new_cost=connection.new_cost;

/*	
printf("1");*/

	Connection<T,Dim>   *connection_new=NULL;	
	Connection<T,Dim>    connection_old;
	
	if (connection.old_connection_exists)
	{
	  connection_old=*connection.connection_old;
	}

	int num_involved_pts=0;
	class Points< T, Dim > * involved_pt_set[3];
	

		
	involved_pt_set[num_involved_pts++]=&point;

	
	if (connection.old_connection_exists)
	{
	  connection_old=*connection.connection_old;
	  if (connection_old.pointA->point!=&point)
	    std::swap(connection_old.pointA,connection_old.pointB);
	  sta_assert_debug0(connection_old.pointA->point==&point);
	  involved_pt_set[num_involved_pts++]=connection_old.pointB->point;
	}
	if (connection.new_connection_exists)
	{
	  if  ((!connection.old_connection_exists)
	    ||(connection_old.pointB->point!=connection.connection_new.pointB->point))
	  involved_pt_set[num_involved_pts++]=connection.connection_new.pointB->point;  
	}
	
	T involved_pt_weights[3];
	T involved_pt_offsets[3];
	for (int i=0;i<num_involved_pts;i++)
	{
	  int pindx=involved_pt_set[i]->get_parity();
	  involved_pt_weights[i]=-particle_connection_state_cost_scale[pindx];
	  involved_pt_offsets[i]=-particle_connection_state_cost_offset[pindx];
	}
	
// printf("2");
// 	bool old_is_bifurcation=(connection.connection_old->pointA->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)||
// 	  (connection.connection_old->pointB->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);

// 	bool old_bifurcation_state[3];
// 	bool has_bifurcation=false;
// 	for (int i=0;i<num_involved_pts;i++)
// 	{
// 	  old_bifurcation_state[i]=(involved_pt_set[i]->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
// 	  has_bifurcation=has_bifurcation|old_bifurcation_state[i];
// 	}
	  
  
	  
	///temporarily remove old edge
	if (connection.old_connection_exists)
	{
	connections.remove_connection(connection.connection_old);
// 	bool check=(std::abs(connection.connection_old->cost-connection.old_cost)>0.0001);
// 	if (!check)
// 	  printf("%f %f\n",connection.connection_old->cost,connection.old_cost);
// 	sta_assert_error(check);
	}

	///temporarily add new edge
	if (connection.new_connection_exists) 
	{
	    
	  
	  
	    if (connection.new_connection_exists&&
	      Connections<T,Dim>::connection_exists(connection.connection_new))
	    {
	      if (connection.old_connection_exists)
		    connections.add_connection(connection_old);
	      return false;
	    }
	
	  //connection_new=connections.add_connection(connection.connection_new,false);
	  connection_new=connections.add_connection(connection.connection_new);
	  sta_assert_debug0(connection_new!=NULL);
	}
// printf("3");	


	//should never happen anyway
if (false)
{
	for (int i=0;i<num_involved_pts;i++)
	{
	  sta_assert(involved_pt_set[i]->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
	  if ((involved_pt_set[i]->endpoint_connections[0]>1)||(involved_pt_set[i]->endpoint_connections[1]>1))
	  {
	    //printf("connection would create a bifurcation: undo\n");
	    if (connection.new_connection_exists) 
	    connections.remove_connection(connection_new);
	    if (connection.old_connection_exists)
		connections.add_connection(connection_old);
	      return false;
	  }
	}
}	

// printf("4");
	
	
// 	has_bifurcation=false;
// 	bool new_bifurcation_state[3];
// 	for (int i=0;i<num_involved_pts;i++)
// 	{
// 	  new_bifurcation_state[i]=(involved_pt_set[i]->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
// 	  has_bifurcation=has_bifurcation|new_bifurcation_state[i];
// 	}
	
	
	
	
// 	bool new_is_bifurcation=(connection.connection_new.pointA->point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)||
// 	  (connection.connection_new.pointB->point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);  
	
	
	
	T E=(new_cost-old_cost)/temp;
	

	for (int i=0;i<num_involved_pts;i++)
	{
	  int pindx=involved_pt_set[i]->get_parity();
	  E+=((involved_pt_weights[i]+particle_connection_state_cost_scale[pindx])*
	  involved_pt_set[i]->compute_cost()+
	    (involved_pt_offsets[i]+particle_connection_state_cost_offset[pindx])
	)/temp;
	}
	
	
      T R=exp(-E);
/*	  
printf("5]"); */   
	  
	  
	R*=old_prob/((new_prob)+std::numeric_limits<T>::epsilon());
	
// printf("[6"); 
  	 //if (false)
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	    
	  /*printf("7"); */	  
	      if (connection.new_connection_exists)
	      {
// 		
		if ((Constraints::constraint_hasloop_follow(point,connection_new,options.constraint_loop_depth))||
		    (options.constraint_isdirectedge&&Constraints::constraint_isdirectedge(connection_new)))
		{
		  connections.remove_connection(connection_new);
		  if (connection.old_connection_exists)
		      connections.add_connection(connection_old);
		  return false;
		}
		 //connection_new->cost=connection.new_cost;
	      }
// 	      printf("8 (%f)",E); 
// 	      printf("(%f)",connection_new->cost); 
// 	      printf("(%f)",connection.new_cost); 
	      
	      
// 	       printf("9");
		this->tracker->update_energy(E);
// 		printf("A");
		this->proposal_accepted++;
// 	    printf("B]\n"); 	  
	    return true;
	  }
/*printf("0]\n"); */	  
	  
      if (connection.new_connection_exists) 
      connections.remove_connection(connection_new);
      if (connection.old_connection_exists)
	  connections.add_connection(connection_old);
        return false;
      
      } catch (mhs::STAError error)
        {
            throw error;
        }
    }
    
    
    

    // searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
    bool  do_tracking3(
        class CEdgecost<T,Dim> & edgecost_fun,
        class CProposal<TData,T,Dim>::TConnecTProposal & proposed_connection,
        OctTreeNode<T,Dim> & tree,
        Points<T,Dim> & npoint,
        T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
        T searchrad, // TODO currently ignored. using point scale instead
        T TempConnect,
        T L
    )
    {
      

      
        bool debug=false;
        if (debug)
            printf("do_tracking\n");


        int side=std::rand()%2;
//         class Points<T,Dim>::CEndpoint 	* p_pole=npoint.endpoints_[side];
	class Points<T,Dim>::CEndpoint 	** p_pole=npoint.endpoints[side];

	
/* THIS IS OLD STYLE! NOW THE EDGE (IF EXTSIS) SHOULD BE CONNNECTED TO THE FIRST PORT!!	
        std::size_t port_id=std::rand()%Points< T, Dim >::maxconnections;

        class Points<T,Dim>::CEndpoint 	* enpt=&p_pole[port_id];
        class Points< T, Dim >::CEndpoint 	* hub=NULL;

	/// check if existing edge is connected with BIF-point
	if ((p_pole[port_id].connected!=NULL)&&(p_pole[port_id].connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION))
	{
	  if (debug)
                printf("connected with bifurecation point\n");
            return false;
	}
*/

	std::size_t port_id=0;
	if ((p_pole[port_id]->connected!=NULL)&&(p_pole[port_id]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION))
	{
	  if (debug)
                printf("connected with bifurecation point\n");
            return false;
	}
	if (npoint.endpoint_connections[side]>1)
	{
	  if (debug)
                printf("side involved in bifurecation \n");
            return false;
	}
	
	sta_assert_debug0((npoint.endpoint_connections[side]==0)||(p_pole[port_id]->connected!=NULL));

        class Points<T,Dim>::CEndpoint 	* enpt=p_pole[port_id];
        class Points< T, Dim >::CEndpoint 	* hub=NULL;

	
	
	sta_assert_debug0(npoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
	

        /*!
         collect all points in search rectangle
         */
        npoint.update_endpoint(side);

        //const Vector<T,Dim> & endpoint=npoint.endpoints_pos[side]; // REF OK?
        Vector<T,Dim>  endpoint=npoint.endpoints_pos[side]; // REF OK?



        std::size_t found;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        try {
            tree.queryRange(
                endpoint,
                search_connection_point_center_candidate,
                candidates,
                query_buffer_size,
                found);
        } catch (mhs::STAError & error)
        {
            throw error;
        }

        sta_assert_debug0(npoint.maxconnections>0);

        /// no points nearby. do nothing
        if (found==1)
        {
            if (debug)
                printf("no points\n",found);
            return false;
        }


        if (debug)
            printf("found %d initial candidates in radius %f\n",found,searchrad);


        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        if (debug)
            printf("collecting endpoints in circle\n");


        class Points< T, Dim >::CEndpoint * connet_candidates_buffer[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** connet_candidates=connet_candidates_buffer;

        std::size_t num_connet_candidates=0;
        {

            {
                /*!
                check for all candidates in the rectangle
                  if one of the endpoints lies within the circle
                    with radius "searchrad"
                */
                for (std::size_t a=0; a<found; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    
		    
//                     if ((point!=&npoint) // no self connections ;-)
// 		       &&((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))) // no bifurcation poins
		    if (point!=&npoint) // no self connections ;-)
                    {
                        //TODO might take into coinsideration:
                        //would always happensif I would use endpoint position
                        //instead of point posiiton because queryRange already supports sphere selection
                        //
                        T candidate_center_dist_sq=(Vector<T,Dim>(point->position)-npoint.position).norm2();
                        if (search_connection_point_center_candidate>candidate_center_dist_sq)
                        {
                            for (int pointside=0; pointside<=1; pointside++)
                            {
			      
				// do not connect with particle sides which are modeling a bifurcation
				// or are already occupied with a connection
				if (point->endpoint_connections[pointside]==0)
				{
				  ///! updating endpoints (not done automatically)
				  point->update_endpoint(pointside);
				  Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
				  T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
				  if (debug)
				      printf("endpointdist: %f\n",std::sqrt(candidate_dist_sq));

				  /*!
				    if not in radius remove
				      point from further consideration
				  */
				  if (searchrad>candidate_dist_sq)
				  {
				      class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
				      /*!
					if there are no free connections, remove
				      endpoint from further consideration
				      */
				      if (freehub!=NULL)
				      {
					  sta_assert_debug0(num_connet_candidates<query_buffer_size);
					  (*connet_candidates++)=freehub;
					  num_connet_candidates++;
				      }
				  }
				}
                            }
                        }


                    }
                    /*  iter++;	*/
                }


                //! add existing connection (if not connected to a bifurcation)
                //bool connection_exists=((p_pole[port_id].connected!=NULL)&&(p_pole[port_id].connected->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION));
                bool connection_exists=(p_pole[port_id]->connected!=NULL);
                if (connection_exists)
                {
//  		    if (p_pole[port_id]->connected->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
//  		      printf("connected particle is bifurcation!!\n");
		    
		    
		  
                    hub=p_pole[port_id]->connected;
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
		    sta_assert_error(hub->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
#endif
		    sta_assert_error(p_pole[port_id]->connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION);  
		    

		    
		    
                    hub->point->update_endpoints();
                    sta_assert_debug0(hub!=NULL);

                    T candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
                    if (searchrad>candidate_dist_sq)
                    {
                        //connet_candidates.push_back(hub);
                        *connet_candidates++=hub;
                        num_connet_candidates++;
                    }
                    else
                    {
                        printf("%f %f\n",searchrad,candidate_dist_sq);
                        hub->point->update_endpoints();
                        candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
                        printf("%f %f\n",searchrad,candidate_dist_sq);
                        npoint.update_endpoints();
                        candidate_dist_sq=((*(enpt->position))-(*(hub->position))).norm2();
                        printf("%f %f\n",searchrad,candidate_dist_sq);
                        sta_assert_error(false);
                        return false; //! not reversable
                    }
                } else
                    /// no points nearby and not connected -> do nothing
                    if ((num_connet_candidates==0))
                    {
                        if (debug)
                            printf("no endpoints\n",num_connet_candidates);

                        return false;
                    }

                T min_cost=std::numeric_limits<T>::max();


                if (debug)
                    printf("computing probabilities for %d points\n",num_connet_candidates);



                T connet_cost[query_buffer_size];
                T connet_prop[query_buffer_size];
                T connet_prop_acc[query_buffer_size];


                class Points< T, Dim >::CEndpoint * connet_candidates_array[query_buffer_size];


                connet_candidates=connet_candidates_buffer;
                int candidates_count=0;
                for (std::size_t i=0; i<num_connet_candidates; i++)
                {


                    class Points< T, Dim >::CEndpoint * candidate_hub=connet_candidates[i];

                    connet_candidates_array[candidates_count]=candidate_hub;

                    Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
                    Points<T,Dim> * candidate_point=candidate_hub->point;

                    Vector<T,Dim> & candidate_point_pos=candidate_point->position;
                    T   candidate_point_size=candidate_point->scale;


                    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;

                    T u=edgecost_fun.u_cost(*enpt,
                                            *candidate_hub,
                                            searchrad,
                                            inrange)+L;


                    sta_assert_error(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));


                    if (debug)
                    {
                        printf("u:  %f\n",u);
                        printf("(%f %f)\n",npoint.position[0],npoint.position[1]);
                        printf("(%f %f)\n",endpoint[0],endpoint[1]);
                        printf("(%f %f)\n",candidate_point_pos[0],candidate_point_pos[1]);
                        printf("(%f %f)\n",candidate_endpoint_pos[0],candidate_endpoint_pos[1]);
// 		      sta_assert_error(false);
                    }

                    connet_cost[candidates_count]=u;

                    min_cost=std::min(min_cost,u);

                    if (inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
                        connet_prop[candidates_count]=mexp((u+Lprior-L)/TempConnect);
                    else
                        connet_prop[candidates_count]=0;

                    if (candidates_count==0)
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count];
                    }
                    else
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
                    }
                    candidates_count++;
                }



                //! since connect_prob=1 -> e^0
                connet_cost[candidates_count]=0;


                T const_no_connection_weight=1;
                if (connet_cost[candidates_count]!=0)
                    const_no_connection_weight=mexp(connet_cost[candidates_count]/TempConnect);


                /// adding the "do not connect" proposal propability
                if (candidates_count==0)
                    connet_prop_acc[candidates_count]=const_no_connection_weight;
                else
                {
                    connet_prop_acc[candidates_count]=connet_prop_acc[candidates_count-1]+const_no_connection_weight;
                }
                connet_prop[candidates_count]=const_no_connection_weight;


                if (debug)
                    printf("creating proposal\n");

                /*!
                  now we choose a candidate with probability connet_prop
                  using connet_prop_acc

                  and we randomliy (uniformly) choose one port from the current point
                  (if there is no port, we create one)
                */
                {

                    // pic candidate with propability connect_prop
                    std::size_t connection_candidate=rand_pic_array(connet_prop_acc,1+num_connet_candidates,connet_prop_acc[candidates_count]);


                    if (debug)
                    {
                        for (int g=0; g<candidates_count+1; g++)
                        {
                            printf("%f,",connet_prop_acc[g]);
                        }
                        printf("\n");
                        for (int g=0; g<candidates_count; g++)
                        {
                            printf("%f,",connet_cost[g]);
                        }
                        printf("\n");
                        for (int g=0; g<candidates_count; g++)
                        {
                            printf("%f,",connet_prop[g]);
                        }
                        printf("\n");
                    }


                    if (debug)
                        printf("choosing %d\n",connection_candidate);


                    sta_assert_debug0(connection_candidate>=0);
                    sta_assert_debug0(connection_candidate<=num_connet_candidates);


                    /*########################################
                             * Old Connnection Cases
                     *#######################################*/

                    proposed_connection.connection_old=(p_pole[port_id]->connection);
                    proposed_connection.old_connection_exists=connection_exists;//(p_pole[port_id].connected!=NULL);


                    if (!proposed_connection.old_connection_exists)
                    {
                        //! same as old conection (no connection)
                        if (connection_candidate==num_connet_candidates)
                            return false;

                        proposed_connection.old_prob=const_no_connection_weight/connet_prop_acc[candidates_count];
                        proposed_connection.old_cost=connet_cost[candidates_count];
                    } else
                    {
                        //! same as old conection
                        if ((connection_candidate<num_connet_candidates)
                                &&(p_pole[port_id]->connected==connet_candidates_array[connection_candidate]))
                            return false;

                        proposed_connection.old_prob=connet_prop[candidates_count-1]/connet_prop_acc[candidates_count];
                        proposed_connection.old_cost=connet_cost[candidates_count-1];
                    }


                    /*########################################
                     * New Connnection Cases
                    *#######################################*/


                    // the "no connection proposal"
                    if (connection_candidate==num_connet_candidates)
                    {
                        if (debug)
                            printf("no connection proposal\n");

                        proposed_connection.new_prob=const_no_connection_weight/connet_prop_acc[candidates_count];
                        proposed_connection.new_connection_exists=false;
                        proposed_connection.new_cost=connet_cost[candidates_count];
                        return true;
                    } else
                        /*!
                         * propose a connection
                         */
                    {
                        if (debug)
                            printf("connection proposal\n");

                        sta_assert_debug0(connection_candidate>=0);
                        sta_assert_debug0(connection_candidate<num_connet_candidates);


                        proposed_connection.connection_new.pointA=(p_pole[port_id]);


                        sta_assert_debug0(connet_candidates_array[connection_candidate]!=NULL);
                        proposed_connection.connection_new.pointB=connet_candidates_array[connection_candidate];

                        if (debug)
                            printf("side %d with side %d\n",connet_candidates_array[connection_candidate]->side,p_pole[port_id]->side);


                        proposed_connection.new_cost=connet_cost[connection_candidate];
                        proposed_connection.new_prob=connet_prop[connection_candidate]/connet_prop_acc[candidates_count];
                        proposed_connection.new_connection_exists=true;

                        sta_assert_debug0(proposed_connection.connection_new.pointA!=NULL);
                        sta_assert_debug0(proposed_connection.connection_new.pointB!=NULL);

                        if (debug)
                            printf("conn index %d\n",connection_candidate);
                        return true;
                    }

                }

            }
        }

        throw mhs::STAError("should not be reached!");
        return true;
    }


};

#endif


