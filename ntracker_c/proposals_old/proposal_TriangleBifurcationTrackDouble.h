#ifndef PROPOSAL_BIFURCATION_TRACK_H
#define PROPOSAL_BIFURCATION_TRACK_H



	    template<typename TData,typename T,int Dim>
	    class CProposalBifurcationTrackBirthDouble : public CProposal<TData,T,Dim>
	    {
	    protected:
		T Lprior;
		  const static int max_stat=10;
		int statistic[max_stat];
		mhs::dataArray<TData>  saliency_map;
	    public:




		CProposalBifurcationTrackBirthDouble(): CProposal<TData,T,Dim>()
		{
		    Lprior=-10;
		    for (int i=0;i<max_stat;i++)
		      statistic[i]=0;
		}
		
		  ~CProposalBifurcationTrackBirthDouble()
		{
		    printf("\n");
		    for (int i=0;i<max_stat;i++)
		      printf("(%d)%d ",i,statistic[i]);
		    printf("\n");
		    
		}
		
		T get_default_weight(){return 1;};

		std::string get_name() {
		  return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH);
		};

		void set_params(const mxArray * params=NULL)
		{
		    if (params==NULL)
			return;

		    if (mhs::mex_hasParam(params,"Lprior")!=-1)
			Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

		}

		void init(const mxArray * feature_struct) 
		{
		    sta_assert_error(this->tracker!=NULL);
		    if (feature_struct==NULL)
			return;

		    try {
			saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
		    } catch (mhs::STAError error)
		    {
			throw error;
		    }      
		};

		bool propose()
		{   
		    pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
		    const std::size_t *		shape			=this->tracker->shape;
		    const T & 			maxscale		=this->tracker->maxscale;
		    const T & 			minscale		=this->tracker->minscale;
		    CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
		    const T &			temp			=this->tracker->opt_temp;
		    const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
		    OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
		    const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
		    const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
		    const T &			collision_search_rad	=this->tracker->collision_search_rad;
		    const std::size_t & 		nvoxel			=this->tracker->numvoxel;
		    const T & 			maxendpointdist		=this->tracker->maxendpointdist;
		    CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
		    class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
		    Connections<T,Dim>  & connections			=this->tracker->connections;
		    T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
		    
		    
		    bool debug=false;
		    
		    this->proposal_called++;
		    
		    class Connection<T,Dim> * edge;

		    /// uniformly pic edge
		    edge=connections.get_rand_edge();
		    if (edge==NULL)
			return false;
		    
		    
		    sta_assert_debug0(edge->pointA->connected!=NULL);
		    sta_assert_debug0(edge->pointB->connected!=NULL);

		    if ((edge->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)||
		      (edge->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT))
		    return false; 
		    
		    sta_assert_debug0(edge->edge_type!=EDGE_TYPES::EDGE_BIFURCATION);
		    
		    //we don't want to create bifurcation terminals!!
		    if ((edge->pointA->point->is_terminal())||(edge->pointB->point->is_terminal()))
		    return false; 
		    
		    int stat_count=0;
	    //1	
	    statistic[stat_count++]++;	
		    
		    
		    try
		    {
// try 
// {
// this->tracker->check_consisty();		      
// } catch(mhs::STAError & error)
// {
//     mhs::STAError error2;
//     error2<<"1"<<error.what()<<"\n";
//     throw error2;
// }
		      /*
		      *	CREATING FIRST POINT
		      *
		      */ 	  
		      class Points<T,Dim>::CEndpoint ** endpoints=edge->points;
		      const Vector<T,Dim> & centerA=(endpoints[0]->point->position);
		      const Vector<T,Dim> & centerB=(endpoints[1]->point->position);
		      T & scaleA=endpoints[0]->point->scale;
		      T & scaleB=endpoints[1]->point->scale;
		      
		      T temp_fact_pos=std::sqrt(temp);
		      T temp_fact_scale=temp_fact_pos+1;
		      T & temp_fact_rot=temp_fact_pos;
		      

		      
		      
		      Points<T,Dim> * p_point=particle_pool.create_obj();
		      Points<T,Dim> & b_point=*p_point;
		      b_point.tracker_birth=3;
		      b_point.scale=myrand(minscale,maxscale);
		      
		      Vector<T,Dim> n[3];
		      n[0]=(centerA-centerB);
		      T n_length=std::sqrt(n[0].norm2());
		      n[0]/=n_length+std::numeric_limits<T>::epsilon();
		      mhs_graphics::createOrths(n[0],n[1],n[2]);

		      
		      T h=0;
		      T h_rel_offset=0.5;		      
		      
		      const T side_scale=1.1;
		      T  a=side_scale*scaleA;
		      T  b=side_scale*scaleB;
		      T  c=side_scale*b_point.scale;
		      
		      
		      // we compute the location of the three center points 
		      // of the three sides
		      T w=(b*b+c*c-a*a)/(2*b*c);
		      if ((std::abs(w)>1))
		      {
		      particle_pool.delete_obj(p_point);
			return false; 
		      }
		      
		      sta_assert_debug0(!(std::abs(w)>1));
		      Vector<T,2> P1;
		      P1[0]=c;
		      P1[1]=0;
		      Vector<T,2> P2;
		      P2[0]=b*w;
		      P2[1]=b*std::sqrt(1-w*w);
		      
		      
		      // the three center points 
		      Vector<T,2> P[3];
		      P[0]=(P1+P2)/2; // devides line a
		      P[1]=P2/2;	  // devides line b	
		      P[2]=P1/2;	  // devides line c	
		    
		      Vector<T,2> n_=P[1]-P[0];
		      n_.normalize();
		      Vector<T,2> v_=(P[2]-P[0]);
		      
		      // optimal height:
		      h_rel_offset=n_.dot(v_);
		      h=std::sqrt(((P[2]-P[0])-n_*h_rel_offset).norm2());
		      // relative offset between P[2] and P[1]
		      h_rel_offset/=std::sqrt(v_.norm2());

		      
		      
		      T sign=(Points<T,Dim>::thickness<0) ? -1 : 1 ; 
		      T b_thickness=sign*Points<T,Dim>::thickness*b_point.scale;
		      h+=b_thickness;
		      
		      
		      // randomly picking a point on the circular orbit around n[0]
		      T orbit_angle=myrand(2*M_PI);
		      Vector<T,Dim> suggested_direction= n[1]*std::cos(orbit_angle)
							-n[2]*std::sin(orbit_angle);
		    
							

		      // computing position of orbit point in image space
		      // offset is relative optimal h-offset
		      Vector<T,Dim> suggested_point=centerA;
		      suggested_point+=n[0]*(h_rel_offset*n_length)+suggested_direction*(h);
		      

			      
		      
		      // computing suggested point position+noise
		      T sigma=(scaleA+scaleB)/4;//+n_length;
		      Vector<T,Dim> randv;
		      randv.rand_normal(sigma);
		      b_point.position=suggested_point+randv;
		      
		      
		      if ((b_point.position[0]<0)||
		      (b_point.position[1]<0)||
		      (b_point.position[2]<0)||
		      (b_point.position[0]-1>shape[0])||
		      (b_point.position[1]-1>shape[1])||
		      (b_point.position[2]-1>shape[2])
		      ){
			particle_pool.delete_obj(p_point);
			return false;
		      }
		      
		    
		      T sigma_r=0.25;
		      // compute suggested direction
		      random_pic_direction<T,Dim>(
			      suggested_direction,
			      b_point.direction,sigma_r);
		      
statistic[stat_count++]++;		  
	  
		      if (colliding( tree,b_point,collision_search_rad))
		      {
			particle_pool.delete_obj(p_point);
			return false;
		      }

		      if (!tree.insert(*p_point))
		      {
			  particle_pool.delete_obj(p_point);
			  p_point->print();
			  throw mhs::STAError("error insering point\n");
		      }		      
/*try 
{
this->tracker->check_consisty();		      
} catch(mhs::STAError & error)
{
    mhs::STAError error2;
    error2<<"after tree:"<<error.what()<<"\n";
    throw error2;
}*/		
		
		      /*
		      *	CREATING SECOND POINT
		      *
		      */ 	
		      
		      Points<T,Dim> * p_point2=particle_pool.create_obj();
		      Points<T,Dim> & t_point=*p_point2;
		      t_point.tracker_birth=3;
		      
		      
		      
		      
		      Vector<T,Dim> t_optimal_position;
		      t_optimal_position=b_point.position+b_point.direction*b_thickness*2;
		      Vector<T,Dim> t_optimal_direction;
		      t_optimal_direction=b_point.direction;
		      
		      Vector<T,Dim> t_randv;
		      t_randv.rand_normal(sigma);
		      
		      t_point.position=t_optimal_position+t_randv;
		      random_pic_direction<T,Dim>(
			      t_optimal_direction,
			      t_point.direction,sigma_r);
		      t_point.scale=myrand(minscale,maxscale);
		      
		      
		      if ((t_point.position[0]<0)||
		      (t_point.position[1]<0)||
		      (t_point.position[2]<0)||
		      (t_point.position[0]-1>shape[0])||
		      (t_point.position[1]-1>shape[1])||
		      (t_point.position[2]-1>shape[2])
		      ){
			particle_pool.delete_obj(p_point);
			particle_pool.delete_obj(p_point2);
			return false;
		      }
		      
		      
		      if (colliding( tree,t_point,collision_search_rad))
		      {
			particle_pool.delete_obj(p_point);
			particle_pool.delete_obj(p_point2);
			return false;
		      }
		      
	    //2	  
	    statistic[stat_count++]++;		  
		      
		      T connection_probability=1;
		      
	    if (debug)
	      printf("[:");
		      
/*
		      *  
		      *  computing the probability for the bifurcation point
		      * 
		      */
		      connection_probability*=normal_dist<T,3>(randv.v,sigma);
	      // 	  printf("cp: %f ",connection_probability);
		      connection_probability*=normal_dist_sphere(suggested_direction,b_point.direction,sigma_r);
	      // 	  printf(", %f (%f %f) \n",normal_dist_sphere(suggested_direction,b_point.direction,temp_fact_rot),std::sqrt(suggested_direction.norm2()),std::sqrt(b_point.direction.norm2()));
		      connection_probability/=2*M_PI;
		      
		    

		      /*
		      *  
		      *  computing the probability for the terminal point
		      * 
		      */
    // 		    printf("connection prop1 %f\n",connection_probability);
		    T connection_probability1=connection_probability;
		      
		      connection_probability*=normal_dist<T,3>(t_randv.v,sigma);
		      connection_probability*=normal_dist_sphere(t_optimal_direction,t_point.direction,sigma_r);
		/*    printf("connection prop2 %f\n",connection_probability/connection_probability1);
	*/	      

			    
		    // edge costs of the OLD configuration
		    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		    T old_edgecost=edge->compute_cost(
		      edgecost_fun,
		      options.connection_bonus_L,
		      options.bifurcation_bonus_L,
		      maxendpointdist,
		      inrange);
		    
		      #ifdef _DEBUG_CHECKS_0_
			      if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
			      {
				edge->pointA->position->print();
				edge->pointB->position->print();
				printf("%f %f\n",options.connection_candidate_searchrad,std::sqrt((*edge->pointA->position-*edge->pointB->position).norm2()));
				sta_assert_debug0(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
			      }
		      #endif	
		    
		    /// of course the old edge should be in range (valid)
		    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));	
		    
		    
		    
	    if (debug)
	      printf("*");	
		      
		      
		    /* 
		    * 
		    * we are going to add the terminal t_point
		    * 
		    */
		    const int side=1;  
		    class Connection<T,Dim> a_new_connection;
		    class Connection<T,Dim> * new_terminal_connection;
		    a_new_connection.pointA=b_point.getfreehub(1-side);
		    a_new_connection.pointB=t_point.getfreehub(1);
		    a_new_connection.edge_type=EDGE_TYPES::EDGE_SEGMENT;
		    new_terminal_connection=connections.add_connection(a_new_connection);
		    
		    
// try 
// {
// this->tracker->check_consisty();		      
// } catch(mhs::STAError & error)
// {
//     mhs::STAError error2;
//     error2<<"after adding single edge:"<<error.what()<<"\n";
//     throw error2;
// }	

	    
		    /* 
		    * 
		    * we are going to create the bifurcation. the existing edge 
		    * and b_point will be a part of it
		    * 
		    */
		    edge->edge_type=EDGE_TYPES::EDGE_BIFURCATION;	  
		      
		   
		    class Connection<T,Dim> * new_connections[2];
		    /// opposed to particle direction
		    
		    //building the bifurcation  
		    for (int i=0;i<2;i++)
		    {
		      
		    
		      a_new_connection.pointA=b_point.getfreehub(side);
		      
		      class Points<T,Dim>::CEndpoint * new_endpoint=edge->points[i];
		      Points<T,Dim> * new_point=new_endpoint->point;
		      a_new_connection.pointB=new_point->getfreehub(new_endpoint->side);
		      
		      sta_assert_debug0(a_new_connection.pointB->point!=a_new_connection.pointA->point);

		      
		      a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
		      
		      new_connections[i]=connections.add_connection(a_new_connection);
		      sta_assert_debug0(new_connections[i]!=NULL);
		    }
		    sta_assert_debug0(new_connections[0]->pointA->point==new_connections[1]->pointA->point);
		    sta_assert_debug0(new_connections[0]->pointA!=new_connections[1]->pointA);
		    sta_assert_debug0(new_connections[0]->pointB!=new_connections[1]->pointB);	
		    sta_assert_debug0(new_connections[0]->pointB->point!=new_connections[1]->pointB->point);	
		    
/*try 
{
this->tracker->check_consisty();		      
} catch(mhs::STAError & error)
{
    mhs::STAError error2;
    error2<<"after adding  two bifurcation edges:"<<error.what()<<"\n";
    throw error2;
}*/		    
		    
	    
			    
		    
		    
	    if (debug)
	      printf("-");		
		    
			  T new_edgecost=b_point.edge_energy(
			      edgecost_fun,
			      options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      maxendpointdist,
			      inrange
			    );
	    // printf("\n[0");	
	    // 	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange2;
	    // 	T new_edgecost_test=edge->compute_cost(
	    // 		  edgecost_fun,
	    // 		  options.connection_bonus_L,
	    // 		  options.bifurcation_bonus_L,
	    // 		  maxendpointdist,
	    // 		  inrange2
	    // 		);
	    // 	printf("bif2: %f\n",new_edgecost_test);
	    // /*printf("1");*/		
	    // 	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange3;
	    // 	T new_edgecost_test2=new_terminal_connection->compute_cost(
	    // 		  edgecost_fun,
	    // 		  options.connection_bonus_L,
	    // 		  options.bifurcation_bonus_L,
	    // 		  maxendpointdist,
	    // 		  inrange3
	    // 		);
	    // 	printf("seg2: %f\n",new_edgecost_test2);
	    // 	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE) 
	    // 	{
	    // 	  sta_assert_error((inrange==inrange2)||(inrange==inrange3));
	    // 	}
	    /*printf("2]\n");		*/	
	    if (debug)
	      printf(".");	

		    
		    if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)  
		      {
			sta_assert_debug0(b_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_terminal_connection);
			particle_pool.delete_obj(p_point2);
			particle_pool.delete_obj(p_point);
			edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
			if (debug)
			  printf("X]\n");			  
			return false;
		      } 
		      
		      
		      
		      
	    //3	  
	    statistic[stat_count++]++;	

	    if (debug)
	      printf("/]\n");			  
		      
		      T b_newpointsaliency;
		      if (!interp3(b_newpointsaliency,
		      saliency_map.data, 
		      b_point.position,
		      shape))
		      {
			sta_assert_debug0(b_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_terminal_connection);
			particle_pool.delete_obj(p_point2);
			particle_pool.delete_obj(p_point);
			edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
			return false;
		      }	
		      
		      T t_newpointsaliency;
		      if (!interp3(t_newpointsaliency,
		      saliency_map.data, 
		      t_point.position,
		      shape))
		      {
			sta_assert_debug0(t_point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_terminal_connection);
			particle_pool.delete_obj(p_point2);
			particle_pool.delete_obj(p_point);
			edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
			return false;
		      }		  
		      
	    //4	  
	    statistic[stat_count++]++;		  
		      
		      /// evaluating data term
		      T b_newenergy_data;	
		      if (!data_fun.eval_data(
			    b_newenergy_data,
			    b_point.direction,
			    b_point.position,
			    b_point.scale))
				{
				  sta_assert_debug0(b_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
				  sta_assert_debug0(t_point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
				  connections.remove_connection(new_connections[0]);
				  connections.remove_connection(new_connections[1]);
				  edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
				  connections.remove_connection(new_terminal_connection);
				  particle_pool.delete_obj(p_point2);
				  particle_pool.delete_obj(p_point);
				  return false;
				  }		
				  
		      T t_newenergy_data;	
		      if (!data_fun.eval_data(
			    t_newenergy_data,
			    t_point.direction,
			    t_point.position,
			    t_point.scale))
				{
				  sta_assert_debug0(b_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
				  sta_assert_debug0(t_point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
				  connections.remove_connection(new_connections[0]);
				  connections.remove_connection(new_connections[1]);
				  edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
				  connections.remove_connection(new_terminal_connection);
				  particle_pool.delete_obj(p_point2);
				  particle_pool.delete_obj(p_point);
				  return false;
				  }				      
	    //5		      
	    statistic[stat_count++]++;		  
		      
		      b_point.point_cost=b_newenergy_data;
		      b_point.saliency=b_newpointsaliency;
		      
		      t_point.point_cost=t_newenergy_data;
		      t_point.saliency=t_newpointsaliency;	  	  
		      
		      
		    ///TODO PointCost 
		      
		      
		    
		    T terminal_point_cost_change=b_point.compute_cost()*(particle_connection_state_cost_scale[2])
		      +(particle_connection_state_cost_offset[2])+
		      t_point.compute_cost()*(particle_connection_state_cost_scale[1])
		      +(particle_connection_state_cost_offset[1]);

		      
		    T E=(new_edgecost-old_edgecost)/temp;
		      E+=(b_point.point_cost+t_point.point_cost)/temp;
		      E+=(terminal_point_cost_change)/temp;

		    T R0=exp(-E);

		     
		    T remove_connection_prob=1.0/(connections.get_num_bifurcations()); 
		    connection_probability*=1.0/(connections.pool->get_numpts()-3);
		    T R=remove_connection_prob/((connection_probability)+std::numeric_limits<T>::epsilon());

		    
//  	     	printf("\nCRE: %f %f %f \n",remove_connection_prob,connection_probability,E);
//  	     	printf("COST: %f %f %f %f %f\n",
// 			new_edgecost,
// 			old_edgecost,
// 			b_point.point_cost,
// 			t_point.point_cost,
// 			terminal_point_cost_change);
//  	     	printf("COST: %f %f %f %f\n",exp(-E),R,R0,R*R0);
		
		R*=R0;
		
		
		    //printf("EDGECOST: %f [%f %f] %f\n",new_edgecost,new_edgecost_test,new_edgecost_test2,new_edgecost_test+new_edgecost_test2);
		    
// try 
// {
// this->tracker->check_consisty();		      
// } catch(mhs::STAError & error)
// {
//     mhs::STAError error2;
//     error2<<"befor test:"<<error.what()<<"\n";
//     throw error2;
// }		    
	    // R=-1;
//  	     	R=1;
		      if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
		      {
			printf("\nCRE: %f %f %f \n",remove_connection_prob,connection_probability,E);
			printf("COST: %f %f %f %f %f\n",
				new_edgecost,
				old_edgecost,
				b_point.point_cost,
				t_point.point_cost,
				terminal_point_cost_change);
			printf("COST: %f %f\n",exp(-E),R);
			    this->tracker->update_energy(E);
			    this->proposal_accepted++;
			    
			
			    if (!tree.insert(*p_point2))
			    {
				connections.remove_connection(new_connections[0]);
				connections.remove_connection(new_connections[1]);
				edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
				connections.remove_connection(new_terminal_connection);		   
				particle_pool.delete_obj(p_point2);
				particle_pool.delete_obj(p_point);
				p_point->print();
				throw mhs::STAError("error insering point\n");
			    }
			    
			    sta_assert_debug0(new_connections[0]->edge_type==EDGE_TYPES::EDGE_BIFURCATION);
			    sta_assert_debug0(new_connections[1]->edge_type==EDGE_TYPES::EDGE_BIFURCATION);
			    sta_assert_debug0(new_terminal_connection->edge_type==EDGE_TYPES::EDGE_SEGMENT);
			    sta_assert_debug0(p_point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			    sta_assert_debug0(p_point2->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
		
		      
			return true;
		      }
	    //6	  
	    statistic[stat_count++]++;		  
		    
			sta_assert_debug0(b_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_terminal_connection);		   
			particle_pool.delete_obj(p_point2);	    
			particle_pool.delete_obj(p_point);
			edge->edge_type=EDGE_TYPES::EDGE_SEGMENT;
			return false;
		    
		    } catch(mhs::STAError & error)
		    {
			throw error;
		    }
		  
		}
	    };





	    template<typename TData,typename T,int Dim>
	    class CProposalBifurcationTrackDeathDouble : public CProposal<TData,T,Dim>
	    {
	    protected:
		T Lprior;
		  const static int max_stat=10;
		int statistic[max_stat];
	    public:




		CProposalBifurcationTrackDeathDouble(): CProposal<TData,T,Dim>()
		{
		    Lprior=-10;
		    for (int i=0;i<max_stat;i++)
		      statistic[i]=0;
		}
		
		  ~CProposalBifurcationTrackDeathDouble()
		{
	    //         printf("\n");
	    // 	for (int i=0;i<max_stat;i++)
	    // 	  printf("%d ",statistic[i]);
	    // 	printf("\n");
		    
		}
		
		T get_default_weight(){return 1;};

		std::string get_name() {
		  return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH);
		};

		void set_params(const mxArray * params=NULL)
		{
		    if (params==NULL)
			return;

		    if (mhs::mex_hasParam(params,"Lprior")!=-1)
			Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

		}

		void init(const mxArray * feature_struct) {};

		bool propose()
		{   
		    pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
		    const std::size_t *		shape			=this->tracker->shape;
		    const T & 			maxscale		=this->tracker->maxscale;
		    const T & 			minscale		=this->tracker->minscale;
		    CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
		    const T &			temp			=this->tracker->opt_temp;
		    const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
		    OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
		    const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
		    const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
		    const T &			collision_search_rad	=this->tracker->collision_search_rad;
		    const std::size_t & 		nvoxel			=this->tracker->numvoxel;
		    const T & 			maxendpointdist		=this->tracker->maxendpointdist;
		    CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
		    class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
		    Connections<T,Dim>  & connections			=this->tracker->connections;
		    T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
		    
		    
		    // TODO Not implemented yet (just a copyt from bif can terminal verseion)
		    sta_assert_error(1==0);
		    
		    this->proposal_called++;
		    
		    /// no terminals?
		    if (connections.get_num_bifurcations()<1)
		      return false;
		    
		    try
		    {
		      /// randomly pic a bifurcation node
		      Points<T,Dim> &  point=connections.get_rand_bifurcation_point();	  
		    
		      /// check if point is a bifurcation! 
		      sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		      sta_assert_debug0(point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT);
		      
		      // there is exactly one side with two connections
		      const int side=(point.endpoint_connections[0]==2) ? 0 : 1;	
		      sta_assert_debug0(point.endpoint_connections[side]==2);
		      
		      ///check if bifurcation terminal
		      if (point.endpoint_connections[1-side]!=0)
			return false;
		    
		      
		    //determine all involved endpoints
		    sta_assert_debug0(point.endpoints[side][0]->connected!=NULL);
		    sta_assert_debug0(point.endpoints[side][1]->connected!=NULL);
		    class Points<T,Dim>::CEndpoint * endpoints[3];
		    endpoints[0]=(point.endpoints[side][0]);
		    endpoints[1]=point.endpoints[side][0]->connected;
		    endpoints[2]=point.endpoints[side][1]->connected;
		    
		    
		    sta_assert_debug0(endpoints[0]->point!=endpoints[1]->point);
		    sta_assert_debug0(endpoints[0]->point!=endpoints[2]->point);
		    sta_assert_debug0(endpoints[2]->point!=endpoints[1]->point);
		    
		    sta_assert_debug0(endpoints[0]->endpoint_connections[0]==2);
		    sta_assert_debug0(endpoints[1]->endpoint_connections[0]==2);
		    sta_assert_debug0(endpoints[2]->endpoint_connections[0]==2);
		    
		    //find the edge that will remain (not connected with point):
		    class Points<T,Dim>::CEndpoint * remaining_edge_endpoint;
		    //remaining_edge_endpoint=endpoints[1]->point->endpoints[endpoints[1]->side][1-endpoints[1]->endpoint_side_indx];
		    remaining_edge_endpoint=endpoints[0]->connected->point->endpoints[endpoints[0]->connected->side][1-endpoints[0]->connected->endpoint_side_indx];
		    sta_assert_debug0(remaining_edge_endpoint->connected!=NULL);
		    sta_assert_debug0(remaining_edge_endpoint->connected->point==endpoints[2]->point);
		    sta_assert_debug0(remaining_edge_endpoint->connected->side==endpoints[2]->side);
		    class Connection<T,Dim> * remaining_connection=remaining_edge_endpoint->connection;
		      
		    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		    //computing edge costs of the OLD configuration
		    T old_edgecost=remaining_connection->compute_cost(
		      edgecost_fun,
		      options.connection_bonus_L,
		      options.bifurcation_bonus_L,
		      maxendpointdist,
		      inrange
		    );
		    
		    /// old edge configuration must be valid!
		    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		    

		    // remove the two directly connected edges (create a backup first)
		    class Connection< T, Dim > edge_backup[2];
		    for (int i=0;i<2;i++)
		    {
		      edge_backup[i]=*(point.endpoints_[side][i].connection);
		      connections.remove_connection(point.endpoints_[side][i].connection);
		    }
		    sta_assert_debug0(remaining_connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION);
		    remaining_connection->edge_type=EDGE_TYPES::EDGE_SEGMENT;
		    
		    T  new_edgecost=remaining_connection->compute_cost(
		      edgecost_fun,
			options.connection_bonus_L,
			options.bifurcation_bonus_L,
			maxendpointdist,
		      inrange
		    );

		    /// endpoint in range but max two edges violates constraint
		    if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		    {
		      sta_assert_error(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
		      sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
		      connections.add_connection(edge_backup[0]);
		      connections.add_connection(edge_backup[1]);
		      sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		      remaining_connection->edge_type=EDGE_TYPES::EDGE_BIFURCATION;
		      return false;
		    }
		    
		    sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));  
		    
		    
		    
		    class Points<T,Dim>::CEndpoint ** remaining_connection_endpoints=remaining_connection->points;
		    const Vector<T,Dim> & centerA=(remaining_connection_endpoints[0]->point->position);
		    const Vector<T,Dim> & centerB=(remaining_connection_endpoints[1]->point->position);
		      
		      T temp_fact_pos=std::sqrt(temp);
		      T temp_fact_scale=temp_fact_pos+1;
		      T & temp_fact_rot=temp_fact_pos;
		      
		      Vector<T,Dim> center=(centerA+centerB)/2;
		      Vector<T,Dim> n[3];
		      n[0]=(centerA-centerB);
		      n[0].normalize();
		      mhs_graphics::createOrths(n[0],n[1],n[2]);
	    // 	  T sigma_inner=std::sqrt((centerA-centerB).norm2())/2;
	    // 	  T sigma_outer=(1+temp_fact_pos)*options.connection_candidate_searchrad;
		      
		      T & scaleA=remaining_connection_endpoints[0]->point->scale;
		      T & scaleB=remaining_connection_endpoints[1]->point->scale;
		      T sigma_inner=(scaleA+scaleB+std::sqrt((centerA-centerB).norm2()))/2;
		      T sigma_outer=(1+temp_fact_pos)*std::sqrt(options.connection_candidate_searchrad);
		      
	    // 	  T sigma_inner=std::sqrt((centerA-centerB).norm2())/2;
	    // 	  T sigma_outer=(1+temp_fact_pos)*std::sqrt(options.connection_candidate_searchrad);
		      

		      
		      Vector<T,Dim> position_with_resp2center=point.position-center;
		      T coordinates[3];
		      coordinates[0]=n[0].dot(position_with_resp2center);
		      coordinates[1]=n[1].dot(position_with_resp2center);
		      coordinates[2]=n[2].dot(position_with_resp2center);
							  
				      
		      Vector<T,Dim> optimal_direction;
		      optimal_direction=point.position-center;
		      optimal_direction.normalize();
		      
		      T connection_probability=1;
		      
		      //computing the probability for the posiiton
		      coordinates[0]*=coordinates[0];
		      coordinates[1]*=coordinates[1];
		      coordinates[2]*=coordinates[2];
		      sigma_inner*=2*sigma_inner;
		      sigma_outer*=2*sigma_outer;
		      T norm=M_PI*M_PI*M_PI;
		      norm*=sigma_inner*sigma_inner*sigma_outer;
		      connection_probability=1.0/std::sqrt(norm)*
					    mexp(coordinates[0]/(sigma_inner)
						+coordinates[1]/(sigma_outer)
						+coordinates[2]/(sigma_outer)
					    );
					    
					      
		      //computing the probability for orientation
		      // considering the connected edge 		
		      Vector<T,Dim> direction=point.direction*Points<T,Dim>::side_sign[1-side];
		      connection_probability*=normal_dist_sphere(optimal_direction,
								direction,
								  temp_fact_rot);
		      
		      //computing the probability for the scale
		      //connection_probability*=1/(maxscale-minscale+std::numeric_limits<T>::epsilon());
		      
			      
		      
		    T terminal_point_cost_change=point.compute_cost()*(particle_connection_state_cost_scale[1])
		      +(particle_connection_state_cost_offset[1]);

		      
		    T E=(new_edgecost-old_edgecost)/temp;
		      E-=(point.point_cost)/temp;
		      E-=(terminal_point_cost_change)/temp;

		    T R=exp(-E);
		      
		    T add_connection_prob=connection_probability/(connections.pool->get_numpts()); 
		    T remove_connection_prob=1.0/(connections.get_num_bifurcations()+3);
		    R*=add_connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());

		      if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
		      {
			printf("\nDEL: %f %f %f \n",add_connection_prob,remove_connection_prob,E);
			printf("COST: %f %f %f %f\n",new_edgecost,old_edgecost,point.point_cost,terminal_point_cost_change);
			printf("COST: %f %f\n",exp(-E),R);
			    this->tracker->update_energy(E);
			    this->proposal_accepted++;
			    particle_pool.delete_obj(&point);
			return true;
		      }
		      
		    
		      sta_assert_error(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
		      sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
		      connections.add_connection(edge_backup[0]);
		      connections.add_connection(edge_backup[1]);
		      sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		      remaining_connection->edge_type=EDGE_TYPES::EDGE_BIFURCATION;
			return false;
		    
		    } catch(mhs::STAError & error)
		    {
			throw error;
		    }
		  
		}
	    };




#endif


