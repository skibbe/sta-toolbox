    friend class CBirth<TData,T,Dim>;
    friend class CDeath<TData,T,Dim>;
    friend class CRotate<TData,T,Dim>;
    friend class CScale<TData,T,Dim>;
    friend class CMove<TData,T,Dim>;
    friend class CProposalConnect<TData,T,Dim>;
    friend class CConnectDeath<TData,T,Dim>;
    friend class CConnectBirth<TData,T,Dim>;
    friend class CInsertDeath<TData,T,Dim>;
    friend class CInsertBirth<TData,T,Dim>;
    friend class CProposalBifurcationBirth<TData,T,Dim>;
    friend class CProposalBifurcationDeath<TData,T,Dim>;
    friend class CProposalBifurcationChange<TData,T,Dim>;
#ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
    friend class CProposalBifurcationTrackBirth<TData,T,Dim>;
    friend class CProposalBifurcationTrackDeath<TData,T,Dim>;
#else     
  #ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING
    friend class CProposalBifurcationTrackBirthDouble<TData,T,Dim>;
    friend class CProposalBifurcationTrackDeathDouble<TData,T,Dim>;
  #endif
#endif
    friend class CProposalBifurcationBirthSimple<TData,T,Dim>;
    friend class CProposalBifurcationDeathSimple<TData,T,Dim>;
    friend class CProposalBifurcationReconn<TData,T,Dim>;
    friend class CProposalBifurcationBirthLine<TData,T,Dim>;
    friend class CProposalBifurcationDeathLine<TData,T,Dim>;
    
    friend class CProposalBifurcationBirthSplit<TData,T,Dim>;
    friend class CProposalBifurcationDeathSplit<TData,T,Dim>;
    
    friend class  CProposalBifurcationCrossingDeath<TData,T,Dim>;
    friend class  CProposalBifurcationCrossingBirth<TData,T,Dim>;
    friend class  CProposalScrambleEdges<TData,T,Dim>;
    friend class  CProposalSmoothEdges<TData,T,Dim>;
    friend class  CProposalFlipTerms<TData,T,Dim>;
    friend  class CBridgeBirth<TData,T,Dim>;
    friend  class CBridgeDeath<TData,T,Dim>;
    friend  class CMSR<TData,T,Dim>;
    friend  class CProposalBifurcationBridgeBirth<TData,T,Dim>;
    friend  class CProposalBifurcationBridgeDeath<TData,T,Dim>;
    friend  class C_init_seed_birth<TData,T,Dim>;
    friend  class CSpike<TData,T,Dim>;
    
    
