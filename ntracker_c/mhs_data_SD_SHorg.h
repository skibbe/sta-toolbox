#ifndef MHS_DATA_SD_SHO_H
#define MHS_DATA_SD_SHO_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataSDSHorg: public CData<T,TData,Dim>
{
private:
    int scale_power;

    const TData * scales;
    std::size_t num_scales;

    const TData * local_stdv;
    int num_alphas_local_stdv;
    
    const TData * data_min_max;
    
//      const TData * local_debug;
//      const TData * local_debug_m;
//        const TData * local_debug_v;
       
    const TData * local_mean;
    int num_alphas_local_mean;

    const TData * sd_data;
    int num_samples_sd_data;
    int order_sd_data;
    int components_sd_data;
    int component_tensor;
    
    
    

    Vector<T,Dim> boundary;
    bool scale_dependend_boundary;


    
    T min_scale;
    T max_scale;
    T scale_correction;
    
    T DataGrad;
    

    T DataThreshold;
    T DataScale;
    T StdvEpsilon;
    T DataMax;
    T DataMin;
    
    T DataGamma;
    
    bool use_AODF_min_max;
    T min_max_epsilon;
    
//     T wxx;
//     T wyy;
//     T wzz;
//     T wxy;
//     T wxz;
//     T wyz;

    
    Vector<T,3> element_size;
    Vector<std::size_t ,3> feature_shape;
    

public:

    float get_minscale() {
        return min_scale/scale_correction;
    };

    float get_maxscale() {
        return max_scale/scale_correction;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataSDSHorg() {
    }

    ~CDataSDSHorg() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
               DataGamma=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
//                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
            printf ( "Th: %f,  Da %f, Gamma %f\n",DataThreshold,DataScale,DataGamma);
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
               * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataGamma;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
        printf ( "Th: %f,  Da %f, Gamma %f\n",DataThreshold,DataScale,DataGamma);
//        printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
    }
#endif

    void init ( const mxArray * feature_struct ) {

        mhs::dataArray<TData>  tmp0;
        mhs::dataArray<TData>  tmp1;
        mhs::dataArray<TData>  tmp2;
        mhs::dataArray<TData>  tmp3;
	mhs::dataArray<TData>  tmp4;
	mhs::dataArray<TData>  tmp5;
	mhs::dataArray<TData>  tmp6;
	
	
	
	
        try {
            tmp3=mhs::dataArray<TData> ( feature_struct,"shape_boundary" );
            sta_assert_error ( tmp3.get_num_elements() ==3 );
            boundary[0]=tmp3.data[0];
            boundary[1]=tmp3.data[1];
            boundary[2]=tmp3.data[2];
            scale_dependend_boundary=true;
        } catch ( mhs::STAError error ) {
            boundary[0]=boundary[1]=boundary[2]=T ( 0 );
            scale_dependend_boundary=false;
        }
        
       

        try {


            tmp1=mhs::dataArray<TData > ( feature_struct,"sd_data" );
            sd_data=tmp1.data;
            num_samples_sd_data=tmp1.dim[tmp1.dim.size()-1];
            printf ( "sd data samples: %d \n",num_samples_sd_data );
	    components_sd_data=tmp1.dim[tmp1.dim.size()-2];
	    //order_sd_data=std::sqrt(4.0*components_sd_data/2)-2;
	    
	    
	    tmp5=mhs::dataArray<TData > ( feature_struct,"L" );
	    order_sd_data=*tmp5.data;
	    component_tensor=((order_sd_data+2)*(order_sd_data+2))/4;
	    
	    printf ( "sd order: %d (components %d)\n",order_sd_data,components_sd_data);

            tmp2=mhs::dataArray<TData> ( feature_struct,"alphas_sdv" );
            local_stdv=tmp2.data;
            num_alphas_local_stdv=tmp2.dim[tmp2.dim.size()-1];
            printf ( "local stdv pol degree: %d \n",num_alphas_local_stdv-1 );

           sta_assert_error ( tmp2.get_num_elements() /num_alphas_local_stdv==tmp1.get_num_elements() /(num_samples_sd_data*(components_sd_data)) );
	   
	    tmp4=mhs::dataArray<TData> ( feature_struct,"alphas_mean" );
            local_mean=tmp4.data;
            num_alphas_local_mean=tmp4.dim[tmp4.dim.size()-1];
            printf ( "local mean pol degree: %d \n",num_alphas_local_mean-1 );

           sta_assert_error ( tmp4.get_num_elements() /num_alphas_local_mean==tmp1.get_num_elements() /(num_samples_sd_data*(components_sd_data)) );

	   
// 	   tmp6=mhs::dataArray<TData> ( feature_struct,"img_m" );
//             local_debug=tmp6.data;
// 	   tmp6=mhs::dataArray<TData> ( feature_struct,"debug_img_slocal_mean" );
//             local_debug_m=tmp6.data;
// 	      tmp6=mhs::dataArray<TData> ( feature_struct,"debug_img_slocal_sdv" );
//             local_debug_v=tmp6.data;
	    
            for ( int i=0; i<3; i++ ) {
                feature_shape[i]=tmp1.dim[i];
            }
        } catch ( mhs::STAError error ) {
            throw error;
        }


        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

        std::swap ( this->shape[0],this->shape[2] );

        for ( int i=0; i<3; i++ ) {
            element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
        }

        printf ( "element size data term:" );
        element_size.print();

        scale_correction=1;
        try {
            scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
        } catch ( mhs::STAError error ) {
            throw error;
        }




	sta_assert_error ( num_scales==num_samples_sd_data );

        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"override_scales" ).data;
            min_scale=scales[0]*scale_correction;
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"override_scales" ).dim[0]-1]*scale_correction;
        } catch ( mhs::STAError error ) {
            min_scale=scales[0];
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
        }
        
        try {
            data_min_max=mhs::dataArray<TData> ( feature_struct,"min_max" ).data;
	    printf ( "found min-max AODF information \n" );
        } catch ( mhs::STAError error ) {
	    data_min_max=NULL;
        }

    }

    void set_params ( const mxArray * params=NULL ) {
        scale_power=2;
        DataScale=1;
        DataThreshold=0;
        StdvEpsilon=0.001;
	DataGrad=-1;
	DataMax=-1;
	DataMin=1;
	DataGamma=1;
	use_AODF_min_max=false;
	min_max_epsilon=0.0000000001;

        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataGrad" ) !=-1 ) {
                    DataGrad=mhs::mex_getParam<T> ( params,"DataGrad",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"Epsilon" ) !=-1 ) {
                    StdvEpsilon=mhs::mex_getParam<T> ( params,"Epsilon",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataMax" ) !=-1 ) {
                    DataMax=mhs::mex_getParam<T> ( params,"DataMax",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataMin" ) !=-1 ) {
                    DataMin=mhs::mex_getParam<T> ( params,"DataMin",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataGamma" ) !=-1 ) {
                    DataGamma=mhs::mex_getParam<T> ( params,"DataGamma",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"min_max_epsilon" ) !=-1 ) {
                    min_max_epsilon=mhs::mex_getParam<T> ( params,"min_max_epsilon",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"use_AODF_min_max" ) !=-1 ) {
                    use_AODF_min_max=mhs::mex_getParam<bool> ( params,"use_AODF_min_max",1 ) [0];
                }
                
                if (use_AODF_min_max)
		{
		  printf ( "using AODF information when avaliable\n" );
		  printf ( "disabling DataGrad\n" );
		  DataGrad=-1;
		}
                
                
                sta_assert_error((!(DataGamma>1)) && (!(DataGamma<0)));
                
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:



public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
      //,const Points<T,Dim> *org_point=NULL
	
    ) {


        try {

	  
// 	  T bla=mhs_fast_math<T>::plm(1,2,0);
// 	  printf("%f\n",bla);
	  
            Vector<T,Dim> position=position_org;
	    
// 	    position=Vector<T,Dim>(63,92,55);
// 	   position=Vector<T,Dim>(55,63,92);
// 	    
            position/=this->element_size;


            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->feature_shape[0] ) || ( Y+1>=this->feature_shape[1] ) || ( X+1>=this->feature_shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }
            


            T org_scale=radius;

            radius*=scale_correction;

            if ( ( radius<min_scale-0.01 )
                    || ( radius>max_scale+0.01 ) ) {
                printf ( "[%f %f %f]\n",min_scale,radius,max_scale );
                sta_assert_error ( ! ( radius<min_scale ) );
                sta_assert_error ( ! ( radius>max_scale ) );
            }


            radius=std::min ( max_scale,std::max ( radius,min_scale ) );

            const T boundary_scale=2;
            if ( ( scale_dependend_boundary ) &&
                    ( ( position[0]+boundary_scale*org_scale>=this->feature_shape[0] ) ||
                      ( position[1]+boundary_scale*org_scale>=this->feature_shape[1] ) ||
                      ( position[2]+boundary_scale*org_scale>=this->feature_shape[2] ) ||
                      ( position[0]-boundary_scale*org_scale<0 ) ||
                      ( position[1]-boundary_scale*org_scale<0 ) ||
                      ( position[2]-boundary_scale*org_scale<0 ) ) ) {
                return false;
            }

         
	    T stdv=0;
            T mean=0;
	    
	    if (!(use_AODF_min_max && (data_min_max!=NULL)))
	    {
		if (num_alphas_local_stdv==1)
		{
		  if ( !data_interpolate<T,T,TData> ( position,stdv,local_stdv,feature_shape.v ) ) {
		      return false;
		  };
		  
	
		  if ( !data_interpolate<T,T,TData> ( position, mean,local_mean,feature_shape.v ) ) {
		      return false;
		  };
		  //printf("%f %f %f %f\n",mean,mean2,stdv,stdv2);
		}else
		{
		  
		  if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
		      return false;
		  };
		
		  if ( !data_interpolate_polynom<T,T,TData> ( position,radius,NULL,local_mean, num_alphas_local_mean,mean,feature_shape.v ) ) {
		      return false;
		  };  
		}
	    }
	    
	    stdv= ( std::max ( stdv,T ( 0 ) ) );
	    
	    
// 	    T f_max=0;
// 	    T f_m=0;
// 	    T f_v=0;
//              if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,local_debug, num_alphas_local_mean,f_max,feature_shape.v ) ) {
// 		  return false;
// 	      };  
// 	       if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,local_debug_m, num_alphas_local_mean,f_m,feature_shape.v ) ) {
// 		  return false;
// 	      };  
// 	       if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,local_debug_v, num_alphas_local_mean,f_v,feature_shape.v ) ) {
// 		  return false;
// 	      };  
	        
	    
// 	    printf("%f | %f \n",stdv,mean);

	    
	    T sd_filter=0;
 	    T sd_filter_2=0;
if (false)
{
	    std::complex<T> tensor[component_tensor]; 
	    {
	      T * tensor_p=(T*)tensor; 
// 	    for ( int i=0; i<components_sd_data; i++ ) {
  // 		if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[i],feature_shape.v,components_sd_data,i ) ) {
  // 		    return false;
  // 		}
  // 	    }

//  	      Vector<T,3> position(31.31,31.32,31.45);
// 	      T radius=scales[0];
	      std::size_t count=0;
	      std::size_t count_data=0;
	      for (int l=0;l<=order_sd_data;l+=2)
	      {
		for (int m=-l;m<=0;m++)
		{
		  sta_assert_debug0(count<2*component_tensor);
		  if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++) ) {
			return false;
		    }
		  
		  count++;
		  
		  sta_assert_debug0(count<2*component_tensor);
		  if (m!=0)
		  {
		    
		    if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++ ) ) {
			return false;
		    }
		    
		   
		  }else
		  {
		    tensor_p[count]=0;
		  }
		  count++;
		    
		}
	      }
	      
// 	      for (int a=0;a<component_tensor;a++)
// 	      {
// 		printf("[%f %f]\n",tensor[a].real(),tensor[a].imag());
// 	      }
// 	      sta_assert_debug0(false);
	    }
	    
	    
	     std::complex<T> tmp;
	    {
	      
	      const T & x=direction[2];
	      const T & y=direction[1];
	      const T & z=direction[0];
	    
	      //std::complex<T> x_myi=std::complex<T>(x,-y);
	      std::complex<T> x_myi=std::complex<T>(x,y); //contravariant
	      std::complex<T> x_myi2=x_myi*x_myi;
	
	      T x2=x*x;
	      T x4=x2*x2;
	      T y2=y*y;
	      T y4=y2*y2;
	      T z2=z*z;
	      T z4=z2*z2;
	      
	      tmp=tensor[0];
// 	      tmp=0;
	      
	      
	      if (order_sd_data>=2)
// 	      if (true)
	      {
		const T l_weight=2*2+1;
		tmp+=T(l_weight*std::sqrt(3.0/2.0))*(x_myi2 * tensor[1]);
 		tmp+=T(l_weight*(std::sqrt(6.0)*z)) *(x_myi*tensor[2]);
 		tmp+=T(l_weight*((-x2-y2)/2+z2))*tensor[3];
	      }
// 	      
	      if (order_sd_data>=4)
// 		 if (true)
	      {
		const T l_weight=4*2+1;
		tmp+=T(l_weight*1.0/4.0 * std::sqrt(35.0/2.0))*(x_myi2*x_myi2*tensor[4]);
		tmp+=T(l_weight*1.0/2.0 * std::sqrt(35.0))*(x_myi2*x_myi*z*tensor[5]);
 		tmp+=T(l_weight*-1.0/2.0 * std::sqrt(5.0/2.0)*(x2 + y2 - 6*z2))*(x_myi2*tensor[6]);
 		tmp+=T(l_weight*-1.0/2.0*std::sqrt(5.0) * z *(3 * x2+ 3 * y2 - 4 * z2)) *(x_myi*tensor[7]);
 		tmp+=T(l_weight*(3* (x4)/8 +  3* (y4)/8 - 3* y2 * z2 + z4 + 3.0/4.0 * x2 *(y2 - 4 * z2)))*tensor[8];
	      }
	      
	      sd_filter=tmp.real();
// 	      printf("%f %f\n",tmp.real(),tmp.imag());
	    }
	    
}

T grad=0;

if (true)
{	 
    T tensor_real[2*component_tensor]; 
    if (!sd_filter_response(tensor_real, position,direction, radius, sd_filter))
      return false;
    
    if (DataGrad>0)
    {
      Vector<T,3> pos=position+direction;
      T tmp;
      if (!sd_filter_response(tensor_real, pos,direction, radius, grad))
	return false;
      
		  pos=position-direction;
      if (!sd_filter_response(tensor_real, pos,direction, radius, tmp))
	return false;
      
      grad=std::abs(grad-tmp)/2;

    }
    
}


  



if (false)
{	    
	    
	    
	    T tensor_real[2*component_tensor]; 
	    
	    
	    
	    {
	      T * tensor_p=tensor_real; 
	      std::size_t count=0;
	      std::size_t count_data=0;
	      for (int l=0;l<=order_sd_data;l+=2)
	      {
		for (int m=-l;m<=0;m++)
		{
		  sta_assert_debug0(count<2*component_tensor);
		  if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++) ) {
			return false;
		    }
		  
		  count++;
		  
		  sta_assert_debug0(count<2*component_tensor);
		  if (m!=0)
		  {
		    
		    if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++ ) ) {
			return false;
		    }
		    
		    count++;
		  }
		}
	      }
	    }
	    
	    if (true)
	     {
	      const T & x=direction[2];
	      const T & y=direction[1];
	      const T & z=direction[0];

	      T * tensor_p=tensor_real; 
	      
	      T x2=x*x;
	      T x3=x2*x;
	      T x4=x2*x2;
	      T y2=y*y;
	      T y3=y2*y;
	      T y4=y2*y2;
	      T z2=z*z;
	      T z4=z2*z2;
	      
	      T m0=x*y;
	      T m1=(x2-y2);
	      T m2=(y2-4*z2);
	      T m3=(x2+y2);
	      T m4=(x4+y4);	      
	      
	      
 	      T tmp=tensor_p[0];
	      if (order_sd_data>=2)
	      {
		const T &a22r=tensor_p[1];
		const T &a22i=tensor_p[2];
		const T &a21r=tensor_p[3];
		const T &a21i=tensor_p[4];
		const T &a20r=tensor_p[5];
		
		const T l_weight=2*2+1;
		tmp+=T(l_weight*std::sqrt(3.0/2.0))*(a22r * m1 - a22i*2*m0 );
 		tmp+=T(l_weight*(std::sqrt(6.0)*z)) *(a21r * x - a21i * y);
 		tmp+=T(l_weight*(z2-m3/2))*a20r;
		
	      }
// 	      
// 	      if (true)
	      if (order_sd_data>=4)
	      {
		const T &a44r=tensor_p[6];
		const T &a44i=tensor_p[7];
		const T &a43r=tensor_p[8];
		const T &a43i=tensor_p[9];
		const T &a42r=tensor_p[10];
		const T &a42i=tensor_p[11];
		const T &a41r=tensor_p[12];
		const T &a41i=tensor_p[13];
		const T &a40r=tensor_p[14];		
		
		const T l_weight=4*2+1;
		
		tmp+=T(l_weight*1.0/4.0 * std::sqrt(35.0/2.0))*(a44r *(m4 -6*x2*y2 ) + a44i*4*(x*y3 - x3*y));
		tmp+=T(l_weight*1.0/2.0 * std::sqrt(35.0))*z*(a43r* (x3-3*x*y2) + a43i * (y3-3*x2*y));
 		tmp+=T(l_weight*-1.0/2.0 * std::sqrt(5.0/2.0)*(m3 - 6*z2))*(a42r *m1 - a42i * (2*m0));
		tmp+=T(l_weight*-1.0/2.0*std::sqrt(5.0) * z *(x2+2* m3 + m2)) *(a41r*x- a41i*y);
		tmp+=T(l_weight*3* (m4/8  - y2 * z2 + z4/3 + x2*m2/4))*a40r;
	      }
	      
	      
	      
	      sd_filter=tmp;
	    }
}    
	
// if (sd_filter<0)	
/*   	  printf("%f %f %f\n",sd_filter,f_max,f_max-mean);
 return false;	  */  

            T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=org_scale;
                break;
            case 2:
                scale1=org_scale*org_scale;
                break;
            case 3:
                scale1=org_scale*org_scale*org_scale;
                break;
            }
            
            
           
            
            T sd_filt;
	     T data_min;
	    T data_max;
	    
	   if (use_AODF_min_max && (data_min_max!=NULL))
	   {
	   
	    if ( !data_interpolate<T,T,TData> ( position,data_min,data_min_max,feature_shape.v,2,0 ) ) {
		  return false;
	    };
	    if ( !data_interpolate<T,T,TData> ( position,data_max,data_min_max,feature_shape.v,2,1 ) ) {
		  return false;
	    };
	    sd_filt=(sd_filter)/(std::abs(data_max)+min_max_epsilon);
	   }else
	   {
	    sd_filt=stdv*(sd_filter-mean);
	    if (DataMax>-1)
	    {
	      sd_filt=std::min(sd_filt,DataMax);
	    }
	    if (DataMin<1)
	    {
	      sd_filt=std::max(sd_filt,DataMin);
	    }
	    
	    if (DataGamma<1)
	    {
	      sd_filt=((sd_filt>0) ? 1 : (-1)) * std::pow(std::abs(sd_filt),DataGamma);
	    }
	   }

#ifdef D_USE_GUI
            if ( debug!=NULL ) {
                std::stringstream s;
                s.precision ( 3 );
                s<<"------------------";
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<" SD : "<<sd_filter<<" N : " <<sd_filt;
                debug->push_back ( s.str() );


/*                s.str ( "" );
                s<<"SDm: "<<f_max<<" m:"<<f_m<<" v:"<<f_v;
                debug->push_back ( s.str() );	*/	
		
                s.str ( "" );
                s<<"sdv : "<<stdv<< "mean : "<<mean;
                debug->push_back ( s.str() );
		
		
		s.str ( "" );
                s<<"grad : "<<grad<< " gradn : "<<grad*stdv;
                debug->push_back ( s.str() );
		
		if (use_AODF_min_max && (data_min_max!=NULL))
		{
		s.str ( "" );
                s<<"("<<data_min<< " << "<<sd_filter<<" << "<<data_max;
                debug->push_back ( s.str() );
		}
		
// 		s.str ( "" );
//                 s<<"C :"<<sd_filter<< " R: "<<sd_filter_2;
//                 debug->push_back ( s.str() );
		
// 		s.str ( "" );
//                 s<<"r :"<<tmp.real()<< " i: "<<tmp.imag();
//                 debug->push_back ( s.str() );
		
            }
#endif


	  
	    

	    if (DataGrad>0)
	    {
	       result=-(DataScale*sd_filt-DataGrad*grad*stdv+DataThreshold)*scale1;
	    }else
	    {
	      result=-(DataScale*sd_filt+DataThreshold)*scale1;
	    }

        } catch ( mhs::STAError error ) {
            throw error;
        }

        return true;
    }
    
    
    bool sd_filter_response(T * tensor_buffer, const Vector<T,Dim> & position,const Vector<T,Dim> & direction, T radius, T & sd_filter)
    {
	    
	    {
	      T * tensor_p=tensor_buffer; 
	      std::size_t count=0;
	      std::size_t count_data=0;
	      for (int l=0;l<=order_sd_data;l+=2)
	      {
		for (int m=-l;m<=0;m++)
		{
		  sta_assert_debug0(count<2*component_tensor);
		  if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++) ) {
			return false;
		    }
		  
		  count++;
		  
		  sta_assert_debug0(count<2*component_tensor);
		  if (m!=0)
		  {
		    
		    if ( !data_interpolate_polynom<T,T,TData> ( position,radius,scales,sd_data, num_samples_sd_data,tensor_p[count],feature_shape.v,components_sd_data,count_data++ ) ) {
			return false;
		    }
		    
		    count++;
		  }
		}
	      }
	    }
	    
	    
	     {
	      const T & x=direction[2];
	      const T & y=direction[1];
	      const T & z=direction[0];

	      T * tensor_p=tensor_buffer; 
	      
	      T x2=x*x;
	      T x3=x2*x;
	      T x4=x2*x2;
	      T y2=y*y;
	      T y3=y2*y;
	      T y4=y2*y2;
	      T z2=z*z;
	      T z4=z2*z2;
	      
	      T m0=x*y;
	      T m1=(x2-y2);
	      T m2=(y2-4*z2);
	      T m3=(x2+y2);
	      T m4=(x4+y4);	      
	      
	      
 	      T tmp=tensor_p[0];
	      if (order_sd_data>=2)
	      {
		const T &a22r=tensor_p[1];
		const T &a22i=tensor_p[2];
		const T &a21r=tensor_p[3];
		const T &a21i=tensor_p[4];
		const T &a20r=tensor_p[5];
		
		const T l_weight=2*2+1;
		tmp+=T(l_weight*std::sqrt(3.0/2.0))*(a22r * m1 - a22i*2*m0 );
 		tmp+=T(l_weight*(std::sqrt(6.0)*z)) *(a21r * x - a21i * y);
 		tmp+=T(l_weight*(z2-m3/2))*a20r;
		
	      }
// 	      
// 	      if (true)
	      if (order_sd_data>=4)
	      {
		const T &a44r=tensor_p[6];
		const T &a44i=tensor_p[7];
		const T &a43r=tensor_p[8];
		const T &a43i=tensor_p[9];
		const T &a42r=tensor_p[10];
		const T &a42i=tensor_p[11];
		const T &a41r=tensor_p[12];
		const T &a41i=tensor_p[13];
		const T &a40r=tensor_p[14];		
		
		const T l_weight=4*2+1;
		
		tmp+=T(l_weight*1.0/4.0 * std::sqrt(35.0/2.0))*(a44r *(m4 -6*x2*y2 ) + a44i*4*(x*y3 - x3*y));
		tmp+=T(l_weight*1.0/2.0 * std::sqrt(35.0))*z*(a43r* (x3-3*x*y2) + a43i * (y3-3*x2*y));
 		tmp+=T(l_weight*-1.0/2.0 * std::sqrt(5.0/2.0)*(m3 - 6*z2))*(a42r *m1 - a42i * (2*m0));
		tmp+=T(l_weight*-1.0/2.0*std::sqrt(5.0) * z *(x2+2* m3 + m2)) *(a41r*x- a41i*y);
		tmp+=T(l_weight*3* (m4/8  - y2 * z2 + z4/3 + x2*m2/4))*a40r;
	      }
	      
	      
	      
	      sd_filter=tmp;
	    }
	    
	    return true;
    }
    
    
    
};

#endif
