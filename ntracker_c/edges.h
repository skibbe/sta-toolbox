#ifndef EDGES_H
#define EDGES_H

#include "particles.h"
#include "edgecost.h"
#include "global.h"

enum class EDGE_TYPES : unsigned int
{
    EDGE_SEGMENT=0,
    EDGE_BIFURCATION=1,
    EDGE_UNDEFINED,
};

template<typename T,int Dim> class Points;



template<typename T,int Dim>
class Connection: public pool_element<class Connection<T,Dim> >
    {

    protected:

        friend pool<Connection<T,Dim> >;
        virtual void clear() {
            pool_element<class Connection >::clear();
#ifdef D_USE_MULTITHREAD
            track_me_id= track_unique_id[omp_get_thread_num()];
#else

            track_me_id=track_unique_id[0];
#endif


            //FIXM ucommented for visualization
            pointA=NULL;
            pointB=NULL;

            edge_type=EDGE_TYPES::EDGE_SEGMENT;

            time_stamp=0;
            //cost=-1;
        }

        std::size_t time_stamp;
    public:
      
        int debug_value;

	bool freeze_topology;
        void set_stamp ( std::size_t s ) {
            time_stamp=s;
        };
        std::size_t get_stamp() const {
            return time_stamp;
        }
        
        
	bool is_protected_topology() const {
	    return freeze_topology;
	}


// 	int test=0;
        const static T no_connection_cost;
        union {
            struct {
                class Points< T, Dim >::CEndpoint * pointA;
                class Points< T, Dim >::CEndpoint * pointB;
            };
            class Points< T, Dim >::CEndpoint * points[2];
        };
        //T cost;
        //T cost_tmp;
        int track_me_id;
        int track_depth;
        EDGE_TYPES edge_type;
        Connection() : pointA ( NULL ),pointB ( NULL ),track_me_id ( 0 ),time_stamp ( 0 ) {
            clear();
	    debug_value=0;
	    freeze_topology=false;
            //test=0;
        };

        T e_cost (
            class CEdgecost<T,Dim> & edgecost_fun,
            T edgeL,
            T bifurL,
            typename CEdgecost<T,Dim>::EDGECOST_STATE & inrange,
	    bool check_distance=true,
//         bool debug=false,bool update_edgepoints=true) const
            bool debug=false
#ifdef EDGECOST_DEBUG        
	, std::stringstream * debugstream=NULL
#endif   		 
	) const {
// 	    if (debug)
// 	      printf("cmputing edge costs\n");


            switch ( edge_type ) {
            case EDGE_TYPES::EDGE_SEGMENT: {
// 		if (update_edgepoints)
// 		{
// 		pointA->point->update_endpoint(pointA->side);
// 		pointB->point->update_endpoint(pointB->side);
// 		}
                sta_assert_error_m ( pointA->endpoint_connections[0]<2,pointA->endpoint_connections[0] );
                sta_assert_error_m ( pointB->endpoint_connections[0]<2,pointB->endpoint_connections[0] );

                T u=edgecost_fun.e_cost (
                        *pointA,
                        *pointB,
                        inrange,check_distance,debug
#ifdef EDGECOST_DEBUG        
	,debugstream
#endif   			
					) +edgeL;
#ifdef EDGECOST_DEBUG
if (debugstream!=NULL)
{
	(*debugstream)<<"u:"<<u<<"   L:"<<edgeL<<"\n";
}
#endif					
					

                return u;
            }
            case EDGE_TYPES::EDGE_BIFURCATION: {
                throw mhs::STAError ( "edge: compute_cost: should not be used anymore" );
                class Points< T, Dim >::CEndpoint * pointC;
                int sideA=pointA->side;
                int side_indx_A=pointA->endpoint_side_indx;
                class Points< T, Dim > * particleA=pointA->point;

                sta_assert_debug0 ( side_indx_A<2 );
                pointC=particleA->endpoints[sideA][1-side_indx_A]->connected;
                sta_assert_debug0 ( pointC!=NULL );
                sta_assert_debug0 ( pointC!=pointA );
                sta_assert_debug0 ( pointC!=pointB );

                sta_assert_debug0 ( pointA->endpoint_connections[0]==2 );
                sta_assert_debug0 ( pointB->endpoint_connections[0]==2 );
                sta_assert_debug0 ( pointC->endpoint_connections[0]==2 );

// 		if (update_edgepoints)
// 		{
// 		pointA->point->update_endpoint(pointA->side);
// 		pointB->point->update_endpoint(pointB->side);
// 		pointC->point->update_endpoint(pointC->side);
// 		}

                T u=edgecost_fun.e_cost (
                        *pointA,
                        *pointB,
                        *pointC,
                        inrange,check_distance,debug ) +bifurL;
// 		if (debug)
// 		{
// 		  printf("@ %u %u %u [%d]\n",pointA->point,pointB->point,pointC->point,inrange);
// 		}

                return u;
            }
            default:
                throw mhs::STAError ( "compute_cost: should never be reached" );
                return 0;
            }
        }

        class Points<T,Dim>::CEndpoint *  switch_pole ( class Points<T,Dim> * point=NULL ) {
            //printf("warning: switch_pole(class Points<T,Dim> * point=NULL) buggy\n");
            Connection<T,Dim> & connection=*this;
            if ( point!=NULL ) {
                int edge_side= ( connection.pointB->point!=point ) ? 0 : 1;
                class Points<T,Dim>::CEndpoint * switching_edge_side=connection.points[edge_side];
                sta_assert_debug0 ( switching_edge_side->point==point );
// printf("s0");
                {
                    int old_point_side=switching_edge_side->side;
                    // printf("(edge side %d) switching from : %d -> %d\n",edge_side,old_point_side,1-old_point_side);

                    //printf("(new point side %d) \n",1-old_point_side);

                    class Points<T,Dim>::CEndpoint * free_endpt=switching_edge_side->point->getfreehub ( 1-old_point_side );
                    if ( free_endpt==NULL ) {
                        return NULL;
                    }

                    //  printf("(new point side %d) \n",free_endpt->side);
                    /*printf("s1");	*/
                    ( *switching_edge_side->endpoint_connections )--;
                    switching_edge_side->connected=NULL;
                    switching_edge_side->connection=NULL;
                    /*printf("s2");*/
                    switching_edge_side=free_endpt;


                    // printf("(new edge point %d) \n",switching_edge_side->side);


                    switching_edge_side->connection=this;
                    /*printf("s3"); */
                    switching_edge_side->connected=connection.points[1-edge_side];
                    ( *switching_edge_side->endpoint_connections ) ++;

                    connection.points[edge_side]=switching_edge_side;
                    /*printf("s4");*/
                    return free_endpt;
                }


            }
            return NULL;
        }
    };




template<typename T,int Dim>
class Connections
{
    /*friend  CTracker<T,Dim>;     */
private:
    std::size_t time_stamp;
public:
  
  

  
    std::vector<Points<T,Dim> * > terminals;
    std::size_t num_terminals;

    std::vector<Points<T,Dim> * > bifurcation_points;
    std::size_t num_bifurcation_points;

    std::vector<Points<T,Dim> * > bifurcation_center_points;
    std::size_t num_bifurcation_center_points;

    typedef class pool<class Connection<T,Dim> > pclass;
    pclass * pool;

    std::size_t pointlimit;

    class Connection<T,Dim> * get_rand_edge (
            class volume_lock<T,Dim> & vlock,
            T lock_radius,
                bool & empty ) {
        empty=false;
        Connection<T,Dim> * edge_ptr=NULL;
#ifdef _MCPU
        #pragma omp critical (EDGE_POINTER)
#endif
        {

            if ( pool->get_numpts() >0 ) {
                edge_ptr= ( pool->getMemW() [std::rand() %pool->get_numpts()] );
                bool can_lock=vlock.lock ( * ( edge_ptr->pointA->position ),lock_radius );
                if ( !can_lock ) {
                    edge_ptr=NULL;
                }
            } else {
                empty=true;
            }
        }
        return edge_ptr;
    }

    class Connection<T,Dim> * get_rand_edge() {

        Connection<T,Dim> * edge_ptr=NULL;
#ifdef _MCPU
        sta_assert_error ( 1!=1 );
        #pragma omp critical (EDGE_POINTER)
#endif
        if ( pool->get_numpts() >0 ) {
            edge_ptr= ( pool->getMemW() [std::rand() %pool->get_numpts()] );
        }

        return edge_ptr;
    }



    void init_pool ( std::size_t pointlimit=10000000 ) {
        this->pointlimit=pointlimit;
        sta_assert_error ( pool==NULL );
        pool=new pclass ( pointlimit );
        terminals.resize ( pointlimit );
        bifurcation_points.resize ( pointlimit );
        bifurcation_center_points.resize ( pointlimit );
    }

    Connections() {

	
      
        num_terminals=0;

        num_bifurcation_points=0;

        num_bifurcation_center_points=0;
        pool=NULL;
        time_stamp=0;
    }

    void increase_stamp() {
        time_stamp++;
    };

    ~Connections() {
        //sta_assert_error ( pool!=NULL );
      if (pool!=NULL)
      {
        delete pool;
      }else
      {
	printf("WARNING: pool = NULL; this should never have happened!!\n");
      }
    }

    int get_num_terminals() {
        return num_terminals;
    }

    Points<T,Dim> & get_rand_point() {
        sta_assert_error ( num_terminals>0 );
        return * ( terminals[std::rand() %num_terminals] );
    }
    void check_consistency2 (CEdgecost <T,Dim> * edgecost_fun=NULL ,int debug_id=0 ) {

        sta_assert_error ( pool!=NULL );
// 	    printf("checking consistency (edges) ..");

	
	
	
	
        std::size_t numconnections=pool->get_numpts();
	
	
	if (debug_id>0)
	{
	  Connection<T,Dim> ** conptr=pool->getMemW();
	  
	    for ( std::size_t i=0; i<numconnections; i++ ) {
		Connection<T,Dim> * con=*conptr++;
		con->debug_value=debug_id;
	    }
	}
	
	
        const Connection<T,Dim> ** conptr=pool->getMem();
           {
            for ( std::size_t i=0; i<numconnections; i++ ) {
                const Connection<T,Dim> * con=*conptr++;
		
		if (debug_id>0)
		{
		    sta_assert_error(con->debug_value==debug_id);
		  
		}

		if (edgecost_fun!=NULL)
		{
		  if (( con->edge_type!=EDGE_TYPES::EDGE_BIFURCATION ))
		  {
		    if (!edgecost_fun->particle_angle_valid(*(con->pointA),*(con->pointB)))
		    {
		      
			      sta_assert_error(con->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			      sta_assert_error(con->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			      mhs::STAError error;
// 			      error<<"error checking edge angle: edge is bif ?:"<< (con->edge_type==EDGE_TYPES::EDGE_BIFURCATION)<<"\n";
			      error<<"error checking edge angle\n";
			      throw error;
		    }
		  }
		}

            }
            return;
        }


// 	    printf("done\n");

        printf ( "checking consistency (terminals) .." );
        for ( std::size_t i=0; i<num_terminals; i++ ) {
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
            sta_assert_error (
                ( ( terminals[i]->endpoint_connections[0]==0 ) && ( terminals[i]->endpoint_connections[1]==1 ) )
                || ( ( terminals[i]->endpoint_connections[1]==0 ) && ( terminals[i]->endpoint_connections[0]==1 ) ) );
#else
            sta_assert_error (
                ( ( terminals[i]->endpoint_connections[0]==0 ) ) || ( ( terminals[i]->endpoint_connections[1]==0 ) ) );
#endif
            for ( std::size_t j=i+1; j<num_terminals; j++ ) {
                sta_assert_error ( terminals[i]!=terminals[j] );
            }
        }
        printf ( "done\n" );
    }
    
    
    void check_consistency4 (CEdgecost <T,Dim> & edgecost_fun) {

        sta_assert_error ( pool!=NULL );
// 	    printf("checking consistency (edges) ..");
	
	
        std::size_t numconnections=pool->get_numpts();
	
	
        const Connection<T,Dim> ** conptr=pool->getMem();
           {
            for ( std::size_t i=0; i<numconnections; i++ ) {
                const Connection<T,Dim> * con=*conptr++;
		
		
		typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		
		con->e_cost(edgecost_fun,0,0,
		    inrange);
		
		sta_assert_error((inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		
		
		
            }
            return;
        }
    }
    
    void check_consistency3 (CEdgecost <T,Dim> & edgecost_fun) {

        sta_assert_error ( pool!=NULL );
// 	    printf("checking consistency (edges) ..");

	
	
	
	
        std::size_t numconnections=pool->get_numpts();
	
	
        const Connection<T,Dim> ** conptr=pool->getMem();
           {
            for ( std::size_t i=0; i<numconnections; i++ ) {
                const Connection<T,Dim> * con=*conptr++;
		
		T dist_m=edgecost_fun.get_current_edge_length_limit();
		
		T dist_c=std::sqrt((*con->pointA->position-*con->pointB->position).norm2());
		bool ok=true;
		if ((dist_c>dist_m))
		{
		  printf("\n A ###################\n");
		  printf("%f %f\n",dist_c,dist_m);
		  printf("\n A ###################\n");
		  ok=false;
		}
		  
		
		con->pointA->point->update_endpoints2();
		con->pointB->point->update_endpoints2();
		dist_c=std::sqrt((*con->pointA->position-*con->pointB->position).norm2());
		if ((dist_c>dist_m))
		{
		  printf("\n B ###################\n");
		  printf("%f %f\n",dist_c,dist_m);
		  printf("\n B ###################\n");
		  ok=false;
		}
		sta_assert_error(ok);
		
            }
            return;
        }


// 	    printf("done\n");

        printf ( "checking consistency (terminals) .." );
        for ( std::size_t i=0; i<num_terminals; i++ ) {
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
            sta_assert_error (
                ( ( terminals[i]->endpoint_connections[0]==0 ) && ( terminals[i]->endpoint_connections[1]==1 ) )
                || ( ( terminals[i]->endpoint_connections[1]==0 ) && ( terminals[i]->endpoint_connections[0]==1 ) ) );
#else
            sta_assert_error (
                ( ( terminals[i]->endpoint_connections[0]==0 ) ) || ( ( terminals[i]->endpoint_connections[1]==0 ) ) );
#endif
            for ( std::size_t j=i+1; j<num_terminals; j++ ) {
                sta_assert_error ( terminals[i]!=terminals[j] );
            }
        }
        printf ( "done\n" );
    }

    void check_consistency (
	T max_endpoint_dist2=-1,CEdgecost <T,Dim> * edgecost_fun=NULL ) {

        sta_assert_error ( pool!=NULL );
// 	    printf("checking consistency (edges) ..");

        std::size_t numconnections=pool->get_numpts();
        const Connection<T,Dim> ** conptr=pool->getMem();
        if ( max_endpoint_dist2>0 ) {
            for ( std::size_t i=0; i<numconnections; i++ ) {
                const Connection<T,Dim> * con=*conptr++;

		if (edgecost_fun!=NULL)
		{
		  if (( con->edge_type!=EDGE_TYPES::EDGE_BIFURCATION ))
		  {
		    if (!edgecost_fun->particle_angle_valid(*(con->pointA),*(con->pointB)))
		    {
		      
			      sta_assert_error(con->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			      sta_assert_error(con->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
			      mhs::STAError error;
// 			      error<<"error checking edge angle: edge is bif ?:"<< (con->edge_type==EDGE_TYPES::EDGE_BIFURCATION)<<"\n";
			      error<<"error checking edge angle\n";
			      throw error;
		    }
		  }
		}
		
		
		
                if ( ( con->get_stamp() ==time_stamp ) && ( con->pointA!=NULL ) ) {
                    sta_assert_error ( ( con->pointB!=NULL ) );
                    //  	      con->pointA->point->update_endpoints();
                    //  	      con->pointB->point->update_endpoints();
                    if ( con->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
                        for ( int a=0; a<2; a++ ) {
                            if ( con->points[a]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {
// 		      typename Points<T,Dim>::CEndpoint * points[3];
                                typename Points<T,Dim>::CEndpoint * points[3];
                                points[0]=con->points[a]->point->endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected;
                                points[1]=con->points[a]->point->endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected;
                                points[2]=con->points[a]->point->endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected;


                                for ( int j=0; j<3; j++ ) {
                                    T endpoint_dist2= ( *points[j]->position-*points[ ( j+1 ) %3]->position ).norm2();
                                    if ( ! ( max_endpoint_dist2>endpoint_dist2 ) ) {
                                        if ( con->edge_type==EDGE_TYPES::EDGE_SEGMENT ) {
                                            printf ( "edge is segment\n" );
                                        }
                                        if ( con->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
                                            printf ( "edge is bifurcation edge\n" );
                                        }

                                        mhs::STAError error;
                                        error<<"error checking edge dist:"<<endpoint_dist2<<" but max"<<max_endpoint_dist2<<"\n";
                                        throw error;
                                    }
                                }

                            }
                        }

                    } else {

                        T dist2= ( *con->pointA->position-*con->pointB->position ).norm2();
                        if ( ! ( max_endpoint_dist2>dist2 ) ) {

                            if ( con->edge_type==EDGE_TYPES::EDGE_SEGMENT ) {
                                printf ( "edge is segment\n" );
                            }
                            if ( con->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
                                printf ( "edge is bifurcation edge\n" );
                            }

                            mhs::STAError error;
                            error<<"error checking edge dist:"<<dist2<<" but max"<<max_endpoint_dist2<<"\n";
                            throw error;
                        }
                    }
                }

            }
            return;
        }


// 	    printf("done\n");

        printf ( "checking consistency (terminals) .." );
        for ( std::size_t i=0; i<num_terminals; i++ ) {
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
            sta_assert_error (
                ( ( terminals[i]->endpoint_connections[0]==0 ) && ( terminals[i]->endpoint_connections[1]==1 ) )
                || ( ( terminals[i]->endpoint_connections[1]==0 ) && ( terminals[i]->endpoint_connections[0]==1 ) ) );
#else
            sta_assert_error (
                ( ( terminals[i]->endpoint_connections[0]==0 ) ) || ( ( terminals[i]->endpoint_connections[1]==0 ) ) );
#endif
            for ( std::size_t j=i+1; j<num_terminals; j++ ) {
                sta_assert_error ( terminals[i]!=terminals[j] );
            }
        }
        printf ( "done\n" );
    }
    
    
    bool check_consitsty2()
    {
      class Connection<T,Dim> * connection=this;
      
	class Points<T,Dim>::CEndpoint * endpoints[2];
        endpoints[0]=connection->points[0];
        endpoints[1]=connection->points[1];
      
	for (int pt=0;pt<2;pt++)
	{
	
	    Points<T,Dim> * point=endpoints[pt]->point;
            int side =endpoints[pt]->side;
            const int * num_conn_side=point->endpoint_connections;
            const int side_indx=endpoints[pt]->endpoint_side_indx;
	    bool found=false;
// 	    for ( int a=0; a<num_conn_side[side]; a++ ) {
// 		printf("edge checkB: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[side],(std::size_t)point->endpoints[side][a]->connected,(std::size_t)point->endpoints[side][a]->connection,point->endpoints[side][a]);
// 		sta_assert_error ( (point->endpoints[side][a]->connected!=NULL)||(!(a<num_conn_side[side])) );
// 	    }
	}
	
	return true;
      
    }

    void print() {

        printf ( "num edges    : %d\n",pool->get_numpts() );
        printf ( "num terminals: %d\n",num_terminals );
    }

    void register_terminal_node ( Points<T,Dim> * p ) {
        sta_assert_debug0 ( p->terminal_indx<=-1 );
        sta_assert_debug0 ( ( p->endpoint_connections[0]==0 ) || ( p->endpoint_connections[1]==0 ) );
        terminals[num_terminals]=p;
        p->terminal_indx=num_terminals;
        num_terminals++;
        sta_assert_debug0 ( num_terminals<pointlimit );
    }

    void unregister_terminal_node ( Points<T,Dim> * p ) {
        sta_assert_debug0 ( p->terminal_indx>-1 );
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
        sta_assert_debug0 ( p->get_num_connections() !=1 );
#else

        sta_assert_debug0 ( ( ( p->endpoint_connections[0]>0 ) && ( p->endpoint_connections[1]>0 ) )
                            || ( p->get_num_connections() ==0 )
                            || ( ( p->endpoint_connections[0]>1 ) || ( p->endpoint_connections[1]>1 ) ) );
#endif
        terminals[p->terminal_indx]=terminals[num_terminals-1];
        terminals[p->terminal_indx]->terminal_indx=p->terminal_indx;
        sta_assert_error ( p->terminal_indx>-1 );
        sta_assert_debug0 ( terminals[p->terminal_indx]->terminal_indx>-1 );
        p->terminal_indx=-2;
        sta_assert_debug0 ( num_terminals>0 );
        num_terminals--;
        sta_assert_debug0 ( num_terminals>=0 );
    }



    int get_num_bifurcations() {
        return num_bifurcation_points;
    }

    Points<T,Dim> & get_rand_bifurcation_point() {
        sta_assert_error ( num_bifurcation_points>0 );
        return * ( bifurcation_points[std::rand() %num_bifurcation_points] );
    }

    int get_num_bifurcation_centers() {
        return num_bifurcation_center_points;
    }

    Points<T,Dim> & get_rand_bifurcation_center_point() {
        sta_assert_error( num_bifurcation_center_points>0 );
        return * ( bifurcation_center_points[std::rand() %num_bifurcation_center_points] );
    }

    void register_bifurcation_node ( Points<T,Dim> * p ) {
        sta_assert_debug0 ( p->bifurcation_indx<=-1 );
// 	    sta_assert_debug0(((p->endpoints[0][0]->connected!=NULL)&&(p->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
// 	      ||((p->endpoints[1][0]->connected!=NULL)&&(p->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION)));
        sta_assert_debug0 ( p->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION );
        p->particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION;
        bifurcation_points[num_bifurcation_points]=p;
        p->bifurcation_indx=num_bifurcation_points;
        num_bifurcation_points++;
        sta_assert_debug0 ( num_bifurcation_points<pointlimit );
// printf("bifurcation point (+)\n");
    }

    void unregister_bifurcation_node ( Points<T,Dim> * p ) {
        sta_assert_debug0 ( p->bifurcation_indx>-1 );
        //sta_assert_debug0(p->get_num_connections()>2);
        sta_assert_debug0 ( p->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION );
        p->particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
        bifurcation_points[p->bifurcation_indx]=bifurcation_points[num_bifurcation_points-1];
        bifurcation_points[p->bifurcation_indx]->bifurcation_indx=p->bifurcation_indx;
        sta_assert_error ( p->bifurcation_indx>-1 );
        sta_assert_debug0 ( bifurcation_points[p->bifurcation_indx]->bifurcation_indx>-1 );
        p->bifurcation_indx=-2;
        sta_assert_debug0 ( num_bifurcation_points>0 );
        num_bifurcation_points--;
        sta_assert_debug0 ( num_bifurcation_points>=0 );
// 	    sta_assert_debug0(((p->endpoints[0][0]->connected!=NULL)&&(p->endpoints[0][0]->connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION))
// 	      ||((p->endpoints[1][0]->connected!=NULL)&&(p->endpoints[1][0]->connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION)));
        sta_assert_debug0 ( p->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION );
// printf("bifurcation point (-)\n");
    }

    void register_center_bifurcation_node ( Points<T,Dim> * p ) {
        sta_assert_debug0 ( p->bifurcation_center_indx<=-1 );
        const int slotid=Points<T,Dim>::bifurcation_center_slot;
        sta_assert_debug0 ( p->endpoint_connections[slotid]==3 );
        sta_assert_debug0 ( p->endpoint_connections[1-slotid]==0 );

        sta_assert_debug0 ( p->endpoints[slotid][0]->connection->edge_type== ( EDGE_TYPES::EDGE_BIFURCATION ) );
        sta_assert_debug0 ( p->endpoints[slotid][1]->connection->edge_type== ( EDGE_TYPES::EDGE_BIFURCATION ) );
        sta_assert_debug0 ( p->endpoints[slotid][2]->connection->edge_type== ( EDGE_TYPES::EDGE_BIFURCATION ) );
        sta_assert_debug0 ( p->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
        p->particle_type=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER;
        bifurcation_center_points[num_bifurcation_center_points]=p;
        p->bifurcation_center_indx=num_bifurcation_center_points;
        num_bifurcation_center_points++;
        sta_assert_debug0 ( num_bifurcation_center_points<pointlimit );
// printf("bifurcation point (+)\n");
    }

    void unregister_center_bifurcation_node ( Points<T,Dim> * p ) {
        sta_assert_debug0 ( p->bifurcation_center_indx>-1 );
        //sta_assert_debug0(p->get_num_connections()>2);
        sta_assert_debug0 ( p->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
        p->particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
        bifurcation_center_points[p->bifurcation_center_indx]=bifurcation_center_points[num_bifurcation_center_points-1];
        bifurcation_center_points[p->bifurcation_center_indx]->bifurcation_center_indx=p->bifurcation_center_indx;
        sta_assert_error ( p->bifurcation_center_indx>-1 );
        sta_assert_debug0 ( bifurcation_center_points[p->bifurcation_center_indx]->bifurcation_center_indx>-1 );
        p->bifurcation_center_indx=-2;
        sta_assert_debug0 ( num_bifurcation_center_points>0 );
        num_bifurcation_center_points--;
        sta_assert_debug0 ( num_bifurcation_center_points>=0 );
        sta_assert_debug0 ( p->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
// printf("bifurcation point (-)\n");
    }
/*
    static bool connection_existsOLD ( Connection<T,Dim>& connection ) {
        if ( Points<T,Dim>::maxconnections>1 ) {
            for ( int s=0; s<2; s++ ) {
                class Points<T,Dim>::CEndpoint * endpointsA=connection.pointA->point->endpoints_[s];
                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                    if ( endpointsA[i].connected!=NULL ) {
                        if ( connection.pointB->point==endpointsA[i].connected->point ) {
                            return true;
                        }
                    }
                }
            }
        }
        
        if ((connection.pointA->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)&&
	  (connection.pointB->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION))
	{
	    for ( int s=0; s<2; s++ ) {
                class Points<T,Dim>::CEndpoint * endpointsA=connection.pointA->point->endpoints_[s];
                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                    if ( endpointsA[i].connected!=NULL ) {
                        if ( connection.pointB->point==endpointsA[i].connected->point ) {
                            return true;
                        }
                    }
                }
            }
	}
        return false;
    }*/



    static bool connection_exists ( const class Points<T,Dim>  & pA,
				    const class Points<T,Dim>  & pB,
				    int depth=2)
    {
    sta_assert_debug0((&pA) != (&pB));
    sta_assert_debug0(((depth>0)&&(depth<4)));

    if ((pA.get_num_connections()<1)||
	  (pB.get_num_connections()<1))
    {
      return false;
    }

    {
      for ( int a=0; a<2; a++ ) {
	    class Points<T,Dim>::CEndpoint * const * endpointsA=pA.endpoints[a];
	    for ( int ca=0; ca<pA.endpoint_connections[a]; ca++ ) { 
	      sta_assert_debug0(endpointsA[ca]->connected!=NULL);
	    
	    const class Points<T,Dim> & pC=*(endpointsA[ca]->connected->point);
	    
	    if ( (&pB)==(&pC)) 
	    {
		      return true;
	    }
	    
	    if (depth>1)
	    {
	      for ( int c=0; c<2; c++ ) {
		class Points<T,Dim>::CEndpoint * const * endpointsC=pC.endpoints[c];
	      
		  for ( int cc=0; cc<pC.endpoint_connections[c]; cc++ ) { 
		      sta_assert_debug0(endpointsC[cc]->connected!=NULL);
		      const class Points<T,Dim> & pD=*(endpointsC[cc]->connected->point);
		      if ((&pB)==(&pD))
		      {
		      return true;
		      }
		      
// 		      if (depth>2)
// 		      {
// 			if (pD.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
// 			{
// 			  
// 			  
// 			}
// 		      }
		    }
	      }
	    }
	  }
      }
  }
  return false;
    }


    static bool connection_exists ( Connection<T,Dim>& connection ) {
	return  connection_exists ( *(connection.pointA->point),*(connection.pointB->point));
      
//        //direct connections
//         if ( Points<T,Dim>::maxconnections>1 ) {
//             for ( int s=0; s<2; s++ ) {
//                 class Points<T,Dim>::CEndpoint * endpointsA=connection.pointA->point->endpoints_[s];
//                 for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
//                     if ( endpointsA[i].connected!=NULL ) {
//                         if ( connection.pointB->point==endpointsA[i].connected->point ) {
//                             return true;
//                         }
//                     }
//                 }
//             }
//         }
//        
//        
//        //indirect connections over bifurcation
//         if ((connection.pointA->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)&&
// 	  (connection.pointB->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION))
// 	{
// 	    for ( int a=0; a<2; a++ ) {
//                 class Points<T,Dim>::CEndpoint ** endpointsA=connection.pointA->point->endpoints[a];
//                 for ( int b=0; b<2; b++ ) {
// 		  class Points<T,Dim>::CEndpoint ** endpointsB=connection.pointB->point->endpoints[b];
// 		  
// // 		  if ((endpointsA[0]->connected!=NULL)&&(endpointsA[0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
// // 		    &&(endpointsB[0]->connected!=NULL)&&(endpointsB[0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
// 		  //during the creation step there might exist less than 3 connections
// 		  if ((endpointsA[0]->connected!=NULL)&&(endpointsA[0]->connected->point->endpoint_connections[Points<T,Dim>::bifurcation_center_slot]>1)
// 		    &&(endpointsB[0]->connected!=NULL)&&(endpointsB[0]->connected->point->endpoint_connections[Points<T,Dim>::bifurcation_center_slot]>1)
// 		  )
// 		  {
// 		    if (endpointsA[0]->connected->point==endpointsB[0]->connected->point)
// 		    {
// 		     return true;
// 		    }
// 		  }
// 		}
//             }
// 	}
//         return false;
    }


    bool same_pt ( Connection<T,Dim> & a, Connection<T,Dim> * & b ) {
        if ( ( ( a.pointA->point!=b->pointA->point ) || ( a.pointB->point!=b->pointB->point ) ) &&
                ( ( a.pointA->point!=b->pointB->point ) || ( a.pointB->point!=b->pointA->point ) ) ) {
            return false;
        }
        return true;
    }

    bool connection_exists2 ( Connection<T,Dim> & connection ) {

        if ( Points<T,Dim>::maxconnections>1 ) {
            class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=connection.pointA->point->endpoints[connection.pointA->side];
            for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                if ( endpointsA[i].connected!=NULL ) {
                    if ( ( endpointsA[i].connected->point==connection.pointB->point ) && ( ( endpointsA[i].connected->side==connection.pointB->side ) ) ) {

                        return true;
                    }
                }
            }
        }

        return false;
    }

    Connection<T,Dim> * add_connection_old ( Connection<T,Dim> & connection )
    //,bool avoid_double_connections=true)
    {
        if ( ( connection.pointA->connected!=NULL )
                && ( connection.pointB->connected!=NULL )
                && ( connection.pointA->connected->point==connection.pointB->point )
                && ( connection.pointA->connected->side==connection.pointB->side ) ) {
            //added 26.03.2014 to see if this happens...(how can???)
            sta_assert_error ( false );
            return NULL;
        }

        sta_assert_debug0 ( ( connection.pointA->connected==NULL ) );
        sta_assert_debug0 ( ( connection.pointB->connected==NULL ) );
        sta_assert_debug0 ( ( connection.pointB!=connection.pointA ) );
        sta_assert_debug0 ( ( connection.pointB->point!=connection.pointA->point ) );

// 	    if ((avoid_double_connections) && (connection_exists(connection)))
// 		return NULL;

#ifdef _DEBUG_CHECKS_0_
        if ( connection_exists ( connection ) ) {
            printf ( "error adding connection: poitners: %u %u",connection.pointA->point,connection.pointB->point );
            printf ( ", side: %u %u\n",connection.pointA->side,connection.pointB->side );

            mhs::STAError error;
            error<<"connection exists!!";
            error<<static_cast<int> ( connection.pointA->point->particle_type ) <<":";
            error<<static_cast<int> ( connection.pointB->point->particle_type ) <<"\n";
            throw error;


        }
#else
        sta_assert_error ( !connection_exists ( connection ) );
#endif




        class Connection<T,Dim> * new_connection =pool->create_obj();

// sta_assert_error(new_connection->test==0);

        *new_connection=connection;

// new_connection->test=1;


        connection.pointA->connection=new_connection;
// 	    connection.pointA->is_source=true;
        connection.pointA->connected=connection.pointB;
        connection.pointA->point->endpoint_connections[connection.pointA->side]++;
        connection.pointB->connection=new_connection;
// 	    connection.pointB->is_source=false;
        connection.pointB->connected=connection.pointA;
        connection.pointB->point->endpoint_connections[connection.pointB->side]++;



        for ( int pt=0; pt<2; pt++ ) {
            const int * conn_side=connection.points[pt]->point->endpoint_connections;

            //already registered?
            bool is_terminal=connection.points[pt]->point->terminal_indx>=0;
            bool is_bifurcation=connection.points[pt]->point->bifurcation_indx>=0;
            bool is_bifurcation_center=connection.points[pt]->point->bifurcation_center_indx>=0;

            //supposd to be registered?
            //bool is_terminal_candidate=(conn_side[0]==0)!=(conn_side[1]==0);
            bool is_terminal_candidate= ( conn_side[0]+conn_side[1]==1 );
            bool is_bifurcation_candidate=
                ( ( conn_side[0]==1 ) && ( connection.points[pt]->point->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) )
                || ( ( conn_side[1]==1 ) && ( connection.points[pt]->point->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) );

            bool is_bifurcation_center_candidate= ( conn_side[0]==3 ) != ( conn_side[1]==3 );

            if ( ( is_terminal_candidate ) && ( !is_terminal ) ) {
                register_terminal_node ( connection.points[pt]->point );
            }
            if ( ( !is_terminal_candidate ) && ( is_terminal ) ) {
                unregister_terminal_node ( connection.points[pt]->point );
            }
            if ( ( is_bifurcation_candidate ) && ( !is_bifurcation ) ) {
                register_bifurcation_node ( connection.points[pt]->point );
            }
            if ( ( !is_bifurcation_candidate ) && ( is_bifurcation ) ) {
                unregister_bifurcation_node ( connection.points[pt]->point );
            }
            if ( ( is_bifurcation_center_candidate ) && ( !is_bifurcation_center ) ) {
                register_center_bifurcation_node ( connection.points[pt]->point );
            }
            if ( ( !is_bifurcation_center_candidate ) && ( is_bifurcation_center ) ) {
                unregister_center_bifurcation_node ( connection.points[pt]->point );
            }

        }




        return new_connection;
    }





    Connection<T,Dim> * add_connection ( Connection<T,Dim> & connection )
    //,bool avoid_double_connections=true)
    {

// printf("0");
// 	    TODO: always check for first free hub here again (might have changed)


        if ( ( connection.pointA->connected!=NULL )
                && ( connection.pointB->connected!=NULL )
                && ( connection.pointA->connection!=NULL )
                && ( connection.pointB->connection!=NULL )
                && ( connection.pointA->connected->point==connection.pointB->point )
                && ( connection.pointA->connected->side==connection.pointB->side ) ) {
            //added 26.03.2014 to see if this happens...(how can???)
            sta_assert_error ( false );
            return NULL;
        }

        //might be still connected if edge is a backup
// 	    sta_assert_debug0((connection.pointA->connected==NULL));
// 	    sta_assert_debug0((connection.pointB->connected==NULL));
// 	    sta_assert_debug0((connection.pointA->connection==NULL));
// 	    sta_assert_debug0((connection.pointB->connection==NULL));
        sta_assert_debug0 ( ( connection.pointB!=connection.pointA ) );
        sta_assert_debug0 ( ( connection.pointB->point!=connection.pointA->point ) );

        connection.pointA=connection.pointA->point->getfreehub ( connection.pointA->side );
        connection.pointB=connection.pointB->point->getfreehub ( connection.pointB->side );

// 	    if ((avoid_double_connections) && (connection_exists(connection)))
// 		return NULL;
// printf("1");

#ifdef _DEBUG_CHECKS_0_
        if ( connection_exists ( connection ) ) {
            printf ( "error adding connection: poitners: %u %u",connection.pointA->point,connection.pointB->point );
            printf ( ", side: %u %u\n",connection.pointA->side,connection.pointB->side );

            mhs::STAError error;
            error<<"connection exists!!";
            error<<static_cast<int> ( connection.pointA->point->particle_type ) <<":";
            error<<static_cast<int> ( connection.pointB->point->particle_type ) <<"\n";
            throw error;


        }
#else
        sta_assert_error ( !connection_exists ( connection ) );
#endif

        /*printf("2");	*/


        class Connection<T,Dim> * new_connection =pool->create_obj();

// sta_assert_error(new_connection->test==0);

        /*printf("3");	  */

        *new_connection=connection;

        new_connection->set_stamp ( time_stamp );


// new_connection->test=1;

// printf("4");

        sta_assert_error ( connection.pointB->point->check_consistency2() );
        sta_assert_error ( connection.pointA->point->check_consistency2() );

        connection.pointA->connection=new_connection;
// 	    connection.pointA->is_source=true;
        connection.pointA->connected=connection.pointB;
        connection.pointA->point->endpoint_connections[connection.pointA->side]++;
        connection.pointB->connection=new_connection;
// 	    connection.pointB->is_source=false;
        connection.pointB->connected=connection.pointA;
        connection.pointB->point->endpoint_connections[connection.pointB->side]++;




        /*printf("5");*/
        for ( int pt=0; pt<2; pt++ ) {
// 	      printf(".");
            const int * conn_side=connection.points[pt]->point->endpoint_connections;

            //already registered?
            bool is_terminal=connection.points[pt]->point->terminal_indx>=0;
            bool is_bifurcation=connection.points[pt]->point->bifurcation_indx>=0;
            bool is_bifurcation_center=connection.points[pt]->point->bifurcation_center_indx>=0;

// 	      printf(".");
            //supposd to be registered?
            //bool is_terminal_candidate=(conn_side[0]==0)!=(conn_side[1]==0);
            bool is_terminal_candidate= ( conn_side[0]+conn_side[1]==1 );

// 	      printf("[%d,%d]\n",conn_side[0],conn_side[1]);

// 	      for (int s=0;s<2;s++)
// 	      {
// 		for (int c=0;c<Points<T,Dim>::maxconnections;c++)
// 		{
// 		  if ((c<=conn_side[s])&&(connection.points[pt]->point->endpoints[s][c]->connection==NULL))
// 		  {
//
// 		  }
//
// 		}
// 	      }
            sta_assert_error ( connection.points[pt]->point->check_consistency2() );

            for ( int s=0; s<2; s++ ) {
                for ( int c=0; c<conn_side[s]; c++ ) {
                    sta_assert_error ( ( connection.points[pt]->point->endpoints[s][c]->connection!=NULL ) );
                }
            }
            /*
               printf(".");*/

            bool is_bifurcation_candidate=
                ( ( conn_side[0]==1 ) && ( connection.points[pt]->point->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) )
                || ( ( conn_side[1]==1 ) && ( connection.points[pt]->point->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) );

            bool is_bifurcation_center_candidate= ( conn_side[0]==3 ) != ( conn_side[1]==3 );

// 	      printf(".");

            if ( ( is_terminal_candidate ) && ( !is_terminal ) ) {
                register_terminal_node ( connection.points[pt]->point );
            }
            if ( ( !is_terminal_candidate ) && ( is_terminal ) ) {
                unregister_terminal_node ( connection.points[pt]->point );
            }
            if ( ( is_bifurcation_candidate ) && ( !is_bifurcation ) ) {
                register_bifurcation_node ( connection.points[pt]->point );
            }
            if ( ( !is_bifurcation_candidate ) && ( is_bifurcation ) ) {
                unregister_bifurcation_node ( connection.points[pt]->point );
            }
            if ( ( is_bifurcation_center_candidate ) && ( !is_bifurcation_center ) ) {
                register_center_bifurcation_node ( connection.points[pt]->point );
            }
            if ( ( !is_bifurcation_center_candidate ) && ( is_bifurcation_center ) ) {
                unregister_center_bifurcation_node ( connection.points[pt]->point );
            }

// 	      printf(";");
        }

// printf("6");

        sta_assert_error ( connection.pointB->point->check_consistency2() );
        sta_assert_error ( connection.pointA->point->check_consistency2() );

        return new_connection;
    }


    void remove_connection_old ( class Connection<T,Dim> * & connection ) {



        sta_assert_error ( ( connection!=NULL ) );
        sta_assert_error ( ( connection->pointA->connected!=NULL ) );
        sta_assert_error ( ( connection->pointB->connected!=NULL ) );



        connection->pointA->point->endpoint_connections[connection->pointA->side]--;
        connection->pointB->point->endpoint_connections[connection->pointB->side]--;

        connection->pointA->connected=NULL;
        connection->pointB->connected=NULL;




        class Points<T,Dim>::CEndpoint * endpoints[2];
        for ( int pt=0; pt<2; pt++ ) {
            endpoints[pt]=connection->points[pt];
            const int * conn_side=connection->points[pt]->point->endpoint_connections;

            //already registered?
            bool is_terminal=connection->points[pt]->point->terminal_indx>=0;
            bool is_bifurcation=connection->points[pt]->point->bifurcation_indx>=0;
            bool is_bifurcation_center=connection->points[pt]->point->bifurcation_center_indx>=0;

            //supposd to be registered?
            //bool is_terminal_candidate=((conn_side[0]==0)!=(conn_side[1]==0));
            bool is_terminal_candidate= ( conn_side[0]+conn_side[1]==1 );
            bool is_bifurcation_candidate=
                ( ( conn_side[0]==1 ) && ( connection->points[pt]->point->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) )
                || ( ( conn_side[1]==1 ) && ( connection->points[pt]->point->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) );

            bool is_bifurcation_center_candidate= ( conn_side[0]==3 ) != ( conn_side[1]==3 );

            sta_assert_debug0 ( ! ( ( conn_side[0]>1 ) && ( conn_side[1]>1 ) ) );


            if ( ( is_terminal_candidate ) && ( !is_terminal ) ) {
                register_terminal_node ( connection->points[pt]->point );
            }
            if ( ( !is_terminal_candidate ) && ( is_terminal ) ) {
                unregister_terminal_node ( connection->points[pt]->point );
            }
            if ( ( is_bifurcation_candidate ) && ( !is_bifurcation ) ) {
                register_bifurcation_node ( connection->points[pt]->point );
            }
            if ( ( !is_bifurcation_candidate ) && ( is_bifurcation ) ) {
                unregister_bifurcation_node ( connection->points[pt]->point );
            }
            if ( ( is_bifurcation_center_candidate ) && ( !is_bifurcation_center ) ) {
                register_center_bifurcation_node ( connection->points[pt]->point );
            }
            if ( ( !is_bifurcation_center_candidate ) && ( is_bifurcation_center ) ) {
                unregister_center_bifurcation_node ( connection->points[pt]->point );
            }
        }


        connection->pointA->connection=NULL;
        connection->pointB->connection=NULL;



// 	    sta_assert_error(connection->test==1);
// 	    connection->test=0;

        pool->delete_obj ( connection );

// printf("*");
        for ( int pt=0; pt<2; pt++ ) {

            //ensure that if only one connection stays connected, it is
            //the first slot of this point!!
            Points<T,Dim> * point=endpoints[pt]->point;
            int side =endpoints[pt]->side;
            const int * num_conn_side=point->endpoint_connections;
            const int side_indx=endpoints[pt]->endpoint_side_indx;

            if ( side_indx<num_conn_side[side] ) {
                std::swap ( point->endpoints[side][side_indx],point->endpoints[side][num_conn_side[side]] );
                std::swap ( point->endpoints[side][side_indx]->endpoint_side_indx,point->endpoints[side][num_conn_side[side]]->endpoint_side_indx );
// 		  printf("(%d)\n",i);
#ifdef _DEBUG_CHECKS_0_
                for ( int a=0; a<num_conn_side[side]; a++ ) {
                    sta_assert_error ( point->endpoints[side][a]->connected!=NULL );
                }
#endif

            }


        }

        // we make the connection unusable!
// 	    connection=NULL;
    }



    // connection will be set to NULL !!
    void remove_connection ( class Connection<T,Dim> * & connection,bool debug=false ) {

        Connection<T,Dim> * connection_deleteme=connection;

        if ( debug ) {
            printf ( "remove_connection\n" );
        }

        sta_assert_error ( ( connection!=NULL ) );

        if ( debug ) {
            printf ( "edge @ %u\n",(std::size_t)connection );
        }
        sta_assert_error ( ( connection_deleteme->pointA!=NULL ) );
        sta_assert_error ( ( connection_deleteme->pointB!=NULL ) );

//         if ( debug ) {
//             printf ( "A" );
//         }
	
	
        class Points<T,Dim>::CEndpoint * endpoints[2];
        endpoints[0]=connection_deleteme->points[0];
        endpoints[1]=connection_deleteme->points[1];

        endpoints[0]->point->check_consistency2(debug);
        endpoints[1]->point->check_consistency2(debug);


        sta_assert_error ( ( connection_deleteme->pointA->connected!=NULL ) );
        sta_assert_error ( ( connection_deleteme->pointB->connected!=NULL ) );

        if ( debug ) {

	  printf ( "enptA: %u %u %u\n",(std::size_t)endpoints[0],(std::size_t)endpoints[0]->connection, (std::size_t)endpoints[0]->connected);
	  printf ( "enptB: %u %u %u\n",(std::size_t)endpoints[1],(std::size_t)endpoints[1]->connection, (std::size_t)endpoints[1]->connected );
        }

        sta_assert_error ( ( connection_deleteme->pointA->connection==connection_deleteme ) );
        sta_assert_error ( ( connection_deleteme->pointB->connection==connection_deleteme ) );

//         if ( debug ) {
//             printf ( "C" );
//         }

        connection_deleteme->pointA->point->endpoint_connections[connection_deleteme->pointA->side]--;
        connection_deleteme->pointB->point->endpoint_connections[connection_deleteme->pointB->side]--;

//         if ( debug ) {
//             printf ( "D" );
//         }
	
	if ( debug ) {
	 printf("01: %u\n",connection_deleteme->pointA->connected); 
	 printf("01: %u\n",connection_deleteme->pointB->connected); 
	 printf("01: %u\n",endpoints[0]); 
	 printf("01: %u\n",endpoints[1]); 	 
	 
	 for ( int pt=0; pt<2; pt++ ) {

            Points<T,Dim> * point=endpoints[pt]->point;
            int side =endpoints[pt]->side;
            const int * num_conn_side=point->endpoint_connections;
            const int side_indx=endpoints[pt]->endpoint_side_indx;
	  
		{
		printf("--------------------------------\n");  
                //for ( int a=0; a<num_conn_side[side]; a++ ) {
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkA0: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[side],(std::size_t)point->endpoints[side][a]->connected,(std::size_t)point->endpoints[side][a]->connection,(std::size_t)point->endpoints[side][a]);
                }
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkA1: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[1-side],(std::size_t)point->endpoints[1-side][a]->connected,(std::size_t)point->endpoints[1-side][a]->connection,(std::size_t)point->endpoints[1-side][a]);
                }

		  printf("--------------------------------\n");
		}
        }

	 
	}
	
        connection_deleteme->pointA->connected=NULL;
        connection_deleteme->pointB->connected=NULL;
	
	if ( debug ) {
	 printf("02: %u\n",connection_deleteme->pointA->connected); 
	 printf("02: %u\n",connection_deleteme->pointB->connected); 
	 printf("02: %u\n",endpoints[0]); 
	 printf("02: %u\n",endpoints[1]); 	 
	}

//         if ( debug ) {
//             printf ( "E\n" );
//         }



        connection_deleteme->pointA->connection=NULL;
//         if ( debug ) {
//             printf ( "E\n" );
//         }
        connection_deleteme->pointB->connection=NULL;
	
	
// 	if ( debug ) {
// 	 printf("03: %u\n",connection_deleteme->pointA->connected); 
// 	 printf("03: %u\n",connection_deleteme->pointB->connected); 
// 	 printf("03: %u\n",endpoints[0]); 
// 	 printf("03: %u\n",endpoints[1]); 	 
// 	}
	
//         if ( debug ) {
//             printf ( "E\n" );
//         }

//         if ( debug ) {
//             printf ( "E\n" );
//         }
// 
//         if ( debug ) {
//             printf ( "1\n" );
//         }
	


        if ( connection==NULL ) {
            throw ( mhs::STAError ( "don't use the edgepointer of a partcle to delete an edge (error remove_connection)" ) );
        }
        pool->delete_obj ( connection );

//         if ( debug ) {
//             printf ( "2\n" );
//         }
	
	if ( debug ) {
	 printf("04: %u\n",endpoints[0]); 
	 printf("04: %u\n",endpoints[1]); 
	 
	 for ( int pt=0; pt<2; pt++ ) {

            Points<T,Dim> * point=endpoints[pt]->point;
	    printf("0"); 
            int side =endpoints[pt]->side;
	    printf("1"); 
            const int * num_conn_side=point->endpoint_connections;
	    printf("2"); 
            const int side_indx=endpoints[pt]->endpoint_side_indx;
	  printf("3"); 
		{
		printf("--------------------------------\n");  
                //for ( int a=0; a<num_conn_side[side]; a++ ) {
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkA0: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[side],(std::size_t)point->endpoints[side][a]->connected,(std::size_t)point->endpoints[side][a]->connection,(std::size_t)point->endpoints[side][a]);
                }
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkA1: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[1-side],(std::size_t)point->endpoints[1-side][a]->connected,(std::size_t)point->endpoints[1-side][a]->connection,(std::size_t)point->endpoints[1-side][a]);
                }

		  printf("--------------------------------\n");
		}
        }

	 
	}

        endpoints[0]->point->check_consistency2(debug);
        endpoints[1]->point->check_consistency2(debug);
	
	  
	    if (debug)
		{
		printf("!!------------------------------\n");  
		}
//         if ( debug ) {
//             printf ( "3\n" );
//         }

        for ( int pt=0; pt<2; pt++ ) {

            //ensure that if only one connection stays connected, it is
            //the first slot of this point!!
            Points<T,Dim> * point=endpoints[pt]->point;
            int side =endpoints[pt]->side;
            const int * num_conn_side=point->endpoint_connections;
            const int side_indx=endpoints[pt]->endpoint_side_indx;
	    
	    if (debug)
		{
		printf("--------------------------------\n");  
                //for ( int a=0; a<num_conn_side[side]; a++ ) {
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkA0: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[side],(std::size_t)point->endpoints[side][a]->connected,(std::size_t)point->endpoints[side][a]->connection,(std::size_t)point->endpoints[side][a]);
                }
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkA1: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[1-side],(std::size_t)point->endpoints[1-side][a]->connected,(std::size_t)point->endpoints[1-side][a]->connection,(std::size_t)point->endpoints[1-side][a]);
                }

                printf("--------------------------------\n");
		}

            if ( side_indx<num_conn_side[side] ) {
                std::swap ( point->endpoints[side][side_indx],point->endpoints[side][num_conn_side[side]] );
                std::swap ( point->endpoints[side][side_indx]->endpoint_side_indx,point->endpoints[side][num_conn_side[side]]->endpoint_side_indx );
// 		  printf("(%d)\n",i);
//#ifdef _DEBUG_CHECKS_0_
// 		if (debug)
// 		{
//                 for ( int a=0; a<num_conn_side[side]; a++ ) {
// 		    printf("edge check: %d %u %u\n", a,(std::size_t)point->endpoints[side][a]->connected,(std::size_t)point->endpoints[side][a]->connection);
//                     sta_assert_error ( point->endpoints[side][a]->connected!=NULL );
//                 }
// 		}
//#endif
            }
            if (debug)
		{
		  
                //for ( int a=0; a<num_conn_side[side]; a++ ) {
                for ( int a=0; a<Points<T,Dim>::maxconnections; a++ ) {
		    printf("edge checkB: %d %d %d: %u %u | %u\n",pt, a,a<num_conn_side[side],(std::size_t)point->endpoints[side][a]->connected,(std::size_t)point->endpoints[side][a]->connection,(std::size_t)point->endpoints[side][a]);
                    sta_assert_error ( (point->endpoints[side][a]->connected!=NULL)||(!(a<num_conn_side[side])) );
                }
		}
        }
//         if ( debug ) {
//             printf ( "4" );
//         }

        endpoints[0]->point->check_consistency2();
        endpoints[1]->point->check_consistency2();

// 	    printf("[");

//         if ( debug ) {
//             printf ( "5" );
//         }
// 	    class Points<T,Dim>::CEndpoint * endpoints[2];
        for ( int pt=0; pt<2; pt++ ) {
// 	      endpoints[pt]=connection->points[pt];
            const int * conn_side=endpoints[pt]->point->endpoint_connections;

            //already registered?
            bool is_terminal=endpoints[pt]->point->terminal_indx>=0;
            bool is_bifurcation=endpoints[pt]->point->bifurcation_indx>=0;
            bool is_bifurcation_center=endpoints[pt]->point->bifurcation_center_indx>=0;

            //supposd to be registered?
            //bool is_terminal_candidate=((conn_side[0]==0)!=(conn_side[1]==0));
//             if ( debug ) {
//                 printf ( "[%d]",pt );
//             }
            bool is_terminal_candidate= ( conn_side[0]+conn_side[1]==1 );
            bool is_bifurcation_candidate=
                ( ( conn_side[0]==1 ) && ( endpoints[pt]->point->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) )
                || ( ( conn_side[1]==1 ) && ( endpoints[pt]->point->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) );

//             if ( debug ) {
//                 printf ( "[%d]",pt );
//             }
            bool is_bifurcation_center_candidate= ( conn_side[0]==3 ) != ( conn_side[1]==3 );

            sta_assert_debug0 ( ! ( ( conn_side[0]>1 ) && ( conn_side[1]>1 ) ) );


            if ( ( is_terminal_candidate ) && ( !is_terminal ) ) {
                register_terminal_node ( endpoints[pt]->point );
            }
            if ( ( !is_terminal_candidate ) && ( is_terminal ) ) {
                unregister_terminal_node ( endpoints[pt]->point );
            }
            if ( ( is_bifurcation_candidate ) && ( !is_bifurcation ) ) {
                register_bifurcation_node ( endpoints[pt]->point );
            }
            if ( ( !is_bifurcation_candidate ) && ( is_bifurcation ) ) {
                unregister_bifurcation_node ( endpoints[pt]->point );
            }
            if ( ( is_bifurcation_center_candidate ) && ( !is_bifurcation_center ) ) {
                register_center_bifurcation_node ( endpoints[pt]->point );
            }
            if ( ( !is_bifurcation_center_candidate ) && ( is_bifurcation_center ) ) {
                unregister_center_bifurcation_node ( endpoints[pt]->point );
            }
        }

//         if ( debug ) {
//             printf ( "6" );
//         }
        endpoints[0]->point->check_consistency2();
        endpoints[1]->point->check_consistency2();

//         if ( debug ) {
//             printf ( "7" );
//         }

// 	    printf("]");




// 	    sta_assert_error(connection->test==1);
// 	    connection->test=0;



// printf("*");


        // we make the connection unusable!
// 	    connection=NULL;
    }

};

template<typename T,int Dim>
const  T Connection<T,Dim>::no_connection_cost=0;

#endif





