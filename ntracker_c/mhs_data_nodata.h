#ifndef MHS_DATA_NODATA_H
#define MHS_DATA_NODATA_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataNodata: public CData<T,TData,Dim>
{
private:

    const TData * scales;
    std::size_t num_scales;
    
    


    
    T min_scale;
    T max_scale;
    T scale_correction;
    
 
    T DataDistScale;
    T DataThickScale;

public:

    float get_minscale() {
        return min_scale;
    };

    float get_maxscale() {
        return max_scale;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataNodata() {
    }

    ~CDataNodata() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
     
    }
    void set_controls ( const mxArray * handle ) {
      
    }
#endif

    void init ( const mxArray * feature_struct ) {

        
       

        try {

        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

        std::swap ( this->shape[0],this->shape[2] );

//         for ( int i=0; i<3; i++ ) {
//             element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
//         }


        scale_correction=1;
        try {
            scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
        } catch ( mhs::STAError error ) {
            throw error;
        }


        try {
            const TData * scales=mhs::dataArray<TData> ( feature_struct,"override_scales" ).data;
            min_scale=scales[0]*scale_correction;
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"override_scales" ).dim[0]-1]*scale_correction;
        } catch ( mhs::STAError error ) {
            min_scale=scales[0];
            max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
        }
        
	}
        catch ( mhs::STAError error ) {
         throw error;
        }

    }

    void set_params ( const mxArray * params=NULL ) {

	DataDistScale=0.1;
	DataThickScale=10;
	  if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"DataDistScale" ) !=-1 ) {
                    DataDistScale=mhs::mex_getParam<T> ( params,"DataDistScale",1 ) [0];
                }
                   if ( mhs::mex_hasParam ( params,"DataThickScale" ) !=-1 ) {
                    DataThickScale=mhs::mex_getParam<T> ( params,"DataThickScale",1 ) [0];
                }
                 } catch ( mhs::STAError & error ) {
                throw error;
		}
	    }}

private:



private:



public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
      //,const Points<T,Dim> *org_point=NULL
	
    ) {
	//sta_assert_error(false);
      
        
	if (p_point!=NULL)
	{
	   const class Vector<T,Dim> & ref_pos=(p_point->ref_position);
	   
	   //result=radius*((ref_pos-position_org)/radius).norm2();
	   T tmp1=((ref_pos-position_org)).norm2()/radius;
	   T tmp=((radius-p_point->ref_scale));
	   result=DataThickScale*tmp*tmp+DataDistScale*tmp1;
	   
	   
#ifdef D_USE_GUI
            if ( debug!=NULL ) {
                std::stringstream s;
                s.precision ( 3 );
                s<<"------------------";
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<" displacement costs : "<<tmp1;
                debug->push_back ( s.str() );
		
		s.str ( "" );
                s<<" scale costs : "<<tmp*tmp;
                debug->push_back ( s.str() );
		
		s.str ( "" );
                s<<" DataThickScale : "<<DataThickScale<<" DataDistScale : "<<DataDistScale;
                debug->push_back ( s.str() );
            }
#endif
	   
// 	   result=0;
	   
	}else
	{
	    printf("?");
	    sta_assert_error(false);
	    return false;
	}
	  
	  
        return true;
    }
    
    
};

#endif
