#ifndef MHS_DATA_SD_SINGLE_H
#define MHS_DATA_SD_SINGLE_H


#include "mhs_data.h"
#include "mhs_graphics.h"

template <typename T,typename TData,int Dim>
class CDataSDSHsingle: public CData<T,TData,Dim>
{
private:
    int scale_power;

//     const TData * local_stdv;
//     int num_alphas_local_stdv;
    

   
    
       

    const TData * sd_data;
    int order_sd_data;
    int components_sd_data;
    int component_tensor;
    
    
    Vector<T,Dim> boundary;
    bool scale_dependend_boundary;

    T fixed_scale;

    
    T DataGrad;
    

    T DataThreshold;
    T DataScale;
    T StdvEpsilon;
    T DataMax;
    T DataMin;
    Vector<T,3> element_size;
    Vector<std::size_t ,3> feature_shape;
    T DataGamma;
    

public:

    float get_minscale() {
        return fixed_scale;
    };

    float get_maxscale() {
        return fixed_scale;//+0.00001;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataSDSHsingle() {
    }

    ~CDataSDSHsingle() {
    }

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
//         if ( handle!=NULL ) {
//             const mxArray *
//             parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
//             if ( parameter!=NULL ) {
//                 DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
//                 DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
// //                 DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
// //                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
//             }
//             printf ( "Th: %f,  Da %f\n",DataThreshold,DataScale);
//         }
    }
    void set_controls ( const mxArray * handle ) {
//         if ( handle!=NULL ) {
//             const mxArray *
//             parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
//             if ( parameter!=NULL ) {
//                 * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
// //                 * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
// //                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
//             }
//         }
//         printf ( "Th: %f,  Da %f\n",DataThreshold,DataScale);
//        printf ( "Th: %f,  Da %f, DaGV %f, GR %f \n",DataThreshold,DataScale,DataScaleGradVessel,DataScaleGradSurface );
    }
#endif

    void init ( const mxArray * feature_struct ) {

        mhs::dataArray<TData>  tmp0;
        mhs::dataArray<TData>  tmp1;
        mhs::dataArray<TData>  tmp2;
        mhs::dataArray<TData>  tmp3;
	mhs::dataArray<TData>  tmp4;
	mhs::dataArray<TData>  tmp5;
	mhs::dataArray<TData>  tmp6;
	
        try {
            tmp3=mhs::dataArray<TData> ( feature_struct,"shape_boundary" );
            sta_assert_error ( tmp3.get_num_elements() ==3 );
            boundary[0]=tmp3.data[0];
            boundary[1]=tmp3.data[1];
            boundary[2]=tmp3.data[2];
            scale_dependend_boundary=true;
        } catch ( mhs::STAError error ) {
            boundary[0]=boundary[1]=boundary[2]=T ( 0 );
            scale_dependend_boundary=false;
        }
        
       

        try {


            tmp1=mhs::dataArray<TData > ( feature_struct,"sd_data" );
            sd_data=tmp1.data;
	    components_sd_data=tmp1.dim[tmp1.dim.size()-1];
	    //order_sd_data=std::sqrt(4.0*components_sd_data/2)-2;
	    
	    
	    tmp5=mhs::dataArray<TData > ( feature_struct,"L" );
	    order_sd_data=*tmp5.data;
	    component_tensor=((order_sd_data+2)*(order_sd_data+2))/4;
	    
	    printf ( "sd order: %d (components %d)\n",order_sd_data,components_sd_data);

            for ( int i=0; i<3; i++ ) {
                feature_shape[i]=tmp1.dim[i];
            }
        } catch ( mhs::STAError error ) {
            throw error;
        }


        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }

        std::swap ( this->shape[0],this->shape[2] );

        for ( int i=0; i<3; i++ ) {
            element_size[i]= ( T ) this->shape[i]/ ( T ) feature_shape[i];
        }

        printf ( "element size data term:" );
        element_size.print();
	const TData * scales;
	
	std::size_t num_scales;   
	try {
            scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();
        } catch ( mhs::STAError error ) {
            throw error;
        }
        
        //sta_assert_error(num_scales==1);
        sta_assert_error(num_scales>0);

	fixed_scale=scales[0];

	

    }

    void set_params ( const mxArray * params=NULL ) {
        scale_power=2;
        DataScale=1;
        DataThreshold=0;
        StdvEpsilon=0.001;
	DataGrad=-1;
	DataMax=-1;
	DataMin=1;
	DataGamma=1;

        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataGrad" ) !=-1 ) {
                    DataGrad=mhs::mex_getParam<T> ( params,"DataGrad",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"Epsilon" ) !=-1 ) {
                    StdvEpsilon=mhs::mex_getParam<T> ( params,"Epsilon",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataMax" ) !=-1 ) {
                    DataMax=mhs::mex_getParam<T> ( params,"DataMax",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"DataMin" ) !=-1 ) {
                    DataMin=mhs::mex_getParam<T> ( params,"DataMin",1 ) [0];
                }
                if ( mhs::mex_hasParam ( params,"DataGamma" ) !=-1 ) {
                    DataGamma=mhs::mex_getParam<T> ( params,"DataGamma",1 ) [0];
                }
                
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:



public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
      //,const Points<T,Dim> *org_point=NULL
	
    ) {


        try {

	  
// 	  T bla=mhs_fast_math<T>::plm(1,2,0);
// 	  printf("%f\n",bla);
	  
            Vector<T,Dim> position=position_org;
	    
// 	    position=Vector<T,Dim>(63,92,55);
// 	   position=Vector<T,Dim>(55,63,92);
// 	    
            position/=this->element_size;


            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->feature_shape[0] ) || ( Y+1>=this->feature_shape[1] ) || ( X+1>=this->feature_shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }
            


            T org_scale=radius;

            if ( ( radius<fixed_scale-0.01 )
                    || ( radius>fixed_scale+0.01 ) ) {
                printf ( "[%f %f %f]\n",fixed_scale,radius,fixed_scale);
                sta_assert_error ( ! ( radius<fixed_scale) );
                sta_assert_error ( ! ( radius>fixed_scale) );
            }


            

            const T boundary_scale=2;
            if ( ( scale_dependend_boundary ) &&
                    ( ( position[0]+boundary_scale*org_scale>=this->feature_shape[0] ) ||
                      ( position[1]+boundary_scale*org_scale>=this->feature_shape[1] ) ||
                      ( position[2]+boundary_scale*org_scale>=this->feature_shape[2] ) ||
                      ( position[0]-boundary_scale*org_scale<0 ) ||
                      ( position[1]-boundary_scale*org_scale<0 ) ||
                      ( position[2]-boundary_scale*org_scale<0 ) ) ) {
                return false;
            }

         
	    
	    
	    T sd_filter=0;
	    
T grad=0;

if (true)
{	 
    T tensor_real[2*component_tensor]; 
    if (!sd_filter_response(tensor_real, position,direction,  sd_filter))
      return false;
    
    if (DataGrad>0)
    {
      Vector<T,3> pos=position+direction;
      T tmp;
      if (!sd_filter_response(tensor_real, pos,direction, grad))
	return false;
      
		  pos=position-direction;
      if (!sd_filter_response(tensor_real, pos,direction, tmp))
	return false;
      
      grad=std::abs(grad-tmp)/2;

    }
    
}


  

            T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=org_scale;
                break;
            case 2:
                scale1=org_scale*org_scale;
                break;
            case 3:
                scale1=org_scale*org_scale*org_scale;
                break;
            }
            
            
            T sd_filt=sd_filter;
	    
	    
	     if (DataGamma<1)
	    {
	      sd_filt=((sd_filt<0) ? -1 : 1)*std::pow(std::abs(sd_filt),DataGamma);
	    }

	   
#ifdef D_USE_GUI
            if ( debug!=NULL ) {
                std::stringstream s;
                s.precision ( 3 );
                s<<"------------------";
                debug->push_back ( s.str() );

                s.str ( "" );
                s<<" SD : "<<sd_filter << " / SDgamma : "<<sd_filt;
                debug->push_back ( s.str() );


		
		
		s.str ( "" );
                s<<"grad : "<<grad;
                debug->push_back ( s.str() );
		

		
            }
#endif

	   

	    if (DataGrad>0)
	    {
	       result=-(DataScale*sd_filt-DataGrad*grad+DataThreshold)*scale1;
	    }else
	    {
	      result=-(DataScale*sd_filt+DataThreshold)*scale1;
	    }

        } catch ( mhs::STAError error ) {
            throw error;
        }

        return true;
    }
    
    
    bool sd_filter_response(T * tensor_buffer, const Vector<T,Dim> & position,const Vector<T,Dim> & direction,  T & sd_filter)
    {
	    
	    {
	      T * tensor_p=tensor_buffer; 
	      std::size_t count=0;
	      std::size_t count_data=0;
	      for (int l=0;l<=order_sd_data;l+=2)
	      {
		for (int m=-l;m<=0;m++)
		{
		  sta_assert_debug0(count<2*component_tensor);
		  
		  if ( !data_interpolate<T,T,TData> ( position,tensor_p[count],sd_data, feature_shape.v,components_sd_data,count_data++) ) {
			return false;
		    }
		  
		  count++;
		  
		  sta_assert_debug0(count<2*component_tensor);
		  if (m!=0)
		  {
		    
		    if ( !data_interpolate<T,T,TData> ( position,tensor_p[count],sd_data, feature_shape.v,components_sd_data,count_data++ ) ) {
			return false;
		    }
		    
		    count++;
		  }
		}
	      }
	    }
	    /*
 bool data_interpolate (
    const Vector<T,3> & pos,
    D & value,
    const TData  *  img,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode=1)
 bool data_interpolate_polynom (
    const Vector<T,3> & pos,
    const T & multi_scale,
    const TData  *  scales, // if NULL then global interpolation is used
    const TData  *  alphas,
    int num_alphas,
    D & value,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode =1)*/
	    
	    
	     {
	      const T & x=direction[2];
	      const T & y=direction[1];
	      const T & z=direction[0];

	      T * tensor_p=tensor_buffer; 
	      
	      T x2=x*x;
	      T x3=x2*x;
	      T x4=x2*x2;
	      T y2=y*y;
	      T y3=y2*y;
	      T y4=y2*y2;
	      T z2=z*z;
	      T z4=z2*z2;
	      
	      T m0=x*y;
	      T m1=(x2-y2);
	      T m2=(y2-4*z2);
	      T m3=(x2+y2);
	      T m4=(x4+y4);	      
	      
	      
 	      T tmp=tensor_p[0];
	      if (order_sd_data>=2)
	      {
		const T &a22r=tensor_p[1];
		const T &a22i=tensor_p[2];
		const T &a21r=tensor_p[3];
		const T &a21i=tensor_p[4];
		const T &a20r=tensor_p[5];
		
		const T l_weight=2*2+1;
		tmp+=T(l_weight*std::sqrt(3.0/2.0))*(a22r * m1 - a22i*2*m0 );
 		tmp+=T(l_weight*(std::sqrt(6.0)*z)) *(a21r * x - a21i * y);
 		tmp+=T(l_weight*(z2-m3/2))*a20r;
		
	      }
// 	      
// 	      if (true)
	      if (order_sd_data>=4)
	      {
		const T &a44r=tensor_p[6];
		const T &a44i=tensor_p[7];
		const T &a43r=tensor_p[8];
		const T &a43i=tensor_p[9];
		const T &a42r=tensor_p[10];
		const T &a42i=tensor_p[11];
		const T &a41r=tensor_p[12];
		const T &a41i=tensor_p[13];
		const T &a40r=tensor_p[14];		
		
		const T l_weight=4*2+1;
		
		tmp+=T(l_weight*1.0/4.0 * std::sqrt(35.0/2.0))*(a44r *(m4 -6*x2*y2 ) + a44i*4*(x*y3 - x3*y));
		tmp+=T(l_weight*1.0/2.0 * std::sqrt(35.0))*z*(a43r* (x3-3*x*y2) + a43i * (y3-3*x2*y));
 		tmp+=T(l_weight*-1.0/2.0 * std::sqrt(5.0/2.0)*(m3 - 6*z2))*(a42r *m1 - a42i * (2*m0));
		tmp+=T(l_weight*-1.0/2.0*std::sqrt(5.0) * z *(x2+2* m3 + m2)) *(a41r*x- a41i*y);
		tmp+=T(l_weight*3* (m4/8  - y2 * z2 + z4/3 + x2*m2/4))*a40r;
	      }
	      
	      
	      
	      sd_filter=tmp;
	    }
	    
	    return true;
    }
    
    
    
};

#endif
