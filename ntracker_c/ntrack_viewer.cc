//#define _POOL_TEST
//#define _NUM_THREADS_ 4

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"

#include "particles.h"
#include "graph_rep.h"
#include "mhs_gui_basic_viewer.h"
#include "mhs_gui_shader_new.h"
#include "mhs_cam.h"

#include "mhs_data_Hessian.h"
#include "mhs_data_DTI.h"
#include "mhs_data_SD.h"
#include "mhs_data_SD_new.h"
#include "mhs_data_SD_SH.h"
#include "mhs_data_SD_SHorg.h"

template<typename TData,typename T, int Dim>
class CTrackViewer;

const int Dim=3;



// Quad vertices
GLfloat quadVertices[] = {
    -1.0f,  1.0f,  0.0f, 1.0f,
     1.0f,  1.0f,  1.0f, 1.0f,
     1.0f, -1.0f,  1.0f, 0.0f,

     1.0f, -1.0f,  1.0f, 0.0f,
    -1.0f, -1.0f,  0.0f, 0.0f,
    -1.0f,  1.0f,  0.0f, 1.0f
};

// template<typename T>
class CRoi
    {

    private:
        typedef unsigned int index_type;
    public:

        GLuint vertex_buffer;
        GLuint normal_buffer;
        GLuint color_buffer;
        GLuint index_buffer;


        std::size_t i_length;
        Vector<float,4> color;


        CRoi ( const mxArray * mesh_data,int index,Vector<std::size_t,3> shape,Vector<float,4> color )
            {

            this->color=color;
            float rescale=1.0/std::max ( std::max ( shape[0],shape[1] ),shape[2] );

	    printf("rescale: %f\n",rescale);
	    
            try
                {

                CTriangleObject mesh ( mesh_data,index );
                std::size_t v_buffer_size=mesh.getNumVerts();
                std::size_t i_buffer_size=mesh.getNumIndeces();

                CTriangleObject::CVertex * tmp_buffer=new CTriangleObject::CVertex[v_buffer_size];
                CTriangleObject::CVertex * tmp_nbuffer=new CTriangleObject::CVertex[v_buffer_size];
                index_type * face_buffer=new index_type[i_buffer_size*3];

                mesh.cpy2IndexBuffer ( face_buffer,0 );
                mesh.cpy2VertexBuffer ( tmp_buffer,tmp_nbuffer );


                CTriangleObject::CVertex * tmp_buffer_p= tmp_buffer;
                CTriangleObject::CVertex * tmp_nbuffer_p= tmp_nbuffer;


//                 for ( std::size_t v=0; v<v_buffer_size; v++ )
//                     {
// 		    std::swap(tmp_buffer_p[v].position [0],tmp_buffer_p[v].position [2]);
// 		      
//                     Vector<float,3> position= ( tmp_buffer_p[v].position );
// 		    Vector<float,3> pos=position*rescale;
//                     tmp_buffer_p[v].position[0]=pos[0];
//                     tmp_buffer_p[v].position[1]=pos[1];
//                     tmp_buffer_p[v].position[2]=pos[2];
// 		    
// 		    
// 		    Vector<float,3> normal= ( tmp_nbuffer_p[v].position );
// 		    std::swap(normal[0],normal[2]);
// 		    normal*=(float)-1;
// 		    tmp_nbuffer_p[v].position[0]=normal[0];
//                     tmp_nbuffer_p[v].position[1]=normal[1];
//                     tmp_nbuffer_p[v].position[2]=normal[2];
//                     }
		
		   for ( std::size_t v=0; v<v_buffer_size; v++ )
                    {
                    Vector<float,3> position= ( tmp_buffer_p[v].position );
		    Vector<float,3> pos=position*rescale;
                    tmp_buffer_p[v].position[0]=pos[0];
                    tmp_buffer_p[v].position[1]=pos[1];
                    tmp_buffer_p[v].position[2]=pos[2];
                    }


		printf("copying to gpu:");
		mhs::mex_dumpStringNOW();
                glGenBuffers ( 1,&vertex_buffer );
                glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
                glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_buffer, GL_STATIC_DRAW );

                glGenBuffers ( 1,&normal_buffer );
                glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_nbuffer, GL_STATIC_DRAW );


                glGenBuffers ( 1,&index_buffer );
                glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
                glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 3*sizeof ( index_type ) *i_buffer_size, face_buffer, GL_STATIC_DRAW );
		printf("done\n");

                delete [] tmp_buffer;
                delete [] tmp_nbuffer;
                delete [] face_buffer;
                mhs_check_gl;


                i_length=i_buffer_size*3;




                }
            catch ( mhs::STAError & error )
                {

                throw error;
                }


            sta_assert_error ( sizeof ( CTriangleObject::CVertex ) ==sizeof ( float ) *3 );

            glEnable ( GL_CULL_FACE );
            glCullFace ( GL_BACK );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );

            mhs_check_gl;
            }

        ~CRoi()
            {
            mhs_check_gl;
            glDeleteBuffers ( 1, &index_buffer );
            mhs_check_gl;
            glDeleteBuffers ( 1, &vertex_buffer );
            mhs_check_gl;
            glDeleteBuffers ( 1, &normal_buffer );
            mhs_check_gl;
            }

        void enable_VBO ( bool normal=true )
            {

            glEnableClientState ( GL_VERTEX_ARRAY );
            glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
            glVertexPointer ( 3, GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );

            if ( normal )
                {
                glEnableClientState ( GL_NORMAL_ARRAY );
                glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                glNormalPointer ( GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );
                }
            glDisableClientState ( GL_COLOR_ARRAY );


            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
            }

        void disable_VBO()
            {
            glDisableClientState ( GL_NORMAL_ARRAY );
            glDisableClientState ( GL_VERTEX_ARRAY );
            glDisableClientState ( GL_COLOR_ARRAY );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );
            }

        void draw()
            {
            enable_VBO();
	    glDisable(GL_CULL_FACE);

            glColor4fv ( color.v );
            std::size_t offset =0;
            glDrawElements ( GL_TRIANGLES, i_length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
            mhs_check_gl;
	    glEnable(GL_CULL_FACE);
            disable_VBO();
	    

            }

    };


template<typename T,typename TData>
class CModelMesh
    {

// friend CTrackViewer<TData,T, Dim>;

    public :

    float datascale=1;

//   class Points: public OctPoints< float, 3>
//   {
//
//       friend pool<Points>;
//     private:
//
//     public:
//       Points()
//       {
//
//       };
//   };
        int resolution;
        
        
	

        enum class MESH : unsigned int
        {
            MESH_CENTERLINE_PARTICLE,
            MESH_PARTICLES,
            MESH_SURFACE,
            MESH_CENTERLINE,
            MESH_BIFURCATIONS,
            MESH_TERMINALS,
            MESH_SURFACE_TERMINALS,
            MESH_FIBER_CENTERLINES,
            MESH_FIBER_TERMINALS,
            MESH_SINGLE_PARTICLES,
	    MESH_BLOB_PARTICLES,
	    MESH_DATA_VIS,
	    MESH_DATA_CENTERLINE,
	    MESH_DATA_SPINE,
	    MESH_LENGTH,
	    MESH_BLOB_PARTICLES_UNIC,
	    MESH_DATA_VIS2,
	    MESH_BLOB_PARTICLES_UNIC2,
	    MESH_SURFACE_UNIQUE,
	    MESH_TERMINALS_UNIQUE,
            MESH_Count,
        };
        class Ellipoid
            {
            public:
                Vector<float,3> position;
                Vector<float,3> direction;
                float scaleA;
                float scaleB;
		int path_id;


                Ellipoid()
                    {
                    scaleA=-1;
                    scaleB=-1;
		    path_id=0;
                    }
            };

        Vector<float,3> dir2color ( Vector<float,3>  dir )
            {
//             dir[0]=std::abs ( dir[0] );
//             dir[1]=std::abs ( dir[1] );
//             dir[2]=std::abs ( dir[2] );

       dir[0]=0.15+0.85*std::abs(dir[0]);
       dir[1]=0.15+0.85*std::abs(dir[1]);
       dir[2]=0.15+0.85*std::abs(dir[2]);

            return dir;
            }

	bool always_transparent;
    protected:

//     OctTreeNode<float,3> * tree;

bool enabled;
        const float fixed_scale=0.5;
//     const float fixed_scale_fibers=0.05;

        GLuint vertex_buffer;
        GLuint normal_buffer;
        GLuint color_buffer;
        GLuint index_buffer;



	
        bool path_sorting;

        class CMesh
            {
            public:
                std::size_t i_offset;
                std::size_t i_length;

                CMesh ()
                    {
                    i_offset=0;
                    i_length=0;
                    }
            };

        std::size_t num_buffered_objects;
        CMesh * buffered_objects;

        std::size_t num_buffered_object_paths;
        CMesh * buffered_object_paths;
	

        typedef unsigned int index_type;

        int selection;
	std::size_t path_selection;

        float scales[2];

        std::vector<Ellipoid> particle_centers;

        std::vector<std::size_t> particle_path_id;
// 	std::vector<std::size_t> particle_path_id_sorted;

    public:
//    OctTreeNode<float,3>  * get_tree(){return tree;};

//     std::vector<Vector<float,3>> & get_particle_centers(){return  particle_centers;}
        std::vector<Ellipoid> & get_particle_centers()
            {
            return  particle_centers;
            }

        float *  get_scales()
            {
            return scales;
            };

        void set_selected_particle ( int selection )
            {
            sta_assert_error ( selection<particle_centers.size() );
            sta_assert_error ( selection<num_buffered_objects );

            this->selection=selection;
            };
	    
	std::size_t set_active_paths ( std::size_t selection )
            {
	    
	    this->path_selection=0;  
            if (path_sorting)
	    {
	      this->path_selection=selection;
	      ///*this->path_selection=std::round(selection*num_buffered_object_paths);*/
	      this->path_selection=std::max(path_selection,std::size_t(0));
	      this->path_selection=std::min(path_selection,num_buffered_object_paths-1);
	      
// 	      this->path_selection+=num_buffered_object_paths;
// 	      this->path_selection%=num_buffered_object_paths;
	      //this->path_selection=std::max(std::size_t(0),path_selection);
	    }/*else 
	    {
		printf("no path sorting\n");
	    }*/
	    
	    return path_selection;
            };    
	    


    protected:
      
      
       class C_pathid_members{
		      public:
		      bool sort_by_members=false;
		      int path_id;
// 		      int org_path_id;
		      std::size_t num_elements;
		      bool operator< (const C_pathid_members& right) const {
			if (sort_by_members)
			{
			  return  num_elements < right.num_elements; 
			}
			
 			return  path_id < right.path_id; 
			//return  org_path_id < right.org_path_id; 
		      }
		      
		        bool operator==(const C_pathid_members &other) const {
			  return (other.path_id==path_id);
			  }
			  bool operator!=(const C_pathid_members &other) const {
			  return (!(other.path_id==path_id));
			  }
		    };

     void  pathlength_count ( const mhs::dataArray<T> & treedata,	
	       const mhs::dataArray<T> & conncection_data,
	       std::vector<C_pathid_members> & unique_path_ids			
// 		std::vector<std::size_t> & path_remap,
// 		std::vector<std::size_t> & path_remap_ids,
// 		std::vector<std::size_t> & index_array_counts,
// 		bool sort_edges=true,
// 		const std::vector<bool> * valid=NULL
			    )
      {
	
		      T * pconncection_data2=conncection_data.data;
			std::size_t num_connections=conncection_data.dim[0];
			
			 T * ptreedata2=treedata.data;
			 
                    std::vector<int> path_ids_unsorted;
                    std::vector<int> path_ids_sorted;
		    std::vector<float> edge_length_unsorted;
		    
                    
			
			path_ids_unsorted.resize ( num_connections );
			 edge_length_unsorted.resize ( num_connections );
			 for ( std::size_t  i=0; i<num_connections; i++ )
			 {
			   edge_length_unsorted[i]=0;
			 }
                      
			    for ( std::size_t  i=0; i<num_connections; i++ )
                            {
                                int pointidxA=pconncection_data2[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXA];
				int pointidxB=pconncection_data2[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXB];
                                T * pointA_data=ptreedata2+pointidxA*treedata.dim[1];
				T * pointB_data=ptreedata2+pointidxB*treedata.dim[1];
                                int path_id= ( ( int ) ( pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_PATH_ID] ) );
				
				Vector<T,3> posA(pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX], 
						 pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
						 pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ]);
				Vector<T,3> posB(pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX], 
						 pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
						 pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ]);
				
				edge_length_unsorted[i]+=std::sqrt((posA-posB).norm2());
				
                                path_ids_unsorted[i]=path_id;
                            pconncection_data2+=conncection_data.dim[1];
                            }
                           

                          /*######################################
			 * 
			 *  sorting path ids 
			 * 
			 ######################################*/
                         path_ids_sorted=path_ids_unsorted;
// 			
			
                        std::sort ( path_ids_sorted.begin(), path_ids_sorted.end() );

			
                          /*######################################
			 * 
			 *  making ids unique 
			 * 
			 ######################################*/			
			
			unique_path_ids.resize(path_ids_unsorted.size());
			for (std::size_t i=0;i<path_ids_sorted.size();i++)
			{
			 unique_path_ids[i].path_id=path_ids_sorted[i];
			 unique_path_ids[i].num_elements=0;
			}
			

                        typename std::vector<C_pathid_members>::iterator it= std::unique ( unique_path_ids.begin(), unique_path_ids.end() ); // 10 20 30 20 10 ?  ?  ?  ?
                        unique_path_ids.resize ( std::distance ( unique_path_ids.begin(),it ) );
			
                        printf ( " / found %d unqiue paths in total / ",unique_path_ids.size() );
                        mhs::mex_dumpStringNOW();



                        int min_path_id=unique_path_ids[0].path_id;
                        int max_path_id=unique_path_ids[unique_path_ids.size()-1].path_id-min_path_id+1;

			
			 /*######################################
			 * 
			 *  sort path by numberof elements
			 * 
			 ######################################*/			
			
                        sta_assert_error ( max_path_id<10000000 );
                        std::vector<std::size_t> inverse_id_map;
                        inverse_id_map.resize ( max_path_id );
			
			
			bool sort_by_members=(unique_path_ids.size()>3);
			if (!sort_by_members)
			{
			 printf("only three path members, no sorting\n"); 
			}else
			{
			  printf("%d path members -> sorting\n",unique_path_ids.size()); 
			}
			  
			  
			
			for ( std::size_t id=0; id<unique_path_ids.size(); id++ )
			{
			sta_assert_error ( max_path_id>unique_path_ids[id].path_id-min_path_id );
			sta_assert_error ( unique_path_ids[id].path_id-min_path_id>-1 );
			inverse_id_map[unique_path_ids[id].path_id-min_path_id]=id;
			unique_path_ids[id].sort_by_members=sort_by_members;
			
			//unique_path_ids[id].sort_by_members=true;
			}
			
			
			for ( std::size_t  count=0; count<num_connections; count++ )
			{
			  std::size_t unique_id_indx=inverse_id_map[path_ids_unsorted[count]-min_path_id];
			  sta_assert_error ( unique_id_indx<unique_path_ids.size() );
			  
                          
                          
                          float p_lengt=edge_length_unsorted[count];
			  //float p_lengt=path_ids_unsorted[count];
			  //unique_path_ids[unique_id_indx].num_elements++;
			  unique_path_ids[unique_id_indx].num_elements+=std::size_t(std::ceil(p_lengt*65536));
			  
			}
// 			std::sort ( unique_path_ids.begin(), unique_path_ids.end() );
      }
      
      
	std::size_t  sort ( const mhs::dataArray<T> & treedata,	
	       const mhs::dataArray<T> & conncection_data,
		std::vector<std::size_t> & path_remap,
		std::vector<std::size_t> & path_remap_ids,
		std::vector<std::size_t> & index_array_counts,
// 		std::vector<int> & path_ids_unsorted,
		bool sort_edges=true,
		const std::vector<bool> * valid=NULL)
	{
		 
		    
                    std::vector<int> path_ids_unsorted;
                    std::vector<int> path_ids_sorted;
		    
                    std::vector<C_pathid_members> unique_path_ids;
		   
                    

      
		    T * ptreedata2=treedata.data;
                    std::size_t numpoints=treedata.dim[0];

                    T * pconncection_data2=conncection_data.data;

                    

                        std::size_t num_particles=0;
			std::size_t num_connections=numpoints;
			if (sort_edges)
			{
			  num_connections=conncection_data.dim[0];
// 			  num_particles=num_connections;
			}
		    
			if (valid!=NULL)
			{
				for ( std::size_t  i=0; i<num_connections; i++ )
				{
				//if (valid[i])
				    if ((valid==NULL) || (*valid)[i])
				    {
					num_particles++;
				    }
				}
			}else 
			{
			  num_particles=num_connections;
			}
		    
			  /*######################################
			 * 
			 *  collecting all path ids
			 * 
			 ######################################*/
			
                        path_ids_unsorted.resize ( num_particles );
                        std::size_t count=0;
                        printf ( "sorting %d edges: ",num_particles );
                        mhs::mex_dumpStringNOW();
			
			 std::size_t count2=0;
			if (sort_edges)
			{
			 
			    for ( std::size_t  i=0; i<num_connections; i++ )
                            {
                            //if (valid[i])
				if ((valid==NULL) || (*valid)[i])
                                {
//                                 point_id_list.push_back ( Vector<std::size_t,2> ( i,count ) );
                                int pointidxA=pconncection_data2[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXA];
                                T * pointA_data=ptreedata2+pointidxA*treedata.dim[1];
                                int path_id= ( ( int ) ( pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_PATH_ID] ) );
                                //int pointidxB=pconncection_data[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXB];

                                path_ids_unsorted[count]=path_id;
                                count++;
                                }else 
				{
				  count2++;
				}
                            pconncection_data2+=conncection_data.dim[1];
                            }
                            
                           
			}else 
			{
			 
			    for ( std::size_t  i=0; i<num_connections; i++ )
                            {
				if ((valid==NULL) || (*valid)[i])
                                {
				 int path_id= ( ( int ) ( ptreedata2[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_PATH_ID] ) ); 
                                path_ids_unsorted[count]=path_id;
                                count++;
                                }else 
				{
				  count2++;
				}
                             ptreedata2+=treedata.dim[1];
                            }
                            
			}
                            sta_assert_error ( count+count2==num_connections );

                          /*######################################
			 * 
			 *  sorting path ids 
			 * 
			 ######################################*/
                         path_ids_sorted=path_ids_unsorted;
// 			
			
                        std::sort ( path_ids_sorted.begin(), path_ids_sorted.end() );

			
                          /*######################################
			 * 
			 *  making ids unique 
			 * 
			 ######################################*/			
			
			unique_path_ids.resize(path_ids_unsorted.size());
			for (std::size_t i=0;i<path_ids_sorted.size();i++)
			{
			 unique_path_ids[i].path_id=path_ids_sorted[i];
			 unique_path_ids[i].num_elements=0;
			}
			

                        typename std::vector<C_pathid_members>::iterator it= std::unique ( unique_path_ids.begin(), unique_path_ids.end() ); // 10 20 30 20 10 ?  ?  ?  ?
                        unique_path_ids.resize ( std::distance ( unique_path_ids.begin(),it ) );
			
                        printf ( " / found %d unqiue paths / ",unique_path_ids.size() );
                        mhs::mex_dumpStringNOW();



                        int min_path_id=unique_path_ids[0].path_id;
                        int max_path_id=unique_path_ids[unique_path_ids.size()-1].path_id-min_path_id+1;

			
			 /*######################################
			 * 
			 *  sort path by numberof elements
			 * 
			 ######################################*/			
			
                        sta_assert_error ( max_path_id<10000000 );
                        std::vector<std::size_t> inverse_id_map;
                        inverse_id_map.resize ( max_path_id );
			
			for ( std::size_t id=0; id<unique_path_ids.size(); id++ )
			{
			sta_assert_error ( max_path_id>unique_path_ids[id].path_id-min_path_id );
			sta_assert_error ( unique_path_ids[id].path_id-min_path_id>-1 );
			inverse_id_map[unique_path_ids[id].path_id-min_path_id]=id;
			unique_path_ids[id].sort_by_members=true;
			}
			
			/*
			for ( std::size_t  count=0; count<num_particles; count++ )
			{
			  std::size_t unique_id_indx=inverse_id_map[path_ids_unsorted[count]-min_path_id];
			  sta_assert_error ( unique_id_indx<unique_path_ids.size() );
			  unique_path_ids[unique_id_indx].num_elements++;
			}
			*/
		
			std::vector<C_pathid_members>  total_unique_path_ids;	
			pathlength_count ( treedata,	conncection_data,total_unique_path_ids);
	       
			sta_assert_error(total_unique_path_ids.size()>=unique_path_ids.size());
			
			std::size_t c2=0;
			for (std::size_t c=0;c<unique_path_ids.size();c++)
			{
			  while (total_unique_path_ids[c2].path_id<unique_path_ids[c].path_id)
			  {
			    c2++;
			   sta_assert_error(c2<total_unique_path_ids.size()); 
			  }
			  
			  sta_assert_error((total_unique_path_ids[c2].path_id==unique_path_ids[c].path_id)); 
			  unique_path_ids[c].num_elements=total_unique_path_ids[c2].num_elements;
			}
			
			
			std::sort ( unique_path_ids.begin(), unique_path_ids.end() );
			
// 			for ( std::size_t  count=0; count<unique_path_ids.size(); count++ )
// 			printf("%u %u\n",count,unique_path_ids[count].path_id);

			 /*######################################
			 * 
			 * 
			 * 
			 ######################################*/
			
                        index_array_counts.resize ( unique_path_ids.size() );
			
			std::vector<std::size_t> index_array_offsets;
                        index_array_offsets.resize ( unique_path_ids.size() );

                        for ( std::size_t id=0; id<unique_path_ids.size(); id++ )
                            {
                            sta_assert_error ( max_path_id>unique_path_ids[id].path_id-min_path_id );
                            sta_assert_error ( unique_path_ids[id].path_id-min_path_id>-1 );
                            inverse_id_map[unique_path_ids[id].path_id-min_path_id]=id;
                            index_array_counts[id]=0;
                            index_array_offsets[id]=0;
                            }

			 /*######################################
			 * 
			 *  creating mapping 
			 * 
			 ######################################*/                       
                            

                        path_remap.resize ( num_particles );
                        path_remap_ids.resize ( num_particles );

                        for ( std::size_t  i=0; i<num_particles; i++ )
                            {
                            path_remap[i]=-1;
                             path_remap_ids[i]=-1;
                            }


                  

                            count=0;
                            for ( std::size_t  i=0; i<num_connections; i++ )
                                {
// 		      if (valid[i])
				    if ((valid==NULL) || (*valid)[i])
                                    {
                                    std::size_t unique_id_indx=inverse_id_map[path_ids_unsorted[count]-min_path_id];
                                    sta_assert_error ( unique_id_indx<unique_path_ids.size() );
                                    index_array_counts[unique_id_indx]++;
                                    count++;
                                    }
                                }
                            for ( std::size_t id=1; id<unique_path_ids.size(); id++ )
                                {
                                index_array_offsets[id]=index_array_offsets[id-1]+index_array_counts[id-1];
                                }
                            count=0;
                            for ( std::size_t  i=0; i<num_connections; i++ )
                                {
// 		      if (valid[i])
				    if ((valid==NULL) || (*valid)[i])
                                    {
                                    std::size_t unique_id_indx=inverse_id_map[path_ids_unsorted[count]-min_path_id];

                                    int remap=index_array_offsets[unique_id_indx];

                                    path_remap[remap]=i;
                                    path_remap_ids[remap]=unique_id_indx;
                                    index_array_offsets[unique_id_indx]++;
                                    index_array_counts[unique_id_indx]--;

                                    count++;
                                    }
                                }

                            for ( std::size_t id=0; id<unique_path_ids.size(); id++ )
                                {
                                if ( index_array_counts[id]!=0 )
                                    printf ( "%u %u\n",id,index_array_counts[id] );
                                sta_assert_error ( index_array_counts[id]==0 );
                                }

//                             if ( false )
//                                 {
//                                 std::vector<int> debug;
//                                 debug=path_remap;
//                                 std::sort ( debug.begin(), debug.end() );
//                                 std::vector<int>::iterator it= std::unique ( debug.begin(), debug.end() ); // 10 20 30 20 10 ?  ?  ?  ?
//                                 debug.resize ( std::distance ( debug.begin(),it ) );
//                                 sta_assert_error ( debug.size() ==path_remap.size() );
//                                 }
	  
	    
		    return unique_path_ids.size();
	}
	
	


        void createMesh_centerline2 ( mhs::dataArray<T> & treedata,
                                      mhs::dataArray<T> & conncection_data,
                                      Vector<std::size_t,3> shape,
                                      int mode=0,
				      int colormode=-1)
            {
            printf ( "init mesh (centerline2) " );

            std::size_t v_buffer_size=0;
            std::size_t i_buffer_size=0;



            CTriangleObject cylinder_particle_mesh ( CTriangleObject::object_id_cylinder,resolution );




            float rescale=1.0/std::max ( std::max ( shape[0],shape[1] ),shape[2] );

            int clynder_verts=cylinder_particle_mesh.getNumVerts();
            sta_assert_error ( clynder_verts%2==0 );

            try
                {
                sta_assert_error ( treedata.data!=NULL )
                    {

                    T * ptreedata=treedata.data;
                    unsigned int numpoints=treedata.dim[0];

                    T * pconncection_data2=conncection_data.data;

                    unsigned int num_connections=conncection_data.dim[0];

                    if ( num_connections<1 )
		    {
			enabled=false;
                        return;
		    }





                    num_buffered_objects=num_connections;
                    buffered_objects=new CMesh[num_buffered_objects];

                    particle_centers.resize ( num_connections );

                    if ( path_sorting )
		    {
                        particle_path_id.resize ( num_connections );
// 			particle_path_id_sorted.resize(num_connections);
		    }


                    sta_assert_error ( conncection_data.dim[1]== ( CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_Count ) );

                    v_buffer_size+=num_connections*cylinder_particle_mesh.getNumVerts();
                    i_buffer_size+=num_connections*cylinder_particle_mesh.getNumIndeces();


		    //printf("\n\n###  %u %u %u %u \n###\n",v_buffer_size,sizeof ( CTriangleObject::CVertex ) ,v_buffer_size,sizeof ( CTriangleObject::Vertex<unsigned char> ));
                    printf ( "vertex buffer size %u MB\n", (( 2*v_buffer_size*sizeof ( CTriangleObject::CVertex ) + v_buffer_size*sizeof ( CTriangleObject::Vertex<unsigned char> )) /1024)/1024 );
                    printf ( "index  buffer size %u MB\n", (( 3*i_buffer_size*sizeof ( index_type ) ) /1024)/1024 );
                    CTriangleObject::CVertex * tmp_buffer=new CTriangleObject::CVertex[v_buffer_size];
                    CTriangleObject::CVertex * tmp_nbuffer=new CTriangleObject::CVertex[v_buffer_size];
                    CTriangleObject::Vertex4<unsigned char> * tmp_cbuffer=new CTriangleObject::Vertex4<unsigned char>[v_buffer_size];



                    CTriangleObject::CVertex * tmp_buffer_p= tmp_buffer;
                    CTriangleObject::CVertex * tmp_nbuffer_p= tmp_nbuffer;
                    CTriangleObject::Vertex4<unsigned char> * tmp_cbuffer_p= tmp_cbuffer;

                    index_type * face_buffer=new index_type[i_buffer_size*3];


                    index_type *  tmp_face_buffer_p= face_buffer;
                    std::size_t index_offset=0;
                    std::size_t indeces_count=0;

                    sta_assert_error ( treedata.dim[1]> ( CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG ) );

                        
	    std::vector<std::size_t> path_lengths;
		    std::size_t max_path_length=0;
		    std::size_t min_path_length=1000000000000000;
		    path_lengths.resize(num_connections);
		    
		    std::vector<std::size_t> path_remap;
		    std::vector<std::size_t> path_remap_ids;
		    std::vector<std::size_t> index_array_counts;
		    std::size_t total_unique_paths=0;
		      std::vector<int> rand_color_code;
		    rand_color_code.resize(num_connections);
		    
		      for (std::size_t i=0;i<num_connections;i++)
		    {
		      path_lengths[i]=0;
		      
		    }
		    
		    
		    std::size_t max_path_id=0;
		    
                    if ( path_sorting )
                    {
		      total_unique_paths=sort ( treedata,	
			    conncection_data,
			    path_remap,path_remap_ids,index_array_counts,//path_ids_unsorted,
			    true,
			    NULL);
		      
			//std::srand (1);
		       //std::srand (std::time(NULL));
		       for ( int p=0; p<num_connections; p++ )
                        {
			     sta_assert_error_c ( p<path_remap.size(),printf("%u %u\n",p,path_remap.size()));      
			      std::size_t i=path_remap[p];
			      sta_assert_error ( i<num_connections );
			        std::size_t path_id=path_remap_ids[p];
			    sta_assert_error ( path_id<num_connections );
			    
			     path_lengths[path_id]++;
			    max_path_length=std::max(path_lengths[path_id],max_path_length);
			    min_path_length=std::min(path_lengths[path_id],min_path_length);
			    
			         if (colormode==3)
			      {
				
				
				 sta_assert_error ( path_id<num_connections );
				rand_color_code[path_id]=path_id;//std::rand();
				max_path_id=std::max(max_path_id,path_id);
				
			      }
			}

			if (max_path_id>3)
			{
			for (std::size_t t=0;t<max_path_id;t++)
			  {
			    std::size_t perm=std::rand() % (max_path_id-t)+t;
			    if (perm<max_path_id)
			    {
			      std::swap(rand_color_code[t],rand_color_code[perm]);
			    }
			  }
			}
		    }



		  printf("generating mesh:");
		  mhs::mex_dumpStringNOW();

                    //for (int i=0; i<num_connections; i++)
                    for ( int p=0; p<num_connections; p++ )
                        {

                        std::size_t i=p;
                        if ( path_sorting )
                            {
			    sta_assert_error(p<num_connections);  
                            i=path_remap[p];
                            }


                        sta_assert_error ( i<num_connections );
                        T * pconncection_data=pconncection_data2+i*conncection_data.dim[1];

                        int pointidxA=pconncection_data[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXA];
                        int pointidxB=pconncection_data[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXB];

                        sta_assert_error ( pointidxA<numpoints );
                        sta_assert_error ( pointidxB<numpoints );

                        T * pointA_data=ptreedata+pointidxA*treedata.dim[1];
                        T * pointB_data=ptreedata+pointidxB*treedata.dim[1];




                        Vector<float,3> positionA ( pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX],
                                                    pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
                                                    pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ] );

                        Vector<float,3> positionB ( pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX],
                                                    pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
                                                    pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ] );

                        Vector<float,3> directionA ( pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRX],
                                                     pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRY],
                                                     pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRZ] );

                        Vector<float,3> directionB ( pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRX],
                                                     pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRY],
                                                     pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRZ] );
                        
                        
                        float costsA = pointA_data[CTracker<float,T,Dim>::POINT_A_COST];
                        float costsB = pointB_data[CTracker<float,T,Dim>::POINT_A_COST];


                        unsigned int typeA= ( ( unsigned int ) ( pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );
                        unsigned int typeB= ( ( unsigned int ) ( pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );


                        if ( typeA== ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) )
                            directionA=directionB;
                        if ( typeB== ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) )
                            directionB=directionA;


                        float scaleA= ( pointA_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_SCALE] );
                        float scaleB= ( pointB_data[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_SCALE] );


                        int color_id=CSceneRenderer::Cyellow;


                        Vector<float,3>  color[2];
                        color[0]=CSceneRenderer::particle_colors[color_id];
                        color[1]=CSceneRenderer::particle_colors[color_id];

                        switch ( mode )
                            {
                            case 1:
                                {

                                scaleA*=0.3f;
                                scaleA=std::max ( std::min ( 0.5f,scaleA ),scaleA );
                                scaleB*=0.3f;
                                scaleB=std::max ( std::min ( 0.5f,scaleB ),scaleB );

                                }
                            break;
                            case 2:
                                {

// 			color[0][0]=std::abs(directionA[0]);
// 			color[0][1]=std::abs(directionA[1]);
// 			color[0][2]=std::abs(directionA[2]);
//
// 			color[1][0]=std::abs(directionB[0]);
// 			color[1][1]=std::abs(directionB[1]);
// 			color[1][2]=std::abs(directionB[2]);
// 			scaleA=scaleB=fixed_scale;
                                scaleA*=0.3f;
                                scaleA=std::max ( std::min ( 0.5f,scaleA ),scaleA );
                                scaleB*=0.3f;
                                scaleB=std::max ( std::min ( 0.5f,scaleB ),scaleB );

                                }
                            break;
                            case 4:
                                {

                                color[0]=dir2color ( directionA );
                                color[1]=dir2color ( directionB );
// 			color[0][0]=std::abs(directionA[0]);
// 			color[0][1]=std::abs(directionA[1]);
// 			color[0][2]=std::abs(directionA[2]);
//
// 			color[1][0]=std::abs(directionB[0]);
// 			color[1][1]=std::abs(directionB[1]);
// 			color[1][2]=std::abs(directionB[2]);
// 			scaleA=scaleB=fixed_scale_fibers;
                                }
                            break;
			    case 5:
                                {
				  scaleA=0.025;
				  scaleB=0.025;
                                }
                            break;
                            }

// printf("A(%d)",colormode);
			    if (( path_sorting )&&(colormode>-1))
                                    {
				      sta_assert_error ( p<path_remap_ids.size() );    
				      sta_assert_error ( p>-1);    
		
				      std::size_t path_id=path_remap_ids[p];
				      
				      
				      
				      switch  (colormode)
				      {
					
// 					case 2:
// 					{
// 					  
// // 					  float pc=float((23124354*path_id)%120+path_id%13)/(120.0f+13);
// 					  int mpaths=(total_unique_paths > 180 ? 180 : total_unique_paths);
// 					  float pc=float(path_id%mpaths)/T(mpaths+1);
// 					  hsv2rgb<float> ( float ( pc*360.0f ) ,(0.3f+0.7f*pc),(0.8f+0.2f*pc), color[0],color[1],color[2]); 
// 					} 
// 					break;
					
					case 3:
					{
					 sta_assert_error ( path_id<numpoints );
					 
				      
					  
// 					  float pc=float((23124354*path_id)%120+path_id%13)/(120.0f+13);
					  int mpaths=(total_unique_paths > 180 ? 180 : total_unique_paths);
					  //float pc=float((rand_color_code[path_id])%128)/T(128);
					  std::size_t div=std::max(std::min(max_path_id+1,std::size_t(360)),std::size_t(1));
					  
// 					  printf("(%u)",path_id);
					  
					  if (max_path_id<4)
					  {
					    div=std::max(std::min(4,360),(1));  
					  }
					  
					   
					  sta_assert_error(rand_color_code.size()>path_id);
					  sta_assert_error(path_lengths.size()>path_id);
					  
// 					  printf("e %u %u ",path_id,rand_color_code.size());
// 					  printf("e %u %u ",rand_color_code[path_id],div);
// 					  printf("e !%u %u! ",0%0,(rand_color_code[path_id])%1);
// 					  printf("e !%u %u! ",0%0,(rand_color_code[path_id])%0);
					  
					  //float pc=float((rand_color_code[path_id])%div)/T(div+1);
					  float pc=float((rand_color_code[path_id])%div)/T(div);
					  
// 					  printf("e");
					  //float br=float(rand_color_code[path_id]+path_id%100)/T(100);
					  float br=float(path_lengths[path_id]-min_path_length)/(max_path_length-min_path_length+1);
					  
// 					  printf("e");
					  
					  //hsv2rgb<float> ( float ( pc*360.0f ) ,(0.5f+0.5f*br),(0.6f+0.4f*br), color[0][0],color[0][1],color[0][2]);
					  
					  
					  
					     hsv2rgb<float> ( float ( pc*360.0f ) ,(0.6f+0.4f*br),(0.8f+0.2f*br), color[0][0],color[0][1],color[0][2]); 
					  
					  
					  color[1][0]=color[0][0];
					  color[1][1]=color[0][1];
					  color[1][2]=color[0][2];
// 					  printf("e");
					} 
					break;
					
					default:
					  color[1][0]=color[0][0]=0.9;
					  color[1][1]=color[0][1]=0.9;
					  color[1][2]=color[0][2]=0.9;
					
// 					default:
// 					{
// 					float pc=float(path_lengths[path_id]-min_path_length)/(max_path_length-min_path_length+1);
//  					hsv2rgb<float> ( float ( pc*360.0f ) ,(0.3f+0.7f*pc),(0.8f+0.2f*pc), color[0],color[1],color[2]); 
// 					}
				      }
					//hsv2rgb<float> ( float ( pc*360.0f ) ,0.75f,1.0f, color[0],color[1],color[2]); 
// 				      color*=pc;
// 				      
// 					tmp_cbuffer_p[v].r=c[0]*255;
// 					tmp_cbuffer_p[v].g=c[1]*255;
// 					tmp_cbuffer_p[v].b=c[2]*255;
				    
				    
				    }




                        //Vector<float,3> n=positionA-positionB;

                        Vector<float,3> n=positionB-positionA;
                        n.normalize();


                        sta_assert_error ( p<particle_centers.size() );
                        particle_centers[p].position= ( positionB+positionA ) /2;
                        particle_centers[p].direction=n;
                        particle_centers[p].scaleA=std::max ( scaleA,scaleB );
                        particle_centers[p].scaleB=std::sqrt ( ( positionB-positionA ).norm2() ) /2;


                        if ( n.dot ( directionA ) <0 )
                            directionA*=T ( -1 );
//
                        if ( directionB.dot ( directionA ) <0 )
                            directionB*=T ( -1 );


                        Vector<float,3> * positions[2];
                        positions[0]=&positionA;
                        positions[1]=&positionB;

                        Vector<float,3> * directions[2];
                        directions[0]=&directionA;
                        directions[1]=&directionB;

                        float * scales[2];
                        scales[0]=&scaleA;
                        scales[1]=&scaleB;

                        CTriangleObject & mesh=cylinder_particle_mesh;

                        mesh.cpy2VertexBuffer ( tmp_buffer_p,tmp_nbuffer_p );
                        std::size_t num_verts=mesh.getNumVerts();
                        std::size_t num_indeces=mesh.getNumIndeces();
                        Matrix<float,4> rotation;


                        for ( int d=0; d<2; d++ )
                            {
                            Vector<float,3> & direction=*directions[d];
                            Vector<float,3> & position=*positions[d];
                            float & scale=*scales[d];

// 		      if (n.dot(direction)>0)
// 			direction*=T(-1);




                            if ( d==0 )
                                {
                                Vector<float,Dim> rot_axA;
                                rot_axA[0]=0;
                                rot_axA[1]=0;
                                rot_axA[2]=1;

                                rotation=Matrix<float,3>::OpenGL_glRotateN12N2 ( rot_axA,directionA );
// 			if (i==0)
// 			{
// 			    (rotation.multv3(rot_axA)).print();
// 			    directionA.print();
// 			}
//
                                }
                            else
                                {

                                Matrix<float,4> rotation2;
// 			  tran_mat(directionA,directionB, rotation2);
                                rotation2=Matrix<float,3>::OpenGL_glRotateN12N2 ( directionA,directionB );
                                rotation=rotation2*rotation;
// 			  if (i==0)
// 			{
// 			   Vector<float,Dim> rot_axA;
// 			rot_axA[0]=0;
// 			rot_axA[1]=0;
// 			rot_axA[2]=1;
// 			   (rotation.multv3(rot_axA)).print();
// 			    directionB.print();
// 			}
// 			  if (i==0)
// 			    (rotation*rotation.transpose()).print();
                                }


//                             float det= rotation.m[0][0]*(rotation.m[1][1]*rotation.m[2][2]-rotation.m[1][2]*rotation.m[2][1])
// 				      -rotation.m[0][1]*(rotation.m[1][0]*rotation.m[2][2]-rotation.m[1][2]*rotation.m[2][0])
// 				      +rotation.m[0][2]*(rotation.m[1][0]*rotation.m[2][1]-rotation.m[1][1]*rotation.m[2][0]);

			    
				      
                            for ( std::size_t v2=0; v2<num_verts/2; v2++ )
                                {
				 
				  
                                //std::size_t v=v2+d*num_verts/2;
                                std::size_t v=v2*2+d;
			    
// 				if (det<0)
// 				{
// 				  v=(num_verts/2-v2-1)*2+d;
// 				}
			    
			    
                                Vector<float,3> vert= ( tmp_buffer_p[v].position );
                                vert[2]=T ( 0 );
// 			  if (i==0)
// 			    vert.print();
                                Vector<float,3> pos= ( position+rotation.multv3 ( vert*scale ) ) *rescale;
                                tmp_buffer_p[v].position[0]=pos[0];
                                tmp_buffer_p[v].position[1]=pos[1];
                                tmp_buffer_p[v].position[2]=pos[2];

				
                                vert= ( tmp_nbuffer_p[v].position );
				Vector<float,3>  n_new=rotation.multv3 ( vert );
				
// 				Vector<float,3> n_new;
// 				if (det>0)
// 				{
// 				  n_new=rotation.multv3 ( vert );
// 				}else
// 				{
// 				  n_new=rotation.multv3 ( vert )*(-1.0f);
// 				}
                                tmp_nbuffer_p[v].position[0]=n_new[0];
                                tmp_nbuffer_p[v].position[1]=n_new[1];
                                tmp_nbuffer_p[v].position[2]=n_new[2];

                                tmp_cbuffer_p[v].r=color[d][0]*255;
                                tmp_cbuffer_p[v].g=color[d][1]*255;
                                tmp_cbuffer_p[v].b=color[d][2]*255;
                                tmp_cbuffer_p[v].a=std::min(costsA,costsB)*255;
                                //tmp_cbuffer_p[v].a=std::max(costsA,costsB)*255;
                                //tmp_cbuffer_p[v].a=(costsA+costsB)*0.5*250;
                                

                                }
                            }

// 		    buffered_objects[i].i_offset=(i*num_indeces*3);
// 		    buffered_objects[i].i_length=(i_buffer_size-i*num_indeces)*3;

// 		     buffered_objects[i].i_offset=0;
// 		    if (i>0)
// 		    buffered_objects[i].i_offset+=3*i_buffer_size-buffered_objects[i-1].i_length;
//
// 		    buffered_objects[i].i_length=(i_buffer_size-i*num_indeces)*3;

                        buffered_objects[p].i_offset=3*indeces_count;
                        buffered_objects[p].i_length=3* ( i_buffer_size-indeces_count );





                        indeces_count+=num_indeces;

                        if ( path_sorting )
                            {
                            std::size_t path_id=path_remap_ids[p];
//                             sta_assert_error ( unique_path_ids.size() >path_id );
                            index_array_counts[path_id]+= ( num_indeces );
                            particle_path_id[p]=path_id;
 			    particle_centers[p].path_id=path_id;
                            }

                        mesh.cpy2IndexBuffer ( tmp_face_buffer_p,index_offset );

                        index_offset+=num_verts;

                        tmp_buffer_p+=num_verts;
                        tmp_nbuffer_p+=num_verts;
                        tmp_cbuffer_p+=num_verts;
                        tmp_face_buffer_p+=num_indeces *3;

// 		    pconncection_data+=conncection_data.dim[1];
                        }

                        
                 printf("done\n");
		 /* mhs::mex_dumpStringNOW();  */  
                        
                        

                    if ( num_buffered_objects>0 )
                        {
			printf("copying to gpu:");
			mhs::mex_dumpStringNOW();
                        glGenBuffers ( 1,&vertex_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_buffer, GL_STATIC_DRAW );

                        glGenBuffers ( 1,&normal_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_nbuffer, GL_STATIC_DRAW );

                        glGenBuffers ( 1,&color_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, color_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::Vertex4<unsigned char> ) *v_buffer_size, tmp_cbuffer, GL_STATIC_DRAW );




                        glGenBuffers ( 1,&index_buffer );
                        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
                        glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 3*sizeof ( index_type ) *i_buffer_size, face_buffer, GL_STATIC_DRAW );
			
			printf("done\n");


                        if ( path_sorting )
                            {
                            std::size_t used_paths=0;
                            for ( std::size_t a=0; a<index_array_counts.size(); a++ )
                                {
                                if ( index_array_counts[a]>0 )
                                    used_paths++;
                                }
                            printf ( "%d of %d paths used\n",used_paths,total_unique_paths );
//                             sta_assert_error ( unique_path_ids.size() ==used_paths );



                            num_buffered_object_paths=used_paths;
                            buffered_object_paths=new CMesh[num_buffered_object_paths];

                            used_paths=0;
                            std::size_t indeces_count=0;
                            for ( std::size_t a=0; a<index_array_counts.size(); a++ )
                                {
                                if ( index_array_counts[a]>0 )
                                    {

                                    buffered_object_paths[used_paths].i_offset=indeces_count*3;
                                    buffered_object_paths[used_paths].i_length= ( i_buffer_size-indeces_count ) *3;
                                    indeces_count+=index_array_counts[a];
// 			  printf("%d %d\n",buffered_object_paths[used_paths].i_offset,buffered_object_paths[used_paths].i_length);
                                    used_paths++;
                                    }
                                }
                            }

                        }

                    delete [] tmp_cbuffer;
                    delete [] tmp_buffer;
                    delete [] tmp_nbuffer;
                    delete [] face_buffer;
                    mhs_check_gl;

//                     buffered_objects[0].i_offset=0;
//                     buffered_objects[0].i_length=i_buffer_size*3;

                     sta_assert_error(buffered_objects[0].i_offset==0);
                     sta_assert_error(buffered_objects[0].i_length==i_buffer_size*3);


                    }
                }
            catch ( mhs::STAError & error )
                {

                throw error;
                }


            sta_assert_error ( sizeof ( CTriangleObject::CVertex ) ==sizeof ( float ) *3 );

            glEnable ( GL_CULL_FACE );
            glCullFace ( GL_BACK );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );

            mhs_check_gl;
            }






        void createMesh_particles ( mhs::dataArray<T> & treedata,
                                    mhs::dataArray<T> & conncection_data,
                                    Vector<std::size_t,3> shape,
                                    int mode=0,
				    CData<T,TData,Dim> *    data_fun=NULL,
				    int colormode=-1,
				    bool hide_nunconnected_particles=true,
				    bool hide_connected_particles=false 
                                  )
            {
            printf ( "init mesh mode %d | ... ",mode );
	    
	    


            std::size_t v_buffer_size=0;
            std::size_t i_buffer_size=0;

  



            CTriangleObject disk_particle_mesh ( CTriangleObject::object_id_disk,resolution );
            CTriangleObject disk_particle_mesh_segment ( CTriangleObject::object_id_disk2,resolution );
            CTriangleObject sphere_particle_mesh ( CTriangleObject::object_id_sphere,resolution );
            CTriangleObject cylinder_particle_mesh ( CTriangleObject::object_id_cylinder,resolution );
	    CTriangleObject data_particle_mesh ( CTriangleObject::object_id_sphere,resolution);

            if ( mode==5 )
                {
                hide_nunconnected_particles=false;
                hide_connected_particles=true;
                }
                
                
                
                
                



            float rescale=1.0/std::max ( std::max ( shape[0],shape[1] ),shape[2] );

            buffered_objects=NULL;


            try
                {
                sta_assert_error ( treedata.data!=NULL )
                    {

                    T * ptreedata2=treedata.data;
                    unsigned int numpoints=treedata.dim[0];




// 		std::vector<int> path_count;
// 		path_count.resize(unique_path_ids.size());



// 	  if (false)
// 	  {
// 		std::vector<int> debug_vec;
// 		debug_vec=path_remap_all;
// 		std::sort (debug_vec.begin(), debug_vec.end());
// 		it= std::unique (debug_vec.begin(), debug_vec.end());   // 10 20 30 20 10 ?  ?  ?  ?
// 		debug_vec.resize( std::distance(debug_vec.begin(),it) );
//
// 		sta_assert_error(debug_vec.size()==numpoints);
// 	    }
// 		num_buffered_object_paths=unique_path_ids.size();
// 		buffered_object_paths=new CMesh[num_buffered_object_paths];
// 		for (std::size_t  i=0; i<unique_path_ids.size(); i++)
// 		{
// 		  buffered_object_paths[i].i_offset=
// 		}

// 		printf("?: %d %d\n",numpoints,remap);
                    ptreedata2=treedata.data;






// 	      if (tree!=NULL)
// 		delete tree;

// 	      std::size_t * myshape=shape.data();
// 	      float ext_max=std::max ( std::max ( shape[0],shape[1] ),shape[2] );
// 	      tree=new OctTreeNode<float,3>(shape.v,ext_max/100);


                    std::size_t * num_connections=NULL;
                    //if ((mode==2)||(mode==3)||(mode==4))
                        {
                        num_connections=new std::size_t[numpoints];

                        for ( int i=0; i<numpoints; i++ )
                            {
                            num_connections[i]=0;
                            }

                        T * pconncection_data=conncection_data.data;


                        unsigned int num_edges=conncection_data.dim[0];
                        sta_assert_error ( conncection_data.dim[1]== ( CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_Count ) );
                        for ( int i=0; i<num_edges; i++ )
                            {
                            int pointidxA=pconncection_data[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXA];
                            int pointidxB=pconncection_data[CTracker<float,T,Dim>::EDGE_ATTRIBUTES::EDGE_A_PINDXB];
                            sta_assert_error ( pointidxA<numpoints );
                            sta_assert_error ( pointidxB<numpoints );

                            num_connections[pointidxA]++;
                            num_connections[pointidxB]++;

                            pconncection_data+=conncection_data.dim[1];
                            }

                        }

                    printf("found %d points\n",numpoints);
                        
                    std::vector<bool> valid;
                    valid.resize ( numpoints );
                    //if (!tree->insert(*opoint))
                    std::size_t num_particles=0;
// 	      std::size_t num_terminals=0;
// 	      std::size_t num_bifurcations=0;
                    for ( int i=0; i<numpoints; i++ )
                        {
                        T * ptreedata=ptreedata2+i*treedata.dim[1];

                        valid[i]=false;
                    


                        //hide_nunconnected_particles
                        if ( ( !hide_nunconnected_particles ) || ( ( ( !hide_connected_particles ) && ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]>0 ) ) ) )
                            {

                            unsigned int type= ( ( unsigned int ) ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );

                            CTriangleObject * meshp=&disk_particle_mesh_segment;
                            if ( type== ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) )
                                meshp=&sphere_particle_mesh;
                            
//printf("num_connections: %d\n",num_connections[i]);
                            
                            
                            if ( num_connections[i]!=2 )
                                {
                                meshp=&disk_particle_mesh;
                                }
// 		    if (num_connections[i]==2)
// 		    {
// 		      sta_assert_error(meshp=&disk_particle_mesh_segment);
// // 		     printf("yuu\n");
// 		    }


                            switch ( mode )
                                {
                                case 1:
                                    {
                                    if ( type!= ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) )
                                        {
// 			   ptreedata+=treedata.dim[1];
                                        continue;
                                        }
                                    }
                                break;
                                case 2:
                                    {
                                    sta_assert_error ( num_connections!=NULL );
                                    if ( ( num_connections[i]!=1 ) )
// 			if ((num_connections[i]!=1)&&(num_connections[i]!=2))
                                        {
// 			   ptreedata+=treedata.dim[1];
                                        continue;
                                        }
                                    if ( num_connections[i]==2 )
                                        {
                                        meshp=&sphere_particle_mesh;
                                        }
                                    }
                                break;
                                case 3:
                                    {
                                    sta_assert_error ( num_connections!=NULL );
                                    if ( num_connections[i]!=1 )
                                        {
// 			   ptreedata+=treedata.dim[1];
                                        continue;
                                        }
                                    }
                                break;
                                case 4:
                                    {
                                    sta_assert_error ( num_connections!=NULL );
                                    if ( num_connections[i]!=1 )
                                        {
// 			   ptreedata+=treedata.dim[1];
                                        continue;
                                        }
                                    }
                                break;
				 case 6:
                                    {
				      sta_assert_error ( num_connections!=NULL );
                                    if (( num_connections[i]<1 )||
					( num_connections[i]>2 ))
                                        {
                                        continue;
                                        }
                                    }
                                break;
				 case 7:
                                    {
				      sta_assert_error ( num_connections!=NULL );
                                    if (( num_connections[i]<1 )||
					( num_connections[i]>2 )||
				       (data_fun==NULL))
                                        {
                                        continue;
                                        }
                                        meshp=&data_particle_mesh;
                                    }
                                break;
				
                                }





                            CTriangleObject & mesh=*meshp;



                            v_buffer_size+=mesh.getNumVerts();
                            i_buffer_size+=mesh.getNumIndeces();

                            valid[i]=true;
                            num_particles++;
                            }
                        //ptreedata+=treedata.dim[1];
                        }

                    if ( num_particles<1 )
                        {
			   printf(" no particles\n");
                        if ( num_connections!=NULL )
                            delete [] num_connections;
                        return;
                        }

      
		      

 
 		    std::vector<std::size_t> path_remap;
		    std::vector<std::size_t> path_remap_ids;
		    std::vector<std::size_t> index_array_counts;
// 		    std::vector<int> path_ids_unsorted;
		    std::size_t total_unique_paths=0;
		    
                    if ( path_sorting )
                        {
		      total_unique_paths=sort ( treedata,	
			    conncection_data,
			    path_remap,path_remap_ids,index_array_counts,//path_ids_unsorted,
			    false,
			    &valid);

			}else
                        {
                        path_remap.resize ( num_particles );
                        std::size_t remap=0;
                        for ( std::size_t  j=0; j<numpoints; j++ )
                            {
                            if ( valid[j] )
                                {
                                path_remap[remap]=j;
                                remap++;
                                }
                            }
                        }



                    num_buffered_objects=num_particles;
                    buffered_objects=new CMesh[num_particles];


                    particle_centers.resize ( num_particles );

                    if ( path_sorting )
                        particle_path_id.resize ( num_particles );


//                 ptreedata=treedata.data;


                    printf ( "vertex buffer size %u MB\n", (( 2*v_buffer_size*sizeof ( CTriangleObject::CVertex )  + v_buffer_size*sizeof ( CTriangleObject::Vertex<unsigned char> )) /1024)/1024 );
                    printf ( "index  buffer size %u MB\n", (( 3*i_buffer_size*sizeof ( index_type ) ) /1024)/1024 );

                    CTriangleObject::CVertex * tmp_buffer=new CTriangleObject::CVertex[v_buffer_size];
                    CTriangleObject::CVertex * tmp_nbuffer=new CTriangleObject::CVertex[v_buffer_size];
                    CTriangleObject::Vertex4<unsigned char> * tmp_cbuffer=new CTriangleObject::Vertex4<unsigned char>[v_buffer_size];



                    CTriangleObject::CVertex * tmp_buffer_p= tmp_buffer;
                    CTriangleObject::CVertex * tmp_nbuffer_p= tmp_nbuffer;
                    CTriangleObject::Vertex4<unsigned char> * tmp_cbuffer_p= tmp_cbuffer;

                    index_type * face_buffer=new index_type[i_buffer_size*3];
                    /*
                    for (int i=0;i<i_buffer_size*3;i++)
                      face_buffer[i]=-1;*/

                    index_type *  tmp_face_buffer_p= face_buffer;
                    std::size_t index_offset=0;



                    //sta_assert_error(treedata.dim[1]==POINT_ATTRIBUTES::POINT_A_Count);
                    //TODO add this check again. temporarily removed
                    sta_assert_error ( treedata.dim[1]> ( CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG ) );
		    
		    
		       T max_data=-std::numeric_limits<T>::max();
		      T min_data=std::numeric_limits<T>::max();
		      T max_abs=1;
		    if (mode==7)
		    {
		      printf ("computing data min and max :");
		      
		   
		      Points<T,3> data_point;
		      for ( std::size_t particle_count=0; particle_count<num_particles; particle_count++ )
		      {
			
			T * ptreedata=ptreedata2+particle_count*treedata.dim[1];
					      
			unsigned int type= ( ( unsigned int ) ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );
			  if ( type!= ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) )
			  {
			  
			  
			      float scale= ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_SCALE] );

			      Vector<float,3> direction ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRX],
					  ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRY],
					  ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRZ] );
					  
			      Vector<float,3> position ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX],
					ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
					ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ] );
                  
                  
                  float costs = ptreedata[CTracker<float,T,Dim>::POINT_A_COST];
                        
					
				T value=0;
				data_point.set_pos_dir_scale(position,direction,scale);
				
				
				
 				if (data_fun->eval_data(
 				  value,
 				  data_point))
				//if (data_fun->data_score ( value,direction,position,scale))
				{
				  
// 				 printf("value :%f\n",value); 
// 				 position.print();
// 				 direction.print();
				max_data=std::max(max_data,-value);
				min_data=std::min(min_data,-value);
				}           
				Vector<float,3> vn1;
				Vector<float,3> vn2;
				mhs_graphics::createOrths(direction,vn1, vn2);
				
				data_point.set_direction(vn1);
				if (data_fun->eval_data(
 				  value,
 				  data_point))
				{
				max_data=std::max(max_data,-value);
				min_data=std::min(min_data,-value);
				}
				data_point.set_direction(vn2);
				if (data_fun->eval_data(
 				  value,
 				  data_point))
				{
				max_data=std::max(max_data,-value);
				min_data=std::min(min_data,-value);
				}           
				
				
			  }                             
                        
                        }
                        
                         printf ("[%f %f] \n",min_data,max_data);
			 //max_abs=std::max(std::abs(max_data),std::abs(min_data));
			 max_abs=std::abs(max_data);
		    }
		    
		    
		    

                    Vector<float,Dim> rot_ax;
                    rot_ax=float ( 0 );

		    
		    
		    
		 
		    
		    std::vector<std::size_t> path_lengths;
		    std::size_t max_path_length=0;
		    std::size_t min_path_length=1000000000000000;
		    path_lengths.resize(numpoints);
		    
		    std::vector<int> rand_color_code;
		    rand_color_code.resize(numpoints);
		    for (std::size_t i=0;i<numpoints;i++)
		    {
		      path_lengths[i]=0;
		      
		    }
		    std::size_t max_path_id=0;
		    
		      
			if (path_sorting)
			{
			  
			  //std::srand (1);
			   //std::srand (std::time(NULL));

			  for ( std::size_t particle_count=0; particle_count<num_particles; particle_count++ )
			  {
		      

			      sta_assert_error_c ( particle_count<path_remap.size(),printf("%u %u\n",particle_count,path_remap.size()));      
			      std::size_t i=path_remap[particle_count];
			      sta_assert_error ( i<numpoints );
			  
			    std::size_t path_id=path_remap_ids[particle_count];
			    sta_assert_error ( path_id<numpoints );
			    path_lengths[path_id]++;
			    max_path_length=std::max(path_lengths[path_id],max_path_length);
			    
			      if (colormode==3)
			      {
				 sta_assert_error ( path_id<numpoints );
				//rand_color_code[path_id]=std::rand();
				 rand_color_code[path_id]=path_id;//std::rand();
				 max_path_id=std::max(max_path_id,path_id);
			      }
			    
			  }
			  
// 			  for (std::size_t t=0;t<max_path_id;t++)
// 			  {
// 			    std::size_t perm=std::rand() % max_path_id;
// 			    std::swap(rand_color_code[t],rand_color_code[perm]);
// 			    
// 			  }
			if (max_path_id>3)
			{
			  for (std::size_t t=0;t<max_path_id;t++)
			  {
			    std::size_t perm=std::rand() % (max_path_id-t)+t;
			    if (perm<max_path_id)
			    {
			      std::swap(rand_color_code[t],rand_color_code[perm]);
			    }
			  }
			}
			  
// 			   for (std::size_t t=0;t<max_path_id;t++)
// 			  {
// 			   printf("%d %d \n",t,rand_color_code[t]); 
// 			  }
			  
			  min_path_length=path_lengths[0];
			  
			    
			  
			  
			}
		    
		  
                    std::size_t indeces_count=0;
                    printf ( "initializing points ... " );
		    mhs::mex_dumpStringNOW();    
                    //for (int i=0; i<num_particles; i++)
                    for ( std::size_t particle_count=0; particle_count<num_particles; particle_count++ )
                        {

// 			  if (particle_count%(num_particles/1000)==0)
// 			  {
// 			    std::size_t p=particle_count/(num_particles/1000);
// 			    if (p<11)
// 			    {
// 			      printf("%d \% \n",p*10);
// 			     mhs::mex_dumpStringNOW(); 
// 			    }
// 			  }
			
 		    

			sta_assert_error_c ( particle_count<path_remap.size(),printf("%u %u\n",particle_count,path_remap.size()));      
                        std::size_t i=path_remap[particle_count];


// 		    if (!(path_id>-1))
// 		      printf("%d\n",path_id);
// 		    sta_assert_error(path_id>-1);
                        sta_assert_error ( i<numpoints );
			

			
			
                        T * ptreedata=ptreedata2+i*treedata.dim[1];

// 		  if ((!hide_nunconnected_particles)||(((ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]>0))))
                        if ( ( !hide_nunconnected_particles ) || ( ( ( !hide_connected_particles ) && ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]>0 ) ) ) )
                            {
			      
			      

                            unsigned int type= ( ( unsigned int ) ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );

// 		    if (type==(static_cast< unsigned int>(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)))
// 		      printf("bif center %d\n",type);

                            float scale= ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_SCALE] );



                            Vector<float,3> direction ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRX],
                                                        ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRY],
                                                        ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRZ] );
                            
                             float costs = ptreedata[CTracker<float,T,Dim>::POINT_A_COST];


                            Vector<float,3> color;
                            CTriangleObject * meshp=&disk_particle_mesh_segment;

                            if ( num_connections[i]!=2 )
                                {
                                meshp=&disk_particle_mesh;
                                }

                            bool patrticle_is_sphere=false;
			    bool blob_particles=false;

                            switch ( mode )
                                {
                                case 1:
                                    {
                                    if ( type!= ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) )
                                        {
					  sta_assert_error(false);
                                        continue;
                                        }
                                    scale=1;
                                    int color_id=CSceneRenderer::Cred;
                                    color=CSceneRenderer::particle_colors[color_id];
                                    }
                                break;
                                case 2:
                                    
                                    {
                                    if ( ( num_connections[i]!=1 ) )
                                        {
					  sta_assert_error(false);
                                        continue;
                                        }
                                    if ( num_connections[i]==1 )
                                        {

                                        scale*=0.3f;
                                        scale=std::max ( std::min ( 0.5f,scale ),scale );
                                        int color_id=CSceneRenderer::Cred;
                                        color=CSceneRenderer::particle_colors[color_id];
                                        }
                                    else
                                        {
                                        scale*=0.4;
                                        int color_id=CSceneRenderer::Cyellow;
                                        color=CSceneRenderer::particle_colors[color_id];
                                        meshp=&sphere_particle_mesh;
                                        patrticle_is_sphere=true;
                                        }
                                    }
                                break;
                                case 3:
                                    
                                    {
                                    if ( num_connections[i]!=1 )
                                        {
					  sta_assert_error(false);
                                        continue;
                                        }
                                    int color_id=CSceneRenderer::Cyellow;
                                    color=CSceneRenderer::particle_colors[color_id];
                                    }
                                break;
                                case 4:
                                    {
                                    if ( num_connections[i]!=1 )
                                        {
					  sta_assert_error(false);
                                        continue;
                                        }
                                    
                                    color=dir2color ( direction );
                                    }
                                break;
				 case 6:
                                    {
					sta_assert_error ( num_connections!=NULL );
                                    if (( num_connections[i]<1 )||
					( num_connections[i]>2 ))
                                        {
                                        continue;
                                        }
					scale*=0.4f;
                                        //scale=std::max ( std::min ( 0.5f,scale ),scale );
                                        blob_particles=true;
                                    }
                                break;
				 case 7:
                                    {
					sta_assert_error ( num_connections!=NULL );
					
                                    if (( num_connections[i]<1 )
					||( num_connections[i]>2 )
					//||( type== ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER )))
				       )
                                        {
                                        continue;
                                        }
					
					scale*=1; //NOTE don\'t change, otherwise data_eval may be out of scale
                                        blob_particles=true;
					
                                    }
                                break;
                                default:
                                    {
                                    int color_id=CSceneRenderer::Cred;
                                    color=CSceneRenderer::particle_colors[color_id];

                                    }
                                }

                            Vector<float,3> position ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX],
                                                       ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
                                                       ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ] );




// 		    if (img_shape==NULL)
// 		    {
// 		      for (int a=0;a<3;a++)
// 		      {
// 			shape[a]=std::max((std::size_t)std::ceil(position[a]),shape[a]);
// 		      }
// 		    }












                            float thickness=scale;

                            Matrix<float,4> rotation;
			    if (blob_particles)
			    {
				int color_id=CSceneRenderer::Cyellow;
                                color=CSceneRenderer::particle_colors[color_id];
				meshp=&sphere_particle_mesh;
				if (mode==7)
				{
				  meshp=&data_particle_mesh;
				}
				
				
                                rotation.Identity();

                                thickness=scale;

			    }else 
                            if ( type==static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) )
                                {
                                meshp=&sphere_particle_mesh;
                                rotation.Identity();

                                //scale*=0.4;
                                thickness=scale;

                                int color_id=CSceneRenderer::Corange;
                                color=CSceneRenderer::particle_colors[color_id];

                                }
                                
                            else
                                {
                                Vector<float,Dim> & n=direction;
                                sta_assert_error ( n.norm2() >0 );
                                rot_ax[0]=n[1];
                                rot_ax[1]=-n[0];
                                rot_ax.normalize();
                                float angle=-180*std::acos ( n[2] ) /M_PI;
                                rotation=Matrix<float,4>::OpenGL_glRotate ( angle,rot_ax );

                                if ( !patrticle_is_sphere )
				{
                                    //thickness*=0.25;
				  thickness*=0.5;
				    //thickness=std::max(thickness,0.5f);
				}
				
				
				
// 			color=CSceneRenderer::particle_colors[color_id];
                                }

                            CTriangleObject & mesh=*meshp;

                            Vector<float,3> scalev ( scale,scale,thickness );


                            scales[0]=std::min ( scales[0],std::min ( scale,thickness ) );
                            scales[1]=std::max ( scales[1],std::max ( scale,thickness ) );



                            mesh.cpy2VertexBuffer ( tmp_buffer_p,tmp_nbuffer_p );
                            std::size_t num_verts=mesh.getNumVerts();
                            std::size_t num_indeces=mesh.getNumIndeces();

 			    Points<T,3> data_point;
			    
			    if (( path_sorting )&&(colormode>-1))
                                    {
				      sta_assert_error ( particle_count<path_remap_ids.size() );    
				      std::size_t path_id=path_remap_ids[particle_count];
				      
				      
				      
				      switch  (colormode)
				      {
					case 2:
					{
					  
// 					  float pc=float((23124354*path_id)%120+path_id%13)/(120.0f+13);
					  int mpaths=(total_unique_paths > 180 ? 180 : total_unique_paths);
					  float pc=float(path_id%mpaths)/T(mpaths+1);
					  hsv2rgb<float> ( float ( pc*360.0f ) ,(0.3f+0.7f*pc),(0.8f+0.2f*pc), color[0],color[1],color[2]); 
					} 
					break;
					
					case 3:
					{
					 sta_assert_error ( path_id<numpoints );
					 
				      
					  
// 					  float pc=float((23124354*path_id)%120+path_id%13)/(120.0f+13);
					  int mpaths=(total_unique_paths > 180 ? 180 : total_unique_paths);
					  //float pc=float((rand_color_code[path_id])%128)/T(128);
					  std::size_t div=std::max(std::min(max_path_id+1,std::size_t(360)),std::size_t(1));
					  
					  if (max_path_id<4)
					  {
					    div=std::max(std::min(4,360),(1));  
					  }
					  
					  float pc=float((rand_color_code[path_id])%div)/T(div);
					  
					  float br=float((rand_color_code[path_id]+path_id)%100)/T(100);
					  
					  float sat=float(path_lengths[path_id]-min_path_length)/(max_path_length-min_path_length+1);
					  hsv2rgb<float> ( float ( pc*360.0f ) ,(0.8f+0.2f*sat),(0.8f+0.2f*br), color[0],color[1],color[2]); 
					  
					  //printf("%f %f %f %u %u %u\n",pc,br,sat,div,max_path_id,rand_color_code[path_id]);
					  
					} 
					break;
					
					default:
					{
					float pc=float(path_lengths[path_id]-min_path_length)/(max_path_length-min_path_length+1);
 					hsv2rgb<float> ( float ( pc*360.0f ) ,(0.3f+0.7f*pc),(0.8f+0.2f*pc), color[0],color[1],color[2]); 
					}
				      }
					//hsv2rgb<float> ( float ( pc*360.0f ) ,0.75f,1.0f, color[0],color[1],color[2]); 
// 				      color*=pc;
// 				      
// 					tmp_cbuffer_p[v].r=c[0]*255;
// 					tmp_cbuffer_p[v].g=c[1]*255;
// 					tmp_cbuffer_p[v].b=c[2]*255;
				    
				    
				    }
			    
			    
			    //#pragma omp parallel for num_threads(omp_get_num_procs())
			    //#pragma omp parallel for num_threads(8)
                            for ( std::size_t v=0; v<num_verts; v++ )
                                {
// 				 printf("%d %d \n",v,num_verts);
                                Vector<float,3> vert= ( tmp_buffer_p[v].position );
				//Vector<float,3> pos= ( position+rotation.multv3 ( vert*scalev ) ) *rescale;
			    
                                Vector<float,3> pos;//= ( position+rotation.multv3 ( vert*scalev ) ) *rescale;
			    
if ((mode==7)&&(true))
//if ((mode==7)&&(false))
				{
				  T value=0.1;
				  T result=0.5;
				  
				  Vector<float,3> dir=vert;
				  dir.normalize();
				  //data_point.set_pos_dir_scale(position,direction,scale);
				  
				  data_point.set_pos_dir_scale(position,dir,scale);
				  
// 				  printf("%d :-)",type);
				  if (data_fun->eval_data(
				    result,
				    data_point))
				  {
				    value=datascale*std::max((-result-min_data)/(max_data-min_data),T(0));
				  }
				  tmp_cbuffer_p[v].r=(value)*255;
				  tmp_cbuffer_p[v].g=(0.5-value)*128+128;
				  tmp_cbuffer_p[v].b=(1-value)*255;
                  tmp_cbuffer_p[v].a=costs*255;

				  value=(0.05+0.95*value)*datascale;
				  pos= ( position+rotation.multv3 ( vert*scalev*value*0.25 ) ) *rescale; 
				}else
				{
// 				  printf("[%d] -\n",v);
				    
				    {
					tmp_cbuffer_p[v].r=color[0]*255;
					tmp_cbuffer_p[v].g=color[1]*255;
					tmp_cbuffer_p[v].b=color[2]*255;
                    tmp_cbuffer_p[v].a=costs*255;
				    }
				  pos= ( position+rotation.multv3 ( vert*scalev ) ) *rescale; 
				}
			    
                                tmp_buffer_p[v].position[0]=pos[0];
                                tmp_buffer_p[v].position[1]=pos[1];
                                tmp_buffer_p[v].position[2]=pos[2];

if (mode!=7)
				{
				  vert= ( tmp_nbuffer_p[v].position );
				  Vector<float,3> n_new=rotation.multv3 ( vert );
//                                   if (mode==3)
//                                   {
//                                     n_new.print();  
//                                   }
				  tmp_nbuffer_p[v].position[0]=n_new[0];
				  tmp_nbuffer_p[v].position[1]=n_new[1];
				  tmp_nbuffer_p[v].position[2]=n_new[2];
				}
                                }
                                
//if ((mode==7)&&(false))
if ((mode==7)&&(true))
				{
				  
				  //#pragma omp parallel for num_threads(omp_get_num_procs())
				  //#pragma omp parallel for num_threads(8)
				  for ( std::size_t v=0; v<num_verts; v++ )
				  {
				    Vector<float,3> N;
				    N=T(0);
				    std::list<Vector<std::size_t,3>> *  vn=mesh.get_nlist(v);
				    if (vn!=NULL)
				    {
				    
				      for (std::list<Vector<std::size_t,3>>::iterator iter=vn->begin();iter!=vn->end();iter++)
				      {
					Vector<std::size_t,3> & face=*iter;
  // 				      face.print();
					
// 					sta_assert_error(face[0]>-1);
// 					sta_assert_error(face[1]>-1);
// 					sta_assert_error(face[2]>-1);
					sta_assert_error(face[0]<num_verts);
					sta_assert_error(face[1]<num_verts);
					sta_assert_error(face[2]<num_verts);
					
					Vector<float,3> p0= ( tmp_buffer_p[face[0]].position );
					Vector<float,3> p1= ( tmp_buffer_p[face[1]].position );
					Vector<float,3> p2= ( tmp_buffer_p[face[2]].position );
					
					Vector<float,3> test= ( tmp_buffer_p[v].position );
					
					//printf("%d %d %d\n",(p0-test).norm2()<0.01,(p1-test).norm2()<0.01,(p2-test).norm2()<0.01);
					//printf("%d %d %d\n",(face[0]==v),(face[1]==v),(face[2]==v));
					
					Vector<float,3> n0=p1-p0;
					n0.normalize();
					
					Vector<float,3> n1=p2-p0;
					n1.normalize();
					
					Vector<float,3> n2=n0.cross(n1);
					
					n2.normalize();
					N+=n2;
				      }
				      N.normalize();
				      N=N*(-1);
				    }else
				    {
				      
				    }
 				    
 				    tmp_nbuffer_p[v].position[0]=N[0];
 				    tmp_nbuffer_p[v].position[1]=N[1];
 				    tmp_nbuffer_p[v].position[2]=N[2];
				  }
				}

// 		    if (i>=particle_centers.size())
// 		      printf("%d %d %d\n",i,particle_centers.size(),numpoints);

                                {

                                sta_assert_error ( particle_count<particle_centers.size() );
                                particle_centers[particle_count].position=position;
                                particle_centers[particle_count].direction=direction;
                                particle_centers[particle_count].scaleA=scale;
                                particle_centers[particle_count].scaleB=thickness;


                                buffered_objects[particle_count].i_offset=3*indeces_count;
                                buffered_objects[particle_count].i_length=3* ( i_buffer_size-indeces_count );

                                indeces_count+=num_indeces;

                                if ( path_sorting )
                                    {
				     sta_assert_error ( particle_count<path_remap_ids.size() );    
                                    std::size_t path_id=path_remap_ids[particle_count];
// 		    sta_assert_error(path_id>-1);
//                                     sta_assert_error ( unique_path_ids.size() >path_id );
                                    index_array_counts[path_id]+= ( num_indeces );
                                    particle_path_id[particle_count]=path_id;
				    particle_centers[particle_count].path_id=path_id;
                                    }

// 		     int particle_path_id=((int)(ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_PATH_ID]));
// 		    if (particle_path_id==10)
// 		      printf("%d\n",particle_path_id);

                                }

// 		    buffered_object_paths[]

// 		    particle_count++;


                            mesh.cpy2IndexBuffer ( tmp_face_buffer_p,index_offset );

                            index_offset+=num_verts;

                            tmp_buffer_p+=num_verts;
                            tmp_nbuffer_p+=num_verts;
                            tmp_cbuffer_p+=num_verts;
                            tmp_face_buffer_p+=num_indeces *3;
                            }
// 		    ptreedata+=treedata.dim[1];
                        }

                    printf ( "done\n" );

// 		 printf("%d of %d particles used\n",particle_count,num_particles);




                    if ( num_buffered_objects>0 )
                        {

                        sta_assert_error ( buffered_objects[0].i_offset==0 );
                        sta_assert_error_c ( buffered_objects[0].i_length==i_buffer_size*3,printf("%u %u\n",buffered_objects[0].i_length,i_buffer_size*3));

			printf("copying to gpu:");
			mhs::mex_dumpStringNOW();

                        glGenBuffers ( 1,&vertex_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_buffer, GL_STATIC_DRAW );

                        glGenBuffers ( 1,&normal_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_nbuffer, GL_STATIC_DRAW );

                        glGenBuffers ( 1,&color_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, color_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::Vertex4<unsigned char> ) *v_buffer_size, tmp_cbuffer, GL_STATIC_DRAW );




                        glGenBuffers ( 1,&index_buffer );
                        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
                        glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 3*sizeof ( index_type ) *i_buffer_size, face_buffer, GL_STATIC_DRAW );
			
			printf("done\n");


                        if ( path_sorting )
                            {
                            std::size_t used_paths=0;
// 			    sta_assert_error(path_ids_unsorted.size()==index_array_counts.size());
                            for ( std::size_t a=0; a<index_array_counts.size(); a++ )
                                {
                                if ( index_array_counts[a]>0 )
                                    used_paths++;
                                }
                            printf ( "%d of %d paths used\n",used_paths,total_unique_paths );
                            //sta_assert_error_c ( path_ids_unsorted.size() ==used_paths,printf("%u %u\n",path_ids_unsorted.size() ,used_paths));

                            num_buffered_object_paths=used_paths;
                            buffered_object_paths=new CMesh[num_buffered_object_paths];

                            used_paths=0;
                            std::size_t indeces_count=0;
                            for ( std::size_t a=0; a<index_array_counts.size(); a++ )
                                {
                                if ( index_array_counts[a]>0 )
                                    {

                                    buffered_object_paths[used_paths].i_offset=indeces_count*3;
                                    buffered_object_paths[used_paths].i_length= ( i_buffer_size-indeces_count ) *3;
                                    indeces_count+=index_array_counts[a];
// 			    printf("%d %d\n",buffered_object_paths[used_paths].i_offset,buffered_object_paths[used_paths].i_length);
                                    used_paths++;
                                    }
                                }
                            }


                        }

                    delete [] tmp_cbuffer;
                    delete [] tmp_buffer;
                    delete [] tmp_nbuffer;
                    delete [] face_buffer;

                    if ( num_connections!=NULL )
                        delete [] num_connections;
                    mhs_check_gl;

// 		buffered_objects[0].i_offset=0;
// 		buffered_objects[0].i_length=i_buffer_size*3;



                    }
                }
            catch ( mhs::STAError & error )
                {

                throw error;
                }


            sta_assert_error ( sizeof ( CTriangleObject::CVertex ) ==sizeof ( float ) *3 );

            glEnable ( GL_CULL_FACE );
            glCullFace ( GL_BACK );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );

            mhs_check_gl;

            }

            
            
            
             void createMesh_spines ( mhs::dataArray<T> & treedata,
                                    mhs::dataArray<T> & conncection_data,
                                    Vector<std::size_t,3> shape,
                                    int mode=0,
				    CData<T,TData,Dim> *    data_fun=NULL
                                  )
            {
            printf ( "(spnes) init mesh mode %d ... ",mode );


            std::size_t v_buffer_size=0;
            std::size_t i_buffer_size=0;



            CTriangleObject spine_particle_mesh ( CTriangleObject::object_id_spine,resolution );


            float rescale=1.0/std::max ( std::max ( shape[0],shape[1] ),shape[2] );

            buffered_objects=NULL;

            try
                {
                sta_assert_error ( treedata.data!=NULL )
                    {

                    T * ptreedata2=treedata.data;
                    unsigned int numpoints=treedata.dim[0];

                    ptreedata2=treedata.data;


                    std::vector<bool> valid;
                    valid.resize ( numpoints );
                    std::size_t num_particles=0;
                    for ( int i=0; i<numpoints; i++ )
		    {
		    T * ptreedata=ptreedata2+i*treedata.dim[1];

		    valid[i]=false;

		    unsigned int type= ( ( unsigned int ) ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );
		    //hide_nunconnected_particles
		    if ((  ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]<1 ) &&
			( type== ( static_cast< unsigned int> ( PARTICLE_TYPES::PARTICLE_BLOB ) ) ))
			{

			CTriangleObject * meshp=&spine_particle_mesh;

			CTriangleObject & mesh=*meshp;



			v_buffer_size+=mesh.getNumVerts();
			i_buffer_size+=mesh.getNumIndeces();

			valid[i]=true;
			num_particles++;
			}
		    }

                    if ( num_particles<1 )
		    {
		      printf(" no particles\n");
		    return;
		    }else 
		    {
		      printf(" (%u particles ) ...",num_particles);
		    }

                    num_buffered_objects=num_particles;
                    buffered_objects=new CMesh[num_particles];

                    particle_centers.resize ( num_particles );

                    printf ( "vertex buffer size %u MB\n", (( 2*v_buffer_size*sizeof ( CTriangleObject::CVertex )  + v_buffer_size*sizeof ( CTriangleObject::Vertex<unsigned char> )) /1024)/1024 );
                    printf ( "index  buffer size %u MB\n", (( 3*i_buffer_size*sizeof ( index_type ) ) /1024)/1024 );

                    CTriangleObject::CVertex * tmp_buffer=new CTriangleObject::CVertex[v_buffer_size];
                    CTriangleObject::CVertex * tmp_nbuffer=new CTriangleObject::CVertex[v_buffer_size];
                    CTriangleObject::Vertex4<unsigned char> * tmp_cbuffer=new CTriangleObject::Vertex4<unsigned char>[v_buffer_size];


                    CTriangleObject::CVertex * tmp_buffer_p= tmp_buffer;
                    CTriangleObject::CVertex * tmp_nbuffer_p= tmp_nbuffer;
                    CTriangleObject::Vertex4<unsigned char> * tmp_cbuffer_p= tmp_cbuffer;

                    index_type * face_buffer=new index_type[i_buffer_size*3];

                    index_type *  tmp_face_buffer_p= face_buffer;
                    std::size_t index_offset=0;



                    //sta_assert_error(treedata.dim[1]==POINT_ATTRIBUTES::POINT_A_Count);
                    //TODO add this check again. temporarily removed
                    sta_assert_error ( treedata.dim[1]> ( CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG ) );
		    
                    Vector<float,Dim> rot_ax;
                    rot_ax=float ( 0 );

		  
                    std::size_t indeces_count=0;
                    printf ( "initializing points ... " );
		    mhs::mex_dumpStringNOW();    
		    std::size_t particle_count=0;
                    //for ( std::size_t particle_count=0; particle_count<num_particles; particle_count++ )
		      
			for ( std::size_t i=0; i<numpoints; i++ )
                        {
			 if (!valid[i]) 
			   continue;
                        //std::size_t i=particle_count;
			  
                        sta_assert_error ( particle_count<numpoints );
                        T * ptreedata=ptreedata2+i*treedata.dim[1];
			{
                            unsigned int type= ( ( unsigned int ) ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_TYPE] ) );

                            float scale= ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_SCALE] );



                            Vector<float,3> direction ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRX],
                                                        ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRY],
                                                        ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DIRZ] );


                            Vector<float,3> color;
                            CTriangleObject * meshp=&spine_particle_mesh;

			    int color_id=CSceneRenderer::Cred;
                            color=CSceneRenderer::particle_colors[color_id];


                            Vector<float,3> position ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX],
                                                       ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
                                                       ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ] );

                            
                            float costs = ptreedata[CTracker<float,T,Dim>::POINT_A_COST];
			    
			    scale*=0.5;
			    
			    float distance= ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_DISTANCE] );
			    
			    //float thickness=std::max(distance,1.0f);
			    
			    
  			    
			    float thickness=std::max(distance/2,1.0f);
  			    position=position-direction*distance/2;
			    
			    
			    
			    Matrix<float,4> rotation;
			    
			    {
				Vector<float,Dim> & n=direction;
				sta_assert_error ( n.norm2() >0 );
				rot_ax[0]=n[1];
				rot_ax[1]=-n[0];
				rot_ax.normalize();
				float angle=-180*std::acos ( n[2] ) /M_PI;
				rotation=Matrix<float,4>::OpenGL_glRotate ( angle,rot_ax );
			    }

                            CTriangleObject & mesh=*meshp;

                            Vector<float,3> scalev ( scale,scale,thickness );

                            mesh.cpy2VertexBuffer ( tmp_buffer_p,tmp_nbuffer_p );
                            std::size_t num_verts=mesh.getNumVerts();
                            std::size_t num_indeces=mesh.getNumIndeces();

 			    Points<T,3> data_point;
                            for ( std::size_t v=0; v<num_verts; v++ )
                                {
				      Vector<float,3> vert= ( tmp_buffer_p[v].position );
				      Vector<float,3> pos;//= ( position+rotation.multv3 ( vert*scalev ) ) *rescale;
				  

				      {
					tmp_cbuffer_p[v].r=color[0]*255;
					tmp_cbuffer_p[v].g=color[1]*255;
					tmp_cbuffer_p[v].b=color[2]*255;
                    tmp_cbuffer_p[v].a=costs*255;
					pos= ( position+rotation.multv3 ( vert*scalev ) ) *rescale; 
				      }
				  
				      tmp_buffer_p[v].position[0]=pos[0];
				      tmp_buffer_p[v].position[1]=pos[1];
				      tmp_buffer_p[v].position[2]=pos[2];

				      {
					vert= ( tmp_nbuffer_p[v].position );
					Vector<float,3> n_new=rotation.multv3 ( vert );
					tmp_nbuffer_p[v].position[0]=n_new[0];
					tmp_nbuffer_p[v].position[1]=n_new[1];
					tmp_nbuffer_p[v].position[2]=n_new[2];
				      }
                                }
                                

                                {

                                sta_assert_error ( particle_count<particle_centers.size() );
                                particle_centers[particle_count].position=position;
                                particle_centers[particle_count].direction=direction;
                                particle_centers[particle_count].scaleA=scale;
                                particle_centers[particle_count].scaleB=thickness;


                                buffered_objects[particle_count].i_offset=3*indeces_count;
                                buffered_objects[particle_count].i_length=3* ( i_buffer_size-indeces_count );

                                indeces_count+=num_indeces;
                                }


                            mesh.cpy2IndexBuffer ( tmp_face_buffer_p,index_offset );

                            index_offset+=num_verts;

                            tmp_buffer_p+=num_verts;
                            tmp_nbuffer_p+=num_verts;
                            tmp_cbuffer_p+=num_verts;
                            tmp_face_buffer_p+=num_indeces *3;
                            }
                            
                            
                            particle_count++;
                        }

                    printf ( "done\n" );


                    if ( num_buffered_objects>0 )
                        {

                        sta_assert_error ( buffered_objects[0].i_offset==0 );
                        sta_assert_error_c ( buffered_objects[0].i_length==i_buffer_size*3,printf("%u %u\n",buffered_objects[0].i_length,i_buffer_size*3));

			printf("copying to gpu:");
			mhs::mex_dumpStringNOW();

                        glGenBuffers ( 1,&vertex_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_buffer, GL_STATIC_DRAW );

                        glGenBuffers ( 1,&normal_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_nbuffer, GL_STATIC_DRAW );

                        glGenBuffers ( 1,&color_buffer );
                        glBindBuffer ( GL_ARRAY_BUFFER, color_buffer );
                        glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::Vertex4<unsigned char> ) *v_buffer_size, tmp_cbuffer, GL_STATIC_DRAW );




                        glGenBuffers ( 1,&index_buffer );
                        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
                        glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 3*sizeof ( index_type ) *i_buffer_size, face_buffer, GL_STATIC_DRAW );
			
			printf("done\n");

                        }

                    delete [] tmp_cbuffer;
                    delete [] tmp_buffer;
                    delete [] tmp_nbuffer;
                    delete [] face_buffer;
                    mhs_check_gl;


                    }
                }
            catch ( mhs::STAError & error )
                {

                throw error;
                }


            sta_assert_error ( sizeof ( CTriangleObject::CVertex ) ==sizeof ( float ) *3 );

            glEnable ( GL_CULL_FACE );
            glCullFace ( GL_BACK );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );

            mhs_check_gl;

            }
            
            



    public:

        void draw(bool transparent=false)
            {
            if ( num_buffered_objects<1 )
                return;

            enable_VBO();
	    
	    if ((path_selection==0)&&(!transparent))
	    {
	      int object_id=0;
	      std::size_t offset=buffered_objects[object_id].i_offset;
	      glDrawElements ( GL_TRIANGLES, buffered_objects[object_id].i_length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
	      mhs_check_gl;
	    }else 
	    {
	      
	      if (transparent)
	      {
		if ( path_selection>1)
		{
		std::size_t offset=buffered_objects[0].i_offset;
		std::size_t offset2=buffered_objects[path_selection-1].i_offset;
		std::size_t length=buffered_objects[path_selection-1].i_length-offset2;

		sta_assert_error ( offset+length<=buffered_objects[0].i_length );
		sta_assert_error ( offset>=0);
		//printf("tans: %u %u %u | %u\n",offset,offset2,length,path_selection);
		glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );		  
		mhs_check_gl;
		}
	      }else{
		if ( path_selection<num_buffered_object_paths)
		  {

		  std::size_t offset=buffered_object_paths[path_selection].i_offset;
		  std::size_t length=buffered_object_paths[path_selection].i_length;
		  sta_assert_error ( offset+length<=buffered_objects[0].i_length );

		  //printf("rest: %u %u\n",offset,length);
		  glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
		  mhs_check_gl;
		  }
	      }
	      
	    }
            disable_VBO();

            }
            
        

        void draw_non_selected ( bool path=false,bool transparent=false)
            {
	   
	   
	      
            if ( num_buffered_objects<1 )
                return;
	    
	   

            if (( selection<0 ) || (path_selection!=0)) // NOTE: we currently  not cope with path_selections here correctly, so we skip@ this
                {

                draw(transparent);
                return;
                }

            
            int current_selection=selection;

            path=path && ( particle_path_id.size() >0 );

            if ( path )
                {
                current_selection=particle_path_id[current_selection];
                //printf("%d %d %d %d\n", current_selection,num_buffered_objects,num_buffered_object_paths,path);
                }

            // printf("%d %d\n", current_selection,num_buffered_object_paths);
            sta_assert_error_m ( ( current_selection<num_buffered_objects ) || ( path ),current_selection );
            sta_assert_error_m ( ( current_selection<num_buffered_object_paths ) || ( !path ),current_selection );

            enable_VBO();

            //if (( current_selection>0 ) && ((path_selection==0)))
		//if (( current_selection>0 ) && ((path_selection<current_selection)))
	    
	    if (((path_selection==0)||(!path)))
	    {
		if ( current_selection>0 ) 
                {
                std::size_t offset=path ? buffered_object_paths[0].i_offset : buffered_objects[0].i_offset;
                std::size_t length=path ? buffered_object_paths[current_selection].i_offset : buffered_objects[current_selection].i_offset;

                sta_assert_error ( offset+length<=buffered_objects[0].i_length );
                glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
                mhs_check_gl;
                }


		if ( current_selection< ( path ? num_buffered_object_paths : num_buffered_objects )-1 )
                {
                std::size_t offset=path ? buffered_object_paths[current_selection+1].i_offset : buffered_objects[current_selection+1].i_offset;
                std::size_t length=path ? buffered_object_paths[current_selection+1].i_length : buffered_objects[current_selection+1].i_length;
                sta_assert_error ( offset+length<=buffered_objects[0].i_length );
                glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
                mhs_check_gl;
                }
	    }else
	    {
		
		if (( current_selection>0 ) && (current_selection < path_selection))
                {
                std::size_t offset=buffered_object_paths[path_selection].i_offset;
                std::size_t length=buffered_object_paths[current_selection].i_offset-offset;

                sta_assert_error ( offset+length<=buffered_objects[0].i_length );
                glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
                mhs_check_gl;
                }

		if (( current_selection< ( num_buffered_object_paths )-1 ) )
                {
		std::size_t remaining_paths=std::max((int)path_selection,current_selection)+1;
                std::size_t offset=buffered_object_paths[remaining_paths].i_offset;
                std::size_t length=buffered_object_paths[remaining_paths].i_length;
                sta_assert_error ( offset+length<=buffered_objects[0].i_length );
                glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
                mhs_check_gl;
                }
	    }
            disable_VBO();
            }

        void draw_selected ( Vector<float,3> color,bool path=false )
            {
	     if ( num_buffered_objects<1 )
                return;
	     
	     
	     
	      if (always_transparent)
		return;
// 	      {
//   
// // 		enable_VBO();
// // 		      int object_id=0;
// // 		      std::size_t offset=buffered_objects[object_id].i_offset;
// // 		      glDrawElements ( GL_TRIANGLES, buffered_objects[object_id].i_length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
// // 		      mhs_check_gl;
// // 		  disable_VBO();
// 		draw();
// 		return;
// 	      }

	      
	      


            if ( selection<0 )
                {
                return;
                }
                
            if ( selection<path_selection) 
       	    {
       	     return; 
       	    }

            path=path && ( particle_path_id.size() >0 );


            int current_selection=selection;


            if ( path )
                {
                current_selection=particle_path_id[current_selection];
//       printf("selection: %d\n",current_selection);
                }

            sta_assert_error ( ( current_selection<num_buffered_objects ) || ( path ) );
            sta_assert_error ( ( current_selection<num_buffered_object_paths ) || ( !path ) );

            enable_VBO ( true,false );
            glColor3fv ( color.v );

//       std::size_t offset=buffered_objects[current_selection].i_offset;
//       std::size_t length=buffered_objects[current_selection].i_length;
            std::size_t offset=path ? buffered_object_paths[current_selection].i_offset : buffered_objects[current_selection].i_offset;
            std::size_t length=path ? buffered_object_paths[current_selection].i_length : buffered_objects[current_selection].i_length;

            if ( path )
                {
                if ( current_selection<num_buffered_object_paths-1 )
                    length=buffered_object_paths[current_selection].i_length-buffered_object_paths[current_selection+1].i_length;
                }
            else
                {
                if ( current_selection<num_buffered_objects-1 )
                    length=buffered_objects[current_selection].i_length-buffered_objects[current_selection+1].i_length;
                }

//        printf("%d %d\n", offset,length);
            sta_assert_error ( offset+length<=buffered_objects[0].i_length );
            glDrawElements ( GL_TRIANGLES, length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
            mhs_check_gl;

            disable_VBO();
            }


        CModelMesh ( mhs::dataArray<T> & treedata,
                     mhs::dataArray<T> & conncection_data,
                     Vector<std::size_t,3> shape,
                     MESH id,
                     int lod=-1,bool path_sorting=false,
		     CData<T,TData,Dim> *    data_fun=NULL,
		     float datascale=1
 		  )
            {
	      this->datascale=datascale;
	      
	      always_transparent=false;
	    enabled=true;

            this->path_sorting=path_sorting;
//      tree=NULL;
            buffered_objects=NULL;
            selection=-1;
	    path_selection=0;
            num_buffered_objects=0;
            resolution=0;
            num_buffered_object_paths=0;
            buffered_object_paths=NULL;

            unsigned int numpoints=treedata.dim[0];
	    if ( numpoints<500000 )
                resolution=1;
            if ( numpoints<200000 )
                resolution=2;
            if ( numpoints<50000 )
                resolution=3;

            if ( lod>-1 )
                {
                switch ( lod )
                    {
                    case 0:
                        resolution=0;
                        break;
                    case 1:
                        resolution=1;
                        break;
		    case 2:
                        resolution=2;
                        break;
                    default:
                        resolution=3;
                        break;
                    }
                }

            scales[0]=std::numeric_limits<float>::max();
            scales[1]=0;
            try
                {
                switch ( id )
                    {
                    case MESH::MESH_CENTERLINE:
                        {
                        createMesh_centerline2 ( treedata,conncection_data,shape,2 );
                        }
                    break;
// 		    case MESH::MESH_CENTERLINE_GRAY:
//                         {
//                         createMesh_centerline2 ( treedata,conncection_data,shape,2,10);
//                         }
//                     break;
                    case MESH::MESH_PARTICLES:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,0,NULL,3  );
                        }
                    break;
                    case MESH::MESH_CENTERLINE_PARTICLE:
                        {
                        createMesh_centerline2 ( treedata,conncection_data,shape,1,3 );
                        }
                    break;
                    case MESH::MESH_SURFACE:
                        {
                        createMesh_centerline2 ( treedata,conncection_data,shape,0 );
                        }
                    break;
		     case MESH::MESH_SURFACE_UNIQUE:
                        {
                        createMesh_centerline2 ( treedata,conncection_data,shape,0,3 );
                        }
                    break;
                    case MESH::MESH_BIFURCATIONS:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,1 );
                        }
                    break;
                    case MESH::MESH_TERMINALS:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,2 );
                        }
                    break;
                    case MESH::MESH_SURFACE_TERMINALS:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,3 );
                        }
                    break;
		     case MESH::MESH_TERMINALS_UNIQUE:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,3,NULL,3 );
                        }
                    break;
                    case MESH::MESH_FIBER_CENTERLINES:
                        {
                        createMesh_centerline2 ( treedata,conncection_data,shape,4 );
                        }
                    break;
                    case MESH::MESH_FIBER_TERMINALS:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,4 );
                        }
                    break;
                    case MESH::MESH_SINGLE_PARTICLES:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,5 );
                        }
                    break;
                    case MESH::MESH_BLOB_PARTICLES:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,6,NULL,1 );
                        }
                    break;    
                    case MESH::MESH_DATA_VIS:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,7,data_fun);
                        }    
                    break;    
                    case MESH::MESH_DATA_CENTERLINE:
                        {
                        createMesh_centerline2 ( treedata,conncection_data,shape,5 );
                        }    
                    break;
		    case MESH::MESH_DATA_SPINE:
                        {
			this->path_sorting=false;
			always_transparent=true;
                        createMesh_spines ( treedata,conncection_data,shape,0 );
                        }    
                    break;
		      case MESH::MESH_BLOB_PARTICLES_UNIC:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,6,NULL );
                        }
                    break; 
		     case MESH::MESH_DATA_VIS2:
                        {
			   this->path_sorting=false;
                        createMesh_particles ( treedata,conncection_data,shape,7,data_fun,-1,false,false);
                        }    
                    break;   
		      case MESH::MESH_BLOB_PARTICLES_UNIC2:
                        {
                        createMesh_particles ( treedata,conncection_data,shape,6,NULL,2 );
                        }
                    break; 
                    }
                }
            catch ( mhs::STAError & error )
                {

                throw error;
                }

            if ( num_buffered_objects>0 )
                printf ( "number of triangles: %u\n",buffered_objects[0].i_length );
            }


        ~CModelMesh()
            {
            if ( num_buffered_objects>0 )
                {

                if ( buffered_objects!=NULL )
                    delete [] buffered_objects;

                if ( buffered_object_paths!=NULL )
                    delete [] buffered_object_paths;
                // if (tree!=NULL) delete tree;


                mhs_check_gl;
                glDeleteBuffers ( 1, &index_buffer );
                mhs_check_gl;
                glDeleteBuffers ( 1, &vertex_buffer );
                mhs_check_gl;
                glDeleteBuffers ( 1, &normal_buffer );
                mhs_check_gl;
                glDeleteBuffers ( 1, &color_buffer );
                mhs_check_gl;
                }
            }

        void enable_VBO ( bool normal=true,bool color=true )
            {

            glEnableClientState ( GL_VERTEX_ARRAY );
            glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
            glVertexPointer ( 3, GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );

            if ( normal )
                {
                glEnableClientState ( GL_NORMAL_ARRAY );
                glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                glNormalPointer ( GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );
                }
            if ( color )
                {
                glEnableClientState ( GL_COLOR_ARRAY );
                glBindBuffer ( GL_ARRAY_BUFFER, color_buffer );
//                 glColorPointer ( 3, GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );
            //glColorPointer ( 3, GL_UNSIGNED_BYTE, sizeof ( CTriangleObject::Vertex<unsigned char> ), 0 );
            glColorPointer ( 4, GL_UNSIGNED_BYTE, sizeof ( CTriangleObject::Vertex4<unsigned char> ), 0 );
                }

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
            }

        void disable_VBO()
            {
            glDisableClientState ( GL_NORMAL_ARRAY );
            glDisableClientState ( GL_VERTEX_ARRAY );
            glDisableClientState ( GL_COLOR_ARRAY );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );


            }


        static Vector<std::size_t,3> estimate_shape ( mhs::dataArray<T> & treedata )
            {
            Vector<std::size_t,3> shape;
            shape=std::size_t ( 0 );

            try
                {
                sta_assert_error ( treedata.data!=NULL )

                T * ptreedata=treedata.data;
                unsigned int numpoints=treedata.dim[0];
                printf ( "num pts: %d\n",numpoints );

                for ( int i=0; i<numpoints; i++ )
                    {

                    Vector<float,3> position ( ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSX],
                                               ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSY],
                                               ptreedata[CTracker<float,T,Dim>::POINT_ATTRIBUTES::POINT_A_POSZ] );

                    for ( int a=0; a<3; a++ )
                        {
                        shape[a]=std::max ( ( std::size_t ) std::ceil ( position[a] ),shape[a] );
                        }

                    ptreedata+=treedata.dim[1];
                    }
                }
            catch ( mhs::STAError & error )
                {

                throw error;
                }

            return shape;
            }
    };



template<typename TData,typename T, int Dim>
class MyViewerMouse: public Mouse<TData,T,Dim>
    {
      
    private:
//       float path_length_threshold;
      
    public:
        float dist_clip = 0;
        

        enum class RENDERER_MODE : unsigned int
        {
            RENDERER_MODE_PARTICLE,
            RENDERER_MODE_CENTERLINE,
            RENDERER_MODE_SURFACE,
            RENDERER_MODE_FIBERS,
            RENDERER_MODE_ALL_PARTICLE,
	    RENDERER_MODE_BLOBS,
	    RENDERER_MODE_DATA,
	    RENDERER_MODE_SPINES,
	    RENDERER_MODE_DATA2,
	    RENDERER_MODE_BLOBS2,
	    RENDERER_MODE_SURFACE_UQ,
            RENDERER_MODE_Count
        };

        RENDERER_MODE render_mode;
	RENDERER_MODE render_mode_old;


        enum class POINT_SELECTION_MODE : unsigned int
        {
            POINT_SELECTION_SINGLE,
            POINT_SELECTION_CONNECTED,
        };

        POINT_SELECTION_MODE point_selection_mode;
	

        MyViewerMouse ( CTrackViewer<TData,T,Dim> * viewer=NULL ) : Mouse<TData,T,Dim> ( viewer )
            {
//         render_mode=RENDERER_MODE::RENDERER_MODE_PARTICLE;
//             render_mode=RENDERER_MODE::RENDERER_MODE_FIBERS;
	      render_mode=RENDERER_MODE::RENDERER_MODE_BLOBS;
	      
	      
	      render_mode=RENDERER_MODE::RENDERER_MODE_SURFACE_UQ;
//render_mode=RENDERER_MODE::RENDERER_MODE_DATA;	      
	      
	      render_mode_old=RENDERER_MODE::RENDERER_MODE_Count;
	    
            point_selection_mode=POINT_SELECTION_MODE::POINT_SELECTION_SINGLE;
// 	    path_length_threshold=0;
            }


        bool  ui()
            {


            if ( !Mouse<TData,T,Dim>::ui() )
                {
                return false;
                }

            CTrackViewer<TData,T,Dim> & V=* ( ( CTrackViewer<TData,T,Dim> * ) ( Mouse<TData,T,Dim>::viewer ) );
	    
	    
	     //if (( Mouse<TData,T,Dim>::mouse_wheel!=0 )&&(Mouse<TData,T,Dim>::keys[SDL_SCANCODE_SPACE])) {
	    if (( Mouse<TData,T,Dim>::mouse_wheel!=0 )) {
	       int speed=   (Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F1])
			  + (Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F2])*10
			  + (Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F3])*100
			  + (Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F4])*1000			  
			  + (Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F5])*10000;
	       
	       if (speed>0)
	       {
		 
		 	if ( Mouse<TData,T,Dim>::mouse_wheel<0 ) {
// 			    V.path_length_threshold*=0.9;
			  V.extra_options[0]+=speed;
			} else {
			  V.extra_options[0]-=speed;
			}
			V.extra_options[0]=std::max(V.extra_options[0],0.0f);
			printf("%f\n",V.extra_options[0]);
	       }else 
           {
                if (Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F6])
                {
                    if (Mouse<TData,T,Dim>::mouse_wheel<0)
                    {        
                        this->dist_clip=std::min(1.1f,this->dist_clip+0.005f);   
                    }else
                    {
                        this->dist_clip=std::max(0.0f,this->dist_clip-0.005f);      
                    }
                    printf("dist_clip : %f\n",this->dist_clip);
                    V.extra_options[1] = this->dist_clip;
                    mhs::mex_dumpStringNOW();
                }
               
           }
        
	       
	    }

            //processing keyboard input
                {
                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_1]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_1] ) )
                    {
		    render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_PARTICLE;
                    }

                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_2]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_2] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_CENTERLINE;
                    }
                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_3]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_3] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_SURFACE;
                    }
                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_4]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_4] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_FIBERS;
                    }
                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_5]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_5] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_ALL_PARTICLE;
                    }
		if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_6]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_6] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_BLOBS;
                    }    
               if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_7]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_7] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_DATA;
                    }
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_8]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_8] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_SPINES;
                    }     
                     if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_9]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_9] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_DATA2;
                    }     
                    
                       if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_0]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_0] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_BLOBS2;
                    }     
                    
                      if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_MINUS]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_MINUS] ) )
                    {
		      render_mode_old=render_mode;
                    render_mode=RENDERER_MODE::RENDERER_MODE_SURFACE_UQ;
                    }    
                    

                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_S]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_S] ) )
                    {
                    if ( point_selection_mode==POINT_SELECTION_MODE::POINT_SELECTION_SINGLE )
                        point_selection_mode=POINT_SELECTION_MODE::POINT_SELECTION_CONNECTED;
                    else
                        point_selection_mode=POINT_SELECTION_MODE::POINT_SELECTION_SINGLE;
                    }
                    
//                     if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_Z]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_Z] ) )
//                     {
//                     //V.del_cam_pos();
// 		      V.reset_current_trafo();
//                     }    
                    

                if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_R]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_R] ) )
                    {
                    unsigned int r= V.get_rendering() +1;
                    V.set_rendering ( r );

                    }
                    
		    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_H]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_H] ) )
                    {
                    
                    V.toggle_center();

                    }
                    
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_L]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_L] ) )
                    {
                    
                    V.toggle_trackvis();

                    }    
                    
                    
                 if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_F]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_F] ) )
                    {
                    
                    V.save_screen();

                    }    
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_A]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_A] ) )
                    {
                    //V.add_cam_pos();
		      V.cam->add_point();
                    }    
		    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_D]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_D] ) )
                    {
                    //V.del_cam_pos();
		      V.cam->remove_point();
                    }    
                    
                    


                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_W]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_W] ) )
                    {
		      if (V.cam->cam_is_flying())
			V.cam->stop_flight(); 
			else
			  V.cam->start_flight(); 
                    }
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_E]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_E] ) )
                    {
		      if (V.cam->cam_is_flying())
			V.cam->stop_flight(); 
			else
			  V.cam->start_flight(false); 
                    }
                    
//                     if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_M]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_M] ) )
//                     {
//                     
//                      //V.render_mode=(V.render_mode+1)%3+1;
//                     printf("render mode old %d\n",V.render_mode);
//                     //V.render_mode=(V.render_mode+1)%4+1;
//                      //V.render_mode++;
//                     // V.render_mode%=5;
//                      printf("render mode new %d\n",V.render_mode);
// 
//                     }
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_N]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_N] ) )
                    {
                    
                    //V.mesh_render_mode=(V.mesh_render_mode+1)%3+1;
                        V.mesh_render_mode=(V.mesh_render_mode+1)%5;
                        //V.mesh_render_mode=(V.mesh_render_mode)%4+1;
                        printf("render mode %d\n",V.mesh_render_mode);

                    }
                    
                    
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_Y]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_Y] ) )
                    {
		      if (V.movie_folder.length()==0)
		      {
			printf("no movie folder set!\n");
		      }else 
		      {
			if (V.recording_flight)
			{
			  V.cam->stop_flight();
			  V.recording_flight=false;
			}else
			{
			  V.frame_flight=0;
			  V.cam->stop_flight();
			  V.cam->start_flight(false); 
			  V.recording_flight=true;
			}
		      }
                    }    
                    
                    if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_U]&& ( !Mouse<TData,T,Dim>::keystates[SDL_SCANCODE_U] ) )
                    {
		      if (V.movie_folder.length()==0)
		      {
			printf("no movie folder set!\n");
		      }else
			V.recording_user=!V.recording_user;
			if (V.recording_user)
			  printf("recording user interaction\n");
			else 
			  printf("stopped recording user interaction\n");
                    }    
                    
                    
                }


            /// mouse interaction
            if ( ( !Mouse<TData,T,Dim>::transition_on ) )
                {
                for ( int b=0; b<3; b++ )
                    {
                    if ( Mouse<TData,T,Dim>::b_down[b] )
                        {
                        if ( !Mouse<TData,T,Dim>::b_down_old[b] )   // mouse_down_event
                            {
                            if ( !Mouse<TData,T,Dim>::is_busy() )
                                {
                                switch ( b )
                                    {
                                    case 0:
                                        {
                                        if ( Mouse<TData,T,Dim>::keys[SDL_SCANCODE_LCTRL] && (!V.no_particles))
                                            {
                                            Mouse<TData,T,Dim>::point_select_mode=0;
                                            Mouse<TData,T,Dim>::point_select_on=true;
                                            Mouse<TData,T,Dim>::mouse_pos_old[0]=-1;
                                            Mouse<TData,T,Dim>::mouse_pos_old[1]=-1;
// 				    printf("partilcle cursor on\n");
                                            }

                                        }
                                    break;
                                    }
                                }
                            }
                        else     //  mouse_is_down
                            {
                            if ( ( Mouse<TData,T,Dim>::point_select_on ) && ( !Mouse<TData,T,Dim>::arcball_on ) && ( !Mouse<TData,T,Dim>::trackball_on ) && ( !Mouse<TData,T,Dim>::trackballZ_on ) )
                                {

                                if ( Mouse<TData,T,Dim>::point_select_mode==0 )
                                    {

                                    if ( ( Mouse<TData,T,Dim>::mouse_pos_old[0]!=Mouse<TData,T,Dim>::mouse_pos[0] ) && ( Mouse<TData,T,Dim>::mouse_pos_old[1]!=Mouse<TData,T,Dim>::mouse_pos[1] ) )
                                        {

                                        V.particle_hittest ( Mouse<TData,T,Dim>::mouse_pos[0],Mouse<TData,T,Dim>::mouse_pos[1],Mouse<TData,T,Dim>::focus_point,Mouse<TData,T,Dim>::focus_point_img );
                                        Mouse<TData,T,Dim>::mouse_pos_old[0]=Mouse<TData,T,Dim>::mouse_pos[0];
                                        Mouse<TData,T,Dim>::mouse_pos_old[1]=Mouse<TData,T,Dim>::mouse_pos[1];
                                        }
                                    }
                                }
                            }
                        }
                    else if ( Mouse<TData,T,Dim>::b_down_old[b] )     // mouse_up_event
                        {
                        if ( b==0 )
                            {
                            if ( ( Mouse<TData,T,Dim>::point_select_on ) )
                                {
                                if ( Mouse<TData,T,Dim>::point_select_mode==0 )
                                    {
                                    }
// 				printf("partilcle cursor off\n");
                                }
                            }

                        }
                    }
                }

            return true;
            }
    };






const char *viewer_field_names[] = {
    "cam",		
};
const std::size_t n_viewer_field_names=  ( sizeof(viewer_field_names) / sizeof(viewer_field_names[ 0 ]) );;


template<typename TData,typename T, int Dim>
class CTrackViewer: public CViewer<TData,T,Dim>
    {

      
	friend class MyViewerMouse<TData,T,Dim>;

        std::vector<CModelMesh<T,TData>* > meshes;
        mhs::dataArray<T> * p_treedata;
        mhs::dataArray<T> * p_conncection_data;
	CData<T,TData,Dim> *    data_fun;
        Vector<std::size_t,3> img_shape;

        std::vector<CRoi* > roi_meshes;
	
// 	 std::size_t    path_length_threshold;

//   Shader * shader;

        bool no_particles;
        bool nice;
        unsigned int aliasing;
        int lod;
        bool sorting;
	bool center;
	bool show_tracks;
	
	//int render_mode=1;   
	
	int mesh_render_mode=1;

	std::size_t rand_counter=0;
	
	float datascale;

        GLint glsl_roi_projection;
        GLint glsl_roi_modelview;
        GLint glsl_roi_normalmat;
        GLint glsl_roi_clipping_plane;
	GLint glsl_roi_render_mode;
    
    
        Shader *roi_shader;
	bool take_screenshot;
       CCam<TData,T,Dim> * cam;
       std::string movie_folder;
       std::size_t frame_flight;
       std::size_t frame_user;
       bool recording_flight;
       bool recording_user;
       
        mhs::CtimeStopper timer;
	double prev_time=0;
	
	float transparency=0;

//   int selected_particle;
	//std::string screenshot_fname;
	std::size_t screenshot_fname_count=0;



        
        SDL_Renderer *renderer = NULL;
        SDL_Texture *texTarget =NULL;
        

         GLuint texColorBuffer;
         GLuint rboDepthStencil;
         GLuint frameBuffer;
         //GLuint vboQuad;
         
        GLuint tex_colorBuffer;
        GLuint tex_depthBuffer;
        GLuint tex_mfbo;
        GLuint tex_texture1;
        GLuint tex_fbo;
         
        GLint  glsl_tex_texture;
        GLint  glsl_pixelsize;
        GLint  glsl_kernel_scale;
        GLint glsl_tex_projection;
        GLint glsl_tex_modelview;
        //GLint glsl_tex_normalmat;
        Shader *tex_shader;
/*        
        GLuint tex_sbo= 0;
        GLuint tex_depthTexture;
        */
        //float tex_kernel_scale[2]={0.25f,0.25f};
        
        
          void init_tex_shader()
            {
            tex_shader=NULL;
            try
                {
                tex_shader=new Shader ( application_directory+"/tex_render_vshader.glsl",
                                        application_directory+"/tex_render_pshader.glsl" );
                
                 glsl_tex_texture=tex_shader->get_variable ( "glsl_tex_texture" );
                 mhs_check_gl
                 glsl_pixelsize=tex_shader->get_variable ( "glsl_pixelsize" );
                 mhs_check_gl
                 glsl_kernel_scale=tex_shader->get_variable ( "glsl_kernel_scale" );
                 mhs_check_gl
                 

//                 glsl_tex_projection=roi_shader->get_variable ( "glsl_projection" );
//                 mhs_check_gl
//                 glsl_tex_modelview=roi_shader->get_variable ( "glsl_modelview" );
//                 mhs_check_gl
               // glsl_tex_normalmat=roi_shader->get_variable ( "glsl_normalmat" );
               // mhs_check_gl
                
                
                }
            catch ( mhs::STAError error )
                {

                mhs::STAError error2;
                error2<<error.str() <<"\n"<<"could not create the shader tex object";
                throw error2;
                }
            }
        
        bool tex_rendering(){return ((this->tex_width>0)&&(this->tex_height>0));};
        
        
        
        
	void tex_init2()
        {
             int width=this->tex_width;
              int height=this->tex_height;
               if (tex_rendering())
                {
                        // multi sampled color buffer
                        glGenRenderbuffers(1, &tex_colorBuffer);
                        glBindRenderbuffer(GL_RENDERBUFFER, tex_colorBuffer);
                        glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4 /* 4 samples */, GL_RGBA8, width, height);
                        mhs_check_gl
                        // multi sampled depth buffer
                        glGenRenderbuffers(1, &tex_depthBuffer);
                        glBindRenderbuffer(GL_RENDERBUFFER, tex_depthBuffer);
                        glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4 /* 4 samples */, GL_DEPTH_COMPONENT, width, height);
                        mhs_check_gl
                        // create fbo for multi sampled content and attach depth and color buffers to it
                        glGenFramebuffers(1, &tex_mfbo);
                        glBindFramebuffer(GL_FRAMEBUFFER, tex_mfbo);
                        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, tex_colorBuffer);
                        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, tex_depthBuffer);
                        mhs_check_gl
                        // create texture
                        glGenTextures(1, &tex_texture1);
                        glBindTexture(GL_TEXTURE_2D, tex_texture1);
                        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
                        mhs_check_gl
                        
//                         glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//                         glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                        
                        
//                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
                        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
                        
                        glGenFramebuffers(1, &tex_fbo);
                        glBindFramebuffer(GL_FRAMEBUFFER_EXT, tex_fbo);
                        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_texture1, 0);
                        
                        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                            printf("init shadow buffer failed\n");
    
                        //shadow map
                  /*      
                        glGenFramebuffers(1, &tex_sbo);
                        glBindFramebuffer(GL_FRAMEBUFFER, tex_sbo);

                        // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
                        glGenTextures(1, &tex_depthTexture);
                        glBindTexture(GL_TEXTURE_2D, tex_depthTexture);
                        glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT16, width, height, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

                        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex_depthTexture, 0);

                        //glDrawBuffer(GL_NONE); // No color buffer is drawn to.

                        // Always check that our framebuffer is ok
                        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                            printf("init shadow buffer failed\n");
                         */
                        
                        
                        
                }
        }
        
        void tex_deinit2()
        {
            if (tex_rendering())
           {               
            glDeleteRenderbuffers(1,&tex_colorBuffer);
            mhs_check_gl;
            glDeleteRenderbuffers(1,&tex_depthBuffer);
            mhs_check_gl;
            glDeleteTextures(1,&tex_texture1);
            mhs_check_gl;
            glDeleteFramebuffers(1,&tex_mfbo);
           mhs_check_gl;
           
           
           //shadow mapping
           
//             glDeleteTextures(1,&tex_depthTexture);
//             mhs_check_gl;
//             glDeleteFramebuffers(1,&tex_sbo);
//             mhs_check_gl;
           }
        }
        
        
        void tex_init( )
        {
                tex_init2();
                return;
                int width=this->tex_width;
                int height=this->tex_height;
                
                if (tex_rendering())
                {
                    
                 printf("render texture size: %d %d\n",width,height)   ;
            
                glGenFramebuffers(1, &frameBuffer);
                glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
              
                //int width;
                //int height;
                //SDL_GetWindowSize(CSceneRenderer::tracker_data_p->tracker_window,&width,&height);
                
                
                mhs_check_gl
              

                glGenTextures(1, &texColorBuffer);
                glBindTexture(GL_TEXTURE_2D, texColorBuffer);

                glTexImage2D(
                    GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL
                );

                //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                
                //GL_DEPTH_ATTACHMENT
                glFramebufferTexture2D(
                    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer, 0
                );
                
                glGenRenderbuffers(1, &rboDepthStencil);
                glBindRenderbuffer(GL_RENDERBUFFER, rboDepthStencil);
                glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
                
                glFramebufferRenderbuffer(
                    GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rboDepthStencil
                );
                
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
                mhs_check_gl;
                
                if (glCheckFramebufferStatus(rboDepthStencil)!=GL_FRAMEBUFFER_COMPLETE)
                {
                    switch (glCheckFramebufferStatus(rboDepthStencil))
                    {
                        case   GL_FRAMEBUFFER_UNDEFINED:
                            printf ("GL_FRAMEBUFFER_UNDEFINED\n");
                            break;
                        case   GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                            printf ("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
                            break;
                        case   GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                            printf ("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
                            break;                            
                         case   GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                            printf ("GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n");
                            break;                                
                          case   GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                            printf ("GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n");
                            break;         
                          case   GL_FRAMEBUFFER_UNSUPPORTED:
                            printf ("GL_FRAMEBUFFER_UNSUPPORTED\n");
                            break;             
                            
                         
                          case   GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                            printf ("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
                            break;         
                          case   GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                            printf ("GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS\n");
                            break;                                             
                          default:
                              printf ("glCheckFramebufferStatus failed\n");
                    }
                    
                }
                
                }
//                  glBindBuffer(GL_ARRAY_BUFFER, vboQuad);
//                 glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
//                 mhs_check_gl;
//              renderer  = SDL_CreateRenderer(CSceneRenderer::tracker_data_p->tracker_window, -1,
//  		SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);      
//              mhs_check_gl
//              int width;
//             int height;
//             SDL_GetWindowSize(CSceneRenderer::tracker_data_p->tracker_window,&width,&height);
//             mhs_check_gl
//              
//              texTarget = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
// 		SDL_TEXTUREACCESS_TARGET, width,height);
             
        }
        
        void  tex_deinit()
       {
           tex_deinit2();
                return;
           
           
           if (tex_rendering())
           {               
            glDeleteRenderbuffers(1,&rboDepthStencil);
            mhs_check_gl;
            glDeleteTextures(1,&texColorBuffer);
            mhs_check_gl;
            glDeleteFramebuffers(1,&frameBuffer);
              mhs_check_gl;
//             glDeleteBuffers ( 1, &vboQuad );
//            mhs_check_gl;
           }

           
//            SDL_DestroyTexture(texTarget);
//            mhs_check_gl
//             SDL_DestroyRenderer(renderer);
//             mhs_check_gl
       }
       
       
	void det_screenshot_fname()
	{
	  
	  
	  bool found=false;
	  while ((!found)&&(screenshot_fname_count<1000))
	  {
	      std::stringstream s;
	      s<<"ntrackviewer_image_"<<1000+screenshot_fname_count<<".png";
	      if (!file_exists(s.str()))
	      {
		//printf("screenshot count: %d \n",screenshot_fname_count);
		found=true;
	      }else
	      {
	      screenshot_fname_count++;
	      }
	  }
	   
	   if (!found)
	   {
	    screenshot_fname_count=0; 
	   }
	    screenshot_fname_count+=1000;
	}
	
	
    public:
      
      
      
      
      void set_transparency(float t)
      {
	transparency=t;
      }
      
      bool is_flying()
      {
	if (cam!=NULL)
	return cam->fly();
	return false;
      }
      
      bool is_intransition()
      {
	 MyViewerMouse<TData,T,Dim> & mouse_state=* ( ( MyViewerMouse<TData,T,Dim>* )  CViewer<TData,T,Dim>::mouse_state );
	 return mouse_state.transition_on;
      }
      
      mxArray * save()
      {
	    sta_assert_error(cam!=NULL);
	    mwSize dim = 1;
	    mxArray * tdata=mxCreateStructArray(1,&dim,n_viewer_field_names,viewer_field_names);
	    mxArray * cam_data=cam->save();
  	    mxSetField(tdata,0,"cam", cam_data);
	    return tdata;
	    
      }
      
      
      void load(const mxArray * data)
      {
	  sta_assert_error(cam!=NULL);
	  try 
	  {
	  if (data==NULL)
	    return;
	  
	  mxArray * struct_array=mxGetField(data,0,"cam");
	  if (struct_array!=NULL)
	  {
	    printf("loading cam data\n");
	    cam->load(struct_array);
	  }
	 } catch (mhs::STAError & error)
            {
                throw error;
            }
      }

/*      
	void add_cam_pos()
	{
	    cam->add_point(*this);
	}
      
      void del_cam_pos()
	{
	    cam->remove_point();
	}
	
	 void fly()
	 {
	   if (cam->cam_is_flying())
	     cam->stop_flight(); 
	     else
	      cam->start_flight(); 
	 }*/
      
      
	void save_screen(){take_screenshot=true;};
      
        void set_rendering ( unsigned int  aliasing )
            {
            aliasing%=3;
            this->aliasing=aliasing;
            printf ( "aliasing: %d\n",aliasing );
            };
        unsigned int get_rendering()
            {
            return aliasing;
            };
        void set_lod ( int lod=-1 )
            {
            this->lod=lod;
            };
        void set_sorting ( bool sorting )
            {
            this->sorting=sorting;
            };
	    
	
	    

        void init ( const mxArray * feature_struct )
            {
            if ( feature_struct==NULL )
                {
                return;
                }

            printf ( "init CViewer\n" );
            CViewer<TData,T,Dim>::init ( feature_struct );

//         printf ( "updateing controls\n" );
//         update_tracker_controls ( true );
            printf ( "ok\n" );
            }


        bool particle_hittest ( GLdouble  winX,  GLdouble  winY,
                                Vector<float,3> & focus_point,
                                Vector<float,3> & focus_point_img )
            {

            MyViewerMouse<TData,T,Dim> & mouse_state=* ( ( MyViewerMouse<TData,T,Dim>* )  CViewer<TData,T,Dim>::mouse_state );
            CModelMesh<T,TData>* mesh=NULL;

            switch ( mouse_state.render_mode )
                {
                case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_PARTICLE ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_PARTICLES );
                    mesh=meshes[mesh_id];
                    }
                break;

                case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_CENTERLINE ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE );
// 	      unsigned int mesh_id=static_cast<unsigned int>(CModelMesh<T,TData>::MESH::MESH_TERMINALS);
                    mesh=meshes[mesh_id];
                    }
                break;

                case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SURFACE ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_SURFACE );
                    mesh=meshes[mesh_id];
                    }
                break;
		 case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SURFACE_UQ ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_SURFACE_UNIQUE );
                    mesh=meshes[mesh_id];
                    }
                break;
                case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_FIBERS ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_FIBER_CENTERLINES );
                    mesh=meshes[mesh_id];
                    }
                break;
                case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ALL_PARTICLE ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_PARTICLES );
                    mesh=meshes[mesh_id];
                    }
                break;
		case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_BLOBS ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BLOB_PARTICLES );
                    mesh=meshes[mesh_id];
                    }
                break;
                case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_DATA ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_VIS);
                    mesh=meshes[mesh_id];
                    }
                break;
		 case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SPINES ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BLOB_PARTICLES_UNIC);
                    mesh=meshes[mesh_id];
                    }
                break;
		case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_DATA2 ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_VIS2);
                    mesh=meshes[mesh_id];
                    }
                break;
		case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_BLOBS2 ) :
                    {
                    unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BLOB_PARTICLES_UNIC2 );
                    mesh=meshes[mesh_id];
                    }
                break;
                }

            if ( mesh==NULL )
                return false;

            return particle_hittest ( winX,  winY,focus_point,focus_point_img,*mesh ) ;

            }

        bool particle_hittest ( GLdouble  winX,  GLdouble  winY,
                                Vector<float,3> & focus_point,
                                Vector<float,3> & focus_point_img,
                                CModelMesh<T,TData> & mesh
                              )
            {
            //TODO ; consider cliplane

// 	 std::vector< Vector<float,3> >&  particles= mesh.get_particle_centers();
            std::vector< class CModelMesh<T,TData>::Ellipoid > &  particles= mesh.get_particle_centers();

            float * scales=mesh.get_scales();

// 	    printf("%u \n",particles.size());
	    
            if ( particles.size() ==0 )
                return false;

            Matrix<double,4> projection;
            Matrix<double,4> modelview;

            Vector<double,3> hit0;
            Vector<double,3> hit1;

            Vector<int,4> viewport;
            viewport[0]=0;
            viewport[1]=0;
            viewport[2]=this->width;
            viewport[3]=this->height;


            projection=CViewer<TData,T,Dim>::projection_mat;
            modelview= ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform );

            float rescale=1.0/std::max ( std::max ( CSceneRenderer::shape[0],CSceneRenderer::shape[1] ),CSceneRenderer::shape[2] );

            Vector<float,3>new_center;
            new_center[0]=rescale*CSceneRenderer::shape[0]/2.0f;
            new_center[1]=rescale*CSceneRenderer::shape[1]/2.0f;
            new_center[2]=rescale*CSceneRenderer::shape[2]/2.0f;
            Matrix<float,4> trafo;
            trafo=modelview;

            gluUnProject ( winX,this->height-winY,0.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit0.v[0],&hit0.v[1],&hit0.v[2] );
            gluUnProject ( winX,this->height-winY,1.0, ( const GLdouble * ) ( modelview.v ), ( const GLdouble * ) ( projection.v ), ( const GLint * ) ( viewport.v ), &hit1.v[0],&hit1.v[1],&hit1.v[2] );

            Vector<float,3> ray_start;
            Vector<float,3> ray_dir;
            Vector<float,3> ray_stop;
            //without clipping
//         ray_start=hit0;
//         ray_dir= ( hit1-hit0 );
            ray_start=hit0;
            ray_stop=hit1;
            ray_dir= ( hit1-hit0 );
            ray_dir.normalize();



            Vector<float,3> selection;
            Vector<float,3> selection_img;
            float dist=std::numeric_limits< float >::max();
// 	    float dist_sqrt=std::numeric_limits< float >::max();
// 	    float new_dist_sqrt;

            float start_dist=std::numeric_limits< float >::max();

            float eye_dist=std::numeric_limits< float >::max();
            float min_dist_sq=rescale*rescale*25*25;
            std::size_t index=0;

            Vector<float,3> nB ( 1,1,1 );
            nB.normalize();


                {
                std::size_t n_points=particles.size();
                Vector<float,Dim> new_pos;
                Points<T,Dim>  point;
		
// 		printf("%u \n",n_points);
		
                for ( unsigned int i=0; i<n_points; i++ )
                    {
		      
		    if (particles[i].path_id<this->extra_options[0])  
		    {
		      continue;
		    }
		    
                    Vector<float,3> & position=particles[i].position;
                    new_pos= ( position*rescale-new_center );

                    float ray_proj_dist= ( ( new_pos-ray_start ).dot ( ray_dir ) );

                    Vector<float,3> closet_ray_pt=ray_start+ray_dir*ray_proj_dist;

                    float tmp= ( closet_ray_pt[0]-new_pos[0] );


                    if ( ray_proj_dist<0 )
                        printf ( "%f\n",ray_proj_dist );





                    float new_dist=tmp*tmp;

                    if ( dist<new_dist )
                        {
                        continue;
                        }
                    tmp= ( closet_ray_pt[1]-new_pos[1] );
                    new_dist+=tmp*tmp;
                    if ( dist<new_dist )
                        {
                        continue;
                        }
                    tmp= ( closet_ray_pt[2]-new_pos[2] );
                    new_dist+=tmp*tmp;

                    if ( dist>new_dist )
                        {

                        float  & scaleA=particles[i].scaleA;
                        if ( scaleA>0 )
                            {
                            float  & scaleB=particles[i].scaleB;
                            float scale=std::max ( scaleA,scaleB ) *rescale;
                            //new_dist_sqrt=std::max(float(0),std::sqrt(new_dist)-scale);
                            //if ( dist_sqrt>new_dist_sqrt )
                            if ( new_dist<scale*scale )
                                {

                                Vector<float,3> pA=new_pos/rescale;
                                Vector<float,3> pB=closet_ray_pt/rescale;

                                bool collision= ( ellipsoids_coolide (
                                                      pA,
                                                      pB,
                                                      particles[i].direction,
                                                      nB,
                                                      scaleB,
                                                      scaleA,
                                                      scaleB,
                                                      scaleB ) );
// 				    scales[0],
// 				    scales[0]));
                                if ( collision )
                                    {
                                    float new_start_dist= ( ray_start-new_pos ).norm2();
//
                                    if ( new_start_dist<start_dist )
                                        {
                                        start_dist=new_start_dist;
                                        dist=new_dist;
// 				  dist_sqrt=new_dist_sqrt;
                                        selection=new_pos;
                                        selection_img=position;
                                        index=i;
                                        }

                                    }
                                }

                            }
                        else
                            {
                            dist=new_dist;
                            selection=new_pos;
                            selection_img=position;
                            index=i;
                            }
                        }

                    }
                }
            if ( dist<min_dist_sq )
                {
                focus_point=trafo.multv3 ( selection );
                focus_point_img=selection_img;
                //selected_particle=index;
                selection_img.print();
                mesh.set_selected_particle ( index );
                return true;
                }


            return false;

            }


        void add_roi ( const mxArray* mesh,int indx,Vector<float,4> color )
            {

            //CRoi(mxArray * mesh_data,int index,Vector<std::size_t,3> shape,Vector<float,4> color)
            CRoi * roi=new CRoi ( mesh,indx,img_shape,color );
            roi_meshes.push_back ( roi );
            printf ( "adding ROI\n" );
            }


        void init_shader()
            {
            roi_shader=NULL;
            try
                {
                roi_shader=new Shader ( application_directory+"/roi_render_vshader.glsl",
                                        application_directory+"/roi_render_pshader.glsl" );

                glsl_roi_projection=roi_shader->get_variable ( "glsl_projection" );
                mhs_check_gl
                glsl_roi_modelview=roi_shader->get_variable ( "glsl_modelview" );
                mhs_check_gl
                glsl_roi_normalmat=roi_shader->get_variable ( "glsl_normalmat" );
                mhs_check_gl
                glsl_roi_clipping_plane=roi_shader->get_variable ( "glsl_clipping_plane" );
                mhs_check_gl
                
                glsl_roi_render_mode=roi_shader->get_variable ( "glsl_roi_render_mode" );
		mhs_check_gl
                
                }
            catch ( mhs::STAError error )
                {

                mhs::STAError error2;
                error2<<error.str() <<"\n"<<"could not create the shader roi object";
                throw error2;
                }
            }
            
            

        CTrackViewer ( Vector<std::size_t,3> shape,
                       std::size_t * existing_handle=NULL )
            :  CViewer<TData,T,Dim> ( existing_handle )

            {

            sta_assert_error ( ( CViewer<TData,T,Dim>::mouse_state ) !=NULL );
            try
                {
                delete CViewer<TData,T,Dim>::mouse_state;
                CViewer<TData,T,Dim>::mouse_state= new MyViewerMouse<TData,T,Dim> ( this );

                CViewer<TData,T,Dim>::set_shape ( shape.v );

                p_treedata=NULL;
                p_conncection_data=NULL;

                img_shape=shape;
		
// 		path_length_threshold=0;

// 	try {
// 	shader=new Shader(application_directory+"/model_render_vshader.glsl",
// 	  application_directory+"/model_render_pshader.glsl");
// // 	shader->get_log();
// 	  mhs_check_gl
// 	 } catch ( mhs::STAError error ) {
//
//             mhs::STAError error2;
//             error2<<error.str() <<"\n"<<"could not create the object";
//             throw error2;
//         }
                no_particles=true;
                aliasing=2;
                lod=-1;
                sorting=false;

		take_screenshot=false;

                init_shader();
                init_tex_shader();
		
		cam = new CCam<TData,T,Dim>(shape,*this);
		
		frame_flight=0;
		frame_user=0;
		recording_flight=false;
		recording_user=false;
		center=true;
		 det_screenshot_fname();
		show_tracks=true;
		datascale=1;
                
               // tex_init( );
               // set_tex_rendering(500,500);
                }
            catch ( mhs::STAError error )
                {

                throw error;
                }






            }

        CTrackViewer ( mhs::dataArray<T> & treedata,
                       mhs::dataArray<T> & conncection_data,
		      CData<T,TData,Dim> * data_fun,
                       Vector<std::size_t,3> shape,
                       std::size_t * existing_handle=NULL,
		       float datascale=1
 		    )
            :  CViewer<TData,T,Dim> ( existing_handle )

            {

//       	selected_particle=-1;
            sta_assert_error ( ( CViewer<TData,T,Dim>::mouse_state ) !=NULL );
            try
                {
                delete CViewer<TData,T,Dim>::mouse_state;
                CViewer<TData,T,Dim>::mouse_state= new MyViewerMouse<TData,T,Dim> ( this );

                CViewer<TData,T,Dim>::set_shape ( shape.v );


                img_shape=shape;
		this->datascale=datascale;
		
// 		path_length_threshold=0;
// 	try {
// 	shader=new Shader(application_directory+"/model_render_vshader.glsl",
// 	  application_directory+"/model_render_pshader.glsl");
// // 	shader->get_log();
// 	  mhs_check_gl
// 	 } catch ( mhs::STAError error ) {
//
//             mhs::STAError error2;
//             error2<<error.str() <<"\n"<<"could not create the object";
//             throw error2;
//         }

		this->data_fun=data_fun;

                p_treedata=&treedata;
                p_conncection_data=&conncection_data;

                meshes.resize ( static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_Count ) );
                for ( int a=0; a<meshes.size(); a++ )
                    meshes[a]=NULL;

                no_particles=false;
                aliasing=2;
                lod=-1;
                sorting=false;
		take_screenshot=false;

                init_shader();
                init_tex_shader();
		cam = new CCam<TData,T,Dim>(shape,*this);
		
		frame_flight=0;
		frame_user=0;
		recording_flight=false;
		recording_user=false;
		center=true;
		show_tracks=true;
		det_screenshot_fname();
                
              // tex_init( );
               //set_tex_rendering(500,500);
                }
            catch ( mhs::STAError error )
                {

                throw error;
                }
                 
            }
            
        void toggle_center()
	{
	 center=!center; 
	  
	}
	
	void toggle_trackvis()
	{
	  show_tracks=!show_tracks;
	}

        ~CTrackViewer()
            {
              tex_deinit( ) ; 
                
            for ( int a=0; a<meshes.size(); a++ )
                {
                if ( meshes[a]!=NULL )
                    delete meshes[a];
                }

            for ( int i=0; i<roi_meshes.size(); i++ )
                {
                delete roi_meshes[i];
                }
            if ( roi_shader!=NULL )
                delete roi_shader;
            
            if (tex_shader!=NULL)
               delete tex_shader;
//       delete shader;
	    
	    if (cam!=NULL)
	      delete cam;
            }
   bool set_tex_rendering(int w,int h){

            tex_deinit();

            this->tex_width=w;
            this->tex_height=h;

            tex_init();

           this->update_projection_mat();

        };         
            
    void set_params(const mxArray * params=NULL)
    {

        
        if (params==NULL)
            return;
        if (mhs::mex_hasParam(params,"movie_folder")!=-1)
	{
            movie_folder=mhs::mex_getParamStr(params,"movie_folder");
	    printf("setting movie output folder to %s\n",movie_folder.c_str());
	}

	
    }

            
       void deinit_mesh (int protect_mesh=-1 )     
       {
	 for (std::size_t id=0;id<meshes.size();id++)
	 {
	   if (( meshes[id]!=NULL )&&(id!=protect_mesh))
	   {
	      delete meshes[id];  
	      meshes[id]=NULL;
	    }
	 }
       }
            
       bool init_mesh ( unsigned int meshid , std::time_t  timestamp=1)
       {
// 	 for (std::size_t id=0;id<meshes.size();id++)
// 	 {
// 	   if (( meshes[id]!=NULL ) && (meshid != id))
// 	   {
// 	      delete meshes[id];  
// 	      meshes[id]=NULL;
// 	    }
// 	 }
	 
	 
	 if ( meshid<meshes.size() )
                {
                if ( meshes[meshid]==NULL )
                    {
		    std::srand(timestamp);  

                    typename  CModelMesh<T,TData>::MESH mode=static_cast<typename CModelMesh<T,TData>::MESH> ( meshid );
                    meshes[meshid]=new CModelMesh<T,TData> ( *p_treedata,*p_conncection_data,
                                                       img_shape,mode,lod,sorting,data_fun,datascale);
                    }
		}
       }
            

        bool draw_mesh ( unsigned int meshid )
            {
            if ( meshid<meshes.size() )
                {
                sta_assert_error( meshes[meshid]!=NULL )
//                     {
// 
//                     typename  CModelMesh<T,TData>::MESH mode=static_cast<typename CModelMesh<T,TData>::MESH> ( meshid );
//                     meshes[meshid]=new CModelMesh<T,TData> ( *p_treedata,*p_conncection_data,
//                                                        img_shape,mode,lod,sorting );
//                     }
// 	    meshes[meshid]->enable_VBO();

                MyViewerMouse<TData,T,Dim> & mouse_state=* ( ( MyViewerMouse<TData,T,Dim>* )  CViewer<TData,T,Dim>::mouse_state );

                bool path_highlight=mouse_state.point_selection_mode==MyViewerMouse<TData,T,Dim>::POINT_SELECTION_MODE::POINT_SELECTION_CONNECTED;


		
		
		
		
// 	  if (path_highlight)
// 	  {
// 	       glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
// 	  }

                glUniform1f ( this->glsl_colorness_min, float ( 1.0 ) );
		glUniform1i(this->glsl_render_mode, this->render_mode);

                Vector<float,3> color ( 0.3,1,0.3 );
                meshes[meshid]->draw_selected ( color,path_highlight );

if (transparency>0)		
{
glEnable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
glUniform1f ( this->glsl_alpha, float ( transparency ) );
glUniform1f ( this->glsl_colorness_min, float ( -0.25 ) );
meshes[meshid]->draw_non_selected ( path_highlight,true );   		
glUniform1f ( this->glsl_colorness_min, float ( 1 ) );
glUniform1f ( this->glsl_alpha, float ( 1.0 ) );
}
		
		
                if ( path_highlight || (meshes[meshid]->always_transparent))
                    {
		    if (path_highlight )  
		    {
		      glUniform1f ( this->glsl_alpha, float ( 0.6 ) );
		    }else
		    {
		      glUniform1f ( this->glsl_alpha, float ( 0.8 ) );
		    }
                    glEnable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
                    }
                    if (!this->clip_plane_hard)
		    {
                     glEnable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
		    }
		    
		    
                glUniform1f ( this->glsl_colorness_min, float ( 0.0 ) );
                meshes[meshid]->draw_non_selected ( path_highlight );

                if ( path_highlight || (meshes[meshid]->always_transparent))
                    {
                    glUniform1f ( this->glsl_alpha, float ( 1 ) );
                    glDisable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
                    }
                    if (!this->clip_plane_hard)
		    {
                       glDisable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
		    }
if (transparency>0)
{
glDisable ( GL_SAMPLE_ALPHA_TO_COVERAGE );		    
}

                glUniform1f ( this->glsl_colorness_min, float ( 1.0 ) );

                return true;
                }
            return false;
            }



        void do_render()
            {
	      
	      rand_counter+=100;
	      
	      CViewer<TData,T,Dim>::do_render();
	      MyViewerMouse<TData,T,Dim> & mouse_state=* ( ( MyViewerMouse<TData,T,Dim>* )  CViewer<TData,T,Dim>::mouse_state );
/*
	      if ( mouse_state.call_from_main>0 ) {
		  switch ( mouse_state.call_from_main ) {
		  case 2:
		  {
			mexCallMATLAB(0, NULL,0,NULL, "start_gui2");
		  }
		  break;
		  }
		  mouse_state.call_from_main=0;
	      }*/


            if ( SDL_GL_MakeCurrent ( CSceneRenderer::tracker_data_p->tracker_window,CSceneRenderer::tracker_data_p->glcontext ) !=0 )
                {
                printf ( "error setting opengl context to current window\n" );
                return;
                }
             
                
                //this->m_transform.print();
//tex_tex               
//  SDL_SetRenderTarget(renderer, texTarget);
//  mhs_check_gl
//  SDL_RenderClear(renderer);
//  mhs_check_gl
	//SDL_RenderCopy(renderer, bmpTex, NULL, NULL);
	//Detach the texture
	            
if (this->tex_rendering())
{
    //glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer); 
    glBindFramebuffer(GL_FRAMEBUFFER, tex_mfbo);
    glPushAttrib(GL_VIEWPORT_BIT);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport ( 0, 0, this->tex_width, this->tex_height );
}else
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);  
}

	    bool cam_is_progressing=cam->fly();
	   
            CViewer<TData,T,Dim>::render_init_default((!cam->cam_is_flying())&&(center)&&(!take_screenshot));
	    //CViewer<TData,T,Dim>::render_init_default((center));

    



            float & rescale=CViewer<TData,T,Dim>::rescale;
            Vector<float,3> & font_color=CViewer<TData,T,Dim>::font_color;



            switch ( aliasing )
                {

                case 0:
                    glDisable ( GL_LINE_SMOOTH );
                    glDisable ( GL_POLYGON_SMOOTH );
                    glDisable ( GL_MULTISAMPLE );
                    glDisable ( GL_DITHER );
                    break;
                case 1:
                    glHint ( GL_LINE_SMOOTH_HINT, GL_NICEST );
                    glHint ( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
                    glEnable ( GL_LINE_SMOOTH );
                    glEnable ( GL_POLYGON_SMOOTH );
                    glDisable ( GL_MULTISAMPLE );
                    glEnable ( GL_DITHER );
                    break;
                case 2:
                    glDisable ( GL_LINE_SMOOTH );
                    glDisable ( GL_POLYGON_SMOOTH );
                    glEnable ( GL_DITHER );
                    glEnable ( GL_MULTISAMPLE );
                    break;

// 	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST );


// 	glEnable(GL_LINE_SMOOTH);



                    glDisable ( GL_BLEND );


                    CViewer<TData,T,Dim>::render_volume();





                }

// 	( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v


// 	  glMultMatrixf ( ( GLfloat * ) ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform*translation ).v );
//

// 	 ( GLfloat * ) ( CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform ).v
//
// 	 center[0],center[1],center[2]
// 	glUniformMatrix4fv(m_ProjectionUniform, 1, GL_FALSE, glm::value_ptr(projection));
// 	glUniformMatrix4fv(m_ViewUniform, 1, GL_FALSE, glm::value_ptr(view));

           // if ( ( !no_particles ) || ( roi_meshes.size() >0 ) )
                {

/*		  
		 roi_shader->bind();
	            glUniformMatrix4fv ( glsl_roi_projection,1,GL_FALSE, ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v );
//                     glUniformMatrix4fv ( glsl_roi_modelview, 1, GL_FALSE, ( GLfloat * ) ( modelview_mat.v ) );
//                     glUniformMatrix4fv ( glsl_roi_normalmat, 1, GL_FALSE, ( GLfloat * ) ( normal_mat.v ) );
                    glUniform1f ( glsl_roi_clipping_plane, CViewer<TData,T,Dim>::clip_plane_dist );
		    
 		  cam->draw(modelview_mat,normal_mat,glsl_roi_modelview,glsl_roi_normalmat);
		roi_shader->unbind(); */

                
		  
		  
                this->enable_model_shader();
                
                //glUniform1f(this->glsl_dist_clip, mouse_state.dist_clip);
                glUniform1f(this->glsl_dist_clip, this->extra_options[1]);
                

                glUniformMatrix4fv ( this->glsl_projection,1,GL_FALSE, ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v );

                Matrix<float,4>  trafo=CViewer<TData,T,Dim>::m_transform_current*CViewer<TData,T,Dim>::m_transform;
                mhs_check_gl
                Vector<float,3> center=this->new_center*T ( -1 );
                Matrix<float,4>  modelview_mat=trafo*Matrix<float,4>::OpenGL_glTranslate ( center );
                glUniformMatrix4fv ( this->glsl_modelview, 1, GL_FALSE, ( GLfloat * ) ( modelview_mat.v ) );
// 	  Matrix<float,4>  normal_mat=trafo.transpose().OpenGL_invert();
                Matrix<float,4>  & normal_mat=trafo;

//                 this->projection_mat.print();
//                 trafo.print();
//                 modelview_mat.print();
                
                mhs_check_gl
                glUniformMatrix4fv ( this->glsl_normalmat, 1, GL_FALSE, ( GLfloat * ) ( normal_mat.v ) );
                mhs_check_gl
                glUniform3fv ( this->glsl_focuspoint, 1, ( GLfloat * ) ( mouse_state.focus_point.v ) );
                glUniform2fv ( this->glsl_vertex_mode, 1, ( GLfloat * ) ( this->shader_options.v ) );
                 glUniform1f ( this->glsl_clipping_plane, CViewer<TData,T,Dim>::clip_plane_dist );
		 
		 
		 
		    if (( !no_particles ) && (show_tracks))
                    {

			switch ( mouse_state.render_mode )
                        {
			    case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_DATA) :
                            {
				glUniform1f ( this->glsl_ambient, 0.25);
			    }break;
			    case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_DATA2) :
                            {
				glUniform1f ( this->glsl_ambient, 0.35);
			    }break;
			    
			    default:
			      glUniform1f ( this->glsl_ambient, 0.1);
			}
		    }
// 		glUniform1f ( this->glsl_clipping_plane, this->clip_plane[0] );

                glUniform1f ( this->glsl_colorness_min, float ( 0 ) );
		
		glUniform1ui ( this->glsl_clipping_hard, this->clip_plane_hard );


		
		
// 		cam->draw(modelview_mat,normal_mat,this->glsl_modelview,this->glsl_normalmat);
// 		
// 		glUniformMatrix4fv ( this->glsl_modelview, 1, GL_FALSE, ( GLfloat * ) ( modelview_mat.v ) );
// 		glUniformMatrix4fv ( this->glsl_normalmat, 1, GL_FALSE, ( GLfloat * ) ( normal_mat.v ) );
// 		 roi_shader->bind();
// 	            glUniformMatrix4fv ( glsl_roi_projection,1,GL_FALSE, ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v );
// //                     glUniformMatrix4fv ( glsl_roi_modelview, 1, GL_FALSE, ( GLfloat * ) ( modelview_mat.v ) );
// //                     glUniformMatrix4fv ( glsl_roi_normalmat, 1, GL_FALSE, ( GLfloat * ) ( normal_mat.v ) );
//                     glUniform1f ( glsl_roi_clipping_plane, CViewer<TData,T,Dim>::clip_plane_dist );
		    
//  		  cam->draw(modelview_mat,normal_mat,glsl_roi_modelview,glsl_roi_normalmat);
		/*roi_shader->unbind(); */		
		

		std::size_t test;
                //if ( !no_particles )
		  if (( !no_particles ) && (show_tracks))
                    {
		      
		    if (mouse_state.render_mode_old!=mouse_state.render_mode)  
		    {
		      mouse_state.render_mode_old=mouse_state.render_mode; 
		      deinit_mesh(static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_VIS));
		    }
		    
		 
		    

                    switch ( mouse_state.render_mode )
                        {
                        case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_PARTICLE ) :
                            {
			      std::time_t time=std::time(NULL)+rand_counter;  
			      
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_PARTICLES );
                            
			    
			    init_mesh(mesh_id,time);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE_PARTICLE );
			    init_mesh(mesh_id,time);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
                            draw_mesh ( mesh_id );
			    
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;

                            }
                        break;

                        case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_CENTERLINE ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BIFURCATIONS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_TERMINALS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;

                        case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SURFACE ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_SURFACE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_SURFACE_TERMINALS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;

                            }
                        break;
			
			case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SURFACE_UQ ) :
                            {
			       std::time_t time=std::time(NULL)+rand_counter;  
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_SURFACE_UNIQUE );
                            init_mesh(mesh_id,time);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_TERMINALS_UNIQUE );
                            init_mesh(mesh_id,time);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;

                            }
                        break;

// 	    case (MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SURFACE):
// 	    {
// 	      unsigned int mesh_id=static_cast<unsigned int>(CModelMesh<T,TData>::MESH::MESH_SURFACE);
// 	      draw_mesh(mesh_id);
// 	      mesh_id=static_cast<unsigned int>(CModelMesh<T,TData>::MESH::MESH_SURFACE_TERMINALS);
// 	      draw_mesh(mesh_id);
// 	    }break;

                        case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_FIBERS ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_FIBER_CENTERLINES );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_FIBER_TERMINALS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;

                        case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_ALL_PARTICLE ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_PARTICLES );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_SINGLE_PARTICLES );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE_PARTICLE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;
			
			
			 case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_BLOBS ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BLOB_PARTICLES);
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			     mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BIFURCATIONS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;
			case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_DATA ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_VIS);
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    
			    mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_CENTERLINE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;
			case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_DATA2) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_VIS2);
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;
			case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_SPINES ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BLOB_PARTICLES_UNIC);
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			     mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BIFURCATIONS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_DATA_SPINE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;
			case ( MyViewerMouse<TData,T,Dim>::RENDERER_MODE::RENDERER_MODE_BLOBS2 ) :
                            {
                            unsigned int mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BLOB_PARTICLES_UNIC2);
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
                            mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_CENTERLINE );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			     mesh_id=static_cast<unsigned int> ( CModelMesh<T,TData>::MESH::MESH_BIFURCATIONS );
                            init_mesh(mesh_id);
			    test=std::max(meshes[mesh_id]->set_active_paths(this->extra_options[0]),test);
			    draw_mesh ( mesh_id );
			    if (this->extra_options[0]>test)
			      this->extra_options[0]=test;
                            }
                        break;
                        }
                    }


                this->disable_model_shader();


                if ( roi_meshes.size() >0 )
                    {
                    roi_shader->bind();

                    glUniformMatrix4fv ( glsl_roi_projection,1,GL_FALSE, ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v );
                    glUniformMatrix4fv ( glsl_roi_modelview, 1, GL_FALSE, ( GLfloat * ) ( modelview_mat.v ) );
                    glUniformMatrix4fv ( glsl_roi_normalmat, 1, GL_FALSE, ( GLfloat * ) ( normal_mat.v ) );
                    glUniform1f ( glsl_roi_clipping_plane, CViewer<TData,T,Dim>::clip_plane_dist );
		    glUniform1i( glsl_roi_render_mode, this->mesh_render_mode );
		    

                    glEnable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
                    for ( int a=0; a<roi_meshes.size(); a++ )
                        {
                        roi_meshes[a]->draw();
                        }
                    glDisable ( GL_SAMPLE_ALPHA_TO_COVERAGE );
                    roi_shader->unbind();
                    }

//                     Vector<int,4> viewport;
//                       glGetIntegerv(GL_VIEWPORT,viewport.v);
//        viewport.print();
                    
if (!take_screenshot)
{
	       roi_shader->bind();
	            glUniformMatrix4fv ( glsl_roi_projection,1,GL_FALSE, ( GLfloat * ) CViewer<TData,T,Dim>::projection_mat.v );
                     glUniformMatrix4fv ( glsl_roi_modelview, 1, GL_FALSE, ( GLfloat * ) ( modelview_mat.v ) );
                     glUniformMatrix4fv ( glsl_roi_normalmat, 1, GL_FALSE, ( GLfloat * ) ( normal_mat.v ) );
                    glUniform1f ( glsl_roi_clipping_plane, CViewer<TData,T,Dim>::clip_plane_dist );
		    
		    
		    #pragma omp critical (CAM_POINT_LIST_CHANGES)
		    
		    {
		      
		    cam->draw(modelview_mat,normal_mat,glsl_roi_modelview,glsl_roi_normalmat);
		    }
		roi_shader->unbind(); 
}



                }





            glDisable ( GL_BLEND );


            CViewer<TData,T,Dim>::render_volume();


            mhs_check_gl
            
            
             
            /*
            	  this->enable_VBO();





            	  int object_id=0;
            	  std::size_t offset=this->buffered_objects[object_id].i_offset;

            	  //glDrawElements ( GL_TRIANGLES, this->buffered_objects[object_id].i_length, GL_UNSIGNED_SHORT, ( void* ) ( offset * sizeof ( ushort ) ) );
            	  glDrawElements ( GL_TRIANGLES, this->buffered_objects[object_id].i_length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( ushort ) ) );

            	  mhs_check_gl;


            	  this->disable_VBO();*/

         

if (this->tex_rendering())
{
    glPopAttrib();
    glBindFramebuffer(GL_READ_FRAMEBUFFER, tex_mfbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, tex_fbo);
    glBlitFramebuffer(0, 0, this->tex_width, this->tex_height, 0, 0, this->tex_width, this->tex_height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //glViewport ( 0, 0, this->width, this->height );

    CViewer<TData,T,Dim>::text_HUD->InitHUD();

    glPushMatrix();
    glLoadIdentity();
                        
    //                         CViewer<TData,T,Dim>::text_HUD->StringoutLine ( "bla",0 );
    tex_shader->bind();
                            glColor4f(1,1,1,1 );
                            //glDisable(GL_DEPTH_TEST);
                            glDisable(GL_CULL_FACE);
                            glEnable ( GL_COLOR_MATERIAL );
                            glMatrixMode ( GL_TEXTURE );
                            glLoadIdentity();
                            glMatrixMode ( GL_MODELVIEW );
                            glPushMatrix();
                            glLoadIdentity();
                            glColor4f ( 1,1,1,1 );
                            glActiveTexture ( GL_TEXTURE0);
                                //glGenerateMipmap(GL_TEXTURE_2D);
                            mhs_check_gl
                            glEnable ( GL_TEXTURE_2D );
                            mhs_check_gl
glBindTexture ( GL_TEXTURE_2D, tex_texture1 );
//glBindTexture ( GL_TEXTURE_2D, texColorBuffer );

                            mhs_check_gl
                            glUniform1i ( glsl_tex_texture, 0 );
                            glUniform2f ( glsl_pixelsize,1.0f/float(this->width),1.0f/float(this->height));
                            glUniform2f ( glsl_kernel_scale,0.5f*float(this->width)/float(this->tex_width),0.5f*float(this->height)/float(this->tex_height));
                            //tex_kernel_scale
                            


                            float left=0.0f;
                            float right=1.0f;
                            float bottom=0.0f;
                            float top=1.0f;
                            const float w=1;
                            const float h=1;
                            float x=0;
                            float y=0;
                            float xw=1;
                            float yw=1;
                            glBegin ( GL_QUADS );
                            {
                                glTexCoord2f ( w,h );
                                glVertex3f ( x+xw,y+yw,0 );

                                glTexCoord2f ( 0,h );
                                glVertex3f ( x,y+yw,0 );

                                glTexCoord2f ( 0,0 );
                                glVertex3f ( x,y,0 );

                                glTexCoord2f ( w,0 );
                                glVertex3f ( x+xw,y,0 );
                            }
                            glEnd();

                            
                            glBindTexture ( GL_TEXTURE_2D, 0 );
                            mhs_check_gl
                            glDisable ( GL_TEXTURE_2D );
    tex_shader->unbind();            
                            glEnable(GL_CULL_FACE);
                        // glEnable(GL_DEPTH_TEST);
                            
                            glPopMatrix();
    glPopMatrix();                     



    CViewer<TData,T,Dim>::text_HUD->DeinitHUD();

    mhs_check_gl

}   

	    
	    
	    glFinish();
            
   mhs_check_gl

	    
	      if (take_screenshot)
	    {
	      take_screenshot=false;
	      std::stringstream s;
	      s<<"ntrackviewer_image_"<<screenshot_fname_count++<<".png"; 
	     this->save_image_to_file(s.str()); 
	    }
	    
	    
	    if ((movie_folder.length()>0))
	    {
	    
	      double current_time=timer.get_total_runtime();
	      if ((recording_user)&&((current_time-prev_time)>1.0/24.0))
	      {
		prev_time=current_time;
		std::stringstream filename;
		filename<<movie_folder<<"/"<<"movie_UI"<<10000+frame_user++<<".jpg";
		this->save_image_to_file2(filename.str()); 
		printf("writing frame %d to disk\n",frame_user);
	      }
	      
	      
	      
	      if ((recording_flight)&&(cam_is_progressing))
	      {
		std::stringstream filename;
		filename<<movie_folder<<"/"<<"movie_FLIGHT"<<10000+frame_flight++<<".jpg";
		this->save_image_to_file2(filename.str()); 
		recording_flight=cam->cam_is_flying();
		printf("writing frame %d to disk\n",frame_flight);
		
	      }

	    }            
            
            SDL_GL_SwapWindow ( CSceneRenderer::tracker_data_p->tracker_window );

	  
	    
            mhs::mex_dumpStringNOW();
// 	    SDL_Delay(100);
            }
            
            


    };


template <typename T,typename TData>
void _mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
    {

    CTrackViewer<TData,T,Dim> * renderer=NULL;
    std::size_t * gui_window_handle_ptr=NULL;
    std::size_t   gui_window_handle;
    
    CData<T,TData,Dim> *    data_fun=NULL;

    const mxArray * params=NULL;
    const mxArray * tracker_state=NULL;

    mhs::dataArray<T> treedata;
    mhs::dataArray<T> conncection_data;

    const mxArray * mesh_data=NULL;
    const mxArray * img_data=NULL;
    const mxArray * state_data=NULL;
    
    const mxArray * feature_struct=NULL;

    
    std::vector<int> tex;
    tex.resize(2);
    tex[0]=tex[1]=-1;

    float transparency=0;
    float datascale=1;
    
    bool sorting=true;


    class CMesh
        {
        public:
            const mxArray * mesh_data;
            Vector<float,4> color;
            std::size_t index;
            CMesh()
                {
                color=float ( 1 );
                color[1]=float ( 0 );
                color[3]=float ( 0.5 );
                index=0;
                mesh_data=NULL;
                }
        };
    std::list<CMesh> mesh_list;

    int aliasing=2;
    int  lod=-1;


    int params_indx=1;

    for ( int i=0; i<nrhs; i++ )
        {
        bool is_mesh_data=true;
        if ( mxIsStruct ( prhs[i] )  && (mxGetField ( prhs[i],0, ( char * ) ( "cam" ) )==NULL))
            {
// 	printf("is mesh?");
            for ( int a=0; a<mxGetNumberOfElements ( prhs[i] ); a++ )
                {

                mxArray * normal_p=mxGetField ( prhs[i],a, ( char * ) ( "n" ) );
                mxArray * vertex_p=mxGetField ( prhs[i],a, ( char * ) ( "v" ) );
                mxArray * face_p=mxGetField ( prhs[i],a, ( char * ) ( "f" ) );
                mxArray * color=mxGetField ( prhs[i],a, ( char * ) ( "color" ) );

// 	  printf("%u %u %u\n",(std::size_t) normal_p,(std::size_t) vertex_p,(std::size_t) face_p);

                if ( ( normal_p==NULL ) || ( vertex_p==NULL ) || ( face_p==NULL ) ||
                        ( mxGetClassID ( normal_p ) !=mhs::mex_getClassId<float>() ) ||
                        ( mxGetClassID ( vertex_p ) !=mhs::mex_getClassId<float>() ) ||
                        ( mxGetClassID ( face_p ) !=mhs::mex_getClassId<uint32_t>() ) )
                    {
                    is_mesh_data=false;
                    mesh_list.clear();
// 	    printf("NO 1!\n");
                    break;
                    }

                CMesh mesh;
                if ( color!=NULL )
                    {
                    printf ( "found color\n" );
                    if ( mxGetClassID ( color ) ==mhs::mex_getClassId<float>() )
                        {
                        mhs::dataArray<float> color_v=mhs::dataArray<float> ( color );
                        if ( ( color_v.dim.size() ==2 ) && ( color_v.dim[0]==4 ) )
                            {
                            mesh.color=color_v.data;
                            }
                        }
                    else if ( mxGetClassID ( color ) ==mhs::mex_getClassId<double>() )
                        {
                        mhs::dataArray<double> color_v=mhs::dataArray<double> ( color );
                        if ( ( color_v.dim.size() ==2 ) && ( color_v.dim[0]==4 ) )
                            {
                            mesh.color=Vector<double,4> ( color_v.data );
                            }
                        }
                    }

                mesh.color.print();
                mesh.index=a;
                mesh.mesh_data=prhs[i];
                mesh_list.push_back ( mesh );

                }
            }
        else
            {
// 	printf("NO 2!\n");
            is_mesh_data=false;
            }

        if ( is_mesh_data )
            {
// 	printf("YES!\n");
            }
//       if (mxIsCell(prhs[i]))
//       {
// 	for  (int i=0;i<mxGetNumberOfElements(prhs[i]);i++)
// 	{
// 	  if ((mxIsStruct(mxGetCell(prhs[i],i)))&&())
// 	  {
//
// 	  }
//
// 	}
//       }

        if ( !is_mesh_data )
            {
	     if ( mxIsStruct ( prhs[i] ) && (mxGetField ( prhs[i],0, ( char * ) ( "cam" ) )!=NULL))
             {
                state_data=prhs[i];//mxGetField ( prhs[i],0, ( char * ) ( "cam" ) ); 
		printf("found state data\n");
	     }
	    else if ( mxIsCell ( prhs[i] ) && ( i==nrhs-1 ) )
                {
                params=prhs[i];
                }
            else if ( mxIsStruct ( prhs[i] ) )
                {
                if ( ( mxGetField ( prhs[i],0, ( char * ) ( "data" ) ) !=NULL ) && ( mxGetField ( prhs[i],0, ( char * ) ( "connections" ) ) !=NULL ) )
                    {
                    tracker_state=prhs[i];
                    printf ( "found tracker data\n" );
                    }

                if ( ( mxGetField ( prhs[i],0, ( char * ) ( "texture3D" ) ) !=NULL ) && ( mxGetField ( prhs[i],0, ( char * ) ( "light_texture3D" ) ) !=NULL ) && ( ( mxGetField ( prhs[i],0, ( char * ) ( "cshape" ) ) !=NULL ) || ( mxGetField ( prhs[i],0, ( char * ) ( "img" ) ) !=NULL ) ) )
                    {
                    img_data=prhs[i];
                    printf ( "found texture\n" );
                    }
                    
                if (( mxGetField ( prhs[i],0, ( char * ) ( "datafunc" ) ) !=NULL ))
		{
		      feature_struct =prhs[i];
		      int datafunc=mhs::dataArray<TData>(feature_struct,"datafunc").data[0];
	  
		      switch(datafunc)
		      {
			      case 0:
			      {
				  data_fun=new CDataDummy<T,TData,Dim>();
				  printf("dataterm: DUMMY\n");
			      }
			      break;
			      case 7:
			      {
				  data_fun=new CDataHessianNew<T,TData,Dim>();
				  printf("dataterm: Hessian New\n");
			      }
			      break;
			      case 8:
			      {
				  data_fun=new CDataSD<T,TData,Dim>();
				  printf("dataterm: SD\n");
			      }
			      break;
			      case 9:
			      {
				  data_fun=new CDataSD2<T,TData,Dim>();
				  printf("dataterm: SD New\n");
			      }
			      break;
			      case 10:
			      {
				  data_fun=new CDataSDSH<T,TData,Dim>();
				  printf("dataterm: SD SH\n");
			      }
			      break;
			      case 11:
			      {
				  data_fun=new CDataSDSHorg<T,TData,Dim>();
				  printf("dataterm: SD SH org\n");
			      }
			      break;
			      
			      case 100:
			      {
				  data_fun=new CDataDTI<T,TData,Dim>();
				  printf("dataterm: DTI\n");
			      }
			      break;  
			      
			      default:
			      {
				  printf("unknown dataterm: %d\n",datafunc);
				  feature_struct=NULL;
			      }
		      }
		      
		      if (data_fun!=NULL)
		      {
			CData<T,TData,Dim> &  data_fun_p=*data_fun;
			mhs::mex_dumpStringNOW();
			printf("->datafunc\n");
			data_fun_p.init(feature_struct);
			data_fun_p.set_params ( );
		      }
		}
                    
                }
            }
        }

    /*
        printf("%u\n",std::size_t(params));
        return;*/

    try
        {
// 	sta_assert_error(tracker_state!=NULL);

        if ( tracker_state!=NULL )
            {
            sta_assert_error ( mxIsStruct ( tracker_state ) );
            treedata=mhs::dataArray<T> ( tracker_state,"data" );
            conncection_data=mhs::dataArray<T> ( tracker_state,"connections" );
            }


        Vector<std::size_t,3> shape;
	bool shape_override=false;

        if ( params!=NULL )
            {
            if ( mhs::mex_hasParam ( params,"handle" ) !=-1 )
                {
                gui_window_handle=mhs::mex_getParam<uint64_t> ( params,"handle",1 ) [0];
                gui_window_handle_ptr=&gui_window_handle;
                printf ( "found window handle\n" );
                }
            if ( mhs::mex_hasParam ( params,"aliasing" ) !=-1 )
                {
                aliasing=mhs::mex_getParam<int> ( params,"aliasing",1 ) [0];
                }
            if ( mhs::mex_hasParam ( params,"lod" ) !=-1 )
                {
                lod=mhs::mex_getParam<int> ( params,"lod",1 ) [0];
                }
            if ( mhs::mex_hasParam ( params,"sorting" ) !=-1 )
                {
                sorting=mhs::mex_getParam<bool> ( params,"sorting",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"shape" ) !=-1 )
                {
                shape=mhs::mex_getParam<double> ( params,"shape",3 );
		shape_override=true;
                }
            if ( mhs::mex_hasParam ( params,"datascale" ) !=-1 )
                {
                datascale=mhs::mex_getParam<float> ( params,"datascale",1 ) [0];
                }     
	      if ( mhs::mex_hasParam ( params,"transparency" ) !=-1 )
                {
                transparency=1-mhs::mex_getParam<float> ( params,"transparency",1 ) [0];
                }         
                
                  if ( mhs::mex_hasParam ( params,"texrender" ) !=-1 )
                {
                    tex=mhs::mex_getParam<int> ( params,"texrender",2 );
                }         
                
                
                
            }


        
if (!shape_override)
{
        if ( img_data!=NULL )
            {
            try
                {

                if ( mxGetField ( img_data,0, ( char * ) ( "img" ) ) !=NULL )
                    {
                    mhs::dataArray<TData> img=mhs::dataArray<TData> ( img_data,"img" );
                    sta_assert_error ( img.dim.size() ==3 );
                    for ( int i=0; i<3; i++ )
                        {
                        shape[i]=img.dim[i];
                        }
                    }
                else
                    {
                    for ( int i=0; i<3; i++ )
                        {
                        shape[i]=mhs::dataArray<TData> ( img_data,"cshape" ).data[2-i];
                        }
                    }
                }
            catch ( mhs::STAError error )
                {
                printf ( "error, missing raw image: %s\n",error.str().c_str() );

                }
            }
        else
            {

            sta_assert_error ( tracker_state!=NULL );
            shape=CModelMesh<T,TData>::estimate_shape ( treedata );
            }
}


// 	shape.print();

// 	return;
//
        if ( tracker_state!=NULL )
            {
            renderer= new CTrackViewer<TData,T,Dim> ( treedata,conncection_data,data_fun,shape,gui_window_handle_ptr,datascale );
            }
        else
            {
            renderer= new CTrackViewer<TData,T,Dim> ( shape,gui_window_handle_ptr );
            }


        renderer->set_rendering ( aliasing );

        renderer->set_lod ( lod );

        renderer->set_sorting ( sorting );

	renderer->set_transparency(transparency);

printf("OK before rendering\n");                
       renderer->set_tex_rendering(tex[0],tex[1]);

printf("OK rendering\n");            
        if ( img_data!=NULL )
            {
            renderer->init ( img_data );
            }


        int count=0;
        for ( typename std::list<CMesh>::iterator iter=mesh_list.begin(); iter!=mesh_list.end(); iter++ )
            {
            printf ( "loading ROI \n",count++ );
            CMesh & mesh=*iter;
            renderer->add_roi ( mesh.mesh_data,mesh.index,mesh.color );
            printf ( "done\n" );
            }
            
            
       renderer->load(state_data);
       renderer->set_params(params);


        bool no_error=true;
        int total_num_threads=2;



        printf ( "->starting rendering\n" );

        bool running=true;
        int running_count=total_num_threads;
        no_error=true;

#ifdef _VALGRIND	
renderer->do_render();	
#else


	bool update_gui=true;
	omp_set_nested(1);
        #pragma omp parallel for num_threads(total_num_threads) shared(running,running_count,update_gui)
        for ( int i=0; i<total_num_threads; i++ )
            {
            int thread_id=omp_get_thread_num();
            #pragma omp critical
                {

                if ( thread_id==0 )
                    printf ( "worker %d is running (GUI) \n",thread_id );
                else if ( thread_id==1 )
                    printf ( "worker %d is running (input) \n",thread_id );
                }


            if ( thread_id==0 ) // if using GUI then GUI thread will be the main thread
                {
                try
                    {
                    int count=0;
                    while ( running )
                        {
			if (update_gui || renderer->is_flying() || renderer->is_intransition()){  
			  renderer->do_render();
			  update_gui=false;
			  renderer->update_tracker_controls();
			  SDL_Delay ( 1 );
			}else
			{
			  renderer->update_tracker_controls();
			 SDL_Delay ( 25 ); 
			}
                        count++;
                        }
                    }
                catch ( mhs::STAError & error )
                    {
                    renderer->send_exit_program_signal();
                    running=false;
                    no_error=false;
                    printf ( "error in GUI thread: %s\n",error.str().c_str() );
                    }
                }
            else if ( thread_id==1 ) //
                {
                try
                    {
                    while ( running )
                        {
                        update_gui=(renderer->ui() || update_gui );
		        if (renderer->vis_options_updated)
			{
			 update_gui=true; 
			 renderer->vis_options_updated=false;
			}
                        if ( renderer->is_exit_program() )
                            {
                            running=false;
                            }
                        SDL_Delay ( 25 );
                        }
                    }
                catch ( mhs::STAError & error )
                    {
                    renderer->send_exit_program_signal();
                    running=false;
                    no_error=false;
                    printf ( "error in INPUT thread: %s\n",error.str().c_str() );
                    }
                }
            printf ( "working threads remaining:%d \n",running_count );



            #pragma omp critical
                {
                if ( thread_id==0 )
                    printf ( "worker %d is exiting (GUI) \n",thread_id );
                else if ( thread_id==1 )
                    printf ( "worker %d is exiting (input) \n",thread_id );
                }
            }
#endif            

        if ( !no_error )
            throw ( mhs::STAError ( "exiting" ) );

	if (nlhs>0)
	{
	  plhs[0]=renderer->save();
	}

        if ( renderer!=NULL )
            delete renderer;
	
	if (data_fun!=NULL)
	  delete data_fun;
	

        }
    catch ( mhs::STAError & error )
        {
        printf ( "error cleaning up!!\n" );
        mexErrMsgTxt ( error.what() );
        }
    catch ( ... )
        {
        printf ( "error cleaning up!!\n" );
        mexErrMsgTxt ( "exeption ?" );
        }

    }





void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
    {

#ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
    printf ( "_DEBUG_CONNECT_LOOP_EXTRA_CHECK enabled\n" );
#endif

    set_application_directory();



    printf ( "mexcfunc #params: %d\n",nrhs );
    if ( nrhs<1 )
        mexErrMsgTxt ( "error: nrhs<1\n" );
    if ( !mxIsStruct ( prhs[0] ) )
        mexErrMsgTxt ( "error: expecting feature struct\n" );

//     const mxArray *img=NULL;
//
//     if ((nrhs>1)&&(!mxIsCell(prhs[1])))
//     {
//       img=prhs[1];
//     }



//     if ((img==NULL)||(mxGetClassID(img)==mxDOUBLE_CLASS))
//         _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
//     else if (mxGetClassID(img)==mxSINGLE_CLASS)
    _mexFunction<double,float> ( nlhs, plhs,  nrhs, prhs );
//     else
//         mexErrMsgTxt("error: unsupported data type\n");


    }







