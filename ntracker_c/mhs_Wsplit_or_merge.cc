//#define _POOL_TEST
//#define _NUM_THREADS_ 4

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"


#define _D_FNOGUI_
 #include "graph_rep.h"



const int Dim=3;
typedef double T;

#define EPS 0.00000000001
#define EPS_R 0.0001


class CSimplePoint;




class CSimpleEdge
{
  public:
    
  CSimplePoint * left;  
  CSimplePoint * right;
  int side_left;
  int side_right;
  int region_id;
    bool deleteme;
    
  bool is_set_to_bif;  
  
  CSimpleEdge()
  {
   left=right=NULL; 
   region_id=-1;
   deleteme=false;
   is_set_to_bif=false;
  }
  
//   presult[EDGE_ATTRIBUTES::EDGE_A_TYPE]=static_cast<int>(con->edge_type);
};

class CSimplePoint : public Points< T, 3 >
{
  public:
  //static const int num_attr=24;//CTracker<double,double,3>::POINT_A_Count;
    static const int num_attr=CTracker<double,double,3>::POINT_A_Count;
  T attributes[num_attr];
  
  std::vector<CSimpleEdge*> edges[2];
  int region_id;
  int unique_ids[2];
  bool deleteme;
  int id;
  
  int  remove_edge(CSimpleEdge* ep2)
  {
      int removed=0;
      for (int e=0;e<2;e++)
      {
	std::vector<CSimpleEdge*>::iterator iter=edges[e].begin();  
	while (iter!=edges[e].end())
	{
	  CSimpleEdge* ep=*iter;
	  if (ep==ep2)
	  {
	    iter=edges[e].erase(iter);
	    removed++;
	  }else
	  {
	    iter++;
	  }
	}
      }
      return removed;
  }
  
  CSimplePoint(): Points< T, 3 >()
  {
    for (int a=0;a<num_attr;a++)
    {
      attributes[a]=0;
    }
    region_id=-1;
    deleteme=false;
    unique_ids[0]=unique_ids[1]=-1;
    id=-1;
  }
  
//   void update_from_edge_info()
//   {
//     for (int i=0;i<2;i++)
//     {
//       this->endpoint_connections[i]=edges[i].size();
//       for (int a=0;a<std::min(this->endpoint_connections[i],maxconnections);a++)
//       {
// 	Points< T, 3 > * cpoint=((edges[i][a]->left==this) ? edges[i][a]->right : edges[i][a]->left);
// 	int side=((edges[i][a]->left==this) ? edges[i][a]->side_right : edges[i][a]->side_left);
// 	this->endpoints[i][a]->connected=cpoint->endpoints[side][0];
//       }
// 			
//     }
//     
//   }
  
  void update_from_attributes()
  {
		    this->set_position(Vector<T,3>(
					      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX],
                                            attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY],
                                            attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ]));
		    
//                     this->direction[0]=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_DIRX]);
//                     this->direction[1]=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_DIRY]);
//                     this->direction[2]=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_DIRZ]);
		    
		    this->set_direction(Vector<T,3>(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_DIRX],
		      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_DIRY],
		      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_DIRZ]));
		    
                    this->tracker_birth=attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_BIRTH_FLAG];
                    this->set_scale(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_SCALE]);
                    this->point_cost=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_COST]);
                    this->saliency=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_SALIENCY]);
		    
// 		    this->freezed=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED]);
// 		    this->freezed_endpoints[0]=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]);
// 		    this->freezed_endpoints[1]=(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]);

		    //this->particle_type=static_cast<PARTICLE_TYPES>((unsigned int)(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_TYPE]));
		    //particle type will be determined automatically
		    this->particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
		    
		    
		    
		    PARTICLE_TYPES particle_type=static_cast<PARTICLE_TYPES>((unsigned int)(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_TYPE]));		    
		     
    
  }
  
  Vector<T,3> get_pos()
  {
//      return Vector<T,3>(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX],
//                                             attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY],
//                                             attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ]);
     return Vector<T,3>(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ],
                                            attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY],
                                            attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX]);    
  }
  
  T  get_scale()
  {
//      return Vector<T,3>(attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX],
//                                             attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY],
//                                             attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ]);
     return attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_SCALE];    
  }
  
  void freeze()
  {
	    attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED]=1;
  }
  
  void border_uid(int side,int id)
  {
	    if (side==0)
	    {
	    attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]=id;    
	    }
	    else if (side==1)
	    {
	      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]=id;    
	    }else
	    {
	      printf("ha? which edge side ist that? %d\n",side);
	    }
  }
  
  void set_type_bif_center()
  {
      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_TYPE]=static_cast<unsigned int>(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);   
  }
  void set_type_bif()
  {
      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_TYPE]=static_cast<unsigned int>(PARTICLE_TYPES::PARTICLE_BIFURCATION);   
  }
  void set_type_seg()
  {
      attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_TYPE]=static_cast<unsigned int>(PARTICLE_TYPES::PARTICLE_SEGMENT);   
  }
  
};



template<typename T,int Dim>
bool colliding2( OctTreeNode<T,Dim> & tree,
                OctPoints<T,Dim> & npoint,
                T searchrad,
		bool debug=false
	      )
{
    Points<T,Dim> & newpoint= *((Points<T,Dim>  *)&npoint);

    std::size_t found;
    class OctPoints<T,Dim> * query_buffer[query_buffer_size];
    class OctPoints<T,Dim> ** candidates;
    candidates=query_buffer;

    CSimplePoint * cp1=(CSimplePoint *) &npoint;
    
    try {
        tree.queryRange(
            newpoint.get_position(),searchrad,candidates,query_buffer_size,found);
    } catch (mhs::STAError & error)
    {
        throw error;
    }
//     printf("found :: %d-------\n",found);
    for (std::size_t a=0; a<found; a++)
    {
        Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
	
	sta_assert_debug0((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)||(!(point->get_direction()[0]<1)));
	
	
	CSimplePoint * cp2=(CSimplePoint *) point;
// 	printf("%d %d\n",cp1->region_id,cp2->region_id);
// 	
	// only check collisions between different regions
	if (!((cp1->region_id)>(cp2->region_id)))
	{
// 	  printf("lolo\n");
	  continue;
	}
	
	
	
        if (point!=&newpoint)
        {
            T   cand_size=point->get_scale();

            T candidate_center_dist_sq=(point->get_position()-newpoint.get_position()).norm2();

	    T thickinessA=newpoint.get_thickness();
	    T thickinessB=point->get_thickness();
	    
	    T scaleA=newpoint.get_scale();
	    T scaleB=point->get_scale();
	    
            T mindist=std::max(thickinessA,scaleA)+std::max(scaleB,thickinessB);
	    //T mindist=scaleA+scaleB;
	    
	    if (!(candidate_center_dist_sq>mindist*mindist))
	    {
	      
	     
		
		
		bool check_collision=true;
		
// 		//check for bifurcation self collision
// 		 if ((point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)||
// 		     (newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
// 		 {
// 		      Points<T,Dim> * points[2];
// 		      points[0]=&newpoint;
// 		      points[1]=point;
// 		      int bif_n_point=(point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER) ? 0 : 1;
// 		      for (int i=0;i<2;i++)
// 		      {
// 		      if ((points[bif_n_point]->endpoint_connections[i]>0)
// 			&&(points[bif_n_point]->endpoints[i][0]->connected->point==points[1-bif_n_point]))
// 		      {
// 			sta_assert_debug0(point->particle_type!=newpoint.particle_type);
// 		      check_collision=false;
// 		      }
// 		      }
// 		 }
		

	if (check_collision)	
	{
	  
	    
  
		T maxdist=std::min(thickinessA,scaleA)+std::min(scaleB,thickinessB);
		if (!(candidate_center_dist_sq>maxdist*maxdist))
		{
// 		  if (cp1->region_id==cp2->region_id)
// 		  {
// 		    printf("#######################################\nwarning: two collising points in same region\n#######################################\n");
// 		  }
		  return true;
		}
		
		 if ((point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)&&
		     (newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
		{
		printf("collision:\n");  
		point->get_position().print();
		newpoint.get_position().print();
		printf("2: %d %d\n",check_collision);
	      }	

	      bool collision=(ellipsoids_coolide(
			    newpoint.get_position(),
			    point->get_position(),
			    newpoint.get_direction(),
			    point->get_direction(),
			    thickinessA,
			    scaleA,
			    thickinessB,
			    scaleB));
 
		
	      if ((point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)&&
		     (newpoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
	      {
		printf("collisin:\n");
		point->get_position().print();
		newpoint.get_position().print();
		printf("%d %d\n",check_collision,collision);
	      }		
		
//     		collision=false;
	      if (collision)
	      {
		 
		
		//if (collision)
		    if (cp1->region_id==cp2->region_id)
		  {
		    printf("#######################################\nwarning: two collising points in same region\n#######################################\n");
		  }

		  return true;		
	      }
	}

	    }
	    
        }
//         iter++;
    }
    return false;
}


bool inbb(Vector<T,3> & o ,Vector<T,3> & s, Vector<T,3> & p)
{
    for (int a=0;a<3;a++)
    {
      if ((p[a]<o[a])||(!(p[a]<(o[a]+s[a]))))
      //if ((p[a]<o[a])||(!(p[a]<(o[a]+s[a]-1))))
      {
	return false;
      }
    }
    return true;
}

mxArray * create_tracker_pt_state(std::list<CSimplePoint> & pt_data,
			      std::list<CSimpleEdge> & eg_data,
			      const mxArray * tracker_state,
			      int region_id,
			      Vector<T,3> offset)
			      //mxArray * & data, mxArray * & connections)
{
  
	mxArray * data;
	mxArray * connections;
	
	
	std::size_t num_pts=0;
	std::size_t num_edges=0;
	std::size_t count=0;
	for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
	{
	  CSimplePoint & pt=*iter;
	  if (region_id==-2)
	    pt.region_id=-2;
	    
	  if (pt.region_id==region_id)
	  {
	    num_pts++;
	    pt.id=count++;
	  }
	}
	for (std::list<CSimpleEdge>::iterator iter=eg_data.begin();iter!=eg_data.end();iter++)
	{
	  CSimpleEdge & edge=*iter;
	  if (region_id==-2)
	    edge.region_id=-2;
	  
	  if (edge.region_id==region_id)
	  {
	    bool bif_left=(edge.left->edges[edge.side_left]).size()>1;
	    bool bif_right=(edge.right->edges[edge.side_right]).size()>1;
	    sta_assert_error(!((bif_left)&&(bif_right)));
	    if (bif_right)
	    {
	      edge.left->set_type_bif();
	      edge.right->set_type_bif_center();
	    }
	    if (bif_left)
	    {
	      edge.right->set_type_bif();
	      edge.left->set_type_bif_center();
	    }
	      
	    num_edges++;
	  }
	}
// 	 printf("0\n");
	
	mwSize dim=1;
	mwSize dims=1;
	mwSize nfields=0;	    
 	mxArray * A=mxCreateStructArray(dim, &dims, nfields,NULL);
	
	int num_fileds=mxGetNumberOfFields(tracker_state);
	for (int i=0;i<num_fileds;i++) 
	{
	  const char * fieldname=mxGetFieldNameByNumber(tracker_state, i);
	  if ((strcmp(fieldname,"data")!=0)
	    &&(strcmp(fieldname,"connections")!=0)
	    &&(std::abs(strncmp(fieldname,"observer",7))!=0)
	    &&(strcmp(fieldname,"tmp_volume")!=0))
	  {
// 	  printf("copying %s\n",fieldname);
	  mxAddField(A,fieldname);
	  mxSetField(A, 0,fieldname, mxDuplicateArray(mxGetField(tracker_state,0,fieldname)));
	  }
	}
// 	 printf("1\n");
  
	unsigned int n_points=num_pts;//pt_data.size();
	unsigned int  npointproperties=CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_Count;
	std::size_t ndims[2];
        ndims[1]=n_points;
        ndims[0]=npointproperties;
        data = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
	T *result = ( T * ) mxGetData (data );
	
// 	 printf("2\n");
	for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
	{
	  if (iter->region_id==region_id)
	  {
	    
	    if (iter->deleteme)
	      printf("pt: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	    memcpy(result,iter->attributes,sizeof(T)*npointproperties);
	    
// 	    result[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX]-=offset[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX];
// 	    result[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY]-=offset[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY];
// 	    result[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ]-=offset[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ];
/*	    result[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSX]-=offset[0];
	    result[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSY]-=offset[1];
	    result[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_POSZ]-=offset[2];*/	    
	    result+=ndims[0];
	  }
	}
	
	{
	  std::size_t numconnections=num_edges;//eg_data.size();
	  std::size_t ndims[2];
	  ndims[1]=numconnections;
	  ndims[0]=CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_Count;
	  connections   = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
// 	  mxSetField(tdata,0,"connections", connections);
	  T* presult = ( T * ) mxGetData (connections);
	  
	  std::size_t count=0;
	  printf("num conn: %d\n",numconnections);
	  for (std::list<CSimpleEdge>::iterator iter=eg_data.begin();iter!=eg_data.end();iter++)
	  {
  // 	  memcpy(presult,iter->attributes,sizeof(T)*npointproperties);
	      if (iter->region_id==region_id)
	      {
		if (iter->deleteme)
	      printf("pt: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	      
		count++;
// 		printf("%d %d\n",count,num_edges);
		if ((iter->left->region_id!=region_id)||(iter->right->region_id!=region_id))
		{
		 printf("error region id:  %d %d",region_id,iter->left->region_id,iter->right->region_id); 
		 break;
		}
		if (count>numconnections)
		{
		 printf("error count\n "); 
		 break;
		} 

	       if ((iter->left==NULL)||(iter->right==NULL))
	       {
		 printf("%u %u",iter->left,iter->right);
	       }
	      
		
	      presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXA]=iter->left->id;
	      presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXB]=iter->right->id;

	      
	      bool is_bif=((iter->left->edges[0].size()+iter->left->edges[1].size())>2)||((iter->right->edges[0].size()+iter->right->edges[1].size())>2);
	      
	      presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_SIDEA]=iter->side_left;
	      presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_SIDEB]=iter->side_right;
	      
	      //presult[EDGE_ATTRIBUTES::EDGE_A_COST]=con->cost;
	      presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_TYPE]=is_bif ? 1 : 0;
	      
	      if (iter->is_set_to_bif)
	      {
		if (!is_bif)
		{
		   printf("bif: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		   printf("%d %d %d %d",iter->left->edges[0].size(),iter->left->edges[1].size(),iter->right->edges[0].size(),iter->right->edges[1].size());
		}
	      }

// 	      result[int(presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXA]*npointproperties)+CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=1;
// 	      result[int(presult[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXB]*npointproperties)+CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=1;
	      
	      presult+=ndims[0];
	      }
	  }
	}
// 	 printf("4\n");
	 mhs::mex_dumpStringNOW();
	
	mxAddField(A,"data");
	mxSetField(A, 0,"data", data);
	mxAddField(A,"connections");
	mxSetField(A, 0,"connections", connections);
	
	if (region_id==-1)
	{
	mxAddField(A,"offline");
	mxSetField(A, 0,"offline", mxCreateDoubleScalar(1));
	}
	
	return A; 
}


	    class t_restore_me{
	    public:
	      CSimplePoint* pt;
	      int side;
	    };


void update_center(Vector<T,3> & epA,Vector<T,3> & epB,Vector<T,3> & epC,T sA,T sB,T sC,Vector<T,3> & pos,T & scale)
{
	
	
	
	compute_inner_circle(
	  epA, // seitenhalbierende pos
	  epB,
	  epC,
	  sA*CEdgecost<T,Dim>::side_scale, //seiten laenge
	  sB*CEdgecost<T,Dim>::side_scale,
	  sC*CEdgecost<T,Dim>::side_scale,
	  pos,
	  scale);
	
	
	scale=std::max(scale,0.05);
}	    
	    
/*!
 *
 *  MAIN FUNCTION
 *
 * */
template <typename T,typename TData>
//template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
      
    std::list<CSimplePoint> pt_data;
    std::vector<CSimplePoint * > pt_data_indx;
    std::list<CSimpleEdge> eg_data;
    OctTreeNode<T,Dim> * tree=NULL;
    
    
    mxArray* bifurcation_data_backup=NULL;
    
    
  
    const mxArray * tracker_state=NULL;

    if ((nrhs>0)&&(mxIsStruct(prhs[0])))
    {
        tracker_state=prhs[0];
    }
//     else
//     {
// 	mexErrMsgTxt("error: expecting feature struct\n");
//     }
    
    try 
    {
    
    bool tracker_state_multitracker;
    int num_tracker_states;
    
    if (tracker_state!=NULL)
	{
	  
	  tracker_state_multitracker=mxGetField(tracker_state,0,(char *)("W"))!=NULL;
	  if (tracker_state_multitracker)
	  {
	    const  mxArray  * tracker_cpy_smaple_state=NULL;
// 	    num_tracker_states=mxGetNumberOfElements(tracker_state);
	    num_tracker_states=mxGetNumberOfElements(mxGetField(tracker_state,0,(char *)("W")));
	    
	    printf("found data for several regions (%d) \n - > merging regions\n",num_tracker_states);
	    


	    //tree=new OctTreeNode<T,Dim>(shape,options.opt_grid_spacing);
	    
	    std::size_t bbox[3];
	    T max_scale=0;
	    bbox[0]=bbox[1]=bbox[2]=0;
	    std::size_t max_restore_id=-1;
	    T particle_thickness=-1;
	    
	    std::vector<std::list<t_restore_me> > restore_candidates;
	    
	    std::size_t path_id_offset=0;
	    for (int t=0;t<num_tracker_states;t++)
	    {
	     
		    const  mxArray  * worker_data=mxGetField(tracker_state,0,(char *)("W"));
		    sta_assert_error(worker_data!=NULL);
		    
		    const  mxArray  * sub_region=mxGetField(worker_data,t,(char *)("shape"));
		    sta_assert_error(sub_region!=NULL);

		    std::size_t shape[3];
		    std::size_t offset[3];
		      
		    double * rdata_p=(double  *)mxGetPr(sub_region);
		    shape[0]=rdata_p[2];
		    shape[1]=rdata_p[1];
		    shape[2]=rdata_p[0];
// 		    shape[0]=rdata_p[0];
// 		    shape[1]=rdata_p[1];
// 		    shape[2]=rdata_p[2];
		    
		    sub_region=mxGetField(worker_data,t,(char *)("offset"));
		    sta_assert_error(sub_region!=NULL);
		    rdata_p=(double  *)mxGetPr(sub_region);
		    offset[0]=rdata_p[2];
		    offset[1]=rdata_p[1];
		    offset[2]=rdata_p[0];
// 		    offset[0]=rdata_p[0];
// 		    offset[1]=rdata_p[1];
// 		    offset[2]=rdata_p[2];
		    
		    
		  
		    sub_region=mxGetField(worker_data,t,(char *)("A"));
		    
		    if (t==0)
		    {
		    tracker_cpy_smaple_state=sub_region;
		    }
		    
		    mhs::dataArray<T>  treedata=mhs::dataArray<T>(sub_region,"data");
		    mhs::dataArray<T>  conncection_data=mhs::dataArray<T>(sub_region,"connections");
		    
		    particle_thickness=(mhs::dataArray<T>(sub_region,"particle_thickness")).data[0];
		    
		      T * ptreedata=treedata.data;
		      unsigned int numpoints=treedata.dim[0];
		      unsigned int num_atrr=treedata.dim[1];
		      
		      if (num_atrr<CSimplePoint::num_attr)
			printf("Warning: num attr less than expected\n");
		      

		      std::size_t path_id_max=0;
		      
		      pt_data_indx.resize(numpoints);
		      printf("initializing %d points \n",(numpoints));
		      for (int i=0; i<numpoints; i++)
		      {
			CSimplePoint point;
			for (int a=0;a<CSimplePoint::num_attr;a++)
			{
			  if (a<num_atrr)
			      point.attributes[a]=ptreedata[a];
			  else 
			    point.attributes[a]=-1;
			}
			
// 			Vector<T,3> pos=point.get_pos();
			
			path_id_max=std::max(point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_PATH_ID],(T)path_id_max);
			if (point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_PATH_ID]>-1)
			  point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_PATH_ID]+=path_id_offset;
			
			

			max_restore_id=std::max(point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A],(T)max_restore_id);
			max_restore_id=std::max(point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B],(T)max_restore_id);
			
			
			point.update_from_attributes();
			point.region_id=t;
			
			
			
			pt_data.push_back(point);
			pt_data_indx[i]=&(pt_data.back());
// 			pt_data.back().init_pointer();
			
// 			pt_data.back().update_from_attributes();
			
			bbox[0]=std::max((T)bbox[0],point.get_position()[0]);
			bbox[2]=std::max((T)bbox[1],point.get_position()[1]);
			bbox[1]=std::max((T)bbox[2],point.get_position()[2]);
			max_scale=std::max(max_scale,point.get_scale());
			
			ptreedata+=treedata.dim[1];
		      }
		      printf("pts: %d\n",pt_data.size());
		      
		      path_id_offset+=path_id_max;
		      
		      T * pconncection_data=conncection_data.data;
		      unsigned int num_connections=conncection_data.dim[0];
		      
      // 		eg_data.resize(num_connections);
		      
		      printf("initializing edges \n");
		      for (int i=0; i<num_connections; i++)
		      {
			  int pointidxA=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXA];
			  int point_sideA=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_SIDEA];
			  int pointidxB=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXB];
			  int point_sideB=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_SIDEB];
			  
			  CSimplePoint & pointA=*(pt_data_indx[pointidxA]);
			  CSimplePoint & pointB=*(pt_data_indx[pointidxB]);
			  
			  CSimpleEdge edge;
			  edge.left=&pointA;
			  edge.right=&pointB;
			  edge.side_left=point_sideA;
			  edge.side_right=point_sideB;
			  eg_data.push_back(edge);
			  
			  pointA.edges[point_sideA].push_back(&(eg_data.back()));
			  pointB.edges[point_sideB].push_back(&(eg_data.back()));
			  
			  pconncection_data+=conncection_data.dim[1];
		      }
		      
		      printf("edges: %d\n",eg_data.size());
	    }
	    
	   
	    
	    
	          printf("max restore id: %d\n",max_restore_id);
		  restore_candidates.resize(max_restore_id+1);
		  
		  for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		  {
		    CSimplePoint & pt=*iter;
		    if (pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED]>0)
		    {
		      bool check=false;
		      if (pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]>-1)
		      {
			t_restore_me r;
			r.pt=&pt;
			r.side=0;
			restore_candidates[(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]].push_back(r);
			check=true;
		      }
		      if (pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]>-1)
		      {
			t_restore_me r;
			r.pt=&pt;
			r.side=1;
			restore_candidates[(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]].push_back(r);
			//restore_candidates[(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]].push_back(&pt);
			check=true;
		      }
// 		       printf("%u\n",&pt);
// 		    printf("%f\n",pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]);
// 		    printf("%f\n",pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]);
// 		    printf("%f\n",pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED]);		       
		      sta_assert_error(check);
		    }
		  }
		  
		printf("cleaning\n");
		 mhs::mex_dumpStringNOW();
		for (int i=0;i<restore_candidates.size();i++)
		{
		  
		  std::list<t_restore_me> & plits=restore_candidates[i];
		  
		  for (std::list<t_restore_me>::iterator iter=plits.begin();iter!=plits.end();iter++)
		  {
		    CSimplePoint & pt=*(iter->pt);
		    
// 		    printf("%u\n",&pt);
// 		    printf("%d\n",(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]);
// 		    printf("%d\n",(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A+1]);
// 		    printf("%d\n",(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]);
// 		    printf("%d\n",(int)pt.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED]);
		    

		    for (int e=0;e<2;e++)
		    {
		      if ((pt.edges[e].size()>0)&&(pt.attributes[(int)(CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A+e)]>-1)) 
		      {
			for (int b=0;b<pt.edges[e].size();b++)
			{
			    CSimpleEdge & edge=*pt.edges[e][b];
			    edge.deleteme=true;
			  
			    CSimplePoint & pointA=*edge.left;
			    CSimplePoint & pointB=*edge.right;
			    
			    CSimplePoint * points[2];
			    points[0]=&pointA;
			    points[1]=&pointB;
			    
// 			    Vector<T,3 > pA=pointA.get_pos();
// 			    Vector<T,3 > pB=pointB.get_pos();
			    

			    for (int e=0;e<2;e++)
			    {
			      if ((points[e]->edges[0].size()>1)||(points[e]->edges[1].size()>1)) 
			      {
				sta_assert_error(points[e]->unique_ids[0]==-1);
				sta_assert_error(points[e]->unique_ids[1]==-1);
				points[e]->deleteme=true;
				for (int d=0;d<2;d++)
				{
				  for (int k=0;k<points[e]->edges[d].size();k++)
				  {
				    points[e]->edges[d][k]->deleteme=true;
				  }
				}
			      }
			    }
			}
		      }
		    }
		   }
		}
		
		printf("edgs: %d\n",eg_data.size());
		std::size_t edges_removed=0;
		{
		  std::list<CSimpleEdge>::iterator iter=eg_data.begin();  
		  while (iter!=eg_data.end())
		  {
		    if (iter->deleteme)
		    {
		      CSimpleEdge & e=*iter;
		       sta_assert(iter->right->remove_edge(&e)==1);
		       sta_assert(iter->left->remove_edge(&e)==1);
		      iter=eg_data.erase(iter);
		      edges_removed++;
		    }else
		    {
		      iter++;
		    }
		  }
		}
		
		mhs::mex_dumpStringNOW();
		printf("pts: %d\n",pt_data.size());
		std::size_t bifurcations_removed=0;  
// 		std::size_t points_freezed=0;  
		{
		  std::list<CSimplePoint>::iterator iter=pt_data.begin();  
		  while  (iter!=pt_data.end())
		  {
		    if (iter->deleteme)
		    {
		      iter=pt_data.erase(iter);
		      bifurcations_removed++;
		    }else
		    {
		     iter++; 
		    }
		  }
		}
		
		mhs::mex_dumpStringNOW();
		printf("%d edges removed (left %d) and %d bifurcations removed  (left  %d)\n",edges_removed,eg_data.size(),bifurcations_removed,pt_data.size());


		std::size_t count=0;
		pt_data_indx.resize(pt_data.size());
		{
		  for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		  {
		    CSimplePoint & pt=*iter;
		    pt.id=count;
		    pt_data_indx[count++]=&pt;
		  }
		}
		
		
		
		int edges_restored=0;
		int bifurcations_restored=0;
		
		
		std::vector<std::size_t>  bifurcation_data_dim;
		T * p_bifurcation_data_backup=NULL;
		try
		{
		  p_bifurcation_data_backup=mhs::mex_getFieldPtr<T>(tracker_state,"bifurcation_backup_data",bifurcation_data_dim);
		  printf("bifurcation data: %d %d\n",bifurcation_data_dim[0],bifurcation_data_dim[1]);
		} catch (mhs::STAError & error)
		{
		    // do nothing
		}
		std::size_t backup_pts_restored=0;
		
// 		if (false)
		for (int i=0;i<restore_candidates.size();i++)
		{
		  
		  std::list<t_restore_me> & plits=restore_candidates[i];
		  
		  if ((plits.size()==2)&&(true)) //just an edge
		  {
		    
		      t_restore_me pts[2];
		      std::list<t_restore_me> & plits=restore_candidates[i];
		      int count=0;
		      for (std::list<t_restore_me>::iterator iter=plits.begin();iter!=plits.end();iter++)
		      {
			t_restore_me & pt=*(iter);
			pts[count++]=pt;
		      }
		      sta_assert_error(count==2);
		      CSimpleEdge edge;
		      edge.left=pts[0].pt;
		      edge.right=pts[1].pt;
		      edge.side_left=pts[0].side;
		      edge.side_right=pts[1].side;
		      
 		      eg_data.push_back(edge);
		      edges_restored++;
// 		      printf("%d %d ",pts[0].side,pts[1].side);
// 		      
 		      pts[0].pt->edges[pts[0].side].push_back(&(eg_data.back()));
 		      pts[1].pt->edges[pts[1].side].push_back(&(eg_data.back())); 
		    
		  }else if ((plits.size()==3)&&(true)) //bifurcation
		  {
		    
		    
		    
		    t_restore_me pts[3];
		    sta_assert_error(p_bifurcation_data_backup!=NULL);
		    CSimplePoint bif_center_point;
		    
		    std::list<t_restore_me> & plits=restore_candidates[i];
		    int count=0;
		    int bif_reg_id=-1; // the restored region_id might have changed. 
		    // assign the same id to all bif involved points (regarding collision)
		    for (std::list<t_restore_me>::iterator iter=plits.begin();iter!=plits.end();iter++)
		    {
		      t_restore_me & pt=*(iter);
		      pts[count]=pt;
		      pt.pt->set_type_bif();
		      bif_reg_id=pt.pt->region_id;
		      count++;
		    }
		    sta_assert_error(count==3);
		    sta_assert_error(bif_reg_id>-1);
		    
		    
		    //TODO probably precompute an index -> faster
		    int indx=-1;
		    //search for correct bif:
 		    for (int z=0;z<bifurcation_data_dim[1];z++)
		    {
		       if (p_bifurcation_data_backup[z*(CTracker<double,double,3>::POINT_A_Count+1)]==i)
		       {
			indx=z; 
		       }
		    }
		    sta_assert_error(indx<bifurcation_data_dim[1])
		    
		    memcpy( bif_center_point.attributes,
			  p_bifurcation_data_backup+1+indx*(CTracker<double,double,3>::POINT_A_Count+1),
			  sizeof(T)*CTracker<double,double,3>::POINT_A_Count);
		    backup_pts_restored++;
			
			
			pt_data.push_back(bif_center_point);
			CSimplePoint * p_bif_center_point=&(pt_data.back());
			
			for (int i=0;i<3;i++)    
			{
			  CSimpleEdge edge;
			  edge.left=pts[i].pt;
			  edge.side_left=pts[i].side;
			  
			  
			  
			  edge.right=p_bif_center_point;
			  edge.side_right=Points<T,Dim>::bifurcation_center_slot;
			  edge.is_set_to_bif=true;
			  
			  eg_data.push_back(edge);
			  pts[i].pt->edges[pts[i].side].push_back(&(eg_data.back()));
			  pts[i].pt->region_id=bif_reg_id;
			  
			  p_bif_center_point->edges[Points<T,Dim>::bifurcation_center_slot].push_back(&(eg_data.back()));
			  p_bif_center_point->region_id=bif_reg_id;
			  edges_restored++;
			}
		      
		      
		      
		      bifurcations_restored++;
		  } else if (plits.size()!=0)
		  {
		    
		   printf("just bullshit %d\n",plits.size()); 
		   
		  }
		  
// 		  for (std::list<CSimplePoint*>::iterator iter=plits.begin();iter!=plits.end();iter++)
// 		  {
// 		    CSimplePoint & pt=*(*iter);
// 		    
// 		  }
		}
		printf("%d edges and %d bifurcations restored\n",edges_restored,bifurcations_restored);
		
		
		
//TODO more clever method do rresolve collisions (one subgrid verses rest for example)
//FIX: collision with biifurcations!!! edge information is necessary!!
if (true)	
{
		printf("resolving collisions (thickness :%f)\n",particle_thickness);
		Points<T,Dim>::thickness=particle_thickness;
		bbox[0]+=1;
		bbox[1]+=1;
		bbox[2]+=1;
		printf("%d %d %d\n",bbox[0],bbox[1],bbox[2]);
		 T cell_size=10;
		 unsigned int grid_id=0;
		 tree=new OctTreeNode<T,Dim>(bbox,cell_size,grid_id,pt_data.size());
		 
		  tree->deletedata(false);
		  
		sta_assert_error(tree!=NULL);
 		int _count=0;
		for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		{
		  CSimplePoint & pt=*iter;
		    pt.deleteme=false;
		    pt.update_from_attributes();
// 		    pt.update_from_edge_info();
		  pt.init_pointer();
// 		  pt.position=pt.get_pos();
		  try
		  {
//  		    printf("%d\n",_count++);
//  		    printf("%d %d %d\n",bbox[0],bbox[1],bbox[2]);
//  		    pt.position.print();
 		    tree->insert(pt,true);
		   } catch (mhs::STAError error)
		  {
		      throw error;
		  }
		}
		
		for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		{
		    CSimplePoint & pt=*iter;
		    if (colliding2(*tree,pt,T(max_scale*2.5)))
		      {
//  			  printf("coll\n");
// 			  continue;
			  pt.deleteme=true;
			  for (int e=0;e<2;e++)
			  {
			    if (pt.edges[e].size()>0) 
			    {
			      for (int b=0;b<pt.edges[e].size();b++)
			      {
				  CSimpleEdge & edge=*pt.edges[e][b];
				  edge.deleteme=true;
				
				  CSimplePoint & pointA=*edge.left;
				  CSimplePoint & pointB=*edge.right;
				  
				  CSimplePoint * points[2];
				  points[0]=&pointA;
				  points[1]=&pointB;
				  
				  for (int e=0;e<2;e++)
				  {
				    if ((points[e]->edges[0].size()>1)||(points[e]->edges[1].size()>1)) 
				    {
				      sta_assert_error(points[e]->unique_ids[0]==-1);
				      sta_assert_error(points[e]->unique_ids[1]==-1);
				      points[e]->deleteme=true;
				      for (int d=0;d<2;d++)
				      {
					for (int k=0;k<points[e]->edges[d].size();k++)
					{
					  points[e]->edges[d][k]->deleteme=true;
					}
				      }
				    }
				  }
			      }
			    }
			  }
		      }
		}
		
		printf("deleting tree");
		
		for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		{
		  CSimplePoint & pt=*iter;
		  pt.unregister_from_grid(0);
		}
		if (tree!=NULL)
		      delete  tree;
			    
		
		printf("done\n");
		
		printf("edgs: %d\n",eg_data.size());
		edges_removed=0;
		{
		  std::list<CSimpleEdge>::iterator iter=eg_data.begin();  
		  while (iter!=eg_data.end())
		  {
		    if (iter->deleteme)
		    {
		      CSimpleEdge & e=*iter;
		       sta_assert(iter->right->remove_edge(&e)==1);
		       sta_assert(iter->left->remove_edge(&e)==1);
		      iter=eg_data.erase(iter);
		      edges_removed++;
		    }else
		    {
		      iter++;
		    }
		  }
		}
		
		mhs::mex_dumpStringNOW();
		printf("pts: %d\n",pt_data.size());
		bifurcations_removed=0;  
		{
		  std::list<CSimplePoint>::iterator iter=pt_data.begin();  
		  while  (iter!=pt_data.end())
		  {
		    if (iter->deleteme)
		    {
		      iter=pt_data.erase(iter);
		      bifurcations_removed++;
		    }else
		    {
		     iter++; 
		    }
		  }
		}
		
		mhs::mex_dumpStringNOW();
		printf("%d edges removed (left %d) and %d points removed  (left  %d)\n",edges_removed,eg_data.size(),bifurcations_removed,pt_data.size());


		count=0;
		pt_data_indx.resize(pt_data.size());
		{
		  for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		  {
		    CSimplePoint & pt=*iter;
		    pt.id=count;
		    pt_data_indx[count++]=&pt;
		  }
		}
}		
		
		
		
		
		
		plhs[0]= create_tracker_pt_state(pt_data,
		eg_data,
		tracker_cpy_smaple_state,-2,Vector<T,3>(0,0,0));
		
		int num_fileds=mxGetNumberOfFields(tracker_state);
		for (int i=0;i<num_fileds;i++) 
		{
		  const char * fieldname=mxGetFieldNameByNumber(tracker_state, i);
// 		  printf("%s %d\n",fieldname,strncmp(fieldname,"observer",7));
		  if (std::abs(strncmp(fieldname,"observer",7))==0)
		  {
		  printf("copying %s\n",fieldname);
		  mxAddField(plhs[0],fieldname);
		  mxSetField(plhs[0], 0,fieldname, mxDuplicateArray(mxGetField(tracker_state,0,fieldname)));
		  }
		}
	  
		
	    
	    
	  }else
	  {
	    printf("found data for one region  \n - > splitting regions\n");
	    if (nrhs>1)
	    {
	    num_tracker_states=1;
	    
	    mhs::dataArray<TData> regions=mhs::dataArray<TData>(prhs[1]);
	    
	    std::vector<Vector<T,3> > offsets;
	    std::vector<Vector<T,3> > shape;
	    
	    sta_assert_error(regions.dim.size()==2);
	    for (int r=0;r<regions.dim[1];r++)
	    {
	      Vector<T,3> o;
	      Vector<T,3> s;
	     for (int d=0;d<regions.dim[0];d++) 
	     {
	       printf("%f ",regions.data[d*regions.dim[1]+r]);
	       
	       if (d<3)
	       {
		 o[d]=regions.data[d*regions.dim[1]+r];
	       }else
	       {
		 s[d-3]=regions.data[d*regions.dim[1]+r];
	       }
	     }
// 	     o-=1;
	     
	     offsets.push_back(o);
	     shape.push_back(s);
	     printf("\n ");
	    }
	    
	    mhs::dataArray<T> treedata;
	    mhs::dataArray<T> conncection_data;

	    if (tracker_state!=NULL)
	    {
		sta_assert_error(mxIsStruct(tracker_state));
		//try
		{
		    treedata=mhs::dataArray<T>(tracker_state,"data");
		    conncection_data=mhs::dataArray<T>(tracker_state,"connections");
		}
		
		  T * ptreedata=treedata.data;
                unsigned int numpoints=treedata.dim[0];
		unsigned int num_atrr=treedata.dim[1];
		
		if (num_atrr<CSimplePoint::num_attr)
		  printf("Warning: num attr less than expected (filling up with 0s)\n");
		
// 		pt_data.resize(numpoints);
		pt_data_indx.resize(numpoints);
                printf("initializing %d points \n",(numpoints));
                for (int i=0; i<numpoints; i++)
		{
		  CSimplePoint point;
		   for (int a=0;a<CSimplePoint::num_attr;a++)
		   {
		     if (a<num_atrr)
			point.attributes[a]=ptreedata[a];
		     else 
		       point.attributes[a]=0;
		   }
		   
		   point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED]=0;
		   point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]=-1; 
		   point.attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]=-1; 
		   
		   Vector<T,3> p =point.get_pos();
		   for (int bb=0;bb<offsets.size();bb++)
		   {
		    bool inA= inbb(offsets[bb] , shape[bb], p);
		    if (inA)
		    {
		      sta_assert(point.region_id==-1);
		      point.region_id=bb;
		    }
		   }
		   
		   pt_data.push_back(point);
		   pt_data_indx[i]=&(pt_data.back());
		   ptreedata+=treedata.dim[1];
// 		   pt_data.back().get_pos().print();
		}
		printf("pts: %d\n",pt_data.size());
		
		T * pconncection_data=conncection_data.data;
                unsigned int num_connections=conncection_data.dim[0];
		
// 		eg_data.resize(num_connections);
		
                printf("initializing edges \n");
                for (int i=0; i<num_connections; i++)
                {
 		    int pointidxA=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXA];
                    int point_sideA=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_SIDEA];
                    int pointidxB=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_PINDXB];
                    int point_sideB=pconncection_data[CTracker<double,double,3>::EDGE_ATTRIBUTES::EDGE_A_SIDEB];
		    
//                     CSimplePoint & pointA=*(pt_data_indx[numpoints-1-pointidxA]);
//                     CSimplePoint & pointB=*(pt_data_indx[numpoints-1-pointidxB]);
		    
		    CSimplePoint & pointA=*(pt_data_indx[pointidxA]);
                    CSimplePoint & pointB=*(pt_data_indx[pointidxB]);
		    
		    CSimpleEdge edge;
		    edge.left=&pointA;
		    edge.right=&pointB;
		    edge.side_left=point_sideA;
		    edge.side_right=point_sideB;
		    eg_data.push_back(edge);
		    
		    pointA.edges[point_sideA].push_back(&(eg_data.back()));
		    pointB.edges[point_sideB].push_back(&(eg_data.back()));
		    
		    pconncection_data+=conncection_data.dim[1];
		}
		
		printf("edges: %d\n",eg_data.size());
		
		
		 printf("searching intersections\n");
		 mhs::mex_dumpStringNOW();
		std::size_t unique_id=0;
		//remove edges between regions
		//for (int i=0; i<num_connections; i++)
		for (std::list<CSimpleEdge>::iterator iter=eg_data.begin();  iter!=eg_data.end(); iter++)
		{
		  CSimpleEdge & edge=*iter;
		   
		   CSimplePoint & pointA=*edge.left;
                   CSimplePoint & pointB=*edge.right;
		   
		   CSimplePoint * points[2];
		   points[0]=&pointA;
		   points[1]=&pointB;
		  
// 		   Vector<T,3 > pA=pointA.get_pos();
// 		   Vector<T,3 > pB=pointB.get_pos();
		   
/*		   
		   pA.print();
		   pB.print();
		   */
		  int regA=pointA.region_id;
		  int regB=pointB.region_id;


		   {
		    if (regA==regB) // in
		    {
		      edge.region_id=regA;
		    }else //border
		    {
		      // is bifurcation point involved
		      // --> remove that sh**
		      for (int e=0;e<2;e++)
		      {
			if ((points[e]->edges[0].size()>1)||(points[e]->edges[1].size()>1)) 
			{
			  if (points[e]->unique_ids[0]>-1)
			  {
			  //printf("##################\n");
			  printf("del: (uid)%d  (type)%d\n",points[e]->unique_ids[0],(int)points[e]->attributes[CTracker<double,double,3>::POINT_ATTRIBUTES::POINT_A_TYPE]);
			  }
			  points[e]->deleteme=true;
			  points[e]->unique_ids[0]=unique_id;
			  
			  for (int d=0;d<2;d++)
			  {
			    for (int k=0;k<points[e]->edges[d].size();k++)
			    {
			      points[e]->edges[d][k]->deleteme=true;
			      points[e]->edges[d][k]->left->unique_ids[points[e]->edges[d][k]->side_left]=unique_id;
			      points[e]->edges[d][k]->right->unique_ids[points[e]->edges[d][k]->side_right]=unique_id;
			      
			      points[e]->edges[d][k]->left->set_type_seg();
			      points[e]->edges[d][k]->right->set_type_seg();
			    }
			  }
			}
		      }
// 		      
 		      sta_assert(((edge.side_left==0)||(edge.side_left==1)));
		      sta_assert(((edge.side_right==0)||(edge.side_right==1)));
 		      edge.left->unique_ids[edge.side_left]=unique_id;
 		      edge.right->unique_ids[edge.side_right]=unique_id;
// 			
		      edge.deleteme=true;

	      
		      unique_id++;
	
		      
		      
		    }
		   }
		}
		 mhs::mex_dumpStringNOW();
		 
		
		
		printf("edgs: %d\n",eg_data.size());
		std::size_t edges_removed=0;
		{
		  std::list<CSimpleEdge>::iterator iter=eg_data.begin();  
		  while (iter!=eg_data.end())
		  {
		    if (iter->deleteme)
		    {
		      
		      CSimpleEdge & e=*iter;
		       sta_assert(iter->right->remove_edge(&e)==1);
		       sta_assert(iter->left->remove_edge(&e)==1);
		      
		      iter=eg_data.erase(iter);
		       
		      edges_removed++;
		    }else
		    {
		      iter++;
		    }
		  }
		}
		
		 mhs::mex_dumpStringNOW();
		 
		 
		 std::size_t backup_pts=0;
		for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		{
		  CSimplePoint & pt=*iter;
		  if (pt.deleteme)
		    backup_pts++;
		}
		//T* bifurcation_data_backup
		std::size_t ndims[2];
		ndims[1]=backup_pts;
		ndims[0]=CTracker<double,double,3>::POINT_A_Count+1;
		bifurcation_data_backup= mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
      // 	  mxSetField(tdata,0,"connections", connections);
		T* bifurcation_data_backup_p= ( T * ) mxGetData (bifurcation_data_backup);
	  
		
		std::size_t backup_pts_backuped=0;
		printf("pts: %d\n",pt_data.size());
		std::size_t bifurcations_removed=0;  
		std::size_t points_freezed=0;  
		{
		  std::list<CSimplePoint>::iterator iter=pt_data.begin();  
		  while  (iter!=pt_data.end())
		  {
		    if (iter->deleteme)
		    {
		      
		      bifurcation_data_backup_p[backup_pts_backuped*(CTracker<double,double,3>::POINT_A_Count+1)]=iter->unique_ids[0];
		      memcpy( bifurcation_data_backup_p+1+backup_pts_backuped*(CTracker<double,double,3>::POINT_A_Count+1),
			      iter->attributes,
			      sizeof(T)*CTracker<double,double,3>::POINT_A_Count);
// 		      memcpy( bifurcation_data_backup_p+iter->unique_ids[0]*CTracker<double,double,3>::POINT_A_Count,
// 			      iter->attributes,
// 			      sizeof(T)*CTracker<double,double,3>::POINT_A_Count);
		      
		      
		      backup_pts_backuped++;
		       
		      iter=pt_data.erase(iter);
		      bifurcations_removed++;
		    }else 
		    {
		      bool f=false;
		      if (iter->unique_ids[0]>-1)
		      {
			iter->border_uid(0,iter->unique_ids[0]);
			iter->freeze();
			f=true;
		      }
		      if (iter->unique_ids[1]>-1)
		      {
			iter->border_uid(1,iter->unique_ids[1]);
			iter->freeze();
			f=true;
		      }
		      if (f)
			points_freezed++;
		      iter++;
		    }
		  }
		}
		sta_assert_error(backup_pts_backuped==backup_pts);
		 mhs::mex_dumpStringNOW();

		 
		printf("%d edges (left %d) and %d bifurcations removed and %d poitns freezed (left  %d)\n",edges_removed,eg_data.size(),bifurcations_removed,points_freezed,pt_data.size());
		
		
		std::size_t count=0;
		bool empty_observer=false;
		pt_data_indx.resize(pt_data.size());
		{
		for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		{
 		  CSimplePoint & pt=*iter;
		  pt.id=count;
		  if (pt.region_id==-1)
		  {
		   empty_observer=true; 
		  }
 		  pt_data_indx[count++]=&pt;
		}
		}
		
		
		
		
		
		{
		    int num_workers=offsets.size();
		    mwSize dim=1;
		    mwSize dims=1;
		    mwSize nfields=1;
		    
		    const char *field_names[] = {
			"W",			// single tracker states
		    };
		    
		    
		    
		    plhs[0]=mxCreateStructArray(dim, &dims, nfields,field_names);
		    //plhs[0]=mxCreateCellArray(dim, &dims);
		     mxArray * worker_state=mxGetField(plhs[0],0,(char *)("W"));
		    
		      dim=1;
		      dims=num_workers+(empty_observer ? 1 : 0);
		      nfields=3;
		      const char *field_namesW[] = {
			"A",			// status
			"shape",			// status
			"offset",			// status
		      };
	  // 	    mxArray * worker_settings=mxGetField(plhs[0],t,(char *)("W"));
		      
		      
		      if (bifurcation_data_backup!=NULL)
		      {
			    mxAddField(plhs[0],"bifurcation_backup_data");
			    mxSetField(plhs[0], 0,"bifurcation_backup_data", bifurcation_data_backup);
		      }
		      
		      int num_fileds=mxGetNumberOfFields(tracker_state);
		      for (int i=0;i<num_fileds;i++) 
		      {
			const char * fieldname=mxGetFieldNameByNumber(tracker_state, i);
			if (std::abs(strncmp(fieldname,"observer",7))==0)
			{
			printf("copying %s\n",fieldname);
			mxAddField(plhs[0],fieldname);
			mxSetField(plhs[0], 0,fieldname, mxDuplicateArray(mxGetField(tracker_state,0,fieldname)));
			}
		      }
		      
		      mxArray * settings=mxCreateStructArray(dim, &dims, nfields,field_namesW);
		     mxSetField(plhs[0],0,(char *)("W"),settings);
		     settings=mxGetField(plhs[0],0,(char *)("W"));
		     
		    std::size_t d=3;  
		    for (int t=0;t<offsets.size();t++)
		    {
		      //mxSetField(settings, 0,"A", tracker[t]->save_tracker_state());
// 		      mxSetField(settings, 0,"shape", mxDuplicateArray(mxGetField(mxGetField(tracker_state,t,(char *)("W")),0,(char *)("shape"))));
// 		      mxSetField(settings, 0,"offset", mxDuplicateArray(mxGetField(mxGetField(tracker_state,t,(char *)("W")),0,(char *)("offset"))));

		      printf("creating region %d of %d\n",t+1,offsets.size());
		      mhs::mex_dumpStringNOW();
		      
		      mxArray * data = mxCreateNumericArray (1,&d,mhs::mex_getClassId<T>(),mxREAL );
		      T *ptr= ( T * ) mxGetData (data );
		      ptr[0]=shape[t][2];
		      ptr[1]=shape[t][1];
		      ptr[2]=shape[t][0];
// 		      ptr[0]=shape[t][0];
// 		      ptr[1]=shape[t][1];
// 		      ptr[2]=shape[t][2];
		     
		      mxSetField(settings, t,"shape", data);
		      
		      data = mxCreateNumericArray (1,&d,mhs::mex_getClassId<T>(),mxREAL );
		      ptr= ( T * ) mxGetData (data );
		      
		      ptr[0]=offsets[t][2];
		      ptr[1]=offsets[t][1];
		      ptr[2]=offsets[t][0];
// 		      ptr[0]=offsets[t][0];
// 		      ptr[1]=offsets[t][1];
// 		      ptr[2]=offsets[t][2];
		      mxSetField(settings, t,"offset", data);
 		      
// 		      printf("A\n");
		      mxSetField(settings, t,"A", create_tracker_pt_state(pt_data,
			      eg_data,
			      tracker_state,t,offsets[t]));
		      /*printf("B\n");	*/	      
		     
		    }
		    if (empty_observer)
		    {
		      
		      printf("creating inactive region\n");
		      mhs::mex_dumpStringNOW();
		      Vector<T,3> o;
		      o=std::numeric_limits< T>::max();
		      Vector<T,3> s;
		      s=std::numeric_limits< T>::min();
		      for (std::list<CSimplePoint>::iterator iter=pt_data.begin();iter!=pt_data.end();iter++)
		      {
			CSimplePoint & pt=*iter;
			if (pt.region_id==-1)
			{
			  Vector<T,3> p=pt.get_pos();
			  for (int i=0;i<3;i++)
			  {
			   o[i]=std::min(p[i],o[i]);
			   s[i]=std::max(p[i],s[i]);
			  }
			}
		      }
		      s-=o;
		      
		      mxArray * data = mxCreateNumericArray (1,&d,mhs::mex_getClassId<T>(),mxREAL );
		      T *ptr= ( T * ) mxGetData (data );
		      ptr[0]=std::ceil(s[2]);
		      ptr[1]=std::ceil(s[1]);
		      ptr[2]=std::ceil(s[0]);
		      
		      mxSetField(settings, offsets.size(),"shape", data);
		      
		      data = mxCreateNumericArray (1,&d,mhs::mex_getClassId<T>(),mxREAL );
		      ptr= ( T * ) mxGetData (data );
		      ptr[0]=std::floor(o[2]);
		      ptr[1]=std::floor(o[1]);
		      ptr[2]=std::floor(o[0]);
		      mxSetField(settings, offsets.size(),"offset", data);
	
		      mxSetField(settings, offsets.size(),"A", create_tracker_pt_state(pt_data,
			      eg_data,
			      tracker_state,-1,o));
		    }
		    
		}
		
		
			
		
		
		
	    }
	    
	    
	    }else{
	     throw(mhs::STAError("error: expecting region array"));
	     }
	  }
	}else
	{
	  
	    if (nrhs>0)
	    {
	      mhs::dataArray<TData> regions=mhs::dataArray<TData>(prhs[0]);
	      
	      std::vector<Vector<T,3> > offsets;
	      std::vector<Vector<T,3> > shape;
	      
	      sta_assert_error(regions.dim.size()==2);
	      for (int r=0;r<regions.dim[1];r++)
	      {
		Vector<T,3> o;
		Vector<T,3> s;
	      for (int d=0;d<regions.dim[0];d++) 
	      {
		printf("%f ",regions.data[d*regions.dim[1]+r]);
		
		if (d<3)
		{
		  o[d]=regions.data[d*regions.dim[1]+r];
		}else
		{
		  s[d-3]=regions.data[d*regions.dim[1]+r];
		}
	      }
// 	      o-=1;
	      
	      offsets.push_back(o);
	      shape.push_back(s);
	      printf("\n ");
	      } 
	      
	    int num_workers=offsets.size();
	    mwSize dim=1;
	    mwSize dims=1;
	    mwSize nfields=1;
	    
	    const char *field_names[] = {
		"W",			// single tracker states
	    }; 
	   
	    
	    plhs[0]=mxCreateStructArray(dim, &dims, nfields,field_names);
	    mxArray * worker_state=mxGetField(plhs[0],0,(char *)("W"));
	  
	  
	    dims=num_workers;
	    nfields=2;
	    const char *field_namesW[] = {
	      "shape",			// status
	      "offset",			// status
	    };
	    mxArray * settings=mxCreateStructArray(dim, &dims, nfields,field_namesW);
	    mxSetField(plhs[0],0,(char *)("W"),settings);
	    settings=mxGetField(plhs[0],0,(char *)("W"));
		     
	    std::size_t d=3;  
	    for (int t=0;t<offsets.size();t++)
	    {
	      //mxSetField(settings, 0,"A", tracker[t]->save_tracker_state());
// 		      mxSetField(settings, 0,"shape", mxDuplicateArray(mxGetField(mxGetField(tracker_state,t,(char *)("W")),0,(char *)("shape"))));
// 		      mxSetField(settings, 0,"offset", mxDuplicateArray(mxGetField(mxGetField(tracker_state,t,(char *)("W")),0,(char *)("offset"))));

	      printf("creating region %d of %d\n",t+1,offsets.size());
	      mhs::mex_dumpStringNOW();
	      
	      mxArray * data = mxCreateNumericArray (1,&d,mhs::mex_getClassId<T>(),mxREAL );
	      T *ptr= ( T * ) mxGetData (data );
	      ptr[0]=shape[t][2];
	      ptr[1]=shape[t][1];
	      ptr[2]=shape[t][0];
	      
	      
	      mxSetField(settings, t,"shape", data);
	      
	      data = mxCreateNumericArray (1,&d,mhs::mex_getClassId<T>(),mxREAL );
	      ptr= ( T * ) mxGetData (data );
	      
	      ptr[0]=offsets[t][2];
	      ptr[1]=offsets[t][1];
	      ptr[2]=offsets[t][0];
	      mxSetField(settings, t,"offset", data);
	    }
	     }
	  
	}
    } catch (mhs::STAError & error)
    {
	// do nothing
      if (tree!=NULL)
	  delete  tree;
       printf("%s\n",error.str().c_str());
      return;
    }

    

}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
   _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
  
}







