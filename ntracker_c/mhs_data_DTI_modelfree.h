#ifndef MHS_DATA_DTI_H
#define MHS_DATA_DTI_H


#include "mhs_data.h"
#include "mhs_graphics.h"
typedef float REAL;
#include "DTI_tracking/SphereInterpolator.cpp"



    
 // TODO lmax= FeatureData.lmax/2+1:
// numbshells= FeatureData.numbvals
    
       //-------------------- computes model-model correlation function
    inline void mminteract_modelfree(REAL dot, REAL fac, REAL *alpha)
    {    

        dot = fabs(dot);
        
        if (dot==1)
        {
           for (int k = 0; k < lmax; k++)
                alpha[k] += fac;
           return;
        }
        
        alpha[0] += fac;
                
        REAL tmp1 = 1;
        REAL tmp2 = dot;
        REAL tmp3,tmp4;
        
        for (int k = 0; k < lmax-1; k++)
        {
            int l = 2*k+2;
            tmp3 = ((2*l-1)*dot*tmp2 - (l-1)*tmp1)/l;
            tmp4 = ((2*l+1)*dot*tmp3 - (l)*tmp2)/(l+1);
            alpha[k+1] += fac*tmp3;
            tmp1 = tmp3;
            tmp2 = tmp4;
            
        }
                
                                    
    }

    
    
    
  inline void getModelSignalCorrelation_modelfree(Particle *p, REAL *alpha)
	{
        
		int xint,yint,zint,spatindex;

        // get voxel index
        REAL Rx = p->R.x/voxsize_w;
        REAL Ry = p->R.y/voxsize_h;
        REAL Rz = p->R.z/voxsize_d;        
        xint = int(floor(Rx));
        yint = int(floor(Ry));
        zint = int(floor(Rz));
                

        int offs = (lmax-2)*numbshells;
        
        // get barycentric interpolation weights/indices
        int i0,i1,i2;
        REAL w0,w1,w2;
        #ifdef PARALLEL_OPENMP
        #pragma omp critical (SPHEREINTERPOL)
        #endif
        {
            sinterp->getInterpolation(p->N);
            i0 = sinterp->idx[0]*offs;
            i1 = sinterp->idx[1]*offs;
            i2 = sinterp->idx[2]*offs;
            w0 = sinterp->interpw[0];
            w1 = sinterp->interpw[1];
            w2 = sinterp->interpw[2];
        }
        
       
        // gather alpha vector needed for modelSignalCorr
        REAL weight;        
        const REAL sq23 = 1/(sqrt(2)*3);
                
        if (xint >= 0 && xint < w && yint >= 0 && yint < h && zint >= 0 && zint < d)
        {

            int sindex = idxmap[(xint + w*(yint+h*zint))];
            if (sindex >= 0)
            {
                spatindex =  sindex*(nip*offs + numbshells*7);      
                REAL tmp ;
                
                for(int k = 0; k< numbshells;k++)
                    for(int j = 2; j < lmax;j++)
                    {
                        int idx = j+k*lmax;
                        int idx2 = j-2+k*(lmax-2);
                        alpha[idx] += p->w*(dataimg[spatindex+i0+idx2]*w0 + dataimg[spatindex+i1+idx2]*w1 + dataimg[spatindex+i2+idx2]* w2);
                    }
                
                for(int k = 0; k< numbshells;k++)
                {
                    pVector n = p->N;
                    REAL qxx = (2*n.x*n.x - n.y*n.y - n.z*n.z)*sq23;
                    REAL qyy = (2*n.y*n.y - n.x*n.x - n.z*n.z)*sq23;
                    REAL qzz = (2*n.z*n.z - n.y*n.y - n.x*n.x)*sq23;
                    REAL qxy = n.x*n.y;
                    REAL qxz = n.x*n.z;
                    REAL qyz = n.z*n.y;
                    REAL d2 = p->w*3*(
                     qxx*dataimg[spatindex+nip*offs +1 + k*7 ] +
                     qyy*dataimg[spatindex+nip*offs +2 + k*7] +
                     qzz*dataimg[spatindex+nip*offs +3 + k*7] +
                     qxy*dataimg[spatindex+nip*offs +4 + k*7] +
                     qxz*dataimg[spatindex+nip*offs +5 + k*7] +
                     qyz*dataimg[spatindex+nip*offs +6 + k*7]);
                    
                    alpha[0+k*lmax] += dataimg[spatindex+nip*offs +0 + k*7 ]*p->w;
                    alpha[1+k*lmax] += d2;
                }
                    
            
            
            }
        }
	}
    
    

    

    //--------- computes data likelihood at a specific location
	inline REAL computeExternalEnergy(Particle *tp, Particle *dp)
	{
        
        REAL m = SpatProb(tp->R);
		if (m == 0)
		{
			return -INFINITY;
		}
        
        
        
        
        REAL *alphaSM_self = (REAL*) malloc(sizeof(REAL)*lmax*numbshells); 
        REAL *alphaMM_self = (REAL*) malloc(sizeof(REAL)*lmax); 
        for (int k = 0; k < lmax; k++)
            alphaMM_self[k] = 0;
        for (int k = 0; k < lmax*numbshells; k++)
            alphaSM_self[k] = 0;

        
        REAL *alphaSM = (REAL*) malloc(sizeof(REAL)*lmax*numbshells); 
        REAL *alphaMM = (REAL*) malloc(sizeof(REAL)*lmax); 
        for (int k = 0; k < lmax; k++)
            alphaMM[k] = 0;
        for (int k = 0; k < lmax*numbshells; k++)
            alphaSM[k] = 0;

        
        
        
        
        int cnt1;
        getModelSignalCorrelation_modelfree(tp, alphaSM_self)   ;         
        mminteract_modelfree(1, tp->w*tp->w,alphaMM_self );
        Particle **P1 = pcontainer->getCell(tp->R,cnt1);
        if (cnt1 > 0)
        {
            for (int k = 0; k < cnt1; k++)
            {
                Particle *p1 = P1[k];            
                if (p1 != dp)
                {
                    REAL fac = 2 * tp->w*p1->w;
                    mminteract_modelfree(tp->N*p1->N, fac,alphaMM_self );
                }
            }
            
        }

      
        int at_least_one_other = 0;
        for (int k = 0; k < cnt1; k++)
        {
            Particle *p1 = P1[k];            
            if (p1 != dp)
            {
                at_least_one_other = 1;
                getModelSignalCorrelation_modelfree(p1, alphaSM)   ;         
                for (int j = k; j < cnt1; j++)
                {
                    Particle *p2 = P1[j];
                    if (p2 != dp)
                    {
                        REAL fac = ((k!=j)?2:1) * p1->w*p2->w;
                        mminteract_modelfree(p2->N*p1->N, fac,alphaMM );
                    }
                }            
            }
        }			
            
        
        int show = 0;
        if (mtrand.rand() > 0.99)
        {
            show = 0;
        }
        
        
            
        int cigar = 0;
        for (int j = 0; j < numbshells;j++)
        {
            int k = 1;

            if (alphaSM[k+j*lmax]> 0 || alphaSM_self[k+j*lmax] > 0)
            {
                cigar = 1;
                break;
            }
            
            
        }
        dbgflag = 0;
        if (cigar == 1)
        {
            
            free(alphaSM);
            free(alphaMM);
            free(alphaSM_self);
            free(alphaMM_self);
            dbgflag = 0;
            
            return -INFINITY;
        }
        
        
        
        REAL m0 = 0;
        REAL m0_with =0;      
            
        REAL energy = 0;
       
        REAL myeps = 0.00000;
        
        int showss =mtrand.frand() >0.9999;
        
        for (int k = 0; k < lmax; k++)
        {
            REAL SM_with = 0;
            REAL SM = 0;
            for (int j = 0; j < numbshells; j++)
            {
                REAL sm_with = (alphaSM[k+j*lmax]+alphaSM_self[k+j*lmax]); 
                REAL sm = alphaSM[k+j*lmax];
                
                SM_with += sm_with*sm_with;
                SM += sm*sm;
            }
            const REAL myeps = 0;
            REAL pp = SM_with / fabs(alphaMM[k]+alphaMM_self[k]+myeps);
            REAL ww = SM / fabs(alphaMM[k]+myeps);
                  
               


            REAL facy = 1;// (lmax+1);
            
            
            if (at_least_one_other == 1)
                energy += (pp-ww) * facy;
            else
                energy += (pp ) * facy;
            
            

            if (show==1)
            {
              if (k == 1 && SM_with / (alphaMM[k]+alphaMM_self[k]+myeps)  > 1)
                {
                
//                fprintf(stderr,"%i %i(%f %f),\n ",k, at_least_one_other,  SM_with / (alphaMM[k]+alphaMM_self[k]) ,   SM / (alphaMM[k])  );
                  fprintf(stderr,"%i %i (%f %f %f %f),\n ",k, at_least_one_other, SM_with / (alphaMM[k]+alphaMM_self[k]+myeps) ,SM_with, alphaMM[k], alphaMM_self[k]);                
                
                  for (int k = 0; k < cnt1; k++)
                    {
                        Particle *p1 = P1[k];            
                        {
                            for (int j = k; j < cnt1; j++)
                            {
                                Particle *p2 = P1[j];
                                {
                                    fprintf(stderr,"%f ",p2->N*p1->N);
                                }
                            }            
                        }
                    }			
                    fprintf(stderr,"\n\n"  );
                
                
                }
            }

        }
        
        
//         if (show==1)
//         {
//            fprintf(stderr,"\n\n");
//         }
//         if (show==1)
//             fprintf(stderr,"%f,\n\n ",energy);
//         
        energy = energy*ex_strength;
        

        energy -= eigen_energy;
            
            
            
        free(alphaSM);
        free(alphaMM);
        free(alphaSM_self);
        free(alphaMM_self);
                    
        
        return energy;
        
	}
    

    
    


template <typename T,typename TData,int Dim>
class CDataDTI: public CData<T,TData,Dim>
{
private:
  
  
   int scale_power;
   
  SphereInterpolator *sinterp;
  const TData * dataimg;
  const TData * mask;
    
    T DataThreshold;
    T DataScale;
       T min_scale;
    T max_scale;
    const TData * scales;
    std::size_t num_scales;
public:

    float get_minscale() {
        return min_scale;
    };

    float get_maxscale() {
        return max_scale;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataDTI() {
        sinterp=NULL;
        
    }

    ~CDataDTI() {
      
      if (sinterp!=NULL)
	delete sinterp;
    }
    
    
// #ifdef D_USE_GUI
//     void read_controls ( const mxArray * handle ) {
//     
//     }
//     void set_controls ( const mxArray * handle ) {
//     
//     }
// #endif
    

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
//                 DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
//                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
             printf ( "Th: %f,  Da %f\n",DataThreshold,DataScale);
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
         printf ( "Th: %f,  Da %f\n",DataThreshold,DataScale);
    }
#endif

    void init ( const mxArray * feature_struct ) {

      
      sta_assert_error(feature_struct!=NULL);
      
      
      mxArray  * sinterpstruct=mxGetField(feature_struct,0,"sinterpstruct");
      
      sta_assert_error(sinterpstruct!=NULL);
      sinterp = new SphereInterpolator(sinterpstruct);
      
      
         try {
	           for ( int i=0; i<3; i++ ) {
		    this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
		  }
	    
	    std::swap ( this->shape[0],this->shape[2] );
	    
	    dataimg=mhs::dataArray<TData> ( feature_struct,"signal" ).data;
	    mask=mhs::dataArray<TData> ( feature_struct,"saliency_map" ).data;
        
	 scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();    
	    
	min_scale=scales[0];
	max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];

        
        } catch ( mhs::STAError error ) {
            throw error;
        }
      
      
	        


//         try {
//             const TData * scales=mhs::dataArray<TData> ( feature_struct,"trueSF" ).data;
//             true_SF= ( *scales ) ==1;
//             printf ( "found SF settings: %d\n",true_SF );
//         } catch ( mhs::STAError error ) {
//             throw error;
//         }


    }

    void set_params ( const mxArray * params=NULL ) {
        DataScale=1;
        DataThreshold=1;
 scale_power=2;
        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }


            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:

	
	  inline T evaluateODF(const Vector<T,Dim> & R_, const Vector<T,Dim>&N_, T len)
	{
	  int nip=sinterp->nverts;
		pVector R(R_[2],R_[1],R_[0]);
		pVector N(N_[2],N_[1],N_[0]);
	  
		const int CU = 10;
		pVector Rs;
		REAL Dn = 0;
		int xint,yint,zint,spatindex;

		sinterp->getInterpolation(N);

		for (int i=-CU; i <= CU;i++)
		{
			Rs = R + (N * len) * ((REAL)i/CU);
        
//             Rs.storeXYZ();
//             REAL Rx = pVector::store[0]/voxsize_w-0.5;
//             REAL Ry = pVector::store[1]/voxsize_h-0.5;
//             REAL Rz = pVector::store[2]/voxsize_d-0.5;
			
			
	    REAL Rx = Rs.x-0.5;
            REAL Ry = Rs.y-0.5;
            REAL Rz = Rs.z-0.5;


            xint = int(floor(Rx));
            yint = int(floor(Ry));
            zint = int(floor(Rz));
	
	    std::size_t d=this->shape[0];
	    std::size_t h=this->shape[1];
	    std::size_t w=this->shape[2];
			
			if (xint >= 0 && xint < w-1 && yint >= 0 && yint < h-1 && zint >= 0 && zint < d-1)
			{
				REAL xfrac = Rx-xint;
				REAL yfrac = Ry-yint;
				REAL zfrac = Rz-zint;
			
				REAL weight;
							
				weight = (1-xfrac)*(1-yfrac)*(1-zfrac);
				spatindex = (xint + w*(yint+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
	
				weight = (xfrac)*(1-yfrac)*(1-zfrac);
				spatindex = (xint+1 + w*(yint+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (1-xfrac)*(yfrac)*(1-zfrac);
				spatindex = (xint + w*(yint+1+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (1-xfrac)*(1-yfrac)*(zfrac);
				spatindex = (xint + w*(yint+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (xfrac)*(yfrac)*(1-zfrac);
				spatindex = (xint+1 + w*(yint+1+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (1-xfrac)*(yfrac)*(zfrac);
				spatindex = (xint + w*(yint+1+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (xfrac)*(1-yfrac)*(zfrac);
				spatindex = (xint+1 + w*(yint+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (xfrac)*(yfrac)*(zfrac);
				spatindex = (xint+1 + w*(yint+1+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
					
			}


		}
		
		Dn *= 1.0/(2*CU+1); 
		return Dn;
	}


public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
    ) {


        try {
	  
	    Vector<T,Dim> position=position_org;
            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->shape[0] ) || ( Y+1>=this->shape[1] ) || ( X+1>=this->shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }
            
            std::size_t index=((Z*this->shape[1])+Y)*this->shape[2]+X;
            if (!(mask[index]>0))
	      return false;
            
	    //result=1;
            result=-evaluateODF(position_org, direction, 1)*DataScale;
	    
	     T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=radius;
                break;
            case 2:
                scale1=radius*radius;
                break;
            case 3:
                scale1=radius*radius*radius;
                break;
            }
            
            result*=scale1;
            

//             T wz= ( position[0]-Z );
//             T wy= ( position[1]-Y );
//             T wx= ( position[2]-X );
// 
// 	    sinterp


// 	    T value;
// 	    data_interpolate<T,T,TData>(position);
// 	    
// 	    const Vector<T,3> & pos,
// 	      D & value,
// 	      const TData  *  img,
// 	      const std::size_t shape[],
// 	      std::size_t stride=1,
// 	      std::size_t indx=0,
// 	      int mode=1)
	    
//             T stdv=0;
//             if ( !data_interpolate_polynom<T,T,TData> ( position,multi_scale,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
//                 return false;
//             };
//             stdv= ( std::max ( stdv,T ( 0 ) ) );
	    
	  
	    //sinterp
            

        } catch ( mhs::STAError error ) {
            throw error;
        }
        
     

        return true;
    }
};

#endif


