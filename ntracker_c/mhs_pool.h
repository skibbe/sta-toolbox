#ifndef MHS_POOL_H
#define MHS_POOL_H


template<typename T,int Dim> class OctPoints;

//static int classid=0;





template<class C>
class pool_element
{
    friend C;
    friend class pool<C>;
protected:

    static int classid;

    std::size_t poolid;

    virtual void clear() {
#ifdef _POOL_TEST
        exist=false;
#endif
    }

public:
#ifdef _POOL_TEST
    bool exist;
#endif

    pool_element ( const pool_element& copy ) {

    }

    pool_element& operator= ( const pool_element& rhs ) {

        return *this;
    }

    pool_element() {
        clear();
    }
};

template<class C>
class pool
{
    friend C;
    friend class pool_element<C>;

protected:


    std::size_t pool_size;
    std::size_t currently_used;
    std::size_t currently_free;
    C * mem;
    //std::size_t * free_addresses;
    C ** free_addresses;
    std::size_t call_count[2];
public:
    //NOTE not really confident
    bool is_valid_adress ( const C * addr ) {
        if ( ( addr<mem ) || ( addr>mem+pool_size-1 ) ) {
            //printf("%u %u %u\n",mem,addr,mem+pool_size-1);
            return false;
        }
        return true;
    }

    //NOTE not really confident
    bool is_active_adress ( const C * addr ) {
        if ( ( addr<mem ) || ( addr>mem+pool_size-1 ) ) {
            //printf("%u %u %u\n",mem,addr,mem+pool_size-1);
            //return false;
            if ( ( addr->poolid<currently_free ) || ( addr->poolid>pool_size-1 ) ) {
                return false;
            }
        }
        return true;
    }

    const C ** getMemEnd() {
        return ( const C ** ) & ( free_addresses[pool_size-1] );
    }

    const C ** getMem() {
        return ( const C ** ) free_addresses+currently_free;
    }
    std::size_t get_numpts() {
        return currently_used;
    };

    C ** getMemW() {
        return ( C ** ) free_addresses+currently_free;
    }
    C * get_mem_ptr() {
        return mem;
    };
    C ** get_free_addresses_ptr() {
        return free_addresses;
    };
    std::size_t get_pool_size() {
        return pool_size;
    };

    C * create_obj() {
        call_count[1]++;
//       printf("(%u %u) %u %u %u\n",call_count[0],call_count[1],currently_used,currently_free,pool_size);
        //printf("\n\nsdsdsdsdsds 3\n\n");
        C * obptr=NULL;
#ifdef _MCPU
        #pragma omp critical (classid)
#endif
        {
	    sta_assert_error( currently_used<pool_size );
	  
            // printf("\n\nsdsdsdsdsds 3.11\n\n");
            obptr=*free_addresses;
            //printf("\n\nsdsdsdsdsds 3.12\n\n");
            sta_assert_debug0 ( obptr>=mem );
            sta_assert_debug0 ( obptr<mem+pool_size );
            //  printf("\n\nsdsdsdsdsds 3.13\n\n");

#ifdef _POOL_TEST
            sta_assert_debug0 ( currently_used<pool_size );
            // printf("\n\nsdsdsdsdsds 3.14\n\n");
            sta_assert_debug0 ( currently_used>=0 );
            //   printf("\n\nsdsdsdsdsds 3.15\n\n");
            sta_assert_debug0 ( currently_free>0 );
            //    printf("\n\nsdsdsdsdsds 3.^2\n\n");
            sta_assert_debug0 ( !obptr->exist );
//       printf("\n\nsdsdsdsdsds 3.^1\n\n");
            //  printf("\n\nsdsdsdsdsds 3.1\n\n");
            obptr->exist=true;
//       sta_assert_debug0(obptr->owner==NULL);
#endif
//         printf("\n\nsdsdsdsdsds 3.1\n\n");
            currently_free--;
            std::swap ( *free_addresses,free_addresses[currently_free] );
            //    printf("\n\nsdsdsdsdsds 3.2\n\n");
            obptr->poolid=currently_free;
            currently_used++;
            //    printf("\n\nsdsdsdsdsds 4\n\n");
        }

        return obptr;
    }


    void delete_obj ( C * & obj ) {
        call_count[0]++;
        //  printf("\n\nsdsdsdsdsds 1\n\n");
#ifdef _POOL_TEST
        sta_assert_debug0 ( currently_free<pool_size );
        sta_assert_debug0 ( currently_used>0 );
        sta_assert_debug0 ( currently_free>=0 );
        if ( !obj->exist ) {
            printf ( "id %u\n",classid );
        }


        sta_assert_debug0 ( obj->exist );
#endif
#ifdef _MCPU
        #pragma omp critical (classid)
#endif
        {

            std::size_t pool_id=obj->poolid;

#ifdef _POOL_TEST
            sta_assert_debug0 ( pool_id>=currently_free );
            sta_assert_debug0 ( free_addresses[currently_free]->exist );
#endif


            free_addresses[currently_free]->poolid=pool_id;
            std::swap ( free_addresses[pool_id],free_addresses[currently_free] );
            currently_used--;
            currently_free++;
            obj->clear();
            //printf("\n\nsdsdsdsdsds 2\n\n");
        }
//       printf("done (%d ,%d)\n",obj->poolid,pool_id);
        //NOTE we make the pointer invalid
        obj=NULL;
    }

    void delete_obj_old ( C * obj ) {
        call_count[0]++;
        //  printf("\n\nsdsdsdsdsds 1\n\n");
#ifdef _POOL_TEST
        sta_assert_debug0 ( currently_free<pool_size );
        sta_assert_debug0 ( currently_used>0 );
        sta_assert_debug0 ( currently_free>=0 );
        if ( !obj->exist ) {
            printf ( "id %u\n",classid );
        }


        sta_assert_debug0 ( obj->exist );
#endif
#ifdef _MCPU
        #pragma omp critical (classid)
#endif
        {

            std::size_t pool_id=obj->poolid;

#ifdef _POOL_TEST
            sta_assert_debug0 ( pool_id>=currently_free );
            sta_assert_debug0 ( free_addresses[currently_free]->exist );
#endif


            free_addresses[currently_free]->poolid=pool_id;
            std::swap ( free_addresses[pool_id],free_addresses[currently_free] );
            currently_used--;
            currently_free++;
            obj->clear();
            //printf("\n\nsdsdsdsdsds 2\n\n");
        }
//       printf("done (%d ,%d)\n",obj->poolid,pool_id);
    }

    pool ( std::size_t size,std::string poolname="x" ) {
        #pragma omp critical
        {
            pool_element<C>::classid++;
            printf ( "init pool %d (label %s) ..",pool_element<C>::classid,poolname.c_str() );
        }
        currently_used=0;
        currently_free=size;
        pool_size=size;
        mem=NULL;
        printf ( "(alloc main: %u)" ,pool_size);
        mem=new C[pool_size];
        printf ( "(alloc dict)" );
        free_addresses=new C*[pool_size];
// printf("0");
        printf ( "(init)" );
        for ( std::size_t i=0; i<pool_size; i++ ) {
            free_addresses[i]=mem+i;
            free_addresses[i]->clear();
        }
        /*printf("1");*/
//      free_addresses= new std::size_t[pool_size];
//      for (std::size_t i=0;i<pool_size;i++)
//      {
// 	free_addresses[i]=i;
//      }
        call_count[0]=call_count[1]=0;
        printf ( " done\n" );
    }

    ~pool() {
        delete [] mem;
        delete [] free_addresses;
    }

};

template<class C>
int pool_element<C>::classid=0;

/*template<class C>
int pool<C>::classid=0;*/


#endif
