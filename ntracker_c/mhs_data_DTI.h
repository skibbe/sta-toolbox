#ifndef MHS_DATA_DTI_H
#define MHS_DATA_DTI_H


#include "mhs_data.h"
#include "mhs_graphics.h"
typedef float REAL;
#include "DTI_tracking/SphereInterpolator.cpp"

template <typename T,typename TData,int Dim>
class CDataDTI: public CData<T,TData,Dim>
{
private:
  
  
   int scale_power;
   
  SphereInterpolator *sinterp;
  const TData * dataimg;
  const TData * mask;
    
    T DataThreshold;
    T DataScale;
       T min_scale;
    T max_scale;
    const TData * scales;
    std::size_t num_scales;
public:

    float get_minscale() {
        return min_scale;
    };

    float get_maxscale() {
        return max_scale;
    };

    float get_scalecorrection() {
        return 1;
    };


    CDataDTI() {
        sinterp=NULL;
        
    }

    ~CDataDTI() {
      
      if (sinterp!=NULL)
	delete sinterp;
    }
    
    
// #ifdef D_USE_GUI
//     void read_controls ( const mxArray * handle ) {
//     
//     }
//     void set_controls ( const mxArray * handle ) {
//     
//     }
// #endif
    

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                DataThreshold=- ( * ( ( double* ) mxGetPr ( parameter ) ) );
                DataScale=* ( ( ( double* ) mxGetPr ( parameter ) ) +2 );
//                 DataScaleGradVessel=* ( ( ( double* ) mxGetPr ( parameter ) ) +3 );
//                 DataScaleGradSurface=* ( ( ( double* ) mxGetPr ( parameter ) ) +4 );
            }
             printf ( "Th: %f,  Da %f\n",DataThreshold,DataScale);
        }
    }
    void set_controls ( const mxArray * handle ) {
        if ( handle!=NULL ) {
            const mxArray *
            parameter= mxGetField ( handle,0, ( char * ) ( "tracker_data" ) );
            if ( parameter!=NULL ) {
                * ( ( double* ) mxGetPr ( parameter ) ) =-DataThreshold;
                * ( ( ( double* ) mxGetPr ( parameter ) ) +2 ) =DataScale;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +3 ) =DataScaleGradVessel;
//                 * ( ( ( double* ) mxGetPr ( parameter ) ) +4 ) =DataScaleGradSurface;
            }
        }
         printf ( "Th: %f,  Da %f\n",DataThreshold,DataScale);
    }
#endif

    void init ( const mxArray * feature_struct ) {

      
      sta_assert_error(feature_struct!=NULL);
      
      
      mxArray  * sinterpstruct=mxGetField(feature_struct,0,"sinterpstruct");
      
      sta_assert_error(sinterpstruct!=NULL);
      sinterp = new SphereInterpolator(sinterpstruct);
      
      
         try {
	           for ( int i=0; i<3; i++ ) {
		    this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
		  }
	    
	    std::swap ( this->shape[0],this->shape[2] );
	    
	    dataimg=mhs::dataArray<TData> ( feature_struct,"signal" ).data;
	    mask=mhs::dataArray<TData> ( feature_struct,"saliency_map" ).data;
        
	 scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
            num_scales=mhs::dataArray<TData> ( feature_struct,"scales" ).get_num_elements();    
	    
	min_scale=scales[0];
	max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];

        
        } catch ( mhs::STAError error ) {
            throw error;
        }
      
      
	        


//         try {
//             const TData * scales=mhs::dataArray<TData> ( feature_struct,"trueSF" ).data;
//             true_SF= ( *scales ) ==1;
//             printf ( "found SF settings: %d\n",true_SF );
//         } catch ( mhs::STAError error ) {
//             throw error;
//         }


    }

    void set_params ( const mxArray * params=NULL ) {
        DataScale=1;
        DataThreshold=1;
 scale_power=2;
        if ( params!=NULL ) {
            try {

                if ( mhs::mex_hasParam ( params,"DataScale" ) !=-1 ) {
                    DataScale=mhs::mex_getParam<T> ( params,"DataScale",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"DataThreshold" ) !=-1 ) {
                    DataThreshold=mhs::mex_getParam<T> ( params,"DataThreshold",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"scale_power" ) !=-1 ) {
                    scale_power=mhs::mex_getParam<int> ( params,"scale_power",1 ) [0];
                }


            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

    }

private:



private:

	
	  inline T evaluateODF(const Vector<T,Dim> & R_, const Vector<T,Dim>&N_, T len)
	{
	  int nip=sinterp->nverts;
		pVector R(R_[2],R_[1],R_[0]);
		pVector N(N_[2],N_[1],N_[0]);
	  
		const int CU = 10;
		pVector Rs;
		REAL Dn = 0;
		int xint,yint,zint,spatindex;

		sinterp->getInterpolation(N);

		for (int i=-CU; i <= CU;i++)
		{
			Rs = R + (N * len) * ((REAL)i/CU);
        
//             Rs.storeXYZ();
//             REAL Rx = pVector::store[0]/voxsize_w-0.5;
//             REAL Ry = pVector::store[1]/voxsize_h-0.5;
//             REAL Rz = pVector::store[2]/voxsize_d-0.5;
			
			
	    REAL Rx = Rs.x-0.5;
            REAL Ry = Rs.y-0.5;
            REAL Rz = Rs.z-0.5;


            xint = int(floor(Rx));
            yint = int(floor(Ry));
            zint = int(floor(Rz));
	
	    std::size_t d=this->shape[0];
	    std::size_t h=this->shape[1];
	    std::size_t w=this->shape[2];
			
			if (xint >= 0 && xint < w-1 && yint >= 0 && yint < h-1 && zint >= 0 && zint < d-1)
			{
				REAL xfrac = Rx-xint;
				REAL yfrac = Ry-yint;
				REAL zfrac = Rz-zint;
			
				REAL weight;
							
				weight = (1-xfrac)*(1-yfrac)*(1-zfrac);
				spatindex = (xint + w*(yint+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
	
				weight = (xfrac)*(1-yfrac)*(1-zfrac);
				spatindex = (xint+1 + w*(yint+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (1-xfrac)*(yfrac)*(1-zfrac);
				spatindex = (xint + w*(yint+1+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (1-xfrac)*(1-yfrac)*(zfrac);
				spatindex = (xint + w*(yint+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (xfrac)*(yfrac)*(1-zfrac);
				spatindex = (xint+1 + w*(yint+1+h*zint)) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (1-xfrac)*(yfrac)*(zfrac);
				spatindex = (xint + w*(yint+1+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (xfrac)*(1-yfrac)*(zfrac);
				spatindex = (xint+1 + w*(yint+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
				weight = (xfrac)*(yfrac)*(zfrac);
				spatindex = (xint+1 + w*(yint+1+h*(zint+1))) *nip;
				Dn += (dataimg[spatindex + sinterp->idx[0]]*sinterp->interpw[0] + dataimg[spatindex + sinterp->idx[1]]*sinterp->interpw[1] + dataimg[spatindex + sinterp->idx[2]]* sinterp->interpw[2])*weight;
			
					
			}


		}
		
		Dn *= 1.0/(2*CU+1); 
		return Dn;
	}


public:

    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim> & position_org,
        T radius
        ,const Points<T,Dim> * p_point=NULL
	//,const Points<T,Dim> * org_point=NULL        
#ifdef D_USE_GUI
        ,std::list<std::string>  * debug=NULL
#endif
    ) {


        try {
	  
	    Vector<T,Dim> position=position_org;
            int Z=std::floor ( position[0] );
            int Y=std::floor ( position[1] );
            int X=std::floor ( position[2] );

            if ( ( Z+1>=this->shape[0] ) || ( Y+1>=this->shape[1] ) || ( X+1>=this->shape[2] ) ||
                    ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                return false;
            }
            
            std::size_t index=((Z*this->shape[1])+Y)*this->shape[2]+X;
            if (!(mask[index]>0))
	      return false;
            
	    //result=1;
            result=-evaluateODF(position_org, direction, 1)*DataScale;
	    
	     T scale1=1;
            switch ( scale_power ) {
            case 1:
                scale1=radius;
                break;
            case 2:
                scale1=radius*radius;
                break;
            case 3:
                scale1=radius*radius*radius;
                break;
            }
            
            result*=scale1;
            

//             T wz= ( position[0]-Z );
//             T wy= ( position[1]-Y );
//             T wx= ( position[2]-X );
// 
// 	    sinterp


// 	    T value;
// 	    data_interpolate<T,T,TData>(position);
// 	    
// 	    const Vector<T,3> & pos,
// 	      D & value,
// 	      const TData  *  img,
// 	      const std::size_t shape[],
// 	      std::size_t stride=1,
// 	      std::size_t indx=0,
// 	      int mode=1)
	    
//             T stdv=0;
//             if ( !data_interpolate_polynom<T,T,TData> ( position,multi_scale,NULL,local_stdv, num_alphas_local_stdv,stdv,feature_shape.v ) ) {
//                 return false;
//             };
//             stdv= ( std::max ( stdv,T ( 0 ) ) );
	    
	  
	    //sinterp
            

        } catch ( mhs::STAError error ) {
            throw error;
        }
        
     

        return true;
    }
};

#endif


