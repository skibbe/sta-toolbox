//#define _POOL_TEST
//#define _NUM_THREADS_ 4


#define _DEBUG_CHECKS_0_

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"


#include "graph_rep.h"
#include "specialfunc.h"


#include "mhs_data_Hessian.h"
#include "mhs_data_DTI.h"
#include "mhs_data_SD.h"
#include "mhs_data_SD_new.h"
#include "mhs_data_SD_SH.h"
#include "mhs_data_SD_SHorg.h"
#include "mhs_data_Spines.h"
#include "mhs_data_SD_SHsingle.h"
#include "mhs_data_nodata.h"
#include "mhs_data_spikes.h"

#ifdef _USE_CUDA_
  #include "mhs_data_GPU.h"	
#endif 


#include <thread>   
#include <chrono>   
#include <time.h> 


#ifndef D_USE_GUI    
  #include "nogui_user_input.h"
#endif



const int Dim=3;


/*
proposal DEATH   , t: 0.0165 (10^4 ms) [0.0000 : 3.5310],  calls   13750983, accept      89504 /   1%
proposal B_TDEATH, t: 0.0167 (10^4 ms) [0.0000 : 3.9506],  calls   13750009, accept        437 /   1%
proposal B_BIRTHS, t: 0.0494 (10^4 ms) [0.0000 : 100.6103],  calls   13755885, accept      37331 /   1%
proposal X_BIRTH , t: 0.0528 (10^4 ms) [0.0000 : 98.2189],  calls   13748122, accept        827 /   1%
proposal C_DEATH , t: 0.1008 (10^4 ms) [0.0000 : 98.2690],  calls   13744912, accept      55037 /   1%
proposal I_DEATH , t: 0.1027 (10^4 ms) [0.0000 : 100.5483],  calls   13748447, accept        191 /   1%
proposal BIRTH   , t: 0.1098 (10^4 ms) [0.0095 : 105.9794],  calls   68752576, accept      46601 /   1%
proposal X_DEATH , t: 0.1239 (10^4 ms) [0.0000 : 100.4505],  calls   13746517, accept       1096 /   1%
proposal CONNECT , t: 0.1297 (10^4 ms) [0.0000 : 100.3504],  calls   13758653, accept     167483 /   2%
proposal C_BIRTH , t: 0.2040 (10^4 ms) [0.0000 : 99.9999],  calls   13750641, accept      88716 /   1%
proposal I_BIRTH , t: 0.2131 (10^4 ms) [0.0000 : 100.4910],  calls   13743970, accept       1004 /   1%
proposal MSR     , t: 0.2203 (10^4 ms) [0.0000 : 90.1985],  calls   13748362, accept     255254 /   2%
proposal B_TBIRTH, t: 0.3021 (10^4 ms) [0.0000 : 102.8705],  calls   13748444, accept       9475 /   1%
proposal B_DEATHS, t: 0.3705 (10^4 ms) [0.0000 : 104.8803],  calls   13752478, accept      46361 /   1%
*/



//matlab -nojvm -nodisplay -nosplash  -D"valgrind --leak-check=full --undef-value-errors=yes --track-origins=yes --show-reachable=yes --error-limit=no  -v --num-callers=20 --leak-resolution=high   --log-file=valgrind.log" -r "load valgrind_debug; ntrack(FeatureData,foptions);exit;"

/*!
 *
 *  MAIN FUNCTION
 *
 * */
template <typename T,typename TData>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
debug_count=1;  
  
{
  

    printf("TODO:\n");
    printf("BIF prop: freeze topology\n");
    printf("BIF prop: consider_birth_death_ratio\n");
    printf("BIF prop: sample_scale_temp_dependent\n");
     printf("BIF prop: dynami edge length\n");
    printf("BIF track: saliency\n");
    printf("BIF prop: terminal collision costs\n");
    printf("if (found==1) may not a valid check if tree is conntree (like it was inc connect propo)\n");
    printf("graph_Rep : grad_normalize_with_volume\n");
    printf("insert B/D bug? (currently don't use it)\n");
    printf("change a/temp+b/temp to (a+b)/temp) / currently use E*opt_temp update_energy to get rid of temp depndenxy\n");
    
    printf("###################\n");
  
  
  
    CData<T,TData,Dim> *    data_fun=NULL;
    CEdgecost<T,Dim> *      edgecost_fun=NULL;
    CTracker<TData,T,Dim> ** tracker=NULL;
    Collision<T,Dim> *		collision_fun=NULL;
       
    
    
    CDataGrid<T,TData,Dim> ** grid_helper_data_fun;
    
    
    std::size_t min_pts_per_tracker=500000;
    int num_workers=1;  
    std::vector<Vector<std::size_t,3> > worker_regions;

    #ifdef D_USE_GUI    
      CTrackerRenderer<TData,T,Dim> * renderer=NULL;
      std::size_t * gui_window_handle_ptr=NULL;
      std::size_t   gui_window_handle;
      
    #else
	C_nogui_command * no_gui_user_cmd=NULL;
    #endif  
      

    int paramindx=0;
    const mxArray * feature_struct=NULL;
    const mxArray * tracker_state=NULL;
    const mxArray * cam_data=NULL;
    const mxArray * params=NULL;
    const mxArray * seeds=NULL;
    
//      mxArray * tracker_state_cell=NULL;
    int num_tracker_states=0;
    bool tracker_state_multitracker=false;


    
    try
    {
	for (std::size_t i=0;i<nrhs;i++)
	{
	    if ((mxIsStruct(prhs[i])) && (mxGetField ( prhs[i],0, ( char * ) ( "datafunc" ) )!=NULL))
	    {
	      sta_assert_error(feature_struct==NULL)
	      feature_struct=prhs[i];
	    }else 
	    if ((mxIsStruct(prhs[i])) && 
	      ((mxGetField ( prhs[i],0, ( char * ) ( "data" ) )!=NULL)&& (mxGetField ( prhs[i],0, ( char * ) ( "connections" ) )!=NULL))||
	      (mxGetField ( prhs[i],0, ( char * ) ( "W" ) )!=NULL)
	      )
	    {
	      sta_assert_error(tracker_state==NULL)
	      tracker_state=prhs[i];
	    }else 
	    if ((mxIsStruct(prhs[i])) && (mxGetField ( prhs[i],0, ( char * ) ( "cam" ) )!=NULL))
	    {
	      sta_assert_error(cam_data==NULL)
	      cam_data=prhs[i];
	    }else if (i==nrhs-1)
	    {
	       params=prhs[i];
	    }else 
	      sta_assert_error(0!=1);
	}
      
      
	bool debug_compute_costs=false;

	int collision_fun_id=-1;
	
	
	if (params!=NULL)
	{
  
	  if (mhs::mex_hasParam(params,"debug_compute_costs")!=-1)
                debug_compute_costs=mhs::mex_getParam<bool>(params,"debug_compute_costs",1)[0];
	  
	  if (mhs::mex_hasParam(params,"collision_fun_id")!=-1)
                collision_fun_id=mhs::mex_getParam<int>(params,"collision_fun_id",1)[0];
	  
	  if (mhs::mex_hasParam(params,"seeds")!=-1)
                seeds=mhs::mex_getParamPtr(params,"seeds");
      
	  if (mhs::mex_hasParam(params,"min_pts_per_tracker")!=-1)
                min_pts_per_tracker=mhs::mex_getParam<std::size_t>(params,"min_pts_per_tracker",1)[0];
      
	  
	     #ifdef D_USE_GUI    
	  if (mhs::mex_hasParam(params,"handle")!=-1)
	  {
	      gui_window_handle=mhs::mex_getParam<uint64_t>(params,"handle",1)[0];
	      gui_window_handle_ptr=&gui_window_handle;
	      printf("found window handle\n");
	  }
	       // std::size_t pointer=mhs::mex_getParam<uint64_t>(params,"handle",1)[0];
	    #endif    
	  
// 	  if (mhs::mex_hasParam(params,"worker")!=-1) 
//                 num_workers=mhs::mex_getParam<bool>(params,"worker",1)[0];
      
	}
	
	
// 	sta_assert_error(num_workers>0);
// 	sta_assert_error(num_workers<8);
	
	
	
	if (tracker_state!=NULL)
	{
	  
	  tracker_state_multitracker=mxGetField(tracker_state,0,(char *)("W"))!=NULL;
	  // if we have existing states, then 
	  //if (mxIsCell(tracker_state))
	  if (tracker_state_multitracker)
	  {
// 	    num_tracker_states=mxGetNumberOfElements(tracker_state);
	    
	    
	    num_tracker_states=mxGetNumberOfElements(mxGetField(tracker_state,0,(char *)("W")));
	    
// 	    num_tracker_states=mxGetNumberOfElements(tracker_state);
	    num_workers=num_tracker_states;
	    
	    //num_workers=std::max(num_tracker_states,num_workers); 
	    printf("found data for several workers: %d %d\n",num_tracker_states,num_workers);
// 	    tracker_state_cell=(mxArray *)tracker_state;
	  }else
	  {
// 	    mwSize dims=1;
// 	    tracker_state_cell=mxCreateCellArray(dims, &dims);
// 	    mxSetCell(tracker_state_cell, 0, (mxArray *)tracker_state);
	    num_tracker_states=1;
	  }
	}
      
        int datafunc=mhs::dataArray<TData>(feature_struct,"datafunc").data[0];
	
	sta_assert_error(num_workers<max_tracking_treads);

	
        switch(datafunc)
        {
	case 0:
        {
            data_fun=new CDataDummy<T,TData,Dim>();
            printf("dataterm: DUMMY\n");
        }
        break;
	
	
	
//         case 1:
//         {
//             data_fun=new CDataSFilter<T,TData,Dim>();
//             printf("dataterm: SFilter\n");
//         }
//         break;
//         case 2:
//         {
//             data_fun=new CDataHough<T,TData,Dim>();
//             printf("dataterm: Hough\n");
//         }
//         break;
//         case 3:
//         {
//             data_fun=new CDataMedial<T,TData,Dim>();
//             printf("dataterm: Medialness\n");
//         }
//         break;
//         case 4:
//         {
//             data_fun=new CDataHessian<T,TData,Dim>();
//             printf("dataterm: Derivatives\n");
//         }
//         break;
// 	case 5:
//         {
//             data_fun=new CDataGVFFilter<T,TData,Dim>();
//             printf("dataterm: GVF\n");
//         }
//         break;
// 	case 6:
//         {
//             data_fun=new CDataRay<T,TData,Dim>();
//             printf("dataterm: Rayburst\n");
//         }
//         break;
	case 7:
        {
            data_fun=new CDataHessianNew<T,TData,Dim>();
            printf("dataterm: Hessian New\n");
        }
        break;
	case 8:
        {
            data_fun=new CDataSD<T,TData,Dim>();
            printf("dataterm: SD\n");
        }
        break;
	case 9:
        {
            data_fun=new CDataSD2<T,TData,Dim>();
            printf("dataterm: SD New\n");
        }
        break;
	case 10:
        {
            data_fun=new CDataSDSH<T,TData,Dim>();
            printf("dataterm: SD SH\n");
        }
        break;
	case 11:
        {
            data_fun=new CDataSDSHorg<T,TData,Dim>();
            printf("dataterm: SD SH org\n");
        }
        break;
	case 12:
        {
            data_fun=new CDataSpines<T,TData,Dim>();
            printf("dataterm: Spine org\n");
        }
        break;
	case 13:
        {
            data_fun=new CDataSDSHsingle<T,TData,Dim>();
            printf("dataterm: SD SH single\n");
        }
        break;	
        
    case 90:
        {
            data_fun=new CDataSpikes<T,TData,Dim>();
            printf("dataterm: Spikes\n");
        }
        break;		
	case 99:
        {
            data_fun=new CDataNodata<T,TData,Dim>();
            printf("dataterm: NONE\n");
        }
        break;		
	
	#ifdef _USE_CUDA_
	case 1000:
        {
            data_fun=new CDataSimple<T,TData,Dim>();
            printf("dataterm: CUDA\n");
        }
        break;
	#endif
	
	case 100:
        {
            data_fun=new CDataDTI<T,TData,Dim>();
            printf("dataterm: DTI\n");
        }
        break;  
	
        default:
        {
            printf("dataterm: ?? %d\n",datafunc);
            sta_assert_error(0==1);
        }
        }
        
        
        

        printf("->edgefunc\n");
        

	edgecost_fun=new CNTrackerScaleInvariantEdgeCosts<T,Dim>();
// 	edgecost_fun=new EdgeCostAugust<T,Dim>();
	
	switch (collision_fun_id)
	{
	  case 1: 
	  {
	      collision_fun=new DTISoftCollision<T,Dim> ();
	      printf("using DTI sof collision\n");
	    }break;
	  case 2: 
	  {
	      collision_fun=new SimpleCollision<T,Dim> ();
	      printf("using Simple collision\n");
	    }break;  
	  
	    default:
	    {
	      collision_fun=new TabletCollision<T,Dim> ();
	      printf("using hard collision\n");
	    }break;
	}
	
 	
//	collision_fun=new TabletCollisionPSF<T,Dim> ();
	
	collision_fun->set_params(params);

        CData<T,TData,Dim> &  data_fun_p=*data_fun;
        CEdgecost<T,Dim> &   edgecost_fun_p=*edgecost_fun;
	
	Collision<T,Dim> &   collision_fun_p=*collision_fun;
	
	mhs::mex_dumpStringNOW();
	
	printf("->datafunc\n");
	data_fun_p.init(feature_struct);
	
	mhs::mex_dumpStringNOW();

	printf("->tracker\n");
// 	num_workers=1;  
	
	tracker= new CTracker<TData,T,Dim>*[num_workers];
	grid_helper_data_fun= new CDataGrid<T,TData,Dim>*[num_workers];
	for (int t=0;t<num_workers;t++)
	{
	  tracker[t]=NULL;
	  grid_helper_data_fun[t]=NULL;
	}
	
	
  	
	std::vector<std::size_t> pts_per_tracker;
	pts_per_tracker.resize(num_workers);
	
	for (int t=0;t<num_workers;t++)
	{
	  pts_per_tracker[t]=min_pts_per_tracker;
	  try
	  {
	    if (tracker_state!=NULL)
	    {
	      if (tracker_state_multitracker)
	      {
		const mxArray * state=(t<num_tracker_states) ? mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("A")) : NULL;
		if (state!=NULL)
		{
		  pts_per_tracker[t]=std::max(pts_per_tracker[t],2*mhs::dataArray<T>(state,"data").dim[0]);
		}
	      }else
	      {
		const mxArray * state=(t<1) ? tracker_state : NULL;
		if (state!=NULL)
		{
		  pts_per_tracker[t]=std::max(pts_per_tracker[t],2*mhs::dataArray<T>(state,"data").dim[0]);
		}		
	      }
	    }
	  }catch  (mhs::STAError & error)
	  {
	    throw mhs::STAError("error in determining pool sizes\n ");
	  }
	}
	
	
	
	
	
	//#pragma omp parallel for num_threads(num_workers) 
	for (int t=0;t<num_workers;t++)
	{
	    grid_helper_data_fun[t]= new CDataGrid<T,TData,Dim>(data_fun);
	    
// 	    {
// 	     std::size_t shape[3];
// 	     std::size_t offset[3];
// 	     grid_helper_data_fun[t]->get_real_shape(shape);
// 	     offset[0]=(t%2)*shape[0]/2;
// 	     offset[1]=((t+1)%2)*shape[1]/2;
// 	     offset[2]=((t+2)%2)*shape[2]/2;
// 	     shape[0]/=2;
// 	     shape[1]/=2;
// 	     shape[2]/=2;	     
// 	     grid_helper_data_fun[t]->set_region(offset,shape);
// 	    }
	    
	    
	    if (tracker_state_multitracker)
	    {
// 		const  mxArray  * worker_struct=mxGetField(tracker_state,0,(char *)("W"));
 		const  mxArray  * worker_data=mxGetField(tracker_state,0,(char *)("W"));
		
// 	        const  mxArray  * worker_data=mxGetField(tracker_state,t,(char *)("W"));
		sta_assert_error(worker_data!=NULL);
	        
// 		const  mxArray  * sub_region=mxGetField(worker_data,0,(char *)("shape"));
		const  mxArray  * sub_region=mxGetField(worker_data,t,(char *)("shape"));
		sta_assert_error(sub_region!=NULL);

		//tracker_state=mxGetField(tracker_states,0,(char *)("A"));
		  
		std::size_t shape[3];
		std::size_t offset[3];
		    
		  double * rdata_p=(double  *)mxGetPr(sub_region);
// 		  shape[0]=rdata_p[2];
// 		  shape[1]=rdata_p[1];
// 		  shape[2]=rdata_p[0];
		  shape[0]=rdata_p[0];
		  shape[1]=rdata_p[1];
		  shape[2]=rdata_p[2];		  
		  
// 		  sub_region=mxGetField(worker_data,0,(char *)("offset"));
		  sub_region=mxGetField(worker_data,t,(char *)("offset"));
		  sta_assert_error(sub_region!=NULL);
		  rdata_p=(double  *)mxGetPr(sub_region);
// 		  offset[0]=rdata_p[2];
// 		  offset[1]=rdata_p[1];
// 		  offset[2]=rdata_p[0];
		  offset[0]=rdata_p[0];
		  offset[1]=rdata_p[1];
		  offset[2]=rdata_p[2];

		  
		  grid_helper_data_fun[t]->set_region(offset,shape);
	    }
	

	    grid_helper_data_fun[t]->init_saliency(feature_struct);
	    
	    
    //         tracker[t]= new CTracker<TData,T,Dim>(data_fun_p,
    //                                            edgecost_fun_p);
	    printf("initializing tracker %u with a pool of size %u\n",t,pts_per_tracker[t]);
	    tracker[t]= new CTracker<TData,T,Dim>(*grid_helper_data_fun[t],
					      edgecost_fun_p,collision_fun_p,pts_per_tracker[t]);
	    
	    printf("done\n");
	}
	mhs::mex_dumpStringNOW();

	
	
	
	bool no_error=true;
	printf("->tracker init\n");
	mhs::mex_dumpStringNOW();
	
	#pragma omp parallel for num_threads(num_workers) 
	for (int t=0;t<num_workers;t++)
	{
	  try
	  {
	    if (tracker_state!=NULL)
	    {
	      
	      if (tracker_state_multitracker)
	      {
		tracker[t]->init_tracker(feature_struct,
				//(t<num_tracker_states) ? mxGetField(tracker_state,t,(char *)("W")) : NULL,
// 				(t<num_tracker_states) ? mxGetField(mxGetField(tracker_state,t,(char *)("W")),0,(char *)("A")) : NULL,
					 (t<num_tracker_states) ? mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("A")) : NULL,
				params, (seeds!=NULL) ? seeds : NULL);
	      }else
	      {
		sta_assert_error(num_workers==1);
		tracker[t]->init_tracker(feature_struct,
				(t<1) ? tracker_state : NULL,
				params, (seeds!=NULL) ? seeds : NULL);
	      }
	    }else
	    { 
	      sta_assert_error(num_workers==1);
	      tracker[t]->init_tracker(feature_struct,
				 NULL,
				params, (seeds!=NULL) ? seeds : NULL);
	    }
	  }catch  (mhs::STAError & error)
	  {
	    no_error=false;
	    printf("error in initing worker %d: %s\n",t,error.str().c_str());
	  }
	}
	mhs::mex_dumpStringNOW();
	if (!no_error)
	  throw (mhs::STAError("exiting"));
	
	#ifdef D_USE_GUI    
	
        printf("init tracker renderer\n");
	  renderer= new CTrackerRenderer<TData,T,Dim>(gui_window_handle_ptr);
	  
      
	  for (int t=0;t<num_workers;t++)
	  renderer->register_tracker(*tracker[t]);
	  std::size_t shape[3];
	  data_fun_p.getshape(shape);
	  renderer->set_shape(shape);
	  renderer->init(feature_struct);
	  
	  for (int t=0;t<num_workers;t++)
	  tracker[t]->set_renderer(renderer);
	  
	  if (tracker_state!=NULL)
	  {
	    renderer->load_state_from_struct(tracker_state);
	  }
	  if (cam_data!=NULL)
	  {
	    renderer->load_cam_path(cam_data);
	  }
	  renderer->set_params(params);
      
      printf("done\n");
	#endif
	

	
	
	int total_num_threads=num_workers;  
	#ifdef D_USE_GUI    
// 	  total_num_threads++;
	total_num_threads+=2;
	
	#else 
	mhs::CtimeStopper timer;
	double last_time=0;
	total_num_threads+=1;
	
	no_gui_user_cmd= new C_nogui_command();
	#endif
	
	
	
	
	if (!debug_compute_costs)
	{
	  printf("->tracking\n");
	  
	  bool running=true;
// 	  #pragma omp parallel sections  num_threads(2) shared(running)
// 	  {
// 	    #pragma omp section
// 	    {
// 	      int count=0;  
// 	      while (running)
// 	      {
// 	      #ifdef  D_USE_GUI
// 		renderer->do_render();
// 		renderer->ui();
// 		renderer->update_tracker_controls();
// 		SDL_Delay(1);
// 		count++;
// 	      #endif
// 	      }
// 	    }
// 	    #pragma omp section
// 	    {
// 	      tracker->track();	     
// 	      running=false;
// 	    }
	  
	  int running_count=num_workers;
	  no_error=true;
	  
	  #pragma omp parallel for num_threads(total_num_threads) shared(running,running_count)
	  for (int i=0;i<total_num_threads;i++)
	  {
	    int thread_id=omp_get_thread_num();
	    #pragma omp critical
	    {
	      #ifdef  D_USE_GUI
		if (thread_id==0)
		  printf("worker %d is running (GUI) \n",thread_id);
		else if (thread_id==1)
		  printf("worker %d is running (input) \n",thread_id);
		else   
	      #else
		  if (thread_id==0)
		    printf("worker %d is running (INFO) \n",thread_id);
		  else
	      #endif
		printf("worker %d is running\n",thread_id);
	    }

	    #ifdef  D_USE_GUI
	    
	      if (thread_id==0)// if using GUI then GUI thread will be the main thread
	      {
		try
		{
		  int count=0;  
		  while (running)
		  {
		    renderer->do_render();
// 		    renderer->ui();
		    renderer->update_tracker_controls();
		    SDL_Delay(1);
		    count++;
		  }
		}catch  (mhs::STAError & error)
		{
		  renderer->send_exit_program_signal();
		  running=false;
		  no_error=false;
		  printf("error in GUI thread: %s\n",error.str().c_str());
		}
	      }else if (thread_id==1)//
	      {
		try
		{
		  while (running)
		  {
		    renderer->ui();
		    SDL_Delay(25);
		  }
		}catch  (mhs::STAError & error)
		{
		  renderer->send_exit_program_signal();
		  running=false;
		  no_error=false;
		  printf("error in INPUT thread: %s\n",error.str().c_str());
		}
	      } else
	    #else 
	      if (thread_id==0)// if not using GUI then infor thread will be the main thread
	      {
		try
		{
		  int count=0;  
		      timespec tmp;
		      tmp.tv_sec=1;
		      tmp.tv_nsec=0;
		  while (running)
		  {
		    
 		    if ((timer.get_total_runtime()-last_time)>10)
		    {
		      printf("--------------------------------------\n");
		      for (int j=0;j<num_workers;j++)
		      {
			tracker[j]->print_info(j);
		      }
		      if (no_gui_user_cmd->has_been_terminated())
		      {
			  for (int j=0;j<num_workers;j++)
			  {
			    tracker[j]->stop_tracking();
			  }
			  running=false;
		      }
		      
		      mhs::mex_dumpStringNOW();
 		      last_time=timer.get_total_runtime();
		    }
		    nanosleep(&tmp, NULL);
// 		    std::this_thread::sleep_for (std::chrono::seconds(1));
		  }
		}catch  (mhs::STAError & error)
		{
		  for (int j=0;j<num_workers;j++)
		    {
		      tracker[j]->stop_tracking();
		    }
		  running=false;
		  no_error=false;
		  printf("error in INFO thread: %s\n",error.str().c_str());
		}
	      }else 
	    #endif
	    {
	      int tracker_id=thread_id;
	      #ifdef  D_USE_GUI
		tracker_id-=2;
	      #else
		tracker_id-=1;  
	      #endif
	      sta_assert_error(tracker_id>-1);
	      sta_assert_error(tracker_id<num_workers);
	      try
		{
		  tracker[tracker_id]->track();
		}catch  (mhs::STAError & error)
		{
		 #ifdef  D_USE_GUI
		    renderer->send_exit_program_signal();
		  #else 	
		    for (int j=0;j<num_workers;j++)
		    {
		      tracker[j]->stop_tracking();
		    }
		  #endif
		  no_error=false;
		  printf("error in working thread %d: %s\n",tracker_id,error.str().c_str());
		}
		#pragma omp critical
		{
		running_count--;	
		}
	    }
	    printf("working threads remaining:%d \n",running_count);
	    

	    
	    #pragma omp critical
	    {
	      if (running_count==0) // last worker is finished
	      {
	       running=false;	      
	      }
	      #ifdef  D_USE_GUI
		if (thread_id==0)
		  printf("worker %d is exiting (GUI) \n",thread_id);
		else if (thread_id==1)
		  printf("worker %d is exiting (input) \n",thread_id);
		else
	      #else 
		  if (thread_id==0)
		    printf("worker %d is exiting (INFO) \n",thread_id);
		  else
	      #endif
		printf("worker %d is exiting \n",thread_id);
	    }
	  }
	  
	  
	}else
	{
	    //tracker-> compute_costs();
	  printf("##################\n NOT IMPLEMENTED IN THE MT VERSION YET\n##################\n");
	}
	if (!no_error)
	  throw (mhs::STAError("exiting"));  
	  
	  
	  

	if (!tracker_state_multitracker)
	{
	  plhs[0]=tracker[0]->save_tracker_state();
	  
 	  mhs::copyField(feature_struct,plhs[0],"cshape");
 	  mhs::copyField(feature_struct,plhs[0],"scales");
 	  mhs::copyToField(params,plhs[0],"options");
	  
	  
	}else
	{
	  mwSize dim=1;
	  mwSize dims=1;
	  mwSize nfields=1;
	  
	  const char *field_names[] = {
	      "W",			// single tracker states
	  };
	  
	  
	  
	  plhs[0]=mxCreateStructArray(dim, &dims, nfields,field_names);
	  mxArray * worker_state=mxGetField(plhs[0],0,(char *)("W"));
 	  mhs::copyField(feature_struct,plhs[0],"cshape");
 	  mhs::copyField(feature_struct,plhs[0],"scales");
	  mhs::copyToField(params,plhs[0],"options");
	  //plhs[0]=mxCreateCellArray(dim, &dims);
	    
	    dim=1;
	    dims=num_workers;
	    nfields=3;
	    const char *field_namesW[] = {
	      "A",			// status
	      "shape",			// status
	      "offset",			// status
	    };
	    
	    if (mxGetField(tracker_state,0,(char *)("bifurcation_backup_data"))!=NULL)
	    {
 		  mxAddField(plhs[0],"bifurcation_backup_data");
// 		  mxSetField(plhs[0], 0,"bifurcation_backup_data", bifurcation_data_backup);
		  mxSetField(plhs[0], 0,"bifurcation_backup_data", mxDuplicateArray(mxGetField(tracker_state,0,(char *)("bifurcation_backup_data"))));
	    }
	    if (mxGetField(tracker_state,0,(char *)("tmp_volume"))!=NULL)
	    {
 		  mxAddField(plhs[0],"tmp_volume");
		  mxSetField(plhs[0], 0,"tmp_volume", mxDuplicateArray(mxGetField(tracker_state,0,(char *)("tmp_volume"))));
	    }
	    
	    
// 	    mxArray * worker_settings=mxGetField(plhs[0],t,(char *)("W"));
	    
 	    mxArray * settings=mxCreateStructArray(dim, &dims, nfields,field_namesW);
	    mxSetField(plhs[0],0,(char *)("W"),settings);
	    settings=mxGetField(plhs[0],0,(char *)("W"));
	  
	  for (int t=0;t<num_workers;t++)
	  {
 	    mxSetField(settings, t,"A", tracker[t]->save_tracker_state());
  	    mxSetField(settings, t,"shape", mxDuplicateArray(mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("shape"))));
  	    mxSetField(settings, t,"offset", mxDuplicateArray(mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("offset"))));
	   // mxSetField(plhs[0],t,(char *)("W"),settings);
	    //mxSetField(worker_state,t,(char *)("W"),settings);
	  }
	  
	    //mxSetCell(plhs[0], t, tracker[t]->save_tracker_state());
	  //plhs[t]=;
	}
	
	
	 #ifdef D_USE_GUI    
          if (renderer!=NULL)
	  {
	   renderer->save_state_in_struct(plhs[0]); 
	  }
	  
	  #endif
	
	
	for (int t=0;t<num_workers;t++)
	{
	   printf("\n##################\n  STATS WORKER %d \n##################\n\n",t);
	  tracker[t]->print_proposal_stat();
	}
	
	for (int t=0;t<num_workers;t++)
	tracker[t]->check_consisty();

    } catch (mhs::STAError & error)
    {
	printf("!################################!\n");	
	printf("ntracker died!!\n");
	printf("!################################!\n");	
        printf("%s \n",error.what());
    }
    catch (...)
    {
	printf("ntracker died!!\n");
        mexErrMsgTxt("exeption ?");
    }

    try
    {
      printf(".. cleaning up\n");
      if (tracker!=NULL)
      {
	  for (int t=0;t<num_workers;t++)
	  {
	    if ( tracker[t]!=NULL)
		delete tracker[t];
	    if ( grid_helper_data_fun[t]!=NULL)
	  delete grid_helper_data_fun[t];
	  }
	  
	  delete [] tracker;
	  delete [] grid_helper_data_fun;
      }
      
      #ifdef D_USE_GUI    
          if (renderer!=NULL)
	    delete renderer;
      #else
	  if (no_gui_user_cmd!=NULL)
	    delete no_gui_user_cmd;  
      #endif
      
   
      if (data_fun!=NULL)
	  delete data_fun;
      if (edgecost_fun!=NULL)
	  delete edgecost_fun;
      if (collision_fun!=NULL)
	  delete collision_fun;
      
    } catch (mhs::STAError & error)
    {
	printf("error cleaning up!!\n");
        mexErrMsgTxt(error.what());
    }
    catch (...)
    {
	printf("error cleaning up!!\n");
        mexErrMsgTxt("exeption ?");
    }
} 
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

#ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
  printf("_DEBUG_CONNECT_LOOP_EXTRA_CHECK enabled\n");
#endif

    set_application_directory();

    printf("mexcfunc #params: %d\n",nrhs);
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");
    if (!mxIsStruct(prhs[0]))
        mexErrMsgTxt("error: expecting feature struct\n");

    const mxArray *img=mxGetField(prhs[0],0,(char *)("img"));
    if (img==NULL){
	img=mxGetField(prhs[0],0,(char *)("scales"));
    }
    
    if (img==NULL){
      printf("could not determine data precision\n");
    }


//     if (mxGetClassID(img)==mxDOUBLE_CLASS)
//         _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
//     else if (mxGetClassID(img)==mxSINGLE_CLASS)
//         _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
//     if (mxGetClassID(img)==mxDOUBLE_CLASS)
//         _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
    if (mxGetClassID(img)==mxSINGLE_CLASS)
        _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported data type\n");
    
   
}







