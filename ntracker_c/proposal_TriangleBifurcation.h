#ifndef PROPOSAL_BIFURCATION_H
#define PROPOSAL_BIFURCATION_H

#include "proposals.h"
#include "edges.h"
#include "mhs_graphics.h"





template<typename TData,typename T,int Dim> class CProposal;

template<typename TData,typename T,int Dim> class CProposalBifurcationDeath;


template<typename TData,typename T,int Dim>
class CProposalBifurcationBirth : public CProposal<TData,T,Dim>
{
  friend class CProposalBifurcationDeath<TData,T,Dim>;
  
protected:
    T Lprior;
     T ConnectEpsilon;
        T lambda;
    const static int max_stat=10;
    int statistic[max_stat];
public:

    
    T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_segment(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }


    CProposalBifurcationBirth(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
	
	 lambda=100;
    }
    

    
    ~CProposalBifurcationBirth()
    {
//         printf("\n");
// 	for (int i=0;i<max_stat;i++)
// 	  printf("%d ",statistic[i]);
// 	printf("\n");
	
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH);
    };
    
    
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH);
    }

    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        	
	std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
      
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	 if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	 
	 if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;
	
	/// no terminals?
	if (connections.get_num_terminals()<1)
	  return false;

	int stat_count=0;
//0	
statistic[stat_count++]++;	

bool debug=false;

if (debug)
  printf("BB[");

	try {
	
	/// randomly pic a terminal node
	Points<T,Dim> &  point=connections.get_rand_point();
	
	
	/// check if point is not a bifurcation terminal!
	if(point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	  return false;
	
//1	
statistic[stat_count++]++;	
	

	// search for a bifurcation candidate (an edge with which we can build a triangle via new 2 edges)
	class Connection<T,Dim> * existing_connection=NULL;
	T connection_prob;
	T connection_costs;
	///TODO check if connection_costs same as new_cedge_costs
	/// DONE-> if enable _DEBUG_CHECKS_1_ it will be checked below
	if (!do_tracking3(
		      edgecost_fun,
		      existing_connection,
		      connection_prob,
		      connection_costs,
		      tree,
		      point,
		      search_connection_point_center_candidate,
		      //options.connection_candidate_searchrad,
		      conn_temp,
		      options.bifurcation_bonus_L,
		      Lprior,
		      ConnectEpsilon,
		      options.bifurcation_neighbors
		      ))
	{
	  if (debug)
	    printf("x]\n");
	  return false;
	}
	
	
// 	T connection_costs2;
// 	{
// 	class Connection<T,Dim> * existing_connection2=NULL;
// 	T connection_prob2;
// 	
// 	///TODO check if connection_costs same as new_cedge_costs
// 	/// DONE-> if enable _DEBUG_CHECKS_1_ it will be checked below
// 	sta_assert_error(do_tracking3(
// 		      edgecost_fun,
// 		      existing_connection2,
// 		      connection_prob2,
// 		      connection_costs2,
// 		      tree,
// 		      point,
// 		      search_connection_point_center_candidate,
// 		      options.connection_candidate_searchrad,
// 		      conn_temp,
// 		      options.bifurcation_bonus_L,
// 		      Lprior,
// 		      existing_connection
// 		      ));
// 	}
	
//2	
statistic[stat_count++]++;	
	
	if (connection_prob<std::numeric_limits<T>::epsilon())
	  return false;
	
	sta_assert_debug0(existing_connection!=NULL);
// 	sta_assert_debug0(existing_connection->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
// 	sta_assert_debug0(existing_connection->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
	
	// there is exactly one free side on a terminal
	const int side=(point.endpoint_connections[0]==1) ? 1 : 0;

	
	sta_assert_debug0(point.endpoints[1-side][0]->connected!=NULL);
	class Points<T,Dim> * invalid_point=point.endpoints[1-side][0]->connected->point;
	sta_assert_debug0(invalid_point!=existing_connection->pointA->point); 
	sta_assert_debug0(invalid_point!=existing_connection->pointB->point);
	sta_assert_debug0(existing_connection!=NULL);
	sta_assert_debug0(existing_connection->pointA->point!=&point);
	sta_assert_debug0(existing_connection->pointB->point!=&point);
	
//1	
statistic[stat_count++]++;
	

	
	// the existing edge should be a normal edge
	sta_assert_error(existing_connection->edge_type==EDGE_TYPES::EDGE_SEGMENT);
	
	// edge costs of the OLD configuration
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	T old_edgecost=existing_connection->e_cost(
	  edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange);
	
	  #ifdef _DEBUG_CHECKS_0_
		  if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		  {
		    existing_connection->pointA->position->print();
		    existing_connection->pointB->position->print();
		    printf("CProposalBifurcationBirth\n");
		    printf("%f %f\n",options.connection_candidate_searchrad,std::sqrt((*existing_connection->pointA->position-*existing_connection->pointB->position).norm2()));
		    sta_assert_debug0(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
		  }
	  #endif	
	
	/// of course the old edge should be in range (valid)
	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	
	
	
	
	// create the bifurcation particle
	Points<T,Dim> * p_b_particle=particle_pool.create_obj();    
	Points<T,Dim> & b_particle=*p_b_particle;
	b_particle.tracker_birth=3;
	b_particle.set_direction(Vector<T,Dim>(1,0,0));
	
	sta_assert_error(!b_particle.has_owner());
	
	
	T particle_costs=0;
	try {
	particle_costs-=point.compute_cost3(temp); //old terminal node
	particle_costs-=existing_connection->pointA->point->compute_cost3(temp); //old segment point A
	particle_costs-=existing_connection->pointB->point->compute_cost3(temp); //old segment point B
	
	} catch (mhs::STAError error)
	{
	 error<<"\nerror in computing cost3 (old costs)\n"; 
	 throw error; 
	}
	
	
	  T particle_terminalcool_cost_old=0;
	  T particle_terminalcool_cost_new=0;

	  class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	  class Collision<T,Dim>::Candidates candA(query_bufferA);
	  
	  int collision_mode=1;
	  if (debug)
	  printf("Cola\n");	
	    if (collision_fun_p.is_soft())  
	    {
	   sta_assert_error(!collision_fun_p.colliding(particle_terminalcool_cost_old,
						tree,
					      point,
					      collision_search_rad,
					      temp,&candA,true,-9,collision_mode));
	    }
	    if (debug)
	  printf("Cola\n");	
	  
	
	bool is_protected_topology=existing_connection->is_protected_topology();
	
	//we back-up the existing edge and remove it
	class Connection<T,Dim> backup_existing_edge=*existing_connection;
	connections.remove_connection(existing_connection);
	
	//we add three new connections to the bif particle
	class Connection<T,Dim> * new_connections[3];
	class Points<T,Dim>::CEndpoint * bifurcation_endpoints[3];
	bifurcation_endpoints[0]=point.endpoints[side][0];
	bifurcation_endpoints[1]=backup_existing_edge.pointA;
	bifurcation_endpoints[2]=backup_existing_edge.pointB;
	

	
	
	
// 	  T testcost= edgecost_fun.u_cost( *bifurcation_endpoints[0],
// 				      *bifurcation_endpoints[1],
// 				      *bifurcation_endpoints[2],
// 				      maxendpointdist,
// 				      inrange
// 				    )+options.bifurcation_bonus_L;
	
	sta_assert_debug0(*bifurcation_endpoints[0]->endpoint_connections==0);
	sta_assert_debug0(*bifurcation_endpoints[1]->endpoint_connections==0);
	sta_assert_debug0(*bifurcation_endpoints[2]->endpoint_connections==0);
	
	class Connection<T,Dim> a_new_connection;
	a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
	
	///create the three edges
	for (int i=0;i<3;i++)
	{
/*printf("%d -> ",static_cast<int>(b_particle.particle_type));	*/  
	  a_new_connection.pointA=b_particle.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	  Points<T,Dim> * pointB=bifurcation_endpoints[i]->point;
	  a_new_connection.pointB=pointB->getfreehub(bifurcation_endpoints[i]->side);
	  
	  
	  sta_assert_debug0(a_new_connection.pointB->point!=a_new_connection.pointA->point);
	  sta_assert_debug0(a_new_connection.pointB->point!=invalid_point);
	  sta_assert_debug0(a_new_connection.pointA->point!=invalid_point);
	  
	  new_connections[i]=connections.add_connection(a_new_connection);
	 
#ifdef _DEBUG_CHECKS_0_
// printf("%d \n",static_cast<int>(b_particle.particle_type));
	  switch(i) 
	  {
	    case 0:
	      sta_assert_error(b_particle.particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
	      break;
	    case 1:
	      sta_assert_error(b_particle.particle_type==(PARTICLE_TYPES::PARTICLE_SEGMENT));
	      break;
	    case 2:
	      sta_assert_error(b_particle.particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
	      break;  
	  }
#endif	    
	}
	
	new_connections[1]->freeze_topology=is_protected_topology;
	 new_connections[2]->freeze_topology=is_protected_topology;
	
	sta_assert_debug0((b_particle.endpoint_connections[Points< T, Dim >::bifurcation_center_slot])==3);
	#ifdef _DEBUG_CHECKS_0_
	  for (int i=0;i<3;i++)
	    sta_assert_debug0((b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->particle_type)==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
	#endif
	
	if (debug)
	  printf("@");	  
	// updateing the bifurcation centr particle pos + scale    
	
	
	
	
  
	
	T new_edgecost=connection_costs;

// 	{
// 	T new_edgecost2=b_particle.edge_energy(
// 	  edgecost_fun,
// 	  options.connection_bonus_L,
// 	  options.bifurcation_bonus_L,
// 	  maxendpointdist,
// 	  inrange);
// 	sta_assert_error(b_particle.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
// 	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	T new_edgecost3=b_particle.edge_energy(
// 	  edgecost_fun,
// 	  options.connection_bonus_L,
// 	  options.bifurcation_bonus_L,
// 	  maxendpointdist,
// 	  inrange);
// 	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	
// 	
// 	
// 	
// 	  printf("%f %f %f %f %f\n",new_edgecost,new_edgecost2,new_edgecost3,connection_costs2,testcost);
// 	  
// 	  //printf("%u %u %u\n",bifurcation_endpoints[0],bifurcation_endpoints[1],bifurcation_endpoints[2]);
// 	  
// 	  for (int i=0;i<3;i++)
// 	  {
// 	    int bifurcation_center_slot=Points<T,Dim>::bifurcation_center_slot;
// 	    
// 	  bifurcation_endpoints[i]->position->print();
// 	   b_particle.endpoints[bifurcation_center_slot][i]->connected->position->print();
// 	  }
// 	  
// /*	  
// 	  printf("%u %u %u\n",b_particle.endpoints[bifurcation_center_slot][0],
// 		 b_particle.endpoints[bifurcation_center_slot][1],
// 		 b_particle.endpoints[bifurcation_center_slot][2]
// 		);*/
// 	}
	
	
	
	if (debug)
	  printf("]\n");	    
	  
	
	
	//previous terminal node:
// 	particle_costs-=point.compute_cost2(static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_TERMINAL));
// 	particle_costs+=point.compute_cost2(static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT));
	//previous edgepointA:
	try {
	particle_costs+=point.compute_cost3(temp); //old terminal node
	particle_costs+=backup_existing_edge.pointA->point->compute_cost3(temp); //old segment point A
	particle_costs+=backup_existing_edge.pointB->point->compute_cost3(temp); //old segment point B	
	}
	catch (mhs::STAError error)
	{
	 error<<"\nerror in computing cost3 (new  costs 3)\n"; 
	 throw error; 
	}
	
	if (debug)
	  printf("Colb\n");	
	if (collision_fun_p.is_soft())  
	    {
	   sta_assert_error(!collision_fun_p.colliding(particle_terminalcool_cost_new,
						tree,
					      point,
					      collision_search_rad,
					      temp,&candA,false,-10,collision_mode));
	    }
	if (debug)
	  printf("Done\n");
	
	/*std::size_t*/ 

// 	T terminal_point_cost_change=point.compute_cost()*(particle_connection_state_cost_scale[2]-particle_connection_state_cost_scale[1])
// 	  +(particle_connection_state_cost_offset[2]-particle_connection_state_cost_offset[1]);

	  
	T E=(new_edgecost-old_edgecost)/temp;
// 	  E+=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;

	  if (collision_fun_p.is_soft())   
	{
	    E+=(particle_terminalcool_cost_new-particle_terminalcool_cost_old)/temp;
	}
	  
          T R=mhs_fast_math<T>::mexp(E);
	  
	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()); 
	connection_prob*=1.0/(connections.get_num_terminals()+1);
	R*=remove_connection_prob/((connection_prob)+std::numeric_limits<T>::epsilon());

 
statistic[stat_count++]++;	
	


	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {

statistic[stat_count++]++;


		if (options.no_double_bifurcations)
		{
		    if (p_b_particle->connects_two_bif())
		    {
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_connections[2]);
			connections.add_connection(backup_existing_edge);
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
			particle_pool.delete_obj(p_b_particle);
			return false;
		    }
		}

		bool out_of_bounce=false;
		b_particle.bifurcation_center_update(shape,out_of_bounce);
		
		if (out_of_bounce)
		{
		   sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			connections.remove_connection(new_connections[0]);
			connections.remove_connection(new_connections[1]);
			connections.remove_connection(new_connections[2]);
			connections.add_connection(backup_existing_edge);
			sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
			particle_pool.delete_obj(p_b_particle);
		    return false;
		}
		
		
		if (options.bifurcation_particle_hard)	
		{
		  if (collision_fun_p.colliding( tree,b_particle,collision_search_rad,temp))
		  {
  // 		  printf("PUFF!! Bif Burth!\n");
			    sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
			    connections.remove_connection(new_connections[0]);
			    connections.remove_connection(new_connections[1]);
			    connections.remove_connection(new_connections[2]);
			    connections.add_connection(backup_existing_edge);
			    sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
			    particle_pool.delete_obj(p_b_particle);
			    return false;
		  }
		}

		// check for loops by following both new edges
		if (Constraints::constraint_hasloop_follow(b_particle,new_connections[0],options.constraint_loop_depth+1))
		{
		  sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		  connections.remove_connection(new_connections[0]);
		  connections.remove_connection(new_connections[1]);
		  connections.remove_connection(new_connections[2]);
		  connections.add_connection(backup_existing_edge);
		  sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
		  particle_pool.delete_obj(p_b_particle);
		  return false;
		}
		
// 		printf("------\n");
		
// 		b_particle.print();
// 		b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->position->print();
// 		b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->position->print();
// 		b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->position->print();
// 		b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point->position.print();
// 		b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->point->position.print();
// 		b_particle.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->point->position.print();
		
		sta_assert_error(!b_particle.has_owner());
		
		if (options.bifurcation_particle_hard)	
		{
// 		sta_assert_error(false);
		  if(!tree.insert(b_particle))
		  {
// 		    particle_pool.delete_obj(p_b_particle);
// 		    throw mhs::STAError("error insering point\n");
		     sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		      connections.remove_connection(new_connections[0]);
		      connections.remove_connection(new_connections[1]);
		      connections.remove_connection(new_connections[2]);
		      connections.add_connection(backup_existing_edge);
		      sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
		      particle_pool.delete_obj(p_b_particle);
		      return false;
		  }
		}
		
		
		if(!bifurcation_tree.insert(b_particle))
		{
		   sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		  connections.remove_connection(new_connections[0]);
		  connections.remove_connection(new_connections[1]);
		  connections.remove_connection(new_connections[2]);
		  connections.add_connection(backup_existing_edge);
		  sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
		  particle_pool.delete_obj(p_b_particle);
		  return false;
// 		  particle_pool.delete_obj(p_b_particle);
// 		  throw mhs::STAError("error insering point\n");
		}
		
		
		point.unregister_from_grid(voxgrid_conn_can_id);
		sta_assert_debug0(point.endpoint_connections[0]>0);
		sta_assert_debug0(point.endpoint_connections[1]>0);
//5	  
statistic[stat_count++]++;		
	      
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		
		  #ifdef BIFURCATION_DEBUG_EDGECHECK
		// 	for (int i=0;i<3;i++)
		// 	{
		// 	new_connections[i]->compute_cost(
		// 		    edgecost_fun,
		// 	      options.connection_bonus_L,
		// 	      options.bifurcation_bonus_L,
		// 	      maxendpointdist,
		// 	      inrange,false,bifurcation_edgecheck_update);
		// 	    
		// 	    /// of course the old edge should be in range (valid)
		// 	    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		// 	}
			  (b_particle.edge_energy(edgecost_fun,
					options.connection_bonus_L,
				      options.bifurcation_bonus_L,
					maxendpointdist,
				      inrange,false,bifurcation_edgecheck_update));
			    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		      #endif
			    
			    
	    #ifdef  D_USE_GUI
// 	      Points< T, Dim > * p_bif_center_point=&b_particle;
// 	      (GuiPoints<T>* (p_bif_center_point))->touch(get_type());
// 	      for (int i=0;i<3;i++)
// 	      {
// 		  (GuiPoints<T>* (p_bif_center_point->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point))->touch(get_type());
// 	      }
// 	      Points< T, Dim > * p_bif_center_point=&b_particle;
	      b_particle.touch(get_type());
	      for (int i=0;i<3;i++)
	      {
		 b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
	      }
	    #endif			    
		
	    sta_assert_error(b_particle.has_owner(voxgrid_bif_center_id));
	    sta_assert_error((!b_particle.has_owner(voxgrid_default_id)!=(options.bifurcation_particle_hard)));
	    
	    return true;
	  }
	  
	  
	  sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
	  connections.remove_connection(new_connections[0]);
	  connections.remove_connection(new_connections[1]);
	  connections.remove_connection(new_connections[2]);
	  connections.add_connection(backup_existing_edge);
	  sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
	  particle_pool.delete_obj(p_b_particle);
	  return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
	
    }
    
    
    

    // searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
    static bool  do_tracking3(
        class CEdgecost<T,Dim> & edgecost_fun,
	class Connection<T,Dim>   *  & proposed_connection,
	T & connection_prob,
	T & connection_cost,
        OctTreeNode<T,Dim> & tree,
        Points<T,Dim> & npoint,
        T search_connection_point_center_candidate, // TODO currently ignored. using point scale instead
        //T searchrad, // TODO currently ignored. using point scale instead
        T TempConnect,
        T bifurL,
	T Lprior,
	T ConnectEpsilon,
	bool bifurcation_neighbors,
	 class Connection<T,Dim>   * compute_probability_for_edge=NULL
    )
    {
      

      
        bool debug=false;
        if (debug)
            printf("do_tracking birfurcation\n");

	
	/// select a free hub a the non-connected endpoint
	const int side=(npoint.endpoint_connections[0]==1) ? 1 : 0;
	class Points<T,Dim>::CEndpoint 	* enpt=npoint.getfreehub(side);
	sta_assert_debug0(enpt!=NULL);
	sta_assert_debug0(npoint.endpoint_connections[side]==0);
	sta_assert_debug0(npoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
	

	/// find the connected point. We want to exclude it from the 
	/// bifurcationcandidate list
	class Points<T,Dim> * connected_point=NULL;
	sta_assert_debug0(npoint.endpoints[1-side][0]->connected!=NULL);
	connected_point=npoint.endpoints[1-side][0]->connected->point;
	sta_assert_debug0(connected_point!=NULL);
	

        /*!
         collect all points in search rectangle
         */
//         npoint.update_endpoint(side);

        //const Vector<T,Dim> & endpoint=npoint.endpoints_pos[side]; // REF OK?
        Vector<T,Dim> & endpoint=*enpt->position; // REF OK?



        std::size_t found;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        try {
            tree.queryRange(
                endpoint,
                search_connection_point_center_candidate,
                candidates,
                query_buffer_size,
                found);
        } catch (mhs::STAError & error)
        {
            throw error;
        }

        sta_assert_debug0(npoint.maxconnections>0);

        /// no points nearby. do nothing
        if (found==1)
        {
            if (debug)
                printf("no points\n",found);
            return false;
        }

        
        T searchrad=CEdgecost<T,Dim>::get_current_edge_length_limit();
        

        if (debug)
            printf("found %d initial candidates in radius %f\n",found,searchrad);

	#ifdef D_USE_MULTITHREAD
	  int thread_id= omp_get_thread_num();
	#else
	  int thread_id= 0;
	#endif
	track_unique_id[thread_id]++;
      
	
	searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        if (debug)
            printf("collecting endpoints in circle\n");


        class Connection< T, Dim > * edge_candidates_buffer[query_buffer_size];
        class Connection< T, Dim > ** edge_candidates=edge_candidates_buffer;
	
	int compute_probability_for_indx=-1;
	

	
        std::size_t num_edge_candidates=0;
        {

            {
                /*!
                check for all candidates in the rectangle
                  if one of the endpoints lies within the circle
                    with radius "searchrad"
                */
                for (std::size_t a=0; a<found; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    

		    if ((point!=&npoint) // no self connections ;-)
		      &&((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)) // should not be true if not in voxgrid
		       &&((bifurcation_neighbors)||(point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION)) // no bifurcation poins
		       &&((point->endpoint_connections[0]==1)||(point->endpoint_connections[1]==1)) // is connected
// 		      &&(((point->endpoint_connections[0]==1)&&(point->endpoints[0][0]->connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION))
// 		        ||((point->endpoint_connections[1]==1)&&(point->endpoints[1][0]->connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION))) // is connected
 		       &&(connected_point!=point)// not already connected with terminal
		       ) 
		      
                    {
                        //TODO might take into coinsideration:
                        //would always happensif I would use endpoint position
                        //instead of point posiiton because queryRange already supports sphere selection
                        //
			{
			/// check if point center is in candidate range  
			 //WARNING propbably is should check (!dist>distmax) to include equal case 
                        T candidate_center_dist_sq=(Vector<T,Dim>(point->get_position())-npoint.get_position()).norm2();
                        if (search_connection_point_center_candidate>candidate_center_dist_sq)
                        {
			  
			    /// search eachside for the connected edge (must be 1 per side ?)
                            for (int pointside=0; pointside<2; pointside++)
                            {
				  class Connection<T,Dim> * connection=NULL;
				  
				  
				  
				   
				  
				    // no edge
				    if (point->endpoint_connections[pointside]==0)  
				      continue;
				    
				     class Points<T,Dim>::CEndpoint * endpointsA=point->endpoints[pointside][0];
				    // an edge must be there
				    sta_assert_error(point->endpoint_connections[pointside]==1);		
				    sta_assert_debug0((endpointsA->connected!=NULL));
				    
					if ((endpointsA->connected!=NULL)///TODO absolute
					&&(endpointsA->connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION)  
					&&(endpointsA->connected->point!=&npoint)//not connected to the terminal point
					&&(endpointsA->connected->point!=connected_point) // not connected to the pre-terminal
					&&(bifurcation_neighbors||(endpointsA->connected->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))
					&&(endpointsA->connected->endpoint_connections[0]==1)///TODO absolute because no bifurcation
					)
					{
					  connection=endpointsA->connection;
					}
	
				  
				  
				  
/*				    
				    int connections_found=0;
				    //NOT NEEDED ANYMORE search the connecting edge
				    class Points<T,Dim>::CEndpoint * endpointsA=point->endpoints_[pointside];
				    for (int i=0;i<Points<T,Dim>::maxconnections;i++)
				    {
				      if ((endpointsA[i].connected!=NULL)
					&&(endpointsA[i].connected->point!=&npoint)
					&&(endpointsA[i].connected->point!=connected_point)
					&&(endpointsA[i].connected->point->endpoint_connections[0]==1)
					&&(endpointsA[i].connected->point->endpoint_connections[1]==1)
				      )
				      {
					connection=endpointsA[i].connection;
					connections_found++;
				      }
				    }
				    sta_assert_debug0(connections_found<=1);
				    if (connections_found==1)
*/				      
				      
				  if (connection!=NULL)
				  {
				    sta_assert_debug0(connection!=NULL);
				    sta_assert_debug0(connection->pointA->connected!=NULL);
				    sta_assert_debug0(connection->pointB->connected!=NULL);				    
				    
				    
				    
				    /// check if both endpoints of edge are in candidate range (max edge range)
				    bool edge_is_valid_candidate=true;
				    
				    if (connection->track_me_id==track_unique_id[thread_id])
				    {
				      edge_is_valid_candidate=false;
				    }else
				    {
				      connection->track_me_id=track_unique_id[thread_id];
				      for (int edge_side=0;edge_side<2;edge_side++)
				      {
					
					class Points<T,Dim> * edge_endpoint=connection->points[edge_side]->point;
					int & side=connection->points[edge_side]->side;
  // 				      edge_endpoint->update_endpoint(side);
					Vector<T,Dim> & candidate_endpoint=edge_endpoint->endpoints_pos[side];
					T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
					if (!(searchrad>candidate_dist_sq))
					{
					  edge_is_valid_candidate=false;
					  break;
					}
				      }
				    }
				    
				    /// add the edge as edge candiate (NOTE: note nay more, thanks thread d: note that currently each egde appears twice)
				    if (edge_is_valid_candidate)
				    {
				      (*edge_candidates++)=connection;
				      sta_assert_debug0(connection->pointA->point!=&npoint);
				      sta_assert_debug0(connection->pointB->point!=&npoint);
				      sta_assert_debug0(connection->pointA->point!=connected_point);
				      sta_assert_debug0(connection->pointB->point!=connected_point);
				      
				      
				      //printf("@: %u %u ",connection,compute_probability_for_edge);
				      if ((compute_probability_for_edge!=NULL)&&(connection==compute_probability_for_edge))
				      {
					compute_probability_for_indx=num_edge_candidates;
					//printf("!");
				      }
				      //printf("\n");
				      num_edge_candidates++;
				    }
				  }
                            }
                        }
		    }/*else
		      throw STAError("already onnected with bifurcation (should not happen with triangle edge representation)");*/
                    }
                }
                
                
                if (compute_probability_for_edge!=NULL)
		{
		  if (bifurcation_neighbors)
		  {
		    if (compute_probability_for_indx<0)
		    {
		      T candidate_dist_sqA=(endpoint-*compute_probability_for_edge->pointA->position).norm2();
		      T candidate_dist_sqB=(endpoint-*compute_probability_for_edge->pointB->position).norm2();
		      
		      printf("%f\n",candidate_dist_sqA);
		      printf("%f\n",candidate_dist_sqB);
		      printf("%f\n",searchrad);
		      
		      throw mhs::STAError("couldn't find edge");
		      //printf("couldn't find edge!!!\n");
		      return false;
		    }
		  }else // the existing edge has been excluded from the previous search
		  {     // and must be added manually
		      sta_assert_error(num_edge_candidates+1<query_buffer_size);
		      (*edge_candidates++)=compute_probability_for_edge;
		      compute_probability_for_indx=num_edge_candidates;
		      num_edge_candidates++;
		  }
		}
	
		
                
		/// no edges nearby -> do nothing
		if ((num_edge_candidates==0))
		{
		    if (debug)
			printf("no edges %d\n",num_edge_candidates);

		    return false;
		}

		//printf("endpoint nearby: %d\n",num_connet_candidates);

                if (debug)
                    printf("computing probabilities for %d edges\n",num_edge_candidates);

		T connet_cost[query_buffer_size];
                T connet_prop[query_buffer_size];
                T connet_prop_acc[query_buffer_size];


		edge_candidates=edge_candidates_buffer;		
		int candidates_count=0;
		
// 		class Connection< T, Dim > ** final_edge_candidates=edge_candidates_buffer;
		
		for (std::size_t i=0; i<num_edge_candidates; i++)
                {
		  
		    class Connection< T, Dim > * edge=edge_candidates_buffer[i];
		    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		    
		    sta_assert_debug0(edge->edge_type==EDGE_TYPES::EDGE_SEGMENT);
		    
		    
                    T u=edgecost_fun.e_cost(*enpt,
                                            *(edge->pointA),
					    *(edge->pointB),
                                            //searchrad,
					    //-  (CEdgecost<T,Dim>::max_edge_length>-1) ? -1 : searchrad, //-1,1, ///NOTE we have sorted out all edges not in distance already
                                            inrange,
					    false
 					  );
                    //sta_assert_error(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));


                    if (debug)
                    {
                        printf("u:  %f\n",u);
                    }

                    

                    if (inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		    {
			connet_cost[candidates_count]=u+bifurL;
                        connet_prop[candidates_count]=mhs_fast_math<T>::mexp((u+Lprior)/TempConnect)+ConnectEpsilon;
		    }
                    else
		    {
			sta_assert_debug0((compute_probability_for_indx!=i));
                        //connet_prop[candidates_count]=0;
		        continue;
		    }

                    if (candidates_count==0)
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count];
                    }
                    else
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
                    }
                    
                    //remapping the search indx
                    if (compute_probability_for_indx==i)
		    {
		      compute_probability_for_indx=candidates_count;
		    }
		    
		    //alining all valid edges to the beginning of the array
		    edge_candidates_buffer[candidates_count]=edge;
                    
                    candidates_count++;
		}
		
		sta_assert_debug0(!(num_edge_candidates<candidates_count));
		num_edge_candidates=candidates_count;
		
		/// no edges nearby -> do nothing
		if ((num_edge_candidates==0))
		{
		    if (debug)
			printf("no valid edges %d\n",num_edge_candidates);

		    sta_assert_debug0((compute_probability_for_edge==NULL));
		    return false;
		}
		
		
                if (compute_probability_for_edge!=NULL)
		{
		  sta_assert_debug0(compute_probability_for_indx>-1);
		  sta_assert_debug0(compute_probability_for_indx<num_edge_candidates);
		  sta_assert_debug0(edge_candidates_buffer[compute_probability_for_indx]==compute_probability_for_edge);
		  connection_prob=connet_prop[compute_probability_for_indx]/(connet_prop_acc[candidates_count-1]+std::numeric_limits< T >::epsilon());
		  connection_cost=connet_cost[compute_probability_for_indx];
		  return true;
		}




                if (debug)
                    printf("creating proposal\n");

                /*!
                  now we choose a candidate with probability connet_prop
                  using connet_prop_acc from the edge candidate list
                */
                {
                    // pic candidate with propability connect_prop
                    std::size_t connection_candidate=rand_pic_array(connet_prop_acc,num_edge_candidates,connet_prop_acc[candidates_count-1]);

                    if (debug)
                        printf("choosing %d\n",connection_candidate);

                    sta_assert_debug0(connection_candidate>=0);
                    sta_assert_debug0(connection_candidate<num_edge_candidates);


                        if (debug)
                            printf("connection proposal\n");

                        sta_assert_debug0(connection_candidate>=0);
                        sta_assert_debug0(connection_candidate<num_edge_candidates);

			if (connet_prop_acc[candidates_count-1]<std::numeric_limits<T>::epsilon())
			{
			  connection_prob=0;
			  return false;
			}
			else
			{
			  connection_prob=connet_prop[connection_candidate]/(connet_prop_acc[candidates_count-1]);
			}
			
			
			
			connection_cost=connet_cost[connection_candidate];
		  
			proposed_connection=edge_candidates_buffer[connection_candidate];
			
                        sta_assert_debug0(proposed_connection->pointA!=NULL);
                        sta_assert_debug0(proposed_connection->pointB!=NULL);

                        if (debug)
                            printf("conn index %d\n",connection_candidate);
			
                        return true;
                }
            }
        }

        mhs::STAError error;
	error<<"should not be reached! ("<<enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH)<<")";
        throw error;
        //throw mhs::STAError("should not be reached!");
        return true;
    }


};





template<typename TData,typename T,int Dim>
class CProposalBifurcationDeath : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
     T ConnectEpsilon;
      const static int max_stat=10;
    int statistic[max_stat];
    
        T lambda;
public:


    T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_segment(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }

    CProposalBifurcationDeath(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
	
	 lambda=100;
    }
    
      ~CProposalBifurcationDeath()
    {
//         printf("\n");
// 	for (int i=0;i<max_stat;i++)
// 	  printf("%d ",statistic[i]);
// 	printf("\n");
	
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH);
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH);
    }

    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
      
      
        if (params==NULL)
            return;
	
	 if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
		Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;
	
	bool debug=false;
	
	/// no terminals?
	if (connections.get_num_bifurcation_centers()<1)
	  return false;

	int stat_count=0;
//0	
statistic[stat_count++]++;	
	
	try
	{
	/// randomly pic a bifurcation center node
	Points<T,Dim> &  point=connections.get_rand_bifurcation_center_point();	  
	
	
		
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
	//uniformly pick an edge that is supposed to be removed
	int remove_edge=std::rand()%3;
	
	if (point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connection->is_protected_topology())
// 	  )(point.is_protected_topology() || 
	{
	  return false;
	}
	
 	class Points<T,Dim> * new_terminal_point=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connected->point;
// 	bifurcation_point=point;	
	
	

	
	if (new_terminal_point->get_num_connections()!=2)
	{
	  if (debug)
	  printf("x]");			

	  return false;
	}
	
	//after removing the bif the terminal must be an ordinary segment terminal
	if ((new_terminal_point->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION)&&
	  (new_terminal_point->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
	{
      if (debug)
	printf("x]\n");			
	    sta_assert_error(1!=0);
	  return false;
	}
	
	T particle_costs=0;
	particle_costs-=point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point->compute_cost3(temp); 
	particle_costs-=point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->point->compute_cost3(temp); 
	particle_costs-=point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->point->compute_cost3(temp); 
	
      
        T particle_terminalcool_cost_old=0;
	  T particle_terminalcool_cost_new=0;

	  class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	  class Collision<T,Dim>::Candidates candA(query_bufferA);
	  
	  int collision_mode=1;
	  
	    if (collision_fun_p.is_soft())  
	    {
	sta_assert_error(!collision_fun_p.colliding(particle_terminalcool_cost_old,
					    tree,
					  *new_terminal_point,
					  collision_search_rad,
					  temp,&candA,true,-9,collision_mode));
	}
	
	
	
	
if(debug)	
  printf("B_DEATH [");		
	
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	//computing edge costs of the OLD configuration
	T old_edgecost=point.e_cost(
	  edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange
	);
/// old edge configuration must be valid!
	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));

if (debug)
  printf(",");			

	
	
	sta_assert_debug0((new_terminal_point->endpoint_connections[0]==1)&&(new_terminal_point->endpoint_connections[1]==1));
if (debug)
  printf("e");			
	
	

if (debug)
  printf(":");	
	
	sta_assert_debug0((new_terminal_point->endpoints[1][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION)||
	  (new_terminal_point->endpoints[0][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION));
	

	class Connection< T, Dim >    * p_new_edges;
	class Connection< T, Dim >    new_edges;
	new_edges.edge_type=EDGE_TYPES::EDGE_SEGMENT;

if (debug)
  printf(".");		
	int j=0;
	for (int i=0;i<3;i++)
	{
	  if (i!=remove_edge)
	  {
	    sta_assert_debug0(j<2);
	    new_edges.points[j]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected;
	    j++;
	  }
	}
	
if (debug)
  printf("q");	

#ifdef  D_USE_GUI
	class Points<T,Dim>::CEndpoint * old_bif_endpoints[3];
#endif	

	 //backup old edges 
	class Connection< T, Dim >  backup_old_edges[3];
	for (int i=0;i<3;i++)
	{
	  
	  sta_assert_error((point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connection)!=NULL);
	  #ifdef  D_USE_GUI
	      old_bif_endpoints[i]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected;
	  #endif
	  backup_old_edges[i]=*point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connection;
	}

if(debug)	
  printf("(remove old)");	
	
	int protectd_topologies=0;
	 //remove old edges 
	for (int i=0;i<3;i++)
	{
	  if(debug)	
	      printf("[%i]:",i);	
	  sta_assert_debug0((point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected)!=NULL);
	  class Connection< T, Dim >    * delete_me=point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection;
	  protectd_topologies+=delete_me->is_protected_topology();
	  sta_assert_debug0(delete_me!=NULL);
	  connections.remove_connection(delete_me,false);
	  if (i<2)
	    sta_assert_debug0((point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected)!=NULL);
	}

if(debug)	
  printf("ok");
	sta_assert_error((protectd_topologies==0)||(protectd_topologies==2))
	
	
	
	//compute costs for new edge
	T  new_edgecost=new_edges.e_cost(
	   edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange
	);
	
if(debug)	
  printf("/");	
	
	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	{
	  for (int i=0;i<3;i++)
	    connections.add_connection(backup_old_edges[i]);
if(debug)	
  printf("?\n");	  
	  return false;
	}
if(debug)	
  printf("\n");	  	
	
	//add new edge
	p_new_edges=connections.add_connection(new_edges);
	

	p_new_edges->freeze_topology=(protectd_topologies==2);
	
	// compute the probablity for the bifurcation given the new edge
	class Connection<T,Dim> * existing_connection=NULL;
	T connection_prob;
	T connection_costs;
	if (!CProposalBifurcationBirth<TData,T,Dim>::do_tracking3(
		      edgecost_fun,
		      existing_connection,
		      connection_prob,
		      connection_costs,
		      tree,
		      *new_terminal_point,
		      search_connection_point_center_candidate,
		      //options.connection_candidate_searchrad,
		      conn_temp,
		      options.bifurcation_bonus_L,
		      Lprior,
		      ConnectEpsilon,
		      options.bifurcation_neighbors,
		      p_new_edges
		      ))
	{
	  
	   mhs::STAError error;
	    error<<"do_tracking3:should not be reached! ("<<enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH)<<")";
	  throw error;
	  //return false;
	}
	
	sta_assert_error(existing_connection==NULL);
	sta_assert_error(p_new_edges!=NULL);
	
if(debug)	
  printf("[");		
	
	particle_costs+=new_terminal_point->compute_cost3(temp); 
	particle_costs+=p_new_edges->pointA->point->compute_cost3(temp); 
	particle_costs+=p_new_edges->pointB->point->compute_cost3(temp); 
	
	if (collision_fun_p.is_soft())  
	    {
	   sta_assert_error(!collision_fun_p.colliding(particle_terminalcool_cost_new,
						tree,
					      *new_terminal_point,
					      collision_search_rad,
					      temp,&candA,false,-10,collision_mode));
	    }
	
if(debug)	
  printf("*");	
	
// 	T terminal_point_cost_change=new_terminal_point->compute_cost()*(particle_connection_state_cost_scale[1]-particle_connection_state_cost_scale[2])
// 	  +(particle_connection_state_cost_offset[1]-particle_connection_state_cost_offset[2]);

	T E=(new_edgecost-old_edgecost)/temp;
// 	  E+=(terminal_point_cost_change)/temp;
	E+=(particle_costs)/temp;
	
	  if (collision_fun_p.is_soft())   
	{
	    E+=(particle_terminalcool_cost_new-particle_terminalcool_cost_old)/temp;
	}

          T R=mhs_fast_math<T>::mexp(E);
	
// 	  printf("%f %f ",E,R);
	T create_connection_prob=connection_prob/connections.get_num_terminals(); 
// 	printf("[%f, %f ",connection_prob,create_connection_prob);
	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()-1+std::numeric_limits< T >::epsilon());
// 	printf("%f] ",remove_connection_prob);
	R*=create_connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());
	
// 	printf(" %f\n ",R);
// 	printf("%f %f %f\n",new_edgecost,old_edgecost,terminal_point_cost_change);
if(debug)	
  printf("^");		
//1
statistic[stat_count++]++;		
	  if ((R>=myrand(1)+std::numeric_limits<T>::epsilon()) && ((conn_tree.insert(*new_terminal_point,false,true))))
	  {
	
	     
	    
	    
	    #ifdef  D_USE_GUI
// 	      Points< T, Dim > * p_bif_center_point=&point;
// 	      (GuiPoints<T>* (p_bif_center_point))->touch(get_type());
// 	      for (int i=0;i<3;i++)
// 	      {
// 		  (GuiPoints<T>* (p_bif_center_point->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point))->touch(get_type());
// 	      }
	      
	      //point.touch(get_type());
	      for (int i=0;i<3;i++)
	      {
		  //point.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		  old_bif_endpoints[i]->point->touch(get_type());
	      }
	    #endif	
		
		Points< T, Dim > * p_point=&point;
		 particle_pool.delete_obj(p_point);
		 sta_assert_debug0(p_point==NULL);
		 
		if (!conn_tree.insert(*new_terminal_point))
		{
		    new_terminal_point->print();
		    throw mhs::STAError("error insering point\n");
		    return false;
		}
		sta_assert_debug0(
		  (new_terminal_point->endpoint_connections[0]>0)!=
		(new_terminal_point->endpoint_connections[1]>0));
		
		


		
		  #ifdef BIFURCATION_DEBUG_EDGECHECK
		  // 	for (int i=0;i<3;i++)
		   	{
		  	p_new_edges->e_cost(
		  		    edgecost_fun,
		  	      options.connection_bonus_L,
		  	      options.bifurcation_bonus_L,
		  	      inrange,true,false,bifurcation_edgecheck_update);
		  	    
		  	    /// of course the old edge should be in range (valid)
		  	    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		  	}
// 			    (b_particle.edge_energy(edgecost_fun,
// 					  options.connection_bonus_L,
// 					options.bifurcation_bonus_L,
// 					  maxendpointdist,
// 					inrange,false,bifurcation_edgecheck_update));
// 			      sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
			#endif
		
		
if(debug)	
  printf(":-)\n");	

		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;

	    return true;
	  }
//2	  
statistic[stat_count++]++;		  
	
if(debug)	
  printf("=");	

	connections.remove_connection(p_new_edges,false);
if(debug)	
  printf(":");		

	for (int i=0;i<3;i++)
	{
if(debug)	
    printf("adding %d",i);
// 	    connections.add_connection(backup_old_edges[i]);
	    connections.add_connection(backup_old_edges[i]);
	    
if(debug)	
    printf("[%d]",i);
	}
if(debug)	
  printf("!\n");	
	
	
	return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }  
	
	
    }
};






template<typename TData,typename T,int Dim>
class CProposalBifurcationChange : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
      const static int max_stat=10;
    int statistic[max_stat];
public:

   T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
     return CProposal<TData,T,Dim>::dynamic_weight_bif(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }



    CProposalBifurcationChange(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
    }
    
      ~CProposalBifurcationChange()
    {
//         printf("\n");
// 	for (int i=0;i<max_stat;i++)
// 	  printf("%d ",statistic[i]);
// 	printf("\n");
	
    }
    
    T get_default_weight(){return 0.1;};

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE);
    };
    
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	
	this->proposal_called++;
	
	
	
	/// no terminals?
	if (connections.get_num_bifurcation_centers()<1)
	  return false;

	int stat_count=0;	
	
	//0
statistic[stat_count++]++;	
	
	try
	{
	/// randomly pic a bifurcation node
	Points<T,Dim> &  point=connections.get_rand_bifurcation_center_point();	  
	
	/// check if point is a bifurcation! 
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
	
	int flip_point=std::rand()%3;
	
	//is the point that remains in bifurcation but the to endpoint switch roles 
	class Points<T,Dim>::CEndpoint  * flip_bif_endpoint=point.endpoints[Points<T,Dim>::bifurcation_center_slot][flip_point]->connected;
	class Points<T,Dim>::CEndpoint  * flip_bif_opposed_endpoint=flip_bif_endpoint->opposite_slots[0];
	
// 	T particle_costs=0;
// 	particle_costs-=flip_bif_endpoint->point->compute_cost3(temp); 
// 	particle_costs-=flip_bif_opposed_endpoint->point->compute_cost3(temp); //FIXME : same point!!!
	
// 	printf("%u %u\n",(std::size_t)flip_bif_opposed_endpoint->point,(std::size_t)flip_bif_opposed_endpoint->point);
	
	
	 
	sta_assert_debug0(flip_bif_endpoint->side==1-flip_bif_opposed_endpoint->side);

	// the new endpoint taking part in the bifurcation
	class Points<T,Dim>::CEndpoint  * bif_new_endpoint=flip_bif_opposed_endpoint->connected;
	
	//check if is connected (will become part of new bifurcation)
	if (bif_new_endpoint==NULL)
	  return false;
	
	//check if edge is segment
	if (flip_bif_opposed_endpoint->connection->edge_type!=EDGE_TYPES::EDGE_SEGMENT)
	  return false;
	
	//if no touching bifurcations avoid it (don't propose it :) )
	if (!(options.bifurcation_neighbors)&&(bif_new_endpoint->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT))
	  return false;
	
	
	T particle_costs=0;
	particle_costs-=flip_bif_endpoint->point->compute_cost3(temp); 
	particle_costs-=bif_new_endpoint->point->compute_cost3(temp); 
	
// 	printf("%u %u\n",(std::size_t)flip_bif_opposed_endpoint->point,(std::size_t)bif_new_endpoint->point);
	
	
	class Points<T,Dim>::CEndpoint  * further_endpts[2];
	int j=0;
	for (int i=0;i<3;i++)
	{
	  if (i!=flip_point)
	  {
	  further_endpts[j++]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i];
	  }
	}
	sta_assert_debug0(j==2);
	sta_assert_debug0(further_endpts[0]!=further_endpts[1]);
	sta_assert_debug0(further_endpts[0]!=flip_bif_endpoint);
	sta_assert_debug0(further_endpts[1]!=flip_bif_endpoint);
	
	
	sta_assert_debug0(flip_bif_endpoint->point->endpoint_connections[0]==1);
	sta_assert_debug0(flip_bif_endpoint->point->endpoint_connections[1]==1);
	
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	//computing edge costs of the OLD configuration
	T old_edgecost=flip_bif_endpoint->point->e_cost(
	   edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange
	);
	
	sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));


//is the point which will not be part of the bif anymore
	int remove_point=std::rand()%2;
	class Points<T,Dim>::CEndpoint  * bif_endpoint_remains=further_endpts[1-remove_point]->connected;
	class Points<T,Dim>::CEndpoint  * bif_endpoint_remove=further_endpts[remove_point]->connected;
	

	// backup and remove all old connections
	class Connection< T, Dim > edge_backup[4];
	class Connection< T, Dim > * p_edge_backup[4];
// 	p_edge_backup[0]=(point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection);
// 	p_edge_backup[1]=(point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connection);
// 	p_edge_backup[2]=(point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connection);
	
	p_edge_backup[0]=flip_bif_endpoint->connection;
	p_edge_backup[1]=bif_endpoint_remains->connection;
	p_edge_backup[2]=bif_endpoint_remove->connection;
	p_edge_backup[3]=(flip_bif_opposed_endpoint->connection);
	
	
	bool protect_topology[4];
	for (int i=0;i<4;i++)
	{
	  protect_topology[i]=p_edge_backup[i]->is_protected_topology();
	}
	
	//(p_edge_backup[0]&&p_edge_backup[2]); //new_single bewteen flip_bif_endpoint and bif_endpoint_remove
	//p_edge_backup[3] //old single  edge -> new flip_bif_opposed_endpoint  and bif_new_endpoint
	//(p_edge_backup[1]&&p_edge_backup[2]); -> bif_endpoint_remains &  flip_bif_opposed_endpoint
	//(p_edge_backup[0]&&p_edge_backup[1]); -> bif_endpoint_remains &  bif_new_endpoint
	
	
	for (int i=0;i<4;i++)
	{
	  edge_backup[i]=*p_edge_backup[i];
	  connections.remove_connection(p_edge_backup[i]);
	}
	
	sta_assert_debug0(bif_endpoint_remains!=bif_new_endpoint);
	sta_assert_debug0(bif_endpoint_remains->point!=bif_new_endpoint->point);
	sta_assert_debug0(flip_bif_opposed_endpoint!=bif_new_endpoint);
	sta_assert_debug0(flip_bif_opposed_endpoint->point!=bif_new_endpoint->point);
	sta_assert_debug0(flip_bif_opposed_endpoint!=bif_endpoint_remains);
	sta_assert_debug0(flip_bif_opposed_endpoint->point!=bif_endpoint_remains->point);
	sta_assert_debug0(point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
	// create new connections
	class Connection<T,Dim> a_new_connection; 

	
	a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;	
	a_new_connection.freeze_topology=p_edge_backup[3] || (p_edge_backup[1]&&p_edge_backup[2]);
	a_new_connection.pointA=point.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	a_new_connection.pointB=flip_bif_opposed_endpoint->point->getfreehub(flip_bif_opposed_endpoint->side);
	
	sta_assert_debug0(a_new_connection.pointA->point!=a_new_connection.pointB->point);
	sta_assert_debug0(a_new_connection.pointA!=a_new_connection.pointB);
	p_edge_backup[0]=connections.add_connection(a_new_connection);
	
	
	a_new_connection.freeze_topology=p_edge_backup[3] || (p_edge_backup[0]&&p_edge_backup[1]);
	a_new_connection.pointA=point.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	a_new_connection.pointB=bif_new_endpoint->point->getfreehub(bif_new_endpoint->side);

	sta_assert_debug0(a_new_connection.pointA->point!=a_new_connection.pointB->point);
	sta_assert_debug0(a_new_connection.pointA!=a_new_connection.pointB);
	p_edge_backup[1]=connections.add_connection(a_new_connection);
	
	a_new_connection.freeze_topology=(p_edge_backup[1]&&p_edge_backup[2]) || (p_edge_backup[0]&&p_edge_backup[1]);;
	a_new_connection.pointA=point.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	a_new_connection.pointB=bif_endpoint_remains->point->getfreehub(bif_endpoint_remains->side);

	sta_assert_debug0(a_new_connection.pointA->point!=a_new_connection.pointB->point);
	sta_assert_debug0(a_new_connection.pointA!=a_new_connection.pointB);
	p_edge_backup[2]=connections.add_connection(a_new_connection);	
	
	
	a_new_connection.edge_type=EDGE_TYPES::EDGE_SEGMENT;	
	a_new_connection.freeze_topology=(p_edge_backup[0]&&p_edge_backup[2]);
	a_new_connection.pointA=flip_bif_endpoint;
	a_new_connection.pointB=bif_endpoint_remove->point->getfreehub(bif_endpoint_remove->side);
	
	
	

	sta_assert_debug0(a_new_connection.pointA->point!=a_new_connection.pointB->point);
	sta_assert_debug0(a_new_connection.pointA!=a_new_connection.pointB);
	p_edge_backup[3]=connections.add_connection(a_new_connection);
	
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
	bool out_of_bounce=false;
	point.bifurcation_center_update(shape,out_of_bounce);
	if (out_of_bounce)
	{
	 for (int i=0;i<4;i++)
	      connections.remove_connection(p_edge_backup[i]);
	    for (int i=0;i<4;i++)
	      connections.add_connection(edge_backup[i]);
	    point.bifurcation_center_update(shape,out_of_bounce);
	 sta_assert_debug0(!out_of_bounce);
	    return false; 
	}
	
	
	
	sta_assert_debug0(flip_bif_endpoint->point->endpoint_connections[0]==1);
	sta_assert_debug0(flip_bif_endpoint->point->endpoint_connections[1]==1);
	//typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	//computing edge costs of the OLD configuration
	T new_edgecost=flip_bif_endpoint->point->e_cost(
	   edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange
	);
	
	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	{
	    for (int i=0;i<4;i++)
	      connections.remove_connection(p_edge_backup[i]);
	    for (int i=0;i<4;i++)
	      connections.add_connection(edge_backup[i]);
	    point.bifurcation_center_update(shape,out_of_bounce);
	 sta_assert_debug0(!out_of_bounce);
	    return false;
	}
	
	
// 	particle_costs+=flip_bif_endpoint->point->compute_cost3(temp); 
// 	particle_costs+=flip_bif_opposed_endpoint->point->compute_cost3(temp);

	particle_costs+=flip_bif_endpoint->point->compute_cost3(temp); 
	particle_costs+=bif_new_endpoint->point->compute_cost3(temp);
	
	
	  
	T E=(new_edgecost-old_edgecost)/temp;
	E+=(particle_costs)/temp;
// 	printf("%f %f\n",new_edgecost,old_edgecost);

        T R=mhs_fast_math<T>::mexp(E);
	
//1
statistic[stat_count++]++;		
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	    
		if (options.no_double_bifurcations)
		{
		    if (point.connects_two_bif())
		    {
			for (int i=0;i<4;i++)
			    connections.remove_connection(p_edge_backup[i]);
			  for (int i=0;i<4;i++)
			    connections.add_connection(edge_backup[i]);
			    point.bifurcation_center_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
			return false;
		    }
		}
	    
	    
		
	    
		if (Constraints::constraint_hasloop_follow(point,p_edge_backup[1],options.constraint_loop_depth))
		{
		   for (int i=0;i<4;i++)
		      connections.remove_connection(p_edge_backup[i]);
		    for (int i=0;i<4;i++)
		      connections.add_connection(edge_backup[i]);
		      point.bifurcation_center_update(shape,out_of_bounce);
		  sta_assert_debug0(!out_of_bounce);
		  return false;
		}
		
		if (options.bifurcation_particle_hard)	
		{
		  if (collision_fun_p.colliding( tree,point,collision_search_rad,temp))
		  {
  			    for (int i=0;i<4;i++)
			      connections.remove_connection(p_edge_backup[i]);
			    for (int i=0;i<4;i++)
			      connections.add_connection(edge_backup[i]);
			    point.bifurcation_center_update(shape,out_of_bounce);
			    sta_assert_debug0(!out_of_bounce);
			    return false;
		  }
		}
		
		
		//new_connection->freeze_topology=connection.freeze_topology;
		
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		
		#ifdef  D_USE_GUI
// 		  Points< T, Dim > * p_bif_center_point=&point;
// 		  (GuiPoints<T>* (p_bif_center_point))->touch(get_type());
// 		  for (int i=0;i<3;i++)
// 		  {
// 		      (GuiPoints<T>* (p_bif_center_point->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point))->touch(get_type());
// 		  }
// 		  (GuiPoints<T>* (bif_new_endpoint->point))->touch(get_type());
		
		  point.touch(get_type());
		  for (int i=0;i<3;i++)
		  {
			point.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
		  }
		  (bif_new_endpoint->point)->touch(get_type());
		  
		#endif	
		
		
// 		  #ifdef BIFURCATION_DEBUG_EDGECHECK
// 		// 	for (int i=0;i<3;i++)
// 		// 	{
// 		// 	new_connections[i]->compute_cost(
// 		// 		    edgecost_fun,
// 		// 	      options.connection_bonus_L,
// 		// 	      options.bifurcation_bonus_L,
// 		// 	      maxendpointdist,
// 		// 	      inrange,false,bifurcation_edgecheck_update);
// 		// 	    
// 		// 	    /// of course the old edge should be in range (valid)
// 		// 	    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 		// 	}
// 			  (b_particle.edge_energy(edgecost_fun,
// 					options.connection_bonus_L,
// 				      options.bifurcation_bonus_L,
// 					maxendpointdist,
// 				      inrange,false,bifurcation_edgecheck_update));
// 			    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 		      #endif

		
	    return true;
	  }
	    
	    //2
	    statistic[stat_count++]++;		  

	   for (int i=0;i<4;i++)
	      connections.remove_connection(p_edge_backup[i]);
	    for (int i=0;i<4;i++)
	      connections.add_connection(edge_backup[i]);
	    point.bifurcation_center_update(shape,out_of_bounce);
	 sta_assert_debug0(!out_of_bounce);
        return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
	
	
	
	
	
	
	
	
	
	
      
    }
};


#ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
  #include "proposal_TriangleBifurcationTrack.h"
#else
/*###################################################################################
 * 
 * 				PREVIOUS STYLE
 * 
 *###################################################################################
 */

  #ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING
    #include "proposal_TriangleBifurcationTrackDouble.h"

  #endif
#endif



#endif


