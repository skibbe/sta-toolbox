#ifndef PROPOSAL_INSERTBIRTHDEATH_H
#define PROPOSAL_INSERTBIRTHDEATH_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CInsertBirth : public CProposal<TData,T,Dim>
{
protected:
    T lambda;
      T particle_energy_change_costs;
      
//     mhs::dataArray<TData>  saliency_map;
//       double saliency_correction[3];
      bool temp_dependent;
      	 bool use_saliency_map;
	  bool consider_birth_death_ratio;
	    bool sample_scale_temp_dependent;
public:
  
    T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
       return CProposal<TData,T,Dim>::dynamic_weight_segment(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  
    CInsertBirth(): CProposal<TData,T,Dim>()
    {
        lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
        //randwalk=false;
// 	saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
	temp_dependent=true;
	
	 use_saliency_map=false;
	 consider_birth_death_ratio=false;
    }
    
    

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;


        if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	    
	   if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];
	   
	   if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];	    
	   
	   	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

//         try {
//             saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
//         } catch (mhs::STAError error)
//         {
//             throw error;
//         }
// 
// 	saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
// 	CData<T,TData,Dim> & data_fun=*(this->tracker->data_fun);	
// 	
// 	std::size_t real_shape[3];
// 	std::size_t shape[3];
// 	std::size_t offset[3];
// 	data_fun.get_real_shape(real_shape);
// 	data_fun.getshape(shape);
// 	data_fun.getoffset(offset);
// 	TData * p_saliency_map=saliency_map.data;
// 
// 	for (int z=offset[0];z<offset[0]+shape[0];z++)
// 	{
// 	  for (int y=offset[1];y<offset[1]+shape[1];y++)
// 	  {
// 	    for (int x=offset[2];x<offset[2]+shape[2];x++)
// 	    {
// 	      
// 	      std::size_t index=(z*real_shape[1]+y)*real_shape[2]+x;
// 	      
// 	      saliency_correction[0]=std::min(saliency_correction[0],double(p_saliency_map[index]));
// 	      saliency_correction[1]=std::max(saliency_correction[1],double(p_saliency_map[index]));
// 	      saliency_correction[2]+=p_saliency_map[index];
// 	    }
// 	  }
// 	}

    }

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);
	
	
	
        this->proposal_called++;
	
	if (sample_scale_temp_dependent && (!(temp<maxscale)))
	 {
	      return false; 
	 }

        class Connection<T,Dim> * edge;

        /// uniformly pic edge
        edge=connections.get_rand_edge();
        if (edge==NULL)
            return false;
        
        if (edge->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
	  return false;

	sta_assert_debug0((*edge->pointA->endpoint_connections==1)&&
 			  (*edge->pointB->endpoint_connections==1));
	
	sta_assert_debug0(edge->edge_type==EDGE_TYPES::EDGE_SEGMENT);
	
#ifdef 	_POOL_TEST
        sta_assert_error(edge->exist);
#endif

	  if (sample_scale_temp_dependent && ((temp>edge->pointA->point->get_scale()||temp>edge->pointB->point->get_scale())))
	  {
		return false; 
	  }
	
         
	
	bool protected_topology=(edge->is_protected_topology());

        /// uniformly one endpoint as reference (with id 0)
        class Points< T, Dim >::CEndpoint * epoints[2];
        int choice=std::rand()%2;
        epoints[choice]=edge->pointA;
        epoints[1-choice]=edge->pointB;
	
	
	
	
	
	
	// cancel if points are tooo close
	{
	    T min_thickness;
	    if (sample_scale_temp_dependent)
	    {
		min_thickness=Points<T,Dim>::predict_thickness_from_scale ( std::max(minscale,temp));
	    }else
	    {
		min_thickness=Points<T,Dim>::predict_thickness_from_scale (minscale);
	    }
	    
	    if (!( min_thickness*min_thickness<(*epoints[0]->position-*epoints[1]->position).norm2()))
	    {
	      return false;
	    }
	}
	
	
	///TODO does this exists? two directly connected bifurcations? this is only for check!
	#ifdef _BIF_CD_REQUIRES_2_ALL_CONNECT_ 
	 sta_assert_error(!((epoints[choice]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)&&(epoints[1-choice]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)));
	 if ((epoints[choice]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)&&(epoints[1-choice]->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION))
 	  return false;
	#endif


        /// update endpoint positions
//         epoints[0]->point->update_endpoint(epoints[0]->side);
//         epoints[1]->point->update_endpoint(epoints[1]->side);

	T lA=epoints[0]->point->get_thickness();
	T lB=epoints[1]->point->get_thickness();
	
	
	
	
	T wa=lB/(lA+lB);
	T wb=lA/(lA+lB);
	
        /// optimnal pos: in between (inversly scale weighted)
        Vector<T,Dim> optimal_pos=(*epoints[0]->position)*wa+(*epoints[1]->position)*wb;

        /// optimnal direction: direction of reference point pointing in edge direction
        Vector<T,Dim> optimal_direction=epoints[0]->point->get_direction()*Points<T,Dim>::side_sign[epoints[0]->side];

        /// optimnal scale: mean scale
        T optimal_scale=((epoints[0]->point->get_scale())+(epoints[1]->point->get_scale()))/2;

        /// create new point
        Points<T,Dim> * p_point=particle_pool.create_obj();
        Points<T,Dim> & point=*p_point;
        point.tracker_birth=2;


        /// likelyness for point the parameter of the new point
        T connection_probability=1;


        T temp_fact_pos=1;
	if (temp_dependent)
	  //temp_fact_pos=std::sqrt(temp);
	  temp_fact_pos=proposal_temp_trafo(temp,optimal_scale,T(0.5));
	
        T temp_fact_scale=temp_fact_pos+1;
        T & temp_fact_rot=temp_fact_pos;

        
	T new_scale;
	if (!single_scale)
	{
	  T scale_lower;
	  T scale_upper;
	  if (sample_scale_temp_dependent)
	  {
	    scale_lower=std::max(std::max(minscale,temp),optimal_scale/temp_fact_scale);
	    scale_lower=std::max(scale_lower,std::min(epoints[0]->point->get_scale(),epoints[1]->point->get_scale()));
	    scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
	    sta_assert_debug0(scale_lower<scale_upper);
	  }else
	  {
	    /// valid scale interval
	    scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
	    scale_lower=std::max(scale_lower,std::min(epoints[0]->point->get_scale(),epoints[1]->point->get_scale()));
	    scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
	  }
	  
	  
	  /// pic scale (uniformly)
  //         point.scale=myrand(scale_lower,scale_upper);
	  connection_probability*=1.0/(scale_upper-scale_lower+std::numeric_limits<T>::epsilon());
	  
	  new_scale=myrand(scale_lower,scale_upper);
	}else
	{
	  new_scale=maxscale;
	}

        /// pic position (normal)
        T agility=temp_fact_pos*std::sqrt((*(epoints[0]->position)-*(epoints[1]->position)).norm2())/4;
        Vector<T,Dim> randv;
        randv.rand_normal(agility);
	
//         point.position=optimal_pos+randv;
	
	point.set_pos_scale(optimal_pos+randv,new_scale);

//         if ((point.position[0]<0)||
//                 (point.position[1]<0)||
//                 (point.position[2]<0)||
// 		  (!(point.position[0]<shape[0]))||
// 		  (!(point.position[1]<shape[1]))||
// 		  (!(point.position[2]<shape[2]))
//            ) 
	if (point.out_of_bounds(shape))
	{
            particle_pool.delete_obj(p_point);
            return false;
        }
        connection_probability*=normal_dist<T,3>(randv.v,agility);

        /*
        if (return_statistic)
        {
            std::size_t center=(std::floor(point.position[0])*shape[1]
              +std::floor(point.position[1]))*shape[2]
              +std::floor(point.position[2]);
          static_data[center*2*numproposals+16]+=1;
        }
        */

// 		pic direction (normal)
//		random_pic_direction<T,Dim>(
//		optimal_direction,
//		point.direction,temp_fact_rot);

      point.set_direction(random_pic_direction<T,Dim>(
            optimal_direction,temp_fact_rot));
	
        connection_probability*=normal_dist_sphere(optimal_direction,point.get_direction(),temp_fact_rot);

        /// we consider the new particel as directed (just because I'm lazy)
        /// -> point must have same orientation as reference point
//         if (optimal_direction.dot(point.direction)<0)
//         {
//             particle_pool.delete_obj(p_point);
//             return false;
//         }



	

	


        T newpointsaliency=0;
//         if (!data_fun.interp3(newpointsaliency,
//                      saliency_map.data,
//                      point.position,
//                      shape))
	  if ((!data_fun.saliency_get_value(newpointsaliency,point.get_position()))||(newpointsaliency<std::numeric_limits<T>::min()))
        {
            particle_pool.delete_obj(p_point);
            return false;
        }
//         if (newpointsaliency<std::numeric_limits<T>::epsilon())
// 	{
// 	    particle_pool.delete_obj(p_point);
// 	    return false;
// 	}
        
        

        T newenergy_data;

        if (!data_fun.eval_data(
                    newenergy_data,
		    point))
//                     point.get_direction(),
//                     point.get_position(),
//                     point.get_scale()))
        {
            particle_pool.delete_obj(p_point);
            return false;
        }
        
        /// point collides?
	T particle_interaction_cost_new=0;
	T particle_interaction_cost_old=0;

        
        point.point_cost=newenergy_data;
        point.saliency=newpointsaliency;///saliency_correction[2];
	
	
	class Connection<T,Dim>   new_connections[2];
	class Connection<T,Dim> * old_connection=edge;
        
        new_connections[0].pointA=epoints[0];
	new_connections[0].pointB=point.getfreehub(1);
	new_connections[0].freeze_topology=protected_topology;
	
        sta_assert_debug0(new_connections[0].pointA!=NULL);
        sta_assert_debug0(new_connections[0].pointB!=NULL);
        
        new_connections[1].pointA=epoints[1];
	new_connections[1].pointB=point.getfreehub(0);
	new_connections[1].freeze_topology=protected_topology;
	
        sta_assert_debug0(new_connections[1].pointA!=NULL);
        sta_assert_debug0(new_connections[1].pointB!=NULL);	

	T new_edgecost=0;
	
        /// computing  new edge costs
        typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
        for (int e=0; e<2; e++)
        {
            new_edgecost+=new_connections[e].e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
            if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
            {
                particle_pool.delete_obj(p_point);
		return false;
            }
        }

        T old_edgecost=0;
        /// computing  old edge costs
        old_edgecost=old_connection->e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
//         sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),(old_connection->e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange,true,true)));

	
	class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	class Collision<T,Dim>::Candidates candA ( query_bufferA );

	class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
	class Collision<T,Dim>::Candidates candB ( query_bufferB );	
	
	class Connection<T,Dim> old_connection_back;
	class Connection<T,Dim> * new_connection_back[2];
	old_connection_back=*old_connection;	
	
	
	int mode=1; // only special collision costs

	//############################
	// before adding edges
	//############################

	
	
	class Points<T,Dim>  & ptA=*(old_connection_back.pointA->point);
	class Points<T,Dim>  & ptB=*(old_connection_back.pointB->point);
	
	T max_pt_rad=std::max(maxscale,point.predict_thickness_from_scale(maxscale))+0.01;
	T ptA_collision_sphere=std::max(ptA.get_scale(),ptA.get_thickness())+max_pt_rad;
	T ptB_collision_sphere=std::max(ptB.get_scale(),ptB.get_thickness())+max_pt_rad;
	T ptI_collision_sphere=std::max(point.get_scale(),point.get_thickness())+max_pt_rad;
	
	if ( collision_fun_p.is_soft() ) {
	    T tmp;
	    sta_assert_error ( !collision_fun_p.colliding (
				    tmp,
				    tree,
				    *(old_connection_back.pointA->point),
				    ptA_collision_sphere,
				    temp,
				    &candA,true,
				    -9,mode  // collect colliding pts
				) );
	    particle_interaction_cost_old+=tmp;

	    sta_assert_error ( !collision_fun_p.colliding (
				    tmp,
				    tree,
				    *(old_connection_back.pointB->point),
				    ptB_collision_sphere,
				    temp,
				    &candB,true,
				    -9,mode  // collect colliding pts
				) );
	    particle_interaction_cost_old+=tmp;
	}
	
	

	connections.remove_connection(old_connection);
	
	//connected via bifurcation?
	if (Connections<T,Dim>::connection_exists(old_connection_back))
	{
	    particle_pool.delete_obj(p_point);
	    connections.add_connection(old_connection_back);
	    return false;
	}
	
	new_connection_back[0]=connections.add_connection(new_connections[0]);
	new_connection_back[1]=connections.add_connection(new_connections[1]);

	
	
	 //NOTE point is not in tree now, so only visible in his has_own
            // collision check
	
	 if ( collision_fun_p.is_soft() ) {
                T tmp;
                if ( collision_fun_p.colliding (
                            tmp,
                            tree,
                            point,
                            ptI_collision_sphere,
                            temp ) ) {
		  
		    connections.remove_connection(new_connection_back[0]);
		    connections.remove_connection(new_connection_back[1]);
		    particle_pool.delete_obj(p_point);
		    connections.add_connection(old_connection_back);
                    return false;
                }
                particle_interaction_cost_new+=tmp;

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                        *(old_connection_back.pointA->point),
                                       ptA_collision_sphere,
                                       temp,
                                       &candA,false,
                                       -10,mode  // do not collect
                                   ) );
                particle_interaction_cost_new+=tmp;

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                        *(old_connection_back.pointB->point),
                                       ptB_collision_sphere,
                                       temp,
                                       &candB,false,
                                       -10,mode  // do not collect
                                   ) );
                particle_interaction_cost_new+=tmp;
            }

	
	if (!collision_fun_p.is_soft())  
	{
	    T particle_interaction_cost_new=0;
	    if (collision_fun_p.colliding(particle_interaction_cost_new, tree,point,collision_search_rad,temp))
	    {
		connections.remove_connection(new_connection_back[0]);
		connections.remove_connection(new_connection_back[1]);
		particle_pool.delete_obj(p_point);
		connections.add_connection(old_connection_back);
		return false;
	    }
	}
	
	
	
	
	
        /// Energy Data term
        T E=(newenergy_data)/temp;
	
	

	
// 	std::size_t id_segment=static_cast<std::size_t>(CONNECTION_STATE::CONNECTION_STATE_SEGMENT);
//         /// Point Costs
//         T pointcost=particle_connection_state_cost_scale[id_segment]*point.compute_cost()+(particle_connection_state_cost_offset[id_segment]);////+PointScaleCost*point.compute_cost_scale(); // 0-> 2 connections
	
	T pointcost=point.compute_cost3(temp);
	
        E+=(pointcost)/temp;


        /// Edge Costs (+new - old)
        E+=(new_edgecost-old_edgecost)/temp;
	
	
	if (collision_fun_p.is_soft())   
	{
	E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	}
      

        /// Energy
        T R=mhs_fast_math<T>::mexp(E);



        /// Proposal probability
        R*=2*(T)(connections.pool->get_numpts());
        R/=  connection_probability
             *(T)(tree.get_numpts()+1)
             *(single_scale ? 1 : (maxscale-minscale))
             +std::numeric_limits<T>::epsilon();//*numvoxel;

	  if (consider_birth_death_ratio)
	  {
		if (use_saliency_map)
		{
		    R*=(lambda)*this->call_w_undo;
		    R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*(nvoxel*point.saliency)*this->call_w_do;//*cpoint.saliency;
		} else
		{
		    R*=(lambda)*this->call_w_undo;
		    R/=std::numeric_limits<T>::epsilon()+(tree.get_numpts()+1)*this->call_w_do;//*cpoint.saliency;
		}
	  }		
	  
	  



        if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
        {
            sta_assert_error(!p_point->has_owner());
	    
	    
	    if (!tree.insert(*p_point))
            {
		connections.remove_connection(new_connection_back[0]);
		connections.remove_connection(new_connection_back[1]);	      
		particle_pool.delete_obj(p_point);
		connections.add_connection(old_connection_back);
		return false;
            }
            sta_assert_error(p_point->has_owner());

	    
	    
// #ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
// 	    sta_assert_error(!Constraints::constraint_hasloop((*epoints[0]->point),options.constraint_loop_depth));
// 	    sta_assert_error(!Constraints::constraint_hasloop((*epoints[1]->point),options.constraint_loop_depth));
// #endif


// #ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK	    
// 	    sta_assert_error(!Constraints::constraint_hasloop((*p_point),options.constraint_loop_depth));
// 	    sta_assert_error(!Constraints::constraint_hasloop((*epoints[0]->point),options.constraint_loop_depth));
// 	    sta_assert_error(!Constraints::constraint_hasloop((*epoints[1]->point),options.constraint_loop_depth));
// #endif
	    
	    
            this->tracker->update_energy(E+particle_energy_change_costs,static_cast<int>(get_type()));
            this->proposal_accepted++;
	    
	    
	    #ifdef  D_USE_GUI
// 	      (GuiPoints<T>* (p_point))->touch(get_type());
// 	      (GuiPoints<T>* (epoints[0]->point))->touch(get_type());
// 	      (GuiPoints<T>* (epoints[1]->point))->touch(get_type());
	      p_point->touch(get_type());
	      epoints[0]->point->touch(get_type());
	      epoints[1]->point->touch(get_type());
	    
	    #endif
	    

            /*
              if (return_statistic)
            {
                std::size_t center=(std::floor(point.position[0])*shape[1]
            +std::floor(point.position[1]))*shape[2]
            +std::floor(point.position[2]);
              static_data[center*2*numproposals+17]+=1;
            }
            */
	    
	    p_point->_path_id=epoints[0]->point->_path_id;
	    
	    this->tracker->update_volume(p_point->get_scale());
	    
            return true;
        }

	connections.remove_connection(new_connection_back[0]);
	connections.remove_connection(new_connection_back[1]);	      
	particle_pool.delete_obj(p_point);
	connections.add_connection(old_connection_back);



        return false;
    }
};

template<typename TData,typename T,int Dim>
class CInsertDeath : public CProposal<TData,T,Dim>
{
protected:
    T lambda;
        T particle_energy_change_costs;
      const static int max_stat=10;
    int statistic[max_stat];    
    
    bool temp_dependent;
	 bool use_saliency_map;
	  bool consider_birth_death_ratio;  
	    bool sample_scale_temp_dependent;
//     std::size_t stat[4];
public:
      T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
       return CProposal<TData,T,Dim>::dynamic_weight_segment(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
    CInsertDeath(): CProposal<TData,T,Dim>()
    {
        lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
	
	temp_dependent=true;
	use_saliency_map=false;
	 consider_birth_death_ratio=false;
// 	stat[0]=stat[1]=stat[2]=stat[3]=0;
    }
    
    ~CInsertDeath()
    {
//         printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("(%d)%d ",i,statistic[i]);
// 	printf("\n");
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	    
	   if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];
	   
	   if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];
	   
	   	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
    }

    // saliency map
    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;
    }

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);
	
	

        this->proposal_called++;

        OctPoints<T,Dim> *  cpoint;

        ///randomly choose a point
        cpoint=tree.get_rand_point();

        if (cpoint==NULL)
        {
            return false;
        }

	int stat_count=0;
statistic[stat_count++]++;	

        Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	
	if (point.isfreezed())
            {
		return false;
	    }

        /// check if point has exactly one connection / side
        if ((point.endpoint_connections[0]!=1)||(point.endpoint_connections[1]!=1))
            return false;
	
	//only segments
	if (point.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
		 return false;	
	
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
        
	//computing point weight before removing the edges (which would change the costs)
	T pointcost=point.compute_cost3(temp);
	
        /// the two connected endpoints of point
        class Points< T, Dim >::CEndpoint * startpoints[2];
        startpoints[0]=point.endpoints[0][0];
	startpoints[1]=point.endpoints[1][0];
	
	
	
	
	
        /// the two connected endpoints connecting to point
        class Points< T, Dim >::CEndpoint * epoints[2];
	epoints[0]=startpoints[0]->connected;
	epoints[1]=startpoints[1]->connected;
	
	
	  if (sample_scale_temp_dependent && ((temp>epoints[0]->point->get_scale() || temp>epoints[1]->point->get_scale() || temp>point.get_scale())))
	  {
		return false; 
	  }
	
	
	
	
	class Connection<T,Dim>   new_connections;
	class Connection<T,Dim> * old_connections[2];
	
        old_connections[0]=startpoints[0]->connection;
        old_connections[1]=startpoints[1]->connection;
	
	bool protected_topologyA=(old_connections[0]->is_protected_topology());
	bool protected_topologyB=(old_connections[1]->is_protected_topology());
	
	
	if (protected_topologyA!=protected_topologyB)
	  return false;
	
	new_connections.freeze_topology=protected_topologyA && protected_topologyB;
	
	
	
	// cancel if points are tooo close
	{
	    T min_thickness;
	    if (sample_scale_temp_dependent)
	    {
		min_thickness=Points<T,Dim>::predict_thickness_from_scale ( std::max(minscale,temp));
	    }else
	    {
		min_thickness=Points<T,Dim>::predict_thickness_from_scale (minscale);
	    }
	    
	    if (!( min_thickness*min_thickness<(*epoints[0]->position-*epoints[1]->position).norm2()))
	    {
	      return false;
	    }
	}
	
	
        ///TODO I forgot if I accepte two directly connected bifurcations (via ordinary edges)
	#ifdef _BIF_CD_REQUIRES_2_ALL_CONNECT_ 
	/// I exclude it for the moment: at least one remaining point must not be a bif-point
        if ((epoints[0]->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)&&
	  (epoints[1]->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	)return false; 
	#endif

statistic[stat_count++]++;	        
	
        sta_assert_error(startpoints[1]->point==&point);
	sta_assert_error(startpoints[0]->point==&point);
	sta_assert_error(startpoints[1]->point==startpoints[0]->point);
	sta_assert_error(startpoints[1]->side!=startpoints[0]->side);
	sta_assert_error(startpoints[1]->connected!=NULL);
	sta_assert_error(startpoints[0]->connected!=NULL);
	
	sta_assert_error(startpoints[1]->connected->point!=&point);
	sta_assert_error(startpoints[0]->connected->point!=&point);
	
	sta_assert_error(startpoints[1]->connected->point!=startpoints[0]->connected->point);
	sta_assert_error(epoints[0]->point!=epoints[1]->point);
	
        int choice=1;// reference
	sta_assert_error(startpoints[choice]->side==1);
	
        /// update endpoint pos (not necessary here??)
//         epoints[0]->point->update_endpoint(epoints[0]->side);
//         epoints[1]->point->update_endpoint(epoints[1]->side);
	
	T lA=epoints[0]->point->get_thickness();
	T lB=epoints[1]->point->get_thickness();
	T wa=lB/(lA+lB);
	T wb=lA/(lA+lB);


        /// optimal position
        Vector<T,Dim> optimal_pos=(*epoints[0]->position)*wa+(*epoints[1]->position)*wb;
        /// optimal direction
        Vector<T,Dim> optimal_direction=epoints[choice]->point->get_direction()*Points<T,Dim>::side_sign[epoints[choice]->side];
        /// optimal scale
        T optimal_scale=((epoints[0]->point->get_scale())+(epoints[1]->point->get_scale()))/2;



        
	 T temp_fact_pos=1;
	if (temp_dependent)
	  temp_fact_pos=proposal_temp_trafo(temp,optimal_scale,T(0.5));
	  //temp_fact_pos=std::sqrt(temp);
	
        T temp_fact_scale=temp_fact_pos+1;
        T & temp_fact_rot=temp_fact_pos;

        //probability for point parameters
        T connection_probability=1;

        T agility=temp_fact_pos*std::sqrt((*(epoints[0]->position)-*(epoints[1]->position)).norm2())/4;

        Vector<T,3> displacement;
        displacement=(point.get_position()-optimal_pos);

        connection_probability*=normal_dist<T,3>(displacement.v,agility);

        connection_probability*=normal_dist_sphere(optimal_direction,point.get_direction(),temp_fact_rot);

      
	if (!single_scale)
	{
	    T scale_lower;
	    T scale_upper;
	    if (sample_scale_temp_dependent)
	    {
	      scale_lower=std::max(std::max(minscale,temp),optimal_scale/temp_fact_scale);
	      scale_lower=std::max(scale_lower,std::min(epoints[0]->point->get_scale(),epoints[1]->point->get_scale()));
	      scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
	      sta_assert_debug0(scale_lower<scale_upper);
	    }else
	    {
	      /// valid scale interval
	      scale_lower=std::max(minscale,optimal_scale/temp_fact_scale);
	      scale_lower=std::max(scale_lower,std::min(epoints[0]->point->get_scale(),epoints[1]->point->get_scale()));
	      scale_upper=std::min(maxscale,optimal_scale*temp_fact_scale); 
	    }
	    


	    if ((point.get_scale()<scale_lower)||(point.get_scale()>scale_upper))
	    {
		return false;
	    }

    statistic[stat_count++]++;	        
	    
	    connection_probability*=1.0/(scale_upper-scale_lower+std::numeric_limits<T>::epsilon());
	}

        /*
        if (return_statistic)
        {
            std::size_t center=(std::floor(point.position[0])*shape[1]
        +std::floor(point.position[1]))*shape[2]
        +std::floor(point.position[2]);
          static_data[center*2*numproposals+18]+=1;
        }
        */

	
	
	
        
        new_connections.pointA=epoints[1];
        new_connections.pointB=epoints[0];
	
	

        // computing new edge cost
        T new_edgecost;
        typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
        new_edgecost=new_connections.e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
        if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
        {
            return false;
        }

statistic[stat_count++]++;	

        T old_edgecost;        
        old_edgecost=old_connections[0]->e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
        sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
        old_edgecost+=old_connections[1]->e_cost(edgecost_fun,options.connection_bonus_L,options.bifurcation_bonus_L,inrange);
        sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));

	

// 	 T particle_interaction_cost_old=0;
// 	if (collision_fun_p.is_soft())    
// 	{
// 	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp));
// 	  
// 	}
	
	
	class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
        class Collision<T,Dim>::Candidates candA ( query_bufferA );

        class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
        class Collision<T,Dim>::Candidates candB ( query_bufferB );
        T particle_interaction_cost_old=0;
        T particle_interaction_cost_new=0;

        int mode=1; // only special collision costs


        //############################
        //NOTE before removing edges
        //############################
	

        if ( collision_fun_p.is_soft() ) {

            T tmp;
            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   point,
                                   collision_search_rad,
                                   temp ) );

            particle_interaction_cost_old+=tmp;

	    //NOTE we hide the bridge point from the tree
	    
	    
            std::size_t  npts_old=tree.get_numpts();
            point.unregister_from_grid ();
            std::size_t  npts_new=tree.get_numpts();
            sta_assert_debug0 ( npts_new+1==npts_old );


            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   *(epoints[0]->point),
                                   collision_search_rad,
                                   temp,
                                   &candA,true,
                                   -9,mode  // collect colliding pts
                               ) );
            particle_interaction_cost_old+=tmp;

            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   *(epoints[1]->point),
                                   collision_search_rad,
                                   temp,
                                   &candB,true,
                                   -9,mode  // collect colliding pts
                               ) );
            particle_interaction_cost_old+=tmp;
        }
        
        
        
	Connection<T,Dim>   *p_connection_new=NULL;	
	Connection<T,Dim>    backup_old_connection[2];
	backup_old_connection[0]=*(old_connections[0]);
	backup_old_connection[1]=*(old_connections[1]);
	
	sta_assert_debug0(startpoints[0]!=startpoints[1]);
	sta_assert_debug0(old_connections[0]->pointA!=old_connections[0]->pointB);
	sta_assert_debug0(old_connections[1]->pointA!=old_connections[1]->pointB);
	
	
	//moved from bottom to here
	sta_assert_error(old_connections[0]!=NULL);
	sta_assert_error(old_connections[0]!=old_connections[1]);
      
	
	connections.remove_connection(old_connections[0]);
	connections.remove_connection(old_connections[1]);
	
	
	if (Connections<T,Dim>::connection_exists(*(new_connections.pointA->point),*(new_connections.pointB->point)))
	{
	     if ( collision_fun_p.is_soft() ) {
		sta_assert_error ( tree.insert ( point) );
	    }
	    connections.add_connection(backup_old_connection[0]);
	    connections.add_connection(backup_old_connection[1]);
	    return false;
	}
	
	try {
	p_connection_new=connections.add_connection(new_connections);
	} catch (mhs::STAError error)
	{
	    error<<"\n insert death after removing 2 edges and trying adding new one";
	    throw error;
	}
	sta_assert_error(p_connection_new!=NULL);  
	
	
	 //############################
        //NOTE after removing edges
        //############################
        
        
	  if ( collision_fun_p.is_soft() ) {
            T tmp;
            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   *(epoints[0]->point),
                                   collision_search_rad,
                                   temp,
                                   &candA,false,
                                   -10,mode  // do not collect
                               ) );
            particle_interaction_cost_new+=tmp;

            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   *(epoints[1]->point),
                                   collision_search_rad,
                                   temp,
                                   &candB,false,
                                   -10,mode  // do not collect
                               ) );
            particle_interaction_cost_new+=tmp;
        }
        
	
	
        // computing new edge cost
        T E=(new_edgecost-old_edgecost)/temp;

        // computing data Derm
        T newenergy=point.point_cost;
        E-=(newenergy)/temp;

	if (collision_fun_p.is_soft())   
	{
	  E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	}
	
        // computing point cost
//         T pointcost=particle_connection_state_cost_scale[2]*point.compute_cost()+(particle_connection_state_cost_offset[2]);////+PointScaleCost*point.compute_cost_scale(); //2 -> 0 connection
        
        E-=pointcost/temp;

	
	

        T R=mhs_fast_math<T>::mexp(E);

        R*=connection_probability*
           (T)(tree.get_numpts())*
           (single_scale ? 1 : (maxscale-minscale));

        R/=2*(T)(connections.pool->get_numpts()+1)
           +std::numeric_limits<T>::epsilon();
	   
	   
	      if (consider_birth_death_ratio)
		{
		  if (use_saliency_map)
		  {
		      R*=(tree.get_numpts())*(nvoxel*point.saliency)*this->call_w_undo;
		      R/=std::numeric_limits<T>::epsilon()+(lambda)*this->call_w_do;
		  } else
		  {
		      R*=(tree.get_numpts())*this->call_w_undo;
		      R/=(lambda)*this->call_w_do;
		  }
		}	   
	   

        if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
        {
	  
#ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK	  
	  sta_assert_error(!Constraints::constraint_hasloop((*epoints[0]->point),options.constraint_loop_depth));
	  sta_assert_error(!Constraints::constraint_hasloop((*epoints[1]->point),options.constraint_loop_depth));
#endif	  
	  

	  
	 
	  
statistic[stat_count++]++;	

	  if (Constraints::constraint_hasloop_follow((*epoints[1]->point),p_connection_new,options.constraint_loop_depth))
	  {
// 	    printf("InsetDeath uiuiui\n");
	    if ( collision_fun_p.is_soft() ) {
		sta_assert_error ( tree.insert ( point) );
	    }	    
	    connections.remove_connection(p_connection_new);
	    connections.add_connection(backup_old_connection[0]);
	    connections.add_connection(backup_old_connection[1]);
	    return false;
	  }
	  
statistic[stat_count++]++;		  

            this->tracker->update_energy(E-particle_energy_change_costs,static_cast<int>(get_type()));
            this->proposal_accepted++;

#ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
	  sta_assert_error(!Constraints::constraint_hasloop((*epoints[0]->point),options.constraint_loop_depth));
	  sta_assert_error(!Constraints::constraint_hasloop((*epoints[1]->point),options.constraint_loop_depth));
#endif


//             sta_assert_error(old_connections[0]!=NULL);
//             sta_assert_error(old_connections[0]!=old_connections[1]);
    
	    	Points< T, Dim > * p_point=&point;
		this->tracker->update_volume(-p_point->get_scale());
            particle_pool.delete_obj(p_point);
	    sta_assert_debug0(p_point==NULL);
	    
	    
	    #ifdef  D_USE_GUI
// 	      (GuiPoints<T>* (epoints[0]->point))->touch(get_type());
// 	      (GuiPoints<T>* (epoints[1]->point))->touch(get_type());
	      epoints[0]->point->touch(get_type());
	      epoints[1]->point->touch(get_type());
	    #endif

            return true;
        }

        
          if ( collision_fun_p.is_soft() ) {
	    sta_assert_error ( tree.insert ( point) );
	}
	connections.remove_connection(p_connection_new);
	connections.add_connection(backup_old_connection[0]);
	connections.add_connection(backup_old_connection[1]);


        return false;

    }
};


#endif


