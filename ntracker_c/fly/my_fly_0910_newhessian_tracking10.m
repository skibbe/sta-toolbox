%%
% cd ~/data/Neurons/Neocortical' Layer 1 Axons'/Subset' 1'/Image' Stacks'/
% Img=zeros([512,512,60]);for a=1:16;s='';if (a<10) s='0';end; I=myreadtif([s,num2str(a),'.tif']);Img(:,:,a)=I{1};end;
% FeatureData=init_mhs3DHessian_new(single(Img),'scale_range',[1.15,3],'nscales',6,'poldeg',4,'crop',[-10,-10,-10],'expand_hessian',false);
%%
FeatureData=init_mhs3DHessian_new(single(Img2),'preprocess_voting',2.5,'element_size',[1,1,1],'scale_range',[1.15,6],'nscales_pol',6,'nscales',6,'poldeg',4,'crop',[5,5,5],'expand_hessian',false,'epsilon',0.001);
%     crop=[6,6,6];
%     nscales=6;
%     scale_range=[1.15,8];    
%     Scales_fac=scale_range(2)-scale_range(1);
%      Scales=scale_range(1):Scales_fac/(nscales-1):scale_range(2);
%     shape=size(Img);
%     [dX dY dZ] = ndgrid(0:(shape(1)-1),0:(shape(2)-1),0:(shape(3)-1));
%     %FeatureData=init_mhs_Simple(single(Img.*dX./max(dX(:))+0.1*randn(size(Img))),'crop',crop,'Scales',Scales,'poldeg',4);
%     FeatureData=init_mhs_Simple(single(Img),'crop',crop,'Scales',Scales,'poldeg',4);
%     [FeatureData.texture3D,FeatureData.light_texture3D]=mhs_create3DtextureNPOT(FeatureData.img,2000);
%%
 

   proposals={...
    'BIRTH',1,...    
    'DEATH',1,...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',1,...    
    'MOVE',1,...
    'SCALE',1,...    
    'CONNECT',1,...
    'B_BIRTH',0.01,...
    'B_DEATH',0.01,...
    'B_CHANGE',0.01,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0.01,...
    'B_BIRTHL',0.01,...
    'B_DEATHL',0.01,...
    'B_BIRTHX',1,...
    'B_DEATHX',1,...
    };


   proposals={...
    'BIRTH',5,...    
    'DEATH',5,...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',5,...    
    'MOVE',5,...
    'SCALE',5,...    
    'CONNECT',1,...
    'B_BIRTH',0.01,...
    'B_DEATH',0.01,...
    'B_CHANGE',0.01,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0.01,...
    'B_BIRTHL',0.01,...
    'B_DEATHL',0.01,...
    'B_BIRTHX',1,...
    'B_DEATHX',1,...
    };


    proposals={...
    'BIRTH',5,...    
    'DEATH',5,...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',5,...    
    'MOVE',5,...
    'SCALE',5,...    
    'CONNECT',1,...
    'B_BIRTH',0.01,...
    'B_DEATH',0.01,...
    'B_CHANGE',0.01,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0.01,...
    'B_BIRTHL',0.01,...
    'B_DEATHL',0.01,...
    'B_BIRTHX',1,...
    'B_DEATHX',1,...
    };


% proposals={...
%     'BIRTH',1,...    
%     'DEATH',1,...
%     'C_BIRTH',0,...    
%     'C_DEATH',0,...
%     'I_BIRTH',0,...    
%     'I_DEATH',0,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',0,...
%     'B_BIRTH',0,...
%     'B_DEATH',0,...
%     'B_CHANGE',0,...
%     'B_TBIRTH',0,...
%     'B_TDEATH',0,...
%     'B_BIRTHS',0,...
%     'B_DEATHS',0,...
%     'B_RECONN',0,...
%     'B_BIRTHL',0,...
%     'B_DEATHL',0,...
%     'B_BIRTHX',0,...
%     'B_DEATHX',0,...
%     };



manual=false;
if manual
   proposals={...
    'BIRTH',0,...    
    'DEATH',0,...
    'C_BIRTH',1,...    
    'C_DEATH',1,...
    'I_BIRTH',1,...    
    'I_DEATH',1,...
    'ROTATE',5,...    
    'MOVE',5,...
    'SCALE',5,...    
    'CONNECT',1,...
    'B_BIRTH',0.01,...
    'B_DEATH',0.01,...
    'B_CHANGE',0.01,...
    'B_TBIRTH',1,...
    'B_TDEATH',1,...
    'B_BIRTHS',1,...
    'B_DEATHS',1,...
    'B_RECONN',0.01,...
    'B_BIRTHL',0.01,...
    'B_DEATHL',0.01,...
    'B_BIRTHX',1,...
    'B_DEATHX',1,...
    };
end;



%  proposals={...
%     'BIRTH',1,...    
%     'DEATH',1,...
%     'C_BIRTH',1,...    
%     'C_DEATH',1,...
%     'I_BIRTH',1,...    
%     'I_DEATH',1,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',1,...
%     'B_BIRTH',1,...
%     'B_DEATH',1,...
%     'B_CHANGE',1,...
%     'B_TBIRTH',1,...
%     'B_TDEATH',1,...
%     'B_BIRTHS',1,...
%     'B_DEATHS',1,...
%     'B_RECONN',1,...
%     'B_BIRTHL',1,...
%     'B_DEATHL',1,...
%     'B_BIRTHX',1,...
%     'B_DEATHX',1,...
%     };



% 
%    proposals={...
%     'BIRTH',5,...    
%     'DEATH',5,...
%     'C_BIRTH',1,...    
%     'C_DEATH',1,...
%     'I_BIRTH',1,...    
%     'I_DEATH',1,...
%     'ROTATE',5,...    
%     'MOVE',5,...
%     'SCALE',5,...    
%     'CONNECT',1,...
%     'B_BIRTH',1,...
%     'B_DEATH',1,...
%     'B_CHANGE',1,...
%     'B_TBIRTH',1,...
%     'B_TDEATH',1,...
%     'B_BIRTHS',1,...
%     'B_DEATHS',1,...
%     'B_RECONN',1,...
%     'B_BIRTHL',1,...
%     'B_DEATHL',1,...
%     'B_BIRTHX',1,...
%     'B_DEATHX',1,...
%     };
% 

%  proposals={...
%     'BIRTH',1,...    
%     'DEATH',1,...
%     'C_BIRTH',1,...    
%     'C_DEATH',1,...
%     'I_BIRTH',1,...    
%     'I_DEATH',1,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',1,...
%     'B_BIRTH',1,...
%     'B_DEATH',1,...
%     'B_CHANGE',0,...
%     'B_TBIRTH',0,...
%     'B_TDEATH',0,...
%     'B_BIRTHS',0,...
%     'B_DEATHS',0,...
%     'B_RECONN',0,...
%     'B_BIRTHL',0,...
%     'B_DEATHL',0,...
%     'B_BIRTHX',0,...
%     'B_DEATHX',0,...
%     };



%   proposals={...
%     'BIRTH',1,...    
%     'DEATH',1,...
%     'C_BIRTH',1,...    
%     'C_DEATH',1,...
%     'I_BIRTH',1,...    
%     'I_DEATH',1,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',1,...
%     'B_BIRTH',0,...
%     'B_DEATH',0,...
%     'B_CHANGE',0,...
%     'B_TBIRTH',0,...
%     'B_TDEATH',0,...
%     'B_BIRTHS',0,...
%     'B_DEATHS',0,...
%     'B_RECONN',0,...
%     'B_BIRTHL',0,...
%     'B_DEATHL',0,...
%     'B_BIRTHX',0,...
%     'B_DEATHX',0,...
%     };

%    proposals={...
%     'BIRTH',1,...    
%     'DEATH',1,...
%     'C_BIRTH',0,...    
%     'C_DEATH',0,...
%     'I_BIRTH',0,...    
%     'I_DEATH',0,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',0,...
%     'B_BIRTH',0,...
%     'B_DEATH',0,...
%     'B_CHANGE',0,...
%     'B_TBIRTH',0,...
%     'B_TDEATH',0,...
%     'B_BIRTHS',0,...
%     'B_DEATHS',0,...
%     'B_RECONN',0,...
%     'B_BIRTHL',0,...
%     'B_DEATHL',0,...
%     'B_BIRTHX',0,...
%     'B_DEATHX',0,...
%     };
   

%    proposals={...
%     'BIRTH',1,...    
%     'DEATH',1,...
%     'C_BIRTH',1,...    
%     'C_DEATH',1,...
%     'I_BIRTH',1,...    
%     'I_DEATH',1,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',1,...
%     'B_BIRTH',1,...
%     'B_DEATH',1,...
%     'B_CHANGE',1,...
%     'B_TBIRTH',1,...
%     'B_TDEATH',1,...
%     'B_BIRTHS',1,...
%     'B_DEATHS',1,...
%     'B_RECONN',1,...
%     'B_BIRTHL',1,...
%     'B_DEATHL',1,...
%     'B_BIRTHX',1,...
%     'B_DEATHX',0,...
%     };
% 
% 
%    proposals={...
%     'BIRTH',0,...    
%     'DEATH',0,...
%     'C_BIRTH',0,...    
%     'C_DEATH',0,...
%     'I_BIRTH',0,...    
%     'I_DEATH',0,...
%     'ROTATE',1,...    
%     'MOVE',1,...
%     'SCALE',1,...    
%     'CONNECT',0,...
%     'B_BIRTH',1,...
%     'B_DEATH',1,...
%     'B_CHANGE',1,...
%     'B_TBIRTH',1,...
%     'B_TDEATH',1,...
%     'B_BIRTHS',1,...
%     'B_DEATHS',1,...
%     'B_RECONN',1,...
%     'B_BIRTHL',1,...
%     'B_DEATHL',1,...
%     'B_BIRTHX',1,...
%     'B_DEATHX',1,...
%     };


     DataScaleGrad=50;
     particle_endpoint_offset=0;
     particle_point_weight_exponent=1;
     bifurcation_particle=false;
     opt_alpha=0.99995;
     
     L=0;
     
     particle_min_thickness=1.5/2;
     particle_thickness=-1/4;
    
    BifurcationHScale=-1;
     
     bifurcation_particle_hard=true;
     bifurcation_neighbors=false;
     
     connection_candidate_searchrad=40;
     
      ThicknessScale=2;
      AngleScale=deg2rad(90);
     BifurcationDScale=10;
     BifurcationScale=10;
     BifurcationArea=1;
     
      OrientationScale=10;
      ConnectionScale=10;     

      DataScaleH=-1;
     particle_connection_state_cost_scale=1;
     use_saliency_map=true;
   
    DataScale=20;
    DataScaleGradVessel=10;
    DataThreshold=-25;
    %BifurcationArea=1;
    %BifurcationScale=20;
    BifurcationCScale=1;
    
%       OrientationScale=-1;
%       ConnectionScale=10;       
    
     
     
     
    
    DataScale=1; 
    particle_connection_state_cost_scale=2;
    DataThreshold=-20;
    
    OrientationScale=50;
     BifurcationDScale=5;
     BifurcationScale=5;
     connection_candidate_searchrad=20;
     
     opt_temp=2.5;
      opt_alpha=0.995;
      
      
      %0413
      BifurcationDScale=10;
     BifurcationScale=10;
     BifurcationCScale=10;
     
     BifurcationScale=10;
     BifurcationCScale=5;
     BifurcationDScale=5;
     
      %0417
     use_saliency_map=true;
     
     DataScale=5; 
    particle_connection_state_cost_scale=2.5*DataScale;
    DataScaleGradVessel=10*DataScale; 
    DataScaleGrad=50*DataScale;
    
    DataThreshold=-20;
    
    
 
    opt_alpha=0.99;
     %0511
     
     
     %0518
      connection_candidate_searchrad=40;
     DataThreshold=-50;
     
     
       
      
      %DataScale=20; 
      %particle_connection_state_cost_scale=DataScale*[1.0,0.9,0.8,0.9,0.8]; 
      
      DataScale=20; 
      Template_Similarity=1;
      particle_connection_state_cost_scale=Template_Similarity*DataScale*[1.0,0.9,0.8,0.9,0.8]; 
      
      BifurcationPaneltyFact=0;
      
      DataScaleGradVessel=5;
      %DataScaleGradVessel=20;
      DataScaleGradSurface=5;
      
      
      DataThreshold=0;
      GlobalDataThreshold=0;
      
      
      temp_dependent=true;
      AngleScale=-1;
      
      
      DataScaleGrad=0;
      connection_candidate_searchrad=40;
        
     ThicknessScale=1;
     
     
     ConnectionScale=2;
     OrientationScale=2;
     
     BifurcationHScale=-1;
     BifurcationDScale=-1;
     BifurcationArea=5;
     BifurcationScale=5;
     BifurcationCScale=2;
     
     particle_connection_state_cost_offset=2;
     
     
     BifurcationHScale=-1;
     BifurcationDScale=-1;
     BifurcationArea=5;
     BifurcationScale=1;
     BifurcationCScale=5;
     
     BifurcationScale=5;
     BifurcationCScale=1;
     
     BifurcationHScale=-1;
     BifurcationDScale=-1;
     BifurcationArea=-1;
     BifurcationScale=1;
     BifurcationCScale=1;     
     
     
     lambda=numel(FeatureData.img(:));
     
     particle_min_thickness=0.5;
     particle_thickness=-1/4;     
     
    
     
     %particle_connection_state_cost_offset=0;
     %lambda=10;
     
%      BifurcationScale=1;
%      BifurcationHScale=0.5;
%      BifurcationArea=5;
%      BifurcationCScale=5;
%      BifurcationDScale=5;
     
     
%      BifurcationArea=1;
%      BifurcationScale=1;
%      BifurcationCScale=1;
%      BifurcationDScale=1;
%      BifurcationHScale=-1;
     
     
%      
%       BifurcationArea=-1;
%       BifurcationScale=1;
%       BifurcationCScale=-1;
%       BifurcationDScale=-1;
%       BifurcationHScale=-1;
     
     
if manual
     opt_temp=1;
     opt_alpha=0.99999;
else
     opt_temp=6;
     opt_alpha=0.9;
     %opt_temp=6;
     %opt_alpha=0.9999;
     opt_alpha=0.999;
end;     
     %opt_temp=DataScale*opt_temp;
     pL=0;
     bL=0;
     
     new_version=true;
     particle_point_weight_exponent=1;
     

     use_saliency_map=true;
     %use_saliency_map=false;
     Epsilon=0.001;
     particle_connection_state_cost_offset=1;
     
     ConnectionScale=2;
     OrientationScale=2;
     
     BifurcationHScale=-1;
     BifurcationArea=-1;
     BifurcationScale=1;
     BifurcationCScale=2;    
     BifurcationDScale=1;
     
     BifurcationScale=2;
     BifurcationCScale=1;    
     BifurcationDScale=1;
     
     ConnectionScale=1;
     OrientationScale=1;
     
     connection_candidate_searchrad=40;
     DataScaleGradVessel=10;
     DataScaleGradSurface=10;
     Epsilon=0.01;
     
     ConnectEpsilon=0.01;
     AreaEpsilon=0.001;
     
     no_collision_threshold=-2;%cos(deg2rad(85));
     %no_collision_threshold=cos(deg2rad(85));
     
     
      directional_weights=true;
     max_vessel_score=1;
     no_double_bifurcations=true;
     
     
%      ConnectionScale=1;
%      OrientationScale=1;
     %no_collision_threshold=cos(deg2rad(75));
     %Epsilon=0.001;
     %Epsilon=0.01;

     
     %particle_connection_state_cost_scale=10;
      
       %DataThreshold=-20;
      %DataScaleGrad=20;
      %DataScaleGradVessel=10;
      
      %DataScale=2; 
    %particle_connection_state_cost_scale=4;
     
%     particle_connection_state_cost_scale=1;
     %DataScale=10;
%     DataScaleGradVessel=10;
%     DataThreshold=-25;
    

        %opt_alpha=0.999;


        
    % opt_temp=1;
     
     %DataThreshold=100;
     %DataScale=3;
     %particle_connection_state_cost_scale=DataScale*3;
     
     %BifurcationCScale=1;
    %'lambda',lambda,...
        options={...
            'no_double_bifurcations',no_double_bifurcations,...
            'max_vessel_score',max_vessel_score,...
            'directional_weights',directional_weights,...
            'no_collision_threshold',no_collision_threshold,...
            'AreaEpsilon',AreaEpsilon,...
            'ConnectEpsilon',ConnectEpsilon,...
            'Epsilon',Epsilon,...
            'temp_dependent',temp_dependent,...
            'particle_collision_two_steps',0,...
            'opt_numiterations',1000000,...
            'opt_alpha',opt_alpha,...%0.99995,...
            'DataThreshold',GlobalDataThreshold,...%-25,...
            'DataScaleGrad',DataScaleGrad,...
            'DataScaleGradSurface',DataScaleGradSurface,...
            'DataScale',DataScale,...
            'DataScaleH',DataScaleH,...
            'new_version',new_version,...
            'DataScaleGradVessel',DataScaleGradVessel,...    
            'particle_endpoint_offset',particle_endpoint_offset,...
            'particle_connection_state_cost_scale',particle_connection_state_cost_scale,...
            'particle_connection_state_cost_offset',particle_connection_state_cost_offset*[1,1,1,1,1],...-pL*[0,0.25,1],
            'particle_thickness',particle_thickness,...-1,...-1/4,...
            'particle_min_thickness',particle_min_thickness,...
            'connection_candidate_searchrad',connection_candidate_searchrad,...%f15,...%8,...
            'connection_bonus_L',-L,...
            'bifurcation_bonus_L',-bL,...
            'ConnectionScale',ConnectionScale,...
            'bifurcation_neighbors',bifurcation_neighbors,...
            'AngleScale',AngleScale,...$deg2rad(60),...
            'ThicknessScale',ThicknessScale,...%,1,...
            'OrientationScale',OrientationScale,...%,1,...
            'BifurcationScale',BifurcationScale,...%,1,...
            'BifurcationHScale',BifurcationHScale,...%,1,...
            'BifurcationCScale',BifurcationCScale,...%,1,..
            'BifurcationArea',BifurcationArea,...%,1,...
            'BifurcationDScale',BifurcationDScale,...%,1,...
            'opt_temp',opt_temp,...
            'constraint_loop_depth',25,...
            'use_saliency_map',use_saliency_map,...
            'proposals',proposals...
            'scale_power',particle_point_weight_exponent,...
            'particle_point_weight_exponent',particle_point_weight_exponent,...    
            'bifurcation_particle_hard',bifurcation_particle_hard,...
            'BifurcationPaneltyFact',BifurcationPaneltyFact,...
            };


