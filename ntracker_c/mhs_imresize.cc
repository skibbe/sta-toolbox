#include "mex.h"

#include <unistd.h>
#include <complex>
#include <map>
#include "mhs_error.h"
#include "mhs_vector.h"


#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"
// #define _SUPPORT_MATLAB_ 

#include <complex>
#include <vector>
#include <string>
#include <ctime>
#include <list>
#include <sstream>
#include <string>
#include <limits>
#include <omp.h>

#define SUB2IND(X, Y, Z, shape)  (((Z)*(shape[1])+(Y)) *(shape[2])+(X)) 


  
// #include "sta_mex_helpfunc.h"
// #include "sta_omp_threads.h"

/*
 mex getLocalMaxC.cpp 
*/


template<typename D,typename T,typename TData>
inline bool data_interpolate (
    const Vector<T,3> & pos,
    D & value,
    const TData  *  img,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode=1)
{

    
switch (mode)
{
   case 0:
    {
	int Z=std::floor ( pos[0]+T(0.5) );
	int Y=std::floor ( pos[1]+T(0.5) );
	int X=std::floor ( pos[2]+T(0.5) );

	if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}
	value=img[SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx];
	
    }break;
  case 1:
    {
      
   	T iz=pos[0];
	T iy=pos[1];
	T ix=pos[2];
	
	int Z=std::floor ( iz );
	int Y=std::floor ( iy );
	int X=std::floor ( ix );

	if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}

	D wz= ( iz-Z );
	D wy= ( iy-Y );
	D wx= ( ix-X );
	D & g=value;
	g=0;
        g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
    }break;
}

    return true;
}


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  try
        {
  
	  Vector<std::size_t,3>  target_shape;
	  target_shape=std::size_t(0);
	  const mxArray * params=prhs[nrhs-1];
	  if ( mxIsCell ( params  )  )
	  {
	      if ( mhs::mex_hasParam ( params,"shape" ) !=-1 )
		      {
		      target_shape=mhs::mex_getParam<T> ( params,"shape",3 ) ;
		      }
	  }
	    
	    
	   mhs::dataArray<T>  img;
	   img=mhs::dataArray<T> ( prhs[0]);
	   
	  Vector<std::size_t,3>  shape;
	  for ( int i=0; i<3; i++ ) {
	      shape[i]=img.dim[i];
	      sta_assert_error(shape[i]>0);
	    }
	    
	  
	  std::swap ( shape[0],shape[2] );
	  //std::swap ( target_shape[0],target_shape[2] );
	    
	  Vector<T,3> scale;
	  scale=shape;
	  scale/=target_shape;  

	  
	  
	  
	  
	  
	  scale.print();
	  
	  mwSize ndims[3];
	  ndims[0]=target_shape[0];
	  ndims[1]=target_shape[1];
	  ndims[2]=target_shape[2];
	  
	  //std::swap ( target_shape[0],target_shape[2] );
	  
	  plhs[0] = mxCreateNumericArray ( 3,ndims,mhs::mex_getClassId<T>(),mxREAL );
	  T *result = ( T * ) mxGetData ( plhs[0]);
	  
	  std::size_t v=target_shape[0]*target_shape[1]*target_shape[2];
	  
	  
	  
 	  std::swap ( shape[0],shape[2] );
	  std::swap ( target_shape[0],target_shape[2] );
	  std::swap ( scale[0],scale[2] );
	  shape.print();
	  target_shape.print();
 	  
	  #pragma omp parallel for num_threads(omp_get_num_procs())
	  for (std::size_t t=0;t<v;t++)
	  {
	   
	    std::size_t vc=t;
	    Vector<T,3> pos_source;
 	    pos_source[0]=vc/(target_shape[2]*target_shape[1]);
 	    vc-=pos_source[0]*(target_shape[2]*target_shape[1]);
	    
 	    pos_source[1]=vc/(target_shape[2]);
	    
 	    pos_source[2]=vc-pos_source[1]*target_shape[2];
 	    
	    pos_source*=scale;
	    
// 	    if (t%1000==0)
// 	      pos_source.print();
	    
// 	    pos_source*=0.5;
	    
// 	    pos_source[0]=std::floor(pos_source[0]+0.5);
// 	    pos_source[1]=std::floor(pos_source[1]+0.5);
// 	    pos_source[2]=std::floor(pos_source[2]+0.5);
	    
	    
// 	    std::size_t p=((pos_source[0]*shape[1])+pos_source[1])*shape[2]+pos_source[2];
// 	    if ((!(p>0))||(!(p<shape[0]*shape[1]*shape[2])))
// 	      continue;
// 	    result[t]=img.data[p];
	    
	    
	    //std::swap(pos_source[0],pos_source[2]);
	    

	     data_interpolate<T,T,T> (
	      pos_source,
	      result[t],
	      img.data,
	      shape.v);
	       
	  

	  }

	  
	  
	  
	  
	  
	  
	
	    
	    
	}
     catch ( mhs::STAError & error )
        {
        printf ( "error cleaning up!!\n" );
        mexErrMsgTxt ( error.what() );
        }
    catch ( ... )
        {
        printf ( "error cleaning up!!\n" );
        mexErrMsgTxt ( "exeption ?" );
        }
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{    
 

    
    if ( nrhs<2 )
        mexErrMsgTxt ( "error: nrhs<2\n" );
    
   const mxArray * img=prhs[0];
    
    if ((img==NULL)||(mxGetClassID(img)==mxDOUBLE_CLASS))
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    else if (mxGetClassID(img)==mxSINGLE_CLASS)
    _mexFunction<float> ( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported data type\n");
}

