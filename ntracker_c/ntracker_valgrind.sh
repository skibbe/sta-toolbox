#!/bin/bash

# ~/matlab2015/bin/mex  -g -f ./engopts.sh ntracker_valgrind.cc -L./ntrack.mexa64 
#LD_LIBRARY_PATH=/home/skibbe-h/matlab2015/bin/glnxa64/   ~/matlab2015/bin/mex -g -v -f  /home/skibbe-h/.matlab/R2015a/mex_C++_glnxa64.xml ntracker_valgrind.cc -L./ntrack.mexa64 -L~/matlab2015/bin/glnxa64/ -L~/matlab2015/sys/os/glnxa64/ -L/home/skibbe-h/matlab2015/bin/glnxa64/  -llibeng -llibmat -llibmx
#LD_LIBRARY_PATH=/home/skibbe-h/matlab2015/bin/glnxa64/   ~/matlab2015/bin/mex -g -v  -f  /home/skibbe-h/.matlab/R2015a/mex_C++_glnxa64.xml ntracker_valgrind.cc  -L/home/skibbe-h/matlab2015/bin/glnxa64/  -leng -lmat -lmx -ldl
#LD_LIBRARY_PATH=~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/  ~/matlab2015/bin/mex -g -v -f  /home/skibbe-h/.matlab/R2015a/mex_C++_glnxa64.xml engdemo.c -L./ntrack.mexa64 -L~/matlab2015/bin/glnxa64/ -L~/matlab2015/sys/os/glnxa64/ 

#mex -g CXXFLAGS="  -std=c++11 -fPIC -march=native -fopenmp -g  -D_VALGIND_CONSOLE_"  CFLAGS="  -std=c++11 -g  -fPIC -march=native -fopenmp  -D_VALGIND_CONSOLE_"  mhs3D2.cc -l gsl -L/usr/lib/openblas-base/libopenblas.so  -l gomp
#mex -g CXXFLAGS=" -D_MCPU -std=c++11 -fPIC -march=native -fopenmp -g  -D_VALGIND_CONSOLE_"  CFLAGS=" -D_MCPU -std=c++11 -g  -fPIC -march=native -fopenmp  -D_VALGIND_CONSOLE_"  mhs3D2.cc -l gsl -L/usr/lib/openblas-base/libopenblas.so  -l gomp
#mex -g CXXFLAGS=" -DSECOND_EV_SYS -std=c++11 -O3 -g  -D_VALGIND_CONSOLE_ -fPIC"  CFLAGS=" -DSECOND_EV_SYS -std=c++11 -O3  -g  -fopenmp  -D_VALGIND_CONSOLE_ -fPIC"  mhs3D2.cc -l gomp
#mex -g CXXFLAGS=" -std=c++11 -fPIC -march=native  -g  -D_VALGIND_CONSOLE_"  CFLAGS=" -std=c++11 -g  -fPIC -march=native   -D_VALGIND_CONSOLE_"  mhs3D2.cc -l gsl -L/usr/lib/openblas-base/libopenblas.so  -l gomp

#mex -g CXXFLAGS=" --std=c++11  -g  -D_VALGIND_CONSOLE_ -fPIC"  CFLAGS=" --std=c++11   -g  -fopenmp  -D_VALGIND_CONSOLE_ -fPIC"  ntrack.cc -l gomp


# NOTE : gcc san tool
 #mex  -f ./engopts.sh -largeArrayDims CXXFLAGS=" -g -fsanitize=bounds" ntracker_valgrind.cc -L./ntrack.mexa64 -lmex -lubsan
 #LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/ ./ntracker_valgrind ./ntrack.mexa64 


 mex    -f ./engopts.sh -largeArrayDims  ntracker_valgrind.cc -L./ntrack.mexa64 -lmex


 # NOTE : get all shared libs
 #$ ldd ntrack.mexa64 | egrep -v 'linux-vdso|ld-linux-x86-64' | cut -f 3 -d ' '



export PATH=~/matlab2015/bin/:$PATH 
#LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/ valgrind --tool=callgrind ./ntracker_valgrind ./ntrack.mexa64

#LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/ valgrind --suppressions=valfrind_ignore.txt  --tool=callgrind --log-file=valgrind.log  ./ntracker_valgrind ./ntrack.mexa64
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/ valgrind  --suppressions=valgrind_matlab_supression_clean.txt --leak-check=full --show-reachable=yes --error-limit=no  -v --num-callers=20 --leak-resolution=high   --log-file=valgrind.log  ./ntracker_valgrind ./ntrack.mexa64


#  LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/ valgrind --suppressions=valfrind_ignore.txt  --leak-check=full  --show-reachable=yes --track-origins=yes --log-file=valgrind.log  ./ntracker_valgrind ./ntrack.mexa64
#LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/matlab2015/bin/glnxa64/:~/matlab2015/sys/os/glnxa64/ ./ntracker_valgrind ./ntrack.mexa64 


#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind --tool=callgrind ./ntracker_valgrind ./ntrack.mexa64


# PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind  --leak-check=full  --show-reachable=yes --track-origins=yes --log-file=valgrind.log  ./ntracker_valgrind ./ntrack.mexa64

#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind --tool=callgrind --dump-instr=yes --trace-symtab=yes --collect-jumps=yes --simulate-cache=yes  ./mhs_valgrind ./mhs3D2.mexa64
# PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind  --leak-check=full --gen-suppressions=yes --show-reachable=yes --track-origins=yes --log-file=memcheck.txt --suppressions=valgrind_ignorelist.txt ./mhs_valgrind ./mhs3D2.mexa64
#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind  --leak-check=full --show-reachable=yes --track-origins=yes --log-file=memcheck.txt --suppressions=valgrind_ignorelist.txt ./mhs_valgrind ./mhs3D2.mexa64

#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind --leak-check=full --show-reachable=yes --error-limit=no --gen-suppressions=all --log-file=minimalraw.log ./mhs_valgrind ./mhs3D2.mexa64
#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind --tool=callgrind ./mhs_valgrind ./mhs3D2.mexa64
#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind --leak-check=full --undef-value-errors=yes  --show-reachable=yes --main-stacksize=16388608 --log-file=minimalraw2.log  ./mhs_valgrind ./mhs3D2.mexa64
#PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/ valgrind --leak-check=full --track-origins=yes --show-reachable=yes  ./mhs_valgrind ./mhs3D2.mexa64 > valgrind_leakcheck.txt


#$PATH=$PATH:~/matlab/bin/ LD_LIBRARY_PATH=~/matlab/bin/glnxa64/:~/matlab/sys/os/glnxa64/  ./mhs_valgrind ./mhs3D2.mexa64