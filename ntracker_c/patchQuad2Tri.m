function p3=patchQuad2Tri(p4)

p3=p4;
p3.faces=[];
for a=1:size(p4.faces,1),
    p3.faces=[p3.faces;p4.faces(a,1:3);p4.faces(a,[3,4,1])];
end;