#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>
#include "EigDecomp3x3.h"


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"

#define EPSILON 0.00000000000001


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   
    
    
     const mxArray *SalTensor;
    SalTensor = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(SalTensor);
    const int *dims = mxGetDimensions(SalTensor);



if (numdim==4)
{
    mhs::dataArray<T>  directions;
    T * grad_weights=NULL;
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

	if (mhs::mex_hasParam(params,"dirs")!=-1)
	  directions=mhs::dataArray<T>(mhs::mex_getParamPtr(params,"dirs"));
	
    }
    for (int i=0;i<directions.dim.size();i++)
      printf("%d\n",directions.dim[i]);
    
    sta_assert(directions.data!=NULL);
    sta_assert(directions.dim.size()==2);
    sta_assert(directions.dim[1]==3);
    
    int shape[3];  
    shape[0] = dims[3];
    shape[1] = dims[2];
    shape[2] = dims[1];
    T *saltensor = (T*) mxGetData(SalTensor);
    if (dims[0]!=3)
      mexErrMsgTxt("error: first dim must be 6\n");
 
    
    int numdir=directions.dim[0];
    
    
    
    
    int ndims[4];
    ndims[0]=numdir;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    ndims[3]=dims[3];
    plhs[0] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
    T *ofield = (T*) mxGetData(plhs[0]);
    
    T *stensor=saltensor;

    
    
		std::size_t numv=shape[0]*shape[1]*shape[2];
	
		
	  
		
		  for (std::size_t idx= 0; idx < numv; idx++)    
		  {
		    
			T & x=  stensor[0]; //xx
			T & y=  stensor[1]; //yy
			T & z=  stensor[2]; //zz
			Vector<T,3> gradient(x,y,z);
			
			for (int i=0;i<numdir;i++)
			{
			  T & nx=directions.data[3*i];
			  T & ny=directions.data[3*i+1];
			  T & nz=directions.data[3*i+2];
			  Vector<T,3> ndir(nx,ny,nz);
			  ofield[i]=std::sqrt((gradient-(ndir*ndir.dot(gradient))).norm2());
			  
			}
			
			ofield+=numdir;
			stensor+=3;
		}
}
    
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}