#ifndef PROPOSAL_BIFURCATION_BD_BRIDGE_H
#define PROPOSAL_BIFURCATION_BD_BRIDGE_H

#include "proposals.h"
#include "edges.h"
#include "mhs_graphics.h"


// #define DEBUG__BIF_BRIDGE_CONNECT
#define DEBUG__BIF_BRIDGE_CONNECT_2


template<typename TData,typename T,int Dim>
class CProposal;
template<typename TData,typename T,int Dim>
class CBridgeBirth;

template<typename TData,typename T,int Dim>
class CProposalBifurcationBridgeDeath;




template<typename TData,typename T,int Dim>
class CProposalBifurcationBridgeBirth : public CProposal<TData,T,Dim>
{


protected:
    T Lprior;
    T ConnectEpsilon;
    T particle_energy_change_costs;

    T lambda;
    bool temp_dependent;
    bool use_saliency_map;
    bool consider_birth_death_ratio;
    bool sample_scale_temp_dependent;
public:


    T dynamic_weight (
        std::size_t num_edges,
        std::size_t num_particles,
        std::size_t num_connected_particles,
        std::size_t num_bifurcations,
        std::size_t num_terminals,
        std::size_t num_segments,
        T volume_img,
        T volume_particles
    ) {
        return CProposal<TData,T,Dim>::dynamic_weight_segment ( num_edges,
                num_particles,
                num_connected_particles,
                num_bifurcations,
                num_terminals,
                num_segments,
                volume_img,
                volume_particles );
    }


    CProposalBifurcationBridgeBirth() : CProposal<TData,T,Dim>() {
        Lprior=-10;
        ConnectEpsilon=0.0001;
	lambda=100;
        particle_energy_change_costs=update_energy_particle_costs ( lambda );

        
        temp_dependent=true;
        use_saliency_map=false;
        consider_birth_death_ratio=false;
        sample_scale_temp_dependent=false;

    }



    ~CProposalBifurcationBridgeBirth() {

    }

    std::string get_name() {
        return enum2string ( PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_BIRTH );
    };

    PROPOSAL_TYPES get_type() {
        return ( PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_BIRTH );
    }

    void set_params ( const mxArray * params=NULL ) {

        sta_assert_error ( this->tracker!=NULL );

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;

        if ( params==NULL ) {
            return;
        }

        if ( mhs::mex_hasParam ( params,"Lprior" ) !=-1 ) {
            Lprior=mhs::mex_getParam<T> ( params,"Lprior",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"ConnectEpsilon" ) !=-1 ) {
            ConnectEpsilon=mhs::mex_getParam<T> ( params,"ConnectEpsilon",1 ) [0];
        }




        if ( mhs::mex_hasParam ( params,"lambda" ) !=-1 ) {
            lambda=mhs::mex_getParam<T> ( params,"lambda",1 ) [0];
        }

        particle_energy_change_costs=update_energy_particle_costs ( lambda );

        if ( mhs::mex_hasParam ( params,"temp_dependent" ) !=-1 ) {
            temp_dependent=mhs::mex_getParam<bool> ( params,"temp_dependent",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }





    }

    void init ( const mxArray * feature_struct ) {};

    bool propose() {
        pool<class Points<T,Dim> > &	particle_pool		=* ( this->tracker->particle_pool );
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=* ( this->tracker->data_fun );
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=* ( this->tracker->tree );
        OctTreeNode<T,Dim> &		conn_tree		=* ( this->tracker->connecion_candidate_tree );
        OctTreeNode<T,Dim> &		bifurcation_tree	=* ( this->tracker->bifurcation_tree );
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=* ( this->tracker->edgecost_fun );
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
        Collision<T,Dim> &   collision_fun_p=* ( this->tracker->collision_fun );
        bool  single_scale		= ( this->tracker->single_scale );

        this->proposal_called++;



        if ( sample_scale_temp_dependent && ( ! ( temp<maxscale ) ) ) {
            return false;
        }

        sta_assert_error ( ! ( options.bifurcation_particle_hard )	);


        bool debug=false;

        /// no bifurcations?
        if ( connections.get_num_bifurcation_centers() <1 ) {
            return false;
        }


        try {

            /// randomly pic a bifurcation center node
            Points<T,Dim> &  bif_point=connections.get_rand_bifurcation_center_point();

            sta_assert_debug0 ( bif_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
	    
// 	    {
// 	      sta_assert_error(bif_point.get_num_connections()==3);
// 	      
// 	      
// 	       typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
// 	      T          old_edgecost=bif_point.e_cost (
//                              edgecost_fun,
//                              options.connection_bonus_L,
//                              options.bifurcation_bonus_L,
//                              inrange
//                          );
// 
//             
// 	      sta_assert_debug0 ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );
// 	    }
	    
	    

            class Points<T,Dim>::CEndpoint * bridges[2][2];

            int bridge_candidate=std::rand() %3;

            bridges[0][0]=bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][bridge_candidate]->connected;
            if ( !bridges[0][0]->point->is_segment() ) {
                return false;
            }


            bridges[0][1]=NULL;
            T proposed_prob=-1;


// #ifdef  D_USE_GUI
// #if  defined(DEBUG__BIF_BRIDGE_CONNECT) || defined(DEBUG__BIF_BRIDGE_CONNECT_2)
//             GuiPoints<T> * gps= ( ( GuiPoints<T> * ) bridges[0][0]->point ) ;
// #endif
// #endif
	    
#ifdef  D_USE_GUI
#if  defined(DEBUG__BIF_BRIDGE_CONNECT) 
            GuiPoints<T> * gps= ( ( GuiPoints<T> * ) bridges[0][0]->point ) ;
#endif
#endif
	    
	    


            // first bridge start-end in bridges[0]
            if ( ! CBridgeBirth<TData,T,Dim>::do_tracking3 (
                        *bridges[0][0],
                        conn_tree,
                        search_connection_point_center_candidate,
                        bridges[0][1],
                        proposed_prob,
                        false
#ifdef  D_USE_GUI
#ifdef  DEBUG__BIF_BRIDGE_CONNECT
                        , ( gps->is_selected )
#endif
#endif
                    ) ) {
                if ( debug ) {
                    printf ( "\n" );
                }
                return false;
            }


            // second bridge start-end in bridges[1]
            int j=0;
            for ( int i=0; i<3; i++ ) {
                if ( i!=bridge_candidate ) {
                    sta_assert_debug0 ( j<2 );
                    bridges[1][j++]=bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected;
                }
            }

            
#ifdef  D_USE_GUI
#if   defined(DEBUG__BIF_BRIDGE_CONNECT_2)
            GuiPoints<T> * gps= ( ( GuiPoints<T> * ) bridges[0][0]->point ) ;
	    if (!gps->is_selected) gps= ( ( GuiPoints<T> * ) bridges[0][1]->point ) ;
	    if (!gps->is_selected) gps= ( ( GuiPoints<T> * ) bridges[1][1]->point ) ;
	    if (!gps->is_selected) gps= ( ( GuiPoints<T> * ) bridges[1][0]->point ) ;
#endif
#endif            

            // prepare edges
            class Connection<T,Dim> new_connections[2];
            T max_edge_length=CEdgecost<T,Dim>::get_current_edge_length_limit();
            max_edge_length*=max_edge_length;

#ifdef  D_USE_GUI
#ifdef  DEBUG__BIF_BRIDGE_CONNECT
            //   GuiPoints<T> * gps= ( ( ( GuiPoints<T> * ) bridges[0][0]->point )->is_selected ) ? ( ( GuiPoints<T> * ) bridges[0][0]->point ) : ( ( GuiPoints<T> * )bridges[0][1]->point  );

            bool valid_a=false;
            if ( gps->is_selected ) {
                T a=bridges[0][0]->get_slot_direction().dot ( bridges[0][1]->get_slot_direction() );
                if ( a<-0.5 ) {
                    valid_a=true;
                    printf ( "angle valid %f\n",a );
                } else {
                    printf ( "invalid %f\n",a );
                }
            }

#endif
#endif





            for ( int b=0; b<2; b++ ) {
                new_connections[b].pointA=bridges[b][0]->point->getfreehub ( bridges[b][0]->side );
                new_connections[b].pointB=bridges[b][1]->point->getfreehub ( bridges[b][1]->side );
                new_connections[b].edge_type=EDGE_TYPES::EDGE_SEGMENT;

                T dist2= ( ( *new_connections[b].pointA->position ) - ( *new_connections[b].pointB->position ) ).norm2();
                if ( ! ( dist2<max_edge_length ) ) {
                    return false;
                }
            }

#ifdef DEBUG__BIF_BRIDGE_CONNECT
#ifdef  D_USE_GUI
            if ( gps->is_selected && valid_a ) {
                printf ( "dist ok\n" );
            }
#endif
#endif


            T undo_proposed_prob;
            if ( ! ( CProposalBifurcationBridgeDeath<TData,T,Dim>::do_tracking3 (
                         * ( bridges[0][0] ),
                         * ( bridges[0][1] ),
                         tree,
                         search_connection_point_center_candidate,
                         ( bridges[1][0] ),
                         ( bridges[1][1] ),
                         undo_proposed_prob,false ) ) ) {
                sta_assert_error ( false );
            }

            //printf("undo_proposed_prob: %f\n",undo_proposed_prob);



            typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;

            T old_edgecost=0;
            T new_edgecost=0;
#ifdef EDGECOST_DEBUG
            std::stringstream  debugstream;
#endif

            T edge_cost_debug[2];
            //compute new edge costs
            for ( int b=0; b<2; b++ ) {
                T tmp=new_connections[b].e_cost (
                          edgecost_fun,
                          options.connection_bonus_L,
                          options.bifurcation_bonus_L,
                          inrange,true,false
#ifdef EDGECOST_DEBUG
#ifdef  D_USE_GUI
#ifdef  DEBUG__BIF_BRIDGE_CONNECT
                          , ( gps->is_selected && valid_a ) ? ( &debugstream ) : NULL
#else
                          ,  NULL
#endif
#endif
#endif
                      );
                if ( inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) {
                    return false;
                }
                new_edgecost+=tmp;
                edge_cost_debug[b]=tmp;
            }

            //computing edge costs of the OLD configuration
            old_edgecost=bif_point.e_cost (
                             edgecost_fun,
                             options.connection_bonus_L,
                             options.bifurcation_bonus_L,
                             inrange
                         );

            T R= T ( connections.get_num_bifurcation_centers() ) / ( T ) ( connections.pool->get_numpts()-1 );

            sta_assert_error ( R>0 );


	    
	    if ( inrange!= ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) )
	    {
	      old_edgecost=bif_point.e_cost (
                             edgecost_fun,
                             options.connection_bonus_L,
                             options.bifurcation_bonus_L,
                             inrange,true,true
                         );
	      printf("inrange: %d\n",static_cast<int>(inrange));
	      sta_assert_debug0 ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );
	    }

            sta_assert_debug0 ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );

            class OctPoints<T,Dim> * query_buffer[4][query_buffer_size];
            class Collision<T,Dim>::Candidates candA ( query_buffer[0] );
            class Collision<T,Dim>::Candidates candB ( query_buffer[1] );
            class Collision<T,Dim>::Candidates candC ( query_buffer[2] );
            class Collision<T,Dim>::Candidates candD ( query_buffer[3] );

            class Collision<T,Dim>::Candidates * cand[4];
            cand[0]=&candA;
            cand[1]=&candB;
            cand[2]=&candC;
            cand[3]=&candD;




            T particle_interaction_cost_old=0;

            class Points<T,Dim> * old_points[4];
            old_points[0]=bridges[0][0]->point;
            old_points[1]=bridges[0][1]->point;
            old_points[2]=bridges[1][0]->point;
            old_points[3]=bridges[1][1]->point;



            int mode=1; // only special collision costs

            //############################
            // before adding edges
            //############################

            T point_cost_old=0;
            for ( int i=0; i<4; i++ ) {
                point_cost_old+=old_points[i]->compute_cost3 ( temp );
            }

            if ( collision_fun_p.is_soft() ) {
                T tmp;

                if ( options.bifurcation_particle_soft ) {
                    sta_assert_error ( !collision_fun_p.colliding ( tmp,
                                       tree,
                                       bif_point,
                                       collision_search_rad,
                                       temp ) );

                    particle_interaction_cost_old+=tmp;

                    sta_assert_error ( bif_point.has_owner ( voxgrid_default_id ) );
                    //NOTE we hide the bridge point from the tree
                    std::size_t  npts_old=tree.get_numpts();
                    bif_point.unregister_from_grid ( voxgrid_default_id );
                    std::size_t  npts_new=tree.get_numpts();
                    sta_assert_debug0 ( npts_new+1==npts_old );


                }



                for ( int i=0; i<4; i++ ) {
                    sta_assert_error ( !collision_fun_p.colliding (
                                           tmp,
                                           tree,
                                           *old_points[i],
                                           collision_search_rad,
                                           temp,
                                           cand[i],true,
                                           -9,mode  // collect colliding pts
                                       ) );
                    particle_interaction_cost_old+=tmp;
                }
            }



            class Connection<T,Dim> *  new_connection_pt[2];
            class Connection<T,Dim>    old_connection_backup[3];


            //deleting old edges
            for ( int i=0; i<3; i++ ) {
                old_connection_backup[i]=*bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connection;
            }

            for ( int i=0; i<3; i++ ) {
                class Connection< T, Dim > * delete_me=bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection;
                sta_assert_debug0 ( ( bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected ) !=NULL );
                connections.remove_connection ( delete_me );
                if ( i<2 ) {
                    sta_assert_debug0 ( ( bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected ) !=NULL );
                }
            }

            //adding new edges
            for ( int b=0; b<2; b++ ) {
                new_connection_pt[b]=connections.add_connection ( new_connections[b] );

            }

            //############################
            // after adding edges
            //############################

            T particle_interaction_cost_new=0;

            //NOTE bridge_pt is not in tree now, so only visible by himself
            // collision check

            T point_cost_new=0;
            for ( int i=0; i<4; i++ ) {
                point_cost_new+=old_points[i]->compute_cost3 ( temp );
            }

            if ( collision_fun_p.is_soft() ) {
                T tmp;


                for ( int i=0; i<4; i++ ) {
                    sta_assert_error ( !collision_fun_p.colliding (
                                           tmp,
                                           tree,
                                           *old_points[i],
                                           collision_search_rad,
                                           temp,
                                           cand[i],false,
                                           -10,mode  // collect colliding pts
                                       ) );
                    particle_interaction_cost_new+=tmp;
                }
            }
            /*
                        #ifdef  D_USE_GUI
            #ifdef  DEBUG__BIF_BRIDGE_CONNECT
                        if ( gps->is_selected ) {
            	     printf("costs\n");
            	    }
            #endif
            #endif  */


            {


                {
                    T oldenergy_data=bif_point.point_cost;

                    T  Eold= ( point_cost_old
                               +particle_interaction_cost_old
                               +old_edgecost
                               +oldenergy_data
                             ) ;
                    T  Enew= ( point_cost_new
                               +particle_interaction_cost_new
                               +new_edgecost
                             ) ;


                    T E= ( Enew-Eold ) /temp;

                    R*=mhs_fast_math<T>::mexp ( E );


                    R*=undo_proposed_prob/ ( proposed_prob+std::numeric_limits<T>::epsilon() );

#ifdef  D_USE_GUI
#ifdef  DEBUG__BIF_BRIDGE_CONNECT
                    if ( gps->is_selected && valid_a ) {
                        //if ( Enew<Eold )
                        {
                            printf ( "( %f %f  pc[%f %f] pi[%f %f] data[%f 0] ec[%f %f] dbg[%f %f]})\n",
                                     Eold,Enew,
                                     point_cost_old,point_cost_new,
                                     particle_interaction_cost_old,particle_interaction_cost_new,
                                     oldenergy_data,old_edgecost, new_edgecost,edge_cost_debug[0],edge_cost_debug[1] );

                            printf ( "%s\n",debugstream.str().c_str() );
//                     T f0= ( connections.get_num_terminals() +T ( 2 ) ) / ( tree.get_numpts() +T ( 1 ) );
//
                            printf ( "R:%f  [%f]\n",R,proposed_prob );
                            printf ( "E0 %f E1 %f\n",mhs_fast_math<T>::mexp ( E ),std::exp ( -E ) );

                        }
                    }
#endif
#endif



                    if ( R>=myrand ( 1 ) +std::numeric_limits<T>::epsilon() ) {

		      #ifdef  D_USE_GUI
		      #if   defined(DEBUG__BIF_BRIDGE_CONNECT_2)
				  if  (gps->is_selected)
				  {
				   printf("BIF BRIDGE BIRTH: EC (%f => %f)? PC (%f => %f)  BD (%f) IC(%f => %f) R (%f/%f)\n ",
					  old_edgecost,new_edgecost,
					  point_cost_old,point_cost_new,
					  oldenergy_data,
					  particle_interaction_cost_old,particle_interaction_cost_new,
					  undo_proposed_prob,proposed_prob
			    );
				  }
		      #endif
		      #endif
		      
		      
                        if ( debug ) {
                            printf ( "7" );
                        }
                        if ( Constraints::constraint_hasloop_follow ( ( *new_connections[0].pointA->point ),new_connection_pt[0],options.constraint_loop_depth ) ) {
                            goto cleanup_particle_and_edges;
                        }


                        Points<T,Dim>  & bridge_endpoint=*bridges[0][1]->point;


#ifndef BRIDGETBIRTHDEATH_SINGLE_PART
                        sta_assert_debug0 ( bridge_endpoint.is_segment() );
#endif
                        if ( ( bridge_endpoint.endpoint_connections[0]>0 ) && ( bridge_endpoint.endpoint_connections[1]>0 ) ) {
                            bridge_endpoint.unregister_from_grid ( voxgrid_conn_can_id );
                        }
                        if ( debug ) {
                            printf ( "8" );
                        }

                        Points< T, Dim > * p_point=& bif_point;
                        particle_pool.delete_obj ( p_point );


                        this->tracker->update_energy ( E+particle_energy_change_costs,static_cast<int> ( get_type() ) );
                        this->proposal_accepted++;


#ifdef  D_USE_GUI
                        for ( int i=0; i<4; i++ ) {
                            old_points[i]->touch ( get_type() );
                        }
#endif

                        if ( debug ) {
                            printf ( "accept\n" );
                        }

                        return true;
                    }
                }
            }


        cleanup_particle_and_edges:

            for ( int b=0; b<2; b++ ) {
                connections.remove_connection ( new_connection_pt[b] );
            }

            if ( options.bifurcation_particle_soft ) {
                sta_assert_error ( tree.insert ( bif_point ) );
            }

            for ( int i=0; i<3; i++ ) {
                connections.add_connection ( old_connection_backup[i] );
            }



            return false;


        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }





};









template<typename TData,typename T,int Dim>
class CProposalBifurcationBridgeDeath : public CProposal<TData,T,Dim>
{


protected:
    T Lprior;
    T ConnectEpsilon;
    T particle_energy_change_costs;

    T lambda;
    bool temp_dependent;
    bool use_saliency_map;
    bool consider_birth_death_ratio;
    bool sample_scale_temp_dependent;
public:


    T dynamic_weight (
        std::size_t num_edges,
        std::size_t num_particles,
        std::size_t num_connected_particles,
        std::size_t num_bifurcations,
        std::size_t num_terminals,
        std::size_t num_segments,
        T volume_img,
        T volume_particles
    ) {
        return CProposal<TData,T,Dim>::dynamic_weight_segment ( num_edges,
                num_particles,
                num_connected_particles,
                num_bifurcations,
                num_terminals,
                num_segments,
                volume_img,
                volume_particles );
    }


    CProposalBifurcationBridgeDeath() : CProposal<TData,T,Dim>() {
        Lprior=-10;
        ConnectEpsilon=0.0001;
	lambda=100;
        particle_energy_change_costs=update_energy_particle_costs ( lambda );

        
        temp_dependent=true;
        use_saliency_map=false;
        consider_birth_death_ratio=false;
        sample_scale_temp_dependent=false;

    }



    ~CProposalBifurcationBridgeDeath() {

    }

    std::string get_name() {
        return enum2string ( PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_DEATH );
    };

    PROPOSAL_TYPES get_type() {
        return ( PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_DEATH );
    }

    void set_params ( const mxArray * params=NULL ) {

        sta_assert_error ( this->tracker!=NULL );

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;

        if ( params==NULL ) {
            return;
        }

        if ( mhs::mex_hasParam ( params,"Lprior" ) !=-1 ) {
            Lprior=mhs::mex_getParam<T> ( params,"Lprior",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"ConnectEpsilon" ) !=-1 ) {
            ConnectEpsilon=mhs::mex_getParam<T> ( params,"ConnectEpsilon",1 ) [0];
        }




        if ( mhs::mex_hasParam ( params,"lambda" ) !=-1 ) {
            lambda=mhs::mex_getParam<T> ( params,"lambda",1 ) [0];
        }

        particle_energy_change_costs=update_energy_particle_costs ( lambda );

        if ( mhs::mex_hasParam ( params,"temp_dependent" ) !=-1 ) {
            temp_dependent=mhs::mex_getParam<bool> ( params,"temp_dependent",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }





    }

    void init ( const mxArray * feature_struct ) {};

    bool propose() {
        pool<class Points<T,Dim> > &	particle_pool		=* ( this->tracker->particle_pool );
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=* ( this->tracker->data_fun );
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=* ( this->tracker->tree );
        OctTreeNode<T,Dim> &		conn_tree		=* ( this->tracker->connecion_candidate_tree );
        OctTreeNode<T,Dim> &		bifurcation_tree	=* ( this->tracker->bifurcation_tree );
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=* ( this->tracker->edgecost_fun );
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
        Collision<T,Dim> &   collision_fun_p=* ( this->tracker->collision_fun );
        bool  single_scale		= ( this->tracker->single_scale );

        this->proposal_called++;

        if ( sample_scale_temp_dependent && ( ! ( temp<maxscale ) ) ) {
            return false;
        }

        class Connection<T,Dim> * edge;

        /// uniformly pic edge
        edge=connections.get_rand_edge();
        if ( edge==NULL ) {
            return false;
        }

        if ( edge->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
            return false;
        }

        sta_assert_debug0 ( ( *edge->pointA->endpoint_connections==1 ) &&
                            ( *edge->pointB->endpoint_connections==1 ) );

        sta_assert_debug0 ( edge->edge_type==EDGE_TYPES::EDGE_SEGMENT );




        class Points<T,Dim>::CEndpoint * bridges[2][2];

        bridges[0][0]=edge->pointA;
        bridges[0][1]=edge->pointB;
        
        //TODO shouldnt bridges[0][0] and bridges[0][1] be randomly shuffeld?

        bridges[1][0]=bridges[1][1]=NULL;



#ifndef BRIDGETBIRTHDEATH_SINGLE_PART
        // check if bridge_end is_segment
        if ( !bridges[0][1]-point->is_segment() ) {
            return false;
        }
#endif

        T undo_proposed_prob;
        T proposed_prob;


        if ( ! ( CProposalBifurcationBridgeDeath<TData,T,Dim>::do_tracking3 (
                     * ( bridges[0][0] ),
                     * ( bridges[0][1] ),
                     tree,
                     search_connection_point_center_candidate,
                     ( bridges[1][0] ),
                     ( bridges[1][1] ),
                     proposed_prob,false ) ) ) {
            return false;
        }

        sta_assert_debug0 ( bridges[1][0]!=NULL );
        sta_assert_debug0 ( bridges[1][1]!=NULL );


	sta_assert_debug0(bridges[0][0]->point!=bridges[0][1]->point);	
	sta_assert_debug0(bridges[0][0]->point!=bridges[1][0]->point);
	sta_assert_debug0(bridges[0][0]->point!=bridges[1][1]->point);	
	sta_assert_debug0(bridges[0][1]->point!=bridges[1][0]->point);
	sta_assert_debug0(bridges[0][1]->point!=bridges[1][1]->point);	
	sta_assert_debug0(bridges[1][0]->point!=bridges[1][1]->point);	



	
	

        typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;

        T old_edgecost=0;
        T new_edgecost=0;


        class Points< T, Dim >::CEndpoint & endpointA=*bridges[0][0];
        class Points< T, Dim >::CEndpoint & endpointB=*bridges[1][0];
        class Points< T, Dim >::CEndpoint & endpointC=*bridges[1][1];
        class Points< T, Dim >::CEndpoint & endpointD=*bridges[0][1];
	
	
        new_edgecost=edgecost_fun.e_cost (
                         endpointA,
                         endpointB,
                         endpointC,
                         inrange ) +options.bifurcation_bonus_L;

        if ( inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) {
            return false;
        }

        sta_assert_error ( ( CBridgeBirth<TData,T,Dim>::do_tracking3 (
                                 *bridges[0][0],
                                 conn_tree,
                                 search_connection_point_center_candidate,
                                 bridges[0][1],
                                 undo_proposed_prob,false,false ) ) ) ;


        old_edgecost+=edgecost_fun.e_cost (
                          endpointA,
                          endpointD,
                          inrange ) +options.connection_bonus_L;

        //sta_assert_debug0 ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );
	if ( inrange!= ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) )
	{
	  edgecost_fun.e_cost (
                          endpointA,
                          endpointD,
                          inrange,true,true );
	  printf("inrange: %d\n",static_cast<int>(inrange));
	  sta_assert_debug0 ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );
	}


        old_edgecost+=edgecost_fun.e_cost (
                          endpointB,
                          endpointC,
                          inrange ) +options.connection_bonus_L;

        sta_assert_debug0 ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) );


        T R= ( T ) ( connections.pool->get_numpts() ) / T ( connections.get_num_bifurcation_centers() +1 ) ;


        sta_assert_error ( R>0 );





        class OctPoints<T,Dim> * query_buffer[4][query_buffer_size];
        class Collision<T,Dim>::Candidates candA ( query_buffer[0] );
        class Collision<T,Dim>::Candidates candB ( query_buffer[1] );
        class Collision<T,Dim>::Candidates candC ( query_buffer[2] );
        class Collision<T,Dim>::Candidates candD ( query_buffer[3] );

        class Collision<T,Dim>::Candidates * cand[4];
        cand[0]=&candA;
        cand[1]=&candB;
        cand[2]=&candC;
        cand[3]=&candD;

        T particle_interaction_cost_old=0;

        class Points<T,Dim> * old_points[4];
        old_points[0]=bridges[0][0]->point;
        old_points[1]=bridges[0][1]->point;
        old_points[2]=bridges[1][0]->point;
        old_points[3]=bridges[1][1]->point;

        int mode=1; // only special collision costs

        //############################
        // before adding edges
        //############################

        T point_cost_old=0;
        for ( int i=0; i<4; i++ ) {
            point_cost_old+=old_points[i]->compute_cost3 ( temp );
        }

        if ( collision_fun_p.is_soft() ) {
            T tmp;
            for ( int i=0; i<4; i++ ) {
                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       *old_points[i],
                                       collision_search_rad,
                                       temp,
                                       cand[i],true,
                                       -9,mode  // collect colliding pts
                                   ) );
                particle_interaction_cost_old+=tmp;
            }
        }

        // backup and remove the old connections
        class Connection<T,Dim>    old_connection_backup[2];
        for ( int i=0; i<2; i++ ) {
            sta_assert_debug0 ( bridges[i][0]->connected!=NULL );
	    class Connection< T, Dim > * delete_me=( bridges[i][0]->connection );
            old_connection_backup[i]=* delete_me;
            connections.remove_connection ( delete_me);
        }


        class Connection<T,Dim> *  new_connection_pt[3];
        Points<T,Dim> * p_b_particle=particle_pool.create_obj();
        Points<T,Dim> & b_particle=*p_b_particle;
        b_particle.tracker_birth=3;
        b_particle.set_direction ( Vector<T,Dim> ( 1,0,0 ) );

//we add three new connections to the bif particle
        class Connection<T,Dim> * new_connections[3];
        class Points<T,Dim>::CEndpoint * bifurcation_endpoints[3];
        bifurcation_endpoints[0]=&endpointA;;
        bifurcation_endpoints[1]=&endpointB;
        bifurcation_endpoints[2]=&endpointC;

        sta_assert_debug0 ( *bifurcation_endpoints[0]->endpoint_connections==0 );
        sta_assert_debug0 ( *bifurcation_endpoints[1]->endpoint_connections==0 );
        sta_assert_debug0 ( *bifurcation_endpoints[2]->endpoint_connections==0 );

        class Connection<T,Dim> a_new_connection;
        a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;

        ///create the three edges
        for ( int i=0; i<3; i++ ) {
            a_new_connection.pointB=b_particle.getfreehub ( Points<T,Dim>::bifurcation_center_slot );
            Points<T,Dim> * pointB=bifurcation_endpoints[i]->point;
            a_new_connection.pointA=pointB->getfreehub ( bifurcation_endpoints[i]->side );
// 	      if (pointB!=&point)
// 	      {
// 		a_new_connection.freeze_topology=backup_existing_edge.is_protected_topology();
// 	      }
            sta_assert_debug0 ( a_new_connection.pointB->point!=a_new_connection.pointA->point );
            new_connection_pt[i]=connections.add_connection ( a_new_connection );
        }




        //############################
        // after adding edges
        //############################

        T particle_interaction_cost_new=0;
        T bifurcation_data_new=0;
        T point_cost_new=0;

        bool out_of_bounce=false;
        b_particle.bifurcation_center_update ( shape,out_of_bounce );

        if ( out_of_bounce ) {
            goto cleanup_particle_and_edges;
        }




        for ( int i=0; i<4; i++ ) {
            point_cost_new+=old_points[i]->compute_cost3 ( temp );
        }

        if ( collision_fun_p.is_soft() ) {

            T tmp;
            if ( options.bifurcation_particle_soft ) {
                if ( collision_fun_p.colliding ( tmp,
                                                 tree,
                                                 b_particle,
                                                 collision_search_rad,
                                                 temp ) ) {
                    goto cleanup_particle_and_edges;
                }

                particle_interaction_cost_old+=tmp;
            }

            for ( int i=0; i<4; i++ ) {
                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       *old_points[i],
                                       collision_search_rad,
                                       temp,
                                       cand[i],false,
                                       -10,mode  // collect colliding pts
                                   ) );
                particle_interaction_cost_new+=tmp;
            }


        }


        if ( !b_particle.compute_data_term ( data_fun,bifurcation_data_new ) ) {
            goto cleanup_particle_and_edges;
        }

        b_particle.point_cost=bifurcation_data_new;


        {


            T  Eold= ( point_cost_old
                       +particle_interaction_cost_old
                       +old_edgecost ) ;

            T  Enew= ( point_cost_new
                       +particle_interaction_cost_new
                       +new_edgecost
                       +bifurcation_data_new
                     ) ;


            T E= ( Enew-Eold ) /temp;

            R*=mhs_fast_math<T>::mexp ( E );


            R*=undo_proposed_prob/ ( proposed_prob+std::numeric_limits<T>::epsilon() );

            if ( R>=myrand ( 1 ) +std::numeric_limits<T>::epsilon() ) {
                if ( Constraints::constraint_hasloop_follow ( *endpointA.point,endpointA.connection,options.constraint_loop_depth ) ) {
                    goto cleanup_particle_and_edges;
                }


                Points<T,Dim>  & bridge_endpoint=* ( endpointD.point );

                if ( bridge_endpoint.get_num_connections() <2 ) {
                    if ( !conn_tree.insert ( bridge_endpoint,true) ) {
                        goto cleanup_particle_and_edges;
                    }
                }else
		{
		  sta_assert_debug0((bridge_endpoint.get_num_connections()==2));
		  bridge_endpoint.unregister_from_grid(voxgrid_conn_can_id);
		}
                
                
                sta_assert_error_c(bridge_endpoint.is_segment()!=bridge_endpoint.has_owner(voxgrid_conn_can_id),printf("num conn: %d\n",bridge_endpoint.get_num_connections()););


                if ( options.bifurcation_particle_hard || options.bifurcation_particle_soft ) {
                    if ( !tree.insert ( b_particle ) ) {
                        goto cleanup_particle_and_edges;
                    }
                }

                if ( !bifurcation_tree.insert ( b_particle ) ) {
                    goto cleanup_particle_and_edges;
                }


                this->tracker->update_energy ( E+particle_energy_change_costs,static_cast<int> ( get_type() ) );
                this->proposal_accepted++;


#ifdef  D_USE_GUI
                for ( int i=0; i<4; i++ ) {
                    old_points[i]->touch ( get_type() );
                }
#endif

               
                return true;
            }

        }



    cleanup_particle_and_edges:

        for ( int b=0; b<3; b++ ) {
            connections.remove_connection ( new_connection_pt[b] );
        }

        for ( int i=0; i<2; i++ ) {
            connections.add_connection ( old_connection_backup[i] );
        }

        particle_pool.delete_obj ( p_b_particle );
	
	{
	Points<T,Dim>  & bridge_endpoint=* ( endpointD.point );
	if ((bridge_endpoint.has_owner(voxgrid_conn_can_id))&&(bridge_endpoint.get_num_connections()==2))
	{
	 bridge_endpoint.unregister_from_grid(voxgrid_conn_can_id); 
	}}               
	
        return false;
    }



public:


    static bool  do_tracking3 (
        const class Points< T, Dim >::CEndpoint  & bride_ep_start,
        const class Points< T, Dim >::CEndpoint  & bride_ep_end,
        OctTreeNode<T,Dim> & conn_tree,
        T search_connection_point_center_candidate,
        class Points< T, Dim >::CEndpoint * & bif_ept_B,
        class Points< T, Dim >::CEndpoint * & bif_ept_C,
        T & proposed_prob,
        bool debug=false
    ) {


        if ( debug ) {
            printf ( "do_tracking\n" );
        }



        class Points<T,Dim> & bride_start=*bride_ep_start.point;
        class Points<T,Dim> & bride_end=*bride_ep_end.point;

        /*!
         collect all points in search rectangle
         */
        const Vector<T,Dim> & endpoint=bride_start.get_position();

        std::size_t found;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        try {
            conn_tree.queryRange (
                endpoint,
                search_connection_point_center_candidate,
                candidates,
                query_buffer_size,
                found );
        } catch ( mhs::STAError & error ) {
            throw error;
        }

//         sta_assert_debug0 ( bridge_start_point.maxconnections>0 );

        /// no points nearby. no existing bridge, do nothing
        if ( (
                    ( ( found==1 ) && ( ( query_buffer[0]==&bride_start ) || ( query_buffer[0]==&bride_end ) ) )
                    || ( ( found==2 ) && ( ( ( query_buffer[0]==&bride_start ) && ( query_buffer[1]==&bride_end ) )
                                           || ( ( query_buffer[1]==&bride_start ) && ( query_buffer[0]==&bride_end ) )
                                         ) )
                )
                &&
                ( bif_ept_C==NULL ) ) {
            if ( debug ) {
                printf ( "no points\n",found );
            }
            return false;
        }


        T current_edge_lengths=CEdgecost<T,Dim>::get_current_edge_length_limit();

        T searchrad=CEdgecost<T,Dim>::get_total_edge_length_limit();


        if ( debug ) {
            printf ( "found %d initial candidates in radius %f\n",found,searchrad );
        }


        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        if ( debug ) {
            printf ( "collecting endpoints in circle\n" );
        }


        class Points< T, Dim >::CEndpoint * connet_candidates_bufferB[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** connet_candidatesB=connet_candidates_bufferB;
        class Points< T, Dim >::CEndpoint * connet_candidates_bufferC[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** connet_candidatesC=connet_candidates_bufferC;

        T connet_prop[query_buffer_size];
        T connet_prop_acc[query_buffer_size];

        std::size_t num_connet_candidates=0;

        if ( bif_ept_B!=NULL ) {
            if ( debug ) {
                printf ( "check existing candidates\n" );
            }
            if ( found> ( query_buffer_size-2 ) ) {
                proposed_prob=-1;
                return false;
            }

            class Points<T,Dim>::CEndpoint & ept_B=* bif_ept_B;
            class Points<T,Dim>::CEndpoint & ept_C=* bif_ept_C;
            /*!
            if not in radius remove
            point from further consideration
            */
            T candidate_dist_sq= ( ( *bride_ep_start.position )- ( *ept_C.position ) ).norm2();
            sta_assert_error ( ! ( searchrad<candidate_dist_sq ) );
            candidate_dist_sq= ( ( *bride_ep_start.position )- ( *ept_B.position ) ).norm2();
            sta_assert_error ( ! ( searchrad<candidate_dist_sq ) );

// 		if ( searchrad>candidate_dist_sq ) {
// 		    candidate_dist_sq= ( ( *bride_ep_start.position )- ( *ept_B.position ) ).norm2();
// 		    if ( searchrad>candidate_dist_sq ) {

            class Points<T,Dim> * pointB=ept_B.point;
            class Points<T,Dim> * pointC=ept_C.point;

            Vector<T,3> center= ( ( pointC->get_position() )
                                  + ( pointB->get_position() )
                                  + ( bride_start.get_position() )
                                ) /T ( 3 );
            Vector<T,3> opt_dir;
            T score=std::numeric_limits< T >::max();
            opt_dir= ( center- ( *ept_C.position ) );
            opt_dir.normalize();
            score=ept_C.get_slot_direction().dot ( opt_dir );

            score=std::min ( score,ept_B.get_slot_direction().dot ( opt_dir ) );
            score=std::min ( score,bride_ep_start.get_slot_direction().dot ( opt_dir ) );

            ( *connet_candidatesB++ ) =&ept_B;
            ( *connet_candidatesC++ ) =&ept_C;
	    
	    sta_assert_debug0(ept_B.point!=ept_C.point);

            score+=1;

            connet_prop[num_connet_candidates]=score;
            connet_prop_acc[num_connet_candidates]=score;
            num_connet_candidates++;
// 		    }else
// 		    {
// 		      proposed_prob=-1;
//                         return false;
// 		    }
// 		} else
// 		{
// 		  proposed_prob=-1;
//                         return false;
// 		}
        }


        int thread_id;

#ifdef D_USE_MULTITHREAD
        if ( thread_id==-1 ) {
            thread_id= omp_get_thread_num();
        }
#else
        thread_id= 0;
#endif

        track_unique_id[thread_id]++;
        {
            {
                /*!
                check for all candidates if endpoints are within the circle
                    with radius "searchrad"
                */
                for ( std::size_t a=0; a<found; a++ ) {
                    Points<T,Dim> * pointB= ( Points<T,Dim> * ) candidates[a];

                    if ( pointB->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT ) {
                        continue;
                    }
                    
                    if ( pointB->get_num_connections() ==0 ) {
                        continue;
                    }

                    if ( ( pointB!=&bride_start ) && ( pointB!=&bride_end ) ) {


                        for ( int pointside=0; pointside<=1; pointside++ ) {
                            if ( ( pointB->endpoint_connections[pointside]==1 ) ) {
                                class Points<T,Dim>::CEndpoint & ept_B=* ( pointB->endpoints[pointside][0] );

                                if ( ( ept_B.connection->track_me_id!=track_unique_id[thread_id] ) &&
                                        ( ept_B.connection->edge_type==EDGE_TYPES::EDGE_SEGMENT )
                                   )  {
                                    ept_B.connection->track_me_id=track_unique_id[thread_id];
                                    class Points<T,Dim>::CEndpoint & ept_C=* ( ept_B.connected );
				    
				     
				    
                                    Points<T,Dim> * pointC  = ept_C.point;

                                    if (( pointC->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT )
				      && ( pointC!=&bride_start ) && ( pointC!=&bride_end ) 
				      && (!Connections<T,Dim>::connection_exists(*pointC,bride_start,1))
				      && (!Connections<T,Dim>::connection_exists(*pointC,bride_end,1))
				      && (!Connections<T,Dim>::connection_exists(*pointB,bride_start,1))
				      && (!Connections<T,Dim>::connection_exists(*pointB,bride_end,1))
				      )
				     {
                                        /*!
                                        if not in radius remove
                                        point from further consideration
                                        */
                                        T candidate_dist_sq= ( ( *bride_ep_start.position )- ( *ept_C.position ) ).norm2();
                                        if ( searchrad>candidate_dist_sq ) {
                                            candidate_dist_sq= ( ( *bride_ep_start.position )- ( *ept_B.position ) ).norm2();
                                            if ( searchrad>candidate_dist_sq ) {

                                                // Vector<T,3> center= ( ( *ept_C.position ) + ( *ept_B.posiiton ) + ( *bride_start.position ) ) /T ( 3 );
                                                Vector<T,3> center= ( ( pointC->get_position() ) + ( pointB->get_position() ) + ( bride_start.get_position() ) ) /T ( 3 );

                                                Vector<T,3> opt_dir;
                                                T score=std::numeric_limits< T >::max();
                                                opt_dir= ( center- ( *ept_C.position ) );
                                                opt_dir.normalize();
                                                score=ept_C.get_slot_direction().dot ( opt_dir );

                                                score=std::min ( score,ept_B.get_slot_direction().dot ( opt_dir ) );
                                                score=std::min ( score,bride_ep_start.get_slot_direction().dot ( opt_dir ) );

                                                ( *connet_candidatesB++ ) =&ept_B;
                                                ( *connet_candidatesC++ ) =&ept_C;

						sta_assert_debug0(ept_B.point!=ept_C.point);
						
                                                score+=1;


                                                connet_prop[num_connet_candidates]=score;
                                                if ( num_connet_candidates==0 ) {
                                                    connet_prop_acc[num_connet_candidates]=score;
                                                } else {
                                                    connet_prop_acc[num_connet_candidates]=connet_prop_acc[num_connet_candidates-1]+score;
                                                }
//                                         connet_prop[num_connet_candidates]=score;
//                                         connet_prop_acc[num_connet_candidates]=score;
                                                num_connet_candidates++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }





                if ( bif_ept_B!=NULL ) {
                    sta_assert_debug0 ( num_connet_candidates>0 );
                    //*existing_prob=connet_prop[0]/connet_prop_acc[num_connet_candidates-1];
                    proposed_prob=connet_prop[0]/connet_prop_acc[num_connet_candidates-1];

                } else {

                    if ( num_connet_candidates<1 ) {
                        return false;
                    }
                    

                    std::size_t connection_candidate=rand_pic_array ( connet_prop_acc,num_connet_candidates,connet_prop_acc[num_connet_candidates-1] );

                    if ( debug ) {
                        printf ( "choosing %d\n",connection_candidate );
                    }
                    
                    
                    proposed_prob=connet_prop[connection_candidate]/connet_prop_acc[num_connet_candidates-1];
                    bif_ept_B=connet_candidates_bufferB[connection_candidate];
                    bif_ept_C=connet_candidates_bufferC[connection_candidate];
		    
		    
		    ///NOTE now check for depth 2 (we were lazy befor, because this very rarly happens)
                    if (
					 (Connections<T,Dim>::connection_exists(*(bif_ept_C->point),bride_start))
				      || (Connections<T,Dim>::connection_exists(*(bif_ept_C->point),bride_end))
				      || (Connections<T,Dim>::connection_exists(*(bif_ept_B->point),bride_start))
				      || (Connections<T,Dim>::connection_exists(*(bif_ept_B->point),bride_end))
		    )
		    {
		         return false;
		    }
		    
		    sta_assert_debug0(bif_ept_B->point!=bif_ept_C->point);
                }

                return true;
            }
        }


    };



};












#endif
