#ifndef PROPOSAL_BIFURCATION_TRACK_H
#define PROPOSAL_BIFURCATION_TRACK_H

 #define DEBUG__BIF_TRCK
 

template<typename TData,typename T,int Dim>
class CProposalBifurcationTrackBirth : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
      const static int max_stat=10;
    int statistic[max_stat];
   /* 
    mhs::dataArray<TData>  saliency_map;
    double saliency_correction[3];*/
     T lambda;
     T particle_energy_change_costs;
     
        bool use_saliency_map;
      bool consider_birth_death_ratio;
    bool sample_scale_temp_dependent;
     
       bool temp_dependent;
public:


     T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_edges(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }


    CProposalBifurcationTrackBirth(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
	 lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	
	        use_saliency_map=false;
        consider_birth_death_ratio=false;
        sample_scale_temp_dependent=false;
	 
//       saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
	
	temp_dependent=true;
    }
    
      ~CProposalBifurcationTrackBirth()
    {
//         printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("(%d)%d ",i,statistic[i]);
// 	printf("\n");
	
    }
    
    T get_default_weight(){return 1;};

    std::string get_name() {
       return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH);
    };
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH);
    }

    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
	particle_energy_change_costs=update_energy_particle_costs(lambda);  
      
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	
	 if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }

 
	particle_energy_change_costs=update_energy_particle_costs(lambda);
    }

    void init(const mxArray * feature_struct) 
    {
	sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

    };

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);
	
	  if ( sample_scale_temp_dependent && ( ! ( temp<maxscale ) ) ) {
            return false;
        }

	
	int stat_count=0;
	statistic[stat_count++]++;	
	
	bool debug=false;
	
	this->proposal_called++;
	
	class Connection<T,Dim> * edge;

        /// uniformly pic edge
        edge=connections.get_rand_edge();
        if (edge==NULL)
            return false;
	
	
	sta_assert_debug0(edge->pointA->connected!=NULL);
	sta_assert_debug0(edge->pointB->connected!=NULL);

	if (options.bifurcation_neighbors)
	{
	  if (edge->edge_type!=EDGE_TYPES::EDGE_SEGMENT)
	    return false;
	}else
	{
	if (((edge->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)||
	  (edge->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)))
	 return false; 
	}
	
	
	
	sta_assert_debug0(edge->edge_type!=EDGE_TYPES::EDGE_BIFURCATION);
	
	
	
	bool protected_topology=(edge->is_protected_topology());
	
	
      if (sample_scale_temp_dependent && ((temp>edge->pointA->point->get_scale()||temp>edge->pointB->point->get_scale())))
      {
	    return false; 
      }
	
//1	
statistic[stat_count++]++;	
	
	
	try
	{
	  class Points<T,Dim>::CEndpoint ** endpoints=edge->points;
	  
// 	  endpoints[0]->point->update_endpoint(endpoints[0]->side);
// 	  endpoints[1]->point->update_endpoint(endpoints[1]->side);
	  const Vector<T,Dim> & centerA=*(endpoints[0]->position);
	  const Vector<T,Dim> & centerB=*(endpoints[1]->position);
	  
	  
	  T particle_costs=0;
	  particle_costs-=endpoints[0]->point->compute_cost3(temp); //old segment point A
	  particle_costs-=endpoints[1]->point->compute_cost3(temp); //old segment point B
	  
	  
 	  //T temp_fact_pos=std::sqrt(temp);
// 	  T temp_fact_scale=temp_fact_pos+1;
// 	  T & temp_fact_rot=temp_fact_pos;
	  
	  const T & scaleA=endpoints[0]->point->get_scale();
	  const T & scaleB=endpoints[1]->point->get_scale();
	  
	  const int side=1; 
	  Points<T,Dim> * p_point=particle_pool.create_obj();
	  Points<T,Dim> & b_point=*p_point;		      
	  b_point.tracker_birth=3;
	  
	  
	   if (!single_scale)
	    {
	      if ( sample_scale_temp_dependent ) {
		
		    T scale_lower=std::max ( minscale,temp );
		    b_point.set_scale(myrand(scale_lower,maxscale));
	      } else {
		    b_point.set_scale(myrand(minscale,maxscale));
	      }
	    }else
	    {
	       b_point.set_scale(maxscale);
	    }
	  
	 

	  Vector<T,Dim> n[3];
	  n[0]=(centerA-centerB);
	  T n_length=std::sqrt(n[0].norm2());
	  n[0]/=n_length+std::numeric_limits<T>::epsilon();
	  mhs_graphics::createOrths(n[0],n[1],n[2]);

	  
	  T h_tmp_length_fact=(scaleA+scaleB)/2;
	  if (temp_dependent)
	    h_tmp_length_fact=0.5*(h_tmp_length_fact+proposal_temp_trafo(temp,h_tmp_length_fact,T(1)));
	     //h_tmp_length_fact=std::sqrt(temp);
	    
	  
	  T h=0;
	  T h_rel_offset=0.5;
if (true)	  
{  
	  // length of sides of the optimal triangle based
	  // on three point scales
	 
	  T  a=CEdgecost<T,Dim>::side_scale*scaleA;
	  T  b=CEdgecost<T,Dim>::side_scale*scaleB;
	  T  c=CEdgecost<T,Dim>::side_scale*b_point.get_scale();
	  
	  
	  // we compute the location of the three center points 
	  // of the three sides
	  T w=(b*b+c*c-a*a)/(2*b*c);
	  if ((std::abs(w)>1))
	  {
	   particle_pool.delete_obj(p_point);
	    return false; 
	  }
	  
	  sta_assert_debug0(!(std::abs(w)>1));
	  Vector<T,2> P1;
	  P1[0]=c;
	  P1[1]=0;
	  Vector<T,2> P2;
	  P2[0]=b*w;
	  P2[1]=b*std::sqrt(1-w*w);
	  
	  
	  // the three center points 
	  Vector<T,2> P[3];
	  P[0]=(P1+P2)/2; // devides line a
	  P[1]=P2/2;	  // devides line b	
	  P[2]=P1/2;	  // devides line c	
	
	  Vector<T,2> n_=P[1]-P[0];
	  n_.normalize();
	  Vector<T,2> v_=(P[2]-P[0]);
	  
	  // optimal height:
	  h_rel_offset=n_.dot(v_);
	  h=std::sqrt(((P[2]-P[0])-n_*h_rel_offset).norm2());
	  // relative offset between P[2] and P[1]
	  h_rel_offset/=std::sqrt(v_.norm2());

	  
	  h*=1+h_tmp_length_fact;
	  
	  
	  T sign=(Points<T,Dim>::thickness<0) ? -1 : 1 ; 
	  T lC=sign*Points<T,Dim>::thickness*b_point.get_scale();
	  h+=lC;
	  
	  //printf("%f %f %f\n",h,h2,(scaleA+scaleB)/2);
}
else	
{
	  h=(scaleA+scaleB)/2;
}	

	  
	
	  // randomly picking a point on the circular orbit around n[0]
	  T orbit_angle=myrand(2*M_PI);
	  Vector<T,Dim> suggested_direction= n[1]*std::cos(orbit_angle)
					    -n[2]*std::sin(orbit_angle);
	
	  // computing position of orbit point in image space
	  // offset is relative optimal h-offset
	  Vector<T,Dim> suggested_point=centerA;
	  suggested_point+=n[0]*(h_rel_offset*n_length)+suggested_direction*(h);
	  
	  T sigma_h=h_tmp_length_fact*h/2;
	  T sigma_h2=(scaleA+scaleB)/4;//+n_length;
	  T noise_pos[3];
	  noise_pos[0]=mynormalrand(sigma_h);
	  noise_pos[1]=mynormalrand(sigma_h2);
	  noise_pos[2]=mynormalrand(sigma_h2);
	  b_point.set_position(suggested_point
		      +suggested_direction*noise_pos[0]
		      +n[0]*noise_pos[1]
		      +n[0].cross(suggested_direction)*noise_pos[2]);
	  
	  if ((b_point.get_position()[0]<0)||
		      (b_point.get_position()[1]<0)||
		      (b_point.get_position()[2]<0)||
		      (!(b_point.get_position()[0]<shape[0]))||
		      (!(b_point.get_position()[1]<shape[1]))||
		      (!(b_point.get_position()[2]<shape[2]))
		      ){
			particle_pool.delete_obj(p_point);
			return false;
		      }	  

	  
	
	  T sigma_r=0.25;
	  
	  b_point.set_direction(random_pic_direction<T,Dim>(suggested_direction,sigma_r));
//2	  
statistic[stat_count++]++;	

	  
	  
	  
	  
	    T newpointsaliency=0;
		if ((!data_fun.saliency_get_value(newpointsaliency,b_point.get_position()))||((newpointsaliency<std::numeric_limits<T>::min())))
		{
		   particle_pool.delete_obj(p_point);
		  return false;
		}
				
		/// evaluating data term
		T newenergy_data;	
		if (!data_fun.eval_data(
		      newenergy_data,
		      b_point))
		  {
			    particle_pool.delete_obj(p_point);
			    return false;
		    }		
		
		    b_point.point_cost=newenergy_data;
		    b_point.saliency=newpointsaliency;///saliency_correction[2];	  
	  
	  
	  
	  
//3	  
statistic[stat_count++]++;		  
	  
	  T connection_probability=1;
	  
if (debug)
  printf("[:");
	  
	  //connection_probability*=normal_dist<T,3>(randv.v,sigma);
	  connection_probability*=normal_dist<T>(noise_pos[0],
					      noise_pos[1],
					      noise_pos[2],
					      sigma_h,
					      sigma_h2,
					      sigma_h2);
	  
	  
// 	  printf("cp: %f ",connection_probability);
	  connection_probability*=normal_dist_sphere(suggested_direction,b_point.get_direction(),sigma_r);
// 	  printf(", %f (%f %f) \n",normal_dist_sphere(suggested_direction,b_point.direction,temp_fact_rot),std::sqrt(suggested_direction.norm2()),std::sqrt(b_point.direction.norm2()));
	  connection_probability/=2*M_PI;
	  
/*connection_probability=1;	*/  
	  
	// edge costs of the OLD configuration
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	T old_edgecost=edge->e_cost(
	  edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange);
	
	  #ifdef _DEBUG_CHECKS_0_
		  if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		  {
		    edge->pointA->position->print();
		    edge->pointB->position->print();
		    printf("%f %f\n",options.connection_candidate_searchrad,std::sqrt((*edge->pointA->position-*edge->pointB->position).norm2()));
		    sta_assert_debug0(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
		  }
	  #endif	
	
	/// of course the old edge should be in range (valid)
	sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));	
	
	
	
	Points<T,Dim> * p_bif_center_point=particle_pool.create_obj();    
	Points<T,Dim> & bif_center_point=*p_bif_center_point;
	bif_center_point.set_direction(Vector<T,Dim>(1,0,0));
	bif_center_point.tracker_birth=3;

	class Connection<T,Dim> edge_backup;
	edge_backup=*edge;	
	
	
	  class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	  class Collision<T,Dim>::Candidates candA ( query_bufferA );

	  class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
	  class Collision<T,Dim>::Candidates candB ( query_bufferB );
	
          T particle_interaction_cost_old=0;
          T particle_interaction_cost_new=0;
	    
	 int mode=1; // only special collision costs

            //############################
            // before adding edges
            //############################

            if ( collision_fun_p.is_soft() ) {
                T tmp;
                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       *(edge_backup.pointA->point),
                                       collision_search_rad,
                                       temp,
                                       &candA,true,
                                       -9,mode  // collect colliding pts
                                   ) );
                particle_interaction_cost_old+=tmp;

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       *(edge_backup.pointB->point),
                                       collision_search_rad,
                                       temp,
                                       &candB,true,
                                       -9,mode  // collect colliding pts
                                   ) );
                particle_interaction_cost_old+=tmp;
            }
	
	
	
	connections.remove_connection(edge);
	
	class Connection<T,Dim> * new_connections[3];
	class Points<T,Dim>::CEndpoint * bifurcation_endpoints[3];
	bifurcation_endpoints[0]=b_point.endpoints[side][0];
	bifurcation_endpoints[1]=edge_backup.pointA;
	bifurcation_endpoints[2]=edge_backup.pointB;
	
// 	bifurcation_endpoints[0]->point->update_endpoint(bifurcation_endpoints[0]->side);
	
	T new_edgecost= edgecost_fun.e_cost( *bifurcation_endpoints[0],
				      *bifurcation_endpoints[1],
				      *bifurcation_endpoints[2],
				      inrange
				    )+options.bifurcation_bonus_L;
				    
	
	if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	{
	  goto cleanup_particle_and_edges_partial;
	}   
				    
	{

	sta_assert_debug0(*bifurcation_endpoints[0]->endpoint_connections==0);
	sta_assert_debug0(*bifurcation_endpoints[1]->endpoint_connections==0);
	sta_assert_debug0(*bifurcation_endpoints[2]->endpoint_connections==0);
	
	class Connection<T,Dim> a_new_connection;
	a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
	
	///create the three edges
	for (int i=0;i<3;i++)
	{
/*printf("%d -> ",static_cast<int>(bif_center_point.particle_type));	*/  
	  a_new_connection.pointA=bif_center_point.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	  Points<T,Dim> * pointB=bifurcation_endpoints[i]->point;
	  a_new_connection.pointB=pointB->getfreehub(bifurcation_endpoints[i]->side);
	  
	  if (i==0)
	  {
	   a_new_connection.freeze_topology=false; 
	  } else
	  {
	    a_new_connection.freeze_topology=protected_topology;
	  }
	  
	  new_connections[i]=connections.add_connection(a_new_connection);
#ifdef _DEBUG_CHECKS_0_
// printf("%d \n",static_cast<int>(bif_center_point.particle_type));
	  switch(i) 
	  {
	    case 0:
	      sta_assert_error(bif_center_point.particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
	      break;
	    case 1:
	      sta_assert_error(bif_center_point.particle_type==(PARTICLE_TYPES::PARTICLE_SEGMENT));
	      break;
	    case 2:
	      sta_assert_error(bif_center_point.particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
	      break;  
	  }
#endif	    
	}
	
	sta_assert_debug0((bif_center_point.endpoint_connections[Points< T, Dim >::bifurcation_center_slot])==3);
	
	
	sta_assert_error((bif_center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected)!=NULL);
	sta_assert_error((bif_center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected)!=NULL);
	sta_assert_error((bif_center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected)!=NULL);
	particle_costs+=bif_center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->point->compute_cost3(temp);
	particle_costs+=bif_center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected->point->compute_cost3(temp);
	particle_costs+=bif_center_point.endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected->point->compute_cost3(temp);
	
	bool out_of_bounce=false; 
	bif_center_point.bifurcation_center_update(shape,out_of_bounce);
	
	if (out_of_bounce)
	{
	    goto cleanup_particle_and_edges;
	}  

// 	  T particle_interaction_cost_new=0;
// 	  if (collision_fun_p.colliding(particle_interaction_cost_new, tree,b_point,collision_search_rad,temp))
// 	  {
// 	      goto cleanup_particle_and_edges;
// 	  }	
	  
	    //############################
            // after adding edges
            //############################
            
            //NOTE bridge_pt is not in tree now, so only visible in his has_own
            // collision check
      
            if ( collision_fun_p.is_soft() ) {
                T tmp;
                if ( collision_fun_p.colliding (
                            tmp,
                            tree,
                            b_point,
                            collision_search_rad,
                            temp ) ) {
		  
		  goto cleanup_particle_and_edges;
                }
                particle_interaction_cost_new+=tmp;

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       *(edge_backup.pointA->point),
                                       collision_search_rad,
                                       temp,
                                       &candA,false,
                                       -10,mode  // do not collect
                                   ) );
                particle_interaction_cost_new+=tmp;

                sta_assert_error ( !collision_fun_p.colliding (
                                       tmp,
                                       tree,
                                       *(edge_backup.pointB->point),
                                       collision_search_rad,
                                       temp,
                                       &candB,false,
                                       -10,mode  // do not collect
                                   ) );
                particle_interaction_cost_new+=tmp;
		
		///NOTE in case of soft bifurcation collision, we need to make the new track aprticle visible 
		if (options.bifurcation_particle_soft)	
		{
		  if (tree.insert(b_point))
		  {
		    goto cleanup_particle_and_edges;
		  }
		  if (collision_fun_p.colliding(tmp,
						tree,
					      bif_center_point,
					      collision_search_rad,
					      temp))
		  {
		    goto cleanup_particle_and_edges;
		  }
		
		  particle_interaction_cost_new+=tmp;	 	
		}
		

            }	  
	  
	  
	  
	  
	
	 { 
	  
	 
	  T bifurcation_data_new=0;  
	 
	  
	  
	  if (!bif_center_point.compute_data_term(data_fun,bifurcation_data_new))
	  {
	    goto cleanup_particle_and_edges;
	  }
	  
	  bif_center_point.point_cost=bifurcation_data_new;
	   
	   
	   
	T E=(new_edgecost-old_edgecost)/temp;
	  E+=(b_point.point_cost)/temp;
	  E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
// 	  E+=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;
	  E+=(bifurcation_data_new)/temp;

        T R=mhs_fast_math<T>::mexp(E);
	  
	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()*3); 
	connection_probability*=1.0/(connections.pool->get_numpts()-2);
	
	
	R*=remove_connection_prob/((connection_probability)+std::numeric_limits<T>::epsilon());
	
	
	    if ( consider_birth_death_ratio ) {
	    if ( use_saliency_map ) {
		R*= ( lambda ) *this->call_w_undo;
		R/=std::numeric_limits<T>::epsilon() + ( tree.get_numpts() +1 ) * ( nvoxel*b_point.saliency ) *this->call_w_do; //*cpoint.saliency;
	    } else {
		R*= ( lambda ) *this->call_w_undo;
		R/=std::numeric_limits<T>::epsilon() + ( tree.get_numpts() +1 ) *this->call_w_do; //*cpoint.saliency;
	    }
	}
//4	  
statistic[stat_count++]++;	

	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	    
		sta_assert_error(!p_point->has_owner(0));
		sta_assert_error(!p_bif_center_point->has_owner(0));
	    
	    	if (options.no_double_bifurcations)
		{
		    if (bif_center_point.connects_two_bif())
		    {
			goto cleanup_particle_and_edges;
		    }
		}
	    
// 	    bif_center_point.bifurcation_center_update();
// 	    bool out_of_bounce=false;
// 	      bif_center_point.bifurcation_center_update(shape,out_of_bounce);
// 	      if (out_of_bounce)
// 	      {
// 		  goto cleanup_particle_and_edges;		  
// 		}
	
	      if (options.bifurcation_particle_hard)	
	      {
		if ((!tree.insert(*p_point))||(collision_fun_p.colliding( tree,bif_center_point,collision_search_rad,temp)))
		{
		  goto cleanup_particle_and_edges;
		}
	      }

		if (options.bifurcation_particle_hard || options.bifurcation_particle_soft)	
		{
		 // printf("options.bifurcation_particle_hard\n");
		 if(!tree.insert(*p_bif_center_point))
		 {
 		   goto cleanup_particle_and_edges;
		 } 
		}
		//FIXME else temprarily disabled (and again enabled because of )
		// ssertion "p.vox_grid_data[grid_id].owner==NULL" failed (line 442 in /media/skibbe-h/b48e79ba-cb15-43b0-adb7-e8f408e6ba78/ext_projects/neuro_tracker/ntracker_c/voxgrid.h)
		// proposal B_TBIRTH died!!
		else // probably incorrect? I completely forgot why?
		{
		  sta_assert_debug0(&b_point==p_point);
		  if (options.bifurcation_particle_soft)	
		  {
		    b_point.has_owner(voxgrid_default_id);
		  }else
		  {
		    if(!tree.insert(*p_point))
		    { 
		      goto cleanup_particle_and_edges;
		    } 
		  }
		}
		
		if ((!conn_tree.insert(*p_point))||(!bifurcation_tree.insert(*p_bif_center_point)))
		{
		    goto cleanup_particle_and_edges;
		}	
		
		
		this->tracker->update_energy(E+particle_energy_change_costs,static_cast<int>(get_type()));
		this->proposal_accepted++;	
		
		#ifdef  D_USE_GUI
		  p_bif_center_point->touch(get_type());
		  for (int i=0;i<3;i++)
		  {
		      ((p_bif_center_point->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point))->touch(get_type());
		  }
		#endif	
		
		this->tracker->update_volume(p_point->get_scale());
		
		int new_id=(p_bif_center_point->endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->point->_path_id);
		p_point->_path_id=new_id;
		p_bif_center_point->_path_id=new_id;
		
	    return true;
	  }
	 }
	}
//5	  
statistic[stat_count++]++;		  
	
	    cleanup_particle_and_edges:	
	    
	    sta_assert_debug0(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	    connections.remove_connection(new_connections[0]);
	    connections.remove_connection(new_connections[1]);
	    connections.remove_connection(new_connections[2]);
	    
	    cleanup_particle_and_edges_partial:	
	    connections.add_connection(edge_backup);
	    particle_pool.delete_obj(p_point);
	    particle_pool.delete_obj(p_bif_center_point);
	    return false;
	    
	     
	    
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
        
        
  
    }
};





template<typename TData,typename T,int Dim>
class CProposalBifurcationTrackDeath : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
      const static int max_stat=10;
    int statistic[max_stat];
     T particle_energy_change_costs;
      T lambda;
      bool temp_dependent;
      
              bool use_saliency_map;
      bool consider_birth_death_ratio;
    bool sample_scale_temp_dependent;
     
public:

T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
            return CProposal<TData,T,Dim>::dynamic_weight_edges(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }


    CProposalBifurcationTrackDeath(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
	 lambda=100;
	particle_energy_change_costs=update_energy_particle_costs(lambda);
	temp_dependent=true;
	
	     use_saliency_map=false;
        consider_birth_death_ratio=false;
        sample_scale_temp_dependent=false;
    }
    
      ~CProposalBifurcationTrackDeath()
    {
//         printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("%d ",statistic[i]);
// 	printf("\n");
	
    }
    
    T get_default_weight(){return 1;};

    std::string get_name() {
       return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH);
    };
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH);
    }

    void set_params(const mxArray * params=NULL)
    {
       sta_assert_error(this->tracker!=NULL);

        std::size_t & nvoxel=this->tracker->numvoxel;
        lambda=nvoxel;
      particle_energy_change_costs=update_energy_particle_costs(lambda);
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	
	if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	
  if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
  
   if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }

  
  
  particle_energy_change_costs=update_energy_particle_costs(lambda);
    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);
	
int stat_count=0;
//0
statistic[stat_count++]++;		
	
	this->proposal_called++;
	
	/// no terminals?
	if (connections.get_num_bifurcation_centers()<1)
	  return false;
	
	try
	{
	  /// randomly pic a bifurcation node
	  Points<T,Dim> &  center_point=connections.get_rand_bifurcation_center_point();	  
	  
	  /// check if point is a bifurcation! 
	  sta_assert_debug0(center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	  


	  int remove_node=std::rand()%3;
	  Points<T,Dim> &  remove_point=*center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_node]->connected->point;
	  
	  if (remove_point.isfreezed())
            {
		return false;
	    }
	    
	    
	      #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_TRCK
			GuiPoints<T> * gps= ( ( GuiPoints<T> * ) &remove_point) ;
	    #endif
	    #endif

	    
	    
	    
	    
// 	    if (remove_point.is_protected_topology())
// 	    {
// 	      return false;
// 	    }
	  
	  // if terminal there is exactly one side with a connection
	  const int side=(remove_point.endpoint_connections[0]==1) ? 0 : 1;	
	  
	  
	  ///check if bifurcation terminal
	  if (remove_point.endpoint_connections[1-side]!=0)
	    return false;
	  
	  
	  if (sample_scale_temp_dependent && ((temp>remove_point.get_scale())))
	  {
		return false; 
	  }
	  
	  
	  class Connection< T, Dim > edge_backup[3];
	class Connection< T, Dim > * p_edge_backup[3];
	p_edge_backup[0]=(center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection);
	p_edge_backup[1]=(center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connection);
	p_edge_backup[2]=(center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connection);
	
	
	if (p_edge_backup[remove_node]->is_protected_topology())
	  return false;
	
	
	
	  
// 	  if (remove_point.endpoints[side][0]->connection->is_protected_topology())
// 	  {
// 	    return false;
// 	  }
	  
	  
// 	T particle_interaction_cost_old=0;
// 	if (collision_fun_p.is_soft())    
// 	{
// 	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,remove_point,collision_search_rad,temp));
// 	  
// 	}  
	
	  
	//determine all involved endpoints
	
	class Points<T,Dim>::CEndpoint * endpoints[3];
	endpoints[0]=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected;
	endpoints[1]=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected;
	endpoints[2]=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected;
	
	
	sta_assert_debug0(endpoints[0]->point!=endpoints[1]->point);
	sta_assert_debug0(endpoints[0]->point!=endpoints[2]->point);
	sta_assert_debug0(endpoints[2]->point!=endpoints[1]->point);
	
	sta_assert_debug0(endpoints[0]->endpoint_connections[0]==1);
	sta_assert_debug0(endpoints[1]->endpoint_connections[0]==1);
	sta_assert_debug0(endpoints[2]->endpoint_connections[0]==1);
	
	
	
	 T particle_costs=0;
	particle_costs-=endpoints[0]->point->compute_cost3(temp); //old bif points
	particle_costs-=endpoints[1]->point->compute_cost3(temp); 
	particle_costs-=endpoints[2]->point->compute_cost3(temp); 
	
	
	
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	//computing edge costs of the OLD configuration
	T old_edgecost=center_point.e_cost(
	   edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange
	);
	
	 if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	      {
		///NOTE in case of time dependent thickness, the triangle may become invalid
		/// in this case we asing a very high cost to the triangle
	        sta_assert_error( ( inrange == CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_AREA));

	      }
// 	sta_assert_debug0(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	
	
	
	
	class Connection< T, Dim > new_connections;
	new_connections.edge_type=EDGE_TYPES::EDGE_SEGMENT;
	class Connection< T, Dim > * p_new_connections;
	int j=0;
	int protected_topology=0;
	for (int i=0;i<3;i++)
	{
	  if (i!=remove_node)
	  {

	    protected_topology+=(p_edge_backup[i]->is_protected_topology());
	    new_connections.points[j++]=center_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected;
	  }
	}
	
	sta_assert_error((protected_topology!=1));
	new_connections.freeze_topology=(protected_topology==2);
	
	
	


        T particle_interaction_cost_old=0;
        T particle_interaction_cost_new=0;



        class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
        class Collision<T,Dim>::Candidates candA ( query_bufferA );

        class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
        class Collision<T,Dim>::Candidates candB ( query_bufferB );


        int mode=1; // only special collision costs
        
   	//############################
        // before removing edges
        //############################

        if ( collision_fun_p.is_soft() ) {

            T tmp;
            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   remove_point,
                                   collision_search_rad,
                                   temp ) );

            particle_interaction_cost_old+=tmp;

	    //NOTE we hide the track_pt point from the tree
	    
	    
            std::size_t  npts_old=tree.get_numpts();
            remove_point.unregister_from_grid (voxgrid_default_id);
            std::size_t  npts_new=tree.get_numpts();
            sta_assert_debug0 ( npts_new+1==npts_old );
	    
	    
	    
	    if (options.bifurcation_particle_soft)	
	    {
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					    tree,
					  center_point,
					  collision_search_rad,
					  temp));
	      
	      sta_assert_error(center_point.has_owner(voxgrid_default_id));
	      //NOTE we hide the bif point from the tree
	      std::size_t  npts_old=tree.get_numpts();
	      center_point.unregister_from_grid (voxgrid_default_id);
	      std::size_t  npts_new=tree.get_numpts();
	      sta_assert_debug0 ( npts_new+1==npts_old );
	    
	      particle_interaction_cost_old+=tmp;	 	
	    }


            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                    *(new_connections.pointA->point),
                                   collision_search_rad,
                                   temp,
                                   &candA,true,
                                   -9,mode  // collect colliding pts
                               ) );
            particle_interaction_cost_old+=tmp;

            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                    *(new_connections.pointB->point),
                                   collision_search_rad,
                                   temp,
                                   &candB,true,
                                   -9,mode  // collect colliding pts
                               ) );
            particle_interaction_cost_old+=tmp;
        }
	
	

	

	
	for (int i=0;i<3;i++)
	{
	  
	  
	  edge_backup[i]=*p_edge_backup[i];
	  connections.remove_connection(p_edge_backup[i]);
	}
	
	
	p_new_connections=connections.add_connection(new_connections);
	
	T new_edgecost=p_new_connections->e_cost(
	   edgecost_fun,
	  options.connection_bonus_L,
	  options.bifurcation_bonus_L,
	  inrange
	);
	
	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	{
	    goto cleanup_particle_and_edges;
	}
	
	{
	
	 if ( collision_fun_p.is_soft() ) {
            T tmp;
            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   *(new_connections.pointA->point),
                                   collision_search_rad,
                                   temp,
                                   &candA,false,
                                   -10,mode  // do not collect
                               ) );
            particle_interaction_cost_new+=tmp;

            sta_assert_error ( !collision_fun_p.colliding (
                                   tmp,
                                   tree,
                                   *(new_connections.pointB->point),
                                   collision_search_rad,
                                   temp,
                                   &candB,false,
                                   -10,mode  // do not collect
                               ) );
            particle_interaction_cost_new+=tmp;
        }  
	
	
	
	class Points<T,Dim>::CEndpoint ** remaining_connection_endpoints=p_new_connections->points;
	const Vector<T,Dim> & centerA=(remaining_connection_endpoints[0]->point->get_position());
	const Vector<T,Dim> & centerB=(remaining_connection_endpoints[1]->point->get_position());
	  
// 	  T temp_fact_pos=std::sqrt(temp);
// 	  T temp_fact_scale=temp_fact_pos+1;
// 	  T & temp_fact_rot=temp_fact_pos;
	  
	  const T & scaleA=remaining_connection_endpoints[0]->point->get_scale();
	  const T & scaleB=remaining_connection_endpoints[1]->point->get_scale();
	  
// 	  endpoints[0]->point->update_endpoint(endpoints[0]->side);
// 	  endpoints[1]->point->update_endpoint(endpoints[1]->side);
// 	  endpoints[2]->point->update_endpoint(endpoints[2]->side);
// 	  const Vector<T,Dim> & centerA=(endpoints[1]->position);
// 	  const Vector<T,Dim> & centerB=(endpoints[2]->position);
	  
	  
	  Vector<T,Dim> n[3];
	  n[0]=(*endpoints[2]->position-*endpoints[1]->position);
	  T n_length=std::sqrt(n[0].norm2());
	  n[0]/=n_length+std::numeric_limits<T>::epsilon();
	  mhs_graphics::createOrths(n[0],n[1],n[2]);
	  
  
	  
	  T  a=CEdgecost<T,Dim>::side_scale*scaleA;
	  T  b=CEdgecost<T,Dim>::side_scale*scaleB;
	  T  c=CEdgecost<T,Dim>::side_scale*remove_point.get_scale();
	  
	  T w=(b*b+c*c-a*a)/(2*b*c);
	  sta_assert_debug0(!(std::abs(w)>1));
	  Vector<T,2> P1;
	  P1[0]=c;
	  P1[1]=0;
	  Vector<T,2> P2;
	  P2[0]=b*w;
	  P2[1]=b*std::sqrt(1-w*w);
	  
	  Vector<T,2> P[3];
	  P[0]=(P1+P2)/2;
	  P[1]=P2/2;
	  P[2]=P1/2;
	  
	  Vector<T,2> n_=P[1]-P[0];
	  n_.normalize();
	  Vector<T,2> v_=(P[2]-P[0]);
	  
	  // optimal height:
	  T h_rel_offset=n_.dot(v_);
	  T h=std::sqrt(((P[2]-P[0])-n_*h_rel_offset).norm2());
	  // relative offset between P[2] and P[1]
	  h_rel_offset/=std::sqrt(v_.norm2());
	  
// 	  T h_tmp_length_fact=std::sqrt(temp);//temp_fact_pos;
	    T h_tmp_length_fact=(scaleA+scaleB)/2;
	  if (temp_dependent)
	    h_tmp_length_fact=0.5*(h_tmp_length_fact+proposal_temp_trafo(temp,h_tmp_length_fact,T(1)));
// 	    h_tmp_length_fact=proposal_temp_trafo(temp,h_tmp_length_fact);
	     //h_tmp_length_fact=std::sqrt(temp);
	  
	  
	  h*=1+h_tmp_length_fact;
	  
// 	  T h=std::sqrt(((P[2]-P[0])-n_*n_.dot(v_)).norm2());
	  
	  T sign=(Points<T,Dim>::thickness<0) ? -1 : 1 ; 
	  T lC=sign*Points<T,Dim>::thickness*remove_point.get_scale();
	  h+=lC;
	  
	  Vector<T,Dim> suggested_direction;
	  suggested_direction=endpoints[0]->position-endpoints[1]->position;
	  suggested_direction=suggested_direction-n[0]*suggested_direction.dot(n[0]);
	  suggested_direction.normalize();

	  
	  // computing position of orbit point in image space
	  Vector<T,Dim> suggested_point=centerA;
	  suggested_point+=n[0]*(h_rel_offset*n_length)+suggested_direction*(h);
	  

	  //T sigma=(scaleA+scaleB)/4;//+n_length;
	  T sigma_r=0.25;
	  T connection_probability=1;
	  
	  suggested_point-=(*endpoints[0]->position); 
	  //connection_probability*=normal_dist<T,3>(suggested_point.v,sigma);
	  
	  T sigma_h=h_tmp_length_fact*h/2;
	  T sigma_h2=(scaleA+scaleB)/4;//+n_length;
	  T noise_pos[3];
	  noise_pos[0]=suggested_direction.dot(suggested_point);
	  noise_pos[1]=n[0].dot(suggested_point);
	  noise_pos[2]=(n[0].cross(suggested_direction)).dot(suggested_point);
		      
	  connection_probability*=normal_dist<T>(noise_pos[0],
					      noise_pos[1],
					      noise_pos[2],
					      sigma_h,
					      sigma_h2,
					      sigma_h2);
	  sta_assert_error_m(!(connection_probability<0),connection_probability);
	  sta_assert_error_m(!(connection_probability>1),connection_probability);
	  
	  
	  connection_probability*=normal_dist_sphere(suggested_direction,remove_point.get_direction(),sigma_r);	  
// 	  connection_probability*=normal_dist_sphere(suggested_direction,remove_point.direction,temp_fact_rot);	  
	  connection_probability/=2*M_PI;


	  particle_costs+=p_new_connections->pointA->point->compute_cost3(temp);
	  particle_costs+=p_new_connections->pointB->point->compute_cost3(temp);
	  

	  T bifurcation_data_old=center_point.point_cost;
	  
	T E=(new_edgecost-old_edgecost)/temp;
	  
	  E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	  
// 	  E-=(terminal_point_cost_change)/temp;
	  E+=(particle_costs)/temp;
	  
	  E-=(bifurcation_data_old)/temp;
	  E-=(remove_point.point_cost)/temp;

        T R=mhs_fast_math<T>::mexp(E);
	  
// 	T add_connection_prob=connection_probability/(connections.pool->get_numpts()); 
// 	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()+1);
// 	R*=add_connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());
	
	//Aug:
	T add_connection_prob=connection_probability/(connections.pool->get_numpts()); 
	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()*3);
	R*=add_connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());
	

	
	 if ( consider_birth_death_ratio ) {
            if ( use_saliency_map ) {
                R*= ( tree.get_numpts() + ( collision_fun_p.is_soft() ) ) * ( nvoxel*remove_point.saliency ) *this->call_w_undo;
                R/=std::numeric_limits<T>::epsilon() + ( lambda ) *this->call_w_do;
            } else {
                R*= ( tree.get_numpts() + ( collision_fun_p.is_soft() ) ) *this->call_w_undo;
                R/= ( lambda ) *this->call_w_do;
            }
        }

	
	    
	//birth:
// 	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()*3); 
// 	connection_probability*=1.0/(connections.pool->get_numpts()-2);
// 	R*=remove_connection_prob/((connection_probability)+std::numeric_limits<T>::epsilon());

	
//1	
statistic[stat_count++]++;			
	
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {

	       #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_TRCK
			 if ( gps->is_selected )
			 {
			    printf("BIF BIRTH: EC (%f => %f)? PC (%f BD (%f) IC(%f => %f) R (%f/%f)\n ",
			      old_edgecost,new_edgecost,
			      particle_costs,bifurcation_data_old,
			      particle_interaction_cost_old,particle_interaction_cost_new,
			      add_connection_prob/remove_connection_prob
			    );
			 }
	    #endif
	    #endif
	    
	    
		this->tracker->update_energy(E-particle_energy_change_costs,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		#ifdef  D_USE_GUI
		  new_connections.pointA->point->touch(get_type());
		  new_connections.pointB->point->touch(get_type());
		#endif	
		
		
		Points< T, Dim > * p_center_point=&center_point;
		particle_pool.delete_obj(p_center_point);
		Points< T, Dim > * p_remove_point=&remove_point;
		this->tracker->update_volume(-p_remove_point->get_scale());
		particle_pool.delete_obj(p_remove_point);
	    return true;
	  }
	}

//2	  
statistic[stat_count++]++;			  
	  
	    cleanup_particle_and_edges:	
	    
	      if ( collision_fun_p.is_soft() ) {
		sta_assert_error ( tree.insert ( remove_point ) );
		
		if (options.bifurcation_particle_soft)	
		{
		  sta_assert_error ( tree.insert ( center_point ) );
		}
		
	      }
	 
	    connections.remove_connection(p_new_connections);
	    
	    for (int i=0;i<3;i++)
	      connections.add_connection(edge_backup[i]);
	    bool out_of_bounce=false;
	      center_point.bifurcation_center_update(shape,out_of_bounce);
	      sta_assert_debug0(!out_of_bounce);
	    return false;
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }
      
    }
};



#endif
