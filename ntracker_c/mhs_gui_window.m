%handle=mhs_gui_window();
%all_handles=mhs_gui_window('handles');
%mhs_gui_window('handle',handle,'shape',[800,600])
%mhs_gui_window('close',handle)
%mhs_gui_window('closeall')

function handle=mhs_gui_window(varargin)


persistent warnme;

if isempty(warnme)
    warnme=1;
    warning('using gl windows will very likely cause a segfault when -> exiting <- matlab. If you want to help fixing this: setting keep_last_window_alive=false; in mhs_gui_create_window_new.cc  will cause the segfault at runtime after creating and closeing a window (the last gl_window). I guess SDL and matlab hate each other :-(.');
end;


if (nargin==1) 
    switch (varargin{1})
        case  'closeall'
        %mhs_gui_create_window_new({'closeall',1});
        handles=mhs_gui_create_window_new({'handles',1});
        for h=1:numel(handles)
            mhs_gui_create_window_new({'close',handles(h)});    
        end;
        
        case  'handles'
        handle=mhs_gui_create_window_new({'handles',1});
    end;
else

    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;

    if exist('handle','var')

     if exist('shape','var')
        mhs_gui_create_window_new({'handle',handle,'w',shape(1),'h',shape(2)});
     else
        fprintf('closing window\n'); 
        mhs_gui_create_window_new({'close',handle});
     end;

    else 
       
       handle=mhs_gui_create_window_new();
       if exist('shape','var')
        mhs_gui_create_window_new({'handle',handle,'w',shape(1),'h',shape(2)});
       end;
       
    end;

end;
