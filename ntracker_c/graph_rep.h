#ifndef GRAPH_REPRESENTATION_H
#define GRAPH_REPRESENTATION_H

#include "particles.h"

#include "proposals.h"

#include "energy.h"
#include "constraints.h"
#include "edgecost.h"

/*
#include "mhs_data_sfilter.h"
#include "mhs_data_hough.h"
#include "mhs_data_medialness.h"
#include "mhs_data_hessian.h"*/


#ifdef D_USE_GUI
#include "mhs_observer.h"
#endif

#ifndef _OCTAVE_
#include "mat.h"
#endif




// const char *field_names[] = {
//     "data",			// particle data
//     "connections",		// connection data
//     "opt_temp",			// current temp state
//     "opt_temp_conn_cost",	// current connection temp state
//     "state_eval_state",		// current evaluation counter state
//     "state_iteration_time",	// time for the current run
//     "state_iteration_time_total",// total computation time
//     "state_iteration_total_num",	// total number of iterations
//     "state_energy_grad_update_counter",  // number of grad updates ignored before updating temp
//     "particle_thickness",	// particle thickness
//     "state_energy_grad",		// energy gradient statistic state current
//     "state_energy_grad_old",		// energy gradient statistic state previous
//     "scale_correction",		// scale correction
//     "offline",		// scale correction
//     "state_movie_frame",
//     "state_particle_number_activity",
//     "state_particle_number_activity_max",
//     "state_particle_number_activity_count0",
//     "state_particle_number_activity_count1",
// };
const char *field_names[] = {
    "data",			// particle data
    "connections",		// connection data
    "opt_temp",			// current temp state
    "opt_temp_conn_cost",	// current connection temp state
    "particle_thickness",	// particle thickness
    "scale_correction",
    "offline",		// scale correction
    "state",
};


const std::size_t nfield_names= ( sizeof ( field_names ) / sizeof ( field_names[ 0 ] ) );;





template<typename TData,typename T,int Dim>
class CTracker
{


public:
    enum  POINT_ATTRIBUTES : std::size_t {
        POINT_A_POSZ=0,
        POINT_A_POSY=1,
        POINT_A_POSX=2,
        POINT_A_DIRZ=3,
        POINT_A_DIRY=4,
        POINT_A_DIRX=5,
        POINT_A_BIRTH_FLAG=6,
        POINT_A_SCALE=7,
        POINT_A_ENDPOINT_A_POSZ=8,
        POINT_A_ENDPOINT_A_POSY=9,
        POINT_A_ENDPOINT_A_POSX=10,
        POINT_A_ENDPOINT_B_POSZ=11,
        POINT_A_ENDPOINT_B_POSY=12,
        POINT_A_ENDPOINT_B_POSX=13,
        POINT_A_COST=14,
        POINT_A_SALIENCY=15,
        POINT_A_TYPE=16,
	POINT_A_DISTANCE=17, // used  to store distance values for spines (only used for visualization)
        POINT_A_NUMCONNECTION_A=18,
        POINT_A_NUMCONNECTION_B=19,
        POINT_A_PATH_ID=20,
        POINT_A_CONNECTED_FLAG=21,
        POINT_A_FREEZED=22,
        POINT_A_FREEZED_ENDPOINTS_A=23,
        POINT_A_FREEZED_ENDPOINTS_B=24,
	POINT_A_PROTECTED_TOPOLOGY=25,
        POINT_A_Count
    };


    enum  EDGE_ATTRIBUTES : std::size_t {
        EDGE_A_PINDXA=0,
        EDGE_A_PINDXB=1,
        EDGE_A_SIDEA=2,
        EDGE_A_SIDEB=3,
        EDGE_A_TYPE=4,
	EDGE_A_PROTECTED_TOPOLOGY=5,
        EDGE_A_Count
    };

    enum  DEBUG_COMMAND : std::size_t {
        DEBUG_NOTHING=0,
        DEBUG_CLEAR_ALL=1,
        DEBUG_ADD_PARTICLE=2,
        DEBUG_COUNT
    };

    enum  COOL_DOWN_SCHEDULER : std::size_t {
        COOL_DOWN_BIRTH_DEATH=0,
        COOL_DOWN_ITERATION=1
    };


    class User_action
    {
    public:
        DEBUG_COMMAND command_type;
        User_action() : command_type ( DEBUG_COMMAND::DEBUG_NOTHING ) {};
    };

    class User_action_clear : public User_action
    {
    public:
        User_action_clear() {
            this->command_type= ( DEBUG_COMMAND::DEBUG_CLEAR_ALL );
        };
    };

    class User_action_add_particle : public User_action
    {
    public:
        User_action_add_particle() {
            this->command_type= ( DEBUG_COMMAND::DEBUG_ADD_PARTICLE );
        };
        Vector<T,3> position;
    };


protected:

    friend class CProposal<TData,T,Dim>;
#include "proposal_friends.inc"


#ifdef D_USE_GUI
    friend class CTrackerRenderer<TData,T,Dim>;
    CTrackerRenderer<TData,T,Dim> * renderer;
#ifndef OLD_VIEWER
    friend class MyMouse<TData,T,Dim>;

#endif
#endif

public:
#ifndef _D_USE_GUI

    void stop_tracking() {
        terminate=true;
    };


    void print_info ( int thread_id ) {

        printf ( "(%d) [T %1.3f] p:%u c:%u t:%u b:%u] (%s) [s:%u , %f]\n",
                 thread_id,
                 opt_temp,
                 tree->get_numpts(),
                 connections.pool->get_numpts(),
                 connections.num_terminals,
                 connections.num_bifurcation_center_points,
                 ( active_scheduler==COOL_DOWN_SCHEDULER::COOL_DOWN_BIRTH_DEATH ) ? "bd" : "i",
		 get_num_graph_segments(),
		 volume/(shape[0]*shape[1]*shape[2])
               );

    }
#endif



    class CStats
    {
    private:
        std::size_t stat_hist_size;
        std::size_t current_index;
        bool fresh;
        T * stats;
        T stat_min;
        T stat_max;
        bool nroamlized;
    public:
        CStats ( std::size_t  stat_hist_size, bool n ) {
            stats=NULL;
            init ( stat_hist_size, n );
        }

        void init ( std::size_t  stat_hist_size, bool n ) {
            if ( stats!=NULL ) {
                delete [] stats;
            }
            stat_hist_size+=stat_hist_size%2;
            this->stat_hist_size=stat_hist_size;
            stats=new T[stat_hist_size];
            memset ( stats,0,sizeof ( T ) *stat_hist_size );
            stat_min=std::numeric_limits<T>::max();
            stat_max=-std::numeric_limits<T>::max();
            current_index=0;
            nroamlized=n;
            fresh=true;
        }

        CStats() {
            stats=NULL;
        }

        ~CStats() {
            if ( stats!=NULL ) {
                delete [] stats;
            }
        }

        std::size_t get_hist_size() const {
            return stat_hist_size;
        };
        std::size_t get_hist_pos() const {
            return current_index;
        };
        const T *  get_hist() const {
            return stats;
        };

        void  add ( T value, bool rel=false ) {
            if ( fresh ) {
                clear ( value );
                fresh=false;
            }

            //std::size_t previous_index=(stat_hist_size+current_index-1)%stat_hist_size;

            if ( rel ) {
                stats[current_index]=stats[ ( current_index-1+stat_hist_size ) %stat_hist_size]-value;
            } else {
                stats[current_index]=value;
            }

            current_index++;
            current_index%=stat_hist_size;

// 	  bool do_update=false;
// 	  if (stat_min<value)
// 	  {
// 	    do_update=true;
// 	  } else
// 	  {
// 	      stat_min=value;
// 	  }
//
// 	  if (stat_max>value)
// 	  {
// 	    do_update=true;
// 	  } else
// 	  {
// 	      stat_max=value;
// 	  }
//
// 	  if (do_update)
            if ( nroamlized ) {
                stat_min=std::numeric_limits<T>::max();
                stat_max=-std::numeric_limits<T>::max();
                for ( std::size_t i=0; i<stat_hist_size; i++ ) {
                    stat_min=std::min ( stats[i],stat_min );
                    stat_max=std::max ( stats[i],stat_max );
                }
            }
        };

        void clear ( T v=0 ) {
            if ( stats==NULL ) {
                return;
            }

            fresh=true;

            for ( std::size_t i=0; i<stat_hist_size; i++ ) {
                stats[i]=v;
            }
            stat_min=std::numeric_limits<T>::max();
            stat_max=-std::numeric_limits<T>::max();
            current_index=0;

        }

        void print_hist ( float t, float l,float w, float h,float level ) {
            if ( stats==NULL ) {
                return;
            }

#ifdef D_USE_GUI

            float p_min=0;
            float p_max=1;
            if ( nroamlized ) {
                p_min=std::min ( float ( stat_min ),level );
                p_max=std::max ( float ( stat_max ),level )-p_min+std::numeric_limits< T>::epsilon();
            }



            //opengl
            glBegin ( GL_LINE_STRIP );
            for ( std::size_t i=0; i<stat_hist_size; i++ ) {
                std::size_t indx= ( current_index+i ) %stat_hist_size;
                float v= ( stats[indx]-p_min ) / ( p_max );
                //glVertex2f(l+(w*i)/stat_hist_size,t+h-v*h);
                glVertex2f ( l+ ( w*i ) /stat_hist_size,t+v*h );
            }
            glEnd();

            glEnable ( GL_LINE_STIPPLE );
            glLineStipple ( 1, ( short ) 0x00FF );
            glBegin ( GL_LINES );
            //if ((level>stat_min)&&(level<stat_max))
            {
                float v= ( level-p_min ) / ( p_max );
// 		glVertex2f(l+(w*0)/stat_hist_size,t+h-v*h);
// 		glVertex2f(l+(w*1)/stat_hist_size,t+h-v*h);
                glVertex2f ( l,t+v*h );
                glVertex2f ( l+w,t+v*h );
            }
// 			      glColor3fv(particle_colors[CSceneRenderer::Corange]);
// 			      glVertex3f(front,focus_point[1],focus_point[2]);
// 			      glVertex3f(back,focus_point[1],focus_point[2]);
// 			      glVertex3f(focus_point[0],top,focus_point[2]);
// 			      glVertex3f(focus_point[0],bottom,focus_point[2]);
// 			      glVertex3f(focus_point[0],focus_point[1],right);
// 			      glVertex3f(focus_point[0],focus_point[1],left);
            glEnd();


            glLineStipple ( 1, ( short ) 0xAAAA );
            glColor3f ( 1,1,1 );
            glBegin ( GL_LINE_STRIP );
            glVertex2f ( l,t );
            glVertex2f ( l,t+h );
            glVertex2f ( l+w,t+h );
            glVertex2f ( l+w,t );
            glVertex2f ( l,t );
            glEnd();

            glLineStipple ( 1, ( short ) 0xFFFF );
            glDisable ( GL_LINE_STIPPLE );

#else
            //print to console


#endif
        }


    };


    class COptions
    {
    public:
        // number of iterations before returning to matlab
        std::size_t opt_numiterations;
	
        // weight for the energy gradient response filter
        T opt_alpha;
        T opt_grid_spacing;

        int  constraint_loop_depth;
//         bool constraint_isdirectedge;

	
	bool opt_spawn_blobs;

//         T no_collision_threshold;


	double min_temp;

	bool bifurcation_particle_soft;
        bool bifurcation_particle_hard;
        bool bifurcation_neighbors;

	bool time_dependent_scale;  
	
//         std::vector<T> particle_connection_state_cost_scale;
//         std::vector<T> particle_connection_state_cost_offset;

	
	bool dynamic_weights;
	
        T connection_bonus_L;
        T bifurcation_bonus_L;
        T connection_candidate_searchrad;
        T linear_cool_down_threshold;

        std::string opt_movie_folder;
        bool opt_create_movie;
	
	T cooldown_threshold;
	
	bool no_double_bifurcations;

	T opt_particles_per_voxel;
        
         bool fixed_cooldown;
	
	
	bool profile;

        COptions() {

	    profile = false;
	    opt_particles_per_voxel=1;
            opt_create_movie=false;
            opt_numiterations=1000000;
	  
            opt_alpha=0.99;
            opt_grid_spacing=-1;
	    dynamic_weights=false;
	    
	    no_double_bifurcations=true;
	    
	    min_temp=T(0.00001);

	    opt_spawn_blobs=false;

            //must be at least 3, otherwise the inser death can create
            //a double connection out of a triagnle
            //constraint_loop_depth=-1;
            constraint_loop_depth=3;
	    
	    cooldown_threshold=0.01;


//             constraint_isdirectedge=true;


            bifurcation_particle_hard=false;
	    bifurcation_particle_soft=false;
            bifurcation_neighbors=false;
	    
	    
	    time_dependent_scale=false;
            
            fixed_cooldown=false;
	    
	    
//             no_collision_threshold=-2;


//             particle_connection_state_cost_scale.resize(5);
//             particle_connection_state_cost_scale[0]
// 	      =particle_connection_state_cost_scale[1]
// 	      =particle_connection_state_cost_scale[2]
// 	      =particle_connection_state_cost_scale[3]
// 	      =particle_connection_state_cost_scale[4]=1;
//
//             particle_connection_state_cost_offset.resize(5);
//             particle_connection_state_cost_offset[0]
// 	      =particle_connection_state_cost_offset[1]
// 	      =particle_connection_state_cost_offset[2]
// 	      =particle_connection_state_cost_offset[3]
// 	      =particle_connection_state_cost_offset[4]=0;

            connection_bonus_L=10;
            bifurcation_bonus_L=connection_bonus_L* ( 3.0/2.0 );
            connection_candidate_searchrad=5;

            linear_cool_down_threshold=0.25;
        }

        void print() {

        }

        void set_params ( const mxArray * params ) {
            if ( params==NULL ) {
                return;
            }


            try {

                /*######################################################
                 *
                 * 	Optimizer related options
                 *
                 *####################################################*/

                if ( mhs::mex_hasParam ( params,"opt_numiterations" ) !=-1 ) {
                    opt_numiterations=mhs::mex_getParam<std::size_t> ( params,"opt_numiterations",1 ) [0];
                }
                //opt_numiterations=mhs::mex_getParam<uint64_t>(params,"opt_numiterations",1)[0];

                if ( mhs::mex_hasParam ( params,"opt_alpha" ) !=-1 ) {
                    opt_alpha=mhs::mex_getParam<T> ( params,"opt_alpha",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"custom_grid_spacing" ) !=-1 ) {
                    opt_grid_spacing=mhs::mex_getParam<T> ( params,"custom_grid_spacing",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"cooldown_threshold" ) !=-1 ) {
                    cooldown_threshold=mhs::mex_getParam<T> ( params,"cooldown_threshold",1 ) [0];
                }


                if ( mhs::mex_hasParam ( params,"opt_movie_folder" ) !=-1 ) {
                    opt_movie_folder=mhs::mex_getParamStr ( params,"opt_movie_folder" );
                    opt_create_movie=true;
                }
                
                if ( mhs::mex_hasParam ( params,"dynamic_weights" ) !=-1 ) {
                    dynamic_weights=mhs::mex_getParam<bool> ( params,"dynamic_weights",1 ) [0];
                }
                
                
                if ( mhs::mex_hasParam ( params,"min_temp" ) !=-1 ) {
                    min_temp=mhs::mex_getParam<T> ( params,"min_temp",1 ) [0];
                }
                

                if ( mhs::mex_hasParam ( params,"opt_particles_per_voxel" ) !=-1 ) {
                    opt_particles_per_voxel=mhs::mex_getParam<T> ( params,"opt_particles_per_voxel",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"opt_spawn_blobs" ) !=-1 ) {
                    opt_spawn_blobs=mhs::mex_getParam<bool> ( params,"opt_spawn_blobs",1 ) [0];
                }
                
                
                if ( mhs::mex_hasParam ( params,"time_dependent_scale" ) !=-1 ) {
                    time_dependent_scale=mhs::mex_getParam<bool> ( params,"time_dependent_scale",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"proposal_profile" ) !=-1 ) {
                    profile=mhs::mex_getParam<bool> ( params,"proposal_profile",1 ) [0];
                }
                

                /*######################################################
                 *
                 * 	Special Constraints related options
                 *
                 *####################################################*/

                if ( mhs::mex_hasParam ( params,"constraint_loop_depth" ) !=-1 ) {
                    constraint_loop_depth=mhs::mex_getParam<int> ( params,"constraint_loop_depth",1 ) [0];
                }
                sta_assert_error ( constraint_loop_depth>=3 );

                if ( mhs::mex_hasParam ( params,"bifurcation_particle_hard" ) !=-1 ) {
                    bifurcation_particle_hard=mhs::mex_getParam<bool> ( params,"bifurcation_particle_hard",1 ) [0];
                }
                
                
                if ( mhs::mex_hasParam ( params,"bifurcation_particle_soft" ) !=-1 ) {
                    bifurcation_particle_soft=mhs::mex_getParam<bool> ( params,"bifurcation_particle_soft",1 ) [0];
                }

                
                sta_assert_error(!(bifurcation_particle_hard&&bifurcation_particle_soft));

                if ( mhs::mex_hasParam ( params,"bifurcation_neighbors" ) !=-1 ) {
                    bifurcation_neighbors=mhs::mex_getParam<bool> ( params,"bifurcation_neighbors",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"linear_cool_down_threshold" ) !=-1 ) {
                    linear_cool_down_threshold=mhs::mex_getParam<T> ( params,"linear_cool_down_threshold",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"no_double_bifurcations" ) !=-1 ) {
                    no_double_bifurcations=mhs::mex_getParam<bool> ( params,"no_double_bifurcations",1 ) [0];
                }



                // should always be true
//         if (mhs::mex_hasParam(params,"max_hard_edge_length")!=-1)
//             max_hard_edge_length=mhs::mex_getParam<bool>(params,"max_hard_edge_length",1)[0];


//             if (mhs::mex_hasParam(params,"particle_connection_state_cost_scale")!=-1)
//                 particle_connection_state_cost_scale=mhs::mex_getParam<T>(params,"particle_connection_state_cost_scale",5);
//
//             if (mhs::mex_hasParam(params,"particle_connection_state_cost_offset")!=-1)
//                 particle_connection_state_cost_offset=mhs::mex_getParam<T>(params,"particle_connection_state_cost_offset",5);

                /*######################################################
                 *
                 * 	Edge related options
                 *
                 *####################################################*/

                if ( mhs::mex_hasParam ( params,"connection_candidate_searchrad" ) !=-1 ) {
                    connection_candidate_searchrad=mhs::mex_getParam<T> ( params,"connection_candidate_searchrad",1 ) [0];
                }

                if ( mhs::mex_hasParam ( params,"connection_bonus_L" ) !=-1 ) {
                    connection_bonus_L=mhs::mex_getParam<T> ( params,"connection_bonus_L",1 ) [0];
                }

                bifurcation_bonus_L=connection_bonus_L* ( 3.0/2.0 );
                if ( mhs::mex_hasParam ( params,"bifurcation_bonus_L" ) !=-1 ) {
                    bifurcation_bonus_L=mhs::mex_getParam<T> ( params,"bifurcation_bonus_L",1 ) [0];
                }
                
                if ( mhs::mex_hasParam ( params,"movie_steprate" ) !=-1 ) {
                    movie_steprate=mhs::mex_getParam<std::size_t> ( params,"movie_steprate",1 ) [0];
		    printf("setting the movie steprate to %d\n",movie_steprate);
                }
                
                if ( mhs::mex_hasParam ( params,"fixed_cooldown" ) !=-1 ) {
                    fixed_cooldown=mhs::mex_getParam<bool> ( params,"fixed_cooldown",1 ) [0];
                    if (fixed_cooldown)
                        printf("fixed iteration cooldown\n");
                }
                

//                 if ( mhs::mex_hasParam ( params,"no_collision_threshold" ) !=-1 ) {
//                     no_collision_threshold=mhs::mex_getParam<T> ( params,"no_collision_threshold",1 ) [0];
//                 }

            } catch ( mhs::STAError error ) {
                throw error;
            }
        }

    };

#ifdef D_TRACKER_ALL_PUBLIC
  public:  
#else    
  protected:
#endif
    CTracker ( std::size_t pointlimit=10000000 ) {
        this->pointlimit=pointlimit;
        movie_state_egdge_history=0;
	movie_state_rate=50;
	
	
        movie_state_soft_max=0;
	
	iterations=0;
	  real_iterations=0;

        state_movie_frame=0;
        is_pausing=false;
	
	is_movie_maker=false;
	

        search_connection_point_center_candidate=0;
        maxendpointdist=0;
        num_decrease=0;
        init_state=0;
        state_eval_state=1;
        state_iteration_time_total=0;
        state_iteration_time=0;
        state_iteration_total_num=0;

        state_energy_grad2[0]=state_energy_grad2[1]=state_energy_grad2[2]=0;
        state_energy_grad_old2[0]=state_energy_grad_old2[1]=state_energy_grad_old2[2]=0;
        state_energy_grad_update_counter2[0]=state_energy_grad_update_counter2[1]=state_energy_grad_update_counter2[2]=0;
        state_energy_grad_update_counter_updated2[0]=state_energy_grad_update_counter_updated2[1]=state_energy_grad_update_counter_updated2[2]=false;

	E_lin=0;
	
        state_particle_number_activity=0;
        state_particle_number_activity_max=0;
        state_particle_number_activity_count.resize ( 2 );
        state_particle_number_activity_count[0]=state_particle_number_activity_count[1]=0;


        
        
        connections.init_pool ( pointlimit );

// 	state_energy_grad_total_updates_counter=0;

	volume=0;
	
        numvoxel=0;
	single_scale=false;
        shape[0]=shape[1]=shape[2]=-1;

        opt_temp=1;
        opt_temp_conn_cost=1;
	opt_temp_conn_cost_dynamic=false;
        particle_pool= new pool<class Points<T,Dim> > ( pointlimit,"points" );
	
	
        tree=NULL;
        connecion_candidate_tree=NULL;
	bifurcation_tree=NULL;

#ifdef D_USE_GUI
        renderer= NULL;//new CTrackerRenderer();
#endif

        offline=false;
        debug_command.clear();


        proposals_per_seconds=0;
        accepted_proposals_per_seconds=0;
        accepted_proposals_per_seconds_selected=0;
        particle_changed_per_seconds=0;



        add_particle_helper=NULL;
        add_particle_helper_deleteme=false;


// 	energy_gradient_stat=new CStats(1000,true);
// 	edge2particle_stat=new CStats(100,false);

// 	edge2particle_stat.init(100,false);
        edge2particle_stat.init ( 200,false );
        edge2particle_rate[0]=edge2particle_rate[1]=edge2particle_rate[2]=0;


        energy_gradient_stat2[0].init ( 200,true );
        energy_gradient_stat2[1].init ( 200,true );
        energy_gradient_stat2[2].init ( 200,true );

        for ( int i=0; i<nproposal_names; i++ ) {
            selective_stat2[i]=false;
        }

        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_BIRTH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_DEATH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH )]=true;
        selective_stat2[static_cast<unsigned int> ( PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH )]=true;
        /*
        state_energy_grad_per_proposal=NULL;
        energy_gradient_stat_per_proposal=NULL;
        state_energy_grad_update_counter_per_proposal=NULL;
        state_energy_grad_update_counter_updated_per_proposal=NULL;
        state_energy_grad_proposal_type=NULL;
        */

        state_energy_grad_mean=0;
        state_energy_grad_mean_old=0;
        particle_number_grad[0]=particle_number_grad[1]=0;
	
	
	state_energy_var=0;
        state_energy_mean=0;
        state_energy_n=0;


        active_scheduler=COOL_DOWN_SCHEDULER::COOL_DOWN_BIRTH_DEATH;
	
	sample_scale_temp_dependent=false;
	
	ready=0;
	
// 	state_energy_grad_mean_n=0;
    }




    T get_E_mean(){return state_energy_mean;};
    T get_E_std(){return ((state_energy_n<2) ?  -1 : mhs_fast_math<T>::sqrt(state_energy_var/(state_energy_n-1)));};

    
    void update_energy ( T E, int proposal_id=-1 ) {

      
// //NOTE correct this in proposal, not here?       
 E*=opt_temp;
      

//       E+=pcosts;
// 	T betha=(1-options.opt_alpha)/std::pow(T(10),opt_temp);
//       T betha=(1-options.opt_alpha)/std::max(opt_temp*opt_temp,T(1));
// 	state_energy_grad=(1-betha)*state_energy_grad+(betha)*E;


        //energy_gradient_stat->add(E);
// 	E/=opt_temp*opt_temp;
// 	E/=opt_temp;

        state_energy_grad_mean+=E;
	
	
	state_energy_n+=1;
	T delta=E-state_energy_mean;
	state_energy_mean+=delta/state_energy_n;
	state_energy_var+=delta*(E-state_energy_mean);

// 	state_energy_grad_mean_n++;
//  	T delta= E -  state_energy_grad_mean;
// 	state_energy_grad_mean+=  delta/state_energy_grad_mean_n;

//         if ( state_energy_grad_update_counter2[0]>0 ) {
//             state_energy_grad2[0]=options.opt_alpha*state_energy_grad2[0]+ ( 1-options.opt_alpha ) *E/opt_temp;
//         } else {
//             state_energy_grad2[0]=E/opt_temp;
//         }
//NOTE whyt it was /opt_Temp before?  	
        if ( state_energy_grad_update_counter2[0]>0 ) {
            state_energy_grad2[0]=options.opt_alpha*state_energy_grad2[0]+ ( 1-options.opt_alpha ) *E;
        } else {
	   printf("init energy history\n");
            state_energy_grad2[0]=E;
	    E_lin=E;
        }	

        //selective statistic
        if ( proposal_id>-1 ) {

            sta_assert_error ( proposal_id<nproposal_names );
            if ( selective_stat2[proposal_id] ) {

	     
	      
                if ( state_energy_grad_update_counter2[1]>0 ) {
                    //double weight=1-(double)particle_changed_per_seconds/accepted_proposals_per_seconds;



                    state_energy_grad2[1]=options.opt_alpha*state_energy_grad2[1]+ ( 1-options.opt_alpha ) *E;
                } else {
                    state_energy_grad2[1]=E;
                }
                
		
                state_energy_grad_update_counter2[1]++;
                state_energy_grad_update_counter_updated2[1]=true;
            }
        }


        std::size_t n_bifurcations=connections.get_num_bifurcation_centers();
        std::size_t n_points=particle_pool->get_numpts()-n_bifurcations;

// 	if (options.opt_create_movie)
// 	{
// 	 movie.check(*this);
// 	}

        if ( particle_number_grad[0]!=n_points ) {
            //particle_number_grad[1]=particle_number_grad[0];
            //particle_number_grad[0]=n_points;

            //T grad=(particle_number_grad[1]-particle_number_grad[0]);
            T grad= ( n_points-particle_number_grad[0] );
            particle_number_grad[0]=n_points;

	    
	    
            if ( state_energy_grad_update_counter2[2]>0 ) {
                state_energy_grad2[2]=options.opt_alpha*state_energy_grad2[2]+ ( 1-options.opt_alpha ) *grad;
            } else {
                state_energy_grad2[2]=grad;
            }
            
            
            state_energy_grad_update_counter2[2]++;
            state_energy_grad_update_counter_updated2[2]=true;


            state_particle_number_activity_count[0]++;
        } else {

            state_particle_number_activity_count[1]++;
        }

        /*
        if (proposal_id>-1)
        {
          sta_assert_error(proposal_id<nproposal_names);
          state_energy_grad_per_proposal[proposal_id]=options.opt_alpha*state_energy_grad_per_proposal[proposal_id]+(1-options.opt_alpha)*E;

          state_energy_grad_update_counter_per_proposal[proposal_id]++;
          state_energy_grad_update_counter_updated_per_proposal[proposal_id]=true;
        }
        */

// 	if (state_energy_grad_update_counter%100==0)
// 	  energy_gradient_stat->add(state_energy_grad);


        state_energy_grad_update_counter2[0]++;
        state_energy_grad_update_counter_updated2[0]=true;
// 	state_energy_grad_total_updates_counter++;
    }
    
    
    void dynamic_point_scale()
    {	if (options.time_dependent_scale)
	{
	  
	  
	  
	T pscale=std::min(opt_temp/maxscale,T(1));
				  //T override_edge_length_epsilon=2* (maxscale -  ((Points<T,Dim>::thickness>0) ?  Points<T,Dim>::thickness : -maxscale*Points<T,Dim>::thickness)*Points<T,Dim>::endpoint_nubble_scale);
	T override_edge_length_epsilon=2* (maxscale -  ((Points<T,Dim>::thickness>0) ?  Points<T,Dim>::thickness : -maxscale*Points<T,Dim>::thickness));
				  //maxscale*
				  
				  
				  
				  override_edge_length_epsilon=CEdgecost<T,Dim>::get_total_edge_length_limit()-pscale*override_edge_length_epsilon;
				  
				  //edgecost_fun->set_max_edge_length_soft(override_edge_length_epsilon);
				  CEdgecost<T,Dim>::set_max_edge_length_soft(override_edge_length_epsilon,current_thread_id);
				  
				  printf("dynamic edge length: %f <- %f\n",CEdgecost<T,Dim>::get_total_edge_length_limit(),override_edge_length_epsilon);
				  Points<T,Dim>::_dynamic_thicknes_fact[current_thread_id]=opt_temp;
				  
		  std::size_t n_points=particle_pool->get_numpts();
		  const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
		  
		  std::list<Points<T,Dim>* > delete_me;
		  for ( unsigned int i=0; i<n_points; i++ ) {
		      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
		      point.update_endpoints2();
		  }
		  
		  for ( unsigned int i=0; i<n_points; i++ ) {
		      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
		      if (!point.valid_triangle(*edgecost_fun))
		      {
			delete_me.push_back(&point);
		      }
		  }
		  
		  if (delete_me.size()>0)
		  {
		    printf("%d bifurcations are not valid and will be removed (particle size has changed) \n",delete_me.size());
		  }
		  for (typename std::list<Points<T,Dim>* >::iterator iter=delete_me.begin();iter!=delete_me.end();iter++)
		  {
		   
		    class Points<T,Dim>  & bif_point=**iter;
		    
		    class Points<T,Dim>  * cpts[3];
		    
		    class Connection<T,Dim>   * old_connection_backup[3];
		    //deleting old edges
		    for ( int i=0; i<3; i++ ) {
			old_connection_backup[i]=bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connection;
			cpts[i]=bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected->point;
		    }
		    
		     for ( int i=0; i<3; i++ ) {
			  connections.remove_connection( old_connection_backup[i] );
		      }

		      class Points<T,Dim>  *bif_point_pt=&bif_point ;
		      particle_pool->delete_obj ( bif_point_pt);
		      
		      for ( int i=0; i<3; i++ ) 
		      {
			
		      Points<T,Dim>  & bif_neigh=* ( cpts[i]);
		      
		      sta_assert_error((bif_neigh.get_num_connections()<2));
		       sta_assert_error(connecion_candidate_tree->insert ( bif_neigh,true));
		      }               
		    
		  }
				  
				  
	}
      }

    inline void update_temp (){// const std::size_t & iteration ) {

      
      
      if (update_dynamic_point)
      {
	dynamic_point_scale();
	update_dynamic_point=false;
      }
      
#ifdef D_USE_GUI
        if ( renderer->get_movie_state() ==CTrackerRenderer<TData,T,Dim>::MOVIE_RECORDING ) {
	  
            std::size_t n_connections=connections.pool->get_numpts();
            //if ( n_connections!=movie_state_egdge_history ) {
//                 if ( n_connections<50 ) {
//                     renderer->set_movie_state ( CTrackerRenderer<TData,T,Dim>::MOVIE_RECORDING_WAITING_FOR_CAPTURE );
//                     renderer->pause();
//                 } else {
//                     if (( n_connections>movie_state_soft_max ) || (state_energy_grad_update_counter2[0]%1000==0)) {
//                         movie_state_soft_max=n_connections;
//                         renderer->set_movie_state ( CTrackerRenderer<TData,T,Dim>::MOVIE_RECORDING_WAITING_FOR_CAPTURE );
//                         renderer->pause();
// 			movie_state_soft_max=0.95*movie_state_soft_max+0.1*n_connections;
// 			printf("NEXT FRAME IF %d > %f\n",n_connections,movie_state_soft_max);
//                     }
//                 }
		//
//                movie_state_egdge_history=n_connections;	      
	    
	    if (is_movie_maker)
	    {
	    if (movie_state_egdge_history!=state_energy_grad_update_counter2[0])
	    {
	       
	       //int rate=50+(n_connections>50)*(450 * (opt_temp>1) + 500 * (opt_temp<1));
	       
	        if ((state_energy_grad_update_counter2[0]%movie_state_rate)==0) {
		  // printf("CURRETN FRAME SNAPRATE: %d \n",rate);
                    renderer->set_movie_state ( CTrackerRenderer<TData,T,Dim>::MOVIE_RECORDING_WAITING_FOR_CAPTURE );
                    renderer->pause();
		    //printf("%d. %d, %d\n",movie_state_egdge_history,state_energy_grad_update_counter2[0],movie_state_rate);
		    //printf("ready for capturing\n");
		    //movie_state_rate=50+(n_connections>50)*(950 * (opt_temp>1) + 1000 * (opt_temp<1));
		    //movie_state_rate=movie_steprate+(opt_temp<1)*(9*movie_steprate);
		    movie_state_rate=movie_steprate+(opt_temp<1)*(2*movie_steprate);
		  }
		  movie_state_egdge_history=state_energy_grad_update_counter2[0];	  
	    }
	    }
	    
	    
               // movie_state_egdge_history=n_connections;
            //}
        }
        else if (renderer->get_movie_state()==CTrackerRenderer<TData,T,Dim>::MOVIE_NOT_RECORDING)
	{
	  movie_state_rate=movie_steprate; 
	}

#endif


        if ( iterations<100000 ) {
            state_energy_grad_old2[0]=state_energy_grad2[0];
            state_energy_grad_old2[1]=state_energy_grad2[1];
            return;
        }


        if ( state_eval_state%10000==0 ) {
            energy_gradient_stat2[0].add ( state_energy_grad2[0] );
            energy_gradient_stat2[1].add ( state_energy_grad2[1] );
            energy_gradient_stat2[2].add ( state_energy_grad2[2] );
	    
// 	    if (state_energy_grad2_old<0)
// 	    {
// 	      
// 	    }else
// 	    {
// 	      printf("grad : %f | %f \n",std::abs(state_energy_grad2_old-state_energy_grad2[2]),state_energy_grad2[2]);  
// 	    }
// 	    state_energy_grad2_old=state_energy_grad2[2];
	      
	    
	    
            

            std::size_t n_points=particle_pool->get_numpts();
            std::size_t n_connections=connections.pool->get_numpts();

            edge2particle_stat.add ( ( double ) ( n_connections ) / ( n_points+1 ) );
// 	      state_particle_number_activity_count[0]=state_particle_number_activity_count[1]=0;
        }


        if ( state_eval_state%1000000==0 ) {
            state_particle_number_activity=state_particle_number_activity_count[0]/ ( state_particle_number_activity_count[1]+std::numeric_limits< double > ::epsilon() );
            state_particle_number_activity_max=std::max ( state_particle_number_activity,state_particle_number_activity_max );
            state_particle_number_activity_count[0]=state_particle_number_activity_count[1]=0;
        }
        
//         if ( state_energy_grad_update_counter2[0]%1000==0 ) {
//         state_energy_var=0;
//         state_energy_mean=0;
//         state_energy_n=0;
// 	}
	
        
if (false)        
{
  
//    if ( state_eval_state%100000==0 ) {
     if ((state_energy_grad_update_counter2[0]%10000==0)&&(state_energy_grad_update_counter_updated2[0]))
     {
       state_energy_grad_update_counter_updated2[2]=false;
       
       double egrad=(state_energy_grad2[0]-E_lin)/2;
	//if ((std::abs(state_energy_grad2[0])<(options.cooldown_threshold/(single_scale? 1 : opt_temp))))
       if ((std::abs(state_energy_grad2[0])<(options.cooldown_threshold)) && (std::abs(egrad)<0.01))
       //if (state_energy_grad2[0]>-options.cooldown_threshold)
	{
		num_decrease++;
		T dfact=0.98;
		//T dfact=0.98;
                opt_temp*=dfact;
		
		if (opt_temp_conn_cost_dynamic)
		{
		  opt_temp_conn_cost*=dfact;
		}	
		{
		  dynamic_point_scale();
		}
//		state_energy_grad2[0]=0;
	}
	
	printf("Energy lin: %f\n",egrad);
	E_lin=state_energy_grad2[0];
   }
}else
{        

        //NOTE  slow cool-down in case of 暇
        if (( state_particle_number_activity<state_particle_number_activity_max*options.linear_cool_down_threshold )||(options.fixed_cooldown)) {
	  std::size_t n_points=particle_pool->get_numpts();
            if ( state_eval_state%(100000*(1+n_points/1000))==0 )
            {
                num_decrease++;
		//T dfact=0.99;
		T dfact=0.98;
                opt_temp*=dfact;
		
		if (opt_temp_conn_cost_dynamic)
		{
		  opt_temp_conn_cost*=dfact;
		}	
		
		
		
		//if (options.time_dependent_scale)
		{
		  dynamic_point_scale();
		}
		state_energy_var=0;
		state_energy_mean=0;
		state_energy_n=0;
		
// 		if (options.time_dependent_scale)
// 		{
// 		  T pscale=std::min(opt_temp/maxscale,T(1));
// 		  T override_edge_length_epsilon=2* (maxscale -  (Points<T,Dim>::thickness>0) ?  Points<T,Dim>::thickness : -maxscale*Points<T,Dim>::thickness);
// 		  //maxscale*
// 		  override_edge_length_epsilon=options.connection_candidate_searchrad-pscale*override_edge_length_epsilon;
// 		  CEdgecost<T,Dim>::override_max_length(override_edge_length_epsilon);
// 		  printf("dynamic edge length: %f\n",override_edge_length_epsilon);
// 		  Points<T,Dim>::dynamic_thicknes_fact=opt_temp;
// 		}
		
            }
            active_scheduler=COOL_DOWN_SCHEDULER::COOL_DOWN_ITERATION;
        } else {
            active_scheduler=COOL_DOWN_SCHEDULER::COOL_DOWN_BIRTH_DEATH;
        }

        {

            if ( state_energy_grad_update_counter_updated2[2] ) {
                state_energy_grad_update_counter_updated2[2]=false;

                if ( particle_changed_per_seconds>0 ) {
                    {
                        //if ( state_energy_grad_update_counter2[2]>100000==0 ) {
			  if ( state_energy_grad_update_counter2[2]>100 ) {
			  
			    //T grad_normalize_with_volume=T(1)/(shape[0]*shape[1]*shape[2]);
			    T grad_normalize_with_volume=(shape[0]*shape[1]*shape[2])*opt_temp/T(100000);
			    //T grad_normalize_with_volume=1;
                            if ( ( std::abs ( state_energy_grad2[2]*grad_normalize_with_volume ) < ( options.cooldown_threshold ) ) ) {
                                num_decrease++;

				//T dfact=0.99;
				T dfact=0.98;
                                opt_temp*=dfact;
				
				if (opt_temp_conn_cost_dynamic)
				{
				  opt_temp_conn_cost*=dfact;
				}	
				
				//if (options.time_dependent_scale)
				{
				  dynamic_point_scale();
				}
		state_energy_var=0;
		state_energy_mean=0;
		state_energy_n=0;
                                state_energy_grad_update_counter2[2]=0;
                            }
                            state_energy_grad_old2[2]=state_energy_grad2[2];
                        }
                    }
                }


                if ( state_energy_grad_update_counter2[2]%10000==0 ) { // TODO: make this particles_per_5minutes_dependent
                    double current_timestamp=particle_number_change_seconds_timer.get_last_call_runtime();
                    particle_changed_per_seconds=10000/ ( current_timestamp );
                }

            }
        }
        
}        

        if ( state_energy_grad_update_counter_updated2[1] ) {
            state_energy_grad_update_counter_updated2[1]=false;

            if ( state_energy_grad_update_counter2[1]>100000==0 ) {
                state_energy_grad_old2[1]=state_energy_grad2[1];
            }

            if ( state_energy_grad_update_counter2[1]%10000==0 ) {
                double current_timestamp=selected_proposals_per_seconds_timer.get_last_call_runtime();
                accepted_proposals_per_seconds_selected=10000/ ( current_timestamp );

            }
        }

        if ( state_energy_grad_update_counter_updated2[0] ) {
            state_energy_grad_update_counter_updated2[0]=false;

            if ( state_energy_grad_update_counter2[0]%10000==0 ) {
                double current_timestamp=total_proposals_per_seconds_timer.get_last_call_runtime();
                accepted_proposals_per_seconds=10000/ ( current_timestamp );

            }
        }




// #ifndef D_USE_GUI
// #ifdef D_USE_MULTITHREAD
// 
//         int thread_id= omp_get_thread_num();
// #else
//         int thread_id= 0;
// #endif
// #endif

        if ( state_eval_state%100000==0 ) {
            double current_timestamp=proposals_per_seconds_timer.get_last_call_runtime();
            proposals_per_seconds=100000/ ( current_timestamp );
        }


        state_eval_state++;
    }


    std::size_t 		pointlimit;

    
    std::size_t 		iterations;
    std::size_t 		real_iterations;
    std::size_t 		num_decrease;
    std::size_t 		init_state;
    COptions 			options;
    CData<T,TData,Dim> *	data_fun;
    CEdgecost<T,Dim> *		edgecost_fun;
    Collision<T,Dim> *		collision_fun;
    std::size_t 		shape[3];
    std::size_t			numvoxel;
    T 				collision_search_rad;
    T				maxendpointdist;
    T 				search_connection_point_center_candidate;

    T 				minscale;
    T      			maxscale;
    bool			single_scale;
    T 				volume;
    bool  sample_scale_temp_dependent;
      
    int current_thread_id;



    CBirth<TData,T,Dim> * add_particle_helper;
    bool add_particle_helper_deleteme;

    
    // global temperature
    T opt_temp;
    // temperature for connection selection (currently constant)
    T opt_temp_conn_cost;
    bool opt_temp_conn_cost_dynamic;

    // further user-independent parameters necessary for storing the state
    std::size_t state_eval_state;
    double state_iteration_time;
    double state_iteration_time_total;
    std::size_t state_iteration_total_num;

    double state_energy_grad2[3];
//     double state_energy_grad2_old=-1;
    double state_energy_grad_old2[3];
    std::size_t state_energy_grad_update_counter2[3];
    bool state_energy_grad_update_counter_updated2[3];
    CStats energy_gradient_stat2[3];
    double E_lin;

    bool selective_stat2[nproposal_names];


    COOL_DOWN_SCHEDULER active_scheduler;

//     std::size_t state_energy_grad_total_updates_counter;

    double state_energy_var;
    double state_energy_mean;
    std::size_t state_energy_n;
    
    
    double state_energy_grad_mean;
    
    double state_energy_grad_mean_old;
    double particle_number_grad[2];

    double state_particle_number_activity;
    double state_particle_number_activity_max;
    std::vector<std::size_t> state_particle_number_activity_count;

//     std::size_t particle_number_grad[2];
#ifndef _D_USE_GUI
    bool terminate=false;
#endif

    std::size_t state_movie_frame;
//     CMovie movie;
    bool is_pausing;


    std::size_t movie_state_egdge_history;
    double movie_state_soft_max;
    std::size_t movie_state_rate;
    static std::size_t movie_steprate;
    bool is_movie_maker;
    
    
   


//     std::size_t state_energy_grad_mean_n=0;
//     double state_energy_grad_mean;



    CStats edge2particle_stat;
    double edge2particle_rate[3];

    /*
    double * state_energy_grad_per_proposal;
    CStats * energy_gradient_stat_per_proposal;
    std::size_t * state_energy_grad_update_counter_per_proposal;
    bool*  state_energy_grad_update_counter_updated_per_proposal;
    int * state_energy_grad_proposal_type;
    */


    mhs::CtimeStopper proposals_per_seconds_timer;
    mhs::CtimeStopper total_proposals_per_seconds_timer;
    mhs::CtimeStopper selected_proposals_per_seconds_timer;
    mhs::CtimeStopper particle_number_change_seconds_timer;
    double proposals_per_seconds;
    double accepted_proposals_per_seconds;
    double accepted_proposals_per_seconds_selected;
    double particle_changed_per_seconds;

    int ready;
    bool update_dynamic_point;


//     DEBUG_COMMAND debug_command;
    std::list<User_action*> debug_command;

    pool<class Points<T,Dim> > * particle_pool;
    OctTreeNode<T,Dim> * tree;
    OctTreeNode<T,Dim> * connecion_candidate_tree;
    OctTreeNode<T,Dim> * bifurcation_tree;
    Connections<T,Dim>  connections;

    bool offline;


    class CProposalOptions
    {
    public:
        class CProposal<TData,T,Dim> * proposal;
        T weight;
	double profiling_time;
	double profiling_time_max;
	double profiling_time_var_k;
	std::size_t profiling_time_var_n;
	double profiling_time_var_sum;
	double profiling_time_var_sum_sqr;
        bool delete_on_destroy;
	CProposalOptions ()
	{
	  profiling_time=0;
	  profiling_time_max=-std::numeric_limits< double>::max();
	  //profiling_time_min=std::numeric_limits< double>::max();
	  profiling_time_var_n=0;
	  profiling_time_var_sum=0;
	  profiling_time_var_sum_sqr=0;
	  
	}
    };
    std::vector<CProposalOptions> proposals;
    
    
    

    void set_params ( const mxArray * params=NULL ) {
        if ( params==NULL ) {
            return;
        }

        try {
// printf("0\n");
            data_fun->set_params ( params );
            /*printf("1\n");	*/
            edgecost_fun->set_params ( params );
            /*printf("2\n");	 */
            options.set_params ( params );
            /*printf("3\n");*/
            Points< T, Dim >::set_params ( params );
// printf("4\n");
            if ( mhs::mex_hasParam ( params,"opt_temp_conn_cost" ) !=-1 ) {
                opt_temp_conn_cost=mhs::mex_getParam<T> ( params,"opt_temp_conn_cost",1 ) [0];
            }

            if ( mhs::mex_hasParam ( params,"opt_temp" ) !=-1 ) {
                opt_temp=mhs::mex_getParam<T> ( params,"opt_temp",1 ) [0];
            }
            
            if ( mhs::mex_hasParam ( params,"opt_temp_conn_cost_dynamic" ) !=-1 ) {
                opt_temp_conn_cost_dynamic=mhs::mex_getParam<bool> ( params,"opt_temp_conn_cost_dynamic",1 ) [0];
            }
            
             if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
	      sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0]; 
	     
	      if (sample_scale_temp_dependent)
	      {
	      opt_temp=std::min(opt_temp,maxscale-T(0.1)); 
	      }

        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }


    //void load_image_data ( const mxArray * feature_struct ) {
    void init_shape_and_scale ( ) {
        try {
            //data_fun->init(feature_struct);
            data_fun->getshape ( shape );
            minscale=data_fun->get_minscale();
            maxscale=data_fun->get_maxscale();
	    
	    single_scale=std::abs(maxscale-minscale)<std::numeric_limits<T>::epsilon();
	    
	    if (single_scale)
	    {
	      printf("tracker is single scale!\n");
	    }


            numvoxel=shape[0]*shape[1]*shape[2];
	    
	    
	    
	   

        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }

    void proposal_register ( class CProposal<TData,T,Dim> * proposal,
                             T weight_do=1,
			     T weight_undo=1,
                             bool delete_on_destroy=false ) {
        sta_assert_error ( init_state==0 );
        if ( weight_do==-1 ) {
            weight_do=proposal->get_default_weight();
	    weight_undo=proposal->get_default_weight();
        }
        proposal->set_weights(weight_do,weight_undo);
        proposal->set_tracker ( *this );
        CProposalOptions poptions;
        poptions.weight=weight_do;
        poptions.proposal=proposal;
        poptions.delete_on_destroy=delete_on_destroy;
        proposals.push_back ( poptions );
    }

    CProposalOptions proposal_register ( std::string proposal_name,
                                         T weight_do=1,T weight_undo=1) {
        sta_assert_error ( init_state==0 );
        CProposalOptions poptions;
        try {
            poptions.proposal=CProposal<TData,T,Dim>::proposal_create_from_name ( proposal_name );
        } catch ( mhs::STAError & error ) {
            throw error;
        }
        if ( weight_do==-1 ) {
            weight_do=poptions.proposal->get_default_weight();
	    weight_undo=poptions.proposal->get_default_weight();
        }
        poptions.proposal->set_weights(weight_do,weight_undo);
        poptions.weight=weight_do;
        poptions.delete_on_destroy=true;
        poptions.proposal->set_tracker ( *this );
        proposals.push_back ( poptions );
        printf ( "adding [%s, w: %.2f  %.2f]\n",proposal_name.c_str(),weight_do,weight_undo );
        return poptions;
    }

    void proposal_clear() {
        for ( int i=0; i<proposals.size(); i++ )
            if ( proposals[i].delete_on_destroy ) {
                delete proposals[i].proposal;
            }

        proposals.clear();
    }
    
    
    void clear_path_labels()
    {
        sta_assert_error(particle_pool!=NULL);
        std::size_t n_points=particle_pool->get_numpts();
        const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();


        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
            point.set_id ( -1 );
            point._path_id=-1;
        }
    }
    
      
    void check_particle_consistensy2()
    {
      try {
        sta_assert_error(particle_pool!=NULL);
        std::size_t n_points=particle_pool->get_numpts();
        const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();

	//printf("checking consisency of %d particles\n",n_points);
	
        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
	   
	    if  ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
	    {
	      
		  T bifurcation_data_old=0;
		  T bifurcation_data_old2=0;
		  
		    sta_assert_error(point.compute_data_term(*this->data_fun,bifurcation_data_old2));
		    bifurcation_data_old=point.point_cost;
		
		sta_assert_error_c(std::abs(bifurcation_data_old-bifurcation_data_old2)<std::numeric_limits< T>::epsilon(),
		    printf("check: %f %f\n",bifurcation_data_old,bifurcation_data_old2)
		  );
		
		    bool out_of_bounce;
		     Vector<T,3> tmp=point.get_position();
		       point.bifurcation_center_update(shape,out_of_bounce);
		     		    
			
	    if (!((tmp-point.get_position()).norm2()<0.00001))
	    {
	      
	      tmp.print();
	      point.get_position().print();
	      sta_assert_error(!out_of_bounce);
	      sta_assert_error((tmp-point.get_position()).norm2()<0.00001);
	     
	    }
			
			sta_assert_error(!out_of_bounce);
			
	      
	    }
	  }
	 } catch ( mhs::STAError  error ) {
	    error <<"\n!!!!!!!!!!!!!!!!!!!>check_particle_consistensy2 <!!!!!!!!!!!!!\n";
            throw error;
        }
    }

    void check_particle_consistensy4()
    {
      try {
        sta_assert_error(particle_pool!=NULL);
        std::size_t n_points=particle_pool->get_numpts();
        const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();

	//printf("checking consisency of %d particles\n",n_points);
	
        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
	   
	    bool cleft=( point.endpoint_connections[0]>0 );
	    bool cright=( point.endpoint_connections[1]>0 );
	    
	    switch ( point.particle_type)
	    {
	      case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER:
	      {
		
	      }break;
	      case PARTICLE_TYPES::PARTICLE_BIFURCATION:
	      {
		 sta_assert_debug0( (cleft && cright) != (point.has_owner(voxgrid_conn_can_id)) ); 
	      }break;
	      case PARTICLE_TYPES::PARTICLE_SEGMENT:
	      {
		 sta_assert_error_c( (cleft && cright) != (point.has_owner(voxgrid_conn_can_id)) ,printf("\n\n %d %d\n\n",cleft,cright);) ; 
		
	      }break;
	    }
	  }
	 } catch ( mhs::STAError  error ) {
	    error <<"\n!!!!!!!!!!!!!!!!!!!>check_particle_consistensy2 <!!!!!!!!!!!!!\n";
            throw error;
        }
    }
    
    
    void check_particle_consistensy()
    {
      try {
        sta_assert_error(particle_pool!=NULL);
        std::size_t n_points=particle_pool->get_numpts();
        const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();

	printf("checking consisency of %d particles\n",n_points);
	
        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
	   
	    if  ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
	    {
		    bool  out_of_bounce;
		    
	  
	      
	      sta_assert_error((point.endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->point->bifurcation_center_neighbourhood_update ( shape,out_of_bounce )));
	      
	      sta_assert_error((point.endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected->point->bifurcation_center_neighbourhood_update ( shape,out_of_bounce )));      
		       
	      sta_assert_error((point.endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected->point->bifurcation_center_neighbourhood_update ( shape,out_of_bounce )));
		  
		  sta_assert_error(point.has_owner(voxgrid_bif_center_id));
	            if (options.bifurcation_particle_hard || options.bifurcation_particle_soft)
		    {
		      sta_assert_error(point.has_owner(voxgrid_default_id));
		      
		    }else
		    {
		       sta_assert_error(!point.has_owner(voxgrid_default_id));
		    }
		    if (options.bifurcation_particle_hard)
		    {
		      Vector<T,3> tmp=point.get_position();
		      T temp;
		      const OctPoints<T,Dim> & npoint=point;
		      	Collision<T,Dim> &   collision_fun_p=*(this->collision_fun);
			  OctTreeNode<T,Dim> &		ntree	=*(this->tree);
			
			
		     bool test1 = ( collision_fun_p.colliding( ntree,
						      npoint,
						      collision_search_rad,
						      temp));
		     
		       point.bifurcation_center_update(shape,out_of_bounce);
		     		     bool test2=( collision_fun_p.colliding( ntree,
						      npoint,
						      collision_search_rad,
						      temp));
			
				     sta_assert_error((tmp-point.get_position()).norm2()<0.00001);
			sta_assert_error_c(((!test1) && (!test2)),printf("test 1 %d  && test 2 %d\n",test1,test2));
			
				     
		    }
// 		    sta_assert_error((!collision_fun->colliding( tree,point,collision_search_rad,temp)));
// 		    } 
// 		    		      point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
// 				      if (options.bifurcation_particle_hard)
// 		    {
// 		      T temp;
// 		    sta_assert_error((!collision_fun->colliding( tree,point,collision_search_rad,temp)));
// 		    }

	    }
	    
	    bool bif_particle_in_grid=options.bifurcation_particle_hard || options.bifurcation_particle_soft;
	    
	    sta_assert_error_c ( ( (!point.has_owner(voxgrid_default_id) || ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER )  )) || ( bif_particle_in_grid&& ( point.has_owner(voxgrid_default_id) ) ) 
	      ,(printf(" has owner0 :[%d %d %d | %d] \n bif hard : %d\n type :%d\n",point.has_owner(voxgrid_default_id),point.has_owner(voxgrid_conn_can_id),point.has_owner(voxgrid_bif_center_id),point.has_owner(),bif_particle_in_grid,point.particle_type ) )
	    );
	    
	    sta_assert_error( ( point.has_owner())  )
	    
	    
// 	T max_pt_rad=std::max(maxscale,point.predict_thickness_from_scale(maxscale))+0.01;
//  	T particle_collision_search_rad=0;
// 	particle_collision_search_rad=std::max(point.get_scale(),point.get_thickness())+ max_pt_rad;
// 	
// 	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
// 	class Collision<T,Dim>::Candidates cand(query_buffer);
// 	
// 	Collision<T,Dim> &   collision_fun_p=*(this->collision_fun);
// 	OctTreeNode<T,Dim> &		ntree	=*(this->tree);
// 	
// 	T temp;
// 	T en[3];
// 	en[0]=en[1]=en[2]=0;
// 	bool coll[3];
// 	coll[0]=collision_fun_p.colliding(en[0],ntree,point,particle_collision_search_rad,temp,&cand,true, T(-9));
// 	coll[1]=collision_fun_p.colliding(en[1],ntree,point,particle_collision_search_rad,temp,&cand,false, T(-10));
// 	coll[2]=collision_fun_p.colliding(en[2],ntree,point,particle_collision_search_rad,temp);
// 	
// 	sta_assert_error_c(((coll[0]==coll[1]) && (coll[0]==coll[2]) && (coll[2]==coll[1])) ,
// 			   (printf("%d %d %d\n",coll[0],coll[1],coll[2]))
// 			  );
// 	sta_assert_error_c(((en[0]==en[1]) && (en[0]==en[2]) && (en[2]==en[1])) ,
// 			   (printf("%f %f %f\n",en[0],en[1],en[2]))
// 			  );
	
// 	collision_search_rad

	    
	    
//             point.check_consistency3();
// 	    point.check_consistency2();
// 	    point.check_consistency(options.bifurcation_particle_hard);
	    
        }
      } catch ( mhs::STAError  error ) {
	    error <<"\n!!!!!!!!!!!!!!!!!!!>check_particle_consistensy <!!!!!!!!!!!!!\n";
            throw error;
        }
    }

    static bool do_label ( Points<T,Dim> & npoint,
                    int & pathid,
		    T & path_length,
                    int max_depths=100000,
                    int depths=0,
                    Points<T,Dim> * startpoint=NULL,int thread_id=-1, T * v_scale  =NULL ) {
        if ( depths>max_depths ) {
            printf("!!!! WARNING: do_label exceeds max depth !!!!\n ");
            return true;
        }
        
        

        if ( ( depths==0 ) && ( npoint.get_num_connections() ==0 ) ) {
	  if (npoint.particle_type==PARTICLE_TYPES::PARTICLE_BLOB)
	  {
	    npoint._path_id=1;
	  }
//printf("no conn, %d %d\n",depths,npoint._path_id);
          return false;
        }

        if ( npoint._path_id>-1 ) {
//printf("npoint._path_id>-1, %d\n",depths);            
            return false;
        }



        if ( depths==0 ) {
#ifdef D_USE_MULTITHREAD
            if ( thread_id==-1 ) {
                thread_id= omp_get_thread_num();
            }
#else
            thread_id= 0;
#endif

            track_unique_id[thread_id]++;
            pathid++;
        }

//         if (thread_id==-1)
// 	{
// 	 printf("%d %d\n",thread_id,depths);
// 	}

        npoint._path_id=pathid;

        {
            for ( int end_id=0; end_id<2; end_id++ ) {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                    if ( endpointsA[i].connected!=NULL ) {
                        if ( endpointsA[i].connection->track_me_id!=track_unique_id[thread_id] ) {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];

			    if (path_length>-1)
			    {
			      if (v_scale!=NULL)
			      {
				Vector<T,3> v=(endpointsA[i].connected->point->get_position()-npoint.get_position());
				v[0]*=v_scale[0];
				v[1]*=v_scale[1];
				v[2]*=v_scale[2];
				path_length+=std::sqrt(v.norm2());
			      }else
			      {
				path_length+=std::sqrt(
						(endpointsA[i].connected->point->get_position()-
						npoint.get_position()).norm2()
						);
				
			      }
			    }
			    
                            bool check=( do_label ( * ( endpointsA[i].connected->point ),
                                         pathid,
					 path_length,
                                         max_depths,
                                         depths+1,
                                         startpoint,thread_id,v_scale ) );
			    if (!check)
			    {
			     printf("path id already set? ids not reset before?\n"); 
			    }
                        }
                    }
                }
            }
        }
        
        return true;
    }

    
    
    static bool do_label_debug ( Points<T,Dim> & npoint,
                    int & pathid,
		    T & path_length,
                    int max_depths=100000,
                    int depths=0,
                    Points<T,Dim> * startpoint=NULL,int thread_id=-1, T * v_scale  =NULL ) {
        if ( depths>max_depths ) {
            printf("!!!! WARNING: do_label exceeds max depth !!!!\n ");
            return true;
        }
        
        

        if ( ( depths==0 ) && ( npoint.get_num_connections() ==0 ) ) {
	  if (npoint.particle_type==PARTICLE_TYPES::PARTICLE_BLOB)
	  {
	    npoint._path_id=1;
	  }
printf("no conn, %d %d\n",depths,npoint._path_id);
          return false;
        }

        if ( npoint._path_id>-1 ) {
            if (depths>0)
            {
                printf("npoint._path_id>-1, %d\n",depths);            
            }
            return false;
        }



        if ( depths==0 ) {
#ifdef D_USE_MULTITHREAD
            if ( thread_id==-1 ) {
                thread_id= omp_get_thread_num();
            }
#else
            thread_id= 0;
#endif

            track_unique_id[thread_id]++;
            pathid++;
        }

//         if (thread_id==-1)
// 	{
// 	 printf("%d %d\n",thread_id,depths);
// 	}

        npoint._path_id=pathid;

        {
            for ( int end_id=0; end_id<2; end_id++ ) {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                    if ( endpointsA[i].connected!=NULL ) {
                        if ( endpointsA[i].connection->track_me_id!=track_unique_id[thread_id] ) {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];

			    if (path_length>-1)
			    {
			      if (v_scale!=NULL)
			      {
				Vector<T,3> v=(endpointsA[i].connected->point->get_position()-npoint.get_position());
				v[0]*=v_scale[0];
				v[1]*=v_scale[1];
				v[2]*=v_scale[2];
				path_length+=std::sqrt(v.norm2());
			      }else
			      {
				path_length+=std::sqrt(
						(endpointsA[i].connected->point->get_position()-
						npoint.get_position()).norm2()
						);
				
			      }
			    }
			    
                            bool check=( do_label ( * ( endpointsA[i].connected->point ),
                                         pathid,
					 path_length,
                                         max_depths,
                                         depths+1,
                                         startpoint,thread_id,v_scale ) );
			    if (!check)
			    {
			     printf("path id already set? ids not reset before?\n"); 
			    }
                        }
                    }
                }
            }
        }
        
        return true;
    }

    
    
     static bool clear_path_ids ( Points<T,Dim> & npoint,
                    int max_depths=100000,
                    int depths=0,
                    Points<T,Dim> * startpoint=NULL,int thread_id=-1 ) {
        if ( depths>max_depths ) {
            return true;
        }
        
        

        if ( ( depths==0 ) && ( npoint.get_num_connections() ==0 ) ) {
	  if (npoint.particle_type==PARTICLE_TYPES::PARTICLE_BLOB)
	  {
	    npoint._path_id=-1;
	  }
          return false;
        }

        if ( npoint._path_id==-1 ) {
            return false;
        }



        if ( depths==0 ) {
#ifdef D_USE_MULTITHREAD
            if ( thread_id==-1 ) {
                thread_id= omp_get_thread_num();
            }
#else
            thread_id= 0;
#endif

            track_unique_id[thread_id]++;
        }


        npoint._path_id=-1;

        {
            for ( int end_id=0; end_id<2; end_id++ ) {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                    if ( endpointsA[i].connected!=NULL ) {
                        if ( endpointsA[i].connection->track_me_id!=track_unique_id[thread_id] ) {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];

			    
			    
                            bool check=( clear_path_ids ( * ( endpointsA[i].connected->point ),
                                         max_depths,
                                         depths+1,
                                         startpoint,thread_id ) );
			    if (!check)
			    {
			     printf("path id already set? ids not reset before?\n"); 
			    }
                        }
                    }
                }
            }
        }
        
        return true;
    }

    

    void save_video_snapshot() {
#ifndef _OCTAVE_

        try {
            sta_assert_error ( options.opt_create_movie );
            std::size_t ndim=1;
//         mxArray *arg2 = save_tracker_state(false);

            mxArray *arg1 = save_frame_state();


            std::stringstream filename;
            filename<<options.opt_movie_folder<<"/MOVIE_"<<state_movie_frame+100000<<".mat";
            printf ( "creating video snapshot %d ..",state_movie_frame );
// 	  mxArray *arg1 = mhs::mex_string2mex(filename.str());
            int status;
            MATFile *pmat;
            pmat = matOpen ( filename.str().c_str(), "w" );
            if ( pmat != NULL ) {
                status = matPutVariable ( pmat, "A", arg1 );
                sta_assert_error ( status==0 );
                sta_assert_error ( matClose ( pmat ) == 0 );
            } else {
                printf ( "could'n write file\n" );
            }

            mxDestroyArray ( arg1 );

//          printf("ok %d\n",state_movie_frame);
            printf ( "ok \n" );
        } catch ( mhs::STAError error ) {
            printf ( "save_frame_state :%s\n",error.str().c_str() );
        }
#endif
    }


public:

    template <typename D>
    bool is_in_bb ( D pos[] ) {
        if ( ( pos[0]<0 ) ||
                ( pos[1]<0 ) ||
                ( pos<0 ) ||
                ( ! ( pos[0]<shape[0] ) ) ||
                ( ! ( pos[1]<shape[1] ) ) ||
                ( ! ( pos[2]<shape[2] ) ) ) {
            return false;
        }
        return true;
    }

#ifdef D_USE_GUI
    void set_renderer ( CTrackerRenderer<TData,T,Dim> * renderer ) {
        sta_assert_error ( this->renderer==NULL ); //TODO unregister renderer
        this->renderer=renderer;
    }
#endif


      std::size_t get_num_graph_segments()
      {
	T n_Terminals=connections.get_num_terminals();
	T n_Bifurcations=3*connections.get_num_bifurcation_centers();
	
	return (n_Terminals+n_Bifurcations)/2;
      }
      
      T update_volume(T dv)
      {
	dv*=2;
	volume+=dv*dv*dv;
      }
      


//     void execute(DEBUG_COMMAND command, void * options=NULL)
//     {
//       switch (command)
//       {
// 	case DEBUG_COMMAND::DEBUG_CLEAR_ALL:
// 	{
// 	  debug_command=command;
// 	}break;
//       }
//     }
    void execute ( User_action * command ) {

        debug_command.push_back ( command );
//       switch (command)
//       {
// 	case DEBUG_COMMAND::DEBUG_CLEAR_ALL:
// 	{
// 	  debug_command=command;
// 	}break;
//       }
    }

    void execute_me() {
        User_action * cmd= debug_command.front();
        debug_command.pop_front();
        sta_assert_error ( cmd!=NULL );

        switch ( cmd->command_type ) {
        case DEBUG_COMMAND::DEBUG_CLEAR_ALL: {

            printf ( "clearing %d edges ",connections.pool->get_numpts() );
            std::size_t numconnections=connections.pool->get_numpts();

            const Connection<T,Dim> ** conptr=connections.pool->getMemEnd();
            for ( std::size_t i=0; i<numconnections; i++ ) {
                Connection<T,Dim> * con=* ( Connection<T,Dim> ** ) ( conptr );
                connections.remove_connection ( con );

// 		connections.pool->delete_obj(con);
            }

            printf ( "(%d  left)\n",connections.pool->get_numpts() );

            printf ( "clearing  %d particles ",particle_pool->get_numpts() );
            std::size_t n_points=particle_pool->get_numpts();
            const class Points<T,Dim> ** particle_ptr=particle_pool->getMemEnd();
            for ( unsigned int i=0; i<n_points; i++ ) {
                Points<T,Dim> * point=* ( ( Points<T,Dim>** ) ( particle_ptr ) );
                particle_pool->delete_obj ( point );
            }
            printf ( "(%d  left)\n",particle_pool->get_numpts() );
            energy_gradient_stat2[0].clear();
            energy_gradient_stat2[1].clear();
            energy_gradient_stat2[2].clear();
            edge2particle_stat.clear();


        }
        break;
        case DEBUG_COMMAND::DEBUG_ADD_PARTICLE: {
            User_action_add_particle * pcmd= ( User_action_add_particle * ) cmd;


            if ( add_particle_helper!=NULL ) {
                sta_assert_error ( is_in_bb ( pcmd->position.v ) );

                try {
                    add_particle_helper->helper_add_particle ( pcmd->position );
                } catch ( mhs::STAError & error ) {
                    printf ( "error: %s\n",error.str().c_str() );
                    throw error;
                }

                printf ( "adding point @\n" );
                pcmd->position.print();
            }
            /*
            Points<T,Dim> * point=particle_pool.create_obj();
            Points<T,Dim> & cpoint=*point;

                cpoint.setpoint(pcmd->position[0],pcmd->position[1],pcmd->position[2]);

            // 	  T new_scale=myrand(minscale,maxscale);
            cpoint.scale=minscale;

            // orientation
            do {
                cpoint.direction=T(0);
                random_pic_direction<T,Dim>(
              cpoint.direction,
              cpoint.direction,
              T(1));
            } while (cpoint.direction[0]+cpoint.direction[1]+cpoint.direction[2]==0);

            if (colliding( tree,cpoint,collision_search_rad))
            {
                particle_pool.delete_obj(point);
            }

            T newpointsaliency;
            if (!data_fun.interp3(newpointsaliency,
                  saliency_map.data,
                  cpoint.position,
                  shape))
            {
                particle_pool.delete_obj(point);
                return false;
            }

              T newenergy_data;
              if (!data_fun.eval_data(
                          newenergy_data,
                          cpoint.direction,
                          cpoint.position,
                          cpoint.scale))
              {
                  particle_pool.delete_obj(point);
                  return false;
              }

              cpoint.point_cost=newenergy_data;
              cpoint.saliency=newpointsaliency/saliency_correction[2];*/





        }
        break;
        }
//       debug_command=DEBUG_COMMAND::DEBUG_NOTHING;
        delete cmd;
    }


    Vector<T,Dim> get_world_offset() {
        Vector<T,Dim> offset;
        offset=T ( 0 );
        if ( data_fun!=NULL ) {
            std::size_t offset_i[Dim];
            data_fun->getoffset ( offset_i );
            for ( int i=0; i<3; i++ ) {
                offset[i]=offset_i[i];
            }

        }
        return offset;
    }


    bool is_offline() {
        return offline;
    }
    void set_offline ( bool offl ) {
        offline=offl;
    }


    CTracker ( CData<T,TData,Dim> & data_fun,
               CEdgecost<T,Dim> & edgecost_fun,
		Collision<T,Dim> & collision_fun,
               std::size_t pointlimit=10000000 )  : CTracker ( pointlimit ) {

        std::srand ( std::time ( NULL ) );
        this->data_fun=&data_fun;
        this->edgecost_fun=&edgecost_fun;
	this->collision_fun=&collision_fun;
	current_thread_id=0;
        

    }

    ~CTracker() {
        /*
        if (energy_gradient_stat_per_proposal!=NULL)
          delete [] energy_gradient_stat_per_proposal;
        if (state_energy_grad_per_proposal!=NULL)
          delete [] state_energy_grad_per_proposal;

        if (state_energy_grad_update_counter_per_proposal!=NULL)
            delete [] state_energy_grad_update_counter_per_proposal;
        if (state_energy_grad_update_counter_updated_per_proposal!=NULL)
            delete [] state_energy_grad_update_counter_updated_per_proposal;

        if (state_energy_grad_proposal_type!=NULL)
          delete [] state_energy_grad_proposal_type;
        */


        printf("deleting particle pool\n");
        delete particle_pool;
        
        
        if ( tree!=NULL ) {
            printf("deleting tree\n");
            mhs::mex_dumpStringNOW();
            delete tree;
        }
        if ( connecion_candidate_tree!=NULL ) {
            printf("deleting connection candidate  tree\n");
            mhs::mex_dumpStringNOW();
            delete connecion_candidate_tree;
        }
        if ( bifurcation_tree!=NULL ) {
            printf("deleting bifurcation tree\n");
            mhs::mex_dumpStringNOW();
            delete bifurcation_tree;
        }
        

        printf("clearing proposals\n");
        mhs::mex_dumpStringNOW();
        proposal_clear();


        for ( typename std::list<User_action*>::iterator iter=debug_command.begin(); iter!=debug_command.end(); iter++ ) {
            User_action* cmd=*iter;
            delete cmd;
        }

        if ( add_particle_helper_deleteme ) {
            printf("clearing add_particle_helper\n");
            mhs::mex_dumpStringNOW();
            delete add_particle_helper;
        }
    }
    /*
        void track()
        {
          bool running=true;
          #pragma omp parallel sections  num_threads(2) shared(running)
          {

    	#pragma omp section
    	{
    	int count=0;
    	while (running)
    	{
    	 #ifdef  D_USE_GUI
    	  renderer->do_render();
    	  {
    	  renderer->ui();
    	  renderer->update_tracker_controls();
    	  SDL_Delay(1);
    	  }
    	  count++;
    	 #endif
    	}
    	}
    	#pragma omp section
    	{

            std::size_t numproposals=proposals.size();
            T * proposals_map=new T[numproposals];

            for (int i=0; i<numproposals; i++)
                proposals_map[i]=proposals[i].weight;
            for (int i=1; i<numproposals; i++)
                proposals_map[i]=proposals_map[i]+proposals_map[i-1];
            for (int i=0; i<numproposals; i++)
                proposals_map[i]/=proposals_map[numproposals-1];

    	mhs::CtimeStopper timer;

            for (std::size_t i=1; i<=options.opt_numiterations; i++)
            {

    	    while ((renderer->is_pause_program())&&(!renderer->is_exit_program()))
    	    {
    	      SDL_Delay(100);
    	    }
    	    if (renderer->is_exit_program())
    	      i=options.opt_numiterations;

    	    update_temp(i);

                T choice=myrand(1);
                int proposal=-1;
                for (int a=0; a<numproposals; a++)
                {
                    if (proposals_map[a]>=choice)
                    {
                        proposal=a;
                        break;
                    }
                }
                sta_assert_error((proposal<numproposals)&&(proposal>-1));


                try {
                  proposals[proposal].proposal->propose();
                } catch (mhs::STAError & error)
                {
    	        printf("error: %s\n",error.str().c_str());
    		printf("proposal %s died!!\n",proposals[proposal].proposal->get_name().c_str());
                    throw error;
                }
            }
    	state_iteration_time+=timer.get_total_runtime();

    	printf("iteration time: %u seconds\n",(unsigned int)(std::ceil(state_iteration_time)));

            delete [] proposals_map;
    	while (!renderer->is_exit_program())
    	{
    	  SDL_Delay(500);
    	}
    	running=false;
          }
          }
        }*/

//    /* void resolve_collissions()
//     {
//
// //         std::size_t n_points=particle_pool->get_numpts();
// // 	const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
//
//
// // 	 for (unsigned int i=0; i<n_points; i++)
// //         {
// //
// // 	     Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
// //
// // 	    sta_assert_error((point._path_id==-1));
// //
// // 		if ((point.endpoint_connections[0]==0)||(point.endpoint_connections[1]==0))
// // 		{
// // 		  sta_assert_error(point.has_owner(voxgrid_conn_can_id));
// // 		}
// // 	}
//
//
//
// 	std::size_t n_points=particle_pool->get_numpts();
// 	const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
// 	for (unsigned int i=0; i<n_points; i++)
//         {
//             Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
// 	    if (((point.endpoint_connections[0]==0)||(point.endpoint_connections[1]==0))&&
// 	      (point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
// 	    {
// 	      sta_assert_error(point.has_owner(voxgrid_conn_can_id));
// 	    }
// 	}
//
//
// // 	return;
//
//        //_path_id
//       int deleted=0;
//
//
//         for (unsigned int i=0; i<n_points; i++)
//         {
//
// 	     Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
//
// 	    if (options.bifurcation_particle_hard || ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
// 	    {
// 	        if (colliding( *tree,point,collision_search_rad))
// 		{
// 		deleted++;
// 		 point._path_id=1;
// 		}
// 	    }
//         }
//
//         printf("going to delete %d overlapping points \n",deleted);
// 	deleted=0;
// 	int deleted_bif=0;
//
//         for (unsigned int i=0; i<n_points; i++)
//         {
//
// 	     Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
//
// 	    if (point._path_id==1)
// 	    {
//
// 	      if ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
// 		  {
// 		    for (int i=0;i<3;i++)
// 		    {
// 		      Points<T,Dim> & connected_point=*point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point;
// 		      if (connected_point._path_id==-1)
// 		      {
// 			// check if terminal or sth candidate
// 			// (probably need to update voxgrid)
// 			connected_point._path_id=2;
// 		      }
// 		      connections.remove_connection(point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection);
// 		    }
// 		      particle_pool->delete_obj(&point);
// 		      deleted++;
// 		  }else
// 		  {
// 		      for (int i=0;i<2;i++)
// 		      {
//
// 			  if ((point.endpoint_connections[i]==1))
// 			  {
//
// 			      if  (point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
// 			      {
//
// 				    sta_assert_debug0(point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
// 				    Points<T,Dim> & bif_point=*point.endpoints[i][0]->connected->point;
//
// 				    for (int i=0;i<3;i++)
// 				    {
// 				      Points<T,Dim> & connected_point=*bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point;
// 				      if (connected_point._path_id==-1)
// 				      {
// 					// check if terminal or sth candidate
// 					// (probably need to update voxgrid)
// 					connected_point._path_id=2;
// 				      }
// 				      connections.remove_connection(bif_point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection);
// 				    }
// 				      bif_point._path_id=-1;
// 				      particle_pool->delete_obj(&bif_point);
// 				      deleted_bif++;
// 			      }else
// 			      {
// 				Points<T,Dim> & connected_point=*point.endpoints[i][0]->connected->point;
// 				if (connected_point._path_id==-1)
// 				{
// 				  // check if terminal or sth candidate
// 				  // (probably need to update voxgrid)
// 				  connected_point._path_id=2;
// 				}
// 				connections.remove_connection(point.endpoints[i][0]->connection);
// 			      }
// 			  }
//
// 		      }
// 		      particle_pool->delete_obj(&point);
// 		      deleted++;
// 		  }
//
//
// 	      deleted++;
// 	    }
//
// // 	    if ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
// // 	    {
// // // 		if (!particle_pool->is_active_adress(&point))
// // // 		{
// // // 		printf("not a valid edge adress\n");
// // // 		}else
// // // 		{
// // // 		  printf("%d \n",point.get_num_connections());
// // // 		  if (point.get_num_connections()<3)
// // // 		  {
// // // 		    int nc=point.get_num_connections();
// // // 		    for (int k=0;k<nc;k++)
// // // 		    {
// // // 		      Connection<T,Dim>  * con=point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection;
// // // 		      connections.remove_connection(con);
// // // 		    }
// // // 		      particle_pool->delete_obj(&point);
// // // 		      deleted++;
// // // 		  }
// // //
// // //
// // //
// // // 		}
// // 	    }
//         }
//
//         n_points=particle_pool->get_numpts();
// 	particle_ptr=particle_pool->getMem();
//         for (unsigned int i=0; i<n_points; i++)
//         {
//              Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
//
//
//
// 	    if ((point._path_id==2))
// 	    {
//
// 	      sta_assert_error((point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
// 	        if ((point.endpoint_connections[0]==0)||(point.endpoint_connections[1]==0))
// 		{
// 		      if (!point.has_owner(voxgrid_conn_can_id))
// 		      {
// 		      if (!connecion_candidate_tree->insert(point))
// 		      {
// 			  point.print();
// 			  throw mhs::STAError("resolve_collissions: error insering point in connection grid\n");
// 		      }
// 		      }
// 		}
// 		point._path_id=-1;
//
// /*
// 	      if (point.get_num_connections()==1)
// 	      {
// 		if (!connecion_candidate_tree->insert(point))
// 		{
// 		    point.print();
// 		    throw mhs::STAError("resolve_collissions: error insering point in connection grid\n");
// 		}
// 	      }else if (point.get_num_connections()==0)
// 	      {
// 		point.unregister_from_grid(voxgrid_conn_can_id);
// 	      }
// 	      else
// 	      {
// 		throw mhs::STAError("resolve_collissions: error , unexpected number of connections\n");
// 	      }
// 	      point._path_id=-1;*/
//
//
// 	    }
//
// 	}
//
//
// //         for (unsigned int i=0; i<n_points; i++)
// //         {
// //             Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
// // 	    sta_assert_error((point._path_id==-1));
// //
// // 		if ((point.endpoint_connections[0]==0)||(point.endpoint_connections[1]==0))
// // 		{
// // 		  sta_assert_error(point.has_owner(voxgrid_conn_can_id));
// // 		}
// // 	}
//
// 	for (unsigned int i=0; i<n_points; i++)
//         {
//             Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[n_points-1-i]);
// 	     sta_assert_error((point._path_id==-1));
// 	    if (((point.endpoint_connections[0]==0)||(point.endpoint_connections[1]==0))&&
// 	      (point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
// 	    {
// 	      sta_assert_error(point.has_owner(voxgrid_conn_can_id));
// 	    }
// 	}g
//
//         printf("%d overlapping points deleted , %d bifurcation points deleted\n",deleted,deleted_bif);
//
//     }*/


    void update_proposal_map(T * proposals_map, bool init, bool dynamic=false)
    {
        std::size_t numproposals=proposals.size();
	  if (dynamic)
	  {
	    if (init)		
	    {
	       for ( int i=0; i<numproposals; i++ ) {

		  proposals_map[i]=proposals[i].weight;
		  
	      }
	    }else
	    {
	        std::size_t num_edges=connections.pool->get_numpts();
		std::size_t num_particles=particle_pool->get_numpts();
		std::size_t num_bifurcations=connections.get_num_bifurcation_centers();
		std::size_t num_connected_particles=num_particles-connecion_candidate_tree->get_numpts()-num_bifurcations;
		sta_assert_error(num_particles>=num_connected_particles);
		sta_assert_error(num_particles>=num_bifurcations);
		sta_assert_error(num_particles>=(connecion_candidate_tree->get_numpts()+num_bifurcations));
		
		std::size_t num_terminals=connections.get_num_terminals();
		std::size_t num_segments=get_num_graph_segments();
		T  volume_img=shape[0]*shape[1]*shape[2];
		T  volume_particles=volume;
		
		//printf("%f %f\n",volume_img,volume);
	    
	      T * proposals_p=new T[numproposals];    
	      T sum=std::numeric_limits<T>::epsilon();
	      for ( int i=0; i<numproposals; i++ ) {
		  proposals_p[i]=proposals[i].proposal->dynamic_weight(
		      num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles
		  );
		  sum+=proposals_p[i];
	      }
	      
	      //printf("\n[");
	      for ( int i=0; i<numproposals; i++ ) {
		  T dyn_w=std::max(proposals_p[i]/sum,T(0.001));
		  proposals_map[i]=proposals[i].weight*dyn_w;
		  //printf("[%1.3f ]",proposals_p[i]);
	      }
	      //printf("]\n");
	      
	      
	    
	      delete [] proposals_p;
	    }
	  }else
	  {
	      for ( int i=0; i<numproposals; i++ ) {
	      
		  proposals_map[i]=proposals[i].weight;
	      }
	  }
	  
	  
	    for ( int i=1; i<numproposals; i++ ) {
		proposals_map[i]=proposals_map[i]+proposals_map[i-1];
	    }
	    for ( int i=0; i<numproposals; i++ ) {
		proposals_map[i]/=proposals_map[numproposals-1];
	    }
	  
    }


    void track() {
	sta_assert_error(ready==2);
      
        if ( offline ) {
            return;
        }
        
        current_thread_id=get_thread_id();
	
	
        std::size_t numproposals=proposals.size();
        T * proposals_map=new T[numproposals];

	update_proposal_map(proposals_map,true,options.dynamic_weights);
	
//         for ( int i=0; i<numproposals; i++ ) {
//             proposals_map[i]=proposals[i].weight;
//         }
//         for ( int i=1; i<numproposals; i++ ) {
//             proposals_map[i]=proposals_map[i]+proposals_map[i-1];
//         }
//         for ( int i=0; i<numproposals; i++ ) {
//             proposals_map[i]/=proposals_map[numproposals-1];
//         }

        mhs::CtimeStopper timer;

	  update_dynamic_point=false;
	  dynamic_point_scale();
	
/*bool start_debugging=false;*/	
	  
  
        //printf("num iterations: %u\n",options.opt_numiterations);
        for ( iterations=1; options.opt_numiterations>iterations; iterations++ ) {

// 	    if (debug_command!=DEBUG_COMMAND::DEBUG_NOTHING)
	   //TODO remove this feature. pause/sync tracking and execute command from viewer
            if ( debug_command.size() >0 ) {
                execute_me();
            }

            if ( iterations%10==0 ) {
                std::size_t npts=particle_pool->get_numpts();
                std::size_t nedges=connections.pool->get_numpts();
                if ( ( npts+100>pointlimit ) || ( nedges+100>pointlimit ) ) {
                    printf ( "running out of memory, returning to matlab, \n please restart using the currnent state \n (more memory will be allocated next time)\n" );
		    if (real_iterations<1)
		    {
		      real_iterations=iterations;
		    }
		    
                    iterations=options.opt_numiterations;
		    
                }
            }

#ifdef D_USE_GUI
            if ( renderer!=NULL ) {
                while ( ( renderer->is_pause_program() ) && ( !renderer->is_exit_program() ) ) {
                    is_pausing=true;
                    SDL_Delay ( 100 );
                }
                is_pausing=false;

                if ( renderer->is_exit_program() ) {
// 		printf("exiting program\n");
		  if (real_iterations<1)
		    {
		      real_iterations=iterations;
		    }
                    iterations=options.opt_numiterations;
		   
                }
            }
#else
            if ( terminate ) {
// 		printf("exiting program\n");
	      if (real_iterations<1)
		    {
		      real_iterations=iterations;
		    }
                iterations=options.opt_numiterations;
		
            }
#endif


//  check_consisty(false, false);
            update_temp ( );
/* check_consisty(false, false);	   */ 
	    
	    if (opt_temp<options.min_temp)
	    {
	      opt_temp=options.min_temp;
	      if (real_iterations<1)
		    {
		      real_iterations=iterations;
		    }
	      iterations=options.opt_numiterations;
	      
	    }
	    
	    if (options.dynamic_weights)
	    {
		if ( state_eval_state%1000000==0 ) {
		  update_proposal_map(proposals_map,false,options.dynamic_weights);
		}
	    }

            T choice=myrand ( 1 );
            int proposal=-1;
            for ( int a=0; a<numproposals; a++ ) {
                if ( proposals_map[a]>=choice ) {
                    proposal=a;
                    break;
                }
            }
            sta_assert_error ( ( proposal<numproposals ) && ( proposal>-1 ) );


            int check=0;
            try {

	      

      
                connections.increase_stamp();
      
// if (start_debugging)		
// {
//     printf("%s\n",proposals[proposal].proposal->get_name().c_str());
// }

// 		 printf("%s\n",proposals[proposal].proposal->get_name().c_str());
/*printf("\n0\n");
		 
printf("\n1\n");		*/  





//    check_particle_consistensy();

      if (!options.profile)
      {
	check= ( proposals[proposal].proposal->propose() ? 1 : 2 );
      } else
      {
	double currrent_time=timer.get_total_runtime();
	check= ( proposals[proposal].proposal->propose() ? 1 : 2 );
	
	double t=timer.get_total_runtime()-currrent_time;
	proposals[proposal].profiling_time+=t;
	proposals[proposal].profiling_time_max=std::max(proposals[proposal].profiling_time_max,t);
	//proposals[proposal].profiling_time_min=std::min(proposals[proposal].profiling_time_min,t);
	if (proposals[proposal].profiling_time_var_n==0)
	{
	  proposals[proposal].profiling_time_var_k=t; 
	}
	  proposals[proposal].profiling_time_var_n++;
	  
	t-=proposals[proposal].profiling_time_var_k;  
	proposals[proposal].profiling_time_var_sum+=t;
	proposals[proposal].profiling_time_var_sum_sqr+=t*t;
      }

                
//   printf("\n2\n");
// 		check_particle_consistensy();
//   printf("\n3\n");
//if ((proposal+1-1==numproposals)&&(check==2))
// if ((proposal+1-1==numproposals))	      
//   start_debugging=true;
		
		
/*if (start_debugging)		
{
    printf("%s\n",proposals[proposal].proposal->get_name().c_str());
}		
	*/	

	      
    	      //connections.check_consistency2(edgecost_fun);
//   	      check_consisty();
//  		connections.check_consistency4 (*edgecost_fun);

//  	    check_particle_consistensy2();

// check_particle_consistensy4();


  	    

            } catch ( mhs::STAError & error ) {
	        
		printf ( "############### PROPOSAL DIED uhiuhihhhuu ##########\n");
                printf ( "error: %s\n",error.str().c_str() );
		printf ( "iteration: %d\n" ,iterations);
                printf ( "proposal was sucessfull ?  %d\n",check==1 );
                printf ( "proposal %s died!!\n",proposals[proposal].proposal->get_name().c_str() );
		printf ( "############### PROPOSAL DIED uhiuhihhhuu ##########\n");
                throw error;
            }
        }
        
        if (real_iterations<1)
	{
        real_iterations=iterations;
	}
        
        state_iteration_time+=timer.get_total_runtime();

        printf ( "iteration time: %u seconds\n", ( unsigned int ) ( std::ceil ( state_iteration_time ) ) );

        delete [] proposals_map;

#ifdef D_USE_GUI
        //watinign for user to exit
        if ( renderer!=NULL ) {
            while ( !renderer->is_exit_program() ) {
                SDL_Delay ( 500 );
            }
        }
#endif
    }
    T compute_costs() {
        try {

            std::size_t n_points=particle_pool->get_numpts();
            const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
            printf ( "#############################################\n" );
            printf ( "total points in particle pool: %u\n",n_points );

            T costs_bifurcations=0;
            T costs_lines=0;


            for ( unsigned int i=0; i<n_points; i++ ) {
                Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );

                switch ( point.particle_type ) {
                case PARTICLE_TYPES::PARTICLE_UNDEFINED: {
                    throw mhs::STAError ( "found PARTICLE_TYPES::PARTICLE_UNDEFINED" );
                }
                break;
// 	      case PARTICLE_TYPES::PARTICLE_SEGMENT:
// 	      {
// 		typename CEdgecost<T,Dim>::EDGECOST_STATE  inrange;
// 		T c=point.edge_energy(*edgecost_fun,
// 		  options.connection_bonus_L,
// 		  options.bifurcation_bonus_L,
// 		  maxendpointdist,
// 		  inrange)-options.connection_bonus_L;
//
// 		printf("%4.1d L %4.8f (%d)\n",i,c,static_cast<int>(inrange));
// 		costs_lines+=c;
//
//
// 	      }break;
                case PARTICLE_TYPES::PARTICLE_BIFURCATION: {
                } break;
                case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER: {

                    typename CEdgecost<T,Dim>::EDGECOST_STATE  inrange;
                    T c=point.edge_energy ( *edgecost_fun,
                                            options.connection_bonus_L,
                                            options.bifurcation_bonus_L,
                                            maxendpointdist,
                                            inrange )-options.bifurcation_bonus_L;

                    printf ( "%4.1d S %4.8f (%d)\n",i,c,static_cast<int> ( inrange ) );
                    costs_bifurcations+=c;
                }
                break;
                }
            }

            printf ( "#############################################\n" );

            printf ( "Total costs     %4.8f\n",costs_bifurcations+costs_lines );
            printf ( "Total costs bif %4.8f\n",costs_bifurcations );
            printf ( "Total costs lin %4.8f\n",costs_lines );

            printf ( "#############################################\n" );

        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }


    void print_proposal_stat() {
      
	
        std::size_t numproposals=proposals.size();
        
	  if (options.profile)
	  {
	    typedef struct
	    {
	      double t;
	      int indx;
	    } T_time_sort;
	    
	    
	    
	    std::list<T_time_sort> ctime;
	    for ( int i=0; i<numproposals; i++ ) {
	      T_time_sort t;
	      //t.t=(proposals[i].profiling_time/proposals[i].proposal->proposal_called);
	      double mean=((proposals[i].profiling_time/proposals[i].proposal->proposal_called));
// 	      double variance=(proposals[i].profiling_time_var_sum_sqr - (proposals[i].profiling_time_var_sum*proposals[i].profiling_time_var_sum)/
// 		    proposals[i].profiling_time_var_n)/(proposals[i].profiling_time_var_n-1);
	      t.t=mean;
	      t.indx=i;
	      ctime.push_back(t);
	    }
	    
	    
	    ctime.sort([](const T_time_sort & a, const T_time_sort & b) { return a.t < b.t; });
	    

	    for ( typename std::list<T_time_sort>::iterator iter=ctime.begin();iter!=ctime.end();iter++) {
		  int i=iter->indx;
	      
		  double mean=((proposals[i].profiling_time/proposals[i].proposal->proposal_called));
		  double variance=(proposals[i].profiling_time_var_sum_sqr - (proposals[i].profiling_time_var_sum*proposals[i].profiling_time_var_sum)/
		    proposals[i].profiling_time_var_n)/(proposals[i].profiling_time_var_n-1);
	     printf ( "proposal %s, t: [%.4f : %.4f : %.4f]  (10^4 ms) [%.4f],  calls %10d, accept %10d / %3d\%\n",
                     proposals[i].proposal->get_name().c_str(),
		     std::max((mean-std::sqrt(variance)),double(0))*10000,
		      mean*10000,
		     (mean+std::sqrt(variance))*10000,
		      ((proposals[i].profiling_time_max))*10000,
                     proposals[i].proposal->proposal_called,
                     proposals[i].proposal->proposal_accepted,
                     ( int ) std::ceil ( 100*double ( proposals[i].proposal->proposal_accepted ) / ( std::numeric_limits<T>::epsilon() +double ( proposals[i].proposal->proposal_called ) ) )
                   );
	     }
	  }else 
	  {
	    for ( int i=0; i<numproposals; i++ ) {
            printf ( "proposal %s, calls %10d, accept %10d / %3d\%\n",
                     proposals[i].proposal->get_name().c_str(),
                     proposals[i].proposal->proposal_called,
                     proposals[i].proposal->proposal_accepted,
                     ( int ) std::ceil ( 100*double ( proposals[i].proposal->proposal_accepted ) / ( std::numeric_limits<T>::epsilon() +double ( proposals[i].proposal->proposal_called ) ) )
                   );
	    }
	  }
        
    }




    void init_tracker_basic ( const mxArray * feature_struct,
                              const mxArray * tracker_state=NULL,
                              const mxArray * params=NULL ) {
//       printf ( ":-)\n" );
	  mhs::mex_dumpStringNOW();
        sta_assert_error ( tree==NULL );
        sta_assert_error ( init_state==0 );
	
	
	
	if (feature_struct==NULL)
	{
	  ready=1;  
	}else
	{
	  sta_assert_error ( feature_struct!=NULL );
	  sta_assert_error ( mxIsStruct ( feature_struct ) );
	}


        /*#####################################
         *
         * 	loading image data
         *
         * ####################################*/

	//if (feature_struct!=NULL)
	{
	  printf ( "init volume\n" );
	  mhs::mex_dumpStringNOW();
	  try {
	      //load_image_data ( feature_struct );
	    init_shape_and_scale();
	  } catch ( mhs::STAError & error ) {
	      throw error;
	  }
	}


        /*#####################################
         *
         * 	setting user parameters
         *
         * ####################################*/

        printf ( "setting parameters\n" );
	mhs::mex_dumpStringNOW();
        try {
            set_params ( params );
        } catch ( mhs::STAError & error ) {
            throw error;
        }


        /*#####################################
        *
        * 	loading tracker data (if exist)
        *
        * ####################################*/

        if ( tracker_state!=NULL ) {
            printf ( "found previous state\n" );
	    mhs::mex_dumpStringNOW();
            sta_assert_error ( mxIsStruct ( tracker_state ) );
            try {
                opt_temp=* ( mhs::mex_getFieldPtr<T> ( tracker_state,"opt_temp" ) );
                opt_temp_conn_cost=* ( mhs::mex_getFieldPtr<T> ( tracker_state,"opt_temp_conn_cost" ) );
                mx2state ( tracker_state );

            } catch ( mhs::STAError & error ) {
                throw error;
            }

            try {
                offline=* ( mhs::mex_getFieldPtr<T> ( tracker_state,"offline" ) );
            } catch ( mhs::STAError & error ) {
                // do nothing
            }

        }


        
        collision_search_rad=maxscale*2+0.1;
        if ( std::abs ( Points<T,Dim>::thickness ) >1 ) {
            collision_search_rad=std::abs ( Points<T,Dim>::thickness ) *maxscale*2+0.1;
        }
//         T search_connection_point_candidate=options.connection_candidate_searchrad;
        maxendpointdist=options.connection_candidate_searchrad*options.connection_candidate_searchrad;

        search_connection_point_center_candidate=T ( 2 ) *maxscale+options.connection_candidate_searchrad+0.1;

       

/*printf("tree 0\n");       */ 
        
	if (true)
	{
	  
	   if ( options.opt_grid_spacing<0 ) {
            options.opt_grid_spacing=search_connection_point_center_candidate/2;
	      }

	  printf("point limit :%d\n",pointlimit);    
	      mhs::mex_dumpStringNOW();    
	      
	  printf("particles / voxel :%f\n",options.opt_particles_per_voxel);    
	      mhs::mex_dumpStringNOW();
	  
	  printf("init main grid:\n");
	  mhs::mex_dumpStringNOW();
	  tree=new OctTreeNode<T,Dim> ( shape,options.opt_grid_spacing,
					options.opt_particles_per_voxel,0,pointlimit );
  /*printf("tree 1\n");     */   
	  //printf("SEARCH RAD: %f %f\n",collision_search_rad,search_connection_point_center_candidate);
	  
	  //NOTE in the worst case, we need as much capacity then for tree
	  T particle_per_voxel=T(options.opt_particles_per_voxel*std::pow(options.opt_grid_spacing,T(3)))
	  /std::pow(T(collision_search_rad*1.5),T(3));
	  
      
//       printf("###################### \n");
//       printf("###################### \n");
//       printf("###################### %f \n",particle_per_voxel);
//       printf("###################### \n");
//       printf("###################### \n");
      
	  printf("init edge grid:\n");
	  mhs::mex_dumpStringNOW();
	  connecion_candidate_tree=new OctTreeNode<T,Dim> ( shape,collision_search_rad*1.5,particle_per_voxel,voxgrid_conn_can_id,pointlimit );
	  
  /*printf("tree 2\n");   */     	
  
	printf("init bifurcation grid:\n");
	mhs::mex_dumpStringNOW();
	  bifurcation_tree=new OctTreeNode<T,Dim> ( shape,collision_search_rad*1.5,options.opt_particles_per_voxel,voxgrid_bif_center_id,pointlimit );
  /*printf("tree done\n");  */      	
	  //bifurcation_tree=new OctTreeNode<T,Dim> ( shape,collision_search_rad*1.5,options.opt_particles_per_voxel,voxgrid_bif_center_id,pointlimit );
	
	
	}else
	{
	  sta_assert_error(( options.opt_grid_spacing<0 ));
	 tree=new OctTreeNode<T,Dim> ( shape,collision_search_rad+0.5,
				      options.opt_particles_per_voxel,0,pointlimit );
	 
        connecion_candidate_tree=new OctTreeNode<T,Dim> ( shape,collision_search_rad+0.5,options.opt_particles_per_voxel,voxgrid_conn_can_id,pointlimit );
	bifurcation_tree=new OctTreeNode<T,Dim> ( shape,collision_search_rad+0.5,options.opt_particles_per_voxel,voxgrid_bif_center_id,pointlimit ); 
	  
	}
	mhs::mex_dumpStringNOW();
	printf("\n");
	
    }


    void init_tracker_add_graph ( const mxArray * feature_struct,
                                  const mxArray * tracker_state=NULL,
                                  const mxArray * params=NULL,
				  std::size_t offset[] = NULL
				) {
        sta_assert_error ( tree!=NULL );
        sta_assert_error ( init_state==0 );
	
	if (feature_struct==NULL)
	{
	  ready=1;  
	}else
	{
	  sta_assert_error ( feature_struct!=NULL );
	  sta_assert_error ( mxIsStruct ( feature_struct ) );
	}
	
        std::size_t n_points_old=particle_pool->get_numpts();
	std::size_t n_edges_old=connections.pool->get_numpts();

        mhs::dataArray<T> treedata;
        mhs::dataArray<T> conncection_data;

        if ( tracker_state!=NULL ) {
            sta_assert_error ( mxIsStruct ( tracker_state ) );
            try {
                treedata=mhs::dataArray<T> ( tracker_state,"data" );
                conncection_data=mhs::dataArray<T> ( tracker_state,"connections" );
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

        Points<T,Dim> * loaded_points=NULL;
        try {
            /*!
            *
            * 	init existing state
            *
            *
            * */
            if ( treedata.data!=NULL ) {
                T * ptreedata=treedata.data;
                std::size_t numpoints=treedata.dim[0];

                sta_assert_error ( numpoints+n_points_old<=pointlimit );
                //sta_assert_error(treedata.dim[1]==POINT_ATTRIBUTES::POINT_A_Count);
                //TODO add this check again. temporarily removed
                sta_assert_error ( treedata.dim[1]>POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG );


                printf ( "initializing points ... " );
                for ( std::size_t i=0; i<numpoints; i++ ) {
/*printf("%d %d\n",i,numpoints);       */             
                    loaded_points=particle_pool->create_obj();

// printf("A");
		    if (offset!=NULL)
		    {
		    loaded_points->set_pos_dir_scale (
                        Vector<T,3> ( ptreedata[POINT_ATTRIBUTES::POINT_A_POSX]+offset[2],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_POSY]+offset[1],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_POSZ]+offset[0] ),
                        Vector<T,3> ( ptreedata[POINT_ATTRIBUTES::POINT_A_DIRX],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_DIRY],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_DIRZ] ),
                        ptreedata[POINT_ATTRIBUTES::POINT_A_SCALE]
                    );  
		    }else
		    {

                    loaded_points->set_pos_dir_scale (
                        Vector<T,3> ( ptreedata[POINT_ATTRIBUTES::POINT_A_POSX],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_POSY],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_POSZ] ),
                        Vector<T,3> ( ptreedata[POINT_ATTRIBUTES::POINT_A_DIRX],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_DIRY],
                                      ptreedata[POINT_ATTRIBUTES::POINT_A_DIRZ] ),
                        ptreedata[POINT_ATTRIBUTES::POINT_A_SCALE]
                    );
		    }
/*printf("B");	*/	    
		    
		    loaded_points->ref_position=loaded_points->get_position();
		    loaded_points->ref_scale=loaded_points->get_scale();
		    
		    update_volume(loaded_points->get_scale());

                    loaded_points->tracker_birth=ptreedata[POINT_ATTRIBUTES::POINT_A_BIRTH_FLAG];
		    


                    loaded_points->point_cost= ( ptreedata[POINT_ATTRIBUTES::POINT_A_COST] );
                    loaded_points->saliency= ( ptreedata[POINT_ATTRIBUTES::POINT_A_SALIENCY] );

/*printf("C");          */          
                    if ( treedata.dim[1]==POINT_ATTRIBUTES::POINT_A_Count ) {
                        loaded_points->freezed= ( ptreedata[POINT_ATTRIBUTES::POINT_A_FREEZED] );
                        loaded_points->freezed_endpoints[0]= ( ptreedata[POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A] );
                        loaded_points->freezed_endpoints[1]= ( ptreedata[POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B] );
			loaded_points->freeze_topology=ptreedata[POINT_ATTRIBUTES::POINT_A_PROTECTED_TOPOLOGY];
                    }

                    //loaded_points->particle_type=static_cast<PARTICLE_TYPES>((unsigned int)(ptreedata[POINT_ATTRIBUTES::POINT_A_TYPE]));
                    //particle type will be determined automatically
                    loaded_points->particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;


// 		    loaded_points->position-=get_world_offset();
                    loaded_points->set_position ( loaded_points->get_position()-get_world_offset() );
		    
		    //NOTE I hope no harm ?
		    loaded_points->_path_id=ptreedata[POINT_ATTRIBUTES::POINT_A_PATH_ID];



                    PARTICLE_TYPES particle_type=static_cast<PARTICLE_TYPES> ( ( unsigned int ) ( ptreedata[POINT_ATTRIBUTES::POINT_A_TYPE] ) );


		    const Vector<T,3> & position=loaded_points->get_position();

/*printf("D");  */                  
                    if ( ( position[0]<0 ) ||
                            ( position[1]<0 ) ||
                            ( position[2]<0 ) ||
                            ( ! ( position[0]<shape[0] ) ) ||
                            ( ! ( position[1]<shape[1] ) ) ||
                            ( ! ( position[2]<shape[2] ) ) ) {
                        printf ( "out of bounce: shape [%d %d %d]\n",shape[0],shape[1],shape[2] );
                        position.print();
                    }
		    

                    OctPoints<T,Dim> * opoint= ( OctPoints<T,Dim> * ) loaded_points;

		    
		    

                    //printf("type: %d\n",static_cast<int>(particle_type));
                    //if ( ( options.bifurcation_particle_hard ) || ( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
		    if (options.opt_spawn_blobs)
		    {
		      if ( particle_type==PARTICLE_TYPES::PARTICLE_BLOB )
		      {
			
			loaded_points->particle_type=particle_type;

			  if ( !tree->insert ( *opoint ) ) {
			      loaded_points->print();
			      throw mhs::STAError ( "error insering point (PARTICLE_BLOB)\n" );

			  }
		      }
		    }else
		    {
		      if ( (  ( options.bifurcation_particle_soft )  || ( options.bifurcation_particle_hard ) || ( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) )&&
			( particle_type!=PARTICLE_TYPES::PARTICLE_BLOB )
		      ) {

			  if ( !tree->insert ( *opoint ,false,false,true,true) ) {
			      loaded_points->print();
			      throw mhs::STAError ( "error inserting point\n" );

			  }
		      }
		    }
		    
/*printf("E");	*/	    
                    loaded_points->init_pointer();
                    
// printf("F");                    
                    loaded_points->_point_id=i;

//                     const Vector<T,3> & position=loaded_points->get_position();
// 
//                     if ( ( position[0]<0 ) ||
//                             ( position[1]<0 ) ||
//                             ( position[2]<0 ) ||
//                             ( ! ( position[0]<shape[0] ) ) ||
//                             ( ! ( position[1]<shape[1] ) ) ||
//                             ( ! ( position[2]<shape[2] ) ) ) {
//                         printf ( "out of bounce: shape [%d %d %d]\n",shape[0],shape[1],shape[2] );
//                         position.print();
//                     }

                    ptreedata+=treedata.dim[1];
                }
                printf ( "done\n" );




                T * pconncection_data=conncection_data.data;


                //WARNING particle_ptr in inverted order!!
                std::size_t n_points=particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
                sta_assert_error_c ( numpoints+n_points_old==n_points,printf("[%d %d %d]\n",numpoints,n_points_old,n_points));
                {
                    class OctPoints<T,Dim> ** index_;
                    unsigned int  npoints_;
                    tree->get_pointlist ( index_,npoints_ );
                    sta_assert_error ( n_points>=npoints_ );
                    printf ( "points pool: %d tree: %d\n",n_points,npoints_ );
                }


                std::size_t num_connections=conncection_data.dim[0];
                //sta_assert_error ( conncection_data.dim[1]==EDGE_ATTRIBUTES::EDGE_A_Count );
		sta_assert_error ( conncection_data.dim[1]>=5);

                sta_assert_error ( num_connections+n_edges_old<=pointlimit );

                printf ( "initializing edges ... " );
                for ( std::size_t i=0; i<num_connections; i++ ) {

                    std::size_t pointidxA=pconncection_data[EDGE_ATTRIBUTES::EDGE_A_PINDXA]+n_points_old;
                    int point_sideA=pconncection_data[EDGE_ATTRIBUTES::EDGE_A_SIDEA];
                    std::size_t pointidxB=pconncection_data[EDGE_ATTRIBUTES::EDGE_A_PINDXB]+n_points_old;
                    int point_sideB=pconncection_data[EDGE_ATTRIBUTES::EDGE_A_SIDEB];

                    sta_assert_error ( pointidxA>=0 );
                    sta_assert_error ( pointidxA<n_points );
                    sta_assert_error ( pointidxB>=0 );
                    sta_assert_error ( pointidxB<n_points );
                    class Connection<T,Dim> connection_new;
                    class Points<T,Dim> * pointA= ( class Points<T,Dim> * ) ( particle_ptr[n_points-1-pointidxA] );
                    class Points<T,Dim> * pointB= ( class Points<T,Dim> * ) ( particle_ptr[n_points-1-pointidxB] );
                    connection_new.pointA=pointA->getfreehub ( point_sideA );
                    connection_new.pointB=pointB->getfreehub ( point_sideB );

                    connection_new.edge_type=static_cast<EDGE_TYPES> ( pconncection_data[EDGE_ATTRIBUTES::EDGE_A_TYPE] );

		    if ( conncection_data.dim[1]==EDGE_ATTRIBUTES::EDGE_A_Count )
		    {
		      connection_new.freeze_topology=pconncection_data[EDGE_ATTRIBUTES::EDGE_A_PROTECTED_TOPOLOGY];
		    }
		    
		    
                    if (( maxendpointdist>-1 )&&(!options.opt_spawn_blobs)) {
                        //TODO this might not be true (but is required) for
                        //the  new bif particle
                        if ( connection_new.edge_type!=EDGE_TYPES::EDGE_BIFURCATION ) {
                            if ( ! ( maxendpointdist> ( *connection_new.pointA->position-*connection_new.pointB->position ).norm2() ) ) {
                                printf ( "%f %f\n",maxendpointdist, ( *connection_new.pointA->position-*connection_new.pointB->position ).norm2() );
                            }
                            sta_assert_error ( maxendpointdist> ( *connection_new.pointA->position-*connection_new.pointB->position ).norm2() );
                        } else {
                            sta_assert_error ( 4*maxendpointdist> ( *connection_new.pointA->position-*connection_new.pointB->position ).norm2() );
                        }
                    }


                    sta_assert_error ( connection_new.pointA!=NULL );
                    sta_assert_error ( connection_new.pointB!=NULL );
		    
		    
		    sta_assert_error ( connection_new.pointA->point!=connection_new.pointB->point );
		    
		    if (( connection_new.edge_type!=EDGE_TYPES::EDGE_BIFURCATION ))
		    {
		      if(!(edgecost_fun->particle_angle_valid(*(connection_new.pointA),*(connection_new.pointB))))
 		      {
			
			printf("Did you change the parameter AngleScale ?\n");
// 			connection_new.pointA->point->get_position().print();
// 			connection_new.pointA->point->get_direction().print();
// 			connection_new.pointB->point->get_position().print();
// 			connection_new.pointB->point->get_direction().print();
// 			//printf("ERROR: %d \n",i);
//  			sta_assert_error (edgecost_fun->particle_angle_valid(*(connection_new.pointA),*(connection_new.pointB)));
 		      }
		    }
		    
		    
		    
		    
                    connections.add_connection ( connection_new );
                    pconncection_data+=conncection_data.dim[1];
                }
                printf ( "done\n" );
                tree->print();


                connecion_candidate_tree->print();
            }
        } catch ( mhs::STAError & error ) {
            if ( error.getCode() ==mhs::STA_RESULT_POINTLIMIT ) {
                loaded_points->print();
                delete loaded_points;
            }
            throw error;
        }
    }
    
    
    
    void init_tracker_seeds ( const mxArray * feature_struct,
                                  const mxArray * seeds=NULL,
                                  const mxArray * params=NULL,
				  std::size_t offset[] = NULL
				) {
        sta_assert_error ( tree!=NULL );
        sta_assert_error ( init_state==0 );
	
        if (feature_struct==NULL)
        {
        ready=1;  
        }else
        {
        sta_assert_error ( feature_struct!=NULL );
        sta_assert_error ( mxIsStruct ( feature_struct ) );
        }
	
        std::size_t n_points_old=particle_pool->get_numpts();
	std::size_t n_edges_old=connections.pool->get_numpts();

        mhs::dataArray<T> seed_data;

        
        if ( seeds!=NULL ) {
            sta_assert_error ( mxIsStruct ( seeds ) );
            try {
                seed_data = mhs::dataArray<T> ( seeds,"data" );
            } catch ( mhs::STAError & error ) {
                throw error;
            }
        }

        Points<T,Dim> * loaded_points=NULL;
        try {
            /*!
            *
            * 	init existing state
            *
            *
            * */
            if ( seed_data.data!=NULL ) {
                T * pseed_data=seed_data.data;
                std::size_t numpoints=seed_data.dim[0];

                sta_assert_error ( numpoints+n_points_old<=pointlimit );
                sta_assert_error ( seed_data.dim[1] == 7 );


                printf ( "initializing points ... " );
                for ( std::size_t i=0; i<numpoints; i++ ) {
/*printf("%d %d\n",i,numpoints);       */             
                    loaded_points=particle_pool->create_obj();

// printf("A");
                    if (offset!=NULL)
                    {
                    loaded_points->set_pos_dir_scale (
                                Vector<T,3> ( pseed_data[POINT_ATTRIBUTES::POINT_A_POSX]+offset[2],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_POSY]+offset[1],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_POSZ]+offset[0] ),
                                Vector<T,3> ( pseed_data[POINT_ATTRIBUTES::POINT_A_DIRX],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_DIRY],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_DIRZ] ),
                                pseed_data[6]
                            );  
                    }else
                    {

                            loaded_points->set_pos_dir_scale (
                                Vector<T,3> ( pseed_data[POINT_ATTRIBUTES::POINT_A_POSX],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_POSY],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_POSZ] ),
                                Vector<T,3> ( pseed_data[POINT_ATTRIBUTES::POINT_A_DIRX],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_DIRY],
                                            pseed_data[POINT_ATTRIBUTES::POINT_A_DIRZ] ),
                                pseed_data[6]
                            );
                    }
/*printf("B");	*/	    
		    
                loaded_points->ref_position=loaded_points->get_position();
                loaded_points->ref_scale=loaded_points->get_scale();
                
                update_volume(loaded_points->get_scale());

                        loaded_points->tracker_birth=0;
                        loaded_points->point_cost= 1;
                        loaded_points->saliency= 1;
                        loaded_points->freezed= true;
                        
                        loaded_points->particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
                        loaded_points->set_position ( loaded_points->get_position()-get_world_offset() );
                
                //NOTE I hope no harm ?
                //loaded_points->_path_id=-1;
                loaded_points->_path_id=i;
                
                  const Vector<T,3> & position=loaded_points->get_position();

/*printf("D");  */                  
                    if ( ( position[0]<0 ) ||
                            ( position[1]<0 ) ||
                            ( position[2]<0 ) ||
                            ( ! ( position[0]<shape[0] ) ) ||
                            ( ! ( position[1]<shape[1] ) ) ||
                            ( ! ( position[2]<shape[2] ) ) ) {
                        printf ( "out of bounce: shape [%d %d %d]\n",shape[0],shape[1],shape[2] );
                        position.print();
                    }
		    

                    OctPoints<T,Dim> * opoint= ( OctPoints<T,Dim> * ) loaded_points;


                if ( !tree->insert ( *opoint ,false,false,true,true) ) {
                    loaded_points->print();
                    throw mhs::STAError ( "error inserting point\n" );

                }
                        loaded_points->init_pointer();
                        
                        loaded_points->_point_id=i;


                        pseed_data+=seed_data.dim[1];
                    }
                    printf ( "done\n" );
            }
        } catch ( mhs::STAError & error ) {
            if ( error.getCode() ==mhs::STA_RESULT_POINTLIMIT ) {
                loaded_points->print();
                delete loaded_points;
            }
            throw error;
        }
    }


    void init_tracker_proposals ( const mxArray * feature_struct,
                                  const mxArray * tracker_state=NULL,
                                  const mxArray * params=NULL ) {
        sta_assert_error ( tree!=NULL );
        sta_assert_error ( init_state==0 );
        sta_assert_error ( feature_struct!=NULL );
        sta_assert_error ( mxIsStruct ( feature_struct ) );
        // INIT PROPOSALS
        try {

            // check if proposal parameters exist
            mxArray * proposal_options=NULL;
            //printf ( "###################\n");  
            //printf ( "###################\n");  
            //printf ( "###################\n");  
            if ( params!=NULL ) {
                //printf ( "params\n");  
                if ( mhs::mex_hasParam ( params,"proposals" ) !=-1 ) {
                  //  printf ( "proposal\n");  
                    proposal_options=mhs::mex_getParamPtr ( params,"proposals" );
                    if ( !mxIsCell ( proposal_options ) ) {
                    //    printf ( "cell\n");  
                        proposal_options=NULL;
                    }
                }
            }

            
            for ( int i=0; i<nproposal_names; i++ ) {
                std::string pname=proposal_names[i];
                
//                 if ( proposal_options!=NULL ) {
//                     
//                     bool isok = (mhs::mex_hasParam ( proposal_options,strtrim ( pname ) ) !=-1 );
//                     printf ( "checking for proposal %s ? : %d \n",pname.c_str(),isok ? 1 : 0  );
//                 }
                
                //printf ( "checking for proposal %s:\n",trtrim(pname).c_str());
                T weight_do=-1;
		T weight_undo=-1;
        
                if ( proposal_options!=NULL ) {
                    
                    //printf ( "has options\n");  
                    if ( mhs::mex_hasParam ( proposal_options,strtrim ( pname ) ) !=-1 ) {
                      //  printf ( "has string\n");  
                      

              
              
			std::vector<T> weights= mhs::mex_getParam<T> ( proposal_options,strtrim ( pname ));
			sta_assert_error(weights.size()<3);
			sta_assert_error(weights.size()>0);
			if (weights.size()==1)
			{
			  weight_do=weights[0];
			  weight_undo=weights[0];
			}else 
			{
			  weight_do=weights[0];
			  weight_undo=weights[1];
			}
                        //weight=mhs::mex_getParam<T> ( proposal_options,strtrim ( pname ),1 ) [0];
			
                    }
                }
                
                //printf ( "checking for proposal %s %f %f:\n",pname.c_str(),weight_do,weight_undo);

                
                if ( ( weight_do>0 ) || ( weight_do==-1 ) ) {

                    //printf("creating proposal %s with weight %f\n",pname.c_str(),weight);
                    CProposalOptions new_proposal=proposal_register ( pname,weight_do,weight_undo );

                    if ( PROPOSAL_TYPES::PROPOSAL_BIRTH==string2enum ( pname ) ) {
                        sta_assert_error ( add_particle_helper==NULL );
                        add_particle_helper= ( CBirth<TData,T,Dim> * ) new_proposal.proposal;
                        add_particle_helper_deleteme=false;
                    }
                }
            }



// 	    for (typename std::vector<class CProposalOptions>::iterator iter = proposals.begin();
//                     iter!=proposals.end(); iter++)
// 		 {
// 		   class CProposal<TData,T,Dim> & proposal=*(iter->proposal);
// 		   printf("proposals: %s\n",strtrim(proposal.get_name()).c_str());
// 		 }
            //printf("\n");

//             unsigned int pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_BIRTH);
//             unsigned int pB=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_DEATH);
//             sta_assert_error(std::abs(proposals[pA].weight-proposals[pB].weight)<std::numeric_limits<T>::epsilon());
//             pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH);
//             pB=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH);
//             sta_assert_error(std::abs(proposals[pA].weight-proposals[pB].weight)<std::numeric_limits<T>::epsilon());
//             pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH);
//             pB=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH);
//             sta_assert_error(std::abs(proposals[pA].weight-proposals[pB].weight)<std::numeric_limits<T>::epsilon());

// 		pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH);
// 		proposals[pA].weight=0;
// 		pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH);
// 		proposals[pA].weight=0;
//  		pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH);
//  		proposals[pA].weight=0;
// 		pA=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH);
//  		proposals[pA].weight=0;

            printf ( "initializing %d proposal " ,proposals.size());
            for ( typename std::vector<class CProposalOptions>::iterator iter = proposals.begin();
                    iter!=proposals.end(); iter++ ) {
                class CProposal<TData,T,Dim> & proposal=* ( iter->proposal );
                //printf("initializing proposal %s\n",proposal.get_name().c_str());
                printf ( "[%s]",strtrim ( proposal.get_name() ).c_str() );
                proposal.init ( feature_struct );
                proposal.set_params ( params );
            }
            printf ( "\n" );


            if ( add_particle_helper==NULL ) {
                add_particle_helper= ( CBirth<TData,T,Dim> * ) CProposal<TData,T,Dim>::proposal_create_from_name ( "BIRTH" );
                add_particle_helper->set_tracker ( *this );
                add_particle_helper->init ( feature_struct );
                add_particle_helper->set_params ( params );
                add_particle_helper_deleteme=true;
                sta_assert_error ( add_particle_helper!=NULL );
            }


            {
                std::size_t n_points=particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
                printf ( "updating point saliencies ... " );
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
// 		  printf("%f ->",point.saliency);
                    add_particle_helper-> helper_update_particle_saliency ( point );
// 		  printf("%f\n",point.saliency);
                }
                printf ( "done\n " );
            }



            //edge2particle_stat=new CStats(100,false);


            /*
            energy_gradient_stat_per_proposal = new CStats[nproposal_names];
            state_energy_grad_per_proposal= new double[nproposal_names];
            state_energy_grad_update_counter_per_proposal=new std::size_t[nproposal_names];
            state_energy_grad_update_counter_updated_per_proposal= new bool[nproposal_names];
            state_energy_grad_proposal_type=new int[nproposal_names];


            for (int i=0;i<nproposal_names;i++)
            {
              energy_gradient_stat_per_proposal[i].init(1000,true);
              state_energy_grad_update_counter_per_proposal[i]=0;
              state_energy_grad_update_counter_updated_per_proposal[i]=false;
              std::string pname=proposal_names[i];
              state_energy_grad_proposal_type[i]=static_cast<unsigned int>(string2enum(pname));
            }
            */

        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }



     void init_graph_structure()
     {
      sta_assert_error ( init_state==0 );
        try {
            
             
	  
	    if (!options.opt_spawn_blobs)
            {
                printf("adding points to grids\n");
                mhs::mex_dumpStringNOW();
                
                std::size_t n_points=particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[n_points-1-i] );
                    if ( ( ( point.endpoint_connections[0]==0 ) || ( point.endpoint_connections[1]==0 ) ) &&
                            ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
                        if ( !connecion_candidate_tree->insert ( point ) ) {
                            connecion_candidate_tree->print();
                            throw mhs::STAError ( "error inserting point in connect cand tree \n" );

                        }
                    }
                    if ( (  point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
                        if ( !bifurcation_tree->insert ( point ) ) {
                            bifurcation_tree->print();
                            throw mhs::STAError ( "error inserting point in bif center tree \n" );

                        }
                    }
                }
            }
// printf("--A2--\n");
            {
                printf("checking points\n");
                mhs::mex_dumpStringNOW();
                
                std::size_t n_points=particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[n_points-1-i] );
		    
		    if (!options.opt_spawn_blobs)
		    {
                    if ( ( ( point.endpoint_connections[0]==0 ) || ( point.endpoint_connections[1]==0 ) ) &&
                            ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
                        sta_assert_error ( point.has_owner ( voxgrid_conn_can_id ) );
                    }
                    if ( (  point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
		         sta_assert_error ( point.has_owner ( voxgrid_bif_center_id) );
		    }
		      
		    }
		    
                }
            }


// printf("--A--\n");
            connecion_candidate_tree->print();
	    
	    //check_consisty();
// printf("--B--\n");
//#ifdef _DEBUG_CHECKS_0_
	     if (!options.opt_spawn_blobs)
            {
                printf("init points\n");
                mhs::mex_dumpStringNOW();
	       bool warned=false;
	      
                std::size_t n_points=particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();

                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[n_points-1-i] );

                    switch ( point.particle_type ) {
                    case PARTICLE_TYPES::PARTICLE_UNDEFINED: {
                        throw mhs::STAError ( "found PARTICLE_TYPES::PARTICLE_UNDEFINED" );
                    }
                    break;
                    case PARTICLE_TYPES::PARTICLE_SEGMENT: {
                        sta_assert_error ( point.has_owner() );
                        for ( int i=0; i<2; i++ ) {
                            if ( point.endpoint_connections[i]>0 ) {
                                sta_assert_error_c ( point.endpoint_connections[i]==1, printf("##############\nnumconn: %d\n##############\n",point.endpoint_connections[i]) );
                                sta_assert_error ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_SEGMENT );
                            }
                        }
                    }
                    break;
                    case PARTICLE_TYPES::PARTICLE_BIFURCATION: {
                        sta_assert_error ( point.has_owner() );
                        sta_assert_error ( point.get_num_connections() >0 );
                        bool foundbif=false;
                        for ( int i=0; i<2; i++ ) {
                            if ( point.endpoint_connections[i]>0 ) {
                                sta_assert_error ( point.endpoint_connections[i]==1 );
                                if ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
                                    foundbif=true;
                                }
                            }
                        }
                        sta_assert_error ( foundbif );
                    }
                    break;
                    case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER: {

                        sta_assert_error ( point.get_num_connections() ==3 );
                        int bslot=Points<T,Dim>::bifurcation_center_slot;
                        sta_assert_error ( point.endpoint_connections[bslot]==3 );
                        for ( int i=0; i<3; i++ ) {
                            sta_assert_error ( point.endpoints[bslot][i]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
                        }
                        sta_assert_error ( ( !point.has_owner(voxgrid_default_id) ) || ( (options.bifurcation_particle_hard || options.bifurcation_particle_soft)&& ( point.has_owner(voxgrid_default_id) ) ) );

                        sta_assert_error (point.has_owner(voxgrid_bif_center_id) );

			
			Vector<T,Dim> position0=point.get_position();

                        std::size_t shape[3];
                        shape[0]=shape[1]=shape[2]=1000000;
                        bool out_of_bounce=false;
                        point.bifurcation_center_update ( shape,out_of_bounce );

                        Vector<T,Dim> position1=point.get_position();
                        if ( ( position0-position1 ).norm2() >std::numeric_limits<T>::epsilon() ) {
                            position0.print();
                            position1.print();
                            //throw mhs::STAError("loaded bifurcation point's position differs from correct one!");
			    if (!warned)
			    {
                            printf ( "WARNING: loaded bifurcation point positions differ from correct ones!\n" );
                            printf ( "WARNING: The positions have been updated, but ensure that this is not due to corrupted data!\n" );
			    warned=true;
			    }
                        }
                    }

                    break;
                    }

                }
            }
//#endif


// printf("--C--\n");
            printf("done\n");
              mhs::mex_dumpStringNOW();

        } catch ( mhs::STAError & error ) {
            throw error;
        } 
       
     }




    void init_tracker ( const mxArray * feature_struct,
                        const mxArray * tracker_state=NULL,
                        const mxArray * params=NULL,
                        const mxArray * seeds=NULL
                      ) {
        sta_assert_error ( init_state==0 );
	//Points<T,Dim>::dynamic_thicknes_fact=0;
	Points<T,Dim>::static_init();
	CEdgecost<T,Dim>::static_init();
// printf("--A00--\n");	
	
	if (feature_struct==NULL)
	{
	 ready=1; 
	}else
	{
	  sta_assert_error ( feature_struct!=NULL );
	  sta_assert_error ( mxIsStruct ( feature_struct ) );
	}

	printf("basic\n");
	mhs::mex_dumpStringNOW();
        try {
            init_tracker_basic ( feature_struct,tracker_state,params );
        } catch ( mhs::STAError & error ) {
            throw error;
        }


        /*#####################################
        *
        * 	init all data
        *
        * ####################################*/

	printf("checking for existing state\n");
	mhs::mex_dumpStringNOW();
	try {
            
            init_tracker_add_graph ( feature_struct,tracker_state,params );
	 } catch ( mhs::STAError & error ) {
            throw error;
        }
        

    if (seeds!=NULL)
    {
        printf("checking for seed state\n");
        mhs::mex_dumpStringNOW();
        try {
                
                init_tracker_seeds( feature_struct,seeds,params );
        } catch ( mhs::STAError & error ) {
                throw error;
            }
    }
    

        printf("graph structure\n");
	mhs::mex_dumpStringNOW();
	try {
            
            init_graph_structure();
	 } catch ( mhs::STAError & error ) {
            throw error;
        }
        
        printf("proposals\n");
	mhs::mex_dumpStringNOW();

// printf("--D--\n");
        if (ready==0)
	{
	  try {
	      init_tracker_proposals ( feature_struct,tracker_state,params );
	  } catch ( mhs::STAError & error ) {
	      throw error;
	  }
	  ready=2;
	}

        init_state++;
    }



    mxArray * state2mx() {
        mxArray * state_array;
        {
            std::size_t num_attributes =1;
            mwSize ndims=12;
            state_array = mxCreateNumericArray ( 1,&ndims,mhs::mex_getClassId<T>(),mxREAL );
            sta_assert_error ( state_array!=NULL )
            T * state= ( T * ) mxGetData ( state_array );

//  	    printf("\n\n real_iterations:  %u %u \n\n",real_iterations,state_iteration_total_num);
	    
            state[0]=state_eval_state;
            state[1]=state_iteration_time;
            state[2]=state_iteration_time+state_iteration_time_total;
            state[3]=state_iteration_total_num+real_iterations;//iterations;//options.opt_numiterations;
            state[4]=state_energy_grad_update_counter2[0];
            state[5]=state_energy_grad2[0];
            state[6]=state_energy_grad_old2[0];
            state[7]=state_movie_frame;
            state[8]=state_particle_number_activity;
            state[9]=state_particle_number_activity_max;
            state[10]=state_particle_number_activity_count[0];
            state[11]=state_particle_number_activity_count[1];
	    
// 	    for (int i=0;i<12;i++)
// 	    {
// 	      printf("%f \n", state[i]);
// 	    }
        }
        return state_array;
    }
    void mx2state ( const  mxArray * state_array ) {
        mhs::dataArray<T> data=mhs::dataArray<T> ( state_array,"state" );
        sta_assert_error ( data.get_num_elements() ==12 );
        {

            T * state= data.data;

            state_eval_state=state[0];
            state_iteration_time=state[1];
            //state[2]=state_iteration_time+state_iteration_time_total;
// 	state[3]=state_iteration_total_num+options.opt_numiterations;
	    state_iteration_total_num=state[3];
            state_energy_grad_update_counter2[0]=state[4];
            state_energy_grad2[0]=state[5];
            state_energy_grad_old2[0]=state[6];
            state_movie_frame=state[7];
            state_particle_number_activity=state[8];
            state_particle_number_activity_max=state[9];
            state_particle_number_activity_count[0]=state[10];
            state_particle_number_activity_count[1]=state[11];
        }

    }
    
    
    



    mxArray * save_tracker_state ( bool checks=true ) {
        mwSize dim = 1;
        mxArray * tdata=mxCreateStructArray ( 1,&dim,nfield_names,field_names );

        mxSetField ( tdata,0,"opt_temp",  mxCreateDoubleScalar ( opt_temp ) );
        mxSetField ( tdata,0,"opt_temp_conn_cost",  mxCreateDoubleScalar ( opt_temp_conn_cost ) );
        mxSetField ( tdata,0,"particle_thickness",  mxCreateDoubleScalar ( Points<T,Dim>::thickness ) );
        mxSetField ( tdata,0,"scale_correction",  mxCreateDoubleScalar ( data_fun->get_scalecorrection() ) );
        mxSetField ( tdata,0,"offline",  mxCreateDoubleScalar ( this->is_offline() ) );



	
// 	T * p_shape=mhs::mex_getFieldPtrCreate<double> ( tdata,3,"shape",0 );
//         p_shape[0]=this->shape[0];
// 	p_shape[1]=this->shape[1];
// 	p_shape[2]=this->shape[2];
	

        //mxSetField(tdata,0,"state_iteration_total_num",  mex_create_value<u_int64_t>(state_iteration_total_num+options.opt_numiterations));


// 	printf("-------------------\n");


        mxSetField ( tdata,0,"state",  state2mx() );
	
	

	
	
// 	check_consisty();
//
//
// 	printf("-------------------\n");

// 	mxSetField(tdata,0,"state_eval_state",  mxCreateDoubleScalar(state_eval_state));
//         mxSetField(tdata,0,"state_iteration_time",  mxCreateDoubleScalar(state_iteration_time));
//         mxSetField(tdata,0,"state_iteration_time_total",  mxCreateDoubleScalar(state_iteration_time+state_iteration_time_total));
//         mxSetField(tdata,0,"state_iteration_total_num",  mxCreateDoubleScalar(state_iteration_total_num+options.opt_numiterations));
// 	mxSetField(tdata,0,"state_energy_grad_update_counter",  mxCreateDoubleScalar(state_energy_grad_update_counter2[0]));
//         mxSetField(tdata,0,"state_energy_grad",  mxCreateDoubleScalar(state_energy_grad2[0]));
//         mxSetField(tdata,0,"state_energy_grad_old",  mxCreateDoubleScalar(state_energy_grad_old2[0]));
// 	mxSetField(tdata,0,"state_movie_frame",  mxCreateDoubleScalar(this->state_movie_frame));



// 	"state_eval_state",		// current evaluation counter state
//     "state_iteration_time",	// time for the current run
//     "state_iteration_time_total",// total computation time
//     "state_iteration_total_num",	// total number of iterations
//     "state_energy_grad_update_counter",  // number of grad updates ignored before updating temp
//     "state_energy_grad",		// energy gradient statistic state current
//     "state_energy_grad_old",		// energy gradient statistic state previous
//     "scale_correction",		// scale correction
//     "state_movie_frame",
//     "state_particle_number_activity",
//     "state_particle_number_activity_max",
//     "state_particle_number_activity_count0"
//     "state_particle_number_activity_count1"

// 	mxSetField(tdata,0,"state_particle_number_activity",  mxCreateDoubleScalar(this->state_particle_number_activity));
// 	mxSetField(tdata,0,"state_particle_number_activity_max",  mxCreateDoubleScalar(this->state_particle_number_activity_max));
// 	mxSetField(tdata,0,"state_particle_number_activity_count0",  mxCreateDoubleScalar(this->state_particle_number_activity_count[0]));
// 	mxSetField(tdata,0,"state_particle_number_activity_count1",  mxCreateDoubleScalar(this->state_particle_number_activity_count[1]));


// 	remove_blacklisted_edges();

        if ( !checks ) {
            printf ( "A" );
        }

        {
            class OctPoints<T,Dim> ** index;
            unsigned int  npoints;
            tree->get_pointlist ( index,npoints );
            if ( checks ) {
                printf ( "npoints collision tree: %u\n",npoints );
            }

            connecion_candidate_tree->get_pointlist ( index,npoints );

            if ( checks ) {
                printf ( "npoints connection cand tree: %u\n",npoints );
            }


            //WARNING  just for debugging
#ifdef _DEBUG_CHECKS_0_
// 	  for (unsigned int i=0; i<npoints; i++)
// 	  {
// 	      Points<T,Dim> & point=*((Points<T,Dim>*)index[i]);
// 	  }
#endif
        }

        //pool<class Points<T,Dim> > * particle_pool;
        std::size_t n_points=particle_pool->get_numpts();
        const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();
// 	const class Points<T,Dim> * part_ptr=*particle_ptr;
        if ( checks ) {
            printf ( "total points in particle pool: %u\n",n_points );
        }




        unsigned int  npointproperties=POINT_ATTRIBUTES::POINT_A_Count;
        mwSize ndims[2];
        ndims[1]=n_points;
        ndims[0]=npointproperties;
        mxArray * pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
        mxSetField ( tdata,0,"data", pdata );
        T *result = ( T * ) mxGetData ( pdata );

        T * presult=result;

        if ( checks ) {
            printf ( "creating result array\n" );
            mhs::mex_dumpStringNOW();
        }

        int endpoint_statistic[Points< T, Dim >::maxconnections+1];
        for ( int i=0; i<=Points< T, Dim >::maxconnections; i++ ) {
            endpoint_statistic[i]=0;
        }


        T min_max_saliency[2];
        min_max_saliency[0]=std::numeric_limits<T>::max();
        min_max_saliency[1]=-std::numeric_limits<T>::max();




//         for ( unsigned int i=0; i<n_points; i++ ) {
//             Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
//             point.set_id ( -1 );
//         }
        if ( checks ) {
            printf ( "clearing path labels\n" );
            mhs::mex_dumpStringNOW();
        }
        
        clear_path_labels();

        if ( checks ) {
            printf ( "labeling paths\n" );
            mhs::mex_dumpStringNOW();
        }

        int path_id=1;
	if (options.opt_spawn_blobs)
	{
	  path_id=2;
	}
	T dummy=-1;
        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
            do_label ( point,path_id,dummy );
        }

        if ( !checks ) {
            printf ( "B" );
        }
/*
        check_particle_consistensy();*/
        
        if ( checks ) {
            printf ( "saving points\n" );
            mhs::mex_dumpStringNOW();
        }

        for ( unsigned int i=0; i<n_points; i++ ) {
            //Points<T,Dim> & point=*((Points<T,Dim>*)particle_ptr[i]);
            const Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );



#ifdef _DEBUG_CHECKS_0_
	    if (!options.opt_spawn_blobs)
	    {
            if ( checks ) {
                if ( ( ( point.endpoint_connections[0]==0 ) || ( point.endpoint_connections[1]==0 ) ) &&
                        ( point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
                    sta_assert_error ( point.has_owner ( voxgrid_conn_can_id ) );
                }

                switch ( point.particle_type ) {
                case PARTICLE_TYPES::PARTICLE_UNDEFINED: {
                    throw mhs::STAError ( "found PARTICLE_TYPES::PARTICLE_UNDEFINED" );
                }
                break;
                case PARTICLE_TYPES::PARTICLE_SEGMENT: {
                    sta_assert_error ( point.has_owner() );
                    for ( int i=0; i<2; i++ ) {
                        if ( point.endpoint_connections[i]>0 ) {
                            sta_assert_error ( point.endpoint_connections[i]==1 );
                            sta_assert_error ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_SEGMENT );
                        }
                    }
                }
                break;
                case PARTICLE_TYPES::PARTICLE_BIFURCATION: {
                    sta_assert_error ( point.has_owner() );
                    sta_assert_error ( point.get_num_connections() >0 );
                    bool foundbif=false;
                    for ( int i=0; i<2; i++ ) {
                        if ( point.endpoint_connections[i]>0 ) {
                            sta_assert_error ( point.endpoint_connections[i]==1 );
                            if ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
                                foundbif=true;
                            }
                        }
                    }
                    sta_assert_error ( foundbif );
                }
                break;
                case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER: {
		    sta_assert_error ( point.has_owner(voxgrid_bif_center_id) ) ;
                    sta_assert_error (  ( (options.bifurcation_particle_hard || options.bifurcation_particle_soft)!= ( !point.has_owner(voxgrid_default_id) ) ) );
                    sta_assert_error ( point.get_num_connections() ==3 );
                    int bslot=Points<T,Dim>::bifurcation_center_slot;
                    sta_assert_error ( point.endpoint_connections[bslot]==3 );
                    for ( int i=0; i<3; i++ ) {
                        sta_assert_error ( point.endpoints[bslot][i]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
                    }
                }
                break;
                }
            }
	    }
#endif
            /*if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
            {
            typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
            T new_edgecost=point.edge_energy(
              *edgecost_fun,
              options.connection_bonus_L,
              options.bifurcation_bonus_L,
              maxendpointdist,
              inrange,
              true
            );
            printf("new_edgecost  (without bonus) %f, range %d\n",new_edgecost-options.bifurcation_bonus_L,static_cast<int>(inrange));
            }*/


            endpoint_statistic[point.endpoint_connections[0]]++;
            endpoint_statistic[point.endpoint_connections[1]]++;

//             point._point_id=i;
            point.set_id ( i );

            presult[POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=-1; //connected or not

            
	    presult[POINT_ATTRIBUTES::POINT_A_PROTECTED_TOPOLOGY]=point.is_protected_topology();

            presult[POINT_ATTRIBUTES::POINT_A_PATH_ID]=point._path_id;
            presult[POINT_ATTRIBUTES::POINT_A_NUMCONNECTION_B]=point.endpoint_connections[0];
            presult[POINT_ATTRIBUTES::POINT_A_NUMCONNECTION_A]=point.endpoint_connections[1];
// 	    presult[17]=-1000000;
// 	    presult[16]=-1000000;
            presult[POINT_ATTRIBUTES::POINT_A_SALIENCY]=point.saliency;
            min_max_saliency[0]=std::min ( point.saliency,min_max_saliency[0] );
            min_max_saliency[1]=std::max ( point.saliency,min_max_saliency[1] );

            presult[POINT_ATTRIBUTES::POINT_A_COST]=point.point_cost;

            if ( checks ) {
                bool check_pt_in_img=true;
                check_pt_in_img=check_pt_in_img&& ( point.get_position() [0]>=0 );
                check_pt_in_img=check_pt_in_img&& ( point.get_position() [0]<shape[0] );
                check_pt_in_img=check_pt_in_img&& ( point.get_position() [1]>=0 );
                check_pt_in_img=check_pt_in_img&& ( point.get_position() [1]<shape[1] );
                check_pt_in_img=check_pt_in_img&& ( point.get_position() [2]>=0 );
                check_pt_in_img=check_pt_in_img&& ( point.get_position() [2]<shape[2] );

                if ( !check_pt_in_img ) {
                    printf ( "###################################################" ) ;
                    printf ( "WARINING : POINT OUT OF RANGE: %d %d %d\n",shape[0],shape[1],shape[2] ) ;
                    point.get_position().print();
                }
            }

            Points<T,Dim>  point_position_corrected=point;
            point_position_corrected.set_position ( point_position_corrected.get_position() +get_world_offset() );
//             point_position_corrected.update_endpoint(0);
//             point_position_corrected.update_endpoint(1);

            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_B_POSX]=point_position_corrected.endpoints_pos[Points<T,Dim>::b_side][0];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_B_POSY]=point_position_corrected.endpoints_pos[Points<T,Dim>::b_side][1];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_B_POSZ]=point_position_corrected.endpoints_pos[Points<T,Dim>::b_side][2];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_A_POSX]=point_position_corrected.endpoints_pos[Points<T,Dim>::a_side][0];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_A_POSY]=point_position_corrected.endpoints_pos[Points<T,Dim>::a_side][1];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_A_POSZ]=point_position_corrected.endpoints_pos[Points<T,Dim>::a_side][2];



            presult[POINT_ATTRIBUTES::POINT_A_TYPE]=static_cast<unsigned int> ( point_position_corrected.particle_type );

            presult[POINT_ATTRIBUTES::POINT_A_SCALE]=point_position_corrected.get_scale();

            presult[POINT_ATTRIBUTES::POINT_A_BIRTH_FLAG]=point_position_corrected.tracker_birth;

            presult[POINT_ATTRIBUTES::POINT_A_DIRX]=point_position_corrected.get_direction() [0];
            presult[POINT_ATTRIBUTES::POINT_A_DIRY]=point_position_corrected.get_direction() [1];
            presult[POINT_ATTRIBUTES::POINT_A_DIRZ]=point_position_corrected.get_direction() [2];



            presult[POINT_ATTRIBUTES::POINT_A_POSX]=point_position_corrected.get_position() [0];
            presult[POINT_ATTRIBUTES::POINT_A_POSY]=point_position_corrected.get_position() [1];
            presult[POINT_ATTRIBUTES::POINT_A_POSZ]=point_position_corrected.get_position() [2];


            presult[POINT_ATTRIBUTES::POINT_A_FREEZED]=point_position_corrected.freezed;
            presult[POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]=point_position_corrected.freezed_endpoints[0];
            presult[POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]=point_position_corrected.freezed_endpoints[1];

            presult+=ndims[0];
        }

        if ( checks ) {
            if ( n_points>0 ) {
                printf ( "saliency: min %f, max %f\n",min_max_saliency[0],min_max_saliency[1] );
            }

            printf ( "------------------------------------\n" );
            printf ( "connection stats:\n" );
            for ( int i=0; i<=Points< T, Dim >::maxconnections; i++ ) {
                printf ( "%d connections: %d \n",i,endpoint_statistic[i] );
            }
            printf ( "\n" );
            if ( Points< T, Dim >::maxconnections>0 ) {
                printf ( "num line segments: %d\n",connections.pool->get_numpts()-endpoint_statistic[2] );
            }
            if ( Points< T, Dim >::maxconnections>1 ) {
                printf ( "num bifurcations: %d\n",endpoint_statistic[2]/3 );
            }


            //printf("number of total connections: %d\n",connections.pool->get_numpts());
            connections.print();
//             connections.check_consistency();
        }

        if ( !checks ) {
            printf ( "C" );
        }

        if ( checks ) {
            printf ( "allocating connection array\n" );
            mhs::mex_dumpStringNOW();
        }
        
        std::size_t numconnections=connections.pool->get_numpts();
        ndims[1]=numconnections;
        ndims[0]=EDGE_ATTRIBUTES::EDGE_A_Count;
        pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
        mxSetField ( tdata,0,"connections", pdata );
        T* result2 = ( T * ) mxGetData ( pdata );
        presult=result2;

        if ( !checks ) {
            printf ( "D" );
        }

//         connections.check_consistency2(edgecost_fun,11);
        if ( checks ) {
            printf ( "storing edges\n" );
            mhs::mex_dumpStringNOW();
        }
        
        const Connection<T,Dim> ** conptr=connections.pool->getMem();
        for ( std::size_t i=0; i<numconnections; i++ ) {
// printf("%d %d ",i,numconnections);
            const Connection<T,Dim> * con=*conptr;
 	    
// 	    sta_assert_error(con->debug_value==11);
/*printf("#");*/	    
            if ( ( con->pointA->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT ) &&
                    ( con->pointB->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT ) ) {
                sta_assert_error ( con->edge_type!=EDGE_TYPES::EDGE_BIFURCATION );
            }
/*printf("#");   */         
	    if (( con->edge_type!=EDGE_TYPES::EDGE_BIFURCATION ))
	    {
	      if (! (edgecost_fun->particle_angle_valid(*(con->pointA),*(con->pointB),true)) )
	      {
// 		bool test0=(!(edgecost_fun->particle_angle_valid(*(con->pointA),*(con->pointB),true)));
// 		bool testA=edgecost_fun->particle_angle_valid(*(con->pointA),*(con->pointB),true);
// 		bool testB=edgecost_fun->particle_angle_valid(*(con->pointB),*(con->pointA),true);
// 		printf("Test: %d %d %d\n",testA,testB,test0);
// 		sta_assert_error(!(edgecost_fun->particle_angle_valid(*(con->pointB),*(con->pointA))));
		printf("Did you change the parameter AngleScale ?\n");
	      }
	    }
// printf("#");
            presult[EDGE_ATTRIBUTES::EDGE_A_PINDXA]=con->pointA->point->_point_id;
            presult[EDGE_ATTRIBUTES::EDGE_A_PINDXB]=con->pointB->point->_point_id;
/*printf("%d %d %d",con->pointA->point->_point_id,con->pointB->point->_point_id,n_points);*/	    
            sta_assert_error ( con->pointA->connected!=NULL );
            sta_assert_error ( con->pointB->connected!=NULL );
// printf("!");
            presult[EDGE_ATTRIBUTES::EDGE_A_SIDEA]=con->pointA->side;
            presult[EDGE_ATTRIBUTES::EDGE_A_SIDEB]=con->pointB->side;
// printf("!");
            //presult[EDGE_ATTRIBUTES::EDGE_A_COST]=con->cost;
            presult[EDGE_ATTRIBUTES::EDGE_A_TYPE]=static_cast<int> ( con->edge_type );
	    
	    presult[EDGE_ATTRIBUTES::EDGE_A_PROTECTED_TOPOLOGY]=con->is_protected_topology();
// printf("!");
            result[int ( presult[EDGE_ATTRIBUTES::EDGE_A_PINDXA]*npointproperties ) +POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=1;
            result[int ( presult[EDGE_ATTRIBUTES::EDGE_A_PINDXB]*npointproperties ) +POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=1;
// printf("#");

// if (con->edge_type==EDGE_TYPES::EDGE_BIFURCATION)
// {
// typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
// T new_edgecost=con->compute_cost(
//   *edgecost_fun,
//   options.connection_bonus_L,
//   options.bifurcation_bonus_L,
//   maxendpointdist,
//   inrange,
//   false
// );
// printf("new_edgecost  (without bonus) %f, range %d\n",new_edgecost-options.bifurcation_bonus_L,static_cast<int>(inrange));
// }

            presult+=ndims[0];
            conptr++;
/*printf("\n");	   */ 
        }

        if ( !checks ) {
            printf ( "E" );
        }
        if ( checks ) {
            printf ( "max point search iter %d\n",maxpiter );
            tree->print();
            printf ( "------------------------------------\n" );

        }
        return tdata;
    }


    void check_consisty(bool warn_only=false, bool pre_edges_check=true) {
      {
      class OctPoints<T,Dim> ** index;
        unsigned int  npoints;
        tree->get_pointlist ( index,npoints );
	try {
        for ( unsigned int i=0; i<npoints; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim> * ) ( index[i] ) );
           
		if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		{
		  
		  typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		point.e_cost(*edgecost_fun,
                               T(0),
			       T(0),
                               inrange,true,false,false);
		if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		{
		  
		  
		  
		 mhs::STAError e;
		 e<<"connection cost error:"<<static_cast<unsigned int>(inrange)<<"\n"
		 <<"point type is :"<<static_cast<unsigned int>(point.particle_type);
		 
		 point.e_cost(*edgecost_fun,
                               T(0),
			       T(0),
//                                maxendpointdist,
                               inrange,true,true,false);
		 
		 if (!warn_only)
		 {
		  throw e;
		 }else
		 {
		    printf ( "%s\n",e.what() );
		 }
		}
		
		
		  /*
		  const class Points< T, Dim >::CEndpoint & endpointA=*(point.endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected);
		  const class Points< T, Dim >::CEndpoint & endpointB=*(point.endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected);
		  const class Points< T, Dim >::CEndpoint & endpointC=*(point.endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected);
		  
		  const Vector<T,Dim> * endpoint_pos[3];
		    endpoint_pos[0]=(endpointA.position);
		    endpoint_pos[1]=(endpointB.position);
		    endpoint_pos[2]=(endpointC.position);
		    
		    const class Points< T, Dim >::CEndpoint * endpoints[3];
		    endpoints[0]=&endpointA;
		    endpoints[1]=&endpointB;
		    endpoints[2]=&endpointC;
		    
		    T endpoint_dist2[3];
		    
		    
		    //sta_assert_error((max_endpoint_dist>0)||(CEdgecost<T,Dim>::max_edge_length<0));
		    
		    T max_edge_length=CEdgecost<T,Dim>::get_current_edge_length_limit();
		    
		 
		      max_edge_length*=max_edge_length;
		      		      
		      for (int i=0;i<3;i++)
		      {
			endpoint_dist2[i]=(*endpoint_pos[i]-*endpoint_pos[(i+1)%3]).norm2();
			T dist2=endpoint_dist2[i];
			if (!(max_edge_length>dist2))
			{
			  
				  sta_assert_error((CEdgecost<T,Dim>::max_edge_length_hard*CEdgecost<T,Dim>::max_edge_length_hard>dist2));
				  sta_assert_error(false);
				  
			} 
		      }
		  
		  const class Points< T, Dim > * points[3];
		  points[0]=endpointA.point;
		  points[1]=endpointB.point;
		  points[2]=endpointC.point;
		  
		    const T & a=CEdgecost<T,Dim>::side_scale*points[0]->get_scale();
		    const T & b=CEdgecost<T,Dim>::side_scale*points[1]->get_scale();
		    const T & c=CEdgecost<T,Dim>::side_scale*points[2]->get_scale();
		    
		    const T * scales[3];
		    scales[0]=&a;
		    scales[1]=&b;
		    scales[2]=&c;
		    
		    
		    

		    
		    //#define _MIN_TRIANGLE_RATIO_ T(1.1)
		    //#define _MIN_TRIANGLE_RATIO_ T(1.01)
		    
		    sta_assert_error(edgecost_fun->valid_triangle(a,b,c,_MIN_TRIANGLE_RATIO_))
		    sta_assert_error(edgecost_fun->valid_triangle(FAST_SQRT(endpoint_dist2[0]),FAST_SQRT(endpoint_dist2[1]),FAST_SQRT(endpoint_dist2[2]),_MIN_TRIANGLE_RATIO_))*/
		}
	    }
	    
	     std::size_t numconnections=connections.pool->get_numpts();
	   const Connection<T,Dim> ** conptr=connections.pool->getMem();
	  for ( std::size_t i=0; i<numconnections; i++ ) {
            const Connection<T,Dim> * con=*conptr;
	      sta_assert_error(con->pointA!=NULL);
		sta_assert_error(con->pointB!=NULL);
            conptr++;
	    
	  }
// 	  printf("%d edgeptr ok\n",numconnections);

	    
	}
	catch ( mhs::STAError & error ) {
	  printf ( "%s\n",error.what() );
	  if (!warn_only)
	    throw error;
      
      }
	
    } 
    return;
	
	
      
      if (options.opt_spawn_blobs)
	return;
      
      
      try {
	if (pre_edges_check)
	  {
	    connections.check_consistency(maxendpointdist,edgecost_fun);
	  }
      }
    catch ( mhs::STAError & error ) {

	  printf ( "%s\n",error.what() );
	  if (!warn_only)
	    throw error;
      
      }
        class OctPoints<T,Dim> ** index;
        unsigned int  npoints;
        tree->get_pointlist ( index,npoints );

        for ( unsigned int i=0; i<npoints; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim> * ) ( index[i] ) );
            try {
                point.check_consistency();
		
		if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		{
		
		    if (Constraints::constraint_hasloop(point,5))
		    {
		      mhs::STAError e;
		      e<<"HAS LOOP!\n";
		      if (!warn_only)
			throw e;
		    }
		}
		
		
		
		typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
		point.e_cost(*edgecost_fun,
                               T(0),
			       T(0),
                               inrange,true,false,false);
		if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		{
		  
		  
		  
		 mhs::STAError e;
		 e<<"connection cost error:"<<static_cast<unsigned int>(inrange)<<"\n"
		 <<"point type is :"<<static_cast<unsigned int>(point.particle_type);
		 
		 point.e_cost(*edgecost_fun,
                               T(0),
			       T(0),
//                                maxendpointdist,
                               inrange,true,true,false);
		 
		 if (!warn_only)
		 {
		  throw e;
		 }else
		 {
		    printf ( "%s\n",e.what() );
		 }
		}
            } catch ( mhs::STAError & error ) {

                printf ( "%s\n",error.what() );
		if (!warn_only)
                throw error;
            }

        }
        
        for ( unsigned int i=0; i<npoints; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim> * ) ( index[i] ) );
            try {
                
		
		if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		{
		    bool out_of_bounce=false;
		    point.bifurcation_center_update(shape,out_of_bounce);
		  
		    sta_assert_error(!out_of_bounce);
		
		    if (Constraints::constraint_hasloop(point,5))
		    {
		      mhs::STAError e;
		      e<<"HAS LOOP AFTER CORRECTION!!\n";
		      if (!warn_only)
		      {
			throw e;
		      }else
		      {
			printf ( "%s\n",e.what() );
		      }
		    }
		}
		
            } catch ( mhs::STAError & error ) {

                printf ( "%s\n",error.what() );
                if (!warn_only)
                throw error;
            }

        }



    }
    



    void check_constraints() {
        class OctPoints<T,Dim> ** index;
        unsigned int  npoints;
        tree->get_pointlist ( index,npoints );

        for ( unsigned int i=0; i<npoints; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim> * ) ( index[i] ) );
            sta_assert_error ( !Constraints::constraint_hasloop ( point,options.constraint_loop_depth ) );

        }
    }



    mxArray * save_frame_state() {
        mwSize dim = 1;
        mxArray * tdata=mxCreateStructArray ( 1,&dim,nfield_names,field_names );

        mxSetField ( tdata,0,"opt_temp",  mxCreateDoubleScalar ( opt_temp ) );
        mxSetField ( tdata,0,"opt_temp_conn_cost",  mxCreateDoubleScalar ( opt_temp_conn_cost ) );
        mxSetField ( tdata,0,"particle_thickness",  mxCreateDoubleScalar ( Points<T,Dim>::thickness ) );
        mxSetField ( tdata,0,"scale_correction",  mxCreateDoubleScalar ( data_fun->get_scalecorrection() ) );
        mxSetField ( tdata,0,"offline",  mxCreateDoubleScalar ( this->is_offline() ) );

        mxSetField ( tdata,0,"state",  state2mx() );
        /*
                mxSetField(tdata,0,"opt_temp",  mxCreateDoubleScalar(opt_temp));
                mxSetField(tdata,0,"opt_temp_conn_cost",  mxCreateDoubleScalar(opt_temp_conn_cost));
                mxSetField(tdata,0,"state_eval_state",  mxCreateDoubleScalar(state_eval_state));
                mxSetField(tdata,0,"state_iteration_time",  mxCreateDoubleScalar(state_iteration_time));
                mxSetField(tdata,0,"state_iteration_time_total",  mxCreateDoubleScalar(state_iteration_time+state_iteration_time_total));

        	//mxSetField(tdata,0,"state_iteration_total_num",  mex_create_value<u_int64_t>(state_iteration_total_num+options.opt_numiterations));

                mxSetField(tdata,0,"state_iteration_total_num",  mxCreateDoubleScalar(state_iteration_total_num+options.opt_numiterations));

        	mxSetField(tdata,0,"state_energy_grad_update_counter",  mxCreateDoubleScalar(state_energy_grad_update_counter2[0]));
                mxSetField(tdata,0,"particle_thickness",  mxCreateDoubleScalar(Points<T,Dim>::thickness));
                mxSetField(tdata,0,"state_energy_grad",  mxCreateDoubleScalar(state_energy_grad2[0]));
                mxSetField(tdata,0,"state_energy_grad_old",  mxCreateDoubleScalar(state_energy_grad_old2[0]));
                mxSetField(tdata,0,"scale_correction",  mxCreateDoubleScalar(data_fun->get_scalecorrection()));
        	mxSetField(tdata,0,"offline",  mxCreateDoubleScalar(this->is_offline()));
        	mxSetField(tdata,0,"state_movie_frame",  mxCreateDoubleScalar(this->state_movie_frame));*/



        std::size_t n_points=particle_pool->get_numpts();
        const class Points<T,Dim> ** particle_ptr=particle_pool->getMem();


        unsigned int  npointproperties=POINT_ATTRIBUTES::POINT_A_Count;
        std::size_t ndims[2];
        ndims[1]=n_points;
        ndims[0]=npointproperties;
        mxArray * pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
        mxSetField ( tdata,0,"data", pdata );
        T *result = ( T * ) mxGetData ( pdata );

        T * presult=result;


        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
            point.set_id ( -1 );
        }

        int path_id=1;
        for ( unsigned int i=0; i<n_points; i++ ) {
            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
            do_label ( point,path_id );
        }

        for ( unsigned int i=0; i<n_points; i++ ) {
            const Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
            point.set_id ( i );

            presult[POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=-1; //connected or not
            presult[POINT_ATTRIBUTES::POINT_A_PATH_ID]=point._path_id;
            presult[POINT_ATTRIBUTES::POINT_A_NUMCONNECTION_B]=point.endpoint_connections[0];
            presult[POINT_ATTRIBUTES::POINT_A_NUMCONNECTION_A]=point.endpoint_connections[1];
// 	    presult[17]=-1000000;
	    
	    presult[POINT_ATTRIBUTES::POINT_A_PROTECTED_TOPOLOGY]=point.is_protected_topology();
	    
// 	    presult[16]=-1000000;
            presult[POINT_ATTRIBUTES::POINT_A_SALIENCY]=point.saliency;
            presult[POINT_ATTRIBUTES::POINT_A_COST]=point.point_cost;

            Points<T,Dim>  point_position_corrected=point;
            point_position_corrected.position+=get_world_offset();
//             point_position_corrected.update_endpoint(0);
//             point_position_corrected.update_endpoint(1);

            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_B_POSX]=point_position_corrected.endpoints_pos[Points<T,Dim>::b_side][0];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_B_POSY]=point_position_corrected.endpoints_pos[Points<T,Dim>::b_side][1];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_B_POSZ]=point_position_corrected.endpoints_pos[Points<T,Dim>::b_side][2];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_A_POSX]=point_position_corrected.endpoints_pos[Points<T,Dim>::a_side][0];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_A_POSY]=point_position_corrected.endpoints_pos[Points<T,Dim>::a_side][1];
            presult[POINT_ATTRIBUTES::POINT_A_ENDPOINT_A_POSZ]=point_position_corrected.endpoints_pos[Points<T,Dim>::a_side][2];



            presult[POINT_ATTRIBUTES::POINT_A_TYPE]=static_cast<unsigned int> ( point_position_corrected.particle_type );

            presult[POINT_ATTRIBUTES::POINT_A_SCALE]=point_position_corrected.scale;

            presult[POINT_ATTRIBUTES::POINT_A_BIRTH_FLAG]=point_position_corrected.tracker_birth;

            presult[POINT_ATTRIBUTES::POINT_A_DIRX]=point_position_corrected.direction[0];
            presult[POINT_ATTRIBUTES::POINT_A_DIRY]=point_position_corrected.direction[1];
            presult[POINT_ATTRIBUTES::POINT_A_DIRZ]=point_position_corrected.direction[2];



            presult[POINT_ATTRIBUTES::POINT_A_POSX]=point_position_corrected.position[0];
            presult[POINT_ATTRIBUTES::POINT_A_POSY]=point_position_corrected.position[1];
            presult[POINT_ATTRIBUTES::POINT_A_POSZ]=point_position_corrected.position[2];


            presult[POINT_ATTRIBUTES::POINT_A_FREEZED]=point_position_corrected.freezed;
            presult[POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_A]=point_position_corrected.freezed_endpoints[0];
            presult[POINT_ATTRIBUTES::POINT_A_FREEZED_ENDPOINTS_B]=point_position_corrected.freezed_endpoints[1];

            presult+=ndims[0];
        }

        std::size_t numconnections=connections.pool->get_numpts();
        ndims[1]=numconnections;
        ndims[0]=EDGE_ATTRIBUTES::EDGE_A_Count;
        pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
        mxSetField ( tdata,0,"connections", pdata );
        T* result2 = ( T * ) mxGetData ( pdata );
        presult=result2;


        const Connection<T,Dim> ** conptr=connections.pool->getMem();
        for ( std::size_t i=0; i<numconnections; i++ ) {
            const Connection<T,Dim> * con=*conptr;

            if ( ( con->pointA->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT ) &&
                    ( con->pointB->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT ) ) {
                sta_assert_error ( con->edge_type!=EDGE_TYPES::EDGE_BIFURCATION );
            }

            presult[EDGE_ATTRIBUTES::EDGE_A_PINDXA]=con->pointA->point->_point_id;
            presult[EDGE_ATTRIBUTES::EDGE_A_PINDXB]=con->pointB->point->_point_id;
            sta_assert_error ( con->pointA->connected!=NULL );
            sta_assert_error ( con->pointB->connected!=NULL );

            presult[EDGE_ATTRIBUTES::EDGE_A_SIDEA]=con->pointA->side;
            presult[EDGE_ATTRIBUTES::EDGE_A_SIDEB]=con->pointB->side;
	    
	    presult[EDGE_ATTRIBUTES::EDGE_A_PROTECTED_TOPOLOGY]=con->is_protected_topology();

            //presult[EDGE_ATTRIBUTES::EDGE_A_COST]=con->cost;
            presult[EDGE_ATTRIBUTES::EDGE_A_TYPE]=static_cast<int> ( con->edge_type );

            result[int ( presult[EDGE_ATTRIBUTES::EDGE_A_PINDXA]*npointproperties ) +POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=1;
            result[int ( presult[EDGE_ATTRIBUTES::EDGE_A_PINDXB]*npointproperties ) +POINT_ATTRIBUTES::POINT_A_CONNECTED_FLAG]=1;

            presult+=ndims[0];
            conptr++;
        }


        return tdata;
    }

};

template<typename TData,typename T,int Dim>
std::size_t CTracker<TData,T,Dim>::movie_steprate=50;


#endif
