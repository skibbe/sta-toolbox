#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"

#include <stdio.h>
#include <stdlib.h>

#include "mhs_gui.h"
#include "mhs_matrix.h"
//#include "mex.h"



int TestThread(void *ptr)
{
  
   
  
    // Window mode MUST include SDL_WINDOW_OPENGL for use with OpenGL.
//   SDL_Window *window = SDL_CreateWindow(
//       "SDL2/OpenGL Demo", 0, 0, 640, 480, 
//       SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
    
/*
  // Create an OpenGL context associated with the window.
  SDL_GLContext glcontext = SDL_GL_CreateContext(window);

  SDL_Log("loop start");
  
   for (int cnt = 0; cnt < 10; ++cnt) {
  
      float c=cnt;
	  // now you can make GL calls.
	glClearColor(0,c/10.0f,0,1);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(window);
	SDL_PumpEvents();
	int mouise_pos[2];
	if (SDL_GetMouseState(mouise_pos,mouise_pos+1) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
	    SDL_Log("Mouse Button 1 (left) is pressed.");
	    printf("%d %d\n",mouise_pos[0],mouise_pos[1]);
	    mhs::mex_dumpStringNOW();
	}
        SDL_Delay(1);
    }
    
    SDL_Log("loop exited.");
  

  // Once finished with OpenGL functions, the SDL_GLContext can be deleted.
  SDL_GL_DeleteContext(glcontext);  
*/
  //SDL_Delay(50);
//   SDL_HideWindow(window);
//   SDL_DestroyWindow(window);
// 
//   
//   SDL_Log("threads end.");
  
//     int cnt;
// 
//     for (cnt = 0; cnt < 10; ++cnt) {
// 
//         SDL_Delay(50);
//     }

    return 1;
}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
  Matrix<float,4> m;
//    m.Identity();
//    m.print();
//    m=2;
//    m.print(); 
//    for (int i=0;i<9;i++)
//     m[i]=i+1;
   for (int i=0;i<4;i++)
     for (int j=0;j<4;j++)
     {
	m.m[i][j]=i*4+j+1;
     }/*
   m.print();*/
   
   Vector<float,4> v;   
   for (int i=0;i<4;i++)  
     v[i]=i+1;
   
   ((m*(m+1))*v).print();
   
   
   
   m.OpenGL_reorthonormalize();

  
  CSceneRenderer * renderer=new CSceneRenderer();
  
  CVrender<float,double> * vrender=new CVrender<float,double>();
  
  std::size_t shape[3];
  shape[0]=1000;
  shape[1]=100;
  shape[2]=100;
  renderer->set_shape(shape);
  renderer->render();
  
  delete renderer;
  delete vrender;
  
//   /*
//   
//     static TTracker_window * tracker_data_p=NULL; 
//   
//     mxArray * handle=mexGetVariable("global", "tracker_window_ptr");
//     if (handle!=NULL)
//     {
//       printf("found tracker-gl window\n");
//     }else{
//       
// 	mexCallMATLAB(0, NULL,0,NULL, "mhs_gui_create_window");
// 	handle=mexGetVariable("global", "tracker_window_ptr");
//         if (handle==NULL)
// 	{
// 	  printf("shit: still no tracker-gl window\n");
// 	  return;
// 	}
//     }
//     
// 
//     
//     
// /*
//     float boundary[2][3];
//     for (int v=0;v<3;v++)
//     {
//       boundary[0][v]=std::numeric_limits< float >::max();
//       boundary[1][v]=-std::numeric_limits< float >::max();
//     }
//     
//     printf("%f %f %f - %f %f %f\n",boundary[0][0],boundary[0][1],boundary[0][2],boundary[1][0],boundary[1][1],boundary[1][2]);
//       
//     for (int v=0;v<num_v;v++)
//     {
//       boundary[0][0]=std::min(boundary[0][0],vertices[v*3]*3);
//       boundary[0][1]=std::min(boundary[0][1],vertices[v*3+1]*3);
//       boundary[0][2]=std::min(boundary[0][2],vertices[v*3+2]*3);
//       boundary[1][0]=std::max(boundary[1][0],vertices[v*3]*3);
//       boundary[1][1]=std::max(boundary[1][1],vertices[v*3+1]*3);
//       boundary[1][2]=std::max(boundary[1][2],vertices[v*3+2]*3);
//     }
//     float center[3];
//     for (int v=0;v<3;v++)
//       center[v]=boundary[1][v]-boundary[0][v];
//     
//     float diam=std::sqrt(center[0]*center[0]+center[1]*center[1]+center[2]*center[2]);
//     
//     for (int v=0;v<3;v++)
//       center[v]=boundary[0][v]+center[v]/2.0f;
//     */
//     
//     CTriangleObject * particle=new CTriangleObject();
//     float particle_color[3]={1.0f,0.5f,0.5f};
//     
//     
//      GLfloat light_position[] = { 0.0, 0.0, 5.0, 1.0 };
//      GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
//      GLfloat mat_shininess[] = { 50.0 };     
//       
//       
//       std::size_t * handle_p=(std::size_t  *)mxGetPr(handle);
//       tracker_data_p=(TTracker_window*)(*handle_p);
//       
//       
//       if (SDL_GL_MakeCurrent(tracker_data_p->tracker_window,tracker_data_p->glcontext)!=0)
//       {
// 	printf("error setting opengl context to current window\n");
// 	return;
//       }
//       
//     
// 
//       
//       
//       int width;
//       int height;
//     SDL_GetWindowSize(tracker_data_p->tracker_window,&width,&height);
//     printf("%d %d\n",width,height);
//     
//     //SDL_SetVideoMode(width, height, bpp, SDL_OPENGL | SDL_RESIZABLE | SDL_DOUBLEBUF);
//       
//       
//       //glClearColor(0,(std::rand()%100)/100.0f,0,1);
//     glClearColor(0,0,0,1);
//       glShadeModel (GL_SMOOTH);
// 
//       
//       printf("start ..");
//       mhs::mex_dumpStringNOW();
//       
//      //glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
//      //glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
//       glEnable ( GL_COLOR_MATERIAL );
//      
//      glEnable(GL_LIGHTING);
//      glEnable(GL_LIGHT0);
//      glEnable(GL_DEPTH_TEST);   
//      glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//      glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
//   
//      
//        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
// 	
//        glViewport(0, 0, width, height);
//        glMatrixMode( GL_PROJECTION );
//        glLoadIdentity();
//         gluPerspective(65.,(float)width/(float)height,1.0,20.0);
//       float V_FOV=70;
//       float dist=2000;
//        
//        float zNear=1;
// 
//        glMatrixMode( GL_MODELVIEW );
//        glLoadIdentity();  
// 	
//        gluLookAt(0,0,-10.0,0,0,0,0.0,1.0,0.0);
// 	  
//        glTranslatef(0,0,-5);
//        float flatness=0.2;
//        glBegin(GL_TRIANGLES);
// 	particle->renderTriangles(particle_color,flatness);
//        glEnd();
//        
//        SDL_GL_SwapWindow(tracker_data_p->tracker_window);
//       
//       printf(" done\n");
//       mhs::mex_dumpStringNOW();
//       
//       
//       delete particle;*/
      
      
  /*
   if (SDL_Init(SDL_INIT_VIDEO) != 0) {
   //if (SDL_Init(SDL_INIT_AUDIO) != 0) {
        fprintf(stderr,
                "\nUnable to initialize SDL:  %s\n",
                SDL_GetError()
               );
        return;
    }*/
  
  
  
  /*
   TestThread( (void *)NULL);
  SDL_Thread *thread;
    int         threadReturnValue;

    printf("\nSimple SDL_CreateThread test:");

    // Simply create a thread
    thread = SDL_CreateThread(TestThread, "TestThread", (void *)NULL);

    if (NULL == thread) {
        printf("\nSDL_CreateThread failed: %s\n", SDL_GetError());
    } else {
        SDL_WaitThread(thread, &threadReturnValue);
        printf("\nThread returned value: %d", threadReturnValue);
    }
*/
  
//   SDL_Quit();
  
//     atexit(SDL_Quit);
//   SDL_Log("exiting");
  

}