

uniform mat4 glsl_projection;
uniform mat4 glsl_modelview;
uniform mat4 glsl_normalmat;


varying vec4 vColor;
varying vec3 vNormal;
varying vec3 vPosition;

void main (void)  
{     

  vColor=gl_Color;

  vec4 mtraf=glsl_modelview*gl_Vertex;
   
  gl_Position  = glsl_projection*mtraf;
  vPosition=gl_Position.xyz;
  

  vNormal= normalize(glsl_normalmat*vec4(gl_Normal,0.0)).xyz;
}     

