#ifndef GLOBAL_H
#define GLOBAL_H

#ifdef _WIN64
  typedef unsigned char		unchar;
  typedef unsigned short	ushort;
  typedef unsigned int		uint;
  typedef unsigned long		ulong;

#endif


#ifndef _D_FNOGUI_
  #define D_USE_GUI
#endif



#define D_USE_MULTITHREAD
  
#ifdef __CLANG__
  //#include   <libiomp/omp.h>
  #include "ext/omp.h"
#else  
  #include "omp.h"
#endif  

#include "mhs_error.h"

  
   

//this is the default (working version)  
//#define _BIFURCATION_POINTS_CANNOT_TERMINAL_

//this is the default (working version) but with experimental bif-tracking  
//#define _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING

#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING 
  #ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
    #define _BIFURCATION_POINTS_CANNOT_TERMINAL_
  #endif
// #else
//   #define _BIFURCATION_POINTS_CANNOT_TERMINAL_NO_TRACKING
#endif

  
  
#define FAST_SQRT mhs_fast_math<T>::sqrt
//#define FAST_SQRT std::sqrt

// #define BIFURCATION_DEBUG_EDGECHECK
// bool bifurcation_edgecheck_update=false;

// #define _BIF_CD_REQUIRES_2_ALL_
// #define _BIF_CD_REQUIRES_2_ALL_CONNECT_


// #define _POOL_TEST

// #define _DEBUG_DISABLE_DATA_TERM_


const unsigned int max_tracking_treads=101;

// used for path search (this values won't be cleared when the func is called a second time)
unsigned int track_unique_id[max_tracking_treads] = {0};


// static unsigned int track_id=0;
//unsigned int track_id=0;

inline int get_thread_id()
{
      #ifdef D_USE_MULTITHREAD
	      return omp_get_thread_num();
      #else
	      return 0;
      #endif
}


//std::size_t pointlimit=10000000;
//  const int pointlimit=500;


// const std::size_t query_buffer_size=1000;
// const std::size_t query_buffer_size=2000;
//const std::size_t query_buffer_size=20000;
const std::size_t query_buffer_size=5000;


std::string strtrim(std::string s) {
    s.erase(std::remove_if(s.begin(), s.end(), isspace), s.end());
    return s;
}

unsigned int debug_count=0;

const unsigned int voxgrid_default_id=0;


const unsigned int voxgrid_conn_can_id=1;

const unsigned int voxgrid_bif_center_id=2;

std::string application_directory; 


#ifdef  D_USE_GUI
  mhs::CtimeStopper global_timer;
#endif

void set_application_directory()
{
    
    
	std::size_t ndim=1;
	mxArray * app_folder= NULL;
	mexCallMATLAB(1, &app_folder,0,NULL, "ntracker_c_folder");
	sta_assert_error(app_folder!=NULL);
	application_directory=mhs::mex_mex2string(app_folder)+"/";
	printf("application directory: %s\n",application_directory.c_str());
  
}


#include "specialfunc.h"
#include "mhs_matrix.h"

#endif
