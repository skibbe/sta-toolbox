#ifndef PROPOSALS_H
#define PROPOSALS_H

#include "global.h"
#include <algorithm>
#include "particles.h"
#include "energy.h"
#include "constraints.h"
#include "edgecost.h"
#include "mhs_data.h"





template<typename TData,typename T,int Dim> class CTracker;


const char *proposal_names[] = {
    "BIRTH   ",         //      0
    "DEATH   ",         //      1
    "ROTATE  ",         //      2
    "MOVE    ",         //      3
    "SCALE   ",         //      4
    "CONNECT ",         //      5
    "C_BIRTH ",         //      6
    "C_DEATH ",         //      7
    "I_BIRTH ",         //      8
    "I_DEATH ",         //      9
    "B_BIRTH ",         //      10
    "B_DEATH ",         //      11
    "B_CHANGE",         //      12
    "B_TBIRTH",         //      13
    "B_TDEATH",         //      14
    "B_BIRTHS",         //      15
    "B_DEATHS",         //      16
    "B_RECONN",         //      17
    "B_BIRTHL",         //      18
    "B_DEATHL",         //      19
    "B_BIRTHX",         //      20
    "B_DEATHX",         //      21
    "B_BIRTHC",         //      22
    "B_DEATHC",         //      23
    "SCRAMBLE",         //      24
    "SMOOTH  ",         //      25
    "FLIPPOS ",         //      26
    "X_BIRTH ",         //      27
    "X_DEATH ",         //      28
    "MSR     ",         //      29
    "BC_BIRTH",         //      30
    "BC_DEATH",         //      31
    "SEED_INIT",        //      32
    "SPIKE",        //      33
//     "E_CREATE",
//     "E_DELETE",
//     "E_CHANGE",
};


const std::size_t nproposal_names= ( sizeof ( proposal_names ) / sizeof ( proposal_names[ 0 ] ) );;

enum class PROPOSAL_TYPES : unsigned int
{
    PROPOSAL_BIRTH,
    PROPOSAL_DEATH,
    PROPOSAL_ROTATE,
    PROPOSAL_MOVE,
    PROPOSAL_SCALE,
    PROPOSAL_CONNECT,
    PROPOSAL_CONNECT_BIRTH,
    PROPOSAL_CONNECT_DEATH,
    PROPOSAL_INSERT_BIRTH,
    PROPOSAL_INSERT_DEATH,
    PROPOSAL_BIFURCATION_BIRTH,
    PROPOSAL_BIFURCATION_DEATH,
    PROPOSAL_BIFURCATION_CHANGE,
#ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
    PROPOSAL_BIFURCATION_TRACK_BIRTH,
    PROPOSAL_BIFURCATION_TRACK_DEATH,
#else
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING
    PROPOSAL_BIFURCATION_TRACK_BIRTH,
    PROPOSAL_BIFURCATION_TRACK_DEATH,
#endif
#endif
    PROPOSAL_BIFURCATION_BIRTH_S,
    PROPOSAL_BIFURCATION_DEATH_S,
    PROPOSAL_BIFURCATION_RECONN,
    PROPOSAL_BIFURCATION_BIRTH_L,
    PROPOSAL_BIFURCATION_DEATH_L,
    PROPOSAL_BIFURCATION_BIRTH_X,
    PROPOSAL_BIFURCATION_DEATH_X,
    PROPOSAL_BIFURCATION_BIRTH_C,
    PROPOSAL_BIFURCATION_DEATH_C,
    PROPOSAL_CONNECT_SCRAMBLE,
    PROPOSAL_CONNECT_SMOOTH,
    PROPOSAL_FLIP_TERM,
    PROPOSAL_BRIDGE_BIRTH,
    PROPOSAL_BRIDGE_DEATH,
    PROPOSAL_MSR,
    PROPOSAL_BIFURCATION_BRIDGE_BIRTH,
    PROPOSAL_BIFURCATION_BRIDGE_DEATH,
    PROPOSAL_SEED_INIT,
    PROPOSAL_SPIKE,
//     PROPOSAL_EDGE_CREATE,
//     PROPOSAL_EDGE_DELETE,
//     PROPOSAL_EDGE_CHANGE,
    PROPOSAL_UNDEFINED,
};





std::string enum2string ( PROPOSAL_TYPES proposal_type )
{
    switch ( proposal_type ) {
    case PROPOSAL_TYPES::PROPOSAL_BIRTH:
        return proposal_names[0];
        break;
    case PROPOSAL_TYPES::PROPOSAL_DEATH:
        return proposal_names[1];
        break;
    case PROPOSAL_TYPES::PROPOSAL_ROTATE:
        return proposal_names[2];
        break;
    case PROPOSAL_TYPES::PROPOSAL_MOVE:
        return proposal_names[3];
        break;
    case PROPOSAL_TYPES::PROPOSAL_SCALE:
        return proposal_names[4];
        break;
    case PROPOSAL_TYPES::PROPOSAL_CONNECT:
        return proposal_names[5];
        break;
    case PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH:
        return proposal_names[6];
        break;
    case PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH:
        return proposal_names[7];
        break;
    case PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH:
        return proposal_names[8];
        break;
    case PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH:
        return proposal_names[9];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH:
        return proposal_names[10];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH:
        return proposal_names[11];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE:
        return proposal_names[12];
        break;
#ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH:
        return proposal_names[13];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH:
        return proposal_names[14];
        break;
#else
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH:
        return proposal_names[13];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH:
        return proposal_names[14];
        break;

#endif
#endif
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S:
        return proposal_names[15];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S:
        return proposal_names[16];
        break;

    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN:
        return proposal_names[17];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_L:
        return proposal_names[18];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L:
        return proposal_names[19];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X:
        return proposal_names[20];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X:
        return proposal_names[21];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_C:
        return proposal_names[22];
        break;
    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_C:
        return proposal_names[23];
        break;	
    case PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE:
        return proposal_names[24];
        break;		
    case PROPOSAL_TYPES::PROPOSAL_CONNECT_SMOOTH:
        return proposal_names[25];
        break;			
      case PROPOSAL_TYPES::PROPOSAL_FLIP_TERM:
        return proposal_names[26];
        break;				
      case PROPOSAL_TYPES::PROPOSAL_BRIDGE_BIRTH:
        return proposal_names[27];
        break;
      case PROPOSAL_TYPES::PROPOSAL_BRIDGE_DEATH:
        return proposal_names[28];
        break;				
      case PROPOSAL_TYPES::PROPOSAL_MSR:
        return proposal_names[29];
        break;			
      case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_BIRTH:
        return proposal_names[30];
        break;			
 case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_DEATH:
        return proposal_names[31];
        break;	
 case PROPOSAL_TYPES::PROPOSAL_SEED_INIT:
        return proposal_names[32];
        break;
        case PROPOSAL_TYPES::PROPOSAL_SPIKE:
        return proposal_names[33];
        break;	        
	
        /*    case PROPOSAL_TYPES::PROPOSAL_EDGE_CREATE:
                return proposal_names[22];
                break;
            case PROPOSAL_TYPES::PROPOSAL_EDGE_DELETE:
                return proposal_names[23];
                break;
            case PROPOSAL_TYPES::PROPOSAL_EDGE_CHANGE:
                return proposal_names[24];
                break;		*/

    default:
        return "unknown";
    }
}

PROPOSAL_TYPES string2enum ( std::string proposal_name )
{
    proposal_name=strtrim ( proposal_name );

    if ( proposal_name.compare ( strtrim ( proposal_names[0] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIRTH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[1] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_DEATH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[2] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_ROTATE;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[3] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_MOVE;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[4] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_SCALE;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[5] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_CONNECT;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[6] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[7] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[8] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[9] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[10] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[11] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[12] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE;
    }
#ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
    else if ( proposal_name.compare ( strtrim ( proposal_names[13] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[14] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH;
    }
#else
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING
    else if ( proposal_name.compare ( strtrim ( proposal_names[13] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[14] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH;
    }
#endif
#endif
    else if ( proposal_name.compare ( strtrim ( proposal_names[15] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[16] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[17] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[18] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_L;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[19] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[20] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[21] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[22] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_C;
    } else if ( proposal_name.compare ( strtrim ( proposal_names[23] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_C;
    }
    else if ( proposal_name.compare ( strtrim ( proposal_names[24] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE;
    }
      else if ( proposal_name.compare ( strtrim ( proposal_names[25] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_CONNECT_SMOOTH;
    }
       else if ( proposal_name.compare ( strtrim ( proposal_names[26] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_FLIP_TERM;
    }
           else if ( proposal_name.compare ( strtrim ( proposal_names[27] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BRIDGE_BIRTH;
    }
    else if ( proposal_name.compare ( strtrim ( proposal_names[28] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BRIDGE_DEATH;
    }
    else if ( proposal_name.compare ( strtrim ( proposal_names[29] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_MSR;
    }
    else if ( proposal_name.compare ( strtrim ( proposal_names[30] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_BIRTH;
    }
     else if ( proposal_name.compare ( strtrim ( proposal_names[31] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_DEATH;
    }
    else if ( proposal_name.compare ( strtrim ( proposal_names[32] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_SEED_INIT;
    }
    else if ( proposal_name.compare ( strtrim ( proposal_names[33] ) ) == 0 ) {
        return PROPOSAL_TYPES::PROPOSAL_SPIKE;
    }
//     else if (proposal_name.compare(strtrim(proposal_names[22])) == 0)
//          return PROPOSAL_TYPES::PROPOSAL_EDGE_CREATE;
//     else if (proposal_name.compare(strtrim(proposal_names[23])) == 0)
//          return PROPOSAL_TYPES::PROPOSAL_EDGE_DELETE;
//     else if (proposal_name.compare(strtrim(proposal_names[24])) == 0)
//          return PROPOSAL_TYPES::PROPOSAL_EDGE_CHANGE;


    mhs::STAError error;
    error<<"unknown proposal name: "<<proposal_name<<"\n" ;
    throw error;
}



#include "proposal_ConnectWithBifurcationNew.h"
#include "proposal_BirthDeath.h"
#include "proposal_Rotate.h"
#include "proposal_Scale.h"
#include "proposal_Move.h"
#include "proposal_ConnectBirthDeathNew.h"
#include "proposal_InsertBirthDeath.h"
#include "proposal_TriangleBifurcation.h"
#include "proposal_TriangleBifurcationCross.h"
#include "proposal_TriangleBifurcationBirthDeathSimple.h"
#include "proposal_ScrambleEdges.h"
#include "proposal_SmoothEdges.h"
#include "proposal_FlipTermPos.h"
#include "proposal_BridgeBirthDeath.h"
#include "proposal_MoveScaleRotate.h"
#include "proposal_TriangleBifurcationBridge.h"
#include "proposal_InitSeed.h"
#include "proposal_Spike.h"





template<typename TData,typename T,int Dim>
class CProposal
{
    friend CTracker<TData,T,Dim>;
protected:


    static CProposal * proposal_create_from_name ( std::string proposal_name ) {
        try {
            PROPOSAL_TYPES proposal_type=string2enum ( proposal_name );
            switch ( proposal_type ) {
            case PROPOSAL_TYPES::PROPOSAL_BIRTH:
                return new  CBirth<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_DEATH:
                return new  CDeath<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_ROTATE:
                return new  CRotate<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_MOVE:
                return new  CMove<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_SCALE:
                return new  CScale<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_CONNECT:
                return new  CProposalConnect<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH:
                return new  CConnectBirth<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_CONNECT_DEATH:
                return new  CConnectDeath<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH:
                return new  CInsertBirth<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_INSERT_DEATH:
                return new  CInsertDeath<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH:
                return new  CProposalBifurcationBirth<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH:
                return new  CProposalBifurcationDeath<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE:
                return new  CProposalBifurcationChange<TData,T,Dim>();
                break;
#ifndef _BIFURCATION_POINTS_CANNOT_TERMINAL_
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH:
                return new  CProposalBifurcationTrackBirth<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH:
                return new  CProposalBifurcationTrackDeath<TData,T,Dim>();
                break;
#else
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_BUT_TRACKING
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH:
                return new  CProposalBifurcationTrackBirthDouble<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH:
                return new  CProposalBifurcationTrackDeathDouble<TData,T,Dim>();
                break;
#endif
#endif
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S:
                return new  CProposalBifurcationBirthSimple<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S:
                return new  CProposalBifurcationDeathSimple<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN:
                return new  CProposalBifurcationReconn<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_L:
                return new  CProposalBifurcationBirthLine<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L:
                return new  CProposalBifurcationDeathLine<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X:
                return new  CProposalBifurcationBirthSplit<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X:
                return new  CProposalBifurcationDeathSplit<TData,T,Dim>();
                break;
	    case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_C:
                return new  CProposalBifurcationCrossingBirth<TData,T,Dim>();
                break;
            case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_C:
                return new  CProposalBifurcationCrossingDeath<TData,T,Dim>();
                break;
	    case PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE:
                return new  CProposalScrambleEdges<TData,T,Dim>();
                break;		
	    case PROPOSAL_TYPES::PROPOSAL_CONNECT_SMOOTH:
                return new  CProposalSmoothEdges<TData,T,Dim>();
                break;		
	    case PROPOSAL_TYPES::PROPOSAL_FLIP_TERM:
                return new  CProposalFlipTerms<TData,T,Dim>();
                break;			
	    case PROPOSAL_TYPES::PROPOSAL_BRIDGE_BIRTH:
                return new  CBridgeBirth<TData,T,Dim>();
                break;
	    case PROPOSAL_TYPES::PROPOSAL_BRIDGE_DEATH:
                return new  CBridgeDeath<TData,T,Dim>();
                break;			
	    case PROPOSAL_TYPES::PROPOSAL_MSR:
                return new  CMSR<TData,T,Dim>();
                break;		
	case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_BIRTH:
                return new  CProposalBifurcationBridgeBirth<TData,T,Dim>();
                break;			
	case PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_DEATH:
                return new  CProposalBifurcationBridgeDeath<TData,T,Dim>();
                break;
        case PROPOSAL_TYPES::PROPOSAL_SEED_INIT:
                return new  C_init_seed_birth<TData,T,Dim>();
                break;
        case PROPOSAL_TYPES::PROPOSAL_SPIKE:
                return new  CSpike<TData,T,Dim>();
                break;			                
            default:
                std::stringstream error;
                error<<"tracker: proposal "<<proposal_name<<"not implemented yet\n";
                throw mhs::STAError ( error.str() );
            }
        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }



    typedef CTracker<TData,T,Dim> Ctrack;
    Ctrack *tracker;
    std::size_t proposal_called;
    std::size_t proposal_accepted;
    
    T call_w_do;
    T call_w_undo;
    
public:
    virtual ~CProposal() {};
    
    
    
    void set_weights(T call_w_do,T call_w_undo)
    {
      this->call_w_do=call_w_do;
      this->call_w_undo=call_w_undo;
      //printf("\n %f %f \n ",call_w_do,call_w_undo);
    };

    
    CProposal() {
        tracker=NULL;
        proposal_called=0;
        proposal_accepted=0;
	call_w_undo=call_w_do=get_default_weight();
    }

    void set_tracker ( Ctrack & tracker ) {
        this->tracker=&tracker;
    }

    virtual void set_params ( const mxArray * params=NULL ) =0;
    virtual void init ( const mxArray * feature_struct ) =0;

    virtual bool propose() =0;

    virtual std::string get_name() =0;
    virtual T get_default_weight() {
        return 1;
    };

    virtual PROPOSAL_TYPES get_type() =0;
    
    virtual T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return get_default_weight();
    }

    // CBirth
    // CDeath
    static T dynamic_weight_volume(
       std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      T volume_weight=((T)num_connected_particles+1)/(((T)num_particles+1));
      return (1-std::max(volume_weight*volume_particles/(volume_img+1),T(0)));
    }
    
    
    //CProposalBifurcationBirthLine
    //CProposalBifurcationDeathLine
    //CProposalBifurcationDeathSplit
    //CProposalBifurcationBirthSplit
    //CProposalBifurcationCrossingDeath
    //CProposalBifurcationCrossingBirth
    //CProposalBifurcationChange
    //CProposalBifurcationReconn
    static T dynamic_weight_bif(
       std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return ((T)num_bifurcations/((T)num_particles+1));
    }
    
    
    // CMove
    // CRotate
    // CScale
    static T dynamic_weight_particle_update(
       std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return (1);
    }
    
    
    // CProposalConnect
    // CProposalBifurcationTrackBirth
    // CProposalBifurcationTrackDeath
    static T dynamic_weight_edges(
       std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return 1;
    }
    
    
    // CProposalBifurcationBirth
    // CProposalBifurcationDeath
    // CProposalBifurcationDeathSimple
    // CProposalBifurcationBirthSimple    
    // CInsertBirth
    // CInsertDeath    
    static T dynamic_weight_segment(
       std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return ((((T)num_segments+1))/((T)num_edges+1));
    }
    
    
    // CConnectBirth
    // CConnectDeath
     static T dynamic_weight_terminal(
       std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return 1;
    }
};

    




#endif

