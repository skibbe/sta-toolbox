#ifndef PROPOSAL_BIFURCATION_BD_CROSS_H
#define PROPOSAL_BIFURCATION_BD_CROSS_H

#include "proposals.h"
#include "edges.h"
#include "mhs_graphics.h"


#define BRIDGETBIRTHDEATH_SINGLE_PART


 #define DEBUG__BIF_NORMAL

template<typename TData,typename T,int Dim> class CProposal;

template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathSimple;
template<typename TData,typename T,int Dim>
class CProposalBifurcationReconn;
template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathSplit;
template<typename TData,typename T,int Dim>
 class CProposalBifurcationCrossingDeath;

template<typename TData,typename T,int Dim>
class CProposalBifurcationBirthSimple : public CProposal<TData,T,Dim>
{
    friend class CProposalBifurcationDeathSimple<TData,T,Dim>;
    friend class CProposalBifurcationReconn<TData,T,Dim>;

protected:
    T Lprior;
    T ConnectEpsilon;
    bool sample_scale_temp_dependent;
public:
  

   T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
      
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_segment(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  

    CProposalBifurcationBirthSimple(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
     }



    ~CProposalBifurcationBirthSimple()
    {

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S);
    };
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	 if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	 
	 if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
        }
    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
	OctTreeNode<T,Dim> &		bifurcation_tree	=*(this->tracker->bifurcation_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	
        this->proposal_called++;


        int stat_count=0;


        bool debug=false;

       

        class Connection<T,Dim> * existing_connection;

        /// uniformly pic edge
        existing_connection=connections.get_rand_edge();
        if (existing_connection==NULL)
            return false;


        sta_assert_debug0(existing_connection->pointA->connected!=NULL);
        sta_assert_debug0(existing_connection->pointB->connected!=NULL);

	
	if (options.bifurcation_neighbors)
	{
	  if (existing_connection->edge_type!=EDGE_TYPES::EDGE_SEGMENT)
	    return false;
	}else
	{
	if (((existing_connection->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)||
 	  (existing_connection->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)))
	 return false; 
	}
	
        sta_assert_debug0(existing_connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION);

	if (sample_scale_temp_dependent && ((temp>existing_connection->pointA->point->get_scale()||temp>existing_connection->pointB->point->get_scale())))
	{
	      return false; 
	}
	
	
        try
        {
            class Points<T,Dim>::CEndpoint *  proposed_endpoint=NULL;

            T connection_prob;
            T connection_costs;
	    
	    
	    
	    

            if (!do_tracking3(
                        edgecost_fun,
                        &proposed_endpoint,
                        &connection_prob,
                        &connection_costs,
                        conn_tree,
			options.bifurcation_neighbors,
                        existing_connection->pointA,
			existing_connection->pointB,
                        search_connection_point_center_candidate,
                        conn_temp,
                        options.bifurcation_bonus_L,
                        Lprior,
			ConnectEpsilon))
            {
                return false;
            }

	    if (debug)
	    printf("B\n");            
            

            if (connection_prob<std::numeric_limits<T>::epsilon())
                return false;
	    
	    if ((Connections<T,Dim>::connection_exists(*(existing_connection->pointA->point),*(proposed_endpoint->point)))||
	      (Connections<T,Dim>::connection_exists(*(existing_connection->pointB->point),*(proposed_endpoint->point)))
	    )
	    {
		//sta_assert_error(proposed_endpoint->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		return false;
	    }

	    if (debug)
	    printf("0\n");
	    
	     
	    T particle_costs_old=0;
	      T particle_costs_new=0;
	    particle_costs_old+=existing_connection->pointA->point->compute_cost3(temp); //old segment point A
	    particle_costs_old+=existing_connection->pointA->point->compute_cost3(temp); //old segment point B
	    particle_costs_old+=proposed_endpoint->point->compute_cost3(temp); //proposed new point
	    
	    
	    #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_NORMAL
			GuiPoints<T> * gps= ( ( GuiPoints<T> * ) proposed_endpoint->point ) ;
	    #endif
	    #endif
	    

            sta_assert_debug0(existing_connection!=NULL);
	    sta_assert_debug0(proposed_endpoint!=NULL);
	    
	    class Points<T,Dim> & point=*(proposed_endpoint->point);

	    // edge costs of the OLD configuration
	    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	    T old_edgecost=existing_connection->e_cost(
	      edgecost_fun,
	      options.connection_bonus_L,
	      options.bifurcation_bonus_L,
	      inrange);
	    
	    /// of course the old edge should be in range (valid)
	    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
	
	    
	  class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	  class Collision<T,Dim>::Candidates candA(query_bufferA);
	  
	  class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
	  class Collision<T,Dim>::Candidates candB(query_bufferB);
	  
	  class OctPoints<T,Dim> * query_bufferC[query_buffer_size];
	  class Collision<T,Dim>::Candidates candC(query_bufferC);
	  
	  T particle_interaction_cost_old=0;
	  T particle_interaction_cost_new=0;
	  
	  
	  class Points<T,Dim> & pointA=point;
	  class Points<T,Dim> & pointB=*(existing_connection->pointA->point);
	  class Points<T,Dim> & pointC=*(existing_connection->pointB->point);
	  
	  int collision_mode=1;
	  if (collision_fun_p.is_soft())  
	  {
	     T tmp;
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointA,
					    collision_search_rad,
					    temp,&candA,true,-9,collision_mode));
	      
	      particle_interaction_cost_old+=tmp;
	      
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointB,
					    collision_search_rad,
					    temp,&candB,true,-9,collision_mode));
	      
	      particle_interaction_cost_old+=tmp;	 
	      
	       sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointC,
					    collision_search_rad,
					    temp,&candC,true,-9,collision_mode));
	      
	      particle_interaction_cost_old+=tmp;	 
	      
	  }
	  
	  

	    
	    

	    // create the bifurcation particle
	    Points<T,Dim> * p_b_particle=particle_pool.create_obj();    
	    Points<T,Dim> & b_particle=*p_b_particle;
	    b_particle.tracker_birth=3;
	    b_particle.set_direction(Vector<T,Dim>(1,0,0));
	    
	    //we back-up the existing edge and remove it
	    class Connection<T,Dim> backup_existing_edge=*existing_connection;
	    connections.remove_connection(existing_connection);
	    
	    //we add three new connections to the bif particle
	    class Connection<T,Dim> * new_connections[3];
	    class Points<T,Dim>::CEndpoint * bifurcation_endpoints[3];
	    bifurcation_endpoints[0]=proposed_endpoint;
	    bifurcation_endpoints[1]=backup_existing_edge.pointA;
	    bifurcation_endpoints[2]=backup_existing_edge.pointB;
	    
	    sta_assert_debug0(*bifurcation_endpoints[0]->endpoint_connections==0);
	    sta_assert_debug0(*bifurcation_endpoints[1]->endpoint_connections==0);
	    sta_assert_debug0(*bifurcation_endpoints[2]->endpoint_connections==0);
	    
	    class Connection<T,Dim> a_new_connection;
	    a_new_connection.edge_type=EDGE_TYPES::EDGE_BIFURCATION;
	    
	    ///create the three edges
	    for (int i=0;i<3;i++)
	    {
	      a_new_connection.pointA=b_particle.getfreehub(Points<T,Dim>::bifurcation_center_slot);
	      Points<T,Dim> * pointB=bifurcation_endpoints[i]->point;
	      a_new_connection.pointB=pointB->getfreehub(bifurcation_endpoints[i]->side);
	      
	      if (pointB!=&point)
	      {
		a_new_connection.freeze_topology=backup_existing_edge.is_protected_topology();
	      }
	      
	      
	      
	      sta_assert_debug0(a_new_connection.pointB->point!=a_new_connection.pointA->point);

	      
	      new_connections[i]=connections.add_connection(a_new_connection);
    #ifdef _DEBUG_CHECKS_0_
	      switch(i) 
	      {
		case 0:
		  sta_assert_error(b_particle.particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
		  break;
		case 1:
		  sta_assert_error(b_particle.particle_type==(PARTICLE_TYPES::PARTICLE_SEGMENT));
		  break;
		case 2:
		  sta_assert_error(b_particle.particle_type==(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
		  break;  
	      }
    #endif	    
	    }
	    
	    sta_assert_debug0((b_particle.endpoint_connections[Points< T, Dim >::bifurcation_center_slot])==3);
	    #ifdef _DEBUG_CHECKS_0_
	      for (int i=0;i<3;i++)
		sta_assert_debug0((b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->particle_type)==(PARTICLE_TYPES::PARTICLE_BIFURCATION));
	    #endif

	    T new_edgecost=connection_costs;  
	     T bifurcation_data_new=0;    
	    
	    bool out_of_bounce=false; 
	    b_particle.bifurcation_center_update(shape,out_of_bounce);
	    
	    if (out_of_bounce)
	    {
		goto cleanup_particle_and_edges;
	    }  
	    
	    
	  
	    particle_costs_new+=b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->point->compute_cost3(temp); 
	    particle_costs_new+=b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected->point->compute_cost3(temp); 
	    particle_costs_new+=b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected->point->compute_cost3(temp); 
  
	  /*
	    #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_NORMAL
			 if ( gps->is_selected )
			 {
			    printf("new interaction\n");
			 }
	    #endif
	    #endif*/
	    

	  if (collision_fun_p.is_soft())  
	  {
	     T tmp;
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointA,
					    collision_search_rad,
					    temp,&candA,false,-10,collision_mode));
	      
	      particle_interaction_cost_new+=tmp;
	      
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointB,
					    collision_search_rad,
					    temp,&candB,false,-10,collision_mode));
	      
	      particle_interaction_cost_new+=tmp;	 
	      
	       sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointC,
					    collision_search_rad,
					    temp,&candC,false,-10,collision_mode));
	      
	      particle_interaction_cost_new+=tmp;	 
	      
	      if (options.bifurcation_particle_soft)	
	      {
		 if (collision_fun_p.colliding(tmp,
					      tree,
					    b_particle,
					    collision_search_rad,
					    temp))
		 {
		   goto cleanup_particle_and_edges;
		 }
	      
		particle_interaction_cost_new+=tmp;	 	
	      }
	      
	  }
	    
	  
	 
// 	  #ifdef  D_USE_GUI
// 	    #ifdef  DEBUG__BIF_NORMAL
// 			 if ( gps->is_selected )
// 			 {
// 			    printf("data term\n ");
// 			 }
// 	    #endif
// 	    #endif
	  
	    
	 
	  
	  if (!b_particle.compute_data_term(data_fun,bifurcation_data_new))
	  {
	    goto cleanup_particle_and_edges;
	  }
	  
	  b_particle.point_cost=bifurcation_data_new;

    {
		  
		T E=(new_edgecost-old_edgecost)/temp;
// 		  E+=(terminal_point_cost_change)/temp;
		E+=(particle_costs_new-particle_costs_old)/temp;
		E+=(bifurcation_data_new)/temp;
		E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;

		  T R=mhs_fast_math<T>::mexp(E);
		  
		T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()); 
		connection_prob*=1.0/(connections.pool->get_numpts()-1);
		R*=remove_connection_prob/((connection_prob)+std::numeric_limits<T>::epsilon());

		
		
		
		
	if (debug)
	    printf("1\n");	

	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	    
	    #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_NORMAL
			 if ( gps->is_selected )
			 {
			    printf("BIF BIRTH: EC (%f => %f)? PC (%f => %f) BD (%f) IC(%f => %f) R (%f/%f)\n ",
				   
			      old_edgecost,new_edgecost,particle_costs_old,particle_costs_new,bifurcation_data_new,
			      particle_interaction_cost_old,particle_interaction_cost_new,
			      remove_connection_prob,connection_prob
			    );
			 }
	    #endif
	    #endif	    
	    
	    
	    if (debug)
	      printf("2\n");
	    
// 	  #ifdef  D_USE_GUI
// 	    #ifdef  DEBUG__BIF_NORMAL
// 			 if ( gps->is_selected )
// 			 {
// 			    printf("double bif?\n ");
// 			 }
// 	    #endif
// 	    #endif	    
	    
		if (options.no_double_bifurcations)
		{
		    if (p_b_particle->connects_two_bif())
		    {
			 goto cleanup_particle_and_edges;
		    }
		}



		
		
		if (options.bifurcation_particle_hard)	
		{
		  if (collision_fun_p.colliding( tree,b_particle,collision_search_rad,temp))
		  {
		      goto cleanup_particle_and_edges;
		  }
		}
	/*	
	  #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_NORMAL
			 if ( gps->is_selected )
			 {
			    printf("loop??\n");
			 }
	    #endif
	    #endif	    */		

		// check for loops by following new edge
		if (Constraints::constraint_hasloop_follow(b_particle,new_connections[0],options.constraint_loop_depth+1))
		{
		    goto cleanup_particle_and_edges;
		}
		
		
		if (options.bifurcation_particle_hard || options.bifurcation_particle_soft)	
		{
		  if(!tree.insert(b_particle))
		  {
		    goto cleanup_particle_and_edges;
		  }
		}
		
		if(!bifurcation_tree.insert(b_particle))
		{
		  goto cleanup_particle_and_edges;
		}
		
		
		if ((point.endpoint_connections[0]>0)&&(point.endpoint_connections[1]>0))
		  point.unregister_from_grid(voxgrid_conn_can_id);

		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
      #ifdef BIFURCATION_DEBUG_EDGECHECK
	  (b_particle.e_cost(edgecost_fun,
			options.connection_bonus_L,
		      options.bifurcation_bonus_L,
		      inrange,true,false,bifurcation_edgecheck_update));
	    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
      #endif
	
	    
	#ifdef  D_USE_GUI
	    b_particle.touch(get_type());
	    for (int i=0;i<3;i++)
	    {
		b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->touch(get_type());
	    }
	  #endif			
	  
	  	int new_id=(b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected->point->_path_id);
		b_particle._path_id=new_id;

	    return true;
	  }
	}
	  

	  cleanup_particle_and_edges:	  
		    sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION);
		    connections.remove_connection(new_connections[0]);
		    connections.remove_connection(new_connections[1]);
		    connections.remove_connection(new_connections[2]);
		    connections.add_connection(backup_existing_edge);
	  cleanup_particle:	  	  
		    particle_pool.delete_obj(p_b_particle);
		    return false;


        } catch(mhs::STAError & error)
        {
            throw error;
        }
    }




    // searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
    static bool  do_tracking3(
        class CEdgecost<T,Dim> & edgecost_fun,
        class Points<T,Dim>::CEndpoint   **  proposed_endpoint, // OLD: if !=NULL then it is a request to get value for an existing edge
        T *  connection_prob,
        T *  connection_cost,
        OctTreeNode<T,Dim> & tree,
	bool bifurcation_neighbors,
        //class Connection<T,Dim> & existing_connection,
	class Points<T,Dim>::CEndpoint * edgepointA,
	class Points<T,Dim>::CEndpoint * edgepointB,
        T search_connection_point_center_candidate, 
        T TempConnect,
        T bifurL,
        T Lprior,
	T ConnectEpsilon,
        int mode=0 
	  // if mode: 0->propose edge at proposed_endpoint[0], connection_prob[0] ,connection_cost[0]
	  // if mode: 1->values for edge given at proposed_endpoint[0] , connection_prob[0] ,connection_cost[0]
	  // if mode: 2->values for edge given at proposed_endpoint[0], connection_prob[0] ,connection_cost[0],  propose edge at proposed_endpoint[1], connection_prob[1] ,connection_cost[1]
    )
    {
      
    
      
      
        T searchrad=CEdgecost<T,Dim>::get_current_edge_length_limit();

	sta_assert_debug0(edgepointA!=edgepointB);


        bool debug=false;
        if (debug)
            printf("do_tracking birfurcation\n");


        std::size_t total_candidates=0;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;


	
	class Points<T,Dim>::CEndpoint * endpoints[2];
	endpoints[0]=edgepointA;
	endpoints[1]=edgepointB;
	
        Vector<T,Dim> * endpoint_pos[2];
        const Vector<T,Dim> * point_pos[2];
        Points<T,Dim> * points[2];

        for (int i=0; i<2; i++)
        {
            class Points<T,Dim>::CEndpoint * enpt=endpoints[i];
            int & side=enpt->side;
            class Points<T,Dim>  * point=enpt->point;
            endpoint_pos[i]=enpt->position;
            point_pos[i]=&point->get_position();
            points[i]=point;
        }

        Vector<T,Dim> center=(*point_pos[0]+*point_pos[1])/2;
        T point_dist=std::sqrt((*point_pos[0]-*point_pos[1]).norm2());

        try {
            tree.queryRange(
                center,
                search_connection_point_center_candidate+point_dist,
                candidates,
                query_buffer_size,
                total_candidates);
        } catch (mhs::STAError & error)
        {
            throw error;
        }
        
        /// no points nearby. do nothing
        if ((total_candidates<3)&&((mode!=1)))
        {
	    int found=0;
	    for (int i=0;i<total_candidates;i++)
	    {
	      found+=((query_buffer[i]==points[0])||(query_buffer[i]==points[1]));
	    }
	    
	    if (found==total_candidates)
	    {
	      if (debug)
		  printf("no points\n",total_candidates);
	      return false;
	    }
        }



        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        class Points< T, Dim >::CEndpoint * point_candidates_buffer[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** point_candidates=point_candidates_buffer;

        //int compute_probability_for_indx=-1;

	
	if (debug)
            printf("searching candidates\n");

        std::size_t num_point_candidates=0;
        {

            {
                /*!
                check for all candidates in the rectangle
                  if one of the endpoints lies within the circle
                    with radius "searchrad"
                */
                for (std::size_t a=0; a<total_candidates; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
                    ///TODO exclude directly connected points (?) only when not reconnecting
		    /// when reconecting we want to be able to switch sides
                    if ((point!=points[0])&&(point!=points[1]))
		    //if ((mode==2)||((point!=points[0])&&(point!=points[1])))
                    {
		      
      	      
		      
                        // point in range of both points!! ?
                        //WARNING propbably is should chack (!dist>distmax) to include equal case
//                         if ((search_connection_point_center_candidate>(Vector<T,Dim>(point->get_position())-points[0]->get_position()).norm2())&&
//                                 (search_connection_point_center_candidate>(Vector<T,Dim>(point->get_position())-points[1]->get_position()).norm2()))
		      if (	(bifurcation_neighbors || (point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))&&
				(search_connection_point_center_candidate>(point->get_position()-points[0]->get_position()).norm2())&&
                                (search_connection_point_center_candidate>(point->get_position()-points[1]->get_position()).norm2()))
                        {
                            // at least one endpoint is free
                            sta_assert_debug0((point->endpoint_connections[0]==0)||(point->endpoint_connections[1]==0));

                            /// each side might be a good candidate
                            for (int pointside=0; pointside<2; pointside++)
                            {
                                // connected
                                if (point->endpoint_connections[pointside]>0)
                                    continue;

                                class Points<T,Dim>::CEndpoint * endpointsA=point->endpoints[pointside][0];

//                                 endpointsA->point->update_endpoint(endpointsA->side);
                                //WARNING propbably should check (!dist>distmax) to include equal case
                                if ((searchrad>(*endpoint_pos[0]-*endpointsA->position).norm2())&&
                                        (searchrad>(*endpoint_pos[1]-*endpointsA->position).norm2())
                                   )
                                {
                                    (*point_candidates++)=endpointsA;
                                    num_point_candidates++;
                                }
                            }
                        }
                    }
                }

            }
        }
        
        

        
        // if proposed_endpoint
        if (mode>0)
	{
	   sta_assert_error(num_point_candidates+1<query_buffer_size);
	   (*point_candidates++)=proposed_endpoint[0];
	   num_point_candidates++;
	}
	
	
	if (debug)
            printf("searching candidates done\n");


//         if ((compute_probability_for_point!=NULL)&&(compute_probability_for_indx<0))
//         {
//             throw mhs::STAError("couldn't find point");
//             return false;
//         }

        /// no points nearby -> do nothing
        if ((num_point_candidates==0))
        {
            if (debug)
                printf("no edges %d\n",num_point_candidates);

            return false;
        }


        if (debug)
            printf("computing probabilities for %d points\n",num_point_candidates);

        T connet_cost[query_buffer_size];
        T connet_prop[query_buffer_size];
        T connet_prop_acc[query_buffer_size];


        point_candidates=point_candidates_buffer;
        int candidates_count=0;
        for (std::size_t i=0; i<num_point_candidates; i++)
        {

            class Points< T, Dim >::CEndpoint * candidate_endpoint=point_candidates_buffer[i];
            typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;



            T u=edgecost_fun.e_cost(*candidate_endpoint,
                                    *(edgepointA),
                                    *(edgepointB),
                                    //searchrad,
                                    //(CEdgecost<T,Dim>::max_edge_length>-1) ? -1 : searchrad, ///NOTE we sort out all edges not in distance already
                                    inrange,
                                    false
                                   );

            sta_assert_error(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));


            if (debug)
            {
                printf("u:  %f\n",u);
            }
            
            
              if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	      {
		 
		///NOTE in case of time dependent thickness, the triangle may become invalid
		/// in this case we asing a very high cost to the triangle
	        if ((mode==0)||((i+1<num_point_candidates)||(( inrange != CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_AREA))))
		{
		  continue;
		}
	      }
	      
	      connet_prop[candidates_count]=mhs_fast_math<T>::mexp((u+Lprior)/TempConnect)+ConnectEpsilon;
	      connet_cost[candidates_count]=u+bifurL;

//             if (inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
//                 connet_prop[candidates_count]=mexp((u+Lprior)/TempConnect)+ConnectEpsilon;
//             else
//                 connet_prop[candidates_count]=0;
	      
	      sta_assert_error_c(!(connet_prop[candidates_count]<0),
		printf("\n %f \n,%f \n,%f \n,%f \n,%f \n,%f \n",
		  u,
		  Lprior,
		  TempConnect,
		  ConnectEpsilon,
		  mhs_fast_math<T>::mexp((u+Lprior)/TempConnect)+ConnectEpsilon,
		  std::exp(-(u+Lprior)/TempConnect)+ConnectEpsilon
		)
	      );

	      if (candidates_count==0)
	      {
		  connet_prop_acc[candidates_count]=connet_prop[candidates_count];
	      }
	      else
	      {
		  connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
	      }
	      
	      //alining all valid endpoints to the beginning of the array
	      point_candidates_buffer[candidates_count]=candidate_endpoint;
	      
	      
	      candidates_count++;
        }
        
        
        std::size_t num_point_candidates_debug=num_point_candidates;
        sta_assert_debug0(!(num_point_candidates<candidates_count));
	num_point_candidates=candidates_count;
	
	      
	
	/// no edges nearby -> do nothing
	if ((num_point_candidates==0))
	{
	    if (debug)
		printf("no valid endpoints %d\n",num_point_candidates);

	    if (mode!=0)
	    {
	    
	     class Points< T, Dim >::CEndpoint * candidate_endpoint=point_candidates_buffer[num_point_candidates_debug-1];
            typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;


	    printf("\n--------------------\n");
            T u=edgecost_fun.e_cost(*candidate_endpoint,
                                    *(edgepointA),
                                    *(edgepointB),
                                    //searchrad,
                                    //(CEdgecost<T,Dim>::max_edge_length>-1) ? -1 : searchrad, ///NOTE we sort out all edges not in distance already
                                    inrange,
                                    true
                                   );
	    printf("\n--------------------\n");
	    if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))
	    {
	      printf("\nERROR:  %d\n",inrange );
	    }
	    
            sta_assert_error(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE));
	    
	    }
	    sta_assert_debug0((mode==0));
	    
	    return false;
	}


        if (mode>0)
        {
	    sta_assert_error(point_candidates_buffer[candidates_count-1]==proposed_endpoint[0])
            connection_prob[0]=connet_prop[candidates_count-1]/(connet_prop_acc[candidates_count-1]+std::numeric_limits< T >::epsilon());
            connection_cost[0]=connet_cost[candidates_count-1];
	    if (mode==1)
	      return true;
        }



        if (debug)
            printf("creating proposal\n");

        /*!
          now we choose a point candidate with probability connet_prop
          using connet_prop_acc from the edge candidate list
        */
        {
	    unsigned int proposed_indx=(mode>0) ? 1 : 0;
	  
            // pic candidate with propability connect_prop
            std::size_t connection_candidate=rand_pic_array(connet_prop_acc,num_point_candidates,connet_prop_acc[candidates_count-1]);
	    
	    // if the new particle is the old one than do nothing
	    if ((mode==2)&&(candidates_count==connection_candidate+1))
	      return false;

            if (debug)
                printf("choosing %d\n",connection_candidate);

            sta_assert_debug0(connection_candidate>=0);
            sta_assert_debug0(connection_candidate<num_point_candidates);


            if (debug)
                printf("connection proposal\n");

            sta_assert_debug0(connection_candidate>=0);
            sta_assert_debug0(connection_candidate<num_point_candidates);

            if (connet_prop_acc[candidates_count-1]<std::numeric_limits<T>::epsilon())
            {
	        if (num_point_candidates>0)
	  printf("%d %f\n",num_point_candidates,connet_prop_acc[candidates_count-1]);
                connection_prob[proposed_indx]=0;
                return false;
            }
            else
            {
                connection_prob[proposed_indx]=connet_prop[connection_candidate]/(connet_prop_acc[candidates_count-1]);
            }



            connection_cost[proposed_indx]=connet_cost[connection_candidate];

            proposed_endpoint[proposed_indx]=point_candidates_buffer[connection_candidate];



            if (debug)
                printf("conn index %d\n",connection_candidate);
            return true;
        }


        mhs::STAError error;
        error<<"should not be reached! ("<<enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S)<<")";
        throw error;
        //throw mhs::STAError("should not be reached!");
        return true;



    }



};














template<typename TData,typename T,int Dim>
class CProposalBifurcationDeathSimple : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
      T ConnectEpsilon;
       bool sample_scale_temp_dependent;
public:


   T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
     return CProposal<TData,T,Dim>::dynamic_weight_segment(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }

    CProposalBifurcationDeathSimple(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
    }

    ~CProposalBifurcationDeathSimple()
    {
//         printf("\n");
//         for (int i=0; i<max_stat; i++)
//             printf("%d ",statistic[i]);
//         printf("\n");

    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S);
    };
    
    PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	
		 if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
		 }
    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
        OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Connections<T,Dim>  & connections			=this->tracker->connections;
        T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);

        this->proposal_called++;
	
	
	
	bool debug=false;
	
	/// no bifurcations?
	if (connections.get_num_bifurcation_centers()<1)
	  return false;

	int stat_count=0;


	try
	{
	/// randomly pic a bifurcation center node
	Points<T,Dim> &  point=connections.get_rand_bifurcation_center_point();	  
	
	sta_assert_debug0(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
	
	//uniformly pick an edge that is supposed to be removed
	int remove_edge=std::rand()%3;
	
// 	if  (point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connection->is_protected_topology())
// 	{
// 	  return false;
// 	}
	
	class Points<T,Dim> * new_terminal_point=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connected->point;


	
	
	if (point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connection->is_protected_topology())
	{
	 return false; 
	}
	
	if (sample_scale_temp_dependent && ((temp>new_terminal_point->get_scale())))
	{
	      return false; 
	}	
	
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;

	class Connection< T, Dim >    * p_new_edges;
	class Connection< T, Dim >    new_edges;
	new_edges.edge_type=EDGE_TYPES::EDGE_SEGMENT;
	
	int protected_edges=0;
	
	int j=0;
	for (int i=0;i<3;i++)
	{
	  if (i!=remove_edge)
	  {
	    sta_assert_debug0(j<2);
	    new_edges.points[j]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected;
	    protected_edges+=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connection->is_protected_topology();
	    j++;
	  }
	}
	
	 #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_NORMAL
			GuiPoints<T> * gps= ( ( GuiPoints<T> * ) new_terminal_point ) ;
	    #endif
	    #endif
	    
	
	
	sta_assert_error((protected_edges==0)||(protected_edges==2));
	
	new_edges.freeze_topology=(protected_edges==2);
	
	
	
	//compute costs for new edge
	T  new_edgecost=new_edges.e_cost(
	   edgecost_fun,
	    options.connection_bonus_L,
	    options.bifurcation_bonus_L,
	    inrange
	);
	
	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	{
// 	  for (int i=0;i<3;i++)
// 	    connections.add_connection(backup_old_edges[i]);
	  bool check=((inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_ANGLE)||(inrange==CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT));
	  sta_assert_error(check);
	  
	  if (!check)
	    printf("? CProposalBifurcationDeathSimple out of range\n");
	  else 
	    printf("? CProposalBifurcationDeathSimple edge vialotes angle\n");

	  
	  return false;
	}
	
	
	  class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
	  class Collision<T,Dim>::Candidates candA(query_bufferA);
	  
	  class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
	  class Collision<T,Dim>::Candidates candB(query_bufferB);
	  
	  class OctPoints<T,Dim> * query_bufferC[query_buffer_size];
	  class Collision<T,Dim>::Candidates candC(query_bufferC);
	  
	  T particle_interaction_cost_old=0;
	  T particle_interaction_cost_new=0;
	  
	  
	  class Points<T,Dim> & pointA=*new_terminal_point;
	  class Points<T,Dim> & pointB=*(new_edges.pointA->point);
	  class Points<T,Dim> & pointC=*(new_edges.pointB->point);
	  
	   Points< T, Dim > & b_particle=point;
	  
	  int collision_mode=1;
	  if (collision_fun_p.is_soft())  
	  {
	    
	    
	    
	    
	     T tmp;
	     
	      if (options.bifurcation_particle_soft)	
	      {
		 sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    b_particle,
					    collision_search_rad,
					    temp));
	      
		particle_interaction_cost_old+=tmp;	 	
		
		sta_assert_error(b_particle.has_owner(voxgrid_default_id));
		 //NOTE we hide the bridge point from the tree
		  std::size_t  npts_old=tree.get_numpts();
		  b_particle.unregister_from_grid (voxgrid_default_id);
		  std::size_t  npts_new=tree.get_numpts();
		  sta_assert_debug0 ( npts_new+1==npts_old );

		
	      }
	     
	     
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointA,
					    collision_search_rad,
					    temp,&candA,true,-9,collision_mode));
	      
	      particle_interaction_cost_old+=tmp;
	      
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointB,
					    collision_search_rad,
					    temp,&candB,true,-9,collision_mode));
	      
	      particle_interaction_cost_old+=tmp;	 
	      
	       sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointC,
					    collision_search_rad,
					    temp,&candC,true,-9,collision_mode));
	      
	      particle_interaction_cost_old+=tmp;	 
	      
	  }	
	  
	  
	  
	   
	  
// 	  T bifurcation_data_old=0;  
// 	  sta_assert_error(b_particle.compute_data_term(data_fun,bifurcation_data_old));
	  
	  T bifurcation_data_old=b_particle.point_cost;
	  
	
	
	
	
	
// 	class Points<T,Dim>::CEndpoint ** endpoints=new_edges.points;
	class Points<T,Dim>::CEndpoint * proposed_endpoint=point.endpoints[Points<T,Dim>::bifurcation_center_slot][remove_edge]->connected;

	T connection_prob;
	T connection_costs;

	if (!CProposalBifurcationBirthSimple<TData,T,Dim>::do_tracking3(
		    edgecost_fun,
		    &proposed_endpoint,
		    &connection_prob,
		    &connection_costs,
		    conn_tree,
		    options.bifurcation_neighbors,
		    new_edges.pointA,
		    new_edges.pointB,
		    search_connection_point_center_candidate,
		    //options.connection_candidate_searchrad,
		    conn_temp,
		    options.bifurcation_bonus_L,
		    Lprior,ConnectEpsilon,1))
	{
	    mhs::STAError error;
 	    error<<"do_tracking3:should not be reached! ("<<enum2string(PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S)<<")";
 	  throw error;
	    return false;
	}
	
	
	
	
	T particle_costs=0;
	particle_costs-=point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point->compute_cost3(temp); 
	particle_costs-=point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->point->compute_cost3(temp); 
	particle_costs-=point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->point->compute_cost3(temp); 
	

	T old_edgecost=connection_costs;
	
	
	
	//backup old edges 
	#ifdef  D_USE_GUI
	  class Points<T ,Dim > * activity_pts[3];
	#endif
	class Connection< T, Dim >  backup_old_edges[3];
	for (int i=0;i<3;i++)
	{
	  backup_old_edges[i]=*point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connection;
	  #ifdef  D_USE_GUI
	    activity_pts[i]=point.endpoints[Points<T,Dim>::bifurcation_center_slot][i]->connected->point;
	  #endif
	}

	
	 //remove old edges 
	for (int i=0;i<3;i++)
	{
	  if(debug)	
	      printf("[%i]:",i);	
	  class Connection< T, Dim > * delete_me=point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connection;
	  sta_assert_debug0((point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected)!=NULL);
	  connections.remove_connection(delete_me);
	  if (i<2)
	    sta_assert_debug0((point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected)!=NULL);
	}
	
	
	
	
	try {
	
	  sta_assert_error(!(Connections<T,Dim>::connection_exists(new_edges)));  
	  
	//add new edge
	p_new_edges=connections.add_connection(new_edges);
	
	}
	catch(mhs::STAError & error)
        {
	  printf("%u\n",new_edges.pointA->side);
	  printf("%u\n",new_edges.pointA->point->endpoint_connections[new_edges.pointA->side]);
	  printf("%u\n",new_edges.pointB->side);
	  printf("%u\n",new_edges.pointB->point->endpoint_connections[new_edges.pointB->side]);
	  
	  
	  printf("%u %u %u\n",new_edges.pointA->point,new_edges.pointB->point,&point);
	  
	  
	  if (new_edges.pointA->opposite_slots[0]->connected!=NULL)
	  {
	    printf("%u %u\n",new_edges.pointA->point,new_edges.pointA->opposite_slots[0]->connected->point);
	  }
	  if (new_edges.pointB->opposite_slots[0]->connected!=NULL)
	  {
	    printf("%u %u %u\n",new_edges.pointB->point,new_edges.pointB->opposite_slots[0]->connected->point,new_edges.pointB->opposite_slots[0]->connected->point->get_num_connections());
	  }
	  
	  for (int i=0;i<3;i++)
	  printf("old edge: %u %u\n",backup_old_edges[i].pointA->point,backup_old_edges[i].pointB->point);
	  
	  
	  printf("WTF %u \n",&point);
            throw error;
        }  
        
        
        if (collision_fun_p.is_soft())  
	  {
	     T tmp;
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointA,
					    collision_search_rad,
					    temp,&candA,false,-10,collision_mode));
	      
	      particle_interaction_cost_new+=tmp;
	      
	      sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointB,
					    collision_search_rad,
					    temp,&candB,false,-10,collision_mode));
	      
	      particle_interaction_cost_new+=tmp;	 
	      
	       sta_assert_error(!collision_fun_p.colliding(tmp,
					      tree,
					    pointC,
					    collision_search_rad,
					    temp,&candC,false,-10,collision_mode));
	      
	      particle_interaction_cost_new+=tmp;	 
	      
	  }
	  
	  
	
	
	particle_costs+=new_edges.pointA->point->compute_cost3(temp); 
	particle_costs+=new_edges.pointB->point->compute_cost3(temp); 
	particle_costs+=new_terminal_point->compute_cost3(temp); 

	T E=(new_edgecost-old_edgecost)/temp;
// 	  E+=(terminal_point_cost_change)/temp;
	E+=(particle_costs)/temp;
	
	E-=(bifurcation_data_old)/temp;
	
	
	E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;

          T R=mhs_fast_math<T>::mexp(E);
	
	T create_connection_prob=connection_prob/connections.pool->get_numpts(); 
	T remove_connection_prob=1.0/(connections.get_num_bifurcation_centers()-1+std::numeric_limits< T >::epsilon());
	R*=create_connection_prob/((remove_connection_prob)+std::numeric_limits<T>::epsilon());

		
// #ifdef  D_USE_GUI
// 	    #ifdef  DEBUG__BIF_NORMAL
// 			 if ( gps->is_selected )
// 			 {
// 			    printf("prop: BIF DEATH: EC (%f => %f)? PC (%f BD (%f) IC(%f => %f) R (%f/%f)\n "
// 			    ,old_edgecost,new_edgecost,particle_costs,bifurcation_data_old,
// 			      particle_interaction_cost_old,particle_interaction_cost_new,
// 			      create_connection_prob,remove_connection_prob
// 			    );
// 			    printf("%f %f %f\n",
// 			      	new_edges.pointA->point->compute_cost3(temp),
// 				new_edges.pointB->point->compute_cost3(temp), 
// 				new_terminal_point->compute_cost3(temp)
// 				   
// 			    );
// 			 }
// 	    #endif
// 	    #endif
	    	
	
	
	  if ((R>=myrand(1)+std::numeric_limits<T>::epsilon())&&((conn_tree.insert(*new_terminal_point,true,true))))
	  {
	    
	    #ifdef  D_USE_GUI
	    #ifdef  DEBUG__BIF_NORMAL
			 if ( gps->is_selected )
			 {
			    printf("BIF DEATH: EC (%f => %f)? PC (%f BD (%f) IC(%f => %f) R (%f/%f)\n "
			    ,old_edgecost,new_edgecost,particle_costs,bifurcation_data_old,
			      particle_interaction_cost_old,particle_interaction_cost_new,
			      create_connection_prob,remove_connection_prob
			    );
			    printf("%f %f %f\n",
			      	new_edges.pointA->point->compute_cost3(temp),
				new_edges.pointB->point->compute_cost3(temp), 
				new_terminal_point->compute_cost3(temp)
				   
			    );
			 }
	    #endif
	    #endif
	    
	    
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
		
		#ifdef  D_USE_GUI
		  for (int i=0;i<3;i++)
		  {
		    activity_pts[i]->touch(get_type());
		  }
		
		#endif	

		
		Points< T, Dim > * p_point=&point;
		 particle_pool.delete_obj(p_point);
		 sta_assert_error(p_point==NULL);
		 
		if (!conn_tree.insert(*new_terminal_point,true))
		{
		    new_terminal_point->print();
		    throw mhs::STAError("error insering point\n");
		    return false;
		}
		sta_assert_debug0(
		  (new_terminal_point->endpoint_connections[0]==0)||
		(new_terminal_point->endpoint_connections[1]==0));
		
		  #ifdef BIFURCATION_DEBUG_EDGECHECK
			p_new_edges->e_cost(
				    edgecost_fun,
			      options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      inrange,true,false,bifurcation_edgecheck_update);
			    
			    /// of course the old edge should be in range (valid)
			    sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
		
		      #endif
		
			    
			    
			    
	    return true;
	  }

	  if (options.bifurcation_particle_soft)	
	  {
	      sta_assert_error(tree.insert(b_particle));
	  }

	connections.remove_connection(p_new_edges);
	for (int i=0;i<3;i++)
	    connections.add_connection(backup_old_edges[i]);
	

	return false;
	
	
	
	
	
	
	
	
	
	} catch(mhs::STAError & error)
        {
            throw error;
        }  
	
	
	
    }
};










#endif