#ifndef MHS_OBSERVER_H
#define MHS_OBSERVER_H






#ifndef OLD_VIEWER
  #include "mhs_gui_viewer.h"
#else

  #include "mhs_gui.h"
  #include "ext/ArcBall.cpp"
  #include "mhs_graphics.h"
  #include "proposals.h"
#include  <iomanip>

  template<typename T, int Dim>
  class CCamPath
  {
    public:
      
      CCamPath()
      {
	
      }
      
      ~CCamPath()
      {
	
      }      
    
  };
  
  template<typename TData,typename T, int Dim>
  class CTrackerRenderer: public CSceneRenderer, CVrender<TData,T>
  {
  private:
    
    
    enum class RENDERER_MODE : unsigned int {
      RENDERER_MODE_DEFAULT=0,
      RENDERER_MODE_CONNECTED=2,
      RENDERER_MODE_LINE=4,
      RENDERER_MODE_WORKER=8,
      RENDERER_MODE_ONLINE=16,
    };
    
    CGLtext<T>  * text_HUD;
    CGprogram   _gcMyPixelProgram;
    CGprogram   _gcMyVertexProgram;
    CGparameter _modelview;
    CGparameter _normalmat;
//     CGparameter _worldview;
    CGparameter _projection;
    CGparameter _focuspointV;
    CGparameter _vertex_mode;
    CGparameter _clipping_plane;
    
    
    const TData * 	img;
//     CGparameter _focuspointP;
    
    Vector<float,4> shader_options;
    int GC_PROGRAM_ID=1;
//     float rescale=1;

    
    Vector<float,3> background_color;
    Vector<float,3> font_color;
    
    std::list<std::string> string_info_stack;
    int string_info_stack_mmem=3;
    void add_info(std::string s)
    {
      string_info_stack.push_front(s);
      if (string_info_stack.size()>string_info_stack_mmem)
	string_info_stack.pop_back();
    }
 
    std::vector<CTracker<TData,T,Dim>* >  tracker_p;
  
    
    GLuint sliceview_textureID;
    static const int sliceview_extents=64;
    float sliceview_slice[sliceview_extents*sliceview_extents];
     
    
   class CVertex
   {
    public:
    Vector<float,3> pos;
    Vector<float,3> color;
     
   };
     
     class C_debug_pt
     {
      public:
	Points<T,Dim>  * debug_point;
	int debug_life;
	int debug_point_tracker_id;
	
	void clear()
	{
	  if ( debug_point!=NULL)
	  {
	    debug_point->clear_selection();
	  }
	  debug_point=NULL;
	}
	
	void assign(Points<T,Dim>  * p, int tracker_id)
	{
	  clear();
	  debug_point =p;
	  p->is_selected=true;
	  debug_point_tracker_id=tracker_id;
	  debug_life=p->get_life();
	  sta_assert_error(tracker_id>-1);
// 	  sta_assert_error(tracker_id<tracker_p.size());
	}
	
	C_debug_pt()
	{
	  debug_point=NULL;
	  debug_life=-1;
	  debug_point_tracker_id=-1;
	}
	
     };
     
     C_debug_pt debug_point;
     
//      static const int num_debug_points=3;
//      C_debug_pt  debug_point_list[num_debug_points];
     
     
    Vector<T,3> debug_poit_world_offset;
    
    mhs::CtimeStopper timer;
    std::size_t sync_tick;
    double last_tick;
    
    
        
    public:
      
    void save_state_in_struct(mxArray * tracker_state)
    {
      if (tracker_state!=NULL)
      {
	try {
	    float * data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,16,"observer_trafo",0);
	    for (int i=0;i<16;i++)
	    {
	    data_p[i]=m_transform.v[i]; 
	    }
	    data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,1,"observer_zoom",0);
	    *data_p=zoom;
	    
	    data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,1,"observer_clipdist",0);
	    *data_p=clip_plane_dist;
	    
	    data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,1,"observer_rendermode",0);
	    *data_p=render_mode;
	    
	    data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,1,"observer_show_tracking",0);
	    *data_p= mouse_state.do_liveview;
	   
	    data_p=mhs::mex_getFieldPtrCreate<float>(tracker_state,1,"observer_show_volume",0);
	    *data_p= mouse_state.do_rendering;
    
	    
	}catch (mhs::STAError error)
	{
	  printf("could'nt save oberser state :%s\n",error.str().c_str());
	} 
      }
    }
    
    
    void load_state_from_struct(const mxArray * tracker_state)
    {
      if (tracker_state!=NULL)
      {
	try {
	  float * data_p =mhs::mex_getFieldPtr<float>(tracker_state,"observer_trafo");
	  for (int i=0;i<16;i++)
	  {
	  m_transform.v[i]=data_p[i]; 
	  }
	  data_p =mhs::mex_getFieldPtr<float>(tracker_state,"observer_zoom");
	  zoom=*data_p;
	  
	  data_p =mhs::mex_getFieldPtr<float>(tracker_state,"observer_clipdist");
	  clip_plane_dist=*data_p;
	  
	  data_p =mhs::mex_getFieldPtr<float>(tracker_state,"observer_rendermode");
	  render_mode=*data_p;
	  
	  data_p=mhs::mex_getFieldPtr<float>(tracker_state,"observer_show_tracking");
	  mouse_state.do_liveview=*data_p;
	   
	  data_p=mhs::mex_getFieldPtr<float>(tracker_state,"observer_show_volume");
	  mouse_state.do_rendering=*data_p;
	  
	  update_projection_mat();
	  }catch (mhs::STAError error)
	  {
	    printf("could'nt find oberser state :%s\n",error.str().c_str());
	  } 
      }
    }
      
//       void do_render()
//       {
// 	for (int i=0;i<tracker_p.size();i++)
// 	{
// 	 sta_assert_error(tracker_p[i]);
// 	 render(*tracker_p[i]);
// 	}
//       }
      
   
      
       
    //void render( CTracker<TData,T,Dim> & tracker)
    void do_render()
    {
      
      bool data_kernel_Debug=false;
      if (call_from_main>0)
      {
      switch (call_from_main)
      {
	case 1:
	{
	      mexCallMATLAB(0, NULL,0,NULL, "start_gui");
		update_tracker_gui();
	}break;
	case 10:
	{
	   data_kernel_Debug=true;   
	}break;
	
      }
      call_from_main=0;
      }
   
      float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
      
//       if ((mouse_state.transition_on))
//       {
// 			    if (focus_point.norm1()<rescale)
// 			    {
// 			    m_transform=m_transform_current*m_transform;
// 			    m_transform_current.Identity();
// 			    update_focus_point(focus_point);
// // 			    mouse_state.transition_on=false;
// 			    mouse_state.reset();
// 			    }else
// 			    {
// 			      focus_point=m_transition.multv3(focus_point);
// 			      m_transform_current=m_transition*m_transform_current;
// 			    }
// 	}
	    
     
      if (SDL_GL_MakeCurrent(tracker_data_p->tracker_window,tracker_data_p->glcontext)!=0)
      {
	printf("error setting opengl context to current window\n");
	return;
      }
      
      
      
      double total_time=timer.get_total_runtime();
      //timer.get_last_call_runtime();
      if (total_time-last_tick>0.001)
      {
	last_tick=total_time;
	sync_tick++;
	
      }
      
//       Vector<float,3> blinking_color(0.9f,0.8f,0.2f);
//       {
// 	float v=(sync_tick%50)/100.0f;
// 	if (v>0.25)
// 	  blinking_color*=1.25f-v;
// 	else 
// 	  blinking_color*=0.75f+v;
//       }
      //hsv2rgb<float>(float(sync_tick*10 % 360),0.85f,1.0f,blinking_color[0],blinking_color[1],blinking_color[2]);
      
//       blinking_color.print();
//       printf("%f\n",total_time);
//       if (n_points<1)
// 	return;

     
      
	
      float new_center[3];
      new_center[0]=rescale*shape[0]/2.0f;
      new_center[1]=rescale*shape[1]/2.0f;
      new_center[2]=rescale*shape[2]/2.0f;
      
      
      
	 
      GLfloat light_position[] = { 0, 0, -2.0, 0.0 };
      GLfloat light_dir[4] = { 0,0,1, 1 };
	
      GLfloat light_ambient[] = { 0.1, 0.1, 0.1, 1.0 };
      GLfloat light_diffuse[] = { 0.9, 0.9, 0.9, 1.0 };
      GLfloat light_specular[] = { 0.5, 0.5, 0.5, 1.0 };
      GLfloat global_ambient[]={0.0,0.0,0.0,1.0};

      
      GLfloat whiteSpecularMaterial[] = {1.0,1.0, 1.0};
      GLfloat mShininess[] = {23}; 	
      GLfloat materialEmission[] = {0.0f, 0.0f, 0.0f, 1.0f};


      
   
      
      
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
//      float zNear=1;
//     float zFar=200;
//    
//    glFrustum(-zoom, zoom,
//              - zoom, zoom,
//              zNear, zFar);
//     
//     float debug_mat[16];    
//     glGetFloatv(GL_PROJECTION_MATRIX,debug_mat);
//     printf("1:---------------\n");
//     printf("%f %f %f %f\n",debug_mat[0],debug_mat[1],debug_mat[2],debug_mat[3]);
//     printf("%f %f %f %f\n",debug_mat[4],debug_mat[5],debug_mat[6],debug_mat[7]);
//     printf("%f %f %f %f\n",debug_mat[8],debug_mat[9],debug_mat[10],debug_mat[11]);
//     printf("%f %f %f %f\n",debug_mat[12],debug_mat[13],debug_mat[14],debug_mat[15]);
//     printf("-----------------\n"); 
//    
//  glLoadIdentity();	
//   gluLookAt(viewer_pos[0],viewer_pos[1],viewer_pos[2],viewer_target[0],viewer_target[1],viewer_target[2],viewer_up[0],viewer_up[1],viewer_up[2]);
// 
// 
//              
//     glGetFloatv(GL_PROJECTION_MATRIX,debug_mat);
//     printf("2:---------------\n");
//     printf("%f %f %f %f\n",debug_mat[0],debug_mat[1],debug_mat[2],debug_mat[3]);
//     printf("%f %f %f %f\n",debug_mat[4],debug_mat[5],debug_mat[6],debug_mat[7]);
//     printf("%f %f %f %f\n",debug_mat[8],debug_mat[9],debug_mat[10],debug_mat[11]);
//     printf("%f %f %f %f\n",debug_mat[12],debug_mat[13],debug_mat[14],debug_mat[15]);
//     printf("-----------------\n"); 
//     glLoadIdentity();
//     
// 
//     Matrix<float,4>::OpenGL_glFrustum(-zoom, zoom,
// 		  - zoom, zoom,
// 		  zNear, zFar).print();      
// 	Matrix<float,4>::OpenGL_glLooktAt(viewer_pos,viewer_target,viewer_up).print();

    
    glMultMatrixf((GLfloat *)projection_mat.v);        
    

  

//     clip_plane[3]=1.0;
    
 
    
    
    cgGLSetStateMatrixParameter( _projection,  CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
    
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light_dir);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);	
    glEnable(GL_COLOR_MATERIAL);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, whiteSpecularMaterial);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, materialEmission);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, light_diffuse);

    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,global_ambient);

    glLightf(GL_LIGHT0,GL_SPOT_EXPONENT,64);
    glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,70);
    
    glEnable(GL_NORMALIZE);
    
//     glClearColor(1,1,1,1);
    glClearColor(background_color[0],background_color[1],background_color[2],1);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_LIGHTING);
    
//     glEnable(GL_ALPHA_TEST);
//     glAlphaFunc(GL_GREATER,0.01);
//     glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_BLEND);
    
    
    glDisable(GL_CLIP_PLANE0); 
    glDisable(GL_CLIP_PLANE1); 
    glDisable(GL_CLIP_PLANE2); 
    glDisable(GL_CLIP_PLANE3); 
    glDisable(GL_CLIP_PLANE4); 
    glDisable(GL_CLIP_PLANE5); 
    
    glMatrixMode( GL_MODELVIEW );
	
	
// 	glPushMatrix();
    {
      glLoadIdentity();  

      glTranslatef(focus_point[0],focus_point[1],focus_point[2]);
      
       glMultMatrixf((GLfloat *) ((m_transform_current*m_transform).OpenGL_get_rot().v));
      Vector<T,3>axis_pos(0,0,0);
// 	Vector<T,3>axis_color(0,0,0);
      Vector<T,3>axis_color(1,0.5,1);
	glLineWidth(2);
// 	   text_HUD.XYZAxis(0.05,axis_pos,axis_color);
	
	text_HUD->XYZAxis(0.05,axis_pos,axis_color);
    }
	
// 	glTranslatef(0,0,1);
// 	glEnable(GL_CLIP_PLANE0); 
// 	Vector<double,4> clip_plane;
// 	clip_plane=0.0;
// 	clip_plane[2]=0.1;   
// 	clip_plane[3]=1.0;   
// 	glClipPlane(GL_CLIP_PLANE0,clip_plane.v);
	
	   
	
// 	glPopMatrix();
// 	glPushMatrix();
	glLoadIdentity();  
	
	
	
	shader_options[0]=0;
	if (mouse_state.is_busy())//((mouse_state.trackballZ_on)||(mouse_state.trackball_on)||(mouse_state.arcball_on))
	{
	 shader_options[0]=1;
	}
	
	
	
 	shader_options[1]=1/rescale;
		
// 	shader_options[1]=shape[0];
// 	shader_options[2]=shape[1];
// 	shader_options[3]=shape[2];
// 	 cgGLSetParameter3fv(_focuspointP,focus_point.v);  
// 	if (PrintlastCGerror())
// 		  printf("error before setting GCG state\n");
	 cgGLSetParameter3fv(_focuspointV,focus_point.v);  
	 cgGLSetParameter2fv(_vertex_mode,shader_options.v); 
// 	 Vector<float,3> clip_plane_f;
// 	 clip_plane_f[0]=clip_plane[0];
// 	 clip_plane_f[1]=clip_plane[1];
// 	 clip_plane_f[2]=clip_plane[2];
	 Vector<float,3> clip_plane;
// 	 clip_plane[0]=-rescale*clip_plane_dist;
// 	 clip_plane[0]=rescale*clip_plane_dist;
	 clip_plane[0]=clip_plane_dist;
	 
// 	 printf("clip_dist %f\n",clip_plane[0]);
// 	 mhs::mex_dumpStringNOW();
	 
	 cgGLSetParameter1f(_clipping_plane,clip_plane[0]); 
	 
	 
// 	 printf("clipplane dist %f\n",clip_plane[0]);
// 				mhs::mex_dumpStringNOW();
	
// 	 if (PrintlastCGerror())
// 		  printf("error in setting GCG state\n");

	glMultMatrixf((GLfloat *) (m_transform_current*m_transform).v);
	
	
	
	
	
	
	
	
// 	cgGLSetStateMatrixParameter(  _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE);
 	
	
	glTranslatef(-new_center[0],-new_center[1],-new_center[2]);
	    
	
	if (tracker_data_p->_cgprogram)
	{
	  tracker_data_p->_cgprogram->enableProfile(CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER);
  	  tracker_data_p->_cgprogram->enableProfile(CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER);
	  cgGLBindProgram(_gcMyVertexProgram);
	  cgGLBindProgram(_gcMyPixelProgram);	  
	}

	

    
 	this->enable_VBO();
        mhs_check_gl;
	
	
	Vector<T,Dim> rot_ax;
	rot_ax=T(0);
	
// 	bool no_single=(render_mode==RENDERER_MODE::RENDERER_MODE_CONNECTED)
// 			||(render_mode==RENDERER_MODE::RENDERER_MODE_LINE);
// 	bool no_single=(render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_CONNECTED))
// 			|(render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_LINE));
	
	bool no_single=(render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_CONNECTED));			
	
			
			
	for (int i=0;i<tracker_p.size();i++)
	{
	  sta_assert_error(tracker_p[i]!=NULL);
	  CTracker<TData,T,Dim> & tracker=*tracker_p[i];
	  
	  if ((render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_ONLINE))&&(tracker.is_offline()))
	    continue;
	  
	  
	  
	  Vector<float,3> worker_color;
	  Vector<float,3> worker_color_select;
	  Vector<float,3> worker_color_bifurcation;
	  bool is_offline=tracker.is_offline();
	  float offline_fact_h=(1-is_offline);
	  float offline_fact_v=0.5*is_offline+(1-is_offline);
          hsv2rgb<float>(float(i*360.0f)/tracker_p.size(),0.75f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2]);
	  //hsv2rgb<float>(float(((i*5+2)%(5*tracker_p.size()))*360.0f)/(5*tracker_p.size()),1.00f,1.0f,worker_color_bifurcation[0],worker_color_bifurcation[1],worker_color_bifurcation[2]);
 	  hsv2rgb<float>(int(float(((i*360))/tracker_p.size()+10))%360,1.00f*offline_fact_h,0.5f*offline_fact_v,worker_color_bifurcation[0],worker_color_bifurcation[1],worker_color_bifurcation[2]);	  
// 	  hsv2rgb<float>(float(i*360.0f)/tracker_p.size(),1.00f,1.0f,worker_color_bifurcation[0],worker_color_bifurcation[1],worker_color_bifurcation[2]);
	  
	  hsv2rgb<float>(int(float(((i*360))/tracker_p.size()+180))%360,0.50f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2]);	  
	  //hsv2rgb<float>(int(float(((i*360))/tracker_p.size()+390))%360,0.50f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2]);	  
	  //hsv2rgb<float>(int((((360)/(4.0f*tracker_p.size()))+180))%360,0.75f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2]);
	  //hsv2rgb<float>(float(((i*2+tracker_p.size()+1)%(2*tracker_p.size()))*360.0f)/(2*tracker_p.size()),0.75f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2]);
	  //hsv2rgb<float>(180.0f+float(i*360.0f)/tracker_p.size(),0.75f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2]);
	  //hsv2rgb<float>(float(90.0f+i*360.0f)/tracker_p.size(),0.85f,1.0f,worker_color_select[0],worker_color_select[1],worker_color_select[2]);
	  
	  
	  
	      
	  
	      
	      Vector<T,3> world_offset=tracker.get_world_offset();
	    
	    const class Points<T,Dim> ** particle_ptr=tracker.particle_pool->getMem();
	    std::size_t n_points=tracker.particle_pool->get_numpts();
	    
    // 	if (false)
	    if (mouse_state.do_liveview)
	    for (unsigned int i=0; i<n_points; i++)
	    {
		Points<T,Dim> * point_p=((Points<T,Dim>*)particle_ptr[i]);
		if (point_p==NULL)
		  continue;
		  
		if (!tracker.particle_pool->is_valid_adress(point_p))
		{
		  SDL_Log("\ninvalid addr\n");
		  continue;
		}
		
		Points<T,Dim>  point=(*point_p);
		
		if ((no_single)&&(!point.isconnected()))
		  continue;

		
		float tx=(point.position[0]+world_offset[0])*rescale;
		float ty=(point.position[1]+world_offset[1])*rescale;
		float tz=(point.position[2]+world_offset[2])*rescale;
		if ((tx<0)||(tx>1))
		  continue;
		if ((ty<0)||(ty>1))
		  continue;
		if ((tz<0)||(tz>1))
		  continue;
		
		
		float scale=point.scale*rescale;
		float thickness=point.get_thickness()*rescale;
		if (!(scale>0)||!(scale<50))
		  continue;
		if (!(thickness>0)||!(thickness<50))
		  continue;
		
	
		
		
		glPushMatrix();
		glTranslatef(tx,ty,tz);
		int particle_typ=0;
		int color_id=0;
		switch (point.tracker_birth)
		{
		    case 0: 
		      color_id=CSceneRenderer::Cred;
		    break;
		    case 1: 
		      color_id=CSceneRenderer::Cgreen;
		    break;
		    case 2: 
		      color_id=CSceneRenderer::Cblue;
		    break;
		    case 3: 
		      color_id=CSceneRenderer::Cyellow;
		    break;
		}
		
		switch (point.particle_type)
		{
		  case PARTICLE_TYPES::PARTICLE_SEGMENT:	  
		  {
		      Vector<T,Dim> & n=point.direction;
		      if (!(n.norm1()>0))
		      {
			  glPopMatrix();
			  continue;
		      }
		      sta_assert_error(n.norm2()>0);
		      rot_ax[0]=n[1];
		      rot_ax[1]=-n[0];
		      rot_ax.normalize();
		      T angle=-180*std::acos(n[2])/M_PI;
		      glRotatef(angle,rot_ax[0],rot_ax[1],rot_ax[2]);
		      particle_typ=1;
		      
		  }
		  break;
		  case PARTICLE_TYPES::PARTICLE_BIFURCATION:	  
		  {
		      Vector<T,Dim> & n=point.direction;
		      if (!(n.norm1()>0))
		      {
			  glPopMatrix();
			  continue;
		      }
		      sta_assert_error(n.norm2()>0);
		      rot_ax[0]=n[1];
		      rot_ax[1]=-n[0];
		      rot_ax.normalize();
		      T angle=-180*std::acos(n[2])/M_PI;
		      glRotatef(angle,rot_ax[0],rot_ax[1],rot_ax[2]);
		      particle_typ=1;
		      
		  }
		  break;
		  case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER:	  
		  {
		    color_id=CSceneRenderer::Cmag;
		  }
		  break;
		}
		
		if (render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_LINE))
		{
		  if (point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		  {
		    color_id=CSceneRenderer::Cyellow;
		  
		  scale*=0.4;
		  thickness=scale;
		  
		  //thickness*=1.25;
		  //scale=thickness;
		  
		  particle_typ=0;
		  }
		}
		
// 		bool in_debug_pt_list=false;
// 		for (int l=0;l<num_debug_points;l++)
// 		{
// 		  if (point_p==debug_point_list[i].debug_point)
// 		    in_debug_pt_list=true;
// 		}
		
		//if (debug_point.debug_point==point_p)
// 		if (point_p->is_selected)
// 		{
// 		  {
// 		  color_id=CSceneRenderer::CWhite;
// 		  glColor3fv(particle_colors[color_id]);     
// 		  }
// 		} else 
// 		if (point_p->is_focus_point)
		//if (debug_point.debug_point==point_p)
		if (point_p->is_selected)
		{
		  if (render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER))
		  {
		     glColor3fv(worker_color_select.v);    
		  }else
		  {
		  color_id=CSceneRenderer::Corange;
		  glColor3fv(particle_colors[color_id]);     
		  }
// 		  glColor3fv(blinking_color.v);   
		}else
		{
		 if (render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER))
		 {
		   if (point.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		    {
		      glColor3fv(worker_color.v);    
		    }else
		    {
		      glColor3fv(worker_color_bifurcation.v);    
		    }
		 }else
		 {
		   if (is_offline)
		     color_id=CSceneRenderer::Cdarkgray;
		  glColor3fv(particle_colors[color_id]);    
		 }
		}
		
			
		
		
		glScalef(scale,scale,thickness);
		
    // 	      cgGLSetStateMatrixParameter( _worldview,  CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
    // 		cgGLSetStateMatrixParameter(  _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_TRANSPOSE);
		  
		  cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
		  cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);

		    std::size_t offset=buffered_objects[particle_typ].i_offset;
		    glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
		glPopMatrix();
		mhs_check_gl;
	    }
	    
	    
    // 	

	      
	    
	    

	    
	    std::size_t numconnections=tracker.connections.pool->get_numpts();
	    
	    int particle_typ=2;
	    int color_id=CSceneRenderer::Cyellow;		
	    Vector<T,Dim> positionn;
	    Vector<T,Dim> n;
	    
		  if (render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER))
		 {
		   hsv2rgb<float>(float(i*360.0f)/tracker_p.size(),0.25f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2]);
		   glColor3fv(worker_color.v);    
		   
		 }else
		 {
		    if (is_offline)
		     color_id=CSceneRenderer::Cdarkgray;
		   glColor3fv(particle_colors[color_id]);  
		 }
		 
	    
	    const Connection<T,Dim> ** conptr=tracker.connections.pool->getMem();
	    if (mouse_state.do_liveview)
	    for (std::size_t i=0; i<numconnections; i++)
	    { 
		const Connection<T,Dim> * con=conptr[i];
		
		if (con==NULL)
		  continue;
		if (!tracker.connections.pool->is_valid_adress(con))
		{
		  SDL_Log("\ninvalid addr\n");
		  continue;
		}
		class Points< T, Dim >::CEndpoint * epointA=con->pointA;
		class Points< T, Dim >::CEndpoint * epointB=con->pointB;
		if ((epointA==NULL)||(epointB==NULL))
		  continue;
		
		Points<T,Dim> * pointA=epointA->point;
		Points<T,Dim> * pointB=epointB->point;
		
		if ((pointA==NULL)||(pointB==NULL))
		  continue;
		
		
		Vector<T,Dim>  pA=pointA->position+world_offset;
		Vector<T,Dim>  pB=pointB->position+world_offset;
	      
		
		
		n=pA-pB;
		T length=std::sqrt(n.norm2());
		
		if (length<0.0001)
		  continue;
		
		n/=length+std::numeric_limits<T>::epsilon();
		
		length*=0.5*rescale;
		
		// probably conected with a "not initialized point"
		if (length>0.25)
		  continue;
		
		float s=std::min(pointA->scale,pointB->scale);
		if ((s<0.0001)||(s>100))
		  continue;
		//s=0.55*std::max(1.0f,s);
		if (render_mode!=static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_LINE))
		{
		  s*=0.3f;
 		  s=std::max(std::min(0.5f,s),s);
// 		  s*=0.3;
		}else{
		  s*=0.3;
		}
		

		glPushMatrix(); 
		positionn=(pA+pB)/2;
		glTranslatef(positionn[0]*rescale,positionn[1]*rescale,positionn[2]*rescale);
		
		rot_ax[0]=n[1];
		rot_ax[1]=-n[0];
		
		sta_assert_error(n.norm2()>0);
		if (!(n.norm1()>0))
		      {
			  glPopMatrix();
			  continue;
		      }
		rot_ax.normalize();
		
		T angle=-180*std::acos(n[2])/M_PI;
		glRotatef(angle,rot_ax[0],rot_ax[1],rot_ax[2]);
		particle_typ=2;
	      
		if (length<0.00001)
		{
		  //printf("ha? skipping edges: lenght %f\n",length);
		   glPopMatrix();
		  continue;
		}
		if (!(length>0))
		{
		   printf("ha? skipping edges: neg (%d) or zero (%d) lenght: %f\n",length<0,!(length<0),length);
		   glPopMatrix();
		  continue;
		}
		glScalef(s*rescale,s*rescale,length);
		  cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
		  cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);		  
		std::size_t offset=buffered_objects[particle_typ].i_offset;
		glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));  
		glPopMatrix();
	    }
	}
	
	
	
	
	std::list<std::string >  messages;	
	
	
	//render_info();
	
	
	if (tracker_data_p->_cgprogram)
	{
  	  tracker_data_p->_cgprogram->disableProfile(CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER);
	  tracker_data_p->_cgprogram->disableProfile(CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER);
	}
	
	
	
	
	
	
	
	if ((mouse_state.is_busy())&&(!(mouse_state.point_select_on)))
	{
	  for (int z=0;z<2;z++)
	    for (int y=0;y<2;y++)
	      for (int x=0;x<2;x++)
// 	  for (int z=0;z<1;z++)
// 	    for (int y=0;y<1;y++)
// 	      for (int x=0;x<1;x++)
	  {
// 	    float scale=4*rescale;
	    float scale=0.01;
	    glPushMatrix();
	      glTranslatef(z*shape[0]*rescale,y*shape[1]*rescale,x*shape[2]*rescale);
	      int particle_typ=0;
	      //int color_id=CSceneRenderer::Clightgray;
	      //int color_id=CSceneRenderer::Cred;
	      int color_id=CSceneRenderer::Corange;
	      glColor3fv(particle_colors[color_id]);   
	      glScalef(scale,scale,scale);
		  std::size_t offset=buffered_objects[particle_typ].i_offset;
	      cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
	      cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);		  
		  glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
	      glPopMatrix();
	  }
	  
	  //if (!(mouse_state.point_select_on))
	  {
	  float left=0;
	  float right=shape[0]*rescale;
	  float top=0;
	  float bottom=shape[1]*rescale;
	  float front=0;
	  float back=shape[2]*rescale;
	  	glLineWidth(1);
		
// 	  glLineStipple(1, (short) 0x00FF); 
// 		glBegin(GL_LINES);
// 			      glColor3fv(particle_colors[CSceneRenderer::Corange]); 
// 			      glVertex3f(front,focus_point[1],focus_point[2]);
// 			      glVertex3f(back,focus_point[1],focus_point[2]);
// 			      glVertex3f(focus_point[0],top,focus_point[2]);
// 			      glVertex3f(focus_point[0],bottom,focus_point[2]);
// 			      glVertex3f(focus_point[0],focus_point[1],right);
// 			      glVertex3f(focus_point[0],focus_point[1],left);			      			      
// 		      glEnd();
// 	  glDisable(GL_LINE_STIPPLE);	
		
	  glDisable(GL_CULL_FACE);
	  glLineStipple(1, (short) 0x00FF); 
		glEnable(GL_LINE_STIPPLE);
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		//glColor3f(0.0f,0.0f,0.7f);
		glColor3fv(particle_colors[CSceneRenderer::Corange]);  
		glBegin(GL_QUADS);
		glVertex3f(left,top,back);
		glVertex3f(left,top,front);
		glVertex3f(left,bottom,front);
		glVertex3f(left,bottom,back);
		glVertex3f(right,top,back);
		glVertex3f(right,top,front);
		glVertex3f(right,bottom,front);
		glVertex3f(right,bottom,back);
		glVertex3f(left,bottom,back);
		glVertex3f(right,bottom,back);
		glVertex3f(right,top,back);
		glVertex3f(left,top,back);
		glVertex3f(left,bottom,front);
		glVertex3f(right,bottom,front);
		glVertex3f(right,top,front);
		glVertex3f(left,top,front);
		glVertex3f(left,top,front);
		glVertex3f(left,top,back);
		glVertex3f(right,top,back);
		glVertex3f(right,top,front);
		glVertex3f(left,bottom,front);
		glVertex3f(left,bottom,back);
		glVertex3f(right,bottom,back);
		glVertex3f(right,bottom,front);
		glEnd();
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		glDisable(GL_LINE_STIPPLE);
		glEnable(GL_CULL_FACE);
	  }
	  
	}
	
	
	/*******************************
	 *
	 * center point
	 * 
	 * ****************************/
	if (false)
	{
	   glPushMatrix();
	   glLoadIdentity();
	   Vector<float,3>  center;
	    center[0]=0.5*shape[0]*rescale-new_center[0];
	    center[1]=0.5*shape[1]*rescale-new_center[1];
	    center[2]=0.5*shape[2]*rescale-new_center[2];
	    
  	    Vector<float,3>   translation=(m_transform_current*m_transform).multv3(center);
	    glTranslatef(translation[0],translation[1],translation[2]);
	    
	    float scale=4*rescale;
	  int particle_typ=3;
	  int color_id=CSceneRenderer::Corange;
	  glColor3fv(particle_colors[color_id]);   
	  glScalef(scale,scale,scale);
	      std::size_t offset=buffered_objects[particle_typ].i_offset;
	      cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
	      cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);		  
	      glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
	  glPopMatrix();
	}

	
	{
	 glPushMatrix();
	   glLoadIdentity();

	   glTranslatef(focus_point[0],focus_point[1],focus_point[2]);
  
	   float scale=0.02*zoom;
	  int particle_typ=0;
	  int color_id=CSceneRenderer::Corange;
	  glColor3fv(particle_colors[color_id]);   
	  glScalef(scale,scale,scale);
	      std::size_t offset=buffered_objects[particle_typ].i_offset;
// 	      cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
// 	      cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);		  
	      glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
	glPopMatrix();
	}
	
	/*******************************
	 *
	 * focus point
	 * 
	 * ****************************/
	
	
	
	if (false)
	{
	float scale=4*rescale;
	

	    glMatrixMode(GL_MODELVIEW);
	    glPushMatrix();
	      
// 	    glTranslatef(new_center[0],new_center[1],new_center[2]);
// 	    Matrix<float,4> rot=(m_transform_current*m_transform);
// 	    
//  	    Matrix<float,4> rotInv=rot.OpenGL_invert();
// 	    glMultMatrixf((GLfloat *)rotInv.OpenGL_get_rot().v);	    
// 	    Vector<float,3>  translation=rot.OpenGL_get_tranV();
// 	    glTranslatef(-translation[0],-translation[1],-translation[2]);
	    
	     glTranslatef(focus_point[0],focus_point[1],focus_point[2]);
	    
	    
// 	    cgGLSetParameter3fv(_focuspoint,_black.data());  
// 	     Vector<float,3> focuspt=translation*(-1);
	    //cgGLSetParameter3fv(_focuspoint,_black.data());  	    
 
	  int particle_typ=3;
	  int color_id=CSceneRenderer::Corange;
	  glColor3fv(particle_colors[color_id]);   
	  glScalef(scale,scale,scale);
	      std::size_t offset=buffered_objects[particle_typ].i_offset;
	      cgGLSetStateMatrixParameter( _modelview,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
	      cgGLSetStateMatrixParameter(  _normalmat,  CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);		  
	      glDrawElements(GL_TRIANGLES, buffered_objects[particle_typ].i_length, GL_UNSIGNED_SHORT,  (void*)(offset * sizeof(ushort)));
	  glPopMatrix();
	}
	
	
	
	
     	 this->disable_VBO();
	mhs_check_gl;
//   glPopMatrix(); 
		  
    glDisable(GL_BLEND);
		 
// if (false)	
if ((do_vrendering)&&(mouse_state.do_rendering))
{

        Vector<float,3>  center;
	center[0]=0.5*shape[0]*rescale-new_center[0];
	center[1]=0.5*shape[1]*rescale-new_center[1];
	center[2]=0.5*shape[2]*rescale-new_center[2];
	
        Matrix<float,4> trafo=(m_transform_current*m_transform); 
        Vector<float,3> translation=trafo.multv3(center);
	Matrix<float,4> rotation=trafo.OpenGL_get_rot();
	this->paint(rotation,translation,shader_options,focus_point,clip_plane);		  
}	
// 	text_HUD.


	

	mhs_check_gl

	text_HUD->InitHUD();
	

	
	
	
	int top_row=0;
			glPushMatrix();
				glLoadIdentity();
 				
			
			
				
			//glColor3f(0,0,0);
				
// 			text_HUD->Stringoutxyz(s.str(),Vector<T,3>(-0.95,0.95,0));
// 			text_HUD->Stringoutxyz(s.str(),Vector<T,3>(0.05,0.95,0));
			
			Vector<float,3> worker_color;
			
			glLineWidth(1);
			
			 if (true)
			{
			  for (int i=0;i<tracker_p.size();i++)
			  {
				sta_assert_error(tracker_p[i]);
				CTracker<TData,T,Dim> & tracker=*tracker_p[i];
				//if (render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER))
				

				{
				  bool is_offline=tracker.is_offline();
				  float offline_fact_h=(1-is_offline);
				  float offline_fact_v=0.5*is_offline+(1-is_offline);
				  hsv2rgb<float>(float(i*360.0f)/tracker_p.size(),0.75f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2]);
				  glColor3fv(worker_color.v);
				}
// 				float t=0.8;
// 				float offset=0.01;
// 				float t=0.0;
// 				float h=0.1;
// 				float l=0.7-offset;
// 				float w=0.3;
				
				
				
				float offset=text_HUD->p2gl_h(5);
				float t=0;
				float h=text_HUD->p2gl_w(80);
				float w=text_HUD->p2gl_w(300);
				float l=1-w-offset;
				
				
				
				float level=0;//tracker.state_energy_grad_mean;
// 				tracker.energy_gradient_stat->print_hist(t, l,w, h,0.001);
				tracker.energy_gradient_stat2[0].print_hist(t+offset, l,w, h,0);
				glColor3fv(worker_color.v);
				tracker.energy_gradient_stat2[2].print_hist(t+h+offset*2, l,w, h,0);
				
				if (tracker_p.size()==1)
				{
				  glColor3f(1,1,0);
				  //tracker.energy_gradient_stat2[1].print_hist(t+h+offset*2, l,w, h,0);
				  tracker.energy_gradient_stat2[1].print_hist(t+offset, l,w, h,0);
				  glColor3f(0,1,1);
 				  tracker.edge2particle_stat.print_hist(t+h+offset*2, l,w, h,1);
				  //tracker.edge2particle_stat.print_hist(t+h, l,w, h,0);
				}
				
				/*
				for (int proposal_id=0;proposal_id<tracker.proposals.size();proposal_id++)
				{
				 
				  if (tracker.state_energy_grad_proposal_type[proposal_id]==static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_BIRTH))
				  {
					glColor3f(1,1,0);
					tracker.energy_gradient_stat_per_proposal[proposal_id].print_hist(t, l,w, h,0);
				  }
				  if (tracker.state_energy_grad_proposal_type[proposal_id]==static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_DEATH))
				  {
					glColor3f(1,0,1);
					tracker.energy_gradient_stat_per_proposal[proposal_id].print_hist(t, l,w, h,0);
				  }
				  
				}
				*/
				
// 				
			  }
			  
		  // 	 glBegin(GL_LINES);
		  // 	      
		  // 		glColor3f(1,1,1);
		  // 		glVertex3f(0,0,0);
		  // 		glVertex3f(1,1,0);
		  // 	  glEnd();
			  
			}
			
			
			
			
			
			
			
			
			std::stringstream s;
			s.precision(2);
			

			std::size_t total_n_bif=0;
			std::size_t total_n_points=0;
			std::size_t total_n_connections=0;
			std::size_t max_proposals=0;
			std::size_t max_accepted_proposals=0;
			std::size_t max_accepted_proposals_selected=0;
			std::size_t max_particle_changed_per_seconds=0;
			for (int i=0;i<tracker_p.size();i++)
			{
			 
			      
			      
			      sta_assert_error(tracker_p[i]);
			      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
			      std::size_t n_points=tracker.particle_pool->get_numpts();
			      total_n_points+=n_points;
			      std::size_t n_connections=tracker.connections.pool->get_numpts();
			      total_n_connections+=n_connections;
			      std::size_t n_bif=tracker.connections.num_bifurcation_center_points;
			      total_n_bif+=n_bif;
			      max_proposals=std::max(tracker.proposals_per_seconds,(double)max_proposals);
			      max_accepted_proposals=std::max(tracker.accepted_proposals_per_seconds,(double)max_accepted_proposals);
			      max_accepted_proposals_selected=std::max(tracker.accepted_proposals_per_seconds_selected,(double)max_accepted_proposals_selected);
			      max_particle_changed_per_seconds=std::max(tracker.particle_changed_per_seconds,(double)max_particle_changed_per_seconds);
			}
			int l_p=std::ceil(std::log10(total_n_points));
			int l_e=std::ceil(std::log10(total_n_connections));
			int l_b=std::ceil(std::log10(total_n_bif));
			int l_pt=std::ceil(std::log10(max_proposals));
			int l_pa=std::ceil(std::log10(max_accepted_proposals));
			int l_ps=std::ceil(std::log10(max_accepted_proposals_selected));
			int l_np=std::ceil(std::log10(max_particle_changed_per_seconds));

			glColor4f(font_color[0],font_color[1],font_color[2],1);
			
			for (int i=0;i<tracker_p.size();i++)
			{
			      
			      sta_assert_error(tracker_p[i]);
			      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
			      std::size_t n_points=tracker.particle_pool->get_numpts();
// 			      total_n_points+=n_points;
			      
			      std::size_t n_connections=tracker.connections.pool->get_numpts();
// 			      total_n_connections+=n_connections;
			      std::size_t n_bif=tracker.connections.num_bifurcation_center_points;
			      
			      T temp=tracker.opt_temp;
/*				s<<"["<<n_points<<" / "<<n_connections<<"] ";    */  
				s.precision(2);
				s.str("");
				s<<"p: "<<std::setfill(' ') << std::setw(l_p)<<n_points<<"  ";
				s<<"e: "<<std::setfill(' ') << std::setw(l_e)<<n_connections<<"  ";
				s<<"b: "<<std::setfill(' ') << std::setw(l_b)<<n_bif<<"  ";
				s<<"T: "<< std::fixed<<temp;
				s<<" (worker "<<std::setfill(' ') << std::setw(2)<<i+1<<")";
				if (tracker.accepted_proposals_per_seconds>0)
				{
				  s<<" "<< std::setw(l_pa)<<std::size_t(tracker.accepted_proposals_per_seconds);
				}
				if (tracker.proposals_per_seconds>0)
				{
				  s<<" "<< std::setw(l_pt)<<std::size_t(tracker.proposals_per_seconds);
				}
				if (tracker.accepted_proposals_per_seconds_selected>0)
				{
				   s<<" "<< std::setw(l_ps)<<std::size_t(tracker.accepted_proposals_per_seconds_selected);
				}
				if (tracker.particle_changed_per_seconds>0)
				{
				   s<<" "<< std::setw(l_np)<<std::size_t(tracker.particle_changed_per_seconds);
				}
				
				
				s.precision(3);
				s<<"   "<<tracker.state_energy_grad2[0]<<"   "<<tracker.state_energy_grad2[1];
				
				if (render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER))
				{
				  bool is_offline=tracker.is_offline();
				  float offline_fact_h=(1-is_offline);
				  float offline_fact_v=0.5*is_offline+(1-is_offline);
				  hsv2rgb<float>(float(i*360.0f)/tracker_p.size(),0.75f*offline_fact_h,1.0f*offline_fact_v,worker_color[0],worker_color[1],worker_color[2]);
				  
				  glColor3fv(worker_color.v);
				}
				text_HUD->StringoutLine(s.str(),i+1);	
				
				
// 				 state_energy_grad,
// 				opt_temp,
// 				iteration/1000000,
// 				options.opt_numiterations/1000000,		
// 				  tree->get_numpts(),
// 				  connections.pool->get_numpts(),
// 				  connections.num_terminals,
// 				  connections.num_bifurcation_center_points
// 				  );
				
				
			}
			glColor4f(font_color[0],font_color[1],font_color[2],1);
			//text_HUD->StringoutLine(s.str(),1);	
				s.str("");
				s<<"p: "<<std::setfill(' ') << std::setw(l_p)<<total_n_points<<"  ";
				s<<"e: "<<std::setfill(' ') << std::setw(l_e)<<total_n_connections<<" (total)";
			text_HUD->StringoutLine(s.str(),0);
	
			top_row+=tracker_p.size()+1;
			
			int offset=0;
			for (std::list<std::string>::iterator iter=string_info_stack.begin();iter!=string_info_stack.end();iter++)
			{
			  
			  if (offset==1) glColor4f(0.5*font_color[0],0.5*font_color[1],0.5*font_color[2],1);
			  text_HUD->StringoutLine(*iter,
						  text_HUD->Stringou_getLastLine()-string_info_stack.size()+offset++);
			}




			



{
  
  
//   for (int l=0;l<num_debug_points;l++)
//   {
//     if (debug_point_list[l].debug_point!=NULL)
//     {
//     if (debug_point_list[l].debug_life!=debug_point_list[l].debug_point->get_life())
//     {
//       debug_point_list[l].debug_point->clear_selection();
//       debug_point_list[l].debug_point=NULL;
//     }
//     }
//   }
     
  

   if (debug_point.debug_point!=NULL)
  {
   if (debug_point.debug_life!=debug_point.debug_point->get_life())
   {
//       debug_point.debug_point->clear_selection();
//      debug_point.debug_point=NULL;
     debug_point.clear();
   }else
   {	
      Vector<T,Dim> & n=debug_point.debug_point->direction;
      T scale=debug_point.debug_point->scale;
      if ((n.norm1()>0)&&(scale>0)&&(scale<1000))
      {
	  
      glColor4f(font_color[0],font_color[1],font_color[2],1);
      top_row++;
      
      
      sta_assert_error(tracker_p.size()>=debug_point.debug_point_tracker_id);
      sta_assert_error(tracker_p[debug_point.debug_point_tracker_id]!=NULL);
      try 
      {
	
	
	if (debug_point.debug_point->particle_type!=(PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
	{
 	  T result;
	  std::list<std::string>  debug_data;
	  if (data_kernel_Debug)
	  {
	    debug_data.push_back("");
	  }
	  tracker_p[debug_point.debug_point_tracker_id]->data_fun->eval_data(result,
				debug_point.debug_point->direction,
				  debug_point.debug_point->position,
				  debug_point.debug_point->scale,
				&debug_data);
	  
	  messages.splice(messages.end(), debug_data);
	  
	  
	  
// 	  for (typename std::list<std::string>::iterator iter=debug_data.begin();iter!=debug_data.end();iter++)
// 	  {
// 	    text_HUD->StringoutLine(*iter,top_row++);
// 	  }
 	  s.str("");
 	  s<<"filter response: "<<result;
	  messages.push_back(s.str());
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	}
	
	
	
	
// 	if (false)
	{
	  
	  s.precision(3);
	  s.str("");
	  s<<"# scale: "<<debug_point.debug_point->scale;
	  messages.push_back(s.str());
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	  
	  s.precision(3);
	  s.str("");
	  s<<"# point dcost: "<<debug_point.debug_point->point_cost;
	  messages.push_back(s.str());
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	  
	  s.precision(20);
	  s.str("");
	  s<<"# point  sail: "<<debug_point.debug_point->saliency;
	  messages.push_back(s.str());
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	  
	  s.precision(3);
	  s.str("");
	  T temp=tracker_p[debug_point.debug_point_tracker_id]->opt_temp;
	  s<<"# point scost: "<<debug_point.debug_point->compute_cost3(temp);
	  messages.push_back(s.str());
	  
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	  
// 	  typename CEdgecost<T,Dim>::EDGECOST_STATE  inrange;
// 	   T ecost=debug_point.debug_point->edge_energy(
// 	    *tracker_p[debug_point.debug_point_tracker_id]->edgecost_fun,
// 	    0,
// 	    0,
// 	    tracker_p[debug_point.debug_point_tracker_id]->maxendpointdist,
// 	    inrange);
// 	  s.str("");
// 	  s<<"# point ecost: "<<ecost<<" valid:"<<(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	  
	  
	  
	  s.str("");
	  s<<"# grid normal:"<<debug_point.debug_point->has_owner(0)<<" con:"<<debug_point.debug_point->has_owner(1);
	  messages.push_back(s.str());
// 	  text_HUD->StringoutLine(s.str(),top_row++);
	  
	  
	  messages.push_back("--snapshots--");
	  s.precision(3);
	  s.str("");
	  s<<"# edgecost: "<<debug_point.debug_point->snapshot_connected_egde_cost;
	  messages.push_back(s.str());
	  
	  s.str("");
	  s<<"# proposed edge: "<<debug_point.debug_point->snapshot_edge_proposed_costs[0]
	   <<" -> "<<debug_point.debug_point->snapshot_edge_proposed_costs[1]
	   <<" : "<<debug_point.debug_point->snapshot_edge_proposed_costs[3]
	   <<" ? "<<(debug_point.debug_point->snapshot_edge_proposed_costs[2]>0)
	   <<" ("<<debug_point.debug_point->snapshot_edge_proposed_costs[4]<<"|"<<debug_point.debug_point->snapshot_edge_proposed_costs[5]<<")";
	  messages.push_back(s.str());
	  
	  
	  
// 	  s.str("");
// 	  s<<"# point fcost: "<<tracker_p[debug_point_tracker_id]->options.particle_connection_state_cost_offset[0];
// 	  text_HUD->StringoutLine(s.str(),top_row++); 
	  s.precision(2);
	}
	
	
	
	for (std::list<std::string>::iterator iter=messages.begin();iter!=messages.end();iter++)
	{
		  text_HUD->StringoutLine((*iter),top_row++);  
	}
	
	/*
	  cpoint.point_cost=newenergy_data;
        cpoint.saliency=newpointsaliency/saliency_correction[2];


        T E=newenergy_data/temp;


        // cost for non-connected point
        T pointcost=particle_connection_state_cost_scale[0]*cpoint.compute_cost()+(particle_connection_state_cost_offset[0]);//+PointScaleCost*cpoint.compute_cost_scale();
        E+=(pointcost)/temp;


        T R=mexp(E);
	*/
	
	
      } catch (...)
      {
	
      }
      top_row++;
      
  
      
     
      
//       top_row++;
	  
      
      sta_assert_error(n.norm2()>0);
	    
      get_patch(sliceview_slice,debug_point.debug_point->position+debug_poit_world_offset,n,sliceview_extents,(scale*3.0f)/sliceview_extents);
      mhs_check_gl
      glDeleteTextures(1,&sliceview_textureID);	
	sliceview_textureID=GenerateTextureRectangle(sliceview_slice,sliceview_extents,sliceview_extents);
      mhs_check_gl
      
glDisable(GL_CULL_FACE);
glEnable(GL_COLOR_MATERIAL);
      glMatrixMode(GL_TEXTURE);
      glLoadIdentity();
      glMatrixMode(GL_MODELVIEW);
      glPushMatrix();
      glLoadIdentity();
    //    glEnable(GL_BLEND);
      glColor4f(1,1,1,1);
      glActiveTextureARB ( GL_TEXTURE0_ARB );
      mhs_check_gl
      glEnable(GL_TEXTURE_2D);
      mhs_check_gl
      glBindTexture ( GL_TEXTURE_2D, sliceview_textureID);
      mhs_check_gl
      


      
	const float w=1;
	const float h=1;
	float pixel_offset_y=5+13;
	float pixel_offset_x=5+8;
	float row=top_row+1;
	float x=float(pixel_offset_x)/width;
	float y=float(height-(row*pixel_offset_y))/height;   
// 	float xw=float(sliceview_extents)/width;
// 	float yw=float(sliceview_extents)/height;
	float xw=float(128)/width;
	float yw=float(128)/height;   
      glBegin ( GL_QUADS );
      {
	  glTexCoord2f (w,h );
	  glVertex3f ( x+xw,y-yw,0 );

	  glTexCoord2f( 0,h );
	  glVertex3f ( x,y-yw,0 );

	  glTexCoord2f( 0,0 );
	  glVertex3f ( x,y,0 );

	  glTexCoord2f( w,0 );
	  glVertex3f ( x+xw,y,0 );
      }
      glEnd();
      
      glBindTexture ( GL_TEXTURE_2D, 0);
      mhs_check_gl
      glDisable(GL_TEXTURE_2D);
      glPopMatrix();
      }
      else
      {
	sta_assert_error(debug_point.debug_point!=NULL);
// 	debug_point.debug_point->is_focus_point=false;
	debug_point.clear();
// 	debug_point.debug_point->clear_selection();
// 	debug_point.debug_point=NULL;  
      }
      
   }
  }
}
/*
  glActiveTextureARB ( GL_TEXTURE0_ARB );
glDisable ( GL_TEXTURE_RECTANGLE_ARB );
glActiveTextureARB ( GL_TEXTURE1_ARB );
glDisable ( GL_TEXTURE_RECTANGLE_ARB );
glActiveTextureARB ( GL_TEXTURE2_ARB );
glDisable ( GL_TEXTURE_RECTANGLE_ARB );*/


			
			
			glPopMatrix();
	text_HUD->DeinitHUD();	
	
	mhs_check_gl
	glFinish();
	SDL_GL_SwapWindow(tracker_data_p->tracker_window);
	
    }
   
      
      
      
      void register_tracker(CTracker<TData,T,Dim> & tracker)
      {
	tracker_p.push_back(&tracker);
      }
      
      
    
 protected:    
   
   
   
  GLuint GenerateTextureRectangle(const float * slice,int w, int h)
  {
	GLenum internalFormat=GL_LUMINANCE8;//GL_LUMINANCE16
	GLuint textureID;

// 	glPixelStorei(GL_UNPACK_ROW_LENGTH, h); 
// 	glPixelStorei (GL_UNPACK_ALIGNMENT, 1); 

	glGenTextures(1, &textureID);


	//glBindTexture(GL_TEXTURE_RECTANGLE_ARB, textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

 	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
 	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);


// 	glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, internalFormat,
// 			  h,w, 0,GL_LUMINANCE, getFormat<float>(), slice);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat,
			  h,w, 0,GL_LUMINANCE, getFormat<float>(), slice);
// 		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		
// 		 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
// 	    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
// 	    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_EDGE);
// 	    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
// 	    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	  
	
 	return textureID;
  }


   
   bool get_patch(float * img_slice,const Vector<T,3> & position,const Vector<T,3> & dir,int res,float scale)
   {
     
    Vector<T,3> vn1;
    Vector<T,3> vn2;
    Vector<T,3> p;
    mhs_graphics::createOrths(dir,vn1,vn2); 
    int shapei[3];
    shapei[0]=shape[0];
    shapei[1]=shape[1];
    shapei[2]=shape[2];

    vn1*=scale;
    vn2*=scale;
    
    if (img!=NULL)
      {
	
	
	
	for (int x=0;x<res;x++)
	{
	 for (int y=0;y<res;y++) 
	 {
	   p=position+vn1*(x-0.5*(res-1))+vn2*(y-0.5*(res-1));
	   
	   if (true)
	   {
		int Z=std::floor(p[0]+0.5f);
		int Y=std::floor(p[1]+0.5f);
		int X=std::floor(p[2]+0.5f);
		  if ((Z>=shapei[0])||(Y>=shapei[1])||(X>=shapei[2])||
                  (Z<0)||(Y<0)||(X<0))
		 {
		  img_slice[y*res+x]=0.0f;
		 }else
		 {
		  img_slice[y*res+x]=img[((Z*shapei[1]+Y)*shapei[2]+X)];
		}
	   }else
	   {
	 	int Z=std::floor(p[0]);
		int Y=std::floor(p[1]);
		int X=std::floor(p[2]);
		  if ((Z>=shapei[0])||(Y>=shapei[1])||(X>=shapei[2])||
                  (Z<0)||(Y<0)||(X<0))
		 {
		  img_slice[y*res+x]=0.0f;
		 }else
		 {
		  
		T wz=(p[0]-Z);
	        T wy=(p[1]-Y);
	        T wx=(p[2]-X);
	        {
	            float & g=img_slice[y*res+x];
		    g=0.0f;
	            g+=(1-wz)*(1-wy)*(1-wx)*img[((Z*shapei[1]+Y)*shapei[2]+X)];
	            g+=(1-wz)*(1-wy)*(wx)*img[((Z*shapei[1]+Y)*shapei[2]+(X+1))];
	            g+=(1-wz)*(wy)*(1-wx)*img[((Z*shapei[1]+(Y+1))*shapei[2]+X)];
	            g+=(1-wz)*(wy)*(wx)*img[((Z*shapei[1]+(Y+1))*shapei[2]+(X+1))];
	            g+=(wz)*(1-wy)*(1-wx)*img[(((Z+1)*shapei[1]+Y)*shapei[2]+X)];
	            g+=(wz)*(1-wy)*(wx)*img[(((Z+1)*shapei[1]+Y)*shapei[2]+(X+1))];
	            g+=(wz)*(wy)*(1-wx)*img[(((Z+1)*shapei[1]+(Y+1))*shapei[2]+X)];
	            g+=(wz)*(wy)*(wx)*img[(((Z+1)*shapei[1]+(Y+1))*shapei[2]+(X+1))];
	        }
		}
	   }
	 }
	}
	
	
	float  minv=std::numeric_limits<float>::max();
	float  maxv=std::numeric_limits<float>::min();
	
	for (int x=0;x<res*res;x++)
	{
	  minv=std::min(minv,img_slice[x]);
	  maxv=std::max(maxv,img_slice[x]);
	}
	float scale=(maxv-minv)+0.000001;
	for (int x=0;x<res*res;x++)
	  img_slice[x]=(img_slice[x]-minv)/scale;
	
// 	int Z=std::floor(position[0]);
// 	int Y=std::floor(position[1]);
// 	int X=std::floor(position[2]);
// 	  
// 	T wz=(position[0]-Z);
//         T wy=(position[1]-Y);
//         T wx=(position[2]-X);
// 
//         Vector<T,Dim> vessel_grad=T(0);
//         for (int i=0; i<3; i++)
//         {
//             T & g=vessel_grad.v[i];
//             g+=(1-wz)*(1-wy)*(1-wx)*gradient_v[((Z*this->shape[1]+Y)*this->shape[2]+X)*3+i];
//             g+=(1-wz)*(1-wy)*(wx)*gradient_v[((Z*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
//             g+=(1-wz)*(wy)*(1-wx)*gradient_v[((Z*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
//             g+=(1-wz)*(wy)*(wx)*gradient_v[((Z*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
//             g+=(wz)*(1-wy)*(1-wx)*gradient_v[(((Z+1)*this->shape[1]+Y)*this->shape[2]+X)*3+i];
//             g+=(wz)*(1-wy)*(wx)*gradient_v[(((Z+1)*this->shape[1]+Y)*this->shape[2]+(X+1))*3+i];
//             g+=(wz)*(wy)*(1-wx)*gradient_v[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+X)*3+i];
//             g+=(wz)*(wy)*(wx)*gradient_v[(((Z+1)*this->shape[1]+(Y+1))*this->shape[2]+(X+1))*3+i];
//         }
	  
	  

// 	  if ((Z+1>=this->shape[0])||(Y+1>=this->shape[1])||(X+1>=this->shape[2])||
// 		  (Z<0)||(Y<0)||(X<0))
// 	      return false;
	
	
	return true;
      }
      return false;
   }
   
   
   
   bool hittest(GLdouble  winX,  GLdouble  winY, Vector<float,3> & focus_point, bool particle_test=true)
   {
     //TODO ; consider cliplane
     
     
     Matrix<double,4> projection;
     Matrix<double,4> modelview;
     
     Vector<double,3> hit0;
     Vector<double,3> hit1;
     
     Vector<int,4> viewport;
     viewport[0]=0;
     viewport[1]=0;
     viewport[2]=width;
     viewport[3]=height;
     
     
     projection=projection_mat;
     modelview=(m_transform_current*m_transform);
     
     float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
     
     Vector<float,3>new_center;
      new_center[0]=rescale*shape[0]/2.0f;
      new_center[1]=rescale*shape[1]/2.0f;
      new_center[2]=rescale*shape[2]/2.0f;
     Matrix<float,4> trafo;
      trafo=modelview;
     
     gluUnProject(winX,height-winY,0.0,(const GLdouble *)(modelview.v),  (const GLdouble *) (projection.v), ( const GLint *)(viewport.v), &hit0.v[0],&hit0.v[1],&hit0.v[2]);
     gluUnProject(winX,height-winY,1.0,(const GLdouble *)(modelview.v),  (const GLdouble *) (projection.v), ( const GLint *)(viewport.v), &hit1.v[0],&hit1.v[1],&hit1.v[2]);
     
     Vector<float,3> ray_start;
     Vector<float,3> ray_dir;
     
     //without clipping
     ray_start=hit0;
     ray_dir=(hit1-hit0);ray_dir.normalize();
    

    
   
    
    if (particle_test)
    {
        Vector<float,3> selection; 
	Vector<float,3> selection_img;
	float dist=std::numeric_limits< float >::max();
	
	float eye_dist=std::numeric_limits< float >::max();
 	float min_dist_sq=rescale*rescale*25*25;
// 	float min_dist_sq=0.05*0.05;
  
	
	bool no_single=(render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_CONNECTED))
			|(render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_LINE));
	
	
	for (int a=0;a<tracker_p.size();a++)
	{
	  
	  sta_assert_error(tracker_p[a]);
	  CTracker<TData,T,Dim> & tracker=*tracker_p[a];
	  
	  if ((render_mode&static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_ONLINE))&&(tracker.is_offline()))
	    continue;
	  
	  Vector<T,3> world_offset=tracker.get_world_offset();
	  
	  const class Points<T,Dim> ** particle_ptr=tracker.particle_pool->getMem();
	  std::size_t n_points=tracker.particle_pool->get_numpts();
	  
	  
	    
	    Vector<float,Dim> new_pos;
	    Points<T,Dim>  point;
	    for (unsigned int i=0; i<n_points; i++)
	    {
		Points<T,Dim> * point_p=((Points<T,Dim>*)particle_ptr[i]);
		if (point_p==NULL)
		  continue;
		  
		if (!tracker.particle_pool->is_valid_adress(point_p))
		{
		  continue;
		}
		
		point=*point_p;
		
		if ((no_single)&&(!point.isconnected()))
		  continue;
		
		Vector<float,3> position;
		
		//new_pos=point.position+world_offset;
		position=point.position+world_offset;
		
		new_pos=(position*rescale-new_center);
		
		float ray_proj_dist=((new_pos-ray_start).dot(ray_dir));
		
		//TODO : consier distance to viewer 
		//IDEA : include scale of particel in collision 
		
		//if (ray_proj_dist<0) continue; //behind eye
		
		Vector<float,3> closet_ray_pt=ray_start+ray_dir*ray_proj_dist;
		
		
		/*
		float tmp=(closet_ray_pt[0]-new_pos[0]);
		float new_dist=tmp*tmp;
		if (min_dist_sq<new_dist) continue;
		tmp=(closet_ray_pt[1]-new_pos[1]);
		new_dist+=tmp*tmp;
		if (min_dist_sq<new_dist) continue;
		tmp=(closet_ray_pt[2]-new_pos[2]);
		new_dist+=tmp*tmp;
		if (min_dist_sq<new_dist) continue;
		if (eye_dist>ray_proj_dist)
		{
		  debug_point=point_p;
		  debug_life=point.get_life();
		  dist=new_dist;
		  selection=new_pos; 
		}
		*/
		
		
		
		float tmp=(closet_ray_pt[0]-new_pos[0]);
		float new_dist=tmp*tmp;
		if (dist<new_dist) continue;
		tmp=(closet_ray_pt[1]-new_pos[1]);
		new_dist+=tmp*tmp;
		if (dist<new_dist) continue;
		tmp=(closet_ray_pt[2]-new_pos[2]);
		new_dist+=tmp*tmp;
		if (dist>new_dist)
		{
		  
		  
		  debug_point.assign(point_p,a);
//  		  if (debug_point.debug_point!=NULL)
// 		    debug_point.debug_point->clear_selection();
// 		  debug_point.clear();
// 		    debug_point.debug_point->is_focus_point=false;
		  
// 		  debug_point.debug_point=point_p;
// 		  debug_point.debug_point->is_focus_point=true;
// 		  debug_point.debug_life=point.get_life();
// 		  debug_point.debug_point_tracker_id=a;
		  dist=new_dist;
		  selection=new_pos; 
		  debug_poit_world_offset=world_offset;
		  selection_img=position;
		}
		
	    }
	}
	//if (dist<0.05*0.05)
	if (dist<min_dist_sq)
	{
	  
	  //get_patch(const Vector<T,3> & position,const Vector<T,3> & dir,sliceview_extents);
	  
	  focus_point=trafo.multv3(selection);
	  focus_point_img=selection_img;
	  return true;
	}
 	//if (debug_point.debug_point!=NULL)
	  debug_point.clear();
// 		    debug_point.debug_point->is_focus_point=false;
	//debug_point.debug_point=NULL;
    }else  // img volume test
    {
      
      ray_start+=new_center;
      ray_start/=rescale;
     
      if (img!=NULL)
      {
		Vector<float,3> selection; 
		int max_depth=2.0*std::max(std::max(shape[0],shape[1]),shape[2]);
		int num_intersections=0;
// 		TData sensitivity=0.25;
		TData sensitivity=0.5;
	
		TData max_value=std::numeric_limits<TData>::min();
		TData min_value=std::numeric_limits<TData>::max();
		TData threshold;
		TData stepsize=1;
		
		
		
		for (int run=0;run<2;run++)
		{
		    Vector<float,3> pt=ray_start;
		     num_intersections=0;
		    threshold=min_value+(max_value-min_value)*sensitivity;
		    
// 		    if (run==1)
// 		    {
// 		    printf("[%f %f %f]\n",min_value,threshold,max_value); 
// 		    }
		    
		    for (int r=0;r<max_depth;r++)
		    {
			  bool pt_in_img=(pt[0]>0)&&(pt[1]>0)&&(pt[2]>0)&&(pt[0]<shape[0])&&(pt[1]<shape[1])&&(pt[2]<shape[2]);
			  
			    if  (((pt_in_img)&&(num_intersections==0))
			      ||((!pt_in_img)&&(num_intersections==1))) 
			      num_intersections++;
			    
			  if (num_intersections==1)
			  {
			  std::size_t position=(std::floor(pt[0])*shape[1]
			    +std::floor(pt[1]))*shape[2]
			    +std::floor(pt[2]);
			  if (position>shape[0]*shape[1]*shape[2])
			  {
			    printf("point selection: out of volume!\n");
			    continue;
			  }
			  TData value=img[position];
			  if (run==0)
			  {
			    max_value=std::max(max_value,value);
			    min_value=std::min(min_value,value);
			  }else
			  {
			      if (value>threshold)
	  		      {
// 				printf("depth: %d\n",r);
	  			focus_point=trafo.multv3((pt*rescale-new_center));
				focus_point_img=pt;
				
				
				
// 				printf("fpioint\n");
// 				focus_point_img.print();
// 				((trafo.OpenGL_invert().multv3(focus_point)+new_center)/rescale).print();
// 				(trafo*(trafo.OpenGL_invert())).print();
// 				printf("---------------\n");
// 				trafo.print();
// 				trafo.OpenGL_invert().print();
				
	  			return true;
	  		      }
			  }
    			  }
			  if (num_intersections==2)
			  {
			    //stepsize=0.75;
			    stepsize=0.75; 
 			    max_depth=r;
			    break;
			  }
			  pt+=ray_dir;
		    }
		}
		
		
      }
    }
    
    
    
    return false;
      
   }
   
   Vector<float,3> focusGL2img()
   {
     Matrix<double,4> projection;
     Matrix<double,4> modelview;
     
     
     Vector<int,4> viewport;
     viewport[0]=0;
     viewport[1]=0;
     viewport[2]=width;
     viewport[3]=height;
     
     
     projection=projection_mat;
     modelview=(m_transform_current*m_transform);
     
     float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
     
     Vector<float,3>new_center;
      new_center[0]=rescale*shape[0]/2.0f;
      new_center[1]=rescale*shape[1]/2.0f;
      new_center[2]=rescale*shape[2]/2.0f;
     Matrix<float,4> trafo;
      trafo=modelview;
      
      return ((trafo.OpenGL_invert().multv3(focus_point)+new_center)/rescale);
   }
    
    
    void update_focus_point(Vector<float,3> & focus_point)
    {
      {
	focus_point=T(0);
	focus_point_img=focusGL2img();
// 	float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
// 	float new_center[3];
// 	new_center[0]=rescale*shape[0]/2.0f;
// 	new_center[1]=rescale*shape[1]/2.0f;
// 	new_center[2]=rescale*shape[2]/2.0f;  
// 	Matrix<float,4> rot=(m_transform_current*m_transform);  
// 	Matrix<float,4> rotInv=rot.OpenGL_invert().OpenGL_get_rot();  
// 	focus_point=rotInv.multv3(rot.OpenGL_get_tranV()*-1)+new_center;
	} 
    }
    
   
    
   
      
    class Mouse
    {
      public:
      int mouse_pos_old[2];
      bool b_down[3];
      bool arcball_on;
      bool trackball_on;
      bool trackballZ_on;
      bool point_select_on;
      int  point_select_mode;
      bool transition_on;
      bool zoom_on;
      bool do_rendering;
      bool do_liveview;
      Mouse()
      {
	mouse_pos_old[0]=mouse_pos_old[1]=0;
	b_down[0]=b_down[1]=b_down[2]=false;
	arcball_on=false;
	trackball_on=false;
	trackballZ_on=false;
	point_select_on=false;
	zoom_on=false;
	do_rendering=true;
	do_liveview=true;
	transition_on=false;
	point_select_mode=0;
      }
      
      bool is_busy()
      {
	return (trackball_on||arcball_on||trackballZ_on||point_select_on||transition_on);
      }
      void reset()
      {
	trackball_on=arcball_on=trackballZ_on=point_select_on=transition_on=false;
      }
    };
    
    Mouse mouse_state;
    ArcBallT * arcBall;
    Matrix3fT   LastRot;	
    Matrix3fT   ThisRot;
//     Matrix4fT   Transform;    
//     Matrix4fT   Transform_current;
    
    Matrix<float,4> m_transform;
    Matrix<float,4> m_transform_current;
    Matrix<float,4> m_transition;
     Matrix<float,4> projection_mat;
    
    Vector<T,Dim> rot_center;
    
    Vector<float,Dim> viewer__start_pos;
    Vector<float,Dim> viewer_pos;
    Vector<float,Dim> viewer_target;
    Vector<float,Dim> viewer_up;
    
    Vector<float,3>  focus_point;
    Vector<float,3>  focus_point_img;
//     Vector<T,Dim> translation;
//     Vector<T,Dim> translation_old;
    float zoom;
    
    bool exit_program;
    bool pause_program;
    int num_keys=1000;
    Uint8 * keystates;
    float clip_plane_dist;
    
    int call_from_main;
    
    unsigned int render_mode;
    
    Uint8 Fkeys[12];
		    
    
    
    public:
//     void ui()
//     {
// 	  
//       
// 	if ((mouse_state.transition_on))
// 	  return;
//       
// 	int num_events=0;
//       	  // SDL_PumpEvents();
// 	  SDL_Event event;
// 	  while (SDL_PollEvent(&event)) 
// 	  {
// 	    
// 	    
// 	     int mouse_pos[2];
// 		  Uint32 button_state=SDL_GetMouseState(mouse_pos,mouse_pos+1);
// 		  bool b_down[3];
// 		  b_down[0]=(button_state & SDL_BUTTON(SDL_BUTTON_LEFT))&&(!(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT)));
// 		  b_down[1]=(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT))&&(!(button_state & SDL_BUTTON(SDL_BUTTON_LEFT)));
// 		  b_down[2]=(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT))&&(button_state & SDL_BUTTON(SDL_BUTTON_LEFT));
// 		  
// 		  int cpy_keys=0;
// 		  const Uint8* keys  = SDL_GetKeyboardState(&cpy_keys);
// 
// 		  
// 		   
// // 		    printf("clipplane dist %d\n",(keys[SDL_SCANCODE_LCTRL]));
// 		  
// 		  switch (event.type) 
// 		  {
// 		      case SDL_MOUSEWHEEL:
// 		      {
// 			      if (keys[SDL_SCANCODE_LCTRL])
// 			      {
// // 				float rescale=std::max(std::max(shape[0],shape[1]),shape[2]);
// // 				{
// // 				if (event.wheel.y<0)
// // 				  clip_plane_dist+=rescale/50.0f;
// // 				else
// // 				  clip_plane_dist-=rescale/50.0f;
// // 				}
// // 				clip_plane_dist=std::min(rescale,std::max(clip_plane_dist,0.0f));
// 				
// 				
// 				{
// 				if (event.wheel.y<0)
// 				  clip_plane_dist+=1/50.0f;
// 				else
// 				  clip_plane_dist-=1/50.0f;
// 				}
// 				clip_plane_dist=std::min(1.0f,std::max(clip_plane_dist,0.0f));
// 				
// 			      }else
// 			      {
// 				{
// 				if (event.wheel.y<0)
// 				  zoom/=1.15;
// 				else
// 				  zoom*=1.15; 
// 				}
// 				//zoom=std::min(4.0f,std::max(0.05f,zoom));
// 				float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
// 				zoom=std::min(4.0f,std::max(rescale,zoom));
// 				update_projection_mat();
// 			      }
// 		      }break;
// 		      case SDL_KEYUP:
// 		      {
// 			printf("SDL_KEYUP!!!\n");
// 		      }break;
// 		      case SDL_KEYDOWN:
// 		      {
// 			printf("SDL_KEYDOWN!!!\n");
// 		      }break;
// 		 }
// 		  
// 		  //if (keys_old!=*keys)
// 		  {
// 		    if (keys[SDL_SCANCODE_Q]&&(!keystates[SDL_SCANCODE_Q]))
// 		    {
// 		      if (exit_program)
// 			return;
// 			
// 		        add_info("exit");
// 			//mhs::mex_dumpStringNOW();
// 			exit_program=true;
// 		    }
// 		    
// 		     if (keys[SDL_SCANCODE_L]&&(!keystates[SDL_SCANCODE_L]))
// 		    {
// 		      mouse_state.do_liveview=!mouse_state.do_liveview;
// 		      if (mouse_state.do_liveview)
// 			{add_info("observer: on");}else
// 			{add_info("observer: off");}
// 		    }
// 		    
// 		    
// 		    if (keys[SDL_SCANCODE_P]&&(!keystates[SDL_SCANCODE_P]))
// 		    { 
// 			pause_program=!pause_program;
// 			if (pause_program)
// 			{
// // 			      printf("pausing!\n");
// 			      add_info("pausing");
// 			}else
// 			{
// 			      add_info("continuing");
// // 			      printf("continuing!\n");
// 			}
// 			//mhs::mex_dumpStringNOW();
// 		    }
// 		    
// 		    if (keys[SDL_SCANCODE_V]&&(!keystates[SDL_SCANCODE_V]))
// 		    {
// 			mouse_state.do_rendering=!mouse_state.do_rendering;
// 			if (mouse_state.do_rendering)
// 			{add_info("volume rendering: on");}else
// 			{add_info("volume rendering: off");}
// 		    }
// 		    
// 		    
// 		    if (keys[SDL_SCANCODE_O])
// 		    {
// 			add_info("showing matlab GUI");
// 			mexCallMATLAB(0, NULL,0,NULL, "start_gui");
// 			update_tracker_gui();
// 		    }
// 		    
// 		    if (keys[SDL_SCANCODE_1])
// 		    {
// 			render_mode=static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_DEFAULT);
// 		    }
// 		    
// 		    if (keys[SDL_SCANCODE_2])
// 		    {
// 			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_CONNECTED);
// 		    }
// 	
// 		     if (keys[SDL_SCANCODE_3])
// 		    {
// 			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_LINE);
// 		    }
// 		    
// 		    if (keys[SDL_SCANCODE_4])
// 		    {
// 			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER);
// 		    }
// 		    
// 		    if (keys[SDL_SCANCODE_5])
// 		    {
// 			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_ONLINE);
// 		    }
// 
// 		     if (keys[SDL_SCANCODE_D])
// 		    {
// 		      for (int i=0;i<tracker_p.size();i++)
// 		      {
// 			  sta_assert_error(tracker_p[i]!=NULL);
// 			  CTracker<TData,T,Dim> & tracker=*tracker_p[i];
//  			  
//  			  tracker.execute(CTracker<TData,T,Dim>::DEBUG_COMMAND::DEBUG_CLEAR_ALL);
// 		      }
// 		    }
// // 	
// 		   
// 		  }
// 	
// 		  
// 	    
// 		 
// 		  //b_down[2]=(button_state & SDL_BUTTON(SDL_BUTTON_MIDDLE));
// 		  
// // 		  int mouse_mode=0;
// 		  /*
// 		  if (!(mouse_state.trackball_on)&&!(mouse_state.arcball_on)&&!(mouse_state.trackballZ_on)) 
// 		  {
// 		    if ((b_down[0])&&(keys[SDL_SCANCODE_S]))
// 		    { 
// 		      
// 		      mouse_mode=1;
// 		      if ((mouse_state.mouse_pos_old[0]!=mouse_pos[0])&&(mouse_state.mouse_pos_old[1]!=mouse_pos[1]))
// 		      {
// 		     
// 		      hittest(mouse_pos[0],mouse_pos[1]);
// 		      mouse_state.b_down[0]=false;
// 		      mouse_state.mouse_pos_old[0]=mouse_pos[0];
// 		      mouse_state.mouse_pos_old[1]=mouse_pos[1];	 
// 		      }
// 		    }
// 		  }
// 		  */
// 		  
// 		  
// // 		  printf("%d %d\n",keys[SDL_SCANCODE_SPACE],keys[SDL_SCANCODE_LSHIFT]);
// 		  
// 		  if ((!mouse_state.transition_on))
// 		  {
// 		  for (int b=0;b<3;b++)
// 		  {
// 		    
//  		  
// 		    
// 		    if (b_down[b])
// 		    {
// 		      if (!mouse_state.b_down[b]) // mouse_down_event
// 		      {
// // 			printf("button %d down\n",b);
// 			//mhs::mex_dumpStringNOW();
// 			if (!mouse_state.is_busy())
// 			{
// 			  switch(b)
// 			  {
// 			    case 0:
// 			    {
// 			      //if (keys[SDL_SCANCODE_S])
// 			      //if (((keys[SDL_SCANCODE_SPACE]&&(!keystates[SDL_SCANCODE_SPACE])))||(keys[SDL_SCANCODE_LSHIFT]&&(!keystates[SDL_SCANCODE_LSHIFT])))
// 			      if ((keys[SDL_SCANCODE_SPACE])||(keys[SDL_SCANCODE_LSHIFT]))
// 			      {
// 				if (keys[SDL_SCANCODE_SPACE])
// 				{
// 				  add_info("particle cursor: on");
// 				  mouse_state.point_select_mode=0;
// 				}
// 				else 
// 				{
// 				  add_info("data cursor: on");
// 				  mouse_state.point_select_mode=1;
// 				}
// 				mouse_state.point_select_on=true;
// 				mouse_state.mouse_pos_old[0]=-1;
// 				mouse_state.mouse_pos_old[1]=-1;
// 			      }else
// 			      {
// 				mouse_state.mouse_pos_old[0]=mouse_pos[0];
// 				mouse_state.mouse_pos_old[1]=mouse_pos[1];	  
// 				mouse_state.arcball_on=true;
// 				Point2fT    MousePt;
// 	    // 		    MousePt.s.X=height-mouse_pos[1];
// 				MousePt.s.Y=mouse_pos[0];
// 				MousePt.s.X=height-mouse_pos[1];		    
// 				LastRot = ThisRot;										// Set Last Static Rotation 
// 				arcBall->click(&MousePt);	
// 				float tx=(mouse_pos[0])/(float(width))-0.5f;
// 				float ty=(mouse_pos[1])/(float(height))-0.5f;
// 				rot_center[0]=tx;
// 				rot_center[1]=ty;
// 				memcpy(&LastRot,&Identity3x3,sizeof(Matrix3fT));
// 			      }
// 			    }break;
// 			    case 1:
// 			    {
// 			      mouse_state.mouse_pos_old[0]=mouse_pos[0];
// 			      mouse_state.mouse_pos_old[1]=mouse_pos[1];	  
// 			      mouse_state.trackball_on=true;
// 			    }break;
// 			    case 2:
// 			    {
// 			      mouse_state.mouse_pos_old[0]=mouse_pos[0];
// 			      mouse_state.mouse_pos_old[1]=mouse_pos[1];	  
// 			      mouse_state.trackballZ_on=true;
// 			    }break;
// 			  }
// 			}
// 		      }else{  //  mouse_is_down
// 			  if (mouse_state.arcball_on)   
// 			  {
// 				Quat4fT     ThisQuat;
// 				Point2fT    MousePt;
// 				MousePt.s.Y=mouse_pos[0];
// 				MousePt.s.X=height-mouse_pos[1];
// 				arcBall->drag(&MousePt, &ThisQuat);						// Update End Vector And Get Rotation As Quaternion
// 				Matrix3fSetRotationFromQuat4f(&ThisRot, &ThisQuat);		// Convert Quaternion Into Matrix3fT
// 				Matrix3fMulMatrix3f(&ThisRot, &LastRot);				// Accumulate Last Rotation Into This One
// 				
// 				m_transform_current.OpenGL_from3x3(ThisRot.M);
// 				update_focus_point(focus_point);
// 
// 			  }else if (mouse_state.trackball_on)   
// 			  {
// 			    float tx=(mouse_state.mouse_pos_old[0]-mouse_pos[0])/(float(width));
// 			    float ty=(mouse_state.mouse_pos_old[1]-mouse_pos[1])/(float(height));
// 			  
//  			    m_transform_current.m[3][0]=ty*zoom*3;
//  			    m_transform_current.m[3][1]=tx*zoom*3;
// // 			    float rescale=std::max(std::max(shape[0],shape[1]),shape[2]);
// // 			    m_transform_current.m[3][0]=ty*zoom*rescale/20.0f;
// // 			    m_transform_current.m[3][1]=tx*zoom*rescale/20.0f;
// 			    update_focus_point(focus_point);
// 			    
// 			  }else if (mouse_state.trackballZ_on)   
// 			  {
// 			    {
// 			      
// 			    float ty=(mouse_state.mouse_pos_old[1]-mouse_pos[1])/(float(height));
// // 			    float rescale=std::max(std::max(shape[0],shape[1]),shape[2]);
// // 			    m_transform_current.m[3][2]=-ty*rescale/20.0f;
// 			    
// 			    m_transform_current.m[3][2]=-ty*3.0f;
// 			    update_focus_point(focus_point);
// 			    }
// 			  }else if (mouse_state.point_select_on)
// 			  {
// 			    	if ((mouse_state.mouse_pos_old[0]!=mouse_pos[0])&&(mouse_state.mouse_pos_old[1]!=mouse_pos[1]))
// 				{
// 				
// 				hittest(mouse_pos[0],mouse_pos[1],focus_point,(mouse_state.point_select_mode==0));
// 				mouse_state.mouse_pos_old[0]=mouse_pos[0];
// 				mouse_state.mouse_pos_old[1]=mouse_pos[1];	 
// 				}
// 			  }
// 		      }
// 		    }else if (mouse_state.b_down[b]) // mouse_up_event
// 		    {
// // 			printf("button %d up\n",b);
// 			
// 			if (b==0)
// 			{
// 			  if (mouse_state.arcball_on)
// 			  {
// 			    mouse_state.arcball_on=false;
// 			    m_transform=m_transform_current*m_transform;
// 			    m_transform_current.Identity();
// 			  }
// 			  if ((mouse_state.point_select_on))
// 			  {
// 			    mouse_state.transition_on=true;
// 			    m_transform_current.Identity();
// 			    m_transition.Identity();
// 			    m_transition.OpenGL_set_tranV(focus_point*-0.2);
// 		    
// 				if ( mouse_state.point_select_mode==0) 
// 				{add_info("particle cursor: off");}
// 				else 
// 				{add_info("data cursor: off");}
// 			    
//  			    //m_transform_current.OpenGL_set_tranV(focus_point*-0.1);
// 			    
// // 			    mouse_state.point_select_on=false;
// // 			    m_transform_current.Identity();
// // 			    m_transform_current.OpenGL_set_tranV(focus_point*-1);
// // 			    m_transform=m_transform_current*m_transform;
// // 			    m_transform_current.Identity();
// // 			    update_focus_point();
// 			  }
// 			}
// 			if (b==1)
// 			{
// 			  mouse_state.trackball_on=false;
// 			  m_transform=m_transform_current*m_transform;
// 			  m_transform_current.Identity();
// 			}
// 			if (b==2)
// 			{
// 			  mouse_state.trackballZ_on=false;
// 			  m_transform=m_transform_current*m_transform;
// 			  m_transform_current.Identity();
// 			}
// 			m_transform.OpenGL_reorthonormalize();
// 			//mhs::mex_dumpStringNOW();
// 		    } 
// 		    mouse_state.b_down[b]=b_down[b];
// 		  }
// 		  }
// 		   sta_assert_error(!(cpy_keys>num_keys));
// 		   memcpy(keystates,keys,sizeof(Uint8)*cpy_keys);
// 		    
// 	  }
//       printf("numev: %d\n",num_events);
//     }
    
      
      
//     void mark_point(unsigned int selection)  
//     {
//       sta_assert_error(selection<num_debug_points);
//       
//       if (debug_point_list[selection].debug_point!=NULL)
//       {
// // 	sta_assert_error(debug_point_list[selection].debug_point->selection_id>-1);
// // 	if (debug_point_list[selection].debug_point->selection_id==selection)
// 	{
// 	  debug_point_list[selection].debug_point->clear_selection();
// 	  debug_point_list[selection].debug_point=NULL;
// 	}
//       }else 
//       {
// 	if (debug_point.debug_point!=NULL)
// 	{
// 	  if (debug_point.debug_point->selection_id>0)
// 	  {
// 	    int old_id=debug_point.debug_point->selection_id;
// 	    debug_point_list[old_id].debug_point->clear_selection();
// 	    debug_point_list[old_id].debug_point=NULL;
// 	  }
// 	  debug_point_list[selection]=debug_point;
// // 	  sta_assert_error(debug_point_list[selection].debug_point!=NULL);
// 	  {
// 	    debug_point_list[selection].debug_point->is_selected=true;
// 	    debug_point_list[selection].debug_point->selection_id=selection;
// 	    debug_point_list[selection].debug_life=debug_point_list[selection].debug_point->get_life();
// 	  }
// 	}
//       }
//       
//     }
      
      
    void ui()
    {
	  
      
       
      
      if ((mouse_state.transition_on))
      {
	float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
			    if (focus_point.norm1()<rescale)
			    {
			    m_transform=m_transform_current*m_transform;
			    m_transform_current.Identity();
			    update_focus_point(focus_point);
// 			    mouse_state.transition_on=false;
			    mouse_state.reset();
			    }else
			    {
			      focus_point=m_transition.multv3(focus_point);
			      m_transform_current=m_transition*m_transform_current;
			    }
	    return;
	}
	    
      
      
// 	if ((mouse_state.transition_on))
// 	  return;
      
	int num_events=0;
      	  // SDL_PumpEvents();
	  SDL_Event event;
	  
	  int mouse_wheel=0;
	  
	  while (SDL_PollEvent(&event)) 
	  {
	    num_events++;
	    	  switch (event.type) 
		  {
		      case SDL_MOUSEWHEEL:
		      {
			
			  mouse_wheel=event.wheel.y;
		      }break;
// 		      case SDL_KEYUP:
// 		      {
// 			if (event.key.repeat == 0)
// // 			printf("SDL_KEYUP %d!!!\n",event.keysym);
// 			  printf("SDL_KEYUP !!!\n");
// 		      }break;
// 		      case SDL_KEYDOWN:
// 		      {
// 			if (event.key.repeat == 0)
// 			  printf("SDL_KEYDOWN !!!\n");
// // 			printf("SDL_KEYDOWN %d!!!\n",event.keysym);
// 		      }break;
		 }
	  }
	  
	    
	  if (num_events==0)  
	    return;
	    
	    
	     int mouse_pos[2];
		  Uint32 button_state=SDL_GetMouseState(mouse_pos,mouse_pos+1);
		  bool b_down[3];
		  b_down[0]=(button_state & SDL_BUTTON(SDL_BUTTON_LEFT))&&(!(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT)));
		  b_down[1]=(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT))&&(!(button_state & SDL_BUTTON(SDL_BUTTON_LEFT)));
		  b_down[2]=(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT))&&(button_state & SDL_BUTTON(SDL_BUTTON_LEFT));
		  
		  int cpy_keys=0;
		  const Uint8* keys  = SDL_GetKeyboardState(&cpy_keys);
// 		   printf("%d %d\n",keys[SDL_SCANCODE_SPACE],keys[SDL_SCANCODE_LSHIFT]);
// 		  printf("%d %d\n",keys[SDL_SCANCODE_LCTRL],keys[SDL_SCANCODE_LSHIFT]);
		   
	/*printf("next %d\n",cpy_keys);	*/   
		  
		  if (mouse_wheel!=0)
		  {
			      if (keys[SDL_SCANCODE_LCTRL])
			      {
				{
				if (mouse_wheel<0)
				  clip_plane_dist+=1/50.0f;
				else
				  clip_plane_dist-=1/50.0f;
				}
				clip_plane_dist=std::min(1.0f,std::max(clip_plane_dist,0.0f));
				
			      }else
			      {
				{
				if (mouse_wheel<0)
				  zoom/=1.15;
				else
				  zoom*=1.15; 
				}
				//zoom=std::min(4.0f,std::max(0.05f,zoom));
				float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
				zoom=std::min(4.0f,std::max(rescale,zoom));
				update_projection_mat();
			      }
  
		  }
		  

		  
		   
// 		    printf("clipplane dist %d\n",(keys[SDL_SCANCODE_LCTRL]));
		  
	
		  
		  //if (keys_old!=*keys)
		  {
		    if (keys[SDL_SCANCODE_Q]&&(!keystates[SDL_SCANCODE_Q]))
		    {
		      if (exit_program)
			return;
			
		        add_info("exit");
			//mhs::mex_dumpStringNOW();
			exit_program=true;
		    }
		    
		     if (keys[SDL_SCANCODE_L]&&(!keystates[SDL_SCANCODE_L]))
		    {
		      mouse_state.do_liveview=!mouse_state.do_liveview;
		      if (mouse_state.do_liveview)
			{add_info("observer: on");}else
			{add_info("observer: off");}
		    }
		    
		    
		    //TODO: focus_point_img is currentlky only valid when selecting 
		    //an IMG point using the data curser. It is not updated by any other 
		    //operation
		    if (keys[SDL_SCANCODE_I]&&(!keystates[SDL_SCANCODE_I]))
		    {
// 		      printf("focus point:\n");
// 		      float rescale=1.0/std::max(std::max(shape[0],shape[1]),shape[2]);
//      
// // 		      Vector<float,3>new_center;
// // 			new_center[0]=rescale*shape[0]/2.0f;
// // 			new_center[1]=rescale*shape[1]/2.0f;
// // 			new_center[2]=rescale*shape[2]/2.0f;
// // 			;
// 		      Vector<float,3>new_center;
// 			new_center[0]=shape[0]/2.0f;
// 			new_center[1]=shape[1]/2.0f;
// 			new_center[2]=shape[2]/2.0f;
// 			;
// 			
// 		      Matrix<float,4> R=m_transform.OpenGL_get_rot().OpenGL_invert();
// 		      Vector<float,3> T=m_transform.OpenGL_get_tranV();
// 			
// 		      Vector<float,3> realpoint=(m_transform.OpenGL_invert().multv3(focus_point))/rescale+new_center;
		      
// 		      Vector<float,3> realpoint=(m_transform.OpenGL_invert().multv3(focus_point))/rescale+new_center;
		      
		      Vector<float,3> realpoint=focus_point_img;
		      
		      for (int a=0;a<tracker_p.size();a++)
		      {
			
			sta_assert_error(tracker_p[a]);
			CTracker<TData,T,Dim> & tracker=*tracker_p[a];
			
			Vector<float,3> offset;
			offset=tracker.get_world_offset();
			Vector<float,3> realpoint_offset=realpoint-offset;
			bool found=false;
			if (tracker.is_in_bb(realpoint_offset.v))
			{
 			  class CTracker<TData,T,Dim>::User_action * action=new class CTracker<TData,T,Dim>::User_action_add_particle();
			  ((class CTracker<TData,T,Dim>::User_action_add_particle *)action)->position=realpoint_offset;
 			  tracker.execute(action);
			  found=true;
			}
			if (found)
			  realpoint.print();
			else 
			  printf("warning , point out of all tracker regions!\n");
			
		      }
// 		      focus_point.print();
		    }
		    
		    if (keys[SDL_SCANCODE_P]&&(!keystates[SDL_SCANCODE_P]))
		    { 
			pause_program=!pause_program;
			if (pause_program)
			{
// 			      printf("pausing!\n");
			      add_info("pausing");
			}else
			{
			      add_info("continuing");
// 			      printf("continuing!\n");
			}
			//mhs::mex_dumpStringNOW();
		    }
		    
		    if (keys[SDL_SCANCODE_V]&&(!keystates[SDL_SCANCODE_V]))
		    {
			mouse_state.do_rendering=!mouse_state.do_rendering;
			if (mouse_state.do_rendering)
			{add_info("volume rendering: on");}else
			{add_info("volume rendering: off");}
		    }
		    
		    
		    if (keys[SDL_SCANCODE_O]&&(!keystates[SDL_SCANCODE_O]))
		    {
		      if (call_from_main==0)
		      {
			add_info("showing matlab GUI");
			call_from_main=1;
		      }
			
		    }
		    
		    
		    
		    
		    for (int k=0;k<12;k++)
		    {
		    
		    if (keys[Fkeys[k]]&&(!keystates[Fkeys[k]]))
		    {
			  
			  //unsigned int selection=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_BIRTH);
			  unsigned int selection=k+12*keys[SDL_SCANCODE_SPACE];
			  if (selection<nproposal_names)
			  {
			    for (int i=0;i<tracker_p.size();i++)
			    {
				  sta_assert_error(tracker_p[i]);
				  CTracker<TData,T,Dim> & tracker=*tracker_p[i];
				  tracker. energy_gradient_stat2[1].clear();
				  tracker.selective_stat2[selection]=!tracker.selective_stat2[selection];
				  if (i==0)
				  {
				    if (tracker.selective_stat2[selection])
				      printf("adding %s to stats\n",proposal_names[selection]); 
				    else 
				      printf("removing %s to stats\n",proposal_names[selection]); 
				  }
			    }
			  }
		    }
		    }
		    
		    
		    /*
		    
		     if (keys[SDL_SCANCODE_SPACE]&&keys[SDL_SCANCODE_F2]&&(!keystates[SDL_SCANCODE_F2]))
		    {
			  unsigned int selection=static_cast<unsigned int>(PROPOSAL_TYPES::PROPOSAL_DEATH);
			  if (selection<nproposal_names)
			  {
			    for (int i=0;i<tracker_p.size();i++)
			    {
				  sta_assert_error(tracker_p[i]);
				  CTracker<TData,T,Dim> & tracker=*tracker_p[i];
				  tracker. energy_gradient_stat2[1].clear();
				  tracker.selective_stat2[selection]=!tracker.selective_stat2[selection];
				  if (i==0)
				  {
				    if (tracker.selective_stat2[selection])
				      printf("adding %s to stats\n",proposal_names[selection]); 
				    else 
				      printf("removing %s to stats\n",proposal_names[selection]); 
				  }
			    }
			  }
// 			  
		    }*/
		    
		    
		    /*
		    if (keys[SDL_SCANCODE_F2]&&(!keystates[SDL_SCANCODE_F2]))
		    {
		      mark_point(1);  
		    }

		    if (keys[SDL_SCANCODE_F3]&&(!keystates[SDL_SCANCODE_F3]))
		    {
		      mark_point(2);  
		    }*/

		    
		    
		    if (keys[SDL_SCANCODE_1]&&(!keystates[SDL_SCANCODE_1]))
		    {
			render_mode=static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_DEFAULT);
		    }
		    
		    if (keys[SDL_SCANCODE_2]&&(!keystates[SDL_SCANCODE_2]))
		    {
			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_CONNECTED);
		    }
	
		     if (keys[SDL_SCANCODE_3]&&(!keystates[SDL_SCANCODE_3]))
		    {
			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_LINE);
		    }
		    
		    if (keys[SDL_SCANCODE_4]&&(!keystates[SDL_SCANCODE_4]))
		    {
			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_WORKER);
		    }
		    
		    if (keys[SDL_SCANCODE_5]&&(!keystates[SDL_SCANCODE_5]))
		    {
			render_mode=render_mode^static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_ONLINE);
		    }

		     if (keys[SDL_SCANCODE_D]&&(!keystates[SDL_SCANCODE_D]))
		    {
		      for (int i=0;i<tracker_p.size();i++)
		      {
			  sta_assert_error(tracker_p[i]!=NULL);
			  CTracker<TData,T,Dim> & tracker=*tracker_p[i];
 			  class CTracker<TData,T,Dim>::User_action * action=new class CTracker<TData,T,Dim>::User_action_clear();
 			  tracker.execute(action);
			  
		      }
		    }
		    
		    
		    if (keys[SDL_SCANCODE_K]&&(!keystates[SDL_SCANCODE_K]))
		    {
		      if (call_from_main==0)
		      {
			add_info("writing debug data 2 matlab");
			call_from_main=10;
		      }
		    }
// 	
		   
		  }
	
		  
	    
		 
		  //b_down[2]=(button_state & SDL_BUTTON(SDL_BUTTON_MIDDLE));
		  
// 		  int mouse_mode=0;
		  /*
		  if (!(mouse_state.trackball_on)&&!(mouse_state.arcball_on)&&!(mouse_state.trackballZ_on)) 
		  {
		    if ((b_down[0])&&(keys[SDL_SCANCODE_S]))
		    { 
		      
		      mouse_mode=1;
		      if ((mouse_state.mouse_pos_old[0]!=mouse_pos[0])&&(mouse_state.mouse_pos_old[1]!=mouse_pos[1]))
		      {
		     
		      hittest(mouse_pos[0],mouse_pos[1]);
		      mouse_state.b_down[0]=false;
		      mouse_state.mouse_pos_old[0]=mouse_pos[0];
		      mouse_state.mouse_pos_old[1]=mouse_pos[1];	 
		      }
		    }
		  }
		  */
		  
		  
// 		  printf("%d %d\n",keys[SDL_SCANCODE_SPACE],keys[SDL_SCANCODE_LSHIFT]);
		  
		  if ((!mouse_state.transition_on))
		  {
		  for (int b=0;b<3;b++)
		  {
		    
 		  
		    
		    if (b_down[b])
		    {
		      if (!mouse_state.b_down[b]) // mouse_down_event
		      {
// 			printf("button %d down\n",b);
			//mhs::mex_dumpStringNOW();
			if (!mouse_state.is_busy())
			{
			  switch(b)
			  {
			    case 0:
			    {
			      //if (keys[SDL_SCANCODE_S])
			      //if (((keys[SDL_SCANCODE_SPACE]&&(!keystates[SDL_SCANCODE_SPACE])))||(keys[SDL_SCANCODE_LSHIFT]&&(!keystates[SDL_SCANCODE_LSHIFT])))
			      if ((keys[SDL_SCANCODE_LCTRL])||(keys[SDL_SCANCODE_LSHIFT]))
			      {
				if (keys[SDL_SCANCODE_LCTRL])
				{
				  add_info("particle cursor: on");
				  mouse_state.point_select_mode=0;
				}
				else 
				{
				  add_info("data cursor: on");
				  mouse_state.point_select_mode=1;
				}
				mouse_state.point_select_on=true;
				mouse_state.mouse_pos_old[0]=-1;
				mouse_state.mouse_pos_old[1]=-1;
			      }else
			      {
				mouse_state.mouse_pos_old[0]=mouse_pos[0];
				mouse_state.mouse_pos_old[1]=mouse_pos[1];	  
				mouse_state.arcball_on=true;
				Point2fT    MousePt;
	    // 		    MousePt.s.X=height-mouse_pos[1];
				MousePt.s.Y=mouse_pos[0];
				MousePt.s.X=height-mouse_pos[1];		    
				LastRot = ThisRot;										// Set Last Static Rotation 
				arcBall->click(&MousePt);	
				float tx=(mouse_pos[0])/(float(width))-0.5f;
				float ty=(mouse_pos[1])/(float(height))-0.5f;
				rot_center[0]=tx;
				rot_center[1]=ty;
				memcpy(&LastRot,&Identity3x3,sizeof(Matrix3fT));
			      }
			    }break;
			    case 1:
			    {
			      mouse_state.mouse_pos_old[0]=mouse_pos[0];
			      mouse_state.mouse_pos_old[1]=mouse_pos[1];	  
			      mouse_state.trackball_on=true;
			    }break;
			    case 2:
			    {
			      mouse_state.mouse_pos_old[0]=mouse_pos[0];
			      mouse_state.mouse_pos_old[1]=mouse_pos[1];	  
			      mouse_state.trackballZ_on=true;
			    }break;
			  }
			}
		      }else{  //  mouse_is_down
			  if (mouse_state.arcball_on)   
			  {
				Quat4fT     ThisQuat;
				Point2fT    MousePt;
				MousePt.s.Y=mouse_pos[0];
				MousePt.s.X=height-mouse_pos[1];
				arcBall->drag(&MousePt, &ThisQuat);						// Update End Vector And Get Rotation As Quaternion
				Matrix3fSetRotationFromQuat4f(&ThisRot, &ThisQuat);		// Convert Quaternion Into Matrix3fT
				Matrix3fMulMatrix3f(&ThisRot, &LastRot);				// Accumulate Last Rotation Into This One
				
				m_transform_current.OpenGL_from3x3(ThisRot.M);
				update_focus_point(focus_point);

			  }else if (mouse_state.trackball_on)   
			  {
			    float tx=(mouse_state.mouse_pos_old[0]-mouse_pos[0])/(float(width));
			    float ty=(mouse_state.mouse_pos_old[1]-mouse_pos[1])/(float(height));
			  
 			    m_transform_current.m[3][0]=ty*zoom*3;
 			    m_transform_current.m[3][1]=tx*zoom*3;
// 			    float rescale=std::max(std::max(shape[0],shape[1]),shape[2]);
// 			    m_transform_current.m[3][0]=ty*zoom*rescale/20.0f;
// 			    m_transform_current.m[3][1]=tx*zoom*rescale/20.0f;
			    update_focus_point(focus_point);
			    
			  }else if (mouse_state.trackballZ_on)   
			  {
			    {
			      
			    float ty=(mouse_state.mouse_pos_old[1]-mouse_pos[1])/(float(height));
// 			    float rescale=std::max(std::max(shape[0],shape[1]),shape[2]);
// 			    m_transform_current.m[3][2]=-ty*rescale/20.0f;
			    
			    m_transform_current.m[3][2]=-ty*3.0f;
			    update_focus_point(focus_point);
			    }
			  }else if (mouse_state.point_select_on)
			  {
			    	if ((mouse_state.mouse_pos_old[0]!=mouse_pos[0])&&(mouse_state.mouse_pos_old[1]!=mouse_pos[1]))
				{
				
				hittest(mouse_pos[0],mouse_pos[1],focus_point,(mouse_state.point_select_mode==0));
				mouse_state.mouse_pos_old[0]=mouse_pos[0];
				mouse_state.mouse_pos_old[1]=mouse_pos[1];	 
				}
			  }
		      }
		    }else if (mouse_state.b_down[b]) // mouse_up_event
		    {
// 			printf("button %d up\n",b);
			
			if (b==0)
			{
			  if (mouse_state.arcball_on)
			  {
			    mouse_state.arcball_on=false;
			    m_transform=m_transform_current*m_transform;
			    m_transform_current.Identity();
			  }
			  if ((mouse_state.point_select_on))
			  {
			    mouse_state.transition_on=true;
			    m_transform_current.Identity();
			    m_transition.Identity();
			    m_transition.OpenGL_set_tranV(focus_point*-0.2);
		    
				if ( mouse_state.point_select_mode==0) 
				{add_info("particle cursor: off");}
				else 
				{add_info("data cursor: off");}
			    
 			    //m_transform_current.OpenGL_set_tranV(focus_point*-0.1);
			    
// 			    mouse_state.point_select_on=false;
// 			    m_transform_current.Identity();
// 			    m_transform_current.OpenGL_set_tranV(focus_point*-1);
// 			    m_transform=m_transform_current*m_transform;
// 			    m_transform_current.Identity();
// 			    update_focus_point();
			  }
			}
			if (b==1)
			{
			  mouse_state.trackball_on=false;
			  m_transform=m_transform_current*m_transform;
			  m_transform_current.Identity();
			}
			if (b==2)
			{
			  mouse_state.trackballZ_on=false;
			  m_transform=m_transform_current*m_transform;
			  m_transform_current.Identity();
			}
			m_transform.OpenGL_reorthonormalize();
			//mhs::mex_dumpStringNOW();
		    } 
		    mouse_state.b_down[b]=b_down[b];
		  }
		  }
 		   sta_assert_error_m(!(cpy_keys>num_keys),cpy_keys<<" "<<num_keys);
		   memcpy(keystates,keys,sizeof(Uint8)*cpy_keys);
		    
	  
    }
    
    static const Matrix3fT   Identity3x3;
    
        bool do_vrendering;
    
      void update_projection_mat()
      {
	 float zNear=1;
    float zFar=200;
      projection_mat=Matrix<float,4>::OpenGL_glFrustum(-zoom, zoom,
		  - zoom, zoom,
		  zNear, zFar);
	    projection_mat*=Matrix<float,4>::OpenGL_glLooktAt(viewer_pos,viewer_target,viewer_up);              
      }
	
	
    CTrackerRenderer(std::size_t * existing_handle=NULL) : CSceneRenderer(existing_handle), CVrender<TData,T>()
    {

      
      this->set_window_handle(this->get_window_handle());
      
      memcpy(&LastRot,&Identity3x3,sizeof(Matrix3fT));
      memcpy(&ThisRot,&Identity3x3,sizeof(Matrix3fT));
      m_transform.Identity();
      m_transform_current.Identity();
      
      call_from_main=0;
      
      background_color=0.0f;
      font_color=1.0f;
      
      arcBall=new ArcBallT(this->height,this->width);
      viewer_pos[0]=0;
      viewer_pos[1]=0;
      viewer_pos[2]=2;
      viewer__start_pos=viewer_pos;
      viewer_target=T(0);
      viewer_up=T(0);
      viewer_up[0]=1;
      zoom=0.45;
      exit_program=false;
      pause_program=false;
      rot_center=T(0);
      do_vrendering=false;
      render_mode=static_cast<unsigned int>(RENDERER_MODE::RENDERER_MODE_DEFAULT);

      sync_tick=0;	      
      last_tick=0;
//       clip_plane_dist=10;
      clip_plane_dist=10;
      const Uint8* keys  = SDL_GetKeyboardState(&num_keys);
      printf("max number of keys: %d\n",num_keys);
      keystates= new Uint8[num_keys];
      for (int i=0;i<num_keys;i++)
	keystates[i]=0;
      
      Fkeys[0]=SDL_SCANCODE_F1;
      Fkeys[1]=SDL_SCANCODE_F2;
      Fkeys[2]=SDL_SCANCODE_F3;
      Fkeys[3]=SDL_SCANCODE_F4;
      Fkeys[4]=SDL_SCANCODE_F5;
      Fkeys[5]=SDL_SCANCODE_F6;
      Fkeys[6]=SDL_SCANCODE_F7;
      Fkeys[7]=SDL_SCANCODE_F8;
      Fkeys[8]=SDL_SCANCODE_F9;
      Fkeys[9]=SDL_SCANCODE_F10;
      Fkeys[10]=SDL_SCANCODE_F11;
      Fkeys[11]=SDL_SCANCODE_F12;
      
      shader_options=0.0f;
      
       update_projection_mat();

       img=NULL;
       text_HUD=new CGLtext<T>(this->width,this->height);
      
       for (int i=0;i<string_info_stack_mmem;i++)
	string_info_stack.push_front(""); 
       
      
       for (int i=0;i<sliceview_extents*sliceview_extents;i++)
	  sliceview_slice[i]=(i%2)==0;//(float(i)/sliceview_extents*sliceview_extents);
       
//        sliceview_slice[2*sliceview_extents+5]=1;
       
       sliceview_textureID=GenerateTextureRectangle(sliceview_slice,sliceview_extents,sliceview_extents);
//       debug_point.debug_point=NULL;
//       debug_point.debug_life=-1;
      
      try
	{
	    {
	    std::stringstream s;
	    s<<shader_path<<"model_render_shader.cg";
	    tracker_data_p->_cgprogram->disableProgram(CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER);
	    tracker_data_p->_cgprogram-> Load_Program(_gcMyVertexProgram,s.str(),CGCprogram::EPROGRAM_TYPE::VERTEX_SHADER);
	    _normalmat= cgGetNamedParameter(_gcMyVertexProgram, "normalmat");
// 	    _worldview = cgGetNamedParameter(_gcMyVertexProgram, "worldView");
	    _modelview= cgGetNamedParameter(_gcMyVertexProgram, "modelview");
	    _projection= cgGetNamedParameter(_gcMyVertexProgram, "projection");
	     _focuspointV= cgGetNamedParameter(_gcMyVertexProgram, "focuspoint");
	     _vertex_mode= cgGetNamedParameter(_gcMyVertexProgram, "vertex_mode");
	   
	     
	     
	    }
	    {
	    std::stringstream s;
	    s<<shader_path<<"model_render_pshader.cg";
	    tracker_data_p->_cgprogram->disableProgram(CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER);
 	    tracker_data_p->_cgprogram-> Load_Program(_gcMyPixelProgram,s.str(),CGCprogram::EPROGRAM_TYPE::PIXEL_SHADER);
	     _clipping_plane= cgGetNamedParameter(_gcMyPixelProgram, "clipping_plane"); 
// 	     _focuspointP= cgGetNamedParameter(_gcMyPixelProgram, "focuspoint");
	     
	      
// 	    _modelview = cgGetNamedParameter(_gcMyVertexProgram, "modelView");
// 	    _worldview = cgGetNamedParameter(_gcMyVertexProgram, "worldView");
	    }
	    if (!PrintlastCGerror())
	      printf("sucessfully loaded the shader programs\n");
	    else 
	    {
	    
	      throw 0;
	    } 
		  
	}catch (...)
	{
		
		printf("error loading the shader programs\n");
		
	}
	  PrintlastCGerror();
      
      
    }
    
    ~CTrackerRenderer()
    {
      delete arcBall;
      delete [] keystates;
       std::cerr<<"deleting cgprograms"<<std::endl;
	if (tracker_data_p!=NULL)
	{
	  cgDestroyProgram ( _gcMyVertexProgram );
	}
	if (text_HUD!=NULL)
	  delete text_HUD;
	
      glDeleteTextures(1,&sliceview_textureID);	
    }
    
    bool is_exit_program()
    {
      return exit_program;
    }
    
    void send_exit_program_signal()
    {
      exit_program=true;
    }
    
    bool is_pause_program()
    {
      return pause_program;
    }


    void init(const mxArray * feature_struct)
    {
       if (feature_struct==NULL)
            return;
      
      try {
	
      CVrender<TData,T>::init(feature_struct);
      do_vrendering=true;
//       clip_plane_dist=0;//std::max(std::max(shape[0],shape[1]),shape[2]);
//       clip_plane_dist=std::max(std::max(shape[0],shape[1]),shape[2]);
      } catch (mhs::STAError error)
	{
	 do_vrendering=false;
	 printf("volume rendering disabled: %s\n",error.str().c_str());
	}
	
	
	try {
	
	  clip_plane_dist=1;
	  update_focus_point(focus_point);
      
	  img=mhs::dataArray<TData>(feature_struct,"img").data;
      }  catch (mhs::STAError error)
	{
	 printf("error, missing raw image: %s\n",error.str().c_str());
	
	}
	
	
// 	set_shape(std::size_t shape[]);
	
	//mhs::mex_dumpStringNOW();
	update_tracker_controls(true);
    }
    
    
    
    void update_tracker_controls(bool force_update=false)
    {

	mhs::mex_dumpStringNOW();
	const mxArray * handle=mexGetVariablePtr("global", "tracker_window_control");
	if (handle!=NULL)
	{
	  const mxArray *changed= mxGetField(handle,0,(char *)("changed"));
	  if (changed!=NULL)
	  {
	      double * handle_p=(double  *)mxGetPr(changed);
	      if (((*handle_p)==1)||(force_update))
	      {
		(*handle_p)=2;
		printf("controls have been updated\n");
		
		
		const mxArray *
		parameter= mxGetField(handle,0,(char *)("volume_pixelDensityThreshold"));
		if (parameter!=NULL) this->parameter_pixelDensityThreshold=*(double*) mxGetPr(parameter);
		
		parameter= mxGetField(handle,0,(char *)("volume_pixelDensity"));
		if (parameter!=NULL) this->parameter_pixelDensity=*(double*) mxGetPr(parameter);
		
		parameter= mxGetField(handle,0,(char *)("volume_bgc"));
		if (parameter!=NULL) {
		  this->background_color[0]=*((double*) mxGetPr(parameter));
		  this->background_color[1]=*(((double*) mxGetPr(parameter))+1);
		  this->background_color[2]=*(((double*) mxGetPr(parameter))+2);
		  font_color=1-std::max(std::max(background_color[0],background_color[1]),background_color[2]);
		};
		
		parameter= mxGetField(handle,0,(char *)("volume_lighting"));
		if (parameter!=NULL) {
		  if (do_vrendering)
		  {
		  this->set_light(*((double*) mxGetPr(parameter)),
			    *(((double*) mxGetPr(parameter))+1),
			    *(((double*) mxGetPr(parameter))+2),
			    *(((double*) mxGetPr(parameter))+3));
		  }
		};		
// 		printf("this->parameter_volume_pixelDensity %f\n",this->parameter_pixelDensity);
// 		printf("this->parameter_pixelDensityThreshold %f\n",this->parameter_pixelDensityThreshold);
	      }
// 	      else 
// 		if ((*handle_p)==0)
// 	      {
// 		const mxArray *parameter= mxGetField(handle,0,(char *)("volume_pixelDensity"));
// 		if (parameter!=NULL) *(double*) mxGetPr(parameter)=this->parameter_pixelDensity;
// 		
// 	      }
	  }
	  changed= mxGetField(handle,0,(char *)("debug_changed"));
	  if (changed!=NULL)
	  {
	      double * handle_p=(double  *)mxGetPr(changed);
	      if ((*handle_p)==1)
	      {
		(*handle_p)=2;
		printf("debug controls have been updated\n");
		
		
		const mxArray *
		parameter= mxGetField(handle,0,(char *)("tracker_temp"));
		if (parameter!=NULL) 
		{
		  for (int i=0;i<tracker_p.size();i++)
		  {
		    sta_assert_error(tracker_p[i]!=NULL);
		    CTracker<TData,T,Dim> & tracker=*tracker_p[i];
		      tracker.opt_temp=*(double*) mxGetPr(parameter);
		  }
		}
		
		
		
		
		parameter= mxGetField(handle,0,(char *)("edgecost"));
		if (parameter!=NULL) 
		{
		  for (int i=0;i<tracker_p.size();i++)
		  {
		      sta_assert_error(tracker_p[i]!=NULL);
		      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
		      sta_assert_error(tracker.data_fun!=NULL);
		      tracker.edgecost_fun->read_controls(handle);
		  }
		}
		
		
		 
		
		parameter= mxGetField(handle,0,(char *)("tracker_data"));
		if (parameter!=NULL) 
		{
		  for (int i=0;i<tracker_p.size();i++)
		  {
		      sta_assert_error(tracker_p[i]!=NULL);
		      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
		      sta_assert_error(tracker.data_fun!=NULL);
		      tracker.data_fun->read_controls(handle);
// 		  }  
// 		  
// 		   for (int i=0;i<tracker_p.size();i++)
// 		    {
// 			sta_assert_error(tracker_p[i]!=NULL);
// 			CTracker<TData,T,Dim> & tracker=*tracker_p[i];
// 			sta_assert_error(tracker.data_fun!=NULL);
// 			tracker.options.particle_connection_state_cost_scale[0]=*(((double*) mxGetPr(parameter))+1);
// 			tracker.options.particle_connection_state_cost_scale[1]=tracker.options.particle_connection_state_cost_scale[0];
// 			tracker.options.particle_connection_state_cost_scale[2]=tracker.options.particle_connection_state_cost_scale[0];
// 			printf("<- costs %f %f %f\n",tracker.options.particle_connection_state_cost_scale[0],
// 			  tracker.options.particle_connection_state_cost_scale[1],
// 			  tracker.options.particle_connection_state_cost_scale[2]
// 			);
		    }
		}
		
// 		parameter= mxGetField(handle,0,(char *)("volume_pixelDensity"));
// 		if (parameter!=NULL) this->parameter_pixelDensity=*(double*) mxGetPr(parameter);
// 		
// 		parameter= mxGetField(handle,0,(char *)("volume_bgc"));
// 		if (parameter!=NULL) {
// 		  this->background_color[0]=*((double*) mxGetPr(parameter));
// 		  this->background_color[1]=*(((double*) mxGetPr(parameter))+1);
// 		  this->background_color[2]=*(((double*) mxGetPr(parameter))+2);
// 		  font_color=1-std::max(std::max(background_color[0],background_color[1]),background_color[2]);
// 		};
// 		
// 		parameter= mxGetField(handle,0,(char *)("volume_lighting"));
// 		if (parameter!=NULL) {
// 		  if (do_vrendering)
// 		  {
// 		  this->set_light(*((double*) mxGetPr(parameter)),
// 			    *(((double*) mxGetPr(parameter))+1),
// 			    *(((double*) mxGetPr(parameter))+2),
// 			    *(((double*) mxGetPr(parameter))+3));
// 		  }
// 		};		
	      }
	  }	  
	  

	}
    }
    
    void update_tracker_gui(int depth=0)
    {
        if (depth>1)
	{
	  printf("called to often\n"); 
	  return;
	}
	mhs::mex_dumpStringNOW();
	const mxArray * handle=mexGetVariablePtr("global", "tracker_window_control");
	if (handle!=NULL)
	{
	  const mxArray *
	  parameter= mxGetField(handle,0,(char *)("tracker_temp"));
	  if (parameter!=NULL) 
	  {
	    double tmp=0;
	    for (int i=0;i<tracker_p.size();i++)
	    {
	      sta_assert_error(tracker_p[i]!=NULL);
	      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
		tmp=std::max(tracker.opt_temp,tmp);
	    }
	    *(double*) mxGetPr(parameter)=tmp;
	  }
	  
	  
	  for (int i=0;i<tracker_p.size();i++)
	  {
	      sta_assert_error(tracker_p[i]!=NULL);
	      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
	      sta_assert_error(tracker.data_fun!=NULL);
	      tracker.data_fun->set_controls(handle);
	  }
	  
	  
	  for (int i=0;i<tracker_p.size();i++)
	  {
	      sta_assert_error(tracker_p[i]!=NULL);
	      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
	      sta_assert_error(tracker.edgecost_fun!=NULL);
	      tracker.edgecost_fun->set_controls(handle);
	  }
	  
	  parameter= mxGetField(handle,0,(char *)("tracker_data"));
	  if (parameter!=NULL) 
	  {
	  for (int i=0;i<tracker_p.size();i++)
	  {
	      sta_assert_error(tracker_p[i]!=NULL);
	      CTracker<TData,T,Dim> & tracker=*tracker_p[i];
	      sta_assert_error(tracker.data_fun!=NULL);
// 	      printf("-> costs %f %f %f\n",tracker.options.particle_connection_state_cost_scale[0],
// 		      tracker.options.particle_connection_state_cost_scale[1],
// 		      tracker.options.particle_connection_state_cost_scale[2]
// 		    );
// 	      *(((double*) mxGetPr(parameter))+1)=tracker.options.particle_connection_state_cost_scale[0];
	      
// 	      tracker.particle_connection_state_cost_scale[1]=tracker.particle_connection_state_cost_scale[1];
// 	      tracker.particle_connection_state_cost_scale[2]=tracker.particle_connection_state_cost_scale[1];
	  }
	  }
	  
	  
	   
		
// 	  if (parameter!=NULL) this->parameter_pixelDensityThreshold=*(double*) mxGetPr(parameter);
	  
	  mexCallMATLAB(0, NULL,0,NULL, "mhs_update_gui");
	}else{
	 printf("no tracker gui data avaliable, starting gui"); 
	 mexCallMATLAB(0, NULL,0,NULL, "start_gui");
	 update_tracker_gui(depth++);
	}
	
    }
    
  };
  
  template<typename TData,typename T, int Dim>
  const Matrix3fT CTrackerRenderer<TData,T,Dim>::Identity3x3={  1.0f,  0.0f,  0.0f,					// NEW: Last Rotation
                             0.0f,  1.0f,  0.0f,
                             0.0f,  0.0f,  1.0f };
			     

#endif

#endif

