#version 130

uniform mat4 glsl_projection;
uniform mat4 glsl_modelview;
uniform mat4 glsl_normalmat;

uniform vec3 glsl_focuspoint;
uniform vec2 glsl_vertex_mode;
uniform float glsl_colorness_min=0.0;
uniform float glsl_alpha=1.0;

uniform float glsl_dist_clip=0; 

// out gl_PerVertex
// {
//   vec4 gl_Position;
// //   float gl_PointSize;
// //   float gl_ClipDistance[];
// };


varying vec4 vColor;
varying float colorness;
varying float specular_w;
varying vec3 vNormal;
varying vec3 vPosition;
varying float dist_trafo;

void main (void)  
{     

  vColor=gl_Color;
  //vColor.w = 1.0;
  
    dist_trafo = vColor.w;
    vColor.w = 1.0;
    
    
    
    
   // wdist = 50 * dist_trafo;
    
    
      //float wdist = max(1.0-(dist_trafo - glsl_dist_clip),0.0);
     
     
    

  vec4 mtraf=glsl_modelview*gl_Vertex;
  
  
  vec3 diff=mtraf.xyz-glsl_focuspoint;
  float dist_sqr=sqrt(dot(diff,diff))*glsl_vertex_mode.y/400.0;
  
  specular_w=1;
  colorness=1.0;  
  if (abs(glsl_vertex_mode.x-1.0)<0.0001)
  {
      if (dist_sqr>0.2)
      {
	colorness=0.0;
      }
      else
      {
	if (glsl_alpha<1.0)
	{
	  colorness=0.0;
	} else 
	{
	  colorness=max(0.01,1.0-dist_sqr*10.0);
	}
	mtraf.xyz=diff*max(2.0/(dist_sqr*8.0+1.0),1.0)+glsl_focuspoint;
      }
      colorness=max(glsl_colorness_min,colorness);
      
  } 
  if (glsl_colorness_min<0)
  {
     colorness=-glsl_colorness_min;
     //vColor.x=-glsl_colorness_min;
     //vColor.y=-glsl_colorness_min;
     //vColor.z=-glsl_colorness_min;
     specular_w=0;
     
  }
  if (glsl_alpha<1.0)
  {
    vColor.w=glsl_alpha;
  }
    
  
  
  
  
   
  gl_Position  = glsl_projection*mtraf;
  
  
  
    if (glsl_dist_clip>0)
    {
        //float wdist = max(1.0-(dist_trafo - glsl_dist_clip)/glsl_dist_clip,0.0);
        
        
        //float wdist = max(1.0-(dist_trafo - glsl_dist_clip)/glsl_dist_clip,0.0);
        
        
        float full_vis_dist = (1-glsl_dist_clip);
        
        {
            float alpha  = 0.0 + sqrt(full_vis_dist)*0.8;
            if ((1-dist_trafo)>alpha*full_vis_dist)
            {
                float w = max(full_vis_dist - (1-dist_trafo),0)/((1-alpha)*full_vis_dist);
                //float dist = min((0.8*(1-glsl_dist_clip))/(1-dist_trafo+0.0000001),1);
                
                //vColor.xyz *=  w;
                vColor.xyz =  (w * vColor.xyz)+(1-w);
                
                //wdist  = 0.5*(1+(1-glsl_dist_clip))*wdist*wdist*0.01*sin(20*wdist);
            
            }
        }
        /*
        {
            const float alpha  = 0.5;
            if ((1-dist_trafo)>alpha*full_vis_dist)
            {
                float w = max(full_vis_dist - (1-dist_trafo),0)/((1-alpha)*full_vis_dist);
                
                float wdist  = 0.5*(1+w)*w*0.01*sin(20*w);
                gl_Position.x+= wdist*cos(10*gl_Position.x);
                gl_Position.y+= wdist*cos(10*gl_Position.y);
                gl_Position.z+= wdist*cos(10*gl_Position.z);
            }
        }
        */
        
        
        //float wdist = max(1.0-(dist_trafo - glsl_dist_clip)/glsl_dist_clip,0.0);
        float wdist = max(1.0-(dist_trafo*1.1 - glsl_dist_clip)/glsl_dist_clip,0.0);
        wdist  = 0.5*(1+(1-glsl_dist_clip))*wdist*wdist*0.01*sin(20*wdist);
        gl_Position.x+= wdist*cos(10*gl_Position.x);
        gl_Position.y+= wdist*cos(10*gl_Position.y);
        gl_Position.z+= wdist*cos(10*gl_Position.z);
  
  
        /*
        float wdist = 1-min(dist_trafo/(glsl_dist_clip*30),1.0);
        wdist  = wdist*wdist*0.02*sin(40*wdist);
        gl_Position.x+= wdist*cos(20*gl_Position.x);
        gl_Position.y+= wdist*cos(20*gl_Position.y);
        gl_Position.z+= wdist*cos(20*gl_Position.z);
        */
    }
  
  
  
  vPosition=gl_Position.xyz;
  
  //vPosition.x += wdist;
  
  
  
   

  vNormal= normalize(glsl_normalmat*vec4(gl_Normal,0.0)).xyz;
}     


// uniform float bla;
// 
// void main()
// {
//     gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
//     gl_Position = gl_Vertex;
//     gl_Position.x=bla;
// }
