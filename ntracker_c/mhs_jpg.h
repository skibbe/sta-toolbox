 #ifndef MHS_WRITE_PNG_H
 #define MHS_WRITE_PNG_H
 
    #ifdef _WIN64
        bool write_jpg(std::string filename,int Width,int Height,unsigned char * image_buffer, int quality=95)
      {

	   
	      return true;
      }
    #else 
      #include "jpeglib.h"
    
      bool write_jpg(std::string filename,int Width,int Height,unsigned char * image_buffer, int quality=95)
      {

	    FILE * outfile;
	    if ((outfile = fopen(filename.c_str(), "wb")) == NULL) {
		fprintf(stderr, "can't open %s\n", filename.c_str());
		return false;
	    }
	    struct jpeg_compress_struct cinfo;
	    struct jpeg_error_mgr jerr;
	    
	    cinfo.err = jpeg_std_error(&jerr);
	    jpeg_create_compress(&cinfo);
	    jpeg_stdio_dest(&cinfo, outfile);

	    cinfo.image_width = Width;      /* image width and height, in pixels */
	    cinfo.image_height = Height;
	    cinfo.input_components = 3;     /* # of color components per pixel */
	    cinfo.in_color_space = JCS_RGB; /* colorspace of input image */
	    
	    jpeg_set_defaults(&cinfo);
	    jpeg_set_quality (&cinfo, quality, true);
	    jpeg_start_compress(&cinfo, true);
	    
	    
	    
	    
	    
	    
	    JSAMPROW row_pointer;        /* pointer to a single row */
	    
	    int row_stride = Width * 3;   /* JSAMPLEs per row in image_buffer */
	    while (cinfo.next_scanline < cinfo.image_height) {
    //             row_pointer[0] =   (JSAMPROW) & image_buffer[cinfo.next_scanline * row_stride];
    //             jpeg_write_scanlines(&cinfo,&row_pointer, 1);
		
		row_pointer = (JSAMPROW) &image_buffer[(cinfo.image_height-1-cinfo.next_scanline)*row_stride];
		jpeg_write_scanlines(&cinfo, &row_pointer, 1);
	    }
	    
	      jpeg_finish_compress(&cinfo);
	      fclose(outfile);
	      jpeg_destroy_compress(&cinfo);
	      return true;
      }
    #endif  
   
 #endif