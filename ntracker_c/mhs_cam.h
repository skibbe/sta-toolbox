#ifndef MHS_CAM_H
#define MHS_CAM_H


#include "mhs_error.h"
#include "mhs_gui.h"

const char *cam_field_names[] = {
    "path_data",	
    "duration",
    "frames",
};
const std::size_t n_cam_field_names=  ( sizeof(cam_field_names) / sizeof(cam_field_names[ 0 ]) );;


template<typename TData,typename T,int Dim>
class CCam
{  
  
  typedef unsigned int index_type;

  class CCamPoint
  {
    public:	
      Vector<float,3> position;
      Vector<float,3> virtual_position;
      Vector<float,3> lookat;
//       Vector<float,3> direction;
      
      
      Quaternion<float> rotation;
      Vector<float,3> translation;
//       float zoom;
//       float clip_dist;
//       float viewer_dist;
      
      std::vector<float> options_vector;
  };
  
  
  class CMesh
  {
    public:	
       GLuint  vertex_buffer;
        GLuint  normal_buffer;
        GLuint  index_buffer;
	std::size_t  i_length;
	
	CMesh()
	{
	  i_length=0;
	  vertex_buffer=0;
	  normal_buffer=0;
	  index_buffer=0;	  
	}
	
	~CMesh()
	{
	 if (i_length>0)
	 {
	   {
            mhs_check_gl;
            glDeleteBuffers ( 1, &index_buffer );
            mhs_check_gl;
            glDeleteBuffers ( 1, &vertex_buffer );
            mhs_check_gl;
            glDeleteBuffers ( 1, &normal_buffer );
            mhs_check_gl;
            }
	 }
	  
	}
	
	   void enable_VBO ( bool normal=true )
            {
	      
	       if (i_length==0)
		 return;
	       
// 	       printf("%d %d %d %d\n",vertex_buffer,normal_buffer,index_buffer,i_length);

            glEnableClientState ( GL_VERTEX_ARRAY );
            glBindBuffer ( GL_ARRAY_BUFFER, vertex_buffer );
            glVertexPointer ( 3, GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );

            if ( normal )
                {
                glEnableClientState ( GL_NORMAL_ARRAY );
                glBindBuffer ( GL_ARRAY_BUFFER, normal_buffer );
                glNormalPointer ( GL_FLOAT, sizeof ( CTriangleObject::CVertex ), 0 );
                }
            glDisableClientState ( GL_COLOR_ARRAY );


            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, index_buffer );
            }

        void disable_VBO()
            {
	      if (i_length==0)
		 return;
            glDisableClientState ( GL_NORMAL_ARRAY );
            glDisableClientState ( GL_VERTEX_ARRAY );
            glDisableClientState ( GL_COLOR_ARRAY );

            glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
            glBindBuffer ( GL_ARRAY_BUFFER, 0 );
            }
            
         void draw()
            {
	      if (i_length==0)
		 return;
// 	    printf("%d\n",i_length);
            std::size_t offset =0;
            glDrawElements ( GL_TRIANGLES, i_length, GL_UNSIGNED_INT, ( void* ) ( offset * sizeof ( index_type ) ) );
            mhs_check_gl;
            }   

	    
  };
  
  Vector<std::size_t,3> shape;
  
  std::list<CCamPoint> campoints;
  typename std::list<CCamPoint>::iterator current_cam_pt;
  std::size_t ani_count;
  std::size_t current_key_frame;
  
  CMesh cam_model;
  CMesh line_model;
  
  mhs::CtimeStopper timer;
  double prev_time;
  bool is_flying;
  bool fly_by_time=true;
  
  double duration=5;
  std::size_t num_frames=500;
  
  CViewer<TData,T,Dim> *V;
//   Quaternion<float> current_rotation;
//   Quaternion<float> identity_rotation;
//   Vector<float,3> current_translation;
//   float current_zoom;
//   float current_history;
//   bool user_is_interacting[2];
//   Matrix<float,4> last_user_trafo;
//   float last_zoom;
  
 
  
  void  init_mesh(CTriangleObject & mesh,CMesh & cam_mesh)
   {
    
     
     
    std::size_t v_buffer_size=mesh.getNumVerts();
    std::size_t i_buffer_size=mesh.getNumIndeces();

    CTriangleObject::CVertex * tmp_buffer=new CTriangleObject::CVertex[v_buffer_size];
    CTriangleObject::CVertex * tmp_nbuffer=new CTriangleObject::CVertex[v_buffer_size];
    index_type * face_buffer=new index_type[i_buffer_size*3];

    mesh.cpy2IndexBuffer ( face_buffer,0 );
    mesh.cpy2VertexBuffer ( tmp_buffer,tmp_nbuffer ); 
    
     glGenBuffers ( 1,&cam_mesh.vertex_buffer );
      glBindBuffer ( GL_ARRAY_BUFFER, cam_mesh.vertex_buffer );
      glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_buffer, GL_STATIC_DRAW );

      glGenBuffers ( 1,&cam_mesh.normal_buffer );
      glBindBuffer ( GL_ARRAY_BUFFER, cam_mesh.normal_buffer );
      glBufferData ( GL_ARRAY_BUFFER, sizeof ( CTriangleObject::CVertex ) *v_buffer_size, tmp_nbuffer, GL_STATIC_DRAW );


      glGenBuffers ( 1,&cam_mesh.index_buffer );
      glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, cam_mesh.index_buffer );
      glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 3*sizeof ( index_type ) *i_buffer_size, face_buffer, GL_STATIC_DRAW );


      delete [] tmp_buffer;
      delete [] tmp_nbuffer;
      delete [] face_buffer;
      mhs_check_gl;

      cam_mesh.i_length=i_buffer_size*3;
      
//       return cam_mesh;
   }
  
  public:
  
  void start_flight(bool by_time=true)
  {
    current_cam_pt=campoints.begin();
    ani_count=0;
    prev_time=timer.get_total_runtime();
    is_flying=true;
    current_key_frame=0;
    fly_by_time=by_time;
//     Matrix<float,4> identity;
//     identity.Identity();
//     identity_rotation=Quaternion<float> (identity);
//     current_rotation=identity_rotation;
//     current_translation=float(0);
//     current_history=0;
//     current_zoom=1;
//     user_is_interacting[0]=user_is_interacting[1]=false;
    
  }
  
  bool cam_is_flying(){return this->is_flying;};
  
  void stop_flight()
  {
     current_cam_pt=campoints.end();
     is_flying=false;
  }
  
  
//    template<typename TData,typename T,int Dim>
  mxArray * save()//CViewer<TData,T,Dim> & V)
  { 
    
    bool has_path=campoints.size()>0;
    
    mwSize dim = 1+has_path;
    mxArray * tdata=mxCreateStructArray(1,&dim,n_cam_field_names,cam_field_names);
    
    
    
    
    
    for (int data_count=0;data_count<(has_path+1);data_count++)
    {
      if ((data_count==1)||(!has_path))
      {
	campoints.clear();
	add_point();
      }
      
	
      printf("data %d\n",data_count);
      
      if (campoints.size()>0)
      {
	std::size_t num_attributes = 16+campoints.begin()->options_vector.size();
	mwSize ndims[2];
	ndims[1]=campoints.size();
	ndims[0]=num_attributes;
	mxArray * pdata = mxCreateNumericArray ( 2,ndims,mhs::mex_getClassId<T>(),mxREAL );
	
	T *result = ( T * ) mxGetData (pdata );
	    
	std::size_t pos=0;
	for (typename std::list<CCamPoint>::iterator iter=campoints.begin();iter!=campoints.end();iter++)
	{
    //       CCamPoint & camp=*iter;
    //       for (int a=0;a<3;a++)
    // 	pos++;
    //       
    //       for (int a=0;a<3;a++)
    // 	pos++;
    //       
    //       for (int a=0;a<3;a++)
    // 	pos++;
    //       
    //       for (int a=0;a<3;a++)
    // 	pos++;
    //       
    //       for (int a=0;a<4;a++)
    // 	pos++;
    //       
    //        pos++;
	  
    //       printf("%d %d\n",pos,ndims[0]*ndims[1]);
	  CCamPoint & camp=*iter;
	  for (int a=0;a<3;a++)
	  result[pos++]=camp.position[a];
	  
	  for (int a=0;a<3;a++)
	  result[pos++]=camp.virtual_position[a];
	  
	  for (int a=0;a<3;a++)
	  result[pos++]=camp.lookat[a];
	  
	  for (int a=0;a<3;a++)
	  result[pos++]=camp.translation[a];
	  
	  for (int a=0;a<4;a++)
	  result[pos++]=camp.rotation[a];
	  
	  
	  for (int a=0;a<camp.options_vector.size();a++)
	    result[pos++]=camp.options_vector[a];
// 	  result[pos++]=camp.zoom;
// 	  
// 	  result[pos++]=camp.clip_dist;
// 	  
// 	  result[pos++]=camp.viewer_dist;
	}
	
	mxSetField(tdata,data_count,"path_data", pdata);
	mxSetField(tdata,data_count,"duration", mhs::mex_create_value<T>(duration));
	mxSetField(tdata,data_count,"frames", mhs::mex_create_value<T>(num_frames));
	printf("filed %d\n",data_count);
	
      }
    }
    
    
    
    
//     printf("ok\n");
//     mhs::mex_dumpStringNOW();
    return tdata;
    
  }
  
  
//   template<typename T>
  void load(const mxArray *  cam_data_array)
  {
    
    if (cam_data_array==NULL)
      return;
    sta_assert_error(mxIsStruct(cam_data_array));
    
      try{
	
	
	for (std::size_t i=0;i<=mxGetNumberOfElements(cam_data_array);i++)
	{
	  
	  mhs::dataArray<T> cam_data=mhs::dataArray<T>(cam_data_array,"path_data");
	  
	  sta_assert_error(cam_data.dim.size()==2);
	  
	  
	  std::size_t data_size=cam_data.dim[0]*cam_data.dim[1];
	  std::size_t  pos=0;
	  
	  std::size_t num_attributes=cam_data.dim[1];
	  
	  T * result=cam_data.data;
	  
	  while (pos<data_size)
	  {
	    CCamPoint  camp;
	      for (int a=0;a<3;a++)
		camp.position[a]=result[pos++];
	      
	      for (int a=0;a<3;a++)
		camp.virtual_position[a]=result[pos++];
	      
	      for (int a=0;a<3;a++)
		camp.lookat[a]=result[pos++];
	      
	      for (int a=0;a<3;a++)
		camp.translation[a]=result[pos++];
	      
	      for (int a=0;a<4;a++)
		camp.rotation[a]=result[pos++];
	      
	      camp.rotation.normalize();
	      
	      camp.options_vector.resize(num_attributes-16);

	      for (int a=0;a<camp.options_vector.size();a++)
		camp.options_vector[a]=result[pos++];
	      
  // 	    camp.zoom=result[pos++];
  // 	    camp.clip_dist=result[pos++];
  // 	    camp.viewer_dist=result[pos++];
	      
	      if (i==0)
	      {
		campoints.push_back(camp);
	      }else 
	      {
		V->set_options_vector(camp.options_vector);
	      }
	  }
	  
	  if (i==0)
	      {
		duration=mhs::dataArray<T>(cam_data_array,"duration").data[0];
		num_frames=mhs::dataArray<T>(cam_data_array,"frames").data[0];
	      }
	}
// 	if ((duration<0)&&((-duration)<campoints.size()))
// 	{
// 	  duration=-campoints.size();
// 	  printf("num frames was shorter than campoints");
// 	}
	
	
     } catch (mhs::STAError & error)
            {
                throw error;
            }
  }
	
	
  
  
//   template<typename TData,typename T,int Dim>
  bool fly()//CViewer<TData,T,Dim> & V)
  {

  if (fly_by_time)
  {
    
    double total_duration=this->duration/campoints.size();
    if (current_cam_pt!=campoints.end())
    {
     
      double time=(timer.get_total_runtime()-prev_time);
      time=std::min(time,total_duration);
//       printf("%f %f\n",time,total_duration);
      
      
      typename std::list<CCamPoint>::iterator previous_cam_pt=current_cam_pt;
      previous_cam_pt--;
      typename std::list<CCamPoint>::iterator next_cam_pt=current_cam_pt;
      next_cam_pt++;
      typename std::list<CCamPoint>::iterator next_next_cam_pt=next_cam_pt;
      next_next_cam_pt++;
  
      if ((next_cam_pt==campoints.end()))
      {
      
	CCamPoint & cp=*current_cam_pt;
	Matrix<float,4> trafo; 
	
	trafo=(cp.rotation).getMatrix();
	trafo.OpenGL_set_tranV(cp.translation);
	
	V->set_options_vector(cp.options_vector);
// 	float zoom=cp.zoom;
// 	float clipdist=cp.clip_dist;
// 	float viewer_dist=cp.viewer_dist;
	
// 	V->set_viewer_dist(viewer_dist);
// 	V->set_clip_plane_dist(clipdist);
// 	V->set_zoom(zoom);
	
	V->set_trafo(trafo);
      }else 
      {
	CCamPoint & cpA=*current_cam_pt;
	CCamPoint & cpB=*next_cam_pt;
	Matrix<float,4> trafo;
	
	
	float wB=(time)/(total_duration);
	Quaternion<float> rot;
	Vector<float,3> trans;
// 	float zoom;
// 	float clipdist;
// 	float viewer_dist;
	
	if ((current_cam_pt!=campoints.begin())
	  &&(next_next_cam_pt!=campoints.end()))
	{
	  CCamPoint & cpQ0=*previous_cam_pt;
	  CCamPoint & cpQ1=*current_cam_pt;
	  CCamPoint & cpQ2=*next_cam_pt;
	  CCamPoint & cpQ3=*next_next_cam_pt;
	  
	  
	  //bool force_quad=(std::sqrt((cpA.translation-cpB.translation).norm2())/std::max(std::max(shape[0],shape[1]),shape[2]))>0.05;
	  bool force_quad=(std::sqrt((cpA.translation-cpB.translation).norm2()))>0.01;
// 	  shape.print();
// 	  printf("force %d\n",force_quad);
 	  //printf("%f %d, force %d\n",std::sqrt((cpA.translation-cpB.translation).norm2()),std::max(std::max(shape[0],shape[1]),shape[2]),force_quad);
	  rot=Quaternion<float>::squad(cpQ1.rotation,cpQ2.rotation,wB,cpQ0.rotation,cpQ3.rotation,0.01,force_quad);

	  trans=(cpA.translation*(1-wB)+cpB.translation*(wB));
// 	  zoom=(cpA.zoom*(1-wB)+cpB.zoom*(wB));
// 	  clipdist=(cpA.clip_dist*(1-wB)+cpB.clip_dist*(wB));
// 	  viewer_dist=(cpA.viewer_dist*(1-wB)+cpB.viewer_dist*(wB));
	}else
	{
	  rot=Quaternion<float>::slerp(cpA.rotation,cpB.rotation,wB);
	  trans=(cpA.translation*(1-wB)+cpB.translation*(wB));
// 	  zoom=(cpA.zoom*(1-wB)+cpB.zoom*(wB));
// 	  clipdist=(cpA.clip_dist*(1-wB)+cpB.clip_dist*(wB));
// 	  viewer_dist=(cpA.viewer_dist*(1-wB)+cpB.viewer_dist*(wB));
	}
	
	std::vector<float> options;
	options.resize(cpA.options_vector.size());
	for (int a=0;a<cpA.options_vector.size();a++)
	{
	  options[a]=(cpA.options_vector[a]*(1-wB)+cpB.options_vector[a]*(wB));
	}
	
// 	trans=(cpA.translation*(1-wB)+cpB.translation*(wB));
// 	zoom=(cpA.zoom*(1-wB)+cpB.zoom*(wB));
// 	rot=Quaternion<float>::lerp(cpA.rotation,cpB.rotation,wB);
	
	
	rot.normalize();
	trafo=rot.getMatrix();
	
	
	trafo.OpenGL_set_tranV(trans);
	
// 	V->set_viewer_dist(viewer_dist);
// 	V->set_clip_plane_dist(clipdist);
// 	V->set_zoom(zoom);
	V->set_options_vector(options);
	V->set_trafo(trafo);
      }
      
      
      if (time>=total_duration)
      {
	 prev_time=timer.get_total_runtime();
	current_cam_pt++;
      }
      
//       if ((ani_count+1)%ani_length==0)
//       {
// 	current_cam_pt++;
//       }
	
	
     
//      ani_count++;
     
     return true;
    }
  }else
  {
//     if (campoints.size()<2)
//       return false;
    double total_frames=num_frames-1;
    if ((current_cam_pt!=campoints.end())&&(ani_count<=total_frames))
    {
      
      
      double progress=ani_count/total_frames;
      
      double keyframe_space=progress*(campoints.size()-1);

      
      if (ani_count<=total_frames)
      {
	
	std::size_t new_key_frame=std::floor(keyframe_space);
	
	printf("keyframe: %d %d [%d / %d]\n",ani_count+1,num_frames,new_key_frame+1,(campoints.size()));
	while (current_key_frame<new_key_frame)
	{
// 	  printf("keyframe changed\n");
	  current_cam_pt++;
	  current_key_frame++;
	}
	
// 	std::list<CCamPoint>::iterator next_cam_pt=current_cam_pt;
// 	next_cam_pt++;
	
	  typename std::list<CCamPoint>::iterator previous_cam_pt=current_cam_pt;
	  previous_cam_pt--;
	  typename std::list<CCamPoint>::iterator next_cam_pt=current_cam_pt;
	  next_cam_pt++;
	  typename std::list<CCamPoint>::iterator next_next_cam_pt=next_cam_pt;
	  next_next_cam_pt++;
	
	
	if ((next_cam_pt==campoints.end()))
	{
	  printf("last frame\n");
	  CCamPoint & cp=*current_cam_pt;
	  Matrix<float,4> trafo; 
	  
	  trafo=(cp.rotation).getMatrix();
	  trafo.OpenGL_set_tranV(cp.translation);
	  
	  
	 /* 
	  float zoom=cp.zoom;
	  float clipdist=cp.clip_dist;
	  float viewer_dist=cp.viewer_dist;
	  
	  V->set_zoom(zoom);
	 
	  V->set_clip_plane_dist(clipdist);*/
// 	  V->set_viewer_dist(viewer_dist);
	  V->set_trafo(trafo);
	  V->set_options_vector(cp.options_vector);
	}else 
	{
	  CCamPoint & cpA=*current_cam_pt;
	  CCamPoint & cpB=*next_cam_pt;
	  Matrix<float,4> trafo;
	  
	  float wB=(keyframe_space-new_key_frame);
	    Quaternion<float> rot;
	  Vector<float,3> trans;
// 	  float zoom;
// 	  float clipdist;
// 	  float viewer_dist;
	  
	  if ((current_cam_pt!=campoints.begin())
	  &&(next_next_cam_pt!=campoints.end()))
	{
	  CCamPoint & cpQ0=*previous_cam_pt;
	  CCamPoint & cpQ1=*current_cam_pt;
	  CCamPoint & cpQ2=*next_cam_pt;
	  CCamPoint & cpQ3=*next_next_cam_pt;
	  
	  bool force_quad=(std::sqrt((cpA.translation-cpB.translation).norm2()))>0.01;
	  rot=Quaternion<float>::squad(cpQ1.rotation,cpQ2.rotation,wB,cpQ0.rotation,cpQ3.rotation,0.01,force_quad);
// 	  rot=Quaternion<float>::squad(cpQ1.rotation,cpQ2.rotation,wB,cpQ0.rotation,cpQ3.rotation);
	  trans=(cpA.translation*(1-wB)+cpB.translation*(wB));
// 	  zoom=(cpA.zoom*(1-wB)+cpB.zoom*(wB));
// 	  clipdist=(cpA.clip_dist*(1-wB)+cpB.clip_dist*(wB));
// 	  viewer_dist=(cpA.viewer_dist*(1-wB)+cpB.viewer_dist*(wB));
	}else
	{
	  rot=Quaternion<float>::slerp(cpA.rotation,cpB.rotation,wB);
	  trans=(cpA.translation*(1-wB)+cpB.translation*(wB));
// 	  zoom=(cpA.zoom*(1-wB)+cpB.zoom*(wB));
// 	  clipdist=(cpA.clip_dist*(1-wB)+cpB.clip_dist*(wB));
// 	  viewer_dist=(cpA.viewer_dist*(1-wB)+cpB.viewer_dist*(wB));
	}
	  
	  
// 	  Quaternion<float> rot=Quaternion<float>::slerp(cpA.rotation,cpB.rotation,wB);
	  rot.normalize();
	  trafo=rot.getMatrix();

// 	  Vector<float,3> trans=(cpA.translation*(1-wB)+cpB.translation*(wB));
 	  trafo.OpenGL_set_tranV(trans);
	  
// 	  float zoom=(cpA.zoom*(1-wB)+cpB.zoom*(wB));
	  
	  std::vector<float> options;
	  options.resize(cpA.options_vector.size());
	  for (int a=0;a<cpA.options_vector.size();a++)
	  {
	    options[a]=(cpA.options_vector[a]*(1-wB)+cpB.options_vector[a]*(wB));
	  }
	  
// 	  V->set_zoom(zoom);
	  V->set_options_vector(options);
	  V->set_trafo(trafo);
// 	  V->set_clip_plane_dist(clipdist);
// 	  V->set_viewer_dist(viewer_dist);
	}
	
	  ani_count++;
      }
     
     
      return true;
    }
  }
    
    
    is_flying=false;
    return false;
  }
  
   
   
   
   
   
    
  CCam(Vector<std::size_t,3>  shape,CViewer<TData,T,Dim> & v)
  {
    V=&v;
    
    this->shape=shape;
    
//     CTriangleObject cylinder_particle_mesh ( CTriangleObject::object_id_cylinder,2);
    CTriangleObject sphere_particle_mesh ( CTriangleObject::object_id_sphere,2);
    CTriangleObject cylinder_particle_mesh ( CTriangleObject::object_id_cylinder,2);
     init_mesh(sphere_particle_mesh,cam_model);
     init_mesh(cylinder_particle_mesh,line_model);
     
    start_flight();
  }
  
//   void add_point(Vector<float,3> position,Vector<float,3> direction,float zoom)
//   void add_point(
//     CViewer<TData,T,Dim> & V,
//     Vector<float,3> position,
//     Vector<float,3> virtual_position,
//     Vector<float,3> lookat,
//     Vector<float,3> direction)
//   {
//     
// //     position=float(0);
//    CCamPoint pt;
//    pt.position=position;
//    pt.direction=direction;
//    pt.lookat=lookat;
//    pt.virtual_position=virtual_position;
//    pt.zoom=1;
//    
//    campoints.push_back(pt);
// //    pt.position.print();
//   }

//   template<typename TData,typename T,int Dim>
void add_point()//CViewer<TData,T,Dim> & V)
  {
    if (is_flying)
      return;
    
      #pragma omp critical (CAM_POINT_LIST_CHANGES)
      {
    
    //     position=float(0);
      CCamPoint pt;
      V->camposGL2img (pt.position,pt.virtual_position,pt.lookat);  
    //   pt.direction=(pt.position-pt.lookat);
    //   pt.direction.normalize();
      pt.rotation=Quaternion<float>(V->get_world_trafo());
      pt.translation=V->get_world_trafo().OpenGL_get_tranV();
//       pt.zoom=V->get_zoom();
//       pt.clip_dist=V->get_clip_plane_dist();
//       pt.viewer_dist=V->get_viewer_dist();
      
      pt.options_vector=V->get_options_vector();
      
      campoints.push_back(pt);
      }
//    pt.position.print();
  }
  
  void remove_point()
  {
    if (is_flying)
      return;
    
    #pragma omp critical (CAM_POINT_LIST_CHANGES)
      {
    
	if (campoints.size()>0)
	{
	  campoints.pop_back();
	  current_cam_pt=campoints.end();
	}
      }

  }
  
  void draw(Matrix<float,4> & modelview_mat,Matrix<float,4> & normal_mat,GLuint glsl_modelview,GLuint glsl_normalmat)
  {
    if (is_flying)
      return;
//     shader.bind();
    if (campoints.size()<1)
      return;
    
    float rescale=1.0/std::max ( std::max ( shape[0],shape[1] ),shape[2] );
//     float scale=5*rescale;
    float scale=0.5*rescale;
    
    Matrix<float,4> model_trafo_rotate;
    model_trafo_rotate.Identity();
    
    cam_model.enable_VBO();
    
    glColor3f(1,1,1);
    
//     glUniformMatrix4fv(glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat).v));	
//         glUniformMatrix4fv(glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,scale))).v));	
//         glUniformMatrix4fv(glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (normal_mat*model_trafo_rotate).v));
//       cam_model.draw();
//      
//       if (false)
    for (typename std::list<CCamPoint>::iterator iter=campoints.begin();iter!=campoints.end();iter++)
    {
      
      CCamPoint & camp=*iter;
// 			Vector<float,3> & n=camp.direction;
// 			Vector<float,3> rot_ax;
//                         if ( ! ( n.norm1() >0 ) ) {
//                             continue;
//                         }
//                         sta_assert_error ( n.norm2() >0 );
//                         rot_ax[0]=n[1];
//                         rot_ax[1]=-n[0];
//                         rot_ax.normalize();
//                         float angle=-180*std::acos ( n[2] ) /M_PI;
		Vector<float,3> direction=camp.lookat-camp.virtual_position;
		direction.normalize();
		Vector<float,3> position=camp.virtual_position+direction*(0.05/rescale);	
			
 		      glUniformMatrix4fv(glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(position*rescale)*model_trafo_rotate*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,scale))).v));
		      glUniformMatrix4fv(glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (normal_mat*model_trafo_rotate).v));
		      
		      cam_model.draw();
		      
    }
    
     glColor3f(1,0.7,0.7);
    
     scale=1.5*rescale;
    for (typename std::list<CCamPoint>::iterator iter=campoints.begin();iter!=campoints.end();iter++)
    {
      
      CCamPoint & camp=*iter;
// 			Vector<float,3> & n=camp.direction;
// 			Vector<float,3> rot_ax;
//                         if ( ! ( n.norm1() >0 ) ) {
//                             continue;
//                         }
//                         sta_assert_error ( n.norm2() >0 );
//                         rot_ax[0]=n[1];
//                         rot_ax[1]=-n[0];
//                         rot_ax.normalize();
//                         float angle=-180*std::acos ( n[2] ) /M_PI;
			
		
      
      
 		      glUniformMatrix4fv(glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(camp.virtual_position*rescale)*model_trafo_rotate*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,scale))).v));
		      glUniformMatrix4fv(glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (normal_mat*model_trafo_rotate).v));
		      
		      cam_model.draw();
		      
    }
   
    
     
//      shader.unbind();

  cam_model.disable_VBO();
  
  
  
  
  line_model.enable_VBO();  
  scale=0.5*rescale;

    for (typename std::list<CCamPoint>::iterator iter=campoints.begin();iter!=campoints.end();iter++)
    {
      
//       glColor3f(0.9,0.7,1);
      glColor3f(0.1,1,1);
      CCamPoint & camp=*iter;
      
      //Vector<float,3> & posB=camp.virtual_position;
      Vector<float,3> direction=camp.lookat-camp.virtual_position;
      direction.normalize();
      Vector<float,3> posA=camp.virtual_position+direction*(0.05/rescale);
      
      Vector<float,3> & posB=camp.virtual_position; 
      
      Vector<float,3> position=(posA+posB)/2;
      Vector<float,3> dir=(posA-posB);
      float length=std::sqrt(dir.norm2())/2;
      if (length>0.000000001)
      {
      
	    dir.normalize();
      
      
      
			Vector<float,3> & n=dir;
			Vector<float,3> rot_ax;
			rot_ax=float(0);
                        if ( ! ( n.norm1() >0 ) ) {
                            continue;
                        }
                        sta_assert_error ( n.norm2() >0 );
                        rot_ax[0]=n[1];
                        rot_ax[1]=-n[0];
                        rot_ax.normalize();
                        float angle=-180*std::acos ( n[2] ) /M_PI;
			
			model_trafo_rotate=Matrix<float,4>::OpenGL_glRotate(angle,rot_ax);
      
      
	  glUniformMatrix4fv(glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(position*rescale)*model_trafo_rotate*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,length*rescale))).v));
	    glUniformMatrix4fv(glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (normal_mat*model_trafo_rotate).v));    
		      
		      line_model.draw();
      }
      
      
      typename std::list<CCamPoint>::iterator next_iter=iter;
      next_iter++;
      if (next_iter!=campoints.end())
      {
	  glColor3f(1,0.7,0.7);
	CCamPoint & campA=*iter;
	CCamPoint & campB=*next_iter;
      Vector<float,3> & posA=campA.virtual_position; 
      Vector<float,3> & posB=campB.virtual_position;
      Vector<float,3> position=(posA+posB)/2;
      Vector<float,3> dir=(posA-posB);
      float length=std::sqrt(dir.norm2())/2;
      if (length>0.000000001)
      {
      
	    dir.normalize();
      
      
      
			Vector<float,3> & n=dir;
			Vector<float,3> rot_ax;
			rot_ax=float(0);
                        if ( ! ( n.norm1() >0 ) ) {
                            continue;
                        }
                        sta_assert_error ( n.norm2() >0 );
                        rot_ax[0]=n[1];
                        rot_ax[1]=-n[0];
                        rot_ax.normalize();
                        float angle=-180*std::acos ( n[2] ) /M_PI;
			
			model_trafo_rotate=Matrix<float,4>::OpenGL_glRotate(angle,rot_ax);
      
      
	  glUniformMatrix4fv(glsl_modelview, 1, GL_FALSE,( GLfloat * )((modelview_mat*Matrix<float,4>::OpenGL_glTranslate(position*rescale)*model_trafo_rotate*Matrix<float,4>::OpenGL_glScale(Vector<float,3>(scale,scale,length*rescale))).v));
	    glUniformMatrix4fv(glsl_normalmat, 1, GL_FALSE,( GLfloat * )( (normal_mat*model_trafo_rotate).v));    
		      
		      line_model.draw();
      }
	
      }
      
//       CCamPoint & camp=*iter;
// 		glEnable ( GL_LINE_STIPPLE );
//                 glColor3fv ( this->particle_colors[CSceneRenderer::Corange] );
//                 glBegin ( GL_LINE );
//                 glVertex3f ( left,top,back );
//                 glVertex3f ( left,top,front );
//                 glEnd();
//                 glPolygonMode ( GL_FRONT_AND_BACK,GL_FILL );
//                 glDisable ( GL_LINE_STIPPLE );
//                 glEnable ( GL_CULL_FACE );
		      
		      
    }     
     
  line_model.disable_VBO();     
    
		
		
     
  }
  
};









#endif








