#ifndef PROPOSAL_MOVE_H
#define PROPOSAL_MOVE_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CMove : public CProposal<TData,T,Dim>
{
protected:
 
      const static int max_stat=10;
		int statistic[max_stat];  
		
      T lambda;		
      bool consider_birth_death_ratio;
      bool use_saliency_map;
	bool sample_scale_temp_dependent;		
//       mhs::dataArray<TData>  saliency_map;
//      double saliency_correction[3];
     	 
     bool temp_dependent;
     bool move_anisotrope;
public:
  
     T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_particle_update(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  
  
    CMove(): CProposal<TData,T,Dim>()
    {
      
       
      for (int i=0;i<max_stat;i++)
	  statistic[i]=0;
      
//       saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
      
       
	temp_dependent=true;
	lambda=100;
	move_anisotrope=true;
	consider_birth_death_ratio=false;
	use_saliency_map=false;
    }
    
    ~CMove()
    {
      
//   printf("\n");
// 	for (int i=0;i<max_stat;i++)
// 	  printf("%d ",statistic[i]);
// 	printf("\n");
    }

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_MOVE);
    };
    
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_MOVE);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        if (params==NULL)
            return;
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	
	if (mhs::mex_hasParam(params,"move_anisotrope")!=-1)
            move_anisotrope=mhs::mex_getParam<bool>(params,"move_anisotrope",1)[0];
	
	if (mhs::mex_hasParam(params,"consider_birth_death_ratio")!=-1)
            consider_birth_death_ratio=mhs::mex_getParam<bool>(params,"consider_birth_death_ratio",1)[0];	
	
	   if (mhs::mex_hasParam(params,"use_saliency_map")!=-1)
            use_saliency_map=mhs::mex_getParam<bool>(params,"use_saliency_map",1)[0];
	   
	           if (mhs::mex_hasParam(params,"lambda")!=-1)
            lambda=mhs::mex_getParam<T>(params,"lambda",1)[0];
	   
	   
	   	   	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];  
    }


    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

//         try {
// 	  saliency_map=mhs::dataArray<TData>(feature_struct,"saliency_map");
//         } catch (mhs::STAError error)
//         {
//             throw error;
//         }
//         
//         saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;
// 	CData<T,TData,Dim> & data_fun=*(this->tracker->data_fun);	
// 	
// 	std::size_t real_shape[3];
// 	std::size_t shape[3];
// 	std::size_t offset[3];
// 	data_fun.get_real_shape(real_shape);
// 	data_fun.getshape(shape);
// 	data_fun.getoffset(offset);
// 	TData * p_saliency_map=saliency_map.data;
// 
// 	for (int z=offset[0];z<offset[0]+shape[0];z++)
// 	{
// 	  for (int y=offset[1];y<offset[1]+shape[1];y++)
// 	  {
// 	    for (int x=offset[2];x<offset[2]+shape[2];x++)
// 	    {
// 	      
// 	      std::size_t index=(z*real_shape[1]+y)*real_shape[2]+x;
// 	      
// 	      saliency_correction[0]=std::min(saliency_correction[0],double(p_saliency_map[index]));
// 	      saliency_correction[1]=std::max(saliency_correction[1],double(p_saliency_map[index]));
// 	      saliency_correction[2]+=p_saliency_map[index];
// 	    }
// 	  }
// 	}

    }

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	this->proposal_called++;	
	
	OctPoints<T,Dim> *  cpoint;
	cpoint=tree.get_rand_point();
	if (cpoint==NULL)
	{
	  return false;
	}
	
int stat_count=0;
//0	
statistic[stat_count++]++;	  
      

	Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	
	if (point.isfreezed())
            {
		return false;
	    }
	
	// do not touch bifurcation centers !
	if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		 return false;

	
          if (sample_scale_temp_dependent && ((temp>point.get_scale())))
	  {
		return false; 
	  }

// printf("E\n");
      
	/*
	if (return_statistic)	
	{
	    std::size_t center=(std::floor(point.position[0])*shape[1]
	      +std::floor(point.position[1]))*shape[2]
	      +std::floor(point.position[2]);
	  static_data[center*2*numproposals+12]+=1;
	}
	*/
	
	Vector<T,Dim> new_pos;
	
	const T & scale=point.get_scale();

	T temp_fact_pos=scale;
	if (temp_dependent)
	  temp_fact_pos*=proposal_temp_trafo(temp,scale,T(1));
	
	
	
	
	  //temp_fact_pos=std::sqrt(temp)*point.scale;
	
	
// 	Vector<T,Dim> new_pos_dir=;
if (move_anisotrope)
{
	Vector<T,3> vn[3];
	vn[0]=point.get_direction();
	mhs_graphics::createOrths(vn[0],vn[1],vn[2]); 	
	//vn[0].rand_normal(2*temp_fact_pos);
// 	vn[0].rand_normal(temp_fact_pos/2);
// 	vn[1].rand_normal(temp_fact_pos);
// 	vn[2].rand_normal(temp_fact_pos);
	vn[0]*=mynormalrand(temp_fact_pos/2);
	vn[1]*=mynormalrand(temp_fact_pos);
	vn[2]*=mynormalrand(temp_fact_pos);
	
	
	new_pos=vn[0]+vn[1]+vn[2];
}else
{
	new_pos.rand_normal(temp_fact_pos);
}	


// 	T displacement=0;
// 	if (collision_fun_p.is_soft())   
// 	{
// 	  displacement=std::sqrt(new_pos.norm2());
// 	}
	
	/*
	if (Points<T,Dim>::thickness<0)
	  new_pos.rand_normal(-temp_fact_pos*point.scale*Points<T,Dim>::thickness);
	else 
	  new_pos.rand_normal(temp_fact_pos*Points<T,Dim>::thickness);		*/    
	
	new_pos+=point.get_position();
	
//1
statistic[stat_count++]++;			  		
	
	if ((new_pos[0]<0)||(new_pos[1]<0)||(new_pos[2]<0)||
	  (!(new_pos[0]<shape[0]))||(!(new_pos[1]<shape[1]))||(!(new_pos[2]<shape[2])))
	{
	    return false;
	}
	
//2
statistic[stat_count++]++;			  		
	  
	if ((point.get_position()-new_pos).norm2()<0.0000001)
	{
	    return false;
	}

	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
	T old_cost=point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      inrange);
// if (inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE))	
// {
//     printf("%d \n",static_cast<int>(inrange));
// }
	sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),
	  (point.e_cost(edgecost_fun,
			       options.connection_bonus_L,
			      options.bifurcation_bonus_L,
			      inrange,true,true))
	);
	
	
	T oldenergy_data=point.point_cost;
	Vector<T,Dim> old_pos=point.get_position();
	
	T particle_interaction_cost_old=0;
	T particle_interaction_cost_new=0;
	
	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	class Collision<T,Dim>::Candidates cand(query_buffer);
	
	if (collision_fun_p.is_soft())    
	{
	
	 
	  //sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,displacement+collision_search_rad,&cand,true,displacement));
	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp));
	}
	
	//NOTE: debug collision sorting
// 	 class OctPoints<T,Dim> * query_buffer2[query_buffer_size];
//  	  class Collision<T,Dim>::Candidates cand2(query_buffer2);
//  	  T particle_interaction_cost_old2=0;
//  	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old2, tree,point,collision_search_rad,&cand2,true));
	    
	
// 	T collision_finite_costs=0;
// 	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
// 	class OctPoints<T,Dim> ** candidates=NULL;
//         std::size_t  found=0;
	
// 	if (Points<T,Dim>::collision_two_steps>0)
// 	{
// 	  sta_assert_error((!colliding( tree,point,collision_search_rad,collision_finite_costs)));
// 	}
	    
	    sta_assert_error(point.has_owner(0));
	    
	    point.set_position(new_pos);
	    
	    
 	    
	    if (!point.update_pos(true))
	    {
	    
// 	      printf("000000002\n");
	      point.set_position(old_pos);
// 	      /*sta_assert_error(point.update_pos());*/
	      
	      return false;
	    }
	    
	    point.update_pos();
 	   
    
	

	// collision cost
// 	if (Points<T,Dim>::collision_two_steps>0)	    
// 	{
// 	  T collision_finite_costs_old=0;
// 	  if (colliding( tree,point,collision_search_rad,collision_finite_costs_old))
// 	  {
// 	    point.position=old_pos;
// 	    point.update_pos();
// 	    return false;
// 	  }
// 	  collision_finite_costs=collision_finite_costs_old-collision_finite_costs;
// 	}else
	    bool out_of_bounce=false;
	{
	 
//   	  printf("00\n");
	  bool check_for_bif=point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
//   	  printf("01\n");
//3
	  
/*printf("F\n");	*/  
statistic[stat_count++]++;			  		  
	  if (out_of_bounce)
	  {
	    point.set_position(old_pos);
	    sta_assert_error(point.update_pos());
	    point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
	    sta_assert_debug0(!out_of_bounce);
	    return false;
	  }
	  
//4
statistic[stat_count++]++;			  		  
	  
	  if (options.bifurcation_particle_hard && check_for_bif)
	  {
	      for (int i=0;i<2;i++) 
	      {
		  if ((point.endpoint_connections[i]==1) && 
		     (point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
		  {
		    sta_assert_debug0(point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);  
		    
		    Points<T,Dim> & bif_center_point=*(point.endpoints[i][0]->connected->point);
		    
		      if (collision_fun_p.colliding( tree,bif_center_point,collision_search_rad,temp))
		      {
			point.set_position(old_pos);
			sta_assert_error(point.update_pos());
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
			return false;
		      }
		  }
	      }
	  }
//5
statistic[stat_count++]++;			  		  



  //NOTE: debug collision sorting
//   {
//     if (collision_fun_p.is_soft())
//     {
//       T eA=0;
//       T eB=0;
//       
//       bool checkA=(collision_fun_p.colliding(eA,tree,point,collision_search_rad,&cand,false));
//       bool checkB=(collision_fun_p.colliding(eB,tree,point,collision_search_rad));
//     
// 
//       if (((!checkA)||(!checkB))) 
//       {
// 	if ((checkA!=checkB)||(std::abs(eA-eB)>std::numeric_limits<T>::epsilon()))
// 	{
// 	  printf("%d %d / %f %f / %d / %5.20f\n",checkA,checkB,eA,eB,cand.found,std::abs(eA-eB));
// 	  
// 	  //printf("%f %f \n",old_scale,new_scale);
// 	}
//       }
//       
//       //if ((10+cand.found)!=(cand2.found))
// //       if ((cand.found+10)<(cand2.found))
// //       {
// // 	printf("%d %d\n",cand.found,cand2.found);
// //       }
//   
//   
//     }
//   }	
	  
	  if (collision_fun_p.colliding(particle_interaction_cost_new, tree,point,collision_search_rad,temp))
	  //if (collision_fun_p.colliding(particle_interaction_cost_new, tree,point,collision_search_rad,&cand,!(collision_fun_p.is_soft())))
	  {
	    point.set_position(old_pos);
	    sta_assert_error(point.update_pos());
	    point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
	    return false;
	  }
	  
	  
	}
			
//6
statistic[stat_count++]++;			  	    

	T newpointsaliency=0;
// 	if (!data_fun.interp3(newpointsaliency,
// 	saliency_map.data, 
// 	point.position,
// 	shape))
	 if ((!data_fun.saliency_get_value(newpointsaliency,point.get_position()))||((newpointsaliency<std::numeric_limits<T>::min())))
	{
		      point.set_position(old_pos);
		      sta_assert_error(point.update_pos());
		      point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
		      return false;
	}
	
	
	
//7
statistic[stat_count++]++;			  		
	T newenergy_data;
	if (!data_fun.eval_data(
	      newenergy_data,
	      point))
// 	      point.get_direction(),
// 	      point.get_position(),
// 		point.get_scale()))
	      {
		      point.set_position(old_pos);
		      sta_assert_error(point.update_pos());
		      point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
		  return false;
		}

		    
	T old_saliency=point.saliency;
	point.saliency=newpointsaliency;///saliency_correction[2];
	// edge cost			
	T new_cost=point.e_cost(edgecost_fun,
				options.connection_bonus_L,
				options.bifurcation_bonus_L,
				inrange);
	
//8
statistic[stat_count++]++;			  		
	if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	{
	  point.saliency=old_saliency;
	  point.set_position(old_pos);
	  sta_assert_error(point.update_pos());
	 point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
	  return false;
	}
	

	
	
	
// 	T E=collision_finite_costs/temp;
	T E=(new_cost-old_cost)/temp;
	E+=(newenergy_data-oldenergy_data)/temp;	
	
	
	
	if (collision_fun_p.is_soft())   
	{
	    E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	}

	T R=mhs_fast_math<T>::mexp(E);
	
	
	 if (consider_birth_death_ratio && use_saliency_map)
	  {
		R*=old_saliency/(point.saliency+std::numeric_limits<T>::epsilon());
	  }		
	
//9
statistic[stat_count++]++;			  		
	
// printf("G\n");

	if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	{
	  
// printf("%f\n",new_cost-options.bifurcation_bonus_L);	  
	  
	    this->tracker->update_energy(E,static_cast<int>(get_type()));
	    this->proposal_accepted++;

	    point.point_cost=newenergy_data;
	    //point.bifurcation_center_neighbourhood_update();

// 	    printf("-%f---\n",);
// 	    old_pos.print();
// 	    new_pos.print();
	    
	    /*    
	      if (return_statistic)	
	      {
		  std::size_t center=(std::floor(point.position[0])*shape[1]
		    +std::floor(point.position[1]))*shape[2]
		    +std::floor(point.position[2]);
		static_data[center*2*numproposals+13]+=1;
	      }				
	      */
	    #ifdef _DEBUG_PARTICLE_HISTORY_
	      point.last_successful_proposal=PARTICLE_HISTORY::PARTICLE_HISTORY_MOVE;
	    #endif
	    
	    #ifdef  D_USE_GUI
	      point.touch(get_type());
	    #endif
	      
	    return true;
	}
	
// printf("H\n");	
    point.saliency=old_saliency;
    point.set_position(old_pos);
    sta_assert_error(point.update_pos());
point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
    return false;
    }
};



#endif


