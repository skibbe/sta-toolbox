


uniform mat4 glsl_worldview;
uniform mat4 glsl_modelview;
uniform mat4 glsl_textview;

uniform vec3 glsl_focuspoint;
uniform vec2 glsl_vertex_mode;

varying vec3 vTextCoord;
varying vec3 vPosition;
varying vec4 vColor;
varying float clip_depth;



void main (void)  
{
 
  gl_Position=glsl_modelview*gl_Vertex;
 
  vTextCoord=( glsl_textview*gl_Position ).xyz;
  
  
  vec4 tex_pos=gl_Position;
  
  gl_Position =  ( glsl_worldview*tex_pos );
  vPosition=tex_pos.xyz;
  clip_depth = gl_Position.z;

}