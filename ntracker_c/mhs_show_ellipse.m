%for t=0:0.1:5;v=[0,0.5,0,3,1,-3,10,1,7-t,0,0,1,0,0,4,1];hit=mhs_show_ellipse([v;v([9:end,1:8])]);view([45,-45]);pause(0.01);if (hit), break;end;end;
function hit=mhs_show_ellipse(V)
addpath ellipse/Algebraic' Separation Condition'/

c={[0.7,0,0],[0,0.7,0]};
psf=3;

figure(132);
clf;
hold on

offset_=zeros(1,16);
%offset_(1:3)=0*-0.000001*[1,1,1];
test=[sta_ellipse_collision_test(V(1,:)),sta_ellipse_collision_test(V(2,:))];



hit=test;

v=1;
plotell(V(v,:),c{v},[0,0,0],psf);
v=2;
plotell(V(v,:),c{v},V(2,1:3)-V(1,1:3),psf);

%title(['bae:',num2str(test),' m:',num2str(test0),' c++:',num2str(test2),' mold:',num2str(test1)]);


function plotell(v,color,pos,psf)
    %pos=v(1:3);
    n1=v(4:6);
    n1=n1./norm(n1);
    %n1=[0,1,1];
    %n1=n1/norm(1);
    thickness=v(7);
    scale=v(8);

    %[x y z] = sphere(16); 
[x y z] = sphere(32); 
    z=thickness*z./max(abs(z(:)));     
    y=scale*y./max(abs(y(:)));     
    x=scale*x./max(abs(x(:)));     

    [n2,n3]=createOrths(n1);
    
    
if psf>0    
    dir_depth=[1,0,0];
    n_d2=dot(dir_depth,n2);
    n_d3=dot(dir_depth,n3);
    
    
    if ((abs(n_d2)+abs(n_d3))>0.001)
      n2=n2*n_d2+n3*n_d3;
      weight_z=norm(n2);
      n2=n2/(weight_z+eps);
      n3=cross(n1,n2);
      n2=(n2*weight_z*psf)+(n2*(1-weight_z));
      
    end;
end;    
    

    Tmp=[z(:),y(:),x(:)]*[n1;n2;n3];
    %pos2=pos*[n1;n2;n3];
    x(:)=Tmp(:,1)+pos(1);
    y(:)=Tmp(:,2)+pos(2);
    z(:)=Tmp(:,3)+pos(3);

    p=surf2patch(x,y,z,z); 

    
    patch(p,'FaceColor',color);
    %v1=n1*10+pos;
    %plot3([-v1(1),v1(1)],[-v1(2),v1(2)],[-v1(3),v1(3)],'r');
    %v1=n2*10;
    %plot3([-v1(1),v1(1)],[-v1(2),v1(2)],[-v1(3),v1(3)],'r');
    %v1=n3*10;
    %plot3([-v1(1),v1(1)],[-v1(2),v1(2)],[-v1(3),v1(3)],'r');
    axis ([-10 10 -10 10 -10 10]);




function [vn1,vn2]=createOrths(v)

  if (abs(v(1))>abs(v(2)))
  
        if (abs(v(2))>abs(v(3)))
        
          vn1=cross(v,[0,0,1]);
        else
          vn1=cross(v,[0,1,0]);
        end;
  else
  
    if (abs(v(1))>abs(v(3)))
    
      vn1=cross(v,[0,0,1]);
    else
    
      vn1=cross(v,[1,0,0]);
    end;      
  end;
  vn1=vn1/norm(vn1);
  vn2=cross(vn1,v);

  