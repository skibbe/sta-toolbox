#ifndef PROPOSAL_MSR_H
#define PROPOSAL_MSR_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;

// #define DEBUG_MSR


template<typename TData,typename T,int Dim>
class CMSR : public CProposal<TData,T,Dim>
{
protected:

    const static int max_stat=10;
    int statistic[max_stat];

    T lambda;
    bool consider_birth_death_ratio;
    bool use_saliency_map;
   bool sample_scale_temp_dependent;
   
   bool msr_ignore_freeze = false;
   bool msr_ignore_freeze_keep_position = false;
   T min_saliency;
//       mhs::dataArray<TData>  saliency_map;
//      double saliency_correction[3];

    bool temp_dependent;
    //bool move_anisotrope;
public:

    T dynamic_weight (
        std::size_t num_edges,
        std::size_t num_particles,
        std::size_t num_connected_particles,
        std::size_t num_bifurcations,
        std::size_t num_terminals,
        std::size_t num_segments,
        T volume_img,
        T volume_particles
    ) {
        return CProposal<TData,T,Dim>::dynamic_weight_particle_update ( num_edges,
                num_particles,
                num_connected_particles,
                num_bifurcations,
                num_terminals,
                num_segments,
                volume_img,
                volume_particles );
    }



    CMSR() : CProposal<TData,T,Dim>() {


        for ( int i=0; i<max_stat; i++ ) {
            statistic[i]=0;
        }

//       saliency_correction[0]=std::numeric_limits< double >::max();
// 	saliency_correction[1]=std::numeric_limits< double >::min();
// 	saliency_correction[2]=0;

min_saliency = std::numeric_limits<T>::min();
        temp_dependent=true;
        lambda=100;
       // move_anisotrope=true;
        consider_birth_death_ratio=false;
        use_saliency_map=false;
    }

    ~CMSR() {

//   printf("\n");
// 	for (int i=0;i<max_stat;i++)
// 	  printf("%d ",statistic[i]);
// 	printf("\n");
    }

    std::string get_name() {
        return enum2string ( PROPOSAL_TYPES::PROPOSAL_MSR );
    };


    PROPOSAL_TYPES get_type() {
        return ( PROPOSAL_TYPES::PROPOSAL_MSR );
    }


    void set_params ( const mxArray * params=NULL ) {
        sta_assert_error ( this->tracker!=NULL );

        if ( params==NULL ) {
            return;
        }

        if ( mhs::mex_hasParam ( params,"temp_dependent" ) !=-1 ) {
            temp_dependent=mhs::mex_getParam<bool> ( params,"temp_dependent",1 ) [0];
        }

//         if ( mhs::mex_hasParam ( params,"move_anisotrope" ) !=-1 ) {
//             move_anisotrope=mhs::mex_getParam<bool> ( params,"move_anisotrope",1 ) [0];
//         }

        if ( mhs::mex_hasParam ( params,"consider_birth_death_ratio" ) !=-1 ) {
            consider_birth_death_ratio=mhs::mex_getParam<bool> ( params,"consider_birth_death_ratio",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
            use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
        }

        if ( mhs::mex_hasParam ( params,"lambda" ) !=-1 ) {
            lambda=mhs::mex_getParam<T> ( params,"lambda",1 ) [0];
        }
        
        if ( mhs::mex_hasParam ( params,"msr_ignore_freeze" ) !=-1 ) {
            msr_ignore_freeze=mhs::mex_getParam<bool> ( params,"msr_ignore_freeze",1 ) [0];
            printf("##################################\n");
            printf("msr_ignore_freeze %d!\n",msr_ignore_freeze);
            printf("##################################\n");
        }
        
        if ( mhs::mex_hasParam ( params,"msr_ignore_freeze_keep_position" ) !=-1 ) {
            msr_ignore_freeze_keep_position=mhs::mex_getParam<bool> ( params,"msr_ignore_freeze_keep_position",1 ) [0];
            printf("##################################\n");
            printf("msr_ignore_freeze_keep_position %d!\n",msr_ignore_freeze_keep_position);
            printf("##################################\n");
        }
        
              if (mhs::mex_hasParam(params,"min_saliency")!=-1)
            min_saliency=mhs::mex_getParam<float>(params,"min_saliency",1)[0];
        
        
         if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1) {
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
	 }
    }


    void init ( const mxArray * feature_struct ) {
        sta_assert_error ( this->tracker!=NULL );
        if ( feature_struct==NULL ) {
            return;
        }


    }

    bool propose() {
        pool<class Points<T,Dim> > &	particle_pool		=* ( this->tracker->particle_pool );
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=* ( this->tracker->data_fun );
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=* ( this->tracker->tree );
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			max_collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=* ( this->tracker->edgecost_fun );
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
        Collision<T,Dim> &   collision_fun_p=* ( this->tracker->collision_fun );
	bool  single_scale		=(this->tracker->single_scale);
	

        this->proposal_called++;

        OctPoints<T,Dim> *  cpoint;
        cpoint=tree.get_rand_point();
        if ( cpoint==NULL ) {
            return false;
        }

        int stat_count=0;
//0
        statistic[stat_count++]++;


        Points<T,Dim> &  point= * ( ( Points<T,Dim>* ) ( cpoint ) );

        if ( point.isfreezed() && (!msr_ignore_freeze)) {
            return false;
        }

        // do not touch bifurcation centers !
        if ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {
            return false;
        }
        
        #if defined(D_USE_GUI) && defined(DEBUG_MSR)
		    GuiPoints<T> * gps= ( ( GuiPoints<T> * ) (&point)) ;
		    if (gps->is_selected)
		    {
			printf("proposing MSR\n");
		    }
	#endif
        
	if (sample_scale_temp_dependent && ((temp>point.get_scale())))
	{
	      return false; 
	}


        bool operations[3];
        bool possible=false;
	operations[2]=false; ///NOTE if single scale it won't be set
	
        while ( !possible ) {
            for ( int i=0; i<2+(!single_scale); i++ ) {
                operations[i]=myrand ( 1 ) >0.5;
                if ( operations[i] ) {
                    possible=true;
                }
            }
        }
        
        if (msr_ignore_freeze_keep_position &&  point.isfreezed()) 
        {
            operations[0] = 0;
        }
	
    int path_id = point._path_id;
        
        
        Vector<T,Dim> old_pos=	point.get_position();  
	Vector<T,Dim> old_dir=	point.get_direction();  
	const T old_scale	=point.get_scale();
	
	T oldenergy_saliency=point.saliency;
	T oldenergy_data=point.point_cost;
	
	
	typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
        T old_cost=point.e_cost ( edgecost_fun,
                                  options.connection_bonus_L,
                                  options.bifurcation_bonus_L,
                                  inrange );

	 if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
	  {
	    ///NOTE in case of time dependent thickness, the triangle may become invalid
	    /// in this case we asing a very high cost to the triangle
	    sta_assert_error( ( inrange == CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_VIOLATES_CONSTRAINT_TRIANGLE_AREA));

	  }
	
//         sta_assert_error_c ( inrange== ( CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ),
//                              ( point.e_cost ( edgecost_fun,
//                                               options.connection_bonus_L,
//                                               options.bifurcation_bonus_L,
//                                               inrange,true,true ) )
//                            );
	
	 T old_pointcost=0;
	 if (operations[2])
	 {
	  old_pointcost=point.compute_cost3(temp);
	 }
	
	
	T particle_interaction_cost_old=0;
        T particle_interaction_cost_new=0;


	
	T bifurcation_data_old=0;
	//T bifurcation_data_old2=0;
	
      for ( int i=0; i<2; i++ ) {
	  if ( ( point.endpoint_connections[i]==1 ) &&
		  ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) ) {
	      sta_assert_debug0 ( point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );

	      Points<T,Dim> & bif_center_point=* ( point.endpoints[i][0]->connected->point );
// 	   sta_assert_error(bif_center_point.compute_data_term(data_fun,bifurcation_data_old2));
	  bifurcation_data_old+=bif_center_point.point_cost;
	  }
      }
      
//       sta_assert_error_c(std::abs(bifurcation_data_old-bifurcation_data_old2)<std::numeric_limits< T>::epsilon(),
// 	printf("check: %f %f\n",bifurcation_data_old,bifurcation_data_old2)
//       );
      
//       if ((bifurcation_data_old>0)||(bifurcation_data_old2>0))
//       {
// 	printf("check: %f %f\n",bifurcation_data_old,bifurcation_data_old2);
//       }
	  


	 #if defined(D_USE_GUI) && defined(DEBUG_MSR)
		    if (gps->is_selected)
		    {
			printf("M %d S %d R %d \n",operations[0],operations[1],operations[2]);
		    }
	#endif
        
        
        
	Vector<T,Dim> new_pos;
	Vector<T,Dim> new_dir;
	T new_scale;
	
	T temp_fact_pos=old_scale;
	T temp_fact_rot=1;
        
	//NOTE MOVE
	if ( operations[0])
	{
		

		
		if ( temp_dependent ) {
		    temp_fact_pos*=proposal_temp_trafo ( temp,old_scale,T ( 1 ) );
		}
		
		new_pos.rand_normal ( temp_fact_pos );


		new_pos+=old_pos;


		if ( ( new_pos[0]<0 ) || ( new_pos[1]<0 ) || ( new_pos[2]<0 ) ||
			( ! ( new_pos[0]<shape[0] ) ) || ( ! ( new_pos[1]<shape[1] ) ) || ( ! ( new_pos[2]<shape[2] ) ) ) {
		    return false;
		}


		if ( ( old_pos-new_pos ).norm2() <0.0000001 ) {
		    return false;
		}

		sta_assert_error ( point.has_owner ( 0 ) );

	} else
	{
	 new_pos=old_pos; 
	}
	
	
	//NOTE ROTATE
	if ( operations[1])
	{	
	  
	  
	  if (temp_dependent) {
	    temp_fact_rot=proposal_temp_trafo(temp,old_scale,T(1));
	  }

	    new_dir=random_pic_direction<T,Dim>(point.get_direction(),temp_fact_rot);
	   
	} else
	{
	 new_dir=old_dir; 
	}
	
	
	T scale_probability=1;
	
	//NOTE SCALE
	if ( operations[2])
	{	
	      T temp_fact_scale=2;
	
	      if (temp_dependent) {
		      temp_fact_scale=1+proposal_temp_trafo(temp,old_scale,T(1));
	      }
	      
	      T scale_lower;
	      T scale_upper;
	
	      if (sample_scale_temp_dependent)
	      {
		scale_lower=std::max(std::max(minscale,temp),old_scale/temp_fact_scale);
		scale_upper=std::min(maxscale,old_scale*temp_fact_scale);
		sta_assert_debug0(scale_lower<scale_upper);
	      }else
	      {
		scale_lower=std::max(minscale,old_scale/temp_fact_scale);
		scale_upper=std::min(maxscale,old_scale*temp_fact_scale);
	      }	
	    
	      new_scale=myrand(scale_lower,scale_upper);
	      
	      
	      if (temp_dependent)
		temp_fact_scale=1+proposal_temp_trafo(temp,new_scale,T(1));
	      
	      T scale_lower_new;
	      T scale_upper_new;
	      if (sample_scale_temp_dependent)
	      {
		scale_lower_new=std::max(std::max(minscale,temp),new_scale/temp_fact_scale);
		scale_upper_new=std::min(maxscale,new_scale*temp_fact_scale);
		sta_assert_debug0(scale_lower_new<scale_upper_new);
	      }else
	      {
		scale_lower_new=std::max(minscale,new_scale/temp_fact_scale);
		scale_upper_new=std::min(maxscale,new_scale*temp_fact_scale);
	      }

	      scale_probability=(scale_upper-scale_lower)/((scale_upper_new-scale_lower_new)+std::numeric_limits<T>::epsilon());

	      sta_assert_error(!(scale_probability<0));

	       
	} else
	{
	 new_scale=old_scale; 
	}
	
	
 	T max_pt_rad=std::max(maxscale,point.predict_thickness_from_scale(maxscale))+0.01;
 	T particle_collision_search_rad=0;
	particle_collision_search_rad=std::max(point.get_scale(),point.get_thickness())+ max_pt_rad;
	
	T max_dist=-1;
	if ( operations[2])
	{
	  max_dist=Points<T,Dim>::predict_outer_sphere_rad(new_scale)-Points<T,Dim>::predict_outer_sphere_rad(old_scale);
	  particle_collision_search_rad+=(max_dist>0)*max_dist;
	}			       
		     
	
	
	//
	
	///sta_assert_debug0(!(max_collision_search_rad<collision_search_rad));
	
	
	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	class Collision<T,Dim>::Candidates cand(query_buffer);
	
	if ( collision_fun_p.is_soft() ) {
	    if (!operations[0])
	    {
	      
	      ///NOTE if -9 : scale not changed, collect all colliding pts (will be the same), else  -1 : colect all pts in particle_collision_search_rad
	      sta_assert_debug0((max_dist>-1) || (!(new_scale>old_scale)));
	      sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,particle_collision_search_rad,temp,&cand,true, (max_dist<0) ? (-9) : (-1) ));

	    }else
	    {
	      
	      sta_assert_error ( !collision_fun_p.colliding ( particle_interaction_cost_old, tree,point,particle_collision_search_rad,temp ) );
	    }
        }
        
         if (options.bifurcation_particle_soft)	
	 {
	      T costs=0;
	      sta_assert_error(!point.point_neighborhood_bifurcation_collision_costs(
				   collision_fun_p,
				   tree,
				   costs,
				   max_collision_search_rad,
				   temp
				  ));
	      particle_interaction_cost_old+=costs;
	 }

	point.set_pos_dir_scale(new_pos,new_dir,new_scale);	

	//NOTE grid update only necessary in case of move
	if (operations[0])
	{
	  if ( !point.update_pos ( true ) ) {
	      point.set_pos_dir_scale ( old_pos, old_dir,old_scale );
	      return false;
	  }

	  point.update_pos();
	}
	
// 	collision_search_rad=std::max(point.get_scale(),point.get_thickness())+ max_pt_rad;
				
	//sta_assert_debug0(!(max_collision_search_rad<collision_search_rad));
	
	T bifurcation_data_new=bifurcation_data_old;

	bool check_for_bif=false;
	bool out_of_bounce=false;
        {
            check_for_bif=point.bifurcation_center_neighbourhood_update ( shape,out_of_bounce );

            if ( out_of_bounce ) {
               goto cleanup_particle_and_edges;
            }


            if ( options.bifurcation_particle_hard && check_for_bif ) {
	      bool valid=false;
                for ( int i=0; i<2; i++ ) {
                    if ( ( point.endpoint_connections[i]==1 ) &&
                            ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) ) {
                        sta_assert_debug0 ( point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );

                        Points<T,Dim> & bif_center_point=* ( point.endpoints[i][0]->connected->point );
		    valid=true;

                        if ( collision_fun_p.colliding ( tree,bif_center_point,max_collision_search_rad,temp ) ) {
                           goto cleanup_particle_and_edges;
                        }
                    }
                }
                sta_assert_error(valid);
                
            }
            
            if ( collision_fun_p.colliding ( particle_interaction_cost_new, tree,point,particle_collision_search_rad,temp,&cand,(!(collision_fun_p.is_soft())) || (operations[0]) ) )
            {
                goto cleanup_particle_and_edges;
            }
            
	    if (options.bifurcation_particle_soft && check_for_bif)	
	    {
		  T costs=0;
		  if (!point.point_neighborhood_bifurcation_collision_costs(
				      collision_fun_p,
				      tree,
				      costs,
				      max_collision_search_rad,
				      temp
				      ))
		  {
		    goto cleanup_particle_and_edges;
		  }
		  particle_interaction_cost_new+=costs;
	    }
            
            if (check_for_bif)
	    {
	      bifurcation_data_new=0;
		for ( int i=0; i<2; i++ ) {
	      if ( ( point.endpoint_connections[i]==1 ) &&
		      ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) ) {
		  sta_assert_debug0 ( point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );

		  Points<T,Dim> & bif_center_point=* ( point.endpoints[i][0]->connected->point );
		    T bif_data=0;
		    if (!bif_center_point.compute_data_term(data_fun,bif_data))
		    {
		      goto cleanup_particle_and_edges;
		    }
		    bifurcation_data_new+=bif_data;
		    bif_center_point.temp=bif_data;
	      }
	    }
	      }
            
        }
        
        {
        
        T newenergy_saliency=0;
	T newenergy_data;       

        // edge cost
        T new_cost=point.e_cost ( edgecost_fun,
                                  options.connection_bonus_L,
                                  options.bifurcation_bonus_L,
                                  inrange );

        if ( inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) {
	     goto cleanup_particle_and_edges;
        }
        
        



	if ( ( !data_fun.saliency_get_value ( newenergy_saliency,point.get_position() ) ) || ( ( newenergy_saliency<min_saliency ) ) ) {
		goto cleanup_particle_and_edges;
        }

        
        if ( !data_fun.eval_data (
                    newenergy_data,
                    point ) )
        {
		goto cleanup_particle_and_edges;
        }

        
        { 

        T E= ( new_cost-old_cost ) /temp;
        E+= ( newenergy_data-oldenergy_data ) /temp;
	E+=(bifurcation_data_new-bifurcation_data_old)/temp;
	
	
      
	if (operations[2])
	{
	  E+=(point.compute_cost3(temp)-old_pointcost)/temp;
	}


        if ( collision_fun_p.is_soft() ) {
            E+= ( particle_interaction_cost_new-particle_interaction_cost_old ) /temp;
        }
        
        #if defined(D_USE_GUI) && defined(DEBUG_MSR)
		    if (gps->is_selected)
		    {
			printf("E ecost: %f\n",( new_cost-old_cost ));
			printf("E data: %f (%f -> %f)\n",( newenergy_data-oldenergy_data ),oldenergy_data,newenergy_data);
			printf("E bif data: %f\n",(bifurcation_data_new-bifurcation_data_old));
			printf("E cost3 : %f\n",(point.compute_cost3(temp)-old_pointcost));
			printf("E softcoll : %f\n",( particle_interaction_cost_new-particle_interaction_cost_old ));
		    }
	#endif
        

        T R=mhs_fast_math<T>::mexp ( E );


        if ( operations[0] && consider_birth_death_ratio && use_saliency_map ) {
            R*=oldenergy_saliency/ ( newenergy_saliency+std::numeric_limits<T>::epsilon() );
        }

        
        
	// NOTE if scale has changed, then move and rot probs are not symmetric
	if ( operations[2])
	{
	    
	    if ( operations[0])
	    {
		T temp_fact_pos_new=new_scale;
		if ( temp_dependent ) {
		    temp_fact_pos_new*=proposal_temp_trafo ( temp,new_scale,T ( 1 ) );
		}
		scale_probability*=normal_dist(old_pos,temp_fact_pos)/(normal_dist(new_pos,temp_fact_pos_new)+std::numeric_limits<T>::epsilon());
	    }
	    
	    if ( operations[1])
	    {
		T temp_fact_rot_new=1;
		if (temp_dependent) {
		  temp_fact_rot_new=proposal_temp_trafo(temp,new_scale,T(1));
		}
		//scale_probability*=normal_dist_sphere_full(old_dir,new_dir, temp_fact_rot)/(normal_dist_sphere_full(old_dir,new_dir, temp_fact_rot_new)+std::numeric_limits<T>::epsilon());
		scale_probability*=normal_dist_sphere(old_dir,new_dir, temp_fact_rot)/(normal_dist_sphere(old_dir,new_dir, temp_fact_rot_new)+std::numeric_limits<T>::epsilon());

	    }
	    
	  
	    R*=scale_probability; 
	}

	 #if defined(D_USE_GUI) && defined(DEBUG_MSR)
		    if (gps->is_selected)
		    {
			printf("total: R %f E %f\n",R,E);
		    }
	#endif
	

        if ( R>=myrand ( 1 ) +std::numeric_limits<T>::epsilon() ) {

            this->tracker->update_energy ( E,static_cast<int> ( get_type() ) );
            this->proposal_accepted++;

	    
	     point.saliency=newenergy_saliency;
            point.point_cost=newenergy_data;
            
            sta_assert_error(path_id == point._path_id);
	    
	    if (check_for_bif)
	    {
		for ( int i=0; i<2; i++ ) {
	      if ( ( point.endpoint_connections[i]==1 ) &&
		      ( point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) ) {
		  sta_assert_debug0 ( point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );

		  Points<T,Dim> & bif_center_point=* ( point.endpoints[i][0]->connected->point );
	      
		  sta_assert_error(bif_center_point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
		  
		    bif_center_point.point_cost=  bif_center_point.temp;
		}
	      }
	    }
	    
	    #ifdef _DEBUG_PARTICLE_HISTORY_
			point.last_successful_proposal=PARTICLE_HISTORY::PARTICLE_HISTORY_MOVE;
	    #endif

	    #ifdef  D_USE_GUI
			point.touch ( get_type() );
	    #endif

            return true;
        }
	}
	}
	
       cleanup_particle_and_edges:	  
       
	  point.set_pos_dir_scale(old_pos,old_dir,old_scale);	
	  sta_assert_error ( (!operations[0]) || point.update_pos() );
	  point.bifurcation_center_neighbourhood_update ( shape,out_of_bounce );
	  sta_assert_debug0 ( !out_of_bounce );
        return false;
    }
};



#endif


