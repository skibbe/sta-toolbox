
varying vec4 vColor;
varying vec3 vNormal;
varying vec3 vPosition;


uniform float glsl_clipping_plane; 
uniform int glsl_roi_render_mode=2; 


void mylit(in float NdotL, in float NdotH, in float m, out vec2 value)
{
  float specular = (NdotL > 0.0) ? pow(max(0.0, NdotH), m) : 0.0;
  value= vec2(max(0.0, NdotL), specular);
}


void myreflect( in vec3 i, vec3 n ,out  vec3 value )
{
  value= i - 2.0 * n * dot(n,i);
}



void myrand(in vec3 co, out vec3 value){
    value.x=fract(sin(dot(co.yz ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
    value.y=fract(sin(dot(co.xz ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
    value.z=fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453)-0.5;
}


void main()
{

     if ((vPosition.z+glsl_clipping_plane)<0.0)
     {
      discard;
     }
     
     //glsl_roi_render_mode++;

   
      vec3 normal2=vNormal;
      vec3 normal;
      vec3 color;
    //if (glsl_roi_render_mode!=3)
    //if ((glsl_roi_render_mode==1)||(glsl_roi_render_mode==2))
    if ((glsl_roi_render_mode==0)||(glsl_roi_render_mode==1))
    {
      myrand(vNormal+normalize(vPosition),normal);
      normal=normalize(vNormal+0.05*normal);
      //color.xyz=vColor.xyz+0.1*normal;
      color.xyz=vColor.xyz+0.01*normal;
     }else{
     normal=vNormal;
     color.xyz=vColor.xyz;
     }
     
     //color.xyz=vColor.xyz;
     
     
     
     
     
     
      
      const vec3 viewPos=vec3( 0.0,0.0,1.0 ); 
      const float ambient=0.05;
      //const float  diffuse=1.0;
      //const float specular=1.0;
      //const float  diffuse=0.5;
      const float specular=0.5;
      const float specularN=16.0;
      const vec3 lightdir=vec3 ( 0.0,0.0,-1.0);
 
      
 	float light=0.0;
	light=dot ( normal,lightdir );
	vec3 reflection;
	myreflect ( lightdir,normal,reflection);
	
	float refl=dot ( reflection,viewPos );

	//vec4 l=lit ( light,refl,specularN );
	vec2 l;
	mylit ( light,refl,specularN,l);     
      
      
switch (glsl_roi_render_mode)       
{
    case 0:
        {
            gl_FragColor.xyz=((1.0-ambient)*l.x+ambient)*color.xyz+specular*l.y;
        }
        break;
    case 1:
        {
            float s=l.y;
            if (s>0.2)
            {
            gl_FragColor.xyz=color.xyz+0.25;
            }else
            {
            float l=(((1.0-ambient)*l.x+ambient));
            if (l > 0.8)
                gl_FragColor.xyz=color.xyz;
            else if (l > 0.6)
                gl_FragColor.xyz=color.xyz*0.8;
            else if (l > 0.4)
                gl_FragColor.xyz=color.xyz*0.3;
            else
                gl_FragColor.xyz=color.xyz*0.1;  
            }      
        }break;
    case 2:
        {
            float s=l.y;
            if (s>0.2)
            {
            gl_FragColor.xyz=color.xyz+0.25;
            }else
            {
            float l=(((1.0-ambient)*l.x+ambient));
            if (l > 0.8)
                gl_FragColor.xyz=color.xyz;
            else if (l > 0.6)
                gl_FragColor.xyz=color.xyz*0.8;
            else if (l > 0.4)
                gl_FragColor.xyz=color.xyz*0.3;
            else
                gl_FragColor.xyz=color.xyz*0.1;  
            }      
        }break;    
    case 3:
        {
            float l=(((1.0-ambient)*l.x+ambient));
            if (l<0.4)
                gl_FragColor.xyz=vec3(0.0);  
            else
            {
                gl_FragColor.xyz=vec3(1.0);  
            }
        }break;          
    case 4:
        {        
            float s=l.y;
            {
            float l=(((1.0-ambient)*l.x+ambient));
            if (l > 0.8)
                gl_FragColor.xyz=color.xyz;
            else 
                gl_FragColor.xyz=color.xyz*0.8;
            }
        }break;
}
      
      /*
if (glsl_roi_render_mode==0) 
{      

	gl_FragColor.xyz=((1.0-ambient)*l.x+ambient)*color.xyz+specular*l.y;
}else   
if ((glsl_roi_render_mode==1)||(glsl_roi_render_mode==2))
{

	float s=l.y;
	if (s>0.2)
	{
	  gl_FragColor.xyz=color.xyz+0.25;
	}else
	{
	  float l=(((1.0-ambient)*l.x+ambient));
	  if (l > 0.8)
	    gl_FragColor.xyz=color.xyz;
	  else if (l > 0.6)
	    gl_FragColor.xyz=color.xyz*0.8;
	  else if (l > 0.4)
	    gl_FragColor.xyz=color.xyz*0.3;
	  else
	    gl_FragColor.xyz=color.xyz*0.1;  
	}      
}else    if (glsl_roi_render_mode==3)
    float l=(((1.0-ambient)*l.x+ambient));
    if (l<0.4)
	    gl_FragColor.xyz=vec3(0.0);  
    else
    {
        gl_FragColor.xyz=vec3(1.0);  
    }
}else 
{
	float s=l.y;
	{
	  float l=(((1.0-ambient)*l.x+ambient));
	  if (l > 0.8)
	    gl_FragColor.xyz=color.xyz;
	  else 
	    gl_FragColor.xyz=color.xyz*0.8;
	}      
}*/
      

      {
	gl_FragColor.w=vColor.w;
      }
    
   
    
    
    
    
}
