#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dlfcn.h>
#include "engine.h"
#include "mex.h" 
#include "./ext/engCallMATLAB/engCallMATLAB.c"
#include <cstdlib>

// extern "C" void __libc_freeres(void);

typedef void (*mexFunction_t)(int nargout, mxArray *pargout [ ], int nargin, const mxArray *pargin[]);

int main(int argc, const char *argv[])

{
  
//   atexit(__libc_freeres);
    
    
  setbuf(stdout, NULL);
  
  Engine *ep;
  char buff[1024];
  int i;

    fprintf(stderr, "starting MATLAB engine\n");
  /* matlab must be in the PATH! */
   if (!(ep = engOpen("matlab -nodisplay"))) {
//   if (!(ep = engOpen(""))) {
    fprintf(stderr, "Can't start MATLAB engine\n");
    return -1;
  }
  engOutputBuffer(ep, buff, 1023);

  /* load the mex file */
  if(argc<2){
    fprintf(stderr, "Error. Give full path to the MEX file as input parameter.\n");
    return -1;
  }
  
/*  
  void *handle = dlopen(argv[1], RTLD_NOW);
  printf("%s\n",argv[1]);
  if(!handle){
    fprintf(stderr, "Error loading MEX file: %s\n", strerror(errno));
    return -1;
  }

  
  mexFunction_t mexfunction = (mexFunction_t)dlsym(handle, "mexFunction");
  if(!mexfunction){
    fprintf(stderr, "MEX file does not contain mexFunction\n");
    return -1;
  }
*/
  /* load input data - for convenience do that using MATLAB engine */
  /* NOTE: parameters are MEX-file specific, so one has to modify this*/
  /* to fit particular needs */
//   engEvalString(ep, "load input.mat");
//   mxArray *arg1 = engGetVariable(ep, "FeatureData");
//   mxArray *arg2 = engGetVariable(ep, "A");
//   mxArray *arg3 = engGetVariable(ep, "params");
//   mxArray *pargout[1] = {0};
//   const mxArray *pargin[3] = {arg1, arg2, arg3};
// 
//   
   printf("loading data\n");
//   
//   /* execute the mex function */
//   mexfunction(1, pargout, 3, pargin);


  //engEvalString(ep, "load debug.mat");
   
  engEvalString(ep, "load valgrind_debug.mat");
  //engEvalString(ep, "load debugws_params.mat");
  
  engEvalString(ep, "addpath /misc/local/data/ext_projects/neuro_tracker/ntracker_c");
  
  
  mxArray *arg1 = engGetVariable(ep, "FeatureData");
//    mxIsStruct(arg1);
  mxArray *arg2 = engGetVariable(ep, "foptions");
  mxArray *pargout[1] = {0};
  
  
mxArray *pargin[2] = {arg1, arg2};
  
  printf("executing mex func\n");
  
  mxArray * result=NULL;
  
  if (engCallMATLAB( ep, 1, &result, 2,pargin, "ntrack")>0)
    printf("failed executing func");
  
  if (result!=NULL)
  {
   if (mxIsCell(result)) 
     printf("is Cell!\n");
   if (mxIsStruct(result)) 
     printf("is Struct!\n");
    
  }
  
//   if ( mexCallMATLAB ( 0, NULL,0,NULL , "ls" ) !=0 ) {
//             printf( "coud'nt load vertex data\n" );
//         }
//   if ( mexCallMATLAB ( 0, NULL,2,pargin , "ntrack" ) !=0 ) {
//             printf( "coud'nt load vertex data\n" );
//         }
//   const mxArray *pargin[2] = {arg1, arg2};
//   mexfunction(0, NULL, 2, pargin);
  
  printf("done\n");
//   engEvalString(ep, "load debug.mat");
//   mxArray *arg1 = engGetVariable(ep, "P1");
//   mxArray *arg2 = engGetVariable(ep, "P2");
//   mxArray *arg3 = engGetVariable(ep, "P3");
//   mxArray *pargout[1] = {0};
//   const mxArray *pargin[3] = {arg1, arg2, arg3};

/*  
  printf("executing mex func\n");
  
  /* execute the mex function */
//   mexfunction(1, pargout, 3, pargin);

  
  printf("done\n");

  /* print the results using MATLAB engine */
  
  /*
  engPutVariable(ep, "result", pargout[0]);
  engEvalString(ep, "result");
  printf("%s\n", buff);
  mxDestroyArray(pargout[0]);
*/  
  engEvalString(ep, "clear all;");
//   dlclose(handle);
  engClose(ep);
  printf("\n\n\n");
  return 0;
}