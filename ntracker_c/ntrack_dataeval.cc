//#define _POOL_TEST
//#define _NUM_THREADS_ 4

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"


#include "graph_rep.h"
#include "specialfunc.h"

#include "mhs_data_sfilter.h"
#include "mhs_data_hough.h"
#include "mhs_data_medialness.h"
#include "mhs_data_hessian.h"



const int Dim=3;


//A=ntrack(FeatureData,{'opt_numiterations',1000000,'maxcontinue',1,'DataThreshold',-25,'DataScaleGrad',10,'DataScale',1,'particle_connection_state_cost_scale',[1,1,1],'DataScaleGradVessel',100,'ConnectionScale',3,'opt_alpha',0.999,'particle_thickness',-1/4,'connection_candidate_searchrad',20,'connection_bonus_L',-10});

/*!
 *
 *  MAIN FUNCTION
 *
 * */
template <typename T,typename TData>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  
    CData<T,TData,Dim> *    data_fun=NULL;
    CEdgecost<T,Dim> *      edgecost_fun=NULL;
    CTracker<TData,T,Dim> * tracker=NULL;

    int paramindx=0;
    const mxArray * feature_struct=NULL;
    const mxArray * tracker_state=NULL;
    const mxArray * params=NULL;


    if ((nrhs>0)&&(mxIsStruct(prhs[paramindx])))
        feature_struct = prhs[paramindx++];
    sta_assert(feature_struct!=NULL);

    if (nrhs>1)
        tracker_state=prhs[paramindx++];

//     if (nrhs>paramindx)
//         params=prhs[nrhs-1];

    try
    {
        
        data_fun=new CDataHessian<T,TData,Dim>();
        
        CData<T,TData,Dim> &  data_fun_p=*data_fun;
        
	data_fun_p.init(feature_struct);
	data_fun_p.set_params(params);
	
	CDataHessian<T,TData,Dim> * rdata_fun=(CDataHessian<T,TData,Dim>*)data_fun;
	
	 mhs::dataArray<TData>  tmp;
	 printf("fuck fuck %d \n",tracker_state!=NULL);	 
         tmp=mhs::dataArray<TData>(tracker_state);
	 int fdim=tmp.dim[0];
	 int npts=tmp.dim[1];
	 sta_assert_error(fdim==7);
	 printf("fdim : %d npts: %d\n",fdim,npts);	 
	 
	  int ndims[2];
	  ndims[0]=4;
	  ndims[1]=npts;
	  plhs[0] = mxCreateNumericArray(2,ndims,mhs::mex_getClassId<TData>(),mxREAL);
	  TData *results = (TData*) mxGetData(plhs[0]);
	 
	
	  Vector<T,Dim> direction;
	  Vector<T,Dim> position;
	  T radius;
	      
	 int count=0; 
	 T  result[4]; 
	 for (int a=0;a<npts;a++)
	 {
// 	  for (int i=0;i<fdim;i++) 
// 	  {
// 	   printf("%f ",tmp.data[i*npts+a]); 
// 	  }
	  position[2]=tmp.data[0*npts+a];
	  position[1]=tmp.data[1*npts+a];
	  position[0]=tmp.data[2*npts+a];
	  direction[2]=tmp.data[3*npts+a];
	  direction[1]=tmp.data[4*npts+a];
	  direction[0]=tmp.data[5*npts+a];
	  radius=tmp.data[6*npts+a];
	  
	  radius=std::max(data_fun_p.get_minscale(),(float)radius);
	  radius=std::min(data_fun_p.get_maxscale(),(float)radius);
	  
	  bool check=rdata_fun->eval_data_train(
		result,
		direction,
		position,
	        radius);
	  
	  if (!check)
	    printf("ahhhhh\n");
	  
// 	  result[0]-=radius*radius;
	  
// 	  result[0]=4*a+1;
// 	  result[1]=4*a+2;
// 	  result[2]=4*a+3;
// 	  result[3]=4*a+4;
	  for (int t=0;t<4;t++)
	  {
	    sta_assert_error(count<npts*4);
	    results[count++]=result[t];
	  }
	  
// 	  printf("\n"); 
	 }
	 

    } catch (mhs::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
    catch (...)
    {
        mexErrMsgTxt("exeption ?");
    }


    if (tracker!=NULL)
        delete tracker;
    if (data_fun!=NULL)
        delete data_fun;
    if (edgecost_fun!=NULL)
        delete edgecost_fun;
  
  
/*

    CData<T,TData,Dim> *    data_fun=NULL;

    int paramindx=0;
    const mxArray * feature_struct=NULL;
    const mxArray * params=NULL;
    const mxArray * particle_data=NULL;

    
    sta_assert(nrhs>1);

    feature_struct = prhs[0];
    particle_data = prhs[1];
    

    if (nrhs>paramindx)
        params=prhs[nrhs-1];

    try
    {
        
        CDataHessian<T,TData,Dim> data_fun;
//         data_fun.init(feature_struct);
	if (params!=NULL)
        data_fun.set_params(params);
	
	
	
// 	TData * data_p=mhs::dataArray<TData>(particle_data).data;
// 	int fdim=mhs::dataArray<TData>(particle_data).dim[0];
// 	int npts=mhs::dataArray<TData>(particle_data).dim[1];
// 	printf("fdim : %d npts: %d\n",fdim,npts);
	
	
    } catch (mhs::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
    catch (...)
    {
        mexErrMsgTxt("exeption ?");
    }*/


}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

#ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
  printf("_DEBUG_CONNECT_LOOP_EXTRA_CHECK enabled\n");
#endif


    printf("mexcfunc #params: %d\n",nrhs);
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");
    if (!mxIsStruct(prhs[0]))
        mexErrMsgTxt("error: expecting feature struct\n");

    const mxArray *img=mxGetField(prhs[0],0,(char *)("img"));
    sta_assert(img!=NULL);


    if (mxGetClassID(img)==mxDOUBLE_CLASS)
        _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
    else if (mxGetClassID(img)==mxSINGLE_CLASS)
        _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported data type\n");
}







