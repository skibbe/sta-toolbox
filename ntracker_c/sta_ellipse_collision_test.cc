#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"
#include "mhs_vector.h"

#define EPSILON 0.00000000000001
#include "ellipsoid_colldet.h"



typedef double T ;

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    if (nrhs<1)
        return;


    mhs::dataArray<T>  saliency_map;
    saliency_map=mhs::dataArray<T>(prhs[0]);

    if (saliency_map.dim.size()!=2)
        return;

    if (saliency_map.dim[0]!=16)
        return;

    T * data=saliency_map.data;

    Vector<T,3> posA(data[0],data[1],data[2]);
    Vector<T,3> dirA(data[3],data[4],data[5]);
    T tA=data[7];
    T scaleA=data[6];
    dirA.normalize();


    Vector<T,3> posB(data[8],data[9],data[10]);
    Vector<T,3> dirB(data[11],data[12],data[13]);
    T tB=data[15];
    T scaleB=data[14];
  dirB.normalize();

    bool collision=ellipsoids_coolide(
                       posB,
                       posA,
                       dirB,
                       dirA,
                       scaleB,
                       tB,
                       scaleA,
                       tA,
		       T(1.0/3.0),
		       true);  
  
//     bool collision=ellipsoids_coolide(
//                        posA,
//                        posB,
//                        dirA,
//                        dirB,
//                        scaleA,
//                        tA,
//                        scaleB,
//                        tB,true);

    if (collision)
        printf("collision!\n");

    int ndims[1];
    ndims[0]=1;
    plhs[0] = mxCreateNumericArray(1,ndims,mhs::mex_getClassId<T>(),mxREAL);
    T *result = (T*) mxGetData(plhs[0]);
    *result=collision;
}





