#ifndef MHS_DATA_H
#define MHS_DATA_H

#include "rand_help.h"
#include "specialfunc.h"
#include "particles.h"
#include <numeric>
// #include "ext/tricubic.h"


#define SUB2IND(X, Y, Z, shape)  (((Z)*(shape[1])+(Y)) *(shape[2])+(X)) 


template<typename D,typename T,typename TData>
inline bool data_interpolate_dir (
    const Vector<T,3> & pos,
    const Vector<T,3> & dir,
    const T & multi_scale,
    const TData  *  scales, 
    const TData  *  alphas,
    int num_alphas,
    D & value,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode =1,
    bool debug=false
 				)
{


    
    const bool quad=false;
    
    
	try {    
	  switch (mode)
	  {
	  
	    case 0:
	    {
	        int Z=std::floor ( pos[0]+T(0.5) );
		int Y=std::floor ( pos[1]+T(0.5) );
		int X=std::floor ( pos[2]+T(0.5) );
      		if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
			( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
		    return false;
		}
		
		value=interp_from_samplesDir(
		multi_scale,
		dir,
		scales,
                alphas+(SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx)*num_alphas,
                num_alphas,quad,debug);
	    }break;
	  
	    case 1:
	    {
		T iz=pos[0];
		T iy=pos[1];
		T ix=pos[2];
		
		int Z=std::floor ( iz );
		int Y=std::floor ( iy );
		int X=std::floor ( ix );

		if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
			( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
		    return false;
		}

		D wz= ( iz-Z );
		D wy= ( iy-Y );
		D wx= ( ix-X );
		D & g=value;
		g=0;
	    
	    
	      
	      {
		 
		  g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X  ,Y  ,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( 1-wz ) * ( 1-wy ) * ( wx )   * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X+1,Y  ,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( 1-wz ) * ( wy )   * ( 1-wx ) * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X  ,Y+1,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( 1-wz ) * ( wy )   * ( wx )   * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X+1,Y+1,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( 1-wy ) * ( 1-wx ) * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X  ,Y  ,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( 1-wy ) * ( wx )   * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X+1,Y  ,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( wy )   * ( 1-wx ) * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X  ,Y+1,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( wy )   * ( wx )   * interp_from_samplesDir ( multi_scale,dir,scales,alphas+( SUB2IND(X+1,Y+1,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );

	      }
	  }break;
	  
	  

	  }
	} catch ( mhs::STAError error ) {
            throw error;
        }
    return true;
}





template<typename D,typename T,typename TData>
inline bool data_interpolate_polynom (
    const Vector<T,3> & pos,
    const T & multi_scale,
    const TData  *  scales, // if NULL then global interpolation is used
    const TData  *  alphas,
    int num_alphas,
    D & value,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode =1)
{


    
    const bool quad=true;
//      bool quad=true;
//      mode=1;
  
//   bool quad=false;
//     mode=0;
    
    
	try {    
	  switch (mode)
	  {
	    case 0:
	    {
	        int Z=std::floor ( pos[0]+T(0.5) );
		int Y=std::floor ( pos[1]+T(0.5) );
		int X=std::floor ( pos[2]+T(0.5) );
      		if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
			( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
		    return false;
		}

		if (scales!=NULL)
		{
		   value=interp_from_samples(multi_scale,scales,alphas+(SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx)*num_alphas,num_alphas,quad);
		   
// 		   for (int a=0;a<num_alphas;a++)
// 		   {
// 		     std::size_t offset=((SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx)*num_alphas);
// 		    const TData *X=alphas+offset;
// 		   printf("%d %d | %f\n",X[a],a,num_alphas);
// 		   }
// 		   
// 		   sta_assert_error(false);
		}else
		{
		  value=eval_polynom1(multi_scale,alphas+(SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx)*num_alphas,num_alphas-1);
		}
	    }break;
	  
	    case 1:
	    {
		T iz=pos[0];
		T iy=pos[1];
		T ix=pos[2];
		
		int Z=std::floor ( iz );
		int Y=std::floor ( iy );
		int X=std::floor ( ix );

		if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
			( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
		    return false;
		}

		D wz= ( iz-Z );
		D wy= ( iy-Y );
		D wx= ( ix-X );
		D & g=value;
		g=0;
	    
	    
	      if (scales!=NULL)
	      {
		 
		  g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X  ,Y  ,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( 1-wz ) * ( 1-wy ) * ( wx )   * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X+1,Y  ,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( 1-wz ) * ( wy )   * ( 1-wx ) * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X  ,Y+1,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( 1-wz ) * ( wy )   * ( wx )   * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X+1,Y+1,Z  ,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( 1-wy ) * ( 1-wx ) * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X  ,Y  ,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( 1-wy ) * ( wx )   * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X+1,Y  ,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( wy )   * ( 1-wx ) * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X  ,Y+1,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );
		  g+= ( wz )   * ( wy )   * ( wx )   * interp_from_samples ( multi_scale,scales,alphas+( SUB2IND(X+1,Y+1,Z+1,shape) *stride+indx ) *num_alphas,num_alphas,quad );

	      }else
	      {
		  g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X  ,Y  ,Z  ,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( 1-wz ) * ( 1-wy ) * ( wx )   * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X+1,Y  ,Z  ,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( 1-wz ) * ( wy )   * ( 1-wx ) * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X  ,Y+1,Z  ,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( 1-wz ) * ( wy )   * ( wx )   * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X+1,Y+1,Z  ,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( wz )   * ( 1-wy ) * ( 1-wx ) * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X  ,Y  ,Z+1,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( wz )   * ( 1-wy ) * ( wx )   * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X+1,Y  ,Z+1,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( wz )   * ( wy )   * ( 1-wx ) * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X  ,Y+1,Z+1,shape) *stride+indx ) *num_alphas,num_alphas-1);
		  g+= ( wz )   * ( wy )   * ( wx )   * eval_polynom1 ( multi_scale,alphas+( SUB2IND(X+1,Y+1,Z+1,shape) *stride+indx ) *num_alphas,num_alphas-1);
	      }
	  }break;
	  
	  

	  }
	} catch ( mhs::STAError error ) {
            throw error;
        }
    return true;
}



template<typename T,typename TData>
class Cpyramid_img
{
  public:
  Vector<std::size_t ,3> shape;
  T scaling_fact;
  const TData * sd_data;
};

template<typename D,typename T,typename TData>
inline bool data_interpolate_multiscale (
    const Vector<T,3> & pos,
    const T & multi_scale,
    const TData  *  scales, 
    const std::vector<Cpyramid_img<T,TData> > & pyramid,
    D & value,
    std::size_t stride=1,
    std::size_t indx=0,
    int mode =1)
{


    
    const bool quad=true;
//   bool quad=false;
//   mode=0;
    
      
    
	try {    
	  switch (mode)
	  {
	    case 0:
	    {
		std::size_t num_alphas=pyramid.size();
		const TData  *  imgX[num_alphas];
		T weights[num_alphas];
		Vector<T,3> img_pos;
		for (std::size_t a=0;a<num_alphas;a++)
		{
		  weights[a]=1;
		  const Cpyramid_img<T,TData> & p=pyramid[a];
		  const Vector<std::size_t ,3> & shape=p.shape;
		  img_pos=pos/p.scaling_fact;
		  int Z=std::floor ( img_pos[0]+T(0.5) );
		  int Y=std::floor ( img_pos[1]+T(0.5) );
		  int X=std::floor ( img_pos[2]+T(0.5) );
// 		  printf("%d %d\n",a,num_alphas);
// 		  printf("%f\n",a,p.scaling_fact);
		  if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
			  ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
		    
// 		    printf("%d %d %d , %d %d %d\n",X,Y,Z,shape[0],shape[1],shape[2]);
		      return false;
		  }  
		  
		  //value=interp_from_samples(multi_scale,scales,alphas+(SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx)*num_alphas,num_alphas,quad);
		  imgX[a]=p.sd_data+(SUB2IND(X  ,Y  ,Z  ,shape.v)*stride+indx);
// 		  printf("%d %d | %f\n",*imgX[a],a,num_alphas);
		}

		value=interp_from_samples_scalespace(multi_scale,weights,scales,imgX,num_alphas,quad);
// 		sta_assert_error(false);
  
	    }break;
	  
	    case 1:
	    {
		std::size_t num_alphas=pyramid.size();
		const TData  *  imgX[8][num_alphas];
		T weights[8][num_alphas];
		Vector<T,3> img_pos;
		for (std::size_t a=0;a<num_alphas;a++)
		{
		  const Cpyramid_img<T,TData> & p=(pyramid)[a];
		  const Vector<std::size_t ,3> & shape=p.shape;
		  img_pos=pos/p.scaling_fact;
		  
		  T iz=img_pos[0];
		  T iy=img_pos[1];
		  T ix=img_pos[2];
		  
		  int Z=std::floor ( iz );
		  int Y=std::floor ( iy );
		  int X=std::floor ( ix );

		  if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
			  ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
		      return false;
		  }
		  
		  D wz= ( iz-Z );
		  D wy= ( iy-Y );
		  D wx= ( ix-X );
		
		  weights[0][a]=( 1-wz ) * ( 1-wy ) * ( 1-wx );
		  weights[1][a]=( 1-wz ) * ( 1-wy ) * ( wx )  ;
		  weights[2][a]=( 1-wz ) * ( wy )   * ( 1-wx );
		  weights[3][a]=( 1-wz ) * ( wy )   * ( wx )  ;
		  weights[4][a]=( wz )   * ( 1-wy ) * ( 1-wx );
		  weights[5][a]=( wz )   * ( 1-wy ) * ( wx )  ;
		  weights[6][a]=( wz )   * ( wy )   * ( 1-wx );
		  weights[7][a]=( wz )   * ( wy )   * ( wx )  ;
		  
		  imgX[0][a]=p.sd_data+(SUB2IND(X  ,Y  ,Z  ,shape.v)*stride+indx);
		  imgX[1][a]=p.sd_data+(SUB2IND(X+1,Y  ,Z  ,shape.v)*stride+indx);
		  imgX[2][a]=p.sd_data+(SUB2IND(X  ,Y+1,Z  ,shape.v)*stride+indx);
		  imgX[3][a]=p.sd_data+(SUB2IND(X+1,Y+1,Z  ,shape.v)*stride+indx);
		  imgX[4][a]=p.sd_data+(SUB2IND(X  ,Y  ,Z+1,shape.v)*stride+indx);
		  imgX[5][a]=p.sd_data+(SUB2IND(X+1,Y  ,Z+1,shape.v)*stride+indx);
		  imgX[6][a]=p.sd_data+(SUB2IND(X  ,Y+1,Z+1,shape.v)*stride+indx);
		  imgX[7][a]=p.sd_data+(SUB2IND(X+1,Y+1,Z+1,shape.v)*stride+indx);
		}
	      
		D & g=value;
		g=0;
		for (int i=0;i<8;i++)
		{
		  g+=interp_from_samples_scalespace(multi_scale,weights[i],scales,imgX[i],num_alphas,quad);
		}

	  }break;
	  
	  

	  }
	} catch ( mhs::STAError error ) {
            throw error;
        }
    return true;
}





template<typename D,typename T,typename TData>
inline bool data_interpolate (
    const Vector<T,3> & pos,
    D & value,
    const TData  *  img,
    const std::size_t shape[],
    std::size_t stride=1,
    std::size_t indx=0,
    int mode=1)
{

    
switch (mode)
{
   case 0:
    {
	int Z=std::floor ( pos[0]+T(0.5) );
	int Y=std::floor ( pos[1]+T(0.5) );
	int X=std::floor ( pos[2]+T(0.5) );

	if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}
	value=img[SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx];
	
    }break;
  case 1:
    {
      
   	T iz=pos[0];
	T iy=pos[1];
	T ix=pos[2];
	
	int Z=std::floor ( iz );
	int Y=std::floor ( iy );
	int X=std::floor ( ix );

	if ( ( Z+1>=shape[0] ) || ( Y+1>=shape[1] ) || ( X+1>=shape[2] ) ||
		( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
	    return false;
	}

	D wz= ( iz-Z );
	D wy= ( iy-Y );
	D wx= ( ix-X );
	D & g=value;
	g=0;
        g+= ( 1-wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( 1-wz ) * ( wy ) * ( wx ) *img[ ( ( ( Z*shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( 1-wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+Y ) *shape[2]+ ( X+1 ) ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( 1-wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+X ) *stride+indx )];
        g+= ( wz ) * ( wy ) * ( wx ) *img[ ( ( ( ( Z+1 ) *shape[1]+ ( Y+1 ) ) *shape[2]+ ( X+1 ) ) *stride+indx )];
    }break;
}

    return true;
}





template <typename T,typename TData,int Dim>
class CData
{
//   friend CData;

protected:
//     std::size_t shape[3];
    Vector<std::size_t,3> shape;
//     Vector<T,3> element_size;

    bool has_saliency_map;
    mhs::dataArray<TData>  saliency_map;
    double saliency_correction[3];

    double * saliency_map_acc_tmp;
    double saliency_map_acc_tmp_max;

    Vector<T,Dim>  saliency_element_size;
    Vector<std::size_t,Dim>  saliency_shape;

    bool use_saliency_map=false;




public:
    
 

    virtual bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const  Vector<T,Dim>& position,
        T scale
        ,const Points<T,Dim> * p_point=NULL
        //,const Points<T,Dim> & point
// 	const Points<T,TData> & point,
#ifdef D_USE_GUI
        ,typename  std::list<std::string> * debug=NULL
//         ,typename  std::list<CDebugInfo> * debug=NULL
#endif
    ) =0;

    bool eval_data (
        T & result,
        const Points<T,Dim> & point
#ifdef D_USE_GUI
        ,typename  std::list<std::string> * debug=NULL
#endif
    ) {

	const Points<T,Dim> *p_point=&point;
// 	return eval_data(result,point.get_direction(),point.get_position(),point.get_scale());
        return this->data_score ( result,point.get_direction(),point.get_position(),point.get_scale()
	//		  ,point
	,p_point
#ifdef D_USE_GUI
                                  ,debug
#endif
                                );
// 	return true;
    };

//   #ifdef D_USE_GUI
//   class CDebugInfo
//     {
//       public:
// 	T value;
// 	std::string name;
//     };
//   #endif

    virtual bool saliency_get_value ( T & newpointsaliency, Vector<T,Dim>  position ) {

        if ( has_saliency_map&& use_saliency_map ) {

            position/=saliency_element_size;

            bool success= interp3 ( newpointsaliency,
                                    saliency_map.data,
                                    position,
                                    shape.v );
            newpointsaliency/=saliency_correction[2];




            return success;
        } else


        {
            newpointsaliency=1.0/ ( shape[0]*shape[1]*shape[2] );
        }
        return true;
        {
        }
    }

    virtual bool saliency_draw ( Vector<T,Dim> & pos, std::size_t & indx ) {
        sta_assert_error ( saliency_map_acc_tmp!=NULL );

        T v= rand_pic_position ( saliency_map_acc_tmp,shape.v,pos.v,indx,1,0,true,saliency_map_acc_tmp_max );

        pos*=saliency_element_size;
//      indx=((pos[0]*shape[1])+pos[1])*shape[2]+pos[2];
        indx= ( ( std::floor ( pos[0] ) *shape[1] ) +std::floor ( pos[1] ) ) *shape[2]+std::floor ( pos[2] );
        sta_assert_error ( ! ( indx<0 ) );
        if ( indx<shape[0]*shape[1]*shape[2] ) {
            return false;
        } else {
            return true;
        }


    }


    CData() {
        saliency_element_size=T ( 1 );
        has_saliency_map=false;
        for ( int i=0; i<Dim; i++ ) {
            shape[i]=0;
        }


        saliency_map_acc_tmp=NULL;
        saliency_correction[0]=std::numeric_limits< double >::max();
        saliency_correction[1]=std::numeric_limits< double >::min();
        saliency_correction[2]=0;

        use_saliency_map=false;
//       element_size=T(1);
    }
    //virtual ~CData()=0;
    virtual ~CData() {
        if ( saliency_map_acc_tmp!=NULL ) {
            delete [] saliency_map_acc_tmp;
        }
    };

    virtual void init ( const mxArray * feature_struct ) =0;

    virtual void init_saliency ( const mxArray * feature_struct ) {
        if ( feature_struct==NULL ) {
            return;
        }

        printf ( "initializing ordinary saliency map" );
        try {
            saliency_map=mhs::dataArray<TData> ( feature_struct,"saliency_map" );
            has_saliency_map=true;
            for ( int i=0; i<3; i++ ) {
                saliency_shape[i]=saliency_map.dim[i];
                saliency_element_size[i]= ( T ) shape[i]/ ( T ) saliency_shape[i];
            }



        } catch ( mhs::STAError error ) {
	    has_saliency_map=false;
	    printf("warining: no saliency map was found\n");
            //throw error;
	    return;
        }
        std::size_t numv=saliency_shape[0]*saliency_shape[1]*saliency_shape[2];
        saliency_map_acc_tmp=new double[numv];

        sta_assert_error ( numv>1 );
        TData * p_saliency_map=saliency_map.data;

        saliency_correction[0]=std::numeric_limits< double >::max();
        saliency_correction[1]=std::numeric_limits< double >::min();
        saliency_correction[2]=0;

        for ( int z=0; z<saliency_shape[0]; z++ ) {
            for ( int y=0; y<saliency_shape[1]; y++ ) {
                for ( int x=0; x<saliency_shape[2]; x++ ) {

                    std::size_t index= ( z*saliency_shape[1]+y ) *saliency_shape[2]+x;

                    saliency_correction[0]=std::min ( saliency_correction[0],double ( p_saliency_map[index] ) );
                    saliency_correction[1]=std::max ( saliency_correction[1],double ( p_saliency_map[index] ) );
                    saliency_correction[2]+=p_saliency_map[index];
                }
            }
        }

        std::size_t count=0;
        for ( int z=0; z<saliency_shape[0]; z++ ) {
            for ( int y=0; y<saliency_shape[1]; y++ ) {
                for ( int x=0; x<saliency_shape[2]; x++ ) {

                    std::size_t index= ( z*saliency_shape[1]+y ) *saliency_shape[2]+x;

                    if ( count==0 ) {
                        saliency_map_acc_tmp[0]=p_saliency_map[index];
                    } else {
                        saliency_map_acc_tmp[count]=saliency_map_acc_tmp[count-1]+p_saliency_map[index];
                    }

                    count++;
                }
            }
        }
        saliency_map_acc_tmp_max=saliency_map_acc_tmp[numv-1];


    }

//     virtual void set_params(const mxArray * params=NULL)=0;
    virtual  void set_params ( const mxArray * params=NULL ) {
        if ( params==NULL ) {
            return;
        }
        try {
            if ( mhs::mex_hasParam ( params,"use_saliency_map" ) !=-1 ) {
                use_saliency_map=mhs::mex_getParam<bool> ( params,"use_saliency_map",1 ) [0];
            }

        } catch ( mhs::STAError & error ) {

            throw error;
        }
        if ( use_saliency_map ) {
            printf ( "using saliency map\n" );
        }

    }





//    template<typename S>
//    void getshape(S shape[])
//       {
// 	for (int i=0;i<Dim;i++)
// 	{
// 	  sta_assert(this->shape[i]>0);
// 	  shape[i]=this->shape[i];
// 	}
//       }

    virtual void getshape ( std::size_t shape[] ) {
        for ( int i=0; i<Dim; i++ ) {
            sta_assert_error ( this->shape[i]>0 );
            shape[i]=this->shape[i];
        }
    }


    virtual void getoffset ( std::size_t offset[] ) {
        for ( int i=0; i<Dim; i++ ) {
            offset[i]=0;
        }
    }


    virtual bool interp3 ( T & result,
                           TData * image,
                           const Vector<T,Dim>& position,
                           const std::size_t shape[] ) {
//         return
//             interp3D ( result,image,position,shape );
	      return 
	    data_interpolate (
		  position,
		  result,
		  image,
		  shape);
    }

#ifdef D_USE_GUI
    virtual void read_controls ( const mxArray * handle ) =0;
    virtual void set_controls ( const mxArray * handle ) =0;
#endif

    virtual float get_minscale() =0;
    virtual float get_maxscale() =0;
    virtual float get_scalecorrection() =0;

    template<typename S>
    void get_real_shape ( S shape[] ) {
        for ( int i=0; i<Dim; i++ ) {
            sta_assert ( this->shape[i]>0 );
            shape[i]=this->shape[i];
        }
    }

    template<typename S>
    void get_real_offset ( S offset[] ) {
        for ( int i=0; i<Dim; i++ ) {
            offset[i]=0;
        }
    }

};



// used in subgrid scenarios (maps to sub-volume)
template <typename T,typename TData,int Dim>
class CDataGrid: public CData<T,TData,Dim>
{

    typedef class CData<T,TData,Dim> * Tdata_pointer;
    Tdata_pointer data_master;
    Vector<std::size_t,3> sub_offset;
//   std::size_t sub_shape[3];
    Vector<std::size_t,Dim> sub_shape;

    Vector<std::size_t,Dim>  saliency_sub_shape;
    Vector<std::size_t,Dim>  saliency_sub_offset;



public:

    bool data_score (
        T & result,
        const  Vector<T,Dim>& direction,
        const  Vector<T,Dim>& position,
        T scale
        ,const Points<T,Dim> * p_point=NULL
	  //,const Points<T,Dim> & point        
#ifdef D_USE_GUI
//         ,  std::list<class CData<T,TData,Dim>::CDebugInfo> * debug=NULL
        ,  std::list<std::string> * debug=NULL
#endif
    ) {
        Vector<T,Dim> offset_position=position+sub_offset;
//       CData<T,TData,Dim> * base_pointer=this;
        return data_master->data_score ( result,direction,offset_position,scale
					,p_point        
#ifdef D_USE_GUI
                                         , debug
#endif
                                       );
    };
//   Vector<std::size_t,Dim> salie


    CDataGrid()  :  CData<T,TData,Dim>() {
        sta_assert_error ( "please register the master data term in the constructor" );
    }

    CDataGrid ( Tdata_pointer  datafun )  :  CData<T,TData,Dim>() {

        for ( int i=0; i<Dim; i++ ) {
            sub_offset[i] = 0;
            sub_shape[i] = 0;
        }

        data_master=datafun;

        data_master->getshape ( sub_shape.v );
        data_master->getshape ( this->shape.v );

    }

    //virtual ~CData()=0;
    ~CDataGrid() {};

    void init ( const mxArray * feature_struct ) {
        if ( feature_struct==NULL ) {
            return;
        }
        //printf("grid helper init %d %d %d\n",sub_shape[0],sub_shape[1],sub_shape[2]);
        try {
            data_master->init ( feature_struct );
            data_master->getshape ( sub_shape.v );
            data_master->getshape ( this->shape.v );
        } catch ( mhs::STAError error ) {
            throw error;
        }
    }

    void init_saliency ( const mxArray * feature_struct ) {

        if ( feature_struct==NULL ) {
            return;
        }

        printf ( "initializing multigrid saliency map with " );

        try {
            this->saliency_map=mhs::dataArray<TData> ( feature_struct,"saliency_map" );
            this->has_saliency_map=true;
            for ( int i=0; i<3; i++ ) {
                this->saliency_shape[i]=this->saliency_map.dim[i];
                this->saliency_element_size[i]= ( T ) this->shape[i]/ ( T ) this->saliency_shape[i];
            }

        } catch ( mhs::STAError error ) {
            //throw error;
	  this->has_saliency_map=false;
	  printf("warining: no saliency map was found\n");
	  return;
        }



        for ( int i=0; i<3; i++ ) {
            saliency_sub_shape[i] = std::ceil ( ( T ) sub_shape[i] / this->saliency_element_size[i] );
            saliency_sub_offset[i] = std::ceil ( ( T ) sub_offset[i] / this->saliency_element_size[i] );
        }

        printf ( "element size: " );
        this->saliency_element_size.print();


//  	saliency_sub_shape.print();
//  	saliency_sub_offset.print();
// 	this->saliency_shape.print();


        std::size_t numv=saliency_sub_shape[0]*saliency_sub_shape[1]*saliency_sub_shape[2];
        std::size_t numv_full=this->saliency_shape[0]*this->saliency_shape[1]*this->saliency_shape[2];
        this->saliency_map_acc_tmp=new double[numv];

        sta_assert_error ( numv>1 );
        TData * p_saliency_map=this->saliency_map.data;

        this->saliency_correction[0]=std::numeric_limits< double >::max();
        this->saliency_correction[1]=std::numeric_limits< double >::min();
        this->saliency_correction[2]=0;
	
	
	
// 	(saliency_sub_offset+saliency_sub_shape).print();
// 	(this->saliency_shape).print();
	
        for ( std::size_t z=saliency_sub_offset[0]; z<saliency_sub_offset[0]+saliency_sub_shape[0]; z++ ) {
            for ( std::size_t y=saliency_sub_offset[1]; y<saliency_sub_offset[1]+saliency_sub_shape[1]; y++ ) {
                for ( std::size_t x=saliency_sub_offset[2]; x<saliency_sub_offset[2]+saliency_sub_shape[2]; x++ ) {

                    
                    //
		    if ((z<this->saliency_shape[0])&&(y<this->saliency_shape[1])&&(x<this->saliency_shape[2]))
		    {
		      std::size_t index= ( z*this->saliency_shape[1]+y ) *this->saliency_shape[2]+x;
		      sta_assert_error ( index<numv_full );
		      this->saliency_correction[0]=std::min ( this->saliency_correction[0],double ( p_saliency_map[index] ) );
		      this->saliency_correction[1]=std::max ( this->saliency_correction[1],double ( p_saliency_map[index] ) );
		      this->saliency_correction[2]+=p_saliency_map[index];
		    }
                }
            }
        }

        std::size_t count=0;
        for ( std::size_t z=saliency_sub_offset[0]; z<saliency_sub_offset[0]+saliency_sub_shape[0]; z++ ) {
            for ( std::size_t y=saliency_sub_offset[1]; y<saliency_sub_offset[1]+saliency_sub_shape[1]; y++ ) {
                for ( std::size_t x=saliency_sub_offset[2]; x<saliency_sub_offset[2]+saliency_sub_shape[2]; x++ ) {

		  if ((z<this->saliency_shape[0])&&(y<this->saliency_shape[1])&&(x<this->saliency_shape[2]))
		  {
		  
		  std::size_t index= ( z*this->saliency_shape[1]+y ) *this->saliency_shape[2]+x;
                    sta_assert_error ( index<numv_full );

                    if ( count==0 ) {
                        this->saliency_map_acc_tmp[0]=p_saliency_map[index];
                    } else {
                        this->saliency_map_acc_tmp[count]=this->saliency_map_acc_tmp[count-1]+p_saliency_map[index];
                    }
		  }else
		  {
		    this->saliency_map_acc_tmp[count]=this->saliency_map_acc_tmp[count-1];
		  }
// 	      if (count==0)
// 	      {
// 		saliency_map_acc_tmp[0]=1;
// 	      }else
// 	      {
// 		saliency_map_acc_tmp[count]=saliency_map_acc_tmp[count-1]+1;
// 	      }
                    count++;
                }
            }
        }
        this->saliency_map_acc_tmp_max=this->saliency_map_acc_tmp[numv-1];
    };

    void set_params ( const mxArray * params=NULL ) {
        CData<T,TData,Dim>::set_params ( params );

        try {
            data_master->set_params ( params );
        } catch ( mhs::STAError error ) {
            throw error;
        }
    };





#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {
        data_master->read_controls ( handle );
    };
    void set_controls ( const mxArray * handle ) {
        data_master->set_controls ( handle );
    };
#endif


    bool interp3 ( T & result,
                   TData * image,
                   const Vector<T,Dim>& position,
                   const std::size_t shape[] ) {
        Vector<T,Dim> offset_position=position+sub_offset;
//         return 	interp3D ( result,
//                            image,
//                            offset_position,
//                            this->shape.v );
	 return 
	    data_interpolate (
		  offset_position,
		  result,
		  image,
		  this->shape.v );
	
    }

    bool saliency_get_value ( T & newpointsaliency,Vector<T,Dim> position ) {

//       sta_assert_error((this->has_saliency_map));
        if ( this->has_saliency_map && this->use_saliency_map ) {

            position/=this->saliency_element_size;
// 	 Vector<T,Dim> offset_position=position+sub_offset;

// 	bool sucess= interp3(newpointsaliency, //NOTE interp3 is already corrected
//                      this->saliency_map.data,
//                      position,
//                      sub_shape.v);

             Vector<T,Dim> offset_position=position+saliency_sub_offset;
//             bool success=interp3D ( newpointsaliency,
//                                     this->saliency_map.data,
//                                     offset_position,
//                                     this->saliency_shape.v );
	    
	    bool success=
	    data_interpolate (
		  offset_position,
		  newpointsaliency,
		  this->saliency_map.data,
		  this->saliency_shape.v);

            newpointsaliency/=this->saliency_correction[2];
            return success;
        } else {
// 	sta_assert_error(false);
            newpointsaliency=1.0/ ( sub_shape[0]*sub_shape[1]*sub_shape[2] );
            return true;
        }
    }

    bool saliency_draw ( Vector<T,Dim> & pos, std::size_t & indx ) {
        sta_assert_error ( this->saliency_map_acc_tmp!=NULL );
        T v= rand_pic_position ( this->saliency_map_acc_tmp,saliency_sub_shape.v,pos.v,indx,1,0,true,this->saliency_map_acc_tmp_max );
        
//         template<typename T,typename TData>
// T rand_pic_position(TData * img,const std::size_t shape[],T pos[],std::size_t  &center,int stride=1,int offset=0,bool subpixel=false,T maxvalue=1)

//      saliency_sub_shape.print();
//      sub_shape.print();

//      pos+=saliency_sub_offset;
        pos*=this->saliency_element_size;
        indx= ( ( std::floor ( pos[0] ) *sub_shape[1] ) +std::floor ( pos[1] ) ) *sub_shape[2]+std::floor ( pos[2] );

        if ( ! ( indx<sub_shape[0]*sub_shape[1]*sub_shape[2] ) ) {
	    Vector<T,Dim> new_shape;
	    new_shape=saliency_sub_shape;
	    new_shape*=this->saliency_element_size;
	    if (!(new_shape[0]*new_shape[1]*new_shape[2]<indx))
	    {
	     return false; 
	    }
	    
	    printf ( "WARNING: drawn out of bounce! check if value is just close to border:\n" );
            printf ( "%d %d [%d %d %d]\n",indx,sub_shape[0]*sub_shape[1]*sub_shape[2],sub_shape[0],sub_shape[1],sub_shape[2] );
	    
	    this->saliency_element_size.print();
	    saliency_sub_shape.print();
	    saliency_sub_offset.print();
            sub_shape.print();
            pos.print();
	    new_shape.print();
            indx= ( ( std::floor ( pos[0]-0.5 ) *sub_shape[1] ) +std::floor ( pos[1]-0.5 ) ) *sub_shape[2]+std::floor ( pos[2]-0.5 );
            sta_assert_error ( indx<sub_shape[0]*sub_shape[1]*sub_shape[2] );
            return false;
        } else {

            return true;
        }

    }


    float get_minscale() {
        return data_master->get_minscale();
    };
    float get_maxscale() {
        return data_master->get_maxscale();
    };
    float get_scalecorrection() {
        return data_master->get_scalecorrection();
    };

    template<typename S>
    void set_region ( S offset[],S shape [] ) {
        for ( int i=0; i<Dim; i++ ) {
            sub_offset[i] = offset[i];
            sub_shape[i] = shape[i];
            if ( ! ( this->shape[i]>=shape[i] ) ) {
                printf ( "org: %d %d %d\n", ( int ) this->shape[0], ( int ) this->shape[1], ( int ) this->shape[2] );
                printf ( "sub: %d %d %d\n", ( int ) shape[0], ( int ) shape[1], ( int ) shape[2] );
                sta_assert_error ( this->shape[i]>=shape[i] );
            }
            sta_assert_error ( this->sub_offset[i]>=0 );
        }
        printf ( "setting subregion to: [%d %d %d] [%d %d %d]\n",offset[0],offset[1],offset[2],sub_shape[0],sub_shape[1],sub_shape[2] );
    }

    void getshape ( std::size_t shape[] )
//     template<typename S>
//     void getshape(S shape[])
    {
// 	printf("grid helper shape %d %d %d\n",sub_shape[0],sub_shape[1],sub_shape[2]);
        for ( int i=0; i<Dim; i++ ) {
            sta_assert_error ( this->sub_shape[i]>0 );
            shape[i]=this->sub_shape[i];
        }
    }

    void getoffset ( std::size_t offset[] ) {
        for ( int i=0; i<Dim; i++ ) {
            offset[i]=this->sub_offset[i];
            sta_assert_error ( this->sub_offset[i]>=0 );
        }
    }




};





// for debug only. constatnt costs (disables data term)
template <typename T,typename TData,int Dim>
class CDataDummy: public CData<T,TData,Dim>
{
private:
    T min_scale;
    T max_scale;
public:

    float get_minscale() {
        return min_scale;
    };
    float get_maxscale() {
        return max_scale;
    };
    float get_scalecorrection() {
        return 1;
    };

#ifdef D_USE_GUI
    void read_controls ( const mxArray * handle ) {};
    void set_controls ( const mxArray * handle ) {};
#endif

    void  set_params ( const mxArray * params=NULL ) {};

    void init ( const mxArray * feature_struct ) {
        printf ( "WARNING: using dummy data term\n" );
        for ( int i=0; i<3; i++ ) {
            this->shape[i]=mhs::dataArray<TData> ( feature_struct,"cshape" ).data[i];
        }
        std::swap ( this->shape[0],this->shape[2] );
        printf ( "shape %d %d %d\n",this->shape[0],this->shape[1],this->shape[2] );

        const TData * scales=mhs::dataArray<TData> ( feature_struct,"scales" ).data;
        min_scale=scales[0];
        max_scale=scales[mhs::dataArray<TData> ( feature_struct,"scales" ).dim[0]-1];
    }



    bool data_score (
        T & result,
        const Vector<T,Dim>& direction,
        const Vector<T,Dim>& position,
        T scale
        ,const Points<T,Dim> * p_point=NULL
	//,const Points<T,Dim> & point
#ifdef D_USE_GUI
//         , std::list<class CData<T,TData,Dim>::CDebugInfo> * debug=NULL
        , std::list<std::string> * debug=NULL
#endif
   ) {
        int Z=std::floor ( position[0] );
        int Y=std::floor ( position[1] );
        int X=std::floor ( position[2] );

        if ( ( Z+1>=this->shape[0] ) || ( Y+1>=this->shape[1] ) || ( X+1>=this->shape[2] ) ||
                ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
            return false;
        }


        result=0;
        return true;
    };


};

#endif
