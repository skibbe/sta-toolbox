#ifndef PROPOSAL_CONNECT_H
#define PROPOSAL_CONNECT_H
#include "proposals.h"

 template<typename TData,typename T,int Dim> class CProposal;


 #ifdef  D_USE_GUI		
 
template<typename T> class	GuiPoints ;
		
#endif		
 
// #define DEBUG_CONNECT


template<typename TData,typename T,int Dim>
class CProposalConnect : public CProposal<TData,T,Dim>
{
protected:
    T Lprior;
    T ConnectEpsilon;
      const static int max_stat=10;
		int statistic[max_stat];
		int propose_statistic[max_stat];
    T non_edge_prop_fact;		
 bool sample_scale_temp_dependent;	
 
public:
  
  T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_edges(num_edges,
		      num_particles,
		      num_connected_particles,		      
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }



    CProposalConnect(): CProposal<TData,T,Dim>()
    {
        Lprior=-10;
	ConnectEpsilon=0.0001;
	for (int i=0;i<max_stat;i++)
	{
	  statistic[i]=0;
	   propose_statistic[i]=0;
	}
	non_edge_prop_fact=1;
	
	     
    }
    
    ~CProposalConnect()
    {
//             printf("\n %s:",enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT).c_str());
// 	for (int i=0;i<max_stat;i++)
// 	  printf("(%d)%d ",i,statistic[i]);
// 	printf("\n");
      
//       printf("add conn:%d / %d\n",propose_statistic[0],propose_statistic[3]);
//       printf("rem conn:%d / %d\n",propose_statistic[1],propose_statistic[4]);
//       printf("cha conn:%d / %d\n",propose_statistic[2],propose_statistic[5]);
    }
    
//     void set_tracker(typename CProposal<TData,T,Dim>::Ctrack & tracker)
//     {
//         CProposal<TData,T,Dim>::CProposal(tracker);
//     }
    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT);
    };
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_CONNECT);
    }

    void set_params(const mxArray * params=NULL)
    {
        if (params==NULL)
            return;

        if (mhs::mex_hasParam(params,"Lprior")!=-1)
            Lprior=mhs::mex_getParam<T>(params,"Lprior",1)[0];
	
	if (mhs::mex_hasParam(params,"ConnectEpsilon")!=-1)
            ConnectEpsilon=mhs::mex_getParam<T>(params,"ConnectEpsilon",1)[0];
	
	if (mhs::mex_hasParam(params,"non_edge_prop_fact")!=-1)
            non_edge_prop_fact=mhs::mex_getParam<T>(params,"non_edge_prop_fact",1)[0];
	
	if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   

    }

    void init(const mxArray * feature_struct) {};

    bool propose()
    {   
      	pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
	const T &			conn_temp		=this->tracker->opt_temp_conn_cost;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
	OctTreeNode<T,Dim> &		conn_tree		=*(this->tracker->connecion_candidate_tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
	CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
	class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Connections<T,Dim>  & connections			=this->tracker->connections;
	T & search_connection_point_center_candidate		=this->tracker->search_connection_point_center_candidate;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	
	
	this->proposal_called++;
	
	OctPoints<T,Dim> *  cpoint;
	cpoint=tree.get_rand_point();
	if (cpoint==NULL)
	  return false;

const bool debug=false;
	  
 int stat_count=0;
 //0
 statistic[stat_count++]++;	

	try {		  
	
if (debug)		  
  printf("[X");	  
	  
	Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));

		
	#if defined(D_USE_GUI)  && defined(DEBUG_CONNECT)		
	GuiPoints<T> * gps=(GuiPoints<T> *)&point;
		  if ( gps->is_selected)
		{
		  printf("con proposal called\n");
		  //printf("(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER) : %d\n",(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
		  //printf("(slot.connected!=NULL) : %d\n",(slot.connected!=NULL));
		}
	#endif		
	
	
	
	// do not touch bifurcation centers !
	 if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		 return false;
	 
	 
	 if (sample_scale_temp_dependent && ((temp>point.get_scale())))
	  {
		return false; 
	  }
	
	//random;y pick a particle side 
	int connection_side=std::rand()%2; 
	  
	sta_assert_debug0((point.endpoint_connections[0]<2)&&(point.endpoint_connections[1]<2));  
	  
	class Points<T,Dim>::CEndpoint & slot=*point.endpoints[connection_side][0];
	
// 	#ifdef  D_USE_GUI  & ifdef  DEBUG_CONNECT	
// 		  if ( gps->is_selected)
// 		{
// 		  printf("(slot.connection->edge_type!=EDGE_TYPES::EDGE_SEGMENT) : %d\n",(slot.connected!=NULL) && (slot.connection->edge_type!=EDGE_TYPES::EDGE_SEGMENT));
// 		  printf(" (slot.connection->is_protected_topology() )) : %d\n",(slot.connected!=NULL) &&  (slot.connection->is_protected_topology() ));
// 		  //printf("(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER) : %d\n",(point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER));
// 		  //printf("(slot.connected!=NULL) : %d\n",(slot.connected!=NULL));
// 		}
// 	#endif		
	
	// do not alter bifurcations and protected connections! 
	if ((slot.connected!=NULL) && 
	  ((slot.connection->edge_type!=EDGE_TYPES::EDGE_SEGMENT) || (slot.connection->is_protected_topology() )))
	  return false;
	
	  
	Connection<T,Dim> *   old_connection=NULL;
	Connection<T,Dim>     new_connection;
	T old_cost=0;
	T old_prop=0;
	T new_cost=0;
	T new_prop=0;
	bool new_connection_exists=false;
	
	
if (debug)		  
  printf("A");



	if (!do_tracking3(
		      edgecost_fun,
		      old_connection,
		      old_cost,
		      old_prop,
		      new_connection,
		      new_connection_exists,
		      new_cost,
		      new_prop,
		      conn_tree,
		      slot,
		      search_connection_point_center_candidate,
		      //options.connection_candidate_searchrad,
		      conn_temp,
		      options.connection_bonus_L
		      ))
	{
if (debug)		  
  printf("]\n");	  
	  return false;
	}
/*	
	if (old_connection!=NULL)
	{
	 sta_assert_error(!connection_exists(old_connection)) 
	  
	}*/
// {	
//   bool old_connection_exists=(old_connection!=NULL);
//   	Connection<T,Dim>   *p_connection_new=NULL;	
// 	Connection<T,Dim>    backup_old_connection;
//   
//   
//   	if (old_connection_exists)
// 	{
// 	  if (debug)
// 	  printf("a");	
// 	  backup_old_connection=*old_connection;
// 	  connections.remove_connection(old_connection);
// 	}
// 	if (new_connection_exists)
// 	{
// 	  if (debug)
// 	  printf("b");	
// 	  p_connection_new=connections.add_connection(new_connection);	
// 	  }
// 	if (new_connection_exists)
// 	  {
// 	if (debug)		  
// 	printf("1");	    
// 	      sta_assert_debug0(p_connection_new!=NULL);
// 	      connections.remove_connection(p_connection_new);
// 	  }
// 	  	  
// 	  if (old_connection_exists)
// 	  {
// 	    if (debug)		  
// 	printf("2");	    
// 	      connections.add_connection(backup_old_connection);
// 	  }
// 	  if (debug)		  
//   printf("]\n");
// }	
// 	return false;


// if (false)
// {
//   
// 	T particle_interaction_cost_old=0;
// 	T particle_interaction_cost_new=0;
// 	
// 	
// 	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
// 	class Collision<T,Dim>::Candidates cand(query_buffer);
// 	if (collision_fun_p.is_soft())    
// 	{
// 	  
// 	  //sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,&cand));
// 	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad,temp,&cand,true,-1));
// 	} 
// 	
// 	
// 	 if (collision_fun_p.colliding(particle_interaction_cost_new,tree,point,collision_search_rad,temp,&cand,!(collision_fun_p.is_soft())))
// 	  {
// 	      point.set_direction(old_dir);
// 			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
// 			sta_assert_debug0(!out_of_bounce);
// 	      return false;
// 	  }
//   
// }

  T particle_terminalcool_cost_old=0;
  T particle_terminalcool_cost_new=0;

  class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
  class Collision<T,Dim>::Candidates candA(query_bufferA);
  
  
  class OctPoints<T,Dim> * query_bufferB[query_buffer_size];
  class Collision<T,Dim>::Candidates candB(query_bufferB);

	
	

	
//1	
statistic[stat_count++]++;
if (debug)		  
  printf("Q");	
	Connection<T,Dim>   *p_connection_new=NULL;	
	Connection<T,Dim>    backup_old_connection;
	bool old_connection_exists=(old_connection!=NULL);

	
	int num_involved_pts=0;
	class Points< T, Dim > * involved_pt_set[3];
	involved_pt_set[num_involved_pts++]=&point;
if (debug)		  
  printf("q");	
	if (old_connection_exists)
	{
if (debug)		  
  printf("r");	  
	  involved_pt_set[num_involved_pts++]=slot.connected->point;
	}
if (debug)		  
  printf("B");		

	if (new_connection_exists)
	{
	  if (debug)
	  printf("c");	
	  sta_assert_debug0(new_connection.pointA==&slot); 
	//different points:	  
	  if ((slot.connected==NULL)||(slot.connected->point!=new_connection.pointB->point))
	  {
	    if (debug)
	    printf("d");	
	    involved_pt_set[num_involved_pts++]=new_connection.pointB->point;  
	  }
	  if (debug)
	  printf("e");	
	}

	sta_assert_debug0(num_involved_pts>1);
	sta_assert_debug0(num_involved_pts<4);
	
if (debug)		  
  printf("C");		
	
	T pointcosts=0;
	for (int i=0;i<num_involved_pts;i++)
	{
	    class Points<T,Dim>		   * p=involved_pt_set[i];
	    pointcosts-=p->compute_cost3(temp);
	}
	

	
	T max_pt_rad=std::max(maxscale,point.predict_thickness_from_scale(maxscale))+0.01;
	T point_collision_sphere=std::max(point.get_scale(),point.get_thickness())+max_pt_rad;
	
	
	int collision_mode=1;
	
// 	bool temporarily_added_pt_to_conntree[2];
// 	temporarily_added_pt_to_conntree[0]=temporarily_added_pt_to_conntree[1]=false;
// 	Points<T,Dim> *  temporarily_added_pt_to_conntree_ptr[2];
	
	
	if (old_connection_exists)
	{
	   if (debug)
	  printf("e");	
	  backup_old_connection=*old_connection;

	  
	      Points<T,Dim> *  connected_point=slot.connected->point;
	      
	      T connected_point_collision_sphere=std::max(connected_point->get_scale(),connected_point->get_thickness())+max_pt_rad;
	      
	      if (collision_fun_p.is_soft())    
	      {
		T tmp=0;
		// if there is no new connection, then the terminal state changes
		if (!new_connection_exists)
		{
		  
		  if (debug)
		    printf("[c A0]");	
		  sta_assert_error(!collision_fun_p.colliding(tmp,
							      tree,
							      point,
							      point_collision_sphere,
							      temp,&candA,true,-9,collision_mode));
		  if (debug)
		    printf("[c A0]");	  
		  particle_terminalcool_cost_old+=tmp;
		}

		if (debug)
		    printf("[c B0]");	
		
		
		// the state for this particle will change anyway
		sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							    *connected_point,
							    connected_point_collision_sphere,
							    temp,&candB,true,-9,collision_mode));
		  if (debug)
		    printf("[c B0]");	
		particle_terminalcool_cost_old+=tmp;
	      } 
	  
	  
	  
	  connections.remove_connection(old_connection);
	  
	  
	      if (collision_fun_p.is_soft())    
	      {
		T tmp=0;
		if (!new_connection_exists)
		{
		  if (debug)
		    printf("[c A1]");	
		  sta_assert_error(!collision_fun_p.colliding(tmp,
							      tree,
							      point,
							      point_collision_sphere,
							      temp,&candA,false,-10,collision_mode));
		  
		  if (debug)
		    printf("[c A1]");	
		  particle_terminalcool_cost_new+=tmp;
		}
		
		if (debug)
		    printf("[c B1]");	
		sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							     *connected_point,
							    connected_point_collision_sphere,
							    temp,&candB,false,-10,collision_mode));
		if (debug)
		    printf("[c B1]");	
		particle_terminalcool_cost_new+=tmp;
	      }
	      
	
	
	  
	}
	if (new_connection_exists)
	{
	  
	   if (debug)
	  printf("f");	
	  if (Connections<T,Dim>::connection_exists(new_connection))
	  {
	    if (old_connection_exists)
	    connections.add_connection(backup_old_connection);
	    return false;
	  }
	  
	      T new_connected_point_collision_sphere=std::max(new_connection.pointB->point->get_scale(),new_connection.pointB->point->get_thickness())+max_pt_rad;
	  
	      if (collision_fun_p.is_soft())    
	      {
		T tmp=0;
		// if there was no old connection, then the terminal state changes
		if (!old_connection_exists)
		{
		  
// 		  if (! point.has_owner(voxgrid_conn_can_id))
// 		  {
// 		    temporarily_added_pt_to_conntree[0]=true;
// 		    temporarily_added_pt_to_conntree_ptr[0]=&point;
// 		    if (!conn_tree.insert(point))
// 		    {
// 			throw mhs::STAError("error insering point\n");
// 			return false;
// 		    }
// 		  }
		  
		  
		  if (debug)
		    printf("[c C0]");	
		  sta_assert_error(!collision_fun_p.colliding(tmp,
							      tree,
							      point,
							      point_collision_sphere,
							      temp,&candA,true,-9,collision_mode));
		  	  if (debug)
		    printf("[c C0]");	
		  particle_terminalcool_cost_old+=tmp;
		}

		
		sta_assert_debug0(new_connection.pointB->point->has_owner(voxgrid_conn_can_id)); 
		
		if (debug)
		    printf("[c D0]");	
		// the state for this particle will change anyway
		sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							    *new_connection.pointB->point,
							    new_connected_point_collision_sphere,
							    temp,&candB,true,-9,collision_mode));
	      if (debug)
		    printf("[c D0]");	
		particle_terminalcool_cost_old+=tmp;
	      } 
	  
	  p_connection_new=connections.add_connection(new_connection);	
	  
	  
	      if (collision_fun_p.is_soft())    
	      {
		T tmp=0;
		if (!old_connection_exists)
		{
		  if (debug)
		    printf("[c C1]");	
		  sta_assert_error(!collision_fun_p.colliding(tmp,
							      tree,
							      point,
							      point_collision_sphere,
							      temp,&candA,false,-10,collision_mode));
		  
		    if (debug)
		    printf("[c C1]");	
		  particle_terminalcool_cost_new+=tmp;
		}

		sta_assert_debug0(new_connection.pointB->point->has_owner(voxgrid_conn_can_id)); 
		
		if (debug)
		    printf("[c D1]");	
		sta_assert_error(!collision_fun_p.colliding(tmp,
							    tree,
							     *new_connection.pointB->point,
							    new_connected_point_collision_sphere,
							    temp,&candB,false,-10,collision_mode));
		if (debug)
		    printf("[c D1]");	
		particle_terminalcool_cost_new+=tmp;
	      }
	  
	}
	if (debug)
	  printf("g");	
	
	for (int i=0;i<num_involved_pts;i++)
	{
	    class Points<T,Dim>		   * p=involved_pt_set[i];
	    pointcosts+=p->compute_cost3(temp);
	}
	
// 	for (int i=0;i<2;i++)
// 	{
// 	  if (temporarily_added_pt_to_conntree[i])
// 	  {
// 	      temporarily_added_pt_to_conntree[i]->unregister_from_grid(voxgrid_conn_can_id);
// 	  }
// 	}
	

// 	printf("%d %d\n",candA.found,candB.found);
	
//2	
statistic[stat_count++]++;	
if (debug)		  
  printf("D");		
	
	T E=(new_cost-old_cost)/temp;
	  E+=pointcosts/temp;  
	
	if (collision_fun_p.is_soft())   
	{
	    E+=(particle_terminalcool_cost_new-particle_terminalcool_cost_old)/temp;
	}
	
	T R=mhs_fast_math<T>::mexp(E);

	#ifdef  D_USE_GUI 
	  #ifdef  DEBUG_CONNECT		
		
		if ( gps->is_selected)
		{
		 printf("particle_terminalcool_cost_new : %f\n",particle_terminalcool_cost_new); 
		 printf("particle_terminalcool_cost_old : %f\n",particle_terminalcool_cost_old); 
		 printf("new_cost : %f\n",new_cost); 
		 printf("old_cost : %f\n",old_cost); 
		 printf("pointcosts : %f\n",pointcosts); 
		}
	  #endif
	#endif		
	
	//  T R=std::exp(-E);
	
	
	#ifdef D_USE_GUI    
	  point.snapshot_edge_proposed_costs[0]=old_cost;
	  point.snapshot_edge_proposed_costs[1]=new_cost;
	  point.snapshot_edge_proposed_costs[2]=0;
	  point.snapshot_edge_proposed_costs[3]=pointcosts;
	  point.snapshot_edge_proposed_costs[4]=old_prop;
	  point.snapshot_edge_proposed_costs[5]=new_prop;
	#endif

	  
	  
	R*=old_prop/((new_prop)+std::numeric_limits<T>::epsilon());

//printf("%f %f %f %f %f\n",new_cost,old_cost,pointcosts,old_prop,new_prop);		
	
 	      if (new_connection_exists&&(!old_connection_exists))
		propose_statistic[0]++;
	      if (old_connection_exists&&(!new_connection_exists))
	      {
// 		printf("%f %f %f\n",new_cost,old_cost,pointcosts);
		propose_statistic[1]++;
	      }
	      if (old_connection_exists&&new_connection_exists)
		propose_statistic[2]++;	
	
	      
// 	  #ifdef D_USE_GUI    
// 	      
// 	  #endif
	      
	
	  if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
	  {
	    #ifdef D_USE_GUI    
	      point.snapshot_edge_proposed_costs[2]=1;
	    #endif
	    
// 	     if (old_connection_exists&&(!new_connection_exists))
// 	      {
//  		printf("%f %f %f\n",new_cost,old_cost,pointcosts);
// 	      }
	    
	  /*printf("7"); */	  
	      if (new_connection_exists)
	      {
// 		
if (debug)		  
  printf("d");
		if (Constraints::constraint_hasloop_follow(point,p_connection_new,options.constraint_loop_depth))
		{
if (debug)		  
  printf("1");
		  connections.remove_connection(p_connection_new);
if (debug)		  
  printf("2");		  
		  if (old_connection_exists)
		  {
if (debug)		  
  printf("3");		    
		      connections.add_connection(backup_old_connection);
if (debug)		  
  printf("4");		      
		  }
if (debug)		  
  printf("]\n");			  
		  return false;
		}
 	      }
 	      
 	      if (new_connection_exists&&(!old_connection_exists))
		propose_statistic[3]++;
	      if (old_connection_exists&&(!new_connection_exists))
		propose_statistic[4]++;
	      if (old_connection_exists&&new_connection_exists)
		propose_statistic[5]++;
 	     
// 	    for (int i=0;i<num_involved_pts;i++)
// 	    {
// 	      class Points<T,Dim>		   * p=involved_pt_set[i];  
// 	      
// 	      if ((p->endpoint_connections[0]==0)||(p->endpoint_connections[1]==0))
// 	      {
// 		if (!conn_tree.insert(*p,true,false,true))
// 		{
// 		  if (new_connection_exists)
// 		  {
// 		  connections.remove_connection(p_connection_new);
// 		  }
// 		  if (old_connection_exists)
// 		  {
// 		      connections.add_connection(backup_old_connection);
// 		  }
// 		  return false;  
// 		}
// 	      }
// 	    }
	      
	      
	      
	    for (int i=0;i<num_involved_pts;i++)
	    {
	      class Points<T,Dim>		   * p=involved_pt_set[i];  
	      #ifdef  D_USE_GUI
// 		(GuiPoints<T>* (p))->touch(get_type());
		  p->touch(get_type());
	      #endif
	      
	      
	      
	      if ((p->endpoint_connections[0]==0)||(p->endpoint_connections[1]==0))
	      {
		if (!conn_tree.insert(*p,true))
		{
		    p->print();
		    throw mhs::STAError("error insering point\n");
		    return false;
		}
	      }else
	      {
		p->unregister_from_grid(voxgrid_conn_can_id);
	      }
	    }
	    
	     
	      
	      
 	      
if (debug)		
 
  printf("5"); 	      
		this->tracker->update_energy(E,static_cast<int>(get_type()));
		this->proposal_accepted++;
if (debug)		  
  printf("]\n");			
	    return true;
	  }
if (debug)		  
  printf("E");	  
	  if (new_connection_exists)
	  {
	      sta_assert_debug0(p_connection_new!=NULL);
	      connections.remove_connection(p_connection_new);
	  }
if (debug)		  
  printf("e");	  	  
	  if (old_connection_exists)
	      connections.add_connection(backup_old_connection);
if (debug)		  
  printf("]\n");	

//3
statistic[stat_count++]++;
        return false;

	
	
      } catch (mhs::STAError error)
        {
            throw error;
        }
    }
    
    
/*    
    	if (!do_tracking3(
		      edgecost_fun,
		      old_connection,
		      old_cost,
		      old_prop,
		      new_connection,
		      new_cost,
		      new_prop,
		      tree,
		      point.endpoints[connection_side],
		      search_connection_point_center_candidate,
		      options.connection_candidate_searchrad,
		      conn_temp,
		      options.connection_bonus_L
		      ))
  */  

    // searchrad must be 2*maxscale (2 mal max radius) // TODO why? I forgot
    bool  do_tracking3(
        class CEdgecost<T,Dim> & edgecost_fun,
        class Connection<T,Dim> * & old_connection,
	T & old_cost,
	T & old_prop,
	class Connection<T,Dim>  & new_connection,
	bool & new_connection_exists,
	T & new_cost,
	T & new_prop,
        OctTreeNode<T,Dim> & tree,
        class Points<T,Dim>::CEndpoint & slot,
        T search_connection_point_center_candidate, 
//         T searchrad, 
        T TempConnect,
        T L
    )
    {
	new_connection_exists=false;  
        bool debug=false;
        if (debug)
            printf("do_tracking\n");

	class Points<T,Dim> & npoint=*slot.point;
	int endpoint_side=slot.side;
	
	#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
	  sta_assert_debug0(npoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION);
	#endif
	
	  
	    #if defined(D_USE_GUI) && defined(DEBUG_CONNECT)		
			    std::stringstream debug_info;
			    GuiPoints<T> * gps=(GuiPoints<T> *)&npoint;
			    if ( gps->is_selected)
			    {
			     printf("do tracking\n"); 
			    }
			      
	  #endif

        /*!
         collect all points in search rectangle
         */
//         npoint.update_endpoint(endpoint_side);
	const Vector<T,Dim> & endpoint=*slot.position;

	std::size_t found;
        class OctPoints<T,Dim> * query_buffer[query_buffer_size];
        class OctPoints<T,Dim> ** candidates;
        candidates=query_buffer;

        try {
            tree.queryRange(
                endpoint,
                search_connection_point_center_candidate,
                candidates,
                query_buffer_size,
                found);
        } catch (mhs::STAError & error)
        {
            throw error;
        }

        sta_assert_debug0(npoint.maxconnections>0);
	
	
// 	#ifdef  D_USE_GUI & ifdef  DEBUG_CONNECT		
// 			    if ( gps->is_selected)
// 			    {
// 			     printf("found %d candidates\n",found);
// 			    }
// 		      
// 	  #endif
	

        /// no points nearby. no existing edge, do nothing
        if ((found==1)&&(query_buffer[0]==&npoint)&&((slot.connected==NULL)))
        {
	    
	    
            if (debug)
                printf("no points\n",found);
            return false;
        }

        
        T searchrad=CEdgecost<T,Dim>::get_current_edge_length_limit(); 

        if (debug)
            printf("found %d initial candidates in radius %f\n",found,searchrad);


        searchrad*=searchrad;
        search_connection_point_center_candidate*=search_connection_point_center_candidate;


        if (debug)
            printf("collecting endpoints in circle\n");


        class Points< T, Dim >::CEndpoint * connet_candidates_buffer[query_buffer_size];
        class Points< T, Dim >::CEndpoint ** connet_candidates=connet_candidates_buffer;

	 #ifdef  D_USE_GUI 
		      #ifdef  DEBUG_CONNECT		
			    if ( gps->is_selected)
			    {
			     printf("found %d candidates\n",found);
			    }
		      #endif
	  #endif
	
	
        std::size_t num_connet_candidates=0;
        {

            {
                /*!
                check for all candidates in the rectangle
                  if one of the endpoints lies within the circle
                    with radius "searchrad"
                */
                for (std::size_t a=0; a<found; a++)
                {
                    Points<T,Dim> * point=(Points<T,Dim> *)candidates[a];
		    
		    if (point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		      continue;
		    
//                     if ((point!=&npoint) // no self connections ;-)
// 		       &&((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))) // no bifurcation poins
		    #ifdef _BIF_CD_REQUIRES_2_ALL_CONNECT_ 
                     if ((point!=&npoint) // no self connections ;-)
 		       &&((point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION))) // no bifurcation poins
		    #else
		    if (point!=&npoint) // no self connections ;-)
		    #endif  
                    {
                        //TODO might take into coinsideration:
                        //would always happensif I would use endpoint position
                        //instead of point posiiton because queryRange already supports sphere selection
                        //
                        T candidate_center_dist_sq=(Vector<T,Dim>(point->get_position())-npoint.get_position()).norm2();
                        if (search_connection_point_center_candidate>candidate_center_dist_sq)
                        {
                            for (int pointside=0; pointside<=1; pointside++)
                            {
			      
				// do not connect with particle sides which are modeling a bifurcation
				// or are already occupied with a connection
				if (point->endpoint_connections[pointside]==0)
				{
				  ///! updating endpoints (not done automatically)
// 				  point->update_endpoint(pointside);
				  Vector<T,Dim> & candidate_endpoint=point->endpoints_pos[pointside];
				  T candidate_dist_sq=(endpoint-candidate_endpoint).norm2();
				  if (debug)
				      printf("endpointdist: %f\n",std::sqrt(candidate_dist_sq));

				  /*!
				    if not in radius remove
				      point from further consideration
				  */
				  if (searchrad>candidate_dist_sq)
				  {
				      class Points< T, Dim >::CEndpoint  * freehub=point->getfreehub(pointside);
				      /*!
					if there are no free connections, remove
				      endpoint from further consideration
				      */
				      if (freehub!=NULL)
				      {
					  sta_assert_debug0(num_connet_candidates<query_buffer_size);
					  (*connet_candidates++)=freehub;
					  num_connet_candidates++;
				      }
				  }
				}
                            }
                        }


                    }
                    /*  iter++;	*/
                }
                //! add existing connection endpoint
                if (slot.connected!=NULL)
                {
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
		    sta_assert_error(slot.connected->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT);
#endif
		    sta_assert_error(slot.connection->edge_type!=EDGE_TYPES::EDGE_BIFURCATION);  
		    
// 		    #ifdef _DEBUG_CHECKS_0_
// 		      slot.connected->point->update_endpoint(slot.connected->side);
// 		      sta_assert_debug0(searchrad>((*(slot.position))-(*(slot.connected->position))).norm2());  
// 		    #endif
		      
// 		      slot.connected->point->update_endpoint(slot.connected->side);
		      sta_assert_debug0(num_connet_candidates+1<query_buffer_size);
		      *connet_candidates++=slot.connected;
		      num_connet_candidates++;
                } else
		{
                    /// no points nearby and not connected -> do nothing
                    if ((num_connet_candidates==0))
                    {
                        if (debug)
                            printf("no endpoints\n",num_connet_candidates);

                        return false;
                    }
		}
		
#ifdef  D_USE_GUI 
    #ifdef  DEBUG_CONNECT		
		GuiPoints<T> * gps=(GuiPoints<T> *)&npoint;
		if ( gps->is_selected)
		{
		 printf("found : %d\n",num_connet_candidates); 
		}
    #endif
#endif		
                T min_cost=std::numeric_limits<T>::max();


                if (debug)
                    printf("computing probabilities for %d points\n",num_connet_candidates);



                T connet_cost[query_buffer_size];
                T connet_prop[query_buffer_size];
                T connet_prop_acc[query_buffer_size];


                class Points< T, Dim >::CEndpoint * connet_candidates_array[query_buffer_size];


                connet_candidates=connet_candidates_buffer;
                int candidates_count=0;
		
		
		std::size_t invalid_Edge=0;
		
		T max_prop=0;
                for (std::size_t i=0; i<num_connet_candidates; i++)
                {


                    class Points< T, Dim >::CEndpoint * candidate_hub=connet_candidates[i];

                    

//                     Vector<T,Dim> & candidate_endpoint_pos=*(candidate_hub->position);
//                     Points<T,Dim> * candidate_point=candidate_hub->point;
// 
//                     const Vector<T,Dim> & candidate_point_pos=candidate_point->get_position();
//                     T   candidate_point_size=candidate_point->get_scale();


                    typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;

		    
		   
		     #if defined(D_USE_GUI) && defined(DEBUG_CONNECT)
			      std::stringstream debug_info;
			      GuiPoints<T> * gps=(GuiPoints<T> *)&npoint;
			      //if 
			      T u=edgecost_fun.e_cost(slot,
                                            *candidate_hub,
                                            inrange,true,false, ( gps->is_selected) ? &debug_info : NULL);
			      
			      if ( gps->is_selected)
			      {
				//if (slot.connected!=NULL)
				//if (slot.connected==NULL)
				{
				printf("------------------------\n");
				printf("already connected? %d [%d %d]\n",(slot.connected!=NULL),i,num_connet_candidates);
				printf(" < n1 m n2 > %f\n",slot.get_slot_direction().dot(candidate_hub->get_slot_direction()));
				printf("%s\n",debug_info.str().c_str());
				}
				
				
			      }
			      
		    #else	
			      T u=edgecost_fun.e_cost(slot,
                                            *candidate_hub,
                                            inrange);
		    #endif
		      
                    


                    sta_assert_error_c(inrange!=(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_OUT_OF_RANGE),
		      (edgecost_fun.e_cost(slot,
                                            *candidate_hub,
                                            inrange,true,true))
		    );


                    if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
		    {
		      invalid_Edge++;
		      continue;
		    }
		    
		    connet_prop[candidates_count]=mhs_fast_math<T>::mexp((u+Lprior)/TempConnect)+ConnectEpsilon;
                    connet_cost[candidates_count]=u+L;
                    min_cost=std::min(min_cost,u);
		    max_prop=std::max(max_prop,connet_prop[candidates_count]);

                    if (candidates_count==0)
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count];
                    }
                    else
                    {
                        connet_prop_acc[candidates_count]=connet_prop[candidates_count]+connet_prop_acc[candidates_count-1];
                    }
                    
                    connet_candidates_array[candidates_count]=candidate_hub;
                    candidates_count++;
                }
                
                #ifdef  D_USE_GUI 
		  #ifdef  DEBUG_CONNECT		
		
		if ( gps->is_selected)
		{
		 printf("valid: %d\n",candidates_count); 
		}
		  #endif
		#endif	

// 		printf("candidates_count: %d\n",candidates_count);

                //! since connect_prob=1 -> e^0
//                 connet_cost[candidates_count]=0;
		connet_cost[candidates_count]=0;


 		//T const_no_connection_weight=mexp(Lprior/TempConnect);
		//T const_no_connection_weight=max_prop+1; // same as best edge
		T const_no_connection_weight=non_edge_prop_fact*(max_prop+1); // prop to best edge
		
		//T const_no_connection_weight=1;//mexp(Lprior/TempConnect);
		
		
		/*
                T const_no_connection_weight=1;
                if (connet_cost[candidates_count]!=0)
                    const_no_connection_weight=mexp(connet_cost[candidates_count]/TempConnect);
		*/
		


                /// adding the "do not connect" proposal propability
                if (candidates_count==0)
                    connet_prop_acc[candidates_count]=const_no_connection_weight;
                else
                {
                    connet_prop_acc[candidates_count]=connet_prop_acc[candidates_count-1]+const_no_connection_weight;
                }
                connet_prop[candidates_count]=const_no_connection_weight;
		
		
		/*for (int i=0;i<= candidates_count;i++)
		{
		 printf("%f ",connet_prop[i]); 
		}
		printf("\n");*/ 


                if (debug)
                    printf("creating proposal\n");

                /*!
                  now we choose a candidate with probability connet_prop
                  using connet_prop_acc

                  and we randomliy (uniformly) choose one port from the current point
                  (if there is no port, we create one)
                */
		
		sta_assert_error(candidates_count==num_connet_candidates-invalid_Edge);
                {

                    // pic candidate with propability connect_prop
                    std::size_t connection_candidate=rand_pic_array(connet_prop_acc,1+candidates_count,connet_prop_acc[candidates_count]);

                    if (debug)
                        printf("choosing %d\n",connection_candidate);


                    sta_assert_debug0(connection_candidate>=0);
                    sta_assert_debug0(connection_candidate<=candidates_count);


                    /*########################################
                             * Old Connnection Cases
                     *#######################################*/
		    
		    
		      


                    if (slot.connected==NULL)
                    {
                        //! same as old conection (no connection)
                        if (connection_candidate==candidates_count)
                            return false;
                        old_prop=const_no_connection_weight/connet_prop_acc[candidates_count];
                        old_cost=connet_cost[candidates_count];
			
			
                    } else
                    {
			old_connection=slot.connection;
                        //! same as old conection
                        if ((connection_candidate<candidates_count)
                                &&(slot.connected==connet_candidates_array[connection_candidate]))
                            return false;
			old_prop=connet_prop[candidates_count-1]/connet_prop_acc[candidates_count];
                        old_cost=connet_cost[candidates_count-1];

                    }


                    /*########################################
                     * New Connnection Cases
                    *#######################################*/
		    

                    // the "no connection proposal"
                    if (connection_candidate==candidates_count)
                    {
                        if (debug)
                            printf("no connection proposal\n");
                        new_prop=const_no_connection_weight/connet_prop_acc[candidates_count];
                        new_cost=connet_cost[candidates_count];
			sta_assert_debug0(new_cost==0);
                        return true;
                    } else
                        /*!
                         * propose a connection
                         */
                    {
			new_connection_exists=true;
                        if (debug)
                            printf("connection proposal\n");

                        sta_assert_debug0(connection_candidate>=0);
                        sta_assert_debug0(connection_candidate<candidates_count);
			sta_assert_debug0(connet_candidates_array[connection_candidate]!=NULL);

			  
                        //new_connection.pointA=slot.point->getfreehub(slot.side);
			new_connection.pointA=&slot;
                        new_connection.pointB=connet_candidates_array[connection_candidate];
                        new_cost=connet_cost[connection_candidate];
                        new_prop=connet_prop[connection_candidate]/connet_prop_acc[candidates_count];

                        if (debug)
                            printf("side %d with side %d\n",connet_candidates_array[connection_candidate]->side,slot.side);



                        sta_assert_debug0(new_connection.pointA!=NULL);
                        sta_assert_debug0(new_connection.pointB!=NULL);
			sta_assert_debug0(new_connection.pointB!=new_connection.pointA);

                        if (debug)
                            printf("conn index %d\n",connection_candidate);
			
// {			
// typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
// 
// T u=edgecost_fun.u_cost(*new_connection.pointA,
// 	    *new_connection.pointB,
// 	    searchrad,
// 	    inrange);
// 
// sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 
// u=edgecost_fun.u_cost(*new_connection.pointB,
// 	    *new_connection.pointA,
// 	    searchrad,
// 	    inrange);
// 
// sta_assert_error(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE));
// 
// 
// }
                        return true;
                    }

                }

            }
        }

        mhs::STAError error;
	error<<"should not be reached! ("<<enum2string(PROPOSAL_TYPES::PROPOSAL_CONNECT)<<")";
        throw error;
        return true;
    }


};

#endif


