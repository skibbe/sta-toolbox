#ifndef MHS_QUAT_H
#define MHS_QUAT_H

#include "mhs_vector.h"
#include "mhs_matrix.h"



template<typename T>
class Quaternion
{
public:
    union{
	  struct{
	      T w;
	      T x;
	      T y;
	      T z;
	  };
	  struct{
	      T qw;
	      T qx;
	      T qy;
	      T qz;
	  };
	  struct{
	      T q0;
	      T q1;
	      T q2;
	      T q3;
	  };
	  struct{
	    T r; 
	    T i[3];
	      
	  };
	  T v[4];
	  T q[4];
	};
	
	
	  void print() const
    {
        std::stringstream s;
        s<<"(";

        for (int i=0; i<4; i++)
            if (i<4-1)
                s<<v[i]<<",";
            else
                s<<v[i]<<")";
        printf("%s\n",s.str().c_str());
    }

	
	const T &operator[](int i) const {
	  return v[i];
	};
	T &operator[](int i)  {
	    return v[i];
	};
    
	Quaternion (Vector<T,3> vec)
	{
		T cos_z_2=0.5f*std::cos(vec[0]);
		T cos_y_2=0.5f*std::cos(vec[1]);
		T cos_x_2=0.5f*std::cos(vec[2]);

		T sin_z_2=0.5f*std::sin(vec[0]);
		T sin_y_2=0.5f*std::sin(vec[1]);
		T sin_x_2=0.5f*std::sin(vec[2]);

		q0=cos_z_2*cos_y_2*cos_x_2	+	sin_z_2*sin_y_2*sin_x_2;
		q1=cos_z_2*cos_y_2*sin_x_2	-	sin_z_2*sin_y_2*cos_x_2;
		q2=cos_z_2*sin_y_2*cos_x_2	+	sin_z_2*cos_y_2*sin_x_2;
		q3=sin_z_2*cos_y_2*cos_x_2	-	cos_z_2*sin_y_2*sin_x_2;

	}
	
	
	Quaternion(Vector<T,3> &v, T t)
	{
		T	theta_div2=0.5f*t;
		T	sin_theta=std::sin(theta_div2);

		this->q1=sin_theta*v.x;
		this->q2=sin_theta*v.y;
		this->q3=sin_theta*v.z;
		this->q0=std::cos(theta_div2);
	}

	Quaternion()
	{
		x=y=z=0;
		w=1;
	}

	Quaternion(T x, T y, T z)
	{
		q0=0;
		q1=x;
		q2=y;
		q3=z;
	}

	Quaternion(T q0, T q1, T q2, T q3)
	{
		this->q0=q0;
		this->q1=q1;
		this->q2=q2;
		this->q3=q3;
	}
	
	Quaternion log() const
	{
		T a = std::acos(r);
		T sina = std::sin(a);
		Quaternion result;

		result.r = 0;
		if (sina > std::numeric_limits<T>::epsilon())
		{
			result.i[0] = a*i[0]/sina;
			result.i[1] = a*i[1]/sina;
			result.i[2] = a*i[2]/sina;
		} else {
			result.i[0]=result.i[1]=result.i[2]=0;
		}
		
		return result;
	}
	
	
	Quaternion exp() const
	{
		T a = std::sqrt(i[0]*i[0]+i[1]*i[1]+i[2]*i[2]);
		
		Quaternion result;

		
		if (a > std::numeric_limits<T>::epsilon() )
		{
			
			T sina = std::sin(a);
			T cosa = std::cos(a);
			result.r = cosa;
			result.i[0] = sina*i[0]/a;
			result.i[1] = sina*i[1]/a;
			result.i[2] = sina*i[2]/a;
		} else {
			result.i[0]=result.i[1]=result.i[2]=0;
			result.r = 1;
		}
		return result;
	}


	
	static Quaternion lerp(Quaternion  Q1,const Quaternion & Q2, T t,bool flip=true)
	{
	  
	    T angle = Q1.dot(Q2);

	    if ((angle < T(0.0))&&(flip))
	    {
 		    Q1 *= T(-1.0);
		    angle *= T(-1.0);
	    }
	  Quaternion  Q;
	  Q = Q1*(1-t)+Q2*t;
	  Q.normalize();
	  return Q;
	}
	
// 	static Quaternion slerp(Quaternion  Q1,const Quaternion & Q2, T t, T flip_to_linear=T(0.000001))
// 	{
// 	  
// 	    T angle = Q1.dot(Q2);
// 
// 	    if (angle < T(0.0))
// 	    {
// 		    Q1 *= T(-1.0);
// 		    angle *= T(-1.0);
// 	    }
// 
// 	    if (angle <= (1-flip_to_linear))
// 	    {
// 		    T theta = std::acos(angle);
// 		    T invsintheta = T(1)/(std::sin(theta)+std::numeric_limits<T>::epsilon());
// 		    T scale = std::sin(theta * (1-t)) * invsintheta;
// 		    T invscale = std::sin(theta * t) * invsintheta;
// 		    return (Q1*scale) + (Q2*invscale);
// 	    }
// 	    else 
// 	    {
// 		    return lerp(Q1,Q2,t);
// 	    }
// 	}
	
	static Quaternion slerp(Quaternion  Q1,const Quaternion & Q2, T t, T flip_to_linear=T(0.01),bool flip=true)
	{
	  
	    T angle = Q1.dot(Q2);

	    if ((angle < T(0.0))&&(flip))
	    {
 		    Q1 *= T(-1.0);
		    angle *= T(-1.0);
	    }

	    if (angle <= std::abs(1-flip_to_linear))
	    {
		    T theta = std::acos(angle);
		    T invsintheta = T(1)/(std::sin(theta)+std::numeric_limits<T>::epsilon());
		    T scale = std::sin(theta * (1-t)) * invsintheta;
		    T invscale = std::sin(theta * t) * invsintheta;
		    return (Q1*scale) + (Q2*invscale);
	    }
	    else 
	    {
		    return lerp(Q1,Q2,t,false);
	    }
	}
	
	
	static Quaternion squad_help(Quaternion  Q0,Quaternion  Q1, Quaternion  Q2)
	{
	
         Quaternion  Q1inv=Q1.operatorInverse();
         return Q1*((((((Q1inv*Q2).log())+((Q1inv*Q0).log()))/(-4))).exp());
	}
        
        static Quaternion squad(Quaternion  Q1,Quaternion  Q2, T t,Quaternion  Q0,Quaternion  Q3, T flip_to_linear=T(0.01),bool force=false)
	{
	 {
	  T q0q1=Q0.dot(Q1);  
	  T q1q2=Q1.dot(Q2);
	  T q2q3=Q2.dot(Q3);
	  T q0q3=Q0.dot(Q3);
	  
// 	  if ((q0q1>0.9)||(q1q2>0.9)||(q2q3>0.9)||(q0q3>0.7))
// 	    return slerp(Q1,Q2,t);
	  if (!force)
	  {
	  if ((q0q1<0.7)||(q1q2<0.7)||(q2q3<0.7)||(q0q3<0.16))
	    return slerp(Q1,Q2,t);
	  }else
	  {
	    if ((q0q1<0.0)||(q1q2<0.0)||(q2q3<0.0)||(q0q3<0.1))
	    return slerp(Q1,Q2,t);
	  }
	   
	   
	  Quaternion tmp_q1=slerp(Q1,Q2,t,flip_to_linear,false);
	  
	  Quaternion s1=squad_help(Q0,Q1,Q2);      
// 	  s1.normalize();
	  Quaternion s2=squad_help(Q1,Q2,Q3);
// 	  s2.normalize();
	
	    Quaternion tmp_q2=slerp(s1,s2,t,flip_to_linear,false);

	
	    return slerp(tmp_q1,tmp_q2,2*t*(1-t),flip_to_linear,false);
	 }
	}
	
	
	
	void AroundV(Vector<T,3> &v, T t)
	{
		T	theta_div2=0.5f*t;
		T	sin_theta=sinf(theta_div2);

		this->q1=sin_theta*v.x;
		this->q2=sin_theta*v.y;
		this->q3=sin_theta*v.z;
		this->q0=std::cos(theta_div2);
	}




	Vector<T,3> AroundVinverse(T & t)
	{
		Vector<T,3>	v;
		t=std::acos(q0);

		T sinf_t_inv = 1.0f/sinf(t);

		v.x=q1*sinf_t_inv;
		v.y=q2*sinf_t_inv;
		v.z=q3*sinf_t_inv;

		t*=2;
		return(v);
	}



	
	T dot(const Quaternion & Q) const
	{
		return (x * Q.x) + (y * Q.y) + (z * Q.z) + (w * Q.w);
	}
	
	
	Quaternion	operator +(const Quaternion &Q) const
	{
		Quaternion	q;
		q.q0=q0+Q.q0;
		q.q1=q1+Q.q1;
		q.q2=q2+Q.q2;
		q.q3=q3+Q.q3;
		return(q);
	}

	Quaternion	operator -(const Quaternion &Q) const
	{
		Quaternion	q;
		q.q0=q0-Q.q0;
		q.q1=q1-Q.q1;
		q.q2=q2-Q.q2;
		q.q3=q3-Q.q3;
		return(q);
	}


	Quaternion & operator +=(const Quaternion &Q)
	{
		q0+=Q.q0;
		q1+=Q.q1;
		q2+=Q.q2;
		q3+=Q.q3;
		return(*this);
	}

	Quaternion & operator -=(const Quaternion &Q)
	{
		q0-=Q.q0;
		q1-=Q.q1;
		q2-=Q.q2;
		q3-=Q.q3;
		return(*this);
	}


	void Conjugate()
	{
		q1*=-1;
		q2*=-1;
		q3*=-1;
	}

	Quaternion OperatorConjugate()
	{
		Quaternion q;
		q.q0=q0;
		q.q1=-q1;
		q.q2=-q2;
		q.q3=-q3;
		return(q);
	}

	T norm()
	{
		T r;
		r=q0*q0+q1*q1+q2*q2+q3*q3;
		r=std::sqrt(r);
		return(r);
	}

	T norm2()
	{
		T r;
		r=q0*q0+q1*q1+q2*q2+q3*q3;
		return(r);
	}

	void normalize()
	{
		T qlength_inv = 1.0f/(std::sqrt(q0*q0+q1*q1+q2*q2+q3*q3));

		q0=q0*qlength_inv;
		q1=q1*qlength_inv;
		q2=q2*qlength_inv;
		q3=q3*qlength_inv;


	}



	void unit_Inverse()
	{
		x*=-1;
		y*=-1;
		z*=-1;
	}
	Quaternion operatorUnit_Inverse()
	{
		Quaternion q;
		q.q0=q0;
		q.q1=-1*q1;
		q.q2=-1*q2;
		q.q3=-1*q3;
		return(q);
	}

	void inverse()
	{
		T qlength_inv = 1.0f/(std::sqrt(q0*q0+q1*q1+q2*q2+q3*q3));

		q0=q0*qlength_inv;
		q1=-q1*qlength_inv;
		q2=-q2*qlength_inv;
		q3=-q3*qlength_inv;

	}

	Quaternion operatorInverse()
	{
		Quaternion q;
		T qlength_inv = 1.0f/(std::sqrt(q0*q0+q1*q1+q2*q2+q3*q3));

		q.q0=q0*qlength_inv;
		q.q1=-q1*qlength_inv;
		q.q2=-q2*qlength_inv;
		q.q3=-q3*qlength_inv;
		return(q);
	}

	Quaternion operator *(const Quaternion &Q) const
	{
	  Quaternion q;



	  q.w = (Q.w * w) - (Q.x * x) - (Q.y * y) - (Q.z * z);
	  q.x = (Q.w * x) + (Q.x * w) + (Q.y * z) - (Q.z * y);
	  q.y = (Q.w * y) + (Q.y * w) + (Q.z * x) - (Q.x * z);
	  q.z = (Q.w * z) + (Q.z * w) + (Q.x * y) - (Q.y * x);

	  return q;	
		
	/*
	q.q0 = q0*Q.q0 - q1*Q.q1 - q2*Q.q2 - q3*Q.q3;
	q.q1 = q0*Q.q1 + q1*Q.q0 + q2*Q.q3 - q3*Q.q2;
	q.q2 = q0*Q.q2 - q1*Q.q3 + q2*Q.q0 - q3*Q.q1;
	q.q3 = q0*Q.q3 + q1*Q.q2 - q2*Q.q1 + q3*Q.q0;
	*/
		
/*
	T prd_0 = (q3 - q2) * (Q.q2 - Q.q3);
	T prd_1 = (q0 + q1) * (Q.q0 + Q.q1);
	T prd_2 = (q0 - q1) * (Q.q2 + Q.q3);
	T prd_3 = (q2 + q3) * (Q.q0 - Q.q1);
	T prd_4 = (q3 - q1) * (Q.q1 - Q.q2);
	T prd_5 = (q3 + q1) * (Q.q1 + Q.q2);
	T prd_6 = (q0 + q2) * (Q.q0 - Q.q3);
	T prd_7 = (q0 - q2) * (Q.q0 + Q.q3);

	T prd_8 = prd_5 + prd_6 + prd_7;
	T prd_9 = 0.5f * (prd_4 + prd_8);

	// and finally build up the result with the temporary products

	q.q0 = prd_0 + prd_9 - prd_5;
	q.q1 = prd_1 + prd_9 - prd_8;
	q.q2 = prd_2 + prd_9 - prd_7;
	q.q3 = prd_3 + prd_9 - prd_6;

	return(q);*/
	}

	Quaternion & operator *=(const Quaternion &Q)
	{
	  
	  T tw = (Q.w * w) - (Q.x * x) - (Q.y * y) - (Q.z * z);
	  T tx = (Q.w * x) + (Q.x * w) + (Q.y * z) - (Q.z * y);
	  T ty = (Q.w * y) + (Q.y * w) + (Q.z * x) - (Q.x * z);
	  T tz = (Q.w * z) + (Q.z * w) + (Q.x * y) - (Q.y * x);
	  
	  w=tw;
	  y=ty;
	  x=tx;
	  z=tz;
/*
	T prd_0 = (q3 - q2) * (Q.q2 - Q.q3);
	T prd_1 = (q0 + q1) * (Q.q0 + Q.q1);
	T prd_2 = (q0 - q1) * (Q.q2 + Q.q3);
	T prd_3 = (q2 + q3) * (Q.q0 - Q.q1);
	T prd_4 = (q3 - q1) * (Q.q1 - Q.q2);
	T prd_5 = (q3 + q1) * (Q.q1 + Q.q2);
	T prd_6 = (q0 + q2) * (Q.q0 - Q.q3);
	T prd_7 = (q0 - q2) * (Q.q0 + Q.q3);

	T prd_8 = prd_5 + prd_6 + prd_7;
	T prd_9 = 0.5f * (prd_4 + prd_8);

	// and finally build up the result with the temporary products

	q0 = prd_0 + prd_9 - prd_5;
	q1 = prd_1 + prd_9 - prd_8;
	q2 = prd_2 + prd_9 - prd_7;
	q3 = prd_3 + prd_9 - prd_6;
	return(*this);*/
	}

	Quaternion operator *(const T &f) const
	{
		Quaternion q;
		q.q0=q0*f;
		q.q1=q1*f;
		q.q2=q2*f;
		q.q3=q3*f;
		return(q);
	}
	
	Quaternion operator /(const T &f) const
	{
		Quaternion q;
		q.q0=q0/f;
		q.q1=q1/f;
		q.q2=q2/f;
		q.q3=q3/f;
		return(q);
	}

	Quaternion & operator *=(const T &f)
	{
		q0*=f;
		q1*=f;
		q2*=f;
		q3*=f;
		return(*this);
	}

	Quaternion & operator =(const Quaternion &Q)
	{
		q0=Q.q0;
		q1=Q.q1;
		q2=Q.q2;
		q3=Q.q3;
		return(*this);
	}

	Matrix<T,4> getMatrix()
	{
		Matrix<T,4> mat;
		mat.Identity();

		mat.m[0][0]=1-2*(qy*qy + qz*qz);
		mat.m[0][1]=  2*(qx*qy - qz*qw);
		mat.m[0][2]=  2*(qx*qz + qy*qw);
		
		mat.m[1][0]=  2*(qx*qy + qz*qw);
		mat.m[1][1]=1-2*(qx*qx + qz*qz);
		mat.m[1][2]=  2*(qy*qz - qx*qw);

		mat.m[2][0]=  2*(qx*qz - qy*qw);
		mat.m[2][1]=  2*(qy*qz + qx*qw);
		mat.m[2][2]=1-2*(qx*qx + qy*qy);

		return (mat);
	}
	
	Quaternion (Matrix<T,4> mat)
	{
// 	   T trace = mat.m[0][0] + mat.m[1][1] + mat.m[2][2] ;
	  T trace = mat.m[0][0] + mat.m[1][1] + mat.m[2][2] + 1;
	    if( trace > 0 ) {// I changed M_EPSILON to 0
// 	      T s = 0.5 / std::sqrt(trace+ 1.0f);
	      T s = 0.5 / std::sqrt(trace);
	      w = 0.25 / s;
	      x = ( mat.m[2][1] - mat.m[1][2] ) * s;
	      y = ( mat.m[0][2] - mat.m[2][0] ) * s;
	      z = ( mat.m[1][0] - mat.m[0][1] ) * s;
	    } else {
	      if ( mat.m[0][0] > mat.m[1][1] && mat.m[0][0] > mat.m[2][2] ) {
		T s = 2.0 * std::sqrt( 1.0 + mat.m[0][0] - mat.m[1][1] - mat.m[2][2]);
		w = (mat.m[2][1] - mat.m[1][2] ) / s;
		x = 0.25 * s;
		y = (mat.m[0][1] + mat.m[1][0] ) / s;
		z = (mat.m[0][2] + mat.m[2][0] ) / s;
	      } else if (mat.m[1][1] > mat.m[2][2]) {
		T s = 2.0 * std::sqrt( 1.0 + mat.m[1][1] - mat.m[0][0] - mat.m[2][2]);
		w = (mat.m[0][2] - mat.m[2][0] ) / s;
		x = (mat.m[0][1] + mat.m[1][0] ) / s;
		y = 0.25f * s;
		z = (mat.m[1][2] + mat.m[2][1] ) / s;
	      } else {
		T s = 2.0 * std::sqrt( 1.0 + mat.m[2][2] - mat.m[0][0] - mat.m[1][1] );
		w = (mat.m[1][0] - mat.m[0][1] ) / s;
		x = (mat.m[0][2] + mat.m[2][0] ) / s;
		y = (mat.m[1][2] + mat.m[2][1] ) / s;
		z = 0.25 * s;
	      }
	    }
	  
	}

   
   
};






















#endif

