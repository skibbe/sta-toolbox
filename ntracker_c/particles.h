#ifndef PARTICLES_H
#define PARTICLES_H




#include "voxgrid.h"
// #include "mcpu_volume.h"
#include "mhs_pool.h"
#include "mhs_vector.h"
#include "rand_help.h"

template <typename T,typename TData,int Dim>
class CData;

#define _DEBUG_PARTICLE_HISTORY_

enum class PARTICLE_TYPES : unsigned int
{
    PARTICLE_SEGMENT=0,
    PARTICLE_BIFURCATION=1,
    PARTICLE_BIFURCATION_CENTER=2,
    PARTICLE_BLOB=3,
    PARTICLE_UNDEFINED,
};


#ifdef _DEBUG_PARTICLE_HISTORY_
enum class PARTICLE_HISTORY : unsigned int
{
    PARTICLE_HISTORY_MOVE,
    PARTICLE_HISTORY_SCALE,
    PARTICLE_HISTORY_ROTATE,
    PARTICLE_HISTORY_NONE,
};
#endif



enum class CONNECTION_STATE : std::size_t
{
    CONNECTION_STATE_NONE=0,
    CONNECTION_STATE_TERMINAL=1,
    CONNECTION_STATE_SEGMENT=2,
    CONNECTION_STATE_BIF_TERMINAL=3,
    CONNECTION_STATE_BIF_SEGMENT=4,
    CONNECTION_STATE_UNKNOWN=1000,
    CONNECTION_STATE_COUNT
};



#include "edges.h"
#include "proposals.h"


#ifdef  D_USE_GUI
	  template<typename T>
	  class GuiPoints
	  {
	  public:
	      static int activity_mode;
	      
	      
	    
	      void clear() {
		  //is_focus_point=false;
		  clear_selection();
		  snapshot_connected_egde_cost=-1;
		  snapshot_edge_proposed_costs[0]=
		      snapshot_edge_proposed_costs[1]=
			  snapshot_edge_proposed_costs[2]=
			      snapshot_edge_proposed_costs[3]=
				  snapshot_edge_proposed_costs[4]=
				      snapshot_edge_proposed_costs[5]=0;
				      last_accepted=0;
				      depth=0;
				      
	      }

	      inline void clear_selection() {
		  is_selected=false;
		  selection_id=-1;
	      }
	      
	      void touch(PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED)
	      {
		
		switch (activity_mode)
		{
		  case 1:
		  {
		    if ((proposal==PROPOSAL_TYPES::PROPOSAL_CONNECT)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN)||
		    (proposal==PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE))
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		  case 2:
		  {
		    if ((proposal==PROPOSAL_TYPES::PROPOSAL_BIRTH)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_CONNECT_BIRTH)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_INSERT_BIRTH)
		    )
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		  case 3:
		  {
		    if (
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_C)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_L)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_S)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH_X)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_C)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_L)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_S)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH_X)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_CHANGE)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_RECONN)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_BIRTH)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_TRACK_DEATH)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_DEATH)||
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BIRTH)
		    )
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		  case 4:
		  {
		    if (
		      (proposal!=PROPOSAL_TYPES::PROPOSAL_MOVE)&&
		      (proposal!=PROPOSAL_TYPES::PROPOSAL_ROTATE)&&
		      (proposal!=PROPOSAL_TYPES::PROPOSAL_SCALE)
		    )
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		  case 5:
		  {
		    if (
		      (proposal==PROPOSAL_TYPES::PROPOSAL_MOVE)
		    )
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		   case 6:
		  {
		    if (
		    (proposal==PROPOSAL_TYPES::PROPOSAL_CONNECT_SCRAMBLE))
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		   case 7:
		  {
		    if (
		    (proposal==PROPOSAL_TYPES::PROPOSAL_CONNECT_SMOOTH))
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		    case 8:
		  {
		    if (
		    //(proposal==PROPOSAL_TYPES::PROPOSAL_FLIP_TERM))
		      (proposal==PROPOSAL_TYPES::PROPOSAL_BIFURCATION_BRIDGE_BIRTH))
		    {
		      last_accepted=global_timer.get_total_runtime();
		    }
		  }break;
		  default:
		  {
		    last_accepted=global_timer.get_total_runtime();
		  }break;
		
		}
		
		
	      }

	      GuiPoints() {
		  clear();
		  touch();
	      };
	      bool is_selected;
	      //bool is_focus_point;
	      int selection_id;
	      
	      double last_accepted;
	      
	      
	      std::size_t depth;


	      T snapshot_connected_egde_cost;
	      T snapshot_edge_proposed_costs[6];
	      
	      
	      

	  };

	  template<typename T>
	  int GuiPoints<T>::activity_mode=0;
#endif

	  
	  
#ifdef  D_USE_TOOL
	  template<typename T,int Dim>
	  class ToolPoints
	  {
	  public:
	      bool delete_me;
	      std::size_t array_indx;
// 	      int path_id;
// 	      
// 	    
// 	      void clear() {
// 		 
// 				      
// 	      }
// 
// 	      Points<T,Dim> * pt;
// 
// 	      ToolPoints(Points<T,Dim> & pt) {
// 		  this->pt=&pt;
// 		  clear();
// 	      };
// 	      
// 	      void store_attribs()
// 	      {
// 		path_id=pt->_path_id;
// 	      };
// 	      void restore_attribs()
// 	      {
// 		pt->_path_id=path_id;
// 	      };
	      
	  };

	  
#endif

// static int track_point_id=0;

class Constraints;
template<typename T,int Dim> class Connection;
template<typename T,int Dim> class Connections;

template<typename T,int Dim>
class Points: public OctPoints< T, Dim >
#ifdef  D_USE_GUI
    , public GuiPoints<T>
#endif
#ifdef  D_USE_TOOL
  , public ToolPoints<T,Dim>
#endif
{

    friend pool<Points>;
    friend Connections< T, Dim >;
    friend Constraints;

    int life;
   
private:
    void clear() {
#ifdef  D_USE_GUI
        GuiPoints<T>::clear();
#endif
        OctPoints< T, Dim >::clear();
        tracker_birth=0;
//         track_me_id=track_point_id;
        endpoint_connections[0]=0;
        endpoint_connections[1]=0;

        //TODO 26.3.2014
        terminal_indx=-1;
        bifurcation_indx=-1;
        bifurcation_center_indx=-1;
	
	 _path_id=-1;

        for ( int b=0; b<2; b++ )
            for ( int a=0; a<maxconnections; a++ ) {
                endpoints[b][a]->connected=NULL;
            }

        //particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
	particle_type=Points<T,Dim>::default_type;
	
#ifdef _DEBUG_PARTICLE_HISTORY_
        last_successful_proposal=PARTICLE_HISTORY::PARTICLE_HISTORY_NONE;
#endif
        life++;
	
// 	time_stamp=0;
	
    }
//     std::size_t time_stamp;
    
    


    // if terminal then terminal array index
    int terminal_indx;
    int bifurcation_indx;
    int bifurcation_center_indx;

    static T particle_connection_state_cost_scale[5];
    static T particle_connection_state_cost_offset[5];
    
    static bool sample_scale_temp_dependent;
    
     static  bool no_bifurcation_self_collision;
     
     static  T bifurcation_panelty;
    
    
    


    // used for graph search like loop detection
//     int track_me_id;

    T scale;
    class Vector<T,Dim> direction;
    
    

    
    


//     void update_endpoint2 ( int i ) {
//         if ( thickness>0 ) {
//             T* ept= endpoints_pos[i].v;
//             const T * dir= direction.v;
//             T  fact=side_sign[i]* ( endpoint_nubble_scale+thickness );
//             const T * pos=this->position.v;
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept= ( *dir ) *fact+ ( *pos );
//         } else {
//             T* ept= endpoints_pos[i].v;
//             const T * dir= direction.v;
// 
//             T  fact=side_sign[i]* ( endpoint_nubble_scale+std::max ( Points<T,Dim>::min_thickness,-thickness*scale ) );
// 
//             const T * pos=this->position.v;
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept= ( *dir ) *fact+ ( *pos );
//         }
//     }

    void update_endpoint2 ( int i ) {
//         if ( thickness>0 ) {
//             T* ept= endpoints_pos[i].v;
//             const T * dir= direction.v;
//             T  fact=side_sign[i]* ( endpoint_nubble_scale+thickness );
//             const T * pos=this->position.v;
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept= ( *dir ) *fact+ ( *pos );
//         } else {
//             T* ept= endpoints_pos[i].v;
//             const T * dir= direction.v;
// 
//             T  fact=side_sign[i]* ( endpoint_nubble_scale+std::max ( Points<T,Dim>::min_thickness,-thickness*scale ) );
// 
//             const T * pos=this->position.v;
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept++= ( *dir++ ) *fact+ ( *pos++ );
//             *ept= ( *dir ) *fact+ ( *pos );
//         }
	    endpoints_pos[i]=this->position+direction*(side_sign[i]*get_thickness()*endpoint_nubble_scale);
      
    }


   


public:

   inline void update_endpoints2() {
        update_endpoint2 ( 0 );
        update_endpoint2 ( 1 );
    }
  
//     void set_stamp ( std::size_t s ) {
// 	time_stamp=s;
//     };
//     std::size_t get_stamp() const {
// 	return time_stamp;
//     }

    const Vector<T,Dim> & get_position() const {
        return this->position;
    }
    const Vector<T,Dim> & get_direction() const {
        return this->direction;
    }
    const T & get_scale() const {
        return this->scale;
    }

    void set_position ( const  Vector<T,Dim> & pos ) {
        this->position=pos;
        update_endpoints2();
    }


    void set_scale ( T s ) {
        this->scale=s;
        update_endpoints2();
//         printf("sdsd");
    }

    template<typename Ta>
    void set_direction ( const Vector<Ta,Dim> & dir ) {
        this->direction=dir;
        update_endpoints2();
    }


    template<typename Ta, typename Tb>
    void set_pos_dir_scale ( const Vector<Ta,Dim> & pos,
                             const Vector<Tb,Dim> & dir,
                             T s ) {
        this->position=pos;
        this->direction=dir;
        this->scale=s;
        update_endpoints2();
    }

    void set_dir_scale (
        const Vector<T,Dim> & dir,
        T s ) {

        this->direction=dir;
        this->scale=s;
        update_endpoints2();
    }

    void set_pos_scale ( const Vector<T,Dim> & pos,
                         T s ) {
        this->position=pos;
        this->scale=s;
        update_endpoints2();
    }
    
    
    template<typename Ta, typename Tb>
    void set_pos_dir ( const Vector<Ta,Dim> & pos,
                             const Vector<Tb,Dim> & dir) {
        this->position=pos;
        this->direction=dir;
        update_endpoints2();
    }

    inline CONNECTION_STATE get_connection_weight() const {
        bool check_connect[2];
        check_connect[0]= ( endpoint_connections[0]>0 );
        check_connect[1]= ( endpoint_connections[1]>0 );


        if ( check_connect[0]&&check_connect[1] ) {
            if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION ) {
                return CONNECTION_STATE::CONNECTION_STATE_BIF_SEGMENT;
            } else {
                return CONNECTION_STATE::CONNECTION_STATE_SEGMENT;
            }
        } else if ( check_connect[0]||check_connect[1] ) {
            if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION ) {
                return CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL;
            } else {
                return CONNECTION_STATE::CONNECTION_STATE_TERMINAL;
            }
        }

        return CONNECTION_STATE::CONNECTION_STATE_NONE;
    }



    bool freezed;
    int  freezed_endpoints[2];
    bool freeze_topology;

    bool isfreezed() const {
        return freezed;
    }

    bool is_protected_topology() const {
        return freeze_topology;
    }
    
    int get_life() {
        return life;
    };

#ifdef _DEBUG_PARTICLE_HISTORY_
    PARTICLE_HISTORY last_successful_proposal;
#endif

    bool is_terminal() {
        return ( terminal_indx>-1 );
    };
    
    bool is_segment() {
        return ( (endpoint_connections[0]==1) && (endpoint_connections[1]==1));
    };


    int get_num_conneced_bifurcations() {
        if ( particle_type== ( PARTICLE_TYPES::PARTICLE_BIFURCATION ) ) {
            int found=0;
            for ( int i=0; i<2; i++ ) {
                if ( ( endpoints[i][0]->connected!=NULL ) && ( endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
                    found++;
                }
            }
            sta_assert_error ( found>0 );
            return found;
        }
        sta_assert_error ( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
        return 0;
    }

    
    bool check_consistency3()
    {

	   
            if ( particle_type!=PARTICLE_TYPES::PARTICLE_BLOB) {
	      
		
                if ( ( ( this->endpoint_connections[0]==0 ) || ( this->endpoint_connections[1]==0 ) ) &&
                        ( this->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) ) {
                    sta_assert_error ( this->has_owner ( voxgrid_conn_can_id ) );
                }

                switch ( this->particle_type ) {
                case PARTICLE_TYPES::PARTICLE_UNDEFINED: {
                    throw mhs::STAError ( "found PARTICLE_TYPES::PARTICLE_UNDEFINED" );
                }
                break;
                case PARTICLE_TYPES::PARTICLE_SEGMENT: {
                    sta_assert_error ( this->has_owner() );
                    for ( int i=0; i<2; i++ ) {
                        if ( this->endpoint_connections[i]>0 ) {
                            sta_assert_error ( this->endpoint_connections[i]==1 );
                            sta_assert_error ( this->endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_SEGMENT );
                        }
                    }
                }
                break;
                case PARTICLE_TYPES::PARTICLE_BIFURCATION: {
                    sta_assert_error ( this->has_owner() );
                    sta_assert_error ( this->get_num_connections() >0 );
                    bool foundbif=false;
                    for ( int i=0; i<2; i++ ) {
                        if ( this->endpoint_connections[i]>0 ) {
                            sta_assert_error ( this->endpoint_connections[i]==1 );
                            if ( this->endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) {
                                foundbif=true;
                            }
                        }
                    }
                    sta_assert_error ( foundbif );
                }
                break;
                case PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER: {
		    sta_assert_error ( this->has_owner(voxgrid_bif_center_id) ) ;
                    //sta_assert_error ( ( !this->has_owner() ) || ( options.bifurcation_particle_hard&& ( this->has_owner() ) ) );
                    sta_assert_error ( this->get_num_connections() ==3 );
                    int bslot=Points<T,Dim>::bifurcation_center_slot;
                    sta_assert_error ( this->endpoint_connections[bslot]==3 );
                    for ( int i=0; i<3; i++ ) {
                        sta_assert_error ( this->endpoints[bslot][i]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
                    }
                }
                break;
                }
            }
            return  true;
    }
    
    bool check_consistency2(bool verbose=false) {
        bool show_all=false;
        int check1[2];
        int check2[2];
        int check3[2];
        int check4[2];
        int check5[2];

        for ( int s=0; s<2; s++ ) {
            check1[s]= ( endpoint_connections[s] );
            check2[s]=0;
            check3[s]=0;
            check4[s]=0;
            check5[s]=0;
            for ( int a=0; a<maxconnections; a++ ) {
                check2[s]+=(endpoints_[s][a].connected!=NULL);
                check3[s]+=(endpoints_[s][a].connection!=NULL);
                if ( a<=check1[s] ) {
                    check4[s]+=(endpoints[s][a]->connected!=NULL);
                } else {
                    check5[s]+=(endpoints[s][a]->connected!=NULL);
                }
            }

            if ( ( check1[s]!=check2[s] ) || ( check1[s]!=check3[s] ) || ( check1[s]!=check3[s] ) || ( check1[s]!=check4[s] ) || ( check2[s]!=check4[s] ) || ( check3[s]!=check4[s] ) ) {
                show_all=true;
            }
        }

        if ( show_all || verbose) {
            printf ( "-----------------\n" );
            for ( int s=0; s<2; s++ ) {
                printf ( "%d: %d %d %d %d,%d\n",s,check1[s],check2[s],check3[s],check4[s],check5[s] );
            }
            return false;
        }
        return true;

    }

    void check_consistency(bool bifurcations_should_be_in_grid=false) {
        if ( ( endpoint_connections[0]!=0 ) != ( endpoint_connections[1]!=0 ) ) {
            //printf("%d %d %d %d\n",tracker_birth,terminal_indx,endpoint_connections[0],endpoint_connections[1]);
            if ( get_num_connections() ==1 ) {
                sta_assert_error ( terminal_indx>-1 );
            } else {
                sta_assert_error ( bifurcation_center_indx>-1 );
            }
        }
        
      


        int s=Points<T,Dim>::bifurcation_center_slot;

        for ( int a=0; a<2; a++ ) {
            if ( endpoint_connections[a]>1 ) {
                sta_assert_error ( endpoint_connections[1-a]==0 );
                sta_assert_error ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
                sta_assert_error ( a==s );
                sta_assert_error ( endpoint_connections[a]==3 );
                for ( int i=0; i<3; i++ ) {
                    sta_assert_error ( endpoints[a][i]->connected->point->particle_type== ( PARTICLE_TYPES::PARTICLE_BIFURCATION ) );
                    sta_assert_error ( endpoints[a][i]->connection->edge_type== ( EDGE_TYPES::EDGE_BIFURCATION ) );
                    sta_assert_error ( bifurcation_center_indx>-1 );
                }
            }
        }

        if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {

            sta_assert_error ( ( endpoint_connections[s]==3 ) );
	    sta_assert_error(((!bifurcations_should_be_in_grid)||(this->has_owner())));
        }


        for ( int b=0; b<2; b++ )
            for ( int a=0; a<endpoint_connections[b]; a++ ) {


                sta_assert_error (
                    endpoints[b][a]->connected!=NULL
                );
                sta_assert_error (
                    endpoints[b][a]->connected->connected==endpoints[b][a]
                );

                sta_assert_error ( endpoints[b][a]->connection!=NULL );

                if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION ) {
                    bool checkA= ( endpoint_connections[b]==1 ) && ( endpoints[b][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
                    bool checkB= ( endpoint_connections[1-b]==1 ) && ( endpoints[1-b][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
                    sta_assert_error ( checkA||checkB );
                }

                sta_assert_error (
                    ( endpoints[b][a]->connection->pointA==endpoints[b][a] ) ||
                    ( endpoints[b][a]->connection->pointB==endpoints[b][a] )
                );
            }
    }

    //no data term reference
    class Vector<T,Dim> ref_position;     
    T ref_scale;
    
    
    int tracker_birth;


    // primary attributes (in addition to position)

    PARTICLE_TYPES particle_type;

    class Vector<T,Dim>     endpoints_pos[2];
    int endpoint_connections[2];


    // global  secondary attributes
    static int point_weight;
    static int maxconnections;
    static T thickness;
    static T min_thickness;
    static T endpoint_nubble_scale;
    
    
    static T _dynamic_thicknes_fact[max_tracking_treads];
    
    static void static_init()
    {
      for (std::size_t t=0;t<max_tracking_treads;t++)
      {
	Points<T,Dim>:: _dynamic_thicknes_fact[t]=T(0);
      }
    }

    // secondary attributes
    T point_cost;
    T temp;
    T saliency;

    int _path_id=-10000;
    mutable long int _point_id;


    // some constants
    static const int a_side=0;
    static const int b_side=1;
    
    static  PARTICLE_TYPES default_type; 

    static const T side_sign[2];//={+1,-1};

    static const int bifurcation_center_slot=0;



    inline T compute_cost3 ( T temp,std::size_t id_connection=static_cast<std::size_t> ( CONNECTION_STATE::CONNECTION_STATE_UNKNOWN ) ) const {
        if ( id_connection== ( static_cast<std::size_t> ( CONNECTION_STATE::CONNECTION_STATE_UNKNOWN ) ) ) {
            id_connection=static_cast<std::size_t> ( get_connection_weight() );
        }


        sta_assert_error ( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
	
// 	T scale_w=compute_cost_nonweighted();
	
// 	if ( id_connection==static_cast<std::size_t> ( CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL ) ) {
//             return scale_w*(particle_connection_state_cost_scale[id_connection] 
//                    +temp*particle_connection_state_cost_offset[id_connection])+scale/ ( temp*temp ); // AUG 14 (yea)
//                   
//         }
// 
//         return scale_w*(particle_connection_state_cost_scale[id_connection] 
//                    +temp*particle_connection_state_cost_offset[id_connection]);
		  
		  
	if (sample_scale_temp_dependent)	  
	{
	  
	  // MAI 18 (yea)
	  if (true) //disable extra bif panelty
	  {
	    if ( id_connection==static_cast<std::size_t> ( CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL ) ) {
	      return compute_cost_nonweighted() *particle_connection_state_cost_scale[id_connection] // AUG 14
		    +scale/ ( temp*temp ); // AUG 14 (yea)
		    //+temp*particle_connection_state_cost_offset[id_connection]+scale/ ( temp*temp ); 
	    }
	  }

        return compute_cost_nonweighted() *particle_connection_state_cost_scale[id_connection] // AUG 14
                ; // AUG 14 (yea)
		  //+temp*particle_connection_state_cost_offset[id_connection]; // AUG 14 (yea)	  
	}else
	{

        if ( id_connection==static_cast<std::size_t> ( CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL ) ) {
            return compute_cost_nonweighted() *particle_connection_state_cost_scale[id_connection] // AUG 14
                   +temp*temp*particle_connection_state_cost_offset[id_connection]+scale/ ( temp*temp ); // AUG 14 (yea)
                   //+temp*particle_connection_state_cost_offset[id_connection]+scale/ ( temp*temp ); 
        }

        return compute_cost_nonweighted() *particle_connection_state_cost_scale[id_connection] // AUG 14
                +temp*temp*particle_connection_state_cost_offset[id_connection]; // AUG 14 (yea)
		  //+temp*particle_connection_state_cost_offset[id_connection]; // AUG 14 (yea)
	}	  
// 	   if ( id_connection==static_cast<std::size_t> ( CONNECTION_STATE::CONNECTION_STATE_BIF_TERMINAL ) ) {
//             return compute_cost_nonweighted() *particle_connection_state_cost_scale[id_connection] 
//                    +compute_temp_weight(temp)*particle_connection_state_cost_offset[id_connection]+scale/ ( temp*temp ); 
//         }
// 
//         return compute_cost_nonweighted() *particle_connection_state_cost_scale[id_connection] 
//                 +compute_temp_weight(temp)*particle_connection_state_cost_offset[id_connection];  
    }

    inline T compute_cost_nonweighted() const {
        switch ( point_weight ) {
        case 1:
            return scale;
        case 2:
            return scale*scale;
        case 3:
            return scale*scale*scale;
        default:
            return 1;
        }
    }
    
    
    inline T compute_temp_weight(const T & tmp) const {
        switch ( point_weight ) {
        case 1:
            return tmp;
        case 2:
            return tmp*tmp;
        case 3:
            return tmp*tmp*tmp;
        default:
            return 1;
        }
    }

    
    


    class CEndpoint
    {
    public:
        int  side;
        int  endpoint_side_indx;
        Points * point;
        class Connection<T,Dim> * connection;
        CEndpoint * connected;

        CEndpoint ** opposite_slots;
        CEndpoint() : point ( NULL ),connected ( NULL ) {};
        Vector<T,Dim> * position;
        int * endpoint_connections;
	
	Vector<T,Dim> get_slot_direction() const {
	  sta_assert_debug0(point!=NULL);
	  return (point->get_direction()*Points< T, Dim >::side_sign[side]);
	};
	
	T get_slot_sign(){return Points< T, Dim >::side_sign[side];};
	
	
	CEndpoint * getfreehub ( ) 	
	{
	  return (point->getfreehub ( side ) );
	}

    };

    /// TODO: each endpoint in independend memory blocks so that
    /// their position can easily be switched (aslso aupdate remove edge!!)
    ///GOAL: if only one edge is connected, then only to the FIRST slot
    CEndpoint * endpoints[2][4];
    CEndpoint   endpoints_[2][4];


    void bifurcation_center_update ( const  std::size_t shape[],bool & out_of_bounce, bool debug = false ) {
        if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {

            sta_assert_debug0 ( endpoint_connections[bifurcation_center_slot]==3 );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][1]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][2]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION );

            sta_assert_debug0 ( endpoints[bifurcation_center_slot][0]->connected->point->bifurcation_indx>-1 );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][1]->connected->point->bifurcation_indx>-1 );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][2]->connected->point->bifurcation_indx>-1 );

            sta_assert_debug0 ( endpoints[bifurcation_center_slot][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][1]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION );
            sta_assert_debug0 ( endpoints[bifurcation_center_slot][2]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION );

            Points<T,Dim>::CEndpoint * epA=endpoints[bifurcation_center_slot][0]->connected;
            Points<T,Dim>::CEndpoint * epB=endpoints[bifurcation_center_slot][1]->connected;
            Points<T,Dim>::CEndpoint * epC=endpoints[bifurcation_center_slot][2]->connected;

	    ///NOTE: ednpoints would be change under tmp dependent thiockens
//             compute_inner_circle (
//                 *epA->position, // seitenhalbierende pos
//                 *epB->position,
//                 *epC->position,
//                 epA->point->scale*CEdgecost<T,Dim>::side_scale, //seiten laenge
//                 epB->point->scale*CEdgecost<T,Dim>::side_scale,
//                 epC->point->scale*CEdgecost<T,Dim>::side_scale,
//                 this->position,
//                 scale,debug );
	    compute_inner_circle (
                epA->point->get_position(), // seitenhalbierende pos
                epB->point->get_position(),
                epC->point->get_position(),
                epA->point->scale*CEdgecost<T,Dim>::side_scale, //seiten laenge
                epB->point->scale*CEdgecost<T,Dim>::side_scale,
                epC->point->scale*CEdgecost<T,Dim>::side_scale,
                this->position,
                scale,debug );	    


	    if (no_bifurcation_self_collision)
	    {
	       scale=(epA->point->get_thickness()+epB->point->get_thickness()+epC->point->get_thickness())/3;
	    }else
	    {
	      scale=std::max ( scale,T(0.05) );
	    }
	    

            Vector <T,Dim> & position=this->position;
            if ( ( position[0]<0 ) ||
                    ( position[1]<0 ) ||
                    ( position[2]<0 ) ||
                    ( ! ( position[0]<shape[0] ) ) ||
                    ( ! ( position[1]<shape[1] ) ) ||
                    ( ! ( position[2]<shape[2] ) ) )
            {
                out_of_bounce=true;
                return;
            }
            out_of_bounce=false;
	    
	    //sta_assert_error(!this->has_owner(0));
	    
            if ( this->has_owner() ) {
	      
	      
// 	      sta_assert_error(this->has_owner());
                try 
                {
                this->update_pos();
	      } catch (mhs::STAError error)
              {
		  error<<"\n bifurcation_center_update fails\n";
                  throw error;
              }
		  
            }

        } else {
            sta_assert_debug0 ( endpoint_connections[bifurcation_center_slot]<2 );
        }
    }

    //returns true if new particel centers needs to be checked for collision
    bool bifurcation_center_neighbourhood_update ( const std::size_t shape[],bool & out_of_bounce ) {
        sta_assert_debug0 ( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );

        if ( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION ) {
            return false;
        }

        out_of_bounce=false;
        bool found=false;
        for ( int i=0; i<2; i++ ) {
            if ( ( endpoint_connections[i]==1 ) && ( endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) ) {
                sta_assert_debug0 ( endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER );
                endpoints[i][0]->connected->point->bifurcation_center_update ( shape,out_of_bounce );
                if ( out_of_bounce ) {
                    return false;
                }
                found=true;
            }
        }
        return found;
    }



    T get_thickness(int thread_id=-1) const {
//         if (( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) && ( particle_type!=PARTICLE_TYPES::PARTICLE_BLOB )) {
//             return ( thickness>0 ) ? Points<T,Dim>::thickness : std::max ( Points<T,Dim>::min_thickness, ( ( -Points<T,Dim>::thickness*scale ) ) );
//         }
      
        if (( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) && ( particle_type!=PARTICLE_TYPES::PARTICLE_BLOB )) {
	  
	    if (thread_id<0){
	      thread_id=get_thread_id();
	    }
	    if (Points<T,Dim>::_dynamic_thicknes_fact[thread_id]>0)
	    {
		T  w=std::min(Points<T,Dim>::_dynamic_thicknes_fact[thread_id]/scale,T(1));
// 		printf("%f %f %f\n",w,scale,Points<T,Dim>::dynamic_thicknes_fact);
		return  ( thickness>0 )? Points<T,Dim>::thickness : std::max ( Points<T,Dim>::min_thickness, ( ( (w-(1-w)*(Points<T,Dim>::thickness))*scale ) ) ); 
	    }else
	    {
		return ( thickness>0 ) ? Points<T,Dim>::thickness : std::max ( Points<T,Dim>::min_thickness, ( ( -Points<T,Dim>::thickness*scale ) ) ); 
	    }
	    
        }
//         
//       if (( particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) && ( particle_type!=PARTICLE_TYPES::PARTICLE_BLOB )) {
//             return ( thickness>0 ) ? (Points<T,Dim>::thickness*Points<T,Dim>::dynamic_thicknes_fact) : std::max ( Points<T,Dim>::min_thickness, ( ( -(Points<T,Dim>::thickness*Points<T,Dim>::dynamic_thicknes_fact)*scale ) ) );
//         }
        
        
        return scale;
    }
    
    static T predict_scale_from_thicknes(T thickness)
    {
            return ( thickness>0 ) ? -1 : (-thickness/(Points<T,Dim>::thickness));
    }

    static T predict_thickness_from_scale(T scale,int thread_id=-1)
    {
            if (thread_id<0){
	      thread_id=get_thread_id();
	    }
	    if (Points<T,Dim>::_dynamic_thicknes_fact[thread_id]>0)
	    {
		T  w=std::min(Points<T,Dim>::_dynamic_thicknes_fact[thread_id]/scale,T(1));
		return  ( thickness>0 )? Points<T,Dim>::thickness : std::max ( Points<T,Dim>::min_thickness, ( ( (w-(1-w)*(Points<T,Dim>::thickness))*scale ) ) ); 
	    }else
	    {
		return ( thickness>0 ) ? Points<T,Dim>::thickness : std::max ( Points<T,Dim>::min_thickness, ( ( -Points<T,Dim>::thickness*scale ) ) ); 
	    }
    }    
    
    
    static T predict_outer_sphere_rad(T scale) {
        return std::max(scale,( thickness>0 ) ? (Points<T,Dim>::thickness) : std::max ( Points<T,Dim>::min_thickness, ( ( -Points<T,Dim>::thickness*scale ) ) ));
        
    }
    


    void optimal_sucessor (
        Vector<T,Dim> &optimal_pos,
        Vector<T,Dim> &track_direction,
        int& optimal_sideA,
        int& optimal_sideB,
        int endpoint=-1 ) {

        if ( endpoint==-1 ) {
#ifdef _BIFURCATION_POINTS_CANNOT_TERMINAL_
            if ( endpoint_connections[0]==1 ) {
                endpoint=1;
            }
            else if ( endpoint_connections[1]==1 ) {
                endpoint=0;
            } else {
                sta_assert_error ( 1==0 );
            }
#else
            endpoint= ( endpoint_connections[0]>0 ) ? 1 : 0;
            sta_assert_error ( endpoint_connections[endpoint]==0 );
#endif
        }

        if ( endpoint==1 ) {
            track_direction=direction*Points<T,Dim>::side_sign[1];
            optimal_sideB=0;
            optimal_sideA=1;
        } else {
            track_direction=direction*Points<T,Dim>::side_sign[0];
            optimal_sideB=1;
            optimal_sideA=0;
        }
//         if ( thickness>0 )
//             optimal_pos= ( this->position )
//                          +track_direction* ( Points<T,Dim>::thickness*2+scale/T ( 2.0 ) );
//         else
//             optimal_pos= ( this->position )
//                          +track_direction*std::max ( Points<T,Dim>::min_thickness, ( ( -Points<T,Dim>::thickness*scale ) ) ) *2;

	
            optimal_pos= ( this->position )
                         +track_direction* ( get_thickness()*2+scale/T ( 2.0 ) );
        
    }


    CEndpoint * getfreehub ( int i ) {
        CEndpoint * hub=NULL;
        for ( int a=0; a<maxconnections; a++ ) {
            if ( endpoints[i][a]->connected==NULL ) {
                hub= ( endpoints[i][a] );
                break;
            }
        }
        sta_assert_error ( endpoint_connections[i]<maxconnections );
        sta_assert_error ( hub==endpoints[i][endpoint_connections[i]] );
        return hub;
    }


    Points() : OctPoints< T, Dim >() 
      #ifdef  D_USE_TOOL
	,ToolPoints< T, Dim >(*this)
      #endif
    {
    
        tracker_birth=0;
        terminal_indx=-1;
        bifurcation_indx=-1;

        direction=T ( 0 );
        direction[0]=T ( 1 );
        scale=1;

        sta_assert_error ( maxconnections<5 );

        init_pointer();
       
        _point_id=-1;
        endpoint_connections[0]=0;
        endpoint_connections[1]=0;
        life=0;
        freezed=false;
	freeze_topology=false;
        freezed_endpoints[0]=-1;
        freezed_endpoints[1]=-1;
	
// 	Points<T,Dim>::dynamic_thicknes_fact=0;
    }

    void init_pointer() {
        for ( int n=0; n<maxconnections; n++ ) {

            endpoints_[0][n].side=a_side;
            endpoints_[0][n].point=this;
            endpoints_[0][n].connected=NULL;
            endpoints_[0][n].connection=NULL;
            endpoints_[0][n].position=& ( endpoints_pos[0] );
            endpoints_[0][n].endpoint_connections=& ( endpoint_connections[0] );
            endpoints_[0][n].endpoint_side_indx=n;
            endpoints[0][n]=& ( endpoints_[0][n] );
            endpoints_[1][n].side=b_side;
            endpoints_[1][n].point=this;
            endpoints_[1][n].connected=NULL;
            endpoints_[1][n].connection=NULL;
            endpoints_[1][n].position=& ( endpoints_pos[1] );
            endpoints_[1][n].endpoint_connections=& ( endpoint_connections[1] );
            endpoints_[1][n].endpoint_side_indx=n;
            endpoints[1][n]=& ( endpoints_[1][n] );
        }
        for ( int n=0; n<maxconnections; n++ ) {
            endpoints_[0][n].opposite_slots=endpoints[1];
            endpoints_[1][n].opposite_slots=endpoints[0];
        }
    }

    bool isconnected() const {
        if ( endpoint_connections[0]+endpoint_connections[1]==0 ) {
            return false;
        }
        return true;
    }

    int get_num_connections() const {
        return ( endpoint_connections[0]+endpoint_connections[1] );
    }

    int get_parity() const {
        return ( int ) ( endpoint_connections[0]>0 )
               + ( int ) ( endpoint_connections[1]>0 );
    }

    void set_id ( long int id ) const {
        this->_point_id=id;
    }

    ~Points() {
// 	if (isconnected())
// 	    throw mhs::STAError("removing connected point!");
//       printf("how?");
//       sta_assert_error(false);
    }

    static void set_params ( const mxArray * params ) {
        if ( params==NULL ) {
            return;
        }
        try {
            // might be removed in future
            if ( mhs::mex_hasParam ( params,"particle_endpoint_offset" ) !=-1 ) {
                Points<T,Dim>::endpoint_nubble_scale=mhs::mex_getParam<T> ( params,"particle_endpoint_offset",1 ) [0];
            }

            if ( mhs::mex_hasParam ( params,"particle_side_connections" ) !=-1 ) {
                Points<T,Dim>::maxconnections=mhs::mex_getParam<int> ( params,"particle_side_connections",1 ) [0];
            }

            if ( mhs::mex_hasParam ( params,"particle_point_weight_exponent" ) !=-1 ) {
                Points<T,Dim>::point_weight=mhs::mex_getParam<int> ( params,"particle_point_weight_exponent",1 ) [0];
            }

            if ( mhs::mex_hasParam ( params,"particle_thickness" ) !=-1 ) {
                Points<T,Dim>::thickness=mhs::mex_getParam<T> ( params,"particle_thickness",1 ) [0];
            }

            if ( mhs::mex_hasParam ( params,"particle_min_thickness" ) !=-1 ) {
                Points<T,Dim>::min_thickness=mhs::mex_getParam<T> ( params,"particle_min_thickness",1 ) [0];
            }
            
            if ( mhs::mex_hasParam ( params,"opt_spawn_blobs" ) !=-1 ) {
                    if (mhs::mex_getParam<bool> ( params,"opt_spawn_blobs",1 ) [0])
		    {
		      Points<T,Dim>::default_type=PARTICLE_TYPES::PARTICLE_BLOB;
		    } else
		    {
		       Points<T,Dim>::default_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
		    }
                }
            
            if ( mhs::mex_hasParam ( params,"sample_scale_temp_dependent" ) !=-1 ) {
	         Points<T,Dim>::sample_scale_temp_dependent=mhs::mex_getParam<bool> ( params,"sample_scale_temp_dependent",1 ) [0];
                
	    }
	    
	    if ( mhs::mex_hasParam ( params,"no_bifurcation_self_collision" ) !=-1 ) {
	         Points<T,Dim>::no_bifurcation_self_collision=mhs::mex_getParam<bool> ( params,"no_bifurcation_self_collision",1 ) [0];
                
	    }
                

                if ( mhs::mex_hasParam ( params,"bifurcation_panelty" ) !=-1 ) {
	         Points<T,Dim>::bifurcation_panelty=mhs::mex_getParam<T> ( params,"bifurcation_panelty",1 ) [0];
                
	    }
                
                
            

// 	    printf("AAA");
            if ( mhs::mex_hasParam ( params,"particle_connection_state_cost_scale" ) !=-1 ) {
                memcpy ( particle_connection_state_cost_scale,mhs::mex_getParam<T> ( params,"particle_connection_state_cost_scale",5 ).data(),sizeof ( T ) *5 );
            }

            if ( mhs::mex_hasParam ( params,"particle_connection_state_cost_offset" ) !=-1 ) {
                memcpy ( particle_connection_state_cost_offset,mhs::mex_getParam<T> ( params,"particle_connection_state_cost_offset",5 ).data(),sizeof ( T ) *5 );
            }

        } catch ( mhs::STAError & error ) {
            throw error;
        }
    }


    T e_cost (
        class CEdgecost<T,Dim> & edgecost_fun,
        T edgeL,
        T bifurL,
//         T maxendpointdist,
        typename CEdgecost<T,Dim>::EDGECOST_STATE  & inrange,
	bool check_distance=true,
        bool debug=false, 
	bool under_construction=true // under consttion means that particle flags might not be correct yet
		  )
    {
//         if ( debug ) {
//             printf ( "computing particle edge costs\n" );
//         }

        Points<T,Dim> & npoint=*this;
        inrange=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE;

        T U=0;

        if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) {
            class Points<T,Dim>::CEndpoint * endpoints[3];

            endpoints[0]=npoint.endpoints[bifurcation_center_slot][0]->connected;
            endpoints[1]=npoint.endpoints[bifurcation_center_slot][1]->connected;
            endpoints[2]=npoint.endpoints[bifurcation_center_slot][2]->connected;

            sta_assert_debug0 ( endpoints[0]!=NULL );
            sta_assert_debug0 ( endpoints[1]!=NULL );
            sta_assert_debug0 ( endpoints[2]!=NULL );
            sta_assert_debug0 ( endpoints[0]!=endpoints[1] );
            sta_assert_debug0 ( endpoints[2]!=endpoints[1] );
            sta_assert_debug0 ( endpoints[2]!=endpoints[0] );
	    
	    sta_assert_debug0(npoint.endpoints[bifurcation_center_slot][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION);
	    sta_assert_debug0(npoint.endpoints[bifurcation_center_slot][1]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION);
	    sta_assert_debug0(npoint.endpoints[bifurcation_center_slot][2]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION);

            U+= edgecost_fun.e_cost ( *endpoints[0],
                                      *endpoints[1],
                                      *endpoints[2],
                                      //maxendpointdist,
                                      inrange,
				      check_distance,
                                      debug
                                    ) +bifurL;

#ifdef  D_USE_GUI
            this->snapshot_connected_egde_cost=U;
#endif
            if ( inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) {
                return 0;
            }


            return U;
        }



        for ( int end_id=0; end_id<2; end_id++ ) {

            if ( npoint.endpoint_connections[end_id]>0 ) {
                // it only has one connection (otherwise it would have been a bif-center)
                sta_assert_debug0 ( npoint.endpoint_connections[end_id]==1 );
		

                if ( npoint.endpoints[end_id][0]->connection->edge_type==EDGE_TYPES::EDGE_SEGMENT ) {

		    sta_assert_debug0( npoint.endpoints[end_id][0]->connected!=NULL);
		  
		    
		  
                    class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id][0];
		    
// 		    sta_assert_debug0((((endpointsA->connected->point->particle_type)!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)&&
// 		      ((endpointsA->point->particle_type)!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))||(under_construction));
		    sta_assert_debug0((((endpointsA->connected->point->particle_type)!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)&&
		      ((endpointsA->point->particle_type)!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)));
		    
                    U+=edgecost_fun.e_cost ( *endpointsA,
                                             * ( endpointsA->connected ),
                                             //maxendpointdist,
                                             inrange,check_distance,debug
                                           ) +edgeL;
                    if ( debug )
                        printf ( "seg: %f [dist :%f ]\n",edgecost_fun.e_cost ( *endpointsA,
                                 * ( endpointsA->connected ),
                                 inrange,true
                                                                             ) +edgeL, ( *endpointsA->position-*endpointsA->connected->position ).norm2() );

                } else {
                    sta_assert_debug0 ( ( npoint.endpoints[end_id][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) );
                    U+=npoint.endpoints[end_id][0]->connected->point->e_cost (
                           edgecost_fun,
                           edgeL,
                           bifurL,
                           //maxendpointdist,
                           inrange,
			   check_distance,
                           debug );

                }
                if ( inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE ) {
                    return 0;
                }
            }
        }

#ifdef  D_USE_GUI
        this->snapshot_connected_egde_cost=U;
#endif

        return U;
    }



//     bool is_connected_with ( const Points<T,Dim> * checkpoint ) const {
//         
// 	const Points<T,Dim> & npoint=*this;
// 	
// 	for ( int end_id=0; end_id<2; end_id++ ) {
//             const class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];
// 
//             for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
//                 if ( endpointsA[i].connected!=NULL ) {
//                     if ( endpointsA[i].connected->point==checkpoint ) {
//                         return true;
//                     }
//                 }
//             }
//         }
//         return false;
//     }

    bool is_connected_with ( const Points<T,Dim> * checkpoint ) const {
        
	const Points<T,Dim> & npoint=*this;
	
	for ( int end_id=0; end_id<2; end_id++ ) {
            const class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

            for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                if ( endpointsA[i].connected!=NULL ) {
		    const Points<T,Dim> * cpoint=endpointsA[i].connected->point;
                    if ( cpoint==checkpoint ) {
                        return true;
                    }
                    
                    if (cpoint->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		    {
		       sta_assert_debug0((cpoint->endpoint_connections[Points< T, Dim >::bifurcation_center_slot])==3);
		       for (int j=0;j<3;j++)
		       {
			 const class Points<T,Dim>::CEndpoint *  cpoint2 = cpoint->endpoints[Points< T, Dim >::bifurcation_center_slot][j]->connected;
			sta_assert_debug0(cpoint2!=NULL);
			if ( cpoint2->point==checkpoint ) {
			    return true;
			  }
		       }
		    }
                }
            }
        }
        return false;
    }
    
    
    bool connects_two_bif(int depths=0)
    {
      
     sta_assert_error(depths<2);
     
     if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION )
     {
       if (
	      (endpoints[0][0]->connected!=NULL)
	    &&(endpoints[1][0]->connected!=NULL)
	    &&(endpoints[0][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)
	    &&(endpoints[1][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)
	  )
       {
	return true; 
       }
     }else 
     {
      if ( particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER )
      {
	try 
	{
	 for ( int end_id=0; end_id<3; end_id++ ) 
	 {
	    class Points<T,Dim>::CEndpoint * endpts = endpoints[Points<T,Dim>::bifurcation_center_slot][end_id];
	    sta_assert_error( endpts !=NULL);
	    if (endpts -> connected->point->connects_two_bif(depths+1))
	    {
	     return true; 
	    }
	    
	     //class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints[end_id];
	   
	 }
	}catch(mhs::STAError & error)
        {
            throw error;
        }
	
      }
     }
     return false;
    }
    
    
   template<typename TData>
   bool compute_data_term(class CData<T,TData,Dim> & data_fun, T & data_new) const
   {
     bool success=true;
     if (particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
     {
	 if (bifurcation_panelty>0)
	 {
       
	  T temp_data=0;
	
	  for (int i=0;i<3;i++)  
	  {
	      if (success)
	      {
	      T newenergy_data;
	      
	      const Points< T, Dim > * point=this->endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point;
	      Vector<T,3> dir=this->position-(point->get_position());
	      dir.normalize();
	      const T & scale=point->get_scale();
	      
	      success= data_fun.data_score (
		newenergy_data,
		dir,
		this->position,
		scale);
	      
// 		temp_data+=newenergy_data;
// 		temp_data+=poin_nonweighted();
	      
	      	temp_data=std::max(newenergy_data+point->compute_cost_nonweighted(),temp_data);
		//temp_data+=point->compute_cost_nonweighted();

	
	      }
	  } 
	  data_new=bifurcation_panelty*std::max(temp_data,T(0));
	 }else
	 {
	  data_new=0; 
	 }
	  //data_new=temp_data/T(3); 
	  
     }
     else 
     {
       T newenergy_data;
       success= data_fun.data_score (
		newenergy_data,direction,this->position,scale);
       data_new+=newenergy_data;
     }
     
     return success;
   }
   
   
   
   
  
  bool point_neighborhood_bifurcation_collision_costs(Collision<T,Dim> &   collision_fun_p,
				   OctTreeNode<T,Dim> &		tree,
				   T & costs,
				   T collision_search_rad,
				   T temp
				  )
  {
                for ( int i=0; i<2; i++ ) {
                    if ( ( this->endpoint_connections[i]==1 ) &&
                            ( this->endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION ) ) {
                        sta_assert_debug0 (( this->endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ));

                        Points<T,Dim> & bif_center_point=* ( this->endpoints[i][0]->connected->point );
			T tmp=0;
                        if ( collision_fun_p.colliding (tmp, tree,bif_center_point,collision_search_rad,temp ) ) {
                           return true;
                        }
                        costs+=tmp;
                    }
                }
                return false;
  }
  
  
  bool valid_triangle( class CEdgecost<T,Dim> & edgecost_fun)
  {
      if (particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)		  
      {
		  const class Points< T, Dim >::CEndpoint & endpointA=*(this->endpoints[Points< T, Dim >::bifurcation_center_slot][0]->connected);
		  const class Points< T, Dim >::CEndpoint & endpointB=*(this->endpoints[Points< T, Dim >::bifurcation_center_slot][1]->connected);
		  const class Points< T, Dim >::CEndpoint & endpointC=*(this->endpoints[Points< T, Dim >::bifurcation_center_slot][2]->connected);
		  
		  const Vector<T,Dim> * endpoint_pos[3];
		    endpoint_pos[0]=(endpointA.position);
		    endpoint_pos[1]=(endpointB.position);
		    endpoint_pos[2]=(endpointC.position);
		    
		    const class Points< T, Dim >::CEndpoint * endpoints[3];
		    endpoints[0]=&endpointA;
		    endpoints[1]=&endpointB;
		    endpoints[2]=&endpointC;
		    
		    T endpoint_dist2[3];
		    
		    
		    //sta_assert_error((max_endpoint_dist>0)||(CEdgecost<T,Dim>::max_edge_length<0));
		    
		    T max_edge_length=CEdgecost<T,Dim>::get_current_edge_length_limit();
		    
		 
		      max_edge_length*=max_edge_length;
		      		      
		      for (int i=0;i<3;i++)
		      {
			endpoint_dist2[i]=(*endpoint_pos[i]-*endpoint_pos[(i+1)%3]).norm2();
			T dist2=endpoint_dist2[i];
			if (!(max_edge_length>dist2))
			{
			  
				  sta_assert_error((CEdgecost<T,Dim>::max_edge_length_hard*CEdgecost<T,Dim>::max_edge_length_hard>dist2));
				  sta_assert_error(false);
				  
			} 
		      }
		  
		  const class Points< T, Dim > * points[3];
		  points[0]=endpointA.point;
		  points[1]=endpointB.point;
		  points[2]=endpointC.point;
		  
		    const T & a=CEdgecost<T,Dim>::side_scale*points[0]->get_scale();
		    const T & b=CEdgecost<T,Dim>::side_scale*points[1]->get_scale();
		    const T & c=CEdgecost<T,Dim>::side_scale*points[2]->get_scale();
		    
		    const T * scales[3];
		    scales[0]=&a;
		    scales[1]=&b;
		    scales[2]=&c;

		    
		    
		    

		    
		    //#define _MIN_TRIANGLE_RATIO_ T(1.1)
		    //#define _MIN_TRIANGLE_RATIO_ T(1.01)
		    
		    if ((!edgecost_fun.valid_triangle(a,b,c,_MIN_TRIANGLE_RATIO_))||
		        (!edgecost_fun.valid_triangle(FAST_SQRT(endpoint_dist2[0]),FAST_SQRT(endpoint_dist2[1]),FAST_SQRT(endpoint_dist2[2]),_MIN_TRIANGLE_RATIO_)))
		      return false;
			
		    
      }
      return true;
  }

};



template<typename T,int Dim>
// T Points<T,Dim>::endpoint_nubble_scale=0;
T Points<T,Dim>::endpoint_nubble_scale=1;

template<typename T,int Dim>
const T Points<T,Dim>::side_sign[2]= {+1,-1};

template<typename T,int Dim>
int Points<T,Dim>::maxconnections=3;

template<typename T,int Dim>
T Points<T,Dim>::thickness=-1;

template<typename T,int Dim>
T Points<T,Dim>::min_thickness=0;

template<typename T,int Dim>
int Points<T,Dim>::point_weight=2;


template<typename T,int Dim>
T Points<T,Dim>::particle_connection_state_cost_scale[5]= {T ( 1 ),T ( 1 ),T ( 1 ),T ( 1 ),T ( 1 ) };

template<typename T,int Dim>
T Points<T,Dim>::particle_connection_state_cost_offset[5]= {T ( 0 ),T ( 0 ),T ( 0 ),T ( 0 ),T ( 0 ) };

template<typename T,int Dim>
bool Points<T,Dim>:: sample_scale_temp_dependent=false;


template<typename T,int Dim>
bool Points<T,Dim>:: no_bifurcation_self_collision=false;

template<typename T,int Dim>
T Points<T,Dim>::bifurcation_panelty=1;


template<typename T,int Dim>
PARTICLE_TYPES  Points<T,Dim>::default_type=PARTICLE_TYPES::PARTICLE_SEGMENT;
     
// template<typename T,int Dim>
// T Points<T,Dim>::dynamic_thicknes_fact = T(0);

 template<typename T,int Dim>
T Points<T,Dim>::_dynamic_thicknes_fact[max_tracking_treads]={T(0)};


#endif





