#include <math.h>
#include "mex.h"
//#include "matrix.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>

     #include <gsl/gsl_math.h>
     #include <gsl/gsl_eigen.h>

#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "mhs_error.h"

#define EPSILON 0.00000000000001


//mex CXXFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "  CFLAGS="-fPIC  -fexceptions -O2  -Wall -march=native -fopenmp "   sta_EVGSL.cc -l gsl -l gslcblas -l gomp


template <typename T>
void eigensystem(T * mat,T * p_eval,T * p_evec, int Dim,
			 gsl_eigen_symmv_workspace * w1=NULL);

template <>
void eigensystem<double>(double * mat,
			 double * p_eval,
			 double * p_evec,
			 int Dim,
			 gsl_eigen_symmv_workspace * w1)
{
   gsl_eigen_symmv_workspace * w=w1;
   
  gsl_matrix_view m 
  = gsl_matrix_view_array (mat, Dim, Dim);

  gsl_matrix_view eval_vec  
  = gsl_matrix_view_array (p_evec, Dim, Dim);

  gsl_vector_view eval_val  
  = gsl_vector_view_array (p_eval, Dim);

  gsl_vector *eval = &(eval_val.vector);
  gsl_matrix *evec = &(eval_vec.matrix);

  if (w1==NULL)
    w = gsl_eigen_symmv_alloc (Dim);
	
  gsl_eigen_symmv (&m.matrix, eval, evec, w);
  
  if (w1==NULL)
    gsl_eigen_symmv_free (w);
      
  gsl_eigen_symmv_sort (eval, evec, 
			GSL_EIGEN_SORT_ABS_DESC); 
};

template <typename T>
void symm_eigensystem(
		 T * mat,
		 T * p_eval,
		 T * p_evec,
		 int Dim,
		 int numvoxel,
		 int * shape=NULL);

template <>
void symm_eigensystem<double>(
		 double * mat_img,
		 double * p_eval,
		 double * p_evec,
		 int Dim,
		 int numvoxel,
		 int * shape)
{
//   printf("EV double\n");
 gsl_eigen_symmv_workspace * w=gsl_eigen_symmv_alloc (Dim);
 if (Dim==2)
 {
   
      for (std::size_t idx= 0; idx < numvoxel; idx++)    
      {
	    double mat[4];
	    mat[0]=mat_img[0];
	    mat[3]=mat_img[1];
	    mat[1]=mat[2]=mat_img[2];
	    
	    eigensystem<double>(
	      mat,
	      p_eval,
	      p_evec,2,w);
				
	    std::swap(p_evec[1],p_evec[2]);
	
	    p_eval+=2;
	    mat_img+=3;
	    p_evec+=4;
    }
 }else
 {
    for (std::size_t idx= 0; idx < numvoxel; idx++)    
    {
	  double mat[9];

	  mat[0]=mat_img[0];
	  mat[1]=mat_img[3];
	  mat[2]=mat_img[5];
	  mat[3]=mat_img[3];
	  mat[4]=mat_img[1];
	  mat[5]=mat_img[4];
	  mat[6]=mat_img[5];
	  mat[7]=mat_img[4];
	  mat[8]=mat_img[2];

	  
	  eigensystem<double>(
	    mat,
	    p_eval,
	    p_evec,3,w);
			      
	  std::swap(p_evec[1],p_evec[3]);
	  std::swap(p_evec[2],p_evec[6]);
	  std::swap(p_evec[5],p_evec[7]);
      
	  p_eval+=3;
	  mat_img+=6;
	  p_evec+=9;
  }
 }
   gsl_eigen_symmv_free (w);
}


template <>
void symm_eigensystem<float>(
		 float * mat_img,
		 float * p_eval,
		 float * p_evec,
		 int Dim,
		 int numvoxel,
		 int * shape)
{
// printf("EV float\n");
 if (Dim==2)
 {
   
 //  sta_assert("eigensys float: war noch zu faul");
//       for (std::size_t idx= 0; idx < numvoxel; idx++)    
//       {
// 	    float mat[4];
// 	    mat[0]=mat_img[0];
// 	    mat[3]=mat_img[1];
// 	    mat[1]=mat[2]=mat_img[2];
// 	    
// 	    eigensystem<double>(
// 	      mat,
// 	      p_eval,
// 	      p_evec,2);
// 				
// 	    std::swap(p_evec[1],p_evec[2]);
// 	
// 	    p_eval+=2;
// 	    mat_img+=3;
// 	    p_evec+=4;
//     }
 }else
 {
    int slice_dim=shape[1]*shape[2];
   
//  for (std::size_t idx= 0; idx < numvoxel; idx++)    
    #pragma omp parallel for
    for (int z=0;z<shape[0];z++)
    {
      gsl_eigen_symmv_workspace * w=gsl_eigen_symmv_alloc (Dim);
      double mat[9];
      double p_evecd[9];
      double p_evald[3]; 
      float* mat_img2=mat_img+z*6*slice_dim;
      float* p_eval2=p_eval+z*3*slice_dim;
      float* p_evec2=p_evec+z*9*slice_dim;
    for (int i=0;i<slice_dim;i++)
    {
	  
// 	  mat[0]=mat_img2[0];//xx
// 	  mat[1]=mat_img2[3];//xy
// 	  mat[2]=mat_img2[4];
// 	  mat[3]=mat_img2[3];
// 	  mat[4]=mat_img2[1];//yy
// 	  mat[5]=mat_img2[5];
// 	  mat[6]=mat_img2[4];
// 	  mat[7]=mat_img2[5];
// 	  mat[8]=mat_img2[2];//zz
	  
	  mat[0]=mat_img2[0];//xx
	  mat[1]=mat_img2[3];//xy
	  mat[2]=mat_img2[5];//xz
	  mat[3]=mat_img2[3];//yx
	  mat[4]=mat_img2[1];//yy
	  mat[5]=mat_img2[4];//yz
	  mat[6]=mat_img2[5];//zx
	  mat[7]=mat_img2[4];//zy
	  mat[8]=mat_img2[2];//zz      

	  
	  eigensystem<double>(
	    mat,
	    p_evald,
	    p_evecd,3,w);
			      
// 	  std::swap(p_evecd[1],p_evecd[3]);
// 	  std::swap(p_evecd[2],p_evecd[6]);
// 	  std::swap(p_evecd[5],p_evecd[7]);
      
	  for (int i=0;i<3;i++)
	    p_eval2[i]=p_evald[i];
// 	  for (int i=0;i<9;i++)
// 	    p_evec2[i]=p_evecd[i];
	  
	  p_evec2[0]=p_evecd[0];
	  p_evec2[1]=p_evecd[3];
	  p_evec2[2]=p_evecd[6];
	  p_evec2[3]=p_evecd[1];
	  p_evec2[4]=p_evecd[4];
	  p_evec2[5]=p_evecd[7];
	  p_evec2[6]=p_evecd[2];
	  p_evec2[7]=p_evecd[5];
	  p_evec2[8]=p_evecd[8];
	  
	  
	  p_eval2+=3;
	  mat_img2+=6;
	  p_evec2+=9;
  }
       gsl_eigen_symmv_free (w);
    }
   
//     for (std::size_t idx= 0; idx < numvoxel; idx++)    
//     {
// 	  double mat[9];
// 	  mat[0]=mat_img[0];
// 	  mat[1]=mat_img[3];
// 	  mat[2]=mat_img[4];
// 	  mat[3]=mat_img[3];
// 	  mat[4]=mat_img[1];
// 	  mat[5]=mat_img[5];
// 	  mat[6]=mat_img[4];
// 	  mat[7]=mat_img[5];
// 	  mat[8]=mat_img[2];
// 	  
// 	  double p_evecd[9];
// 	  double p_evald[3];
// 	  
// 	  eigensystem<double>(
// 	    mat,
// 	    p_evald,
// 	    p_evecd,3,w);
// 			      
// 	  std::swap(p_evecd[1],p_evecd[3]);
// 	  std::swap(p_evecd[2],p_evecd[6]);
// 	  std::swap(p_evecd[5],p_evecd[7]);
//       
// 	  for (int i=0;i<3;i++)
// 	    p_eval[i]=p_evald[i];
// 	  for (int i=0;i<9;i++)
// 	    p_evec[i]=p_evecd[i];
// 	  
// 	  p_eval+=3;
// 	  mat_img+=6;
// 	  p_evec+=9;
//   }
 }

}


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

  
    const mxArray *SalTensor;
    SalTensor = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(SalTensor);
    const int *dims = mxGetDimensions(SalTensor);

//     printf("input dims : %d\n",numdim);
if (numdim==4)
{
    int shape[3];  
    shape[0] = dims[3];
    shape[1] = dims[2];
    shape[2] = dims[1];    
    T *saltensor = (T*) mxGetData(SalTensor);
    if (dims[0]!=6)
      mexErrMsgTxt("error: first dim must be 6\n");
    
    int ndims[4];
    ndims[0]=9;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    ndims[3]=dims[3];
    plhs[0] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
    T *orient = (T*) mxGetData(plhs[0]);

    ndims[0]=3;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    ndims[3]=dims[3];
    plhs[1] = mxCreateNumericArray(4,ndims,mxGetClassID(SalTensor),mxREAL);
    T *eigenvalues = (T*) mxGetData(plhs[1]);
    
    
    
    
    bool sort=true;
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"sort")!=-1)
            sort=mhs::mex_getParam<bool>(params,"sort",1)[0];
    }
    
    
    T *stensor=saltensor;

		std::size_t numv=shape[0]*shape[1]*shape[2];
		  
		symm_eigensystem<T>(
		 stensor,
		 eigenvalues,
		 orient,
		 3,
		 numv,shape);
		
		
// 		  for (std::size_t idx= 0; idx < numv; idx++)    
// 		  {
// 			T mat[9];
// 			mat[0]=stensor[0];
// 			mat[1]=stensor[3];
// 			mat[2]=stensor[4];
// 			mat[3]=stensor[3];
// 			mat[4]=stensor[1];
// 			mat[5]=stensor[5];
// 			mat[6]=stensor[4];
// 			mat[7]=stensor[5];
// 			mat[8]=stensor[2];
// 			
// 			eigensystem<T>(
// 			  mat,
// 			 eigenvalues,
// 			 orient,3);
// 					    
// 			std::swap(orient[1],orient[3]);
// 			std::swap(orient[2],orient[6]);
// 			std::swap(orient[5],orient[7]);
// 			
// 		    
// 			eigenvalues+=3;
// 			stensor+=6;
// 			orient+=9;
// 		}
}else 
  if (numdim==3)
{
    int shape[2];  
    shape[0] = dims[2];
    shape[1] = dims[1];
    T *saltensor = (T*) mxGetData(SalTensor);
    if (dims[0]!=3)
      mexErrMsgTxt("error: first dim must be 3\n");
    
    int ndims[3];
    ndims[0]=4;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    plhs[0] = mxCreateNumericArray(3,ndims,mxGetClassID(SalTensor),mxREAL);
    T *orient = (T*) mxGetData(plhs[0]);

    //int ndims[3];
    ndims[0]=2;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    plhs[1] = mxCreateNumericArray(3,ndims,mxGetClassID(SalTensor),mxREAL);
    T *eigenvalues = (T*) mxGetData(plhs[1]);
    
    
    
    
    bool sort=true;
    
    
    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (mhs::mex_hasParam(params,"sort")!=-1)
            sort=mhs::mex_getParam<bool>(params,"sort",1)[0];
    }
    
    
    
   
    T *stensor=saltensor;

		std::size_t numv=shape[0]*shape[1];
		  
		symm_eigensystem<T>(
		 stensor,
		 eigenvalues,
		 orient,
		 2,
		 numv,shape);
		
// 		  for (std::size_t idx= 0; idx < numv; idx++)    
// 		  {
// 			T mat[4];
// 			mat[0]=stensor[0];
// 			mat[3]=stensor[1];
// 			mat[1]=mat[2]=stensor[2];
// 			
// 			eigensystem<T>(
// 			  mat,
// 			 eigenvalues,
// 			 orient,2);
// 			
// 					    
// 			std::swap(orient[1],orient[2]);
// 			
// 		    
// 			eigenvalues+=2;
// 			stensor+=3;
// 			orient+=4;
// 		}
}
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}