//#define _POOL_TEST
//#define _NUM_THREADS_ 4

#define _SUPPORT_MATLAB_
#include "sta_mex_helpfunc.h"

#define D_TRACKER_ALL_PUBLIC
//TODO : create new graph class for manupulations

#define D_USE_TOOL

#include "graph_rep.h"
#include "specialfunc.h"
#include "numres.h"



#include <thread>
#include <chrono>
#include <time.h>




// /*
// template<typename T,int Dim>
// class HardCollisionLabel : public Collision<T,Dim>
// {
// protected:
// public:
//   
//   bool is_soft (){return false;}; 
//     
//   
//   HardCollisionLabel  () : Collision<T,Dim>()
//   {
//   }
//   
// /*  virtual bool colliding(
// 		  T & soft_collision_costs,
// 		  OctTreeNode<T,Dim> & tree,
//                   const OctPoints<T,Dim> & npoint,
//                   T searchrad,
// 		  T temp,
// 		  Candidates * point_candidates=NULL,
// 		  bool update=true,
// 		  T point_candidates_update_max_displacement=-10,
// 		  int mode=0
// 			)
//   {sta_assert_error_m(false,"not implemented");return false;};
//   
//   virtual bool colliding(
// 		  T & soft_collision_costs,
// 		  const OctPoints<T,Dim> & pointA,
// 		  const OctPoints<T,Dim> & pointB,
// 		  T temp,
// 		  int mode=0,
// 		    T * candidate_center_dist_sq_ptr=NULL,
// 		  T * mindist_ptr=NULL
// 			)
//   {sta_assert_error_m(false,"not implemented");return false;};
// 	*/	  
//   
//   
//   bool colliding(
// 		  T & soft_collision_costs,
// 		  const Points<T,Dim> & newpoint,
// 		  const Points<T,Dim> & point,
// 		  T temp,
// 		  int mode=0,
// 		  T * candidate_center_dist_sq_ptr=NULL,
// 		  T * mindist_ptr=NULL
// 			)
//   {
//     return false;
//   }
//   
//   bool colliding(
// 		  T & soft_collision_costs,
// 		  OctTreeNode<T,Dim> & tree,
//                   const OctPoints<T,Dim> & npoint,
//                   T searchrad,
// 		 T temp,
// 		  class Collision<T,Dim>::Candidates * point_candidates=NULL,
// 		 bool update=true,T point_candidates_update_max_displacement=-10,
// 		 int mode=0
// 		// ,PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED
// 		)
//   {
//     
//     
//     Points<T,Dim> & newpoint= * ( ( Points<T,Dim>  * ) &npoint );
//     
//     //TODO dynamically set to 0 if point_candidates!=NULL
// //     class Collision<T,Dim>::Candidates cand;
//     class OctPoints<T,Dim> * query_buffer[query_buffer_size*(point_candidates==NULL)];
//     class Collision<T,Dim>::Candidates cand(query_buffer);
// //     std::size_t found;
// //     class OctPoints<T,Dim> * query_buffer[query_buffer_size];
//     class OctPoints<T,Dim> ** candidates;
//     
//     
//     if (point_candidates==NULL)
//     {
//       point_candidates=&cand;
//     }
//     
//     candidates=point_candidates->p_query_buffer;
//     std::size_t & found=point_candidates->found;
// 
//     if (update)
//     {
//       try {
// 	  tree.queryRange (
// 	      newpoint.get_position(),searchrad,candidates,query_buffer_size,found );
//       } catch ( mhs::STAError & error ) {
// 	  throw error;
//       }
//     }
//     
//     sta_assert_debug0 ( ( newpoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );
//     
//     soft_collision_costs=0;
//     for ( std::size_t a=0; a<found; a++ ) {
//         Points<T,Dim> * point= ( Points<T,Dim> * ) candidates[a];
// 
//         sta_assert_debug0 ( ( point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );
// 
//         if (( point!=&newpoint ) && 
// 	  (point->_path_id!=newpoint._path_id) && 
// 	  (point->point_cost>-2)
// 	) {
// //             T   cand_size=point->get_scale();
// 	  
// 	   T thickinessA=newpoint.get_thickness();
// 	   T thickinessB=point->get_thickness();
// 	   T scaleA=newpoint.get_scale();
// 	   T scaleB=point->get_scale();
// 	  
// 	   bool collision= ( ellipsoids_coolide (
// 				newpoint.get_position(),
// 				point->get_position(),
// 				newpoint.get_direction(),
// 				point->get_direction(),
// 				thickinessA,
// 				scaleA,
// 				thickinessB,
// 				scaleB ) );
// 	   
// 	   if (collision)
// 	     return true;
// /*
//             T candidate_center_dist_sq= ( point->get_position()-newpoint.get_position() ).norm2();
//              
// 	    T scaleA=newpoint.get_scale();
// 	    T scaleB=point->get_scale();
// 
// 	    T mindist=scaleA+scaleB;
// 
//             if ( ! ( candidate_center_dist_sq>mindist*mindist ) ) {
// 		  return  true;
//             }*/
// 
//         }
//     }
//     return false;
//     
//   }
//   
//   
//   
//   void set_params ( const mxArray * params=NULL )
//   {
//      if ( params==NULL ) {
//             return;
//         }
//         try {
//             
//             
// 
//         } catch ( mhs::STAError & error ) {
// 
//             throw error;
//         }
//   }
// };*/

template<typename T,int Dim>
static bool assign_attribute_to_path( Points<T,Dim> & npoint,
		T attribute,
		T * attributes,
		std::size_t max_attributes,
		int max_depths=100000,
		int depths=0,
		Points<T,Dim> * startpoint=NULL,int thread_id=-1,int pathid=-1 ) {
    if ( depths>max_depths ) {
	return true;
    }
    
    

    if ( ( depths==0 ) && ( npoint.get_num_connections() ==0 ) ) {
      return false;
    }

    


    if ( depths==0 ) {
#ifdef D_USE_MULTITHREAD
	if ( thread_id==-1 ) {
	    thread_id= omp_get_thread_num();
	}
#else
	thread_id= 0;
#endif

	track_unique_id[thread_id]++;
    }
    if ((npoint._point_id<0)||(npoint._point_id>max_attributes))
    {
      sta_assert_error_c(( depths==0 ),printf("point ID %d\n",npoint._point_id));
    }else
    {
      attributes[npoint._point_id]=attribute;
    }

//         if (thread_id==-1)
// 	{
// 	 printf("%d %d\n",thread_id,depths);
// 	}

    {
	for ( int end_id=0; end_id<2; end_id++ ) {
	    //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
	    class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

	    for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
		if ( endpointsA[i].connected!=NULL ) {
		    if ( endpointsA[i].connection->track_me_id!=track_unique_id[thread_id] ) {
			endpointsA[i].connection->track_me_id=track_unique_id[thread_id];

			
			bool check=( assign_attribute_to_path ( * ( endpointsA[i].connected->point ),
				      attribute,
				      attributes,
				      max_attributes,
				      max_depths,
				      depths+1,
				      startpoint,thread_id,pathid) );
			if (!check)
			{
			  printf("path id already set? ids not reset before?\n"); 
			}
		    }
		}
	    }
	}
    }
    
    return true;
}


template<typename T,int Dim>
bool mark_docolliding(
		  T & soft_collision_costs,
		  OctTreeNode<T,Dim> & tree,
                  const OctPoints<T,Dim> & npoint,
                  T searchrad,
// 		  class Collision<T,Dim>::Candidates * point_candidates=NULL,
		 std::list<int> & coll_path_labels,
		 bool init=true
		// ,PROPOSAL_TYPES proposal=PROPOSAL_TYPES::PROPOSAL_UNDEFINED
		)
  {
    
    
    Points<T,Dim> & newpoint= * ( ( Points<T,Dim>  * ) &npoint );
    
    //TODO dynamically set to 0 if point_candidates!=NULL
//      class Collision<T,Dim>::Candidates cand;
     class OctPoints<T,Dim> * query_buffer[query_buffer_size];
     class Collision<T,Dim>::Candidates cand(query_buffer);
//     std::size_t found;
//     class OctPoints<T,Dim> * query_buffer[query_buffer_size];
     class OctPoints<T,Dim> ** candidates;
     class Collision<T,Dim>::Candidates *point_candidates=NULL;
    
//     if (point_candidates==NULL)
//     {
//       point_candidates=&cand;
//     }
    
    point_candidates=&cand;
    
    candidates=point_candidates->p_query_buffer;
    std::size_t & found=point_candidates->found;

    {
      try {
	  tree.queryRange (
	      newpoint.get_position(),searchrad,candidates,query_buffer_size,found );
      } catch ( mhs::STAError & error ) {
	  throw error;
      }
    }
    
    if (init)
      coll_path_labels.clear();
    
    sta_assert_debug0 ( ( newpoint.particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );
    
    bool hascollisions=false;
    for ( std::size_t a=0; a<found; a++ ) {
        Points<T,Dim> * point= ( Points<T,Dim> * ) candidates[a];

        sta_assert_debug0 ( ( point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) );

        if (( point!=&newpoint ) && 
	  (point->_path_id!=newpoint._path_id) && 
	  (point->point_cost>-2)
	) {
//             T   cand_size=point->get_scale();
	  
	   T thickinessA=newpoint.get_thickness();
	   T thickinessB=point->get_thickness();
	   T scaleA=newpoint.get_scale();
	   T scaleB=point->get_scale();
	  
	   bool collision= ( ellipsoids_coolide (
				newpoint.get_position(),
				point->get_position(),
				newpoint.get_direction(),
				point->get_direction(),
				thickinessA,
				scaleA,
				thickinessB,
				scaleB ) ) ;
	   
	   if (collision)
	   {
	     if (init)
	     {
	        
	        hascollisions=true;
	        bool inlist=false;
	        for (std::list<int>::iterator iter=coll_path_labels.begin();iter!=coll_path_labels.end();iter++)
		{
		  if ((*iter)==newpoint._path_id)
		  {
		    inlist=true;
		    break;
		  }
		}
		if (!inlist)
		  coll_path_labels.push_back(newpoint._path_id);
	     }else
	     {
	       bool inlist=false; 
	      for (std::list<int>::iterator iter=coll_path_labels.begin();iter!=coll_path_labels.end();iter++)
	      {
		if ((*iter)==newpoint._path_id)
		{
		  inlist=true;
		  break;
		}
	      } 
	      if (inlist) 
		return true;
	     }
	   }
/*
            T candidate_center_dist_sq= ( point->get_position()-newpoint.get_position() ).norm2();
             
	    T scaleA=newpoint.get_scale();
	    T scaleB=point->get_scale();

	    T mindist=scaleA+scaleB;

            if ( ! ( candidate_center_dist_sq>mindist*mindist ) ) {
		  return  true;
            }*/

        }
    }
    
    
	
    return (hascollisions);
    
  }

template<typename T,int Dim>
bool mark_collisions ( Points<T,Dim> & npoint,
		    OctTreeNode<T,Dim> & tree,
		    T searchrad,
		    //Collision<T,Dim> & collfun,
                    int max_depths=100000,
                    int depths=0,
                    Points<T,Dim> * startpoint=NULL,int thread_id=-1 ) {
        if ( depths>max_depths ) {
            return true;
        }
        
        static  std::list<int> coll_path_labels;
        
        if (npoint.particle_type!=PARTICLE_TYPES::PARTICLE_SEGMENT)
	{
	   return false;
	}
        
        T soft_collision_costs=0;
	if (mark_docolliding<T,Dim>(
	      soft_collision_costs,
	      tree,
	      npoint,
	      searchrad,
	      coll_path_labels,
	      (depths==0)
	      ))
	{
	    npoint.point_cost=-2;
	}
	
        
	
         
        
        

        if ( depths==0 ) {
#ifdef D_USE_MULTITHREAD
            if ( thread_id==-1 ) {
                thread_id= omp_get_thread_num();
            }
#else
            thread_id= 0;
#endif

            track_unique_id[thread_id]++;
        }


        {
            for ( int end_id=0; end_id<2; end_id++ ) {
                //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
                class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
                    if ( endpointsA[i].connected!=NULL ) {
                        if ( endpointsA[i].connection->track_me_id!=track_unique_id[thread_id] ) {
                            endpointsA[i].connection->track_me_id=track_unique_id[thread_id];
			    
			    mark_collisions (  * ( endpointsA[i].connected->point ),
			      tree,
			      searchrad,
			      //collfun,
			      max_depths,
			      depths+1,
			      startpoint,thread_id);
			    
                        }
                    }
                }
            }
        }
        
        return true;
    }



float sym_sphere32[32][3]=
{
    {-0.318275, 0.889279, -0.328455},
    {-0.070502, -0.338471, 0.938332},
    {-0.004531, -0.999700, 0.024068},
    {0.176051, 0.891438, -0.417545},
    {-0.494055, 0.588525, -0.639959},
    {0.047585, 0.111997, 0.992569},
    {-0.521185, -0.171197, -0.836097},
    {-0.762523, 0.206614, -0.613082},
    {-0.857729, 0.476461, 0.193099},
    {0.583041, 0.743080, -0.328475},
    {0.726695, -0.231964, -0.646612},
    {-0.578074, 0.633355, 0.514482},
    {-0.071476, 0.692774, -0.717604},
    {-0.732246, -0.242232, 0.636505},
    {-0.236027, 0.918611, 0.316932},
    {-0.565219, 0.823269, 0.052493},
    {-0.434495, -0.898030, -0.068960},
    {-0.214337, -0.503208, -0.837163},
    {0.862566, 0.428223, -0.269452},
    {0.414110, 0.564176, -0.714295},
    {-0.766142, -0.621874, -0.162169},
    {0.749949, -0.599177, 0.280292},
    {0.367008, -0.243405, 0.897808},
    {0.973626, 0.205063, 0.100007},
    {0.953394, -0.051030, -0.297382},
    {-0.422966, -0.076553, 0.902906},
    {0.222193, 0.863308, 0.453133},
    {0.146194, -0.702413, -0.696594},
    {-0.829269, -0.229429, -0.509584},
    {0.330627, -0.354778, -0.874539},
    {-0.567614, -0.602591, -0.560980},
    {-0.956992, 0.217960, -0.191465}
};

std::size_t track_unique_ids=0;

const int Dim=3;

template<typename T>
static void do_label_segments ( Points<T,Dim> & npoint,
                                int & pathid,
                                std::size_t & num_particles,
                                T & length,
                                bool & is_terminal,
                                bool segments_only=false,
                                std::list<T> * segments=NULL,
                                int max_depths=100000,
                                int depths=0,T segment_lengt=0,Vector<T,3> pos=T ( 0 ) )
{
    if ( depths>max_depths )
    {
        return;
    }

    if ( ( depths==0 ) && ( npoint.get_num_connections() ==0 ) )
    {
        return;
    }

    if ( npoint._path_id>-1 )
    {
        return;
    }

    if ( ( npoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) && ( segments_only ) )
    {
        return;
    }





    if ( depths==0 )
    {

        track_unique_ids++;
        pathid++;
        num_particles=0;
        length=0;
        if ( segments!=NULL )
        {
            segments->clear();
        }
        is_terminal=false;
    }
    else
    {
        T l=std::sqrt ( ( npoint.get_position()-pos ).norm2() );
        length+=l;
        segment_lengt+=l;
    }

    if ( ( npoint.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER ) && ( segments!=NULL ) )
    {

        segments->push_back ( segment_lengt );
        segment_lengt=0;
    }


    if ( npoint.get_num_connections() ==1 )
    {
        is_terminal=true;
    }

    num_particles++;

//         if (thread_id==-1)
// 	{
// 	 printf("%d %d\n",thread_id,depths);
// 	}

    npoint._path_id=pathid;

    {
        for ( int end_id=0; end_id<2; end_id++ )
        {
            //class std::vector<class Points<T,Dim>::CEndpoint> & endpointsA=npoint.endpoints[end_id];
            class Points<T,Dim>::CEndpoint * endpointsA=npoint.endpoints_[end_id];

            for ( int i=0; i<Points<T,Dim>::maxconnections; i++ )
            {
                if ( endpointsA[i].connected!=NULL )
                {
                    if ( endpointsA[i].connection->track_me_id!=track_unique_ids )
                    {
                        endpointsA[i].connection->track_me_id=track_unique_ids;


                        ( do_label_segments ( * ( endpointsA[i].connected->point ),
                                              pathid,
                                              num_particles,
                                              length,
                                              is_terminal,
                                              segments_only,
                                              segments,
                                              max_depths,
                                              depths+1,segment_lengt,npoint.get_position() ) );
                    }
                }
            }
        }
    }
}


//matlab -nojvm -nodisplay -nosplash  -D"valgrind --leak-check=full --undef-value-errors=yes --track-origins=yes --show-reachable=yes --error-limit=no  -v --num-callers=20 --leak-resolution=high   --log-file=valgrind.log" -r "load valgrind_debug; ntrack(FeatureData,foptions);exit;"

/*!
 *
 *  MAIN FUNCTION
 *
 * */
template <typename T,typename TData>
void _mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    debug_count=1;

    T TOOL_EPS=0.0000000001;
    {



        CData<T,TData,Dim> *    data_fun=NULL;
        CEdgecost<T,Dim> *      edgecost_fun=NULL;
//     CTracker<TData,T,Dim> ** tracker=NULL;
        Collision<T,Dim> *		collision_fun=NULL;

        CDataGrid<T,TData,Dim>*new_grid_helper_data_fun = NULL;
        CTracker<TData,T,Dim>* single_tracker = NULL;

//     CDataGrid<T,TData,Dim> ** grid_helper_data_fun;

        CTracker<TData,T,Dim> ** tracker=NULL;
        CDataGrid<T,TData,Dim> ** grid_helper_data_fun=NULL;


        int num_workers=1;
        std::vector<Vector<std::size_t,3> > worker_regions;




        int paramindx=0;
        const mxArray * feature_struct=NULL;
        const mxArray * tracker_state=NULL;
        const mxArray * cam_data=NULL;
        const mxArray * params=NULL;
        const mxArray * tool_params=NULL;

        int num_tracker_states=0;
        bool tracker_state_multitracker=false;

	
	std::size_t point_buffer=1000000;

        int tool=1;



        try
        {

            printf ( "evaluating input parameters " );
            for ( std::size_t i=0; i<nrhs; i++ )
            {
// 	    if ((mxIsStruct(prhs[i])) && (mxGetField ( prhs[i],0, ( char * ) ( "datafunc" ) )!=NULL))
// 	    {
// 	      sta_assert_error(feature_struct==NULL)
// 	      feature_struct=prhs[i];
// 	    }else
                if ( ( mxIsStruct ( prhs[i] ) ) &&
                        ( ( mxGetField ( prhs[i],0, ( char * ) ( "data" ) ) !=NULL ) && ( mxGetField ( prhs[i],0, ( char * ) ( "connections" ) ) !=NULL ) ) ||
                        ( mxGetField ( prhs[i],0, ( char * ) ( "W" ) ) !=NULL )
                   )
                {
                    sta_assert_error ( tracker_state==NULL )
                    tracker_state=prhs[i];
                }
                else if ( ( mxIsStruct ( prhs[i] ) ) && ( mxGetField ( prhs[i],0, ( char * ) ( "cam" ) ) !=NULL ) )
                {
                    sta_assert_error ( cam_data==NULL )
                    cam_data=prhs[i];
                }
                else if ( i==nrhs-1 )
                {
                    tool_params=prhs[i];
                }
                else
                    sta_assert_error ( 0!=1 );
            }

            printf ( " .. done\n" );

            if ( tracker_state==NULL )
            {
                printf ( "no tracker state found\n" );
                return;
            }


            if ( tool_params!=NULL )
            {
                if ( mhs::mex_hasParam ( tool_params,"tool" ) !=-1 )
                    tool=mhs::mex_getParam<int> ( tool_params,"tool",1 ) [0];

                if ( mhs::mex_hasParam ( tool_params,"params" ) !=-1 )
                    params=mhs::mex_getParamPtr ( tool_params,"params" );
		
		if ( mhs::mex_hasParam ( tool_params,"point_buffer" ) !=-1 )
                    point_buffer=mhs::mex_getParam<std::size_t> ( tool_params,"point_buffer",1 )[0];
		
		

            }


            if ( params==NULL )
            {
                params=mxGetField ( tracker_state,0, ( char * ) ( "options" ) );
            }


            bool debug_compute_costs=false;

            int collision_fun_id=-1;


            if ( params!=NULL )
            {

                if ( mhs::mex_hasParam ( params,"debug_compute_costs" ) !=-1 )
                    debug_compute_costs=mhs::mex_getParam<bool> ( params,"debug_compute_costs",1 ) [0];

                if ( mhs::mex_hasParam ( params,"collision_fun_id" ) !=-1 )
                    collision_fun_id=mhs::mex_getParam<int> ( params,"collision_fun_id",1 ) [0];



#ifdef D_USE_GUI
                if ( mhs::mex_hasParam ( params,"handle" ) !=-1 )
                {
                    gui_window_handle=mhs::mex_getParam<uint64_t> ( params,"handle",1 ) [0];
                    gui_window_handle_ptr=&gui_window_handle;
                    printf ( "found window handle\n" );
                }
                // std::size_t pointer=mhs::mex_getParam<uint64_t>(params,"handle",1)[0];
#endif




// 	  if (mhs::mex_hasParam(params,"worker")!=-1)
//                 num_workers=mhs::mex_getParam<bool>(params,"worker",1)[0];

            }




            if ( tracker_state!=NULL )
            {

                tracker_state_multitracker=mxGetField ( tracker_state,0, ( char * ) ( "W" ) ) !=NULL;
                // if we have existing states, then
                //if (mxIsCell(tracker_state))
                if ( tracker_state_multitracker )
                {
// 	    num_tracker_states=mxGetNumberOfElements(tracker_state);


                    num_tracker_states=mxGetNumberOfElements ( mxGetField ( tracker_state,0, ( char * ) ( "W" ) ) );

// 	    num_tracker_states=mxGetNumberOfElements(tracker_state);
                    num_workers=num_tracker_states;

                    //num_workers=std::max(num_tracker_states,num_workers);
                    printf ( "found data for several workers: %d %d\n",num_tracker_states,num_workers );
// 	    tracker_state_cell=(mxArray *)tracker_state;
                }
                else
                {
// 	    mwSize dims=1;
// 	    tracker_state_cell=mxCreateCellArray(dims, &dims);
// 	    mxSetCell(tracker_state_cell, 0, (mxArray *)tracker_state);
                    num_tracker_states=1;
                }
            }

            //int datafunc=mhs::dataArray<TData>(feature_struct,"datafunc").data[0];

            sta_assert_error ( num_workers<max_tracking_treads );

            data_fun=new CDataDummy<T,TData,Dim>();


            printf ( "->edgefunc\n" );


            edgecost_fun=new CNTrackerScaleInvariantEdgeCosts<T,Dim>();
	    
// 	    if (tool==6)
// 	    {
// 		printf ( "tool is 6, setting the collision func \n" );
// 		collision_fun_id=2;
// 	    }

            switch ( collision_fun_id )
            {
            case 1:
            {
                collision_fun=new DTISoftCollision<T,Dim> ();
                printf ( "using DTI sof collision\n" );
            }
//             case 2:
//             {
//                 collision_fun=new HardCollisionLabel<T,Dim> ();
//                 printf ( "using HArd Label collision\n" );
//             }
            break;

            default:
            {
                collision_fun=new TabletCollision<T,Dim> ();
                printf ( "using hard collision\n" );
            }
            break;
            }


            collision_fun->set_params ( params );

            CData<T,TData,Dim> &  data_fun_p=*data_fun;
            CEdgecost<T,Dim> &   edgecost_fun_p=*edgecost_fun;

            Collision<T,Dim> &   collision_fun_p=*collision_fun;

            mhs::mex_dumpStringNOW();

            printf ( "->datafunc (dummy)\n" );
            const mxArray * feature_struct_dummy=tracker_state;
            data_fun_p.init ( feature_struct_dummy );

            mhs::mex_dumpStringNOW();

            printf ( "->tracker\n" );





            std::size_t min_pts_per_tracker=500000;
            std::vector<std::size_t> pts_per_tracker;
            std::size_t total_num_pts=0;
            pts_per_tracker.resize ( num_workers );

            for ( int t=0; t<num_workers; t++ )
            {
                pts_per_tracker[t]=min_pts_per_tracker;
                try
                {
                    if ( tracker_state!=NULL )
                    {
                        if ( tracker_state_multitracker )
                        {
                            const mxArray * state= ( t<num_tracker_states ) ? mxGetField ( mxGetField ( tracker_state,0, ( char * ) ( "W" ) ),t, ( char * ) ( "A" ) ) : NULL;
                            if ( state!=NULL )
                            {
                                total_num_pts+=mhs::dataArray<T> ( state,"data" ).dim[0];
                                pts_per_tracker[t]=std::max ( pts_per_tracker[t],2*mhs::dataArray<T> ( state,"data" ).dim[0] );
                            }
                        }
                        else
                        {
                            const mxArray * state= ( t<1 ) ? tracker_state : NULL;
                            if ( state!=NULL )
                            {
                                total_num_pts+=mhs::dataArray<T> ( state,"data" ).dim[0];
                                pts_per_tracker[t]=std::max ( pts_per_tracker[t],2*mhs::dataArray<T> ( state,"data" ).dim[0] );
                            }
                        }
                    }
                }
                catch ( mhs::STAError & error )
                {
                    throw mhs::STAError ( "error in determining pool sizes\n " );
                }
            }

// 	tracker= new CTracker<TData,T,Dim>*[num_workers];
// 	grid_helper_data_fun= new CDataGrid<T,TData,Dim>*[num_workers];
//
// 	for (int t=0;t<num_workers;t++)
// 	{
// 	  tracker[t]=NULL;
// 	  grid_helper_data_fun[t]=NULL;
// 	}



            //#pragma omp parallel for num_threads(num_workers)
// 	for (int t=0;t<num_workers;t++)
// 	{
// 	    grid_helper_data_fun[t]= new CDataGrid<T,TData,Dim>(data_fun);
//
// 	    if (tracker_state_multitracker)
// 	    {
//  		const  mxArray  * worker_data=mxGetField(tracker_state,0,(char *)("W"));
//
// 		sta_assert_error(worker_data!=NULL);
//
// 		const  mxArray  * sub_region=mxGetField(worker_data,t,(char *)("shape"));
// 		sta_assert_error(sub_region!=NULL);
//
// 		std::size_t shape[3];
// 		std::size_t offset[3];
//
// 		  double * rdata_p=(double  *)mxGetPr(sub_region);
// 		  shape[0]=rdata_p[0];
// 		  shape[1]=rdata_p[1];
// 		  shape[2]=rdata_p[2];
//
// 		  sub_region=mxGetField(worker_data,t,(char *)("offset"));
// 		  sta_assert_error(sub_region!=NULL);
// 		  rdata_p=(double  *)mxGetPr(sub_region);
// 		  offset[0]=rdata_p[0];
// 		  offset[1]=rdata_p[1];
// 		  offset[2]=rdata_p[2];
//
// 		  grid_helper_data_fun[t]->set_region(offset,shape);
// 	    }
// 	    //grid_helper_data_fun[t]->init_saliency(feature_struct);
//
//
// 	    printf("initializing tracker %u with a pool of size %u (saliency has not been initialized) \n",t,pts_per_tracker[t]);
// 	    tracker[t]= new CTracker<TData,T,Dim>(*grid_helper_data_fun[t],
// 					      edgecost_fun_p,collision_fun_p,pts_per_tracker[t]);
// 	}
//
            printf ( "-> init new data func\n" );
            mhs::mex_dumpStringNOW();

            //new_grid_helper_data_fun=new CDataGrid<T,TData,Dim>(data_fun);
            printf ( "-> creating new graph \n" );
            
#ifdef GRID_DEBUG 
grid_verbose = 1;
#endif
            single_tracker= new  CTracker<TData,T,Dim> ( *data_fun,
                    edgecost_fun_p,collision_fun_p,2*total_num_pts+point_buffer );

	    
	    
	    
            printf ( "-> init new graph \n" );
            single_tracker->init_tracker_basic ( feature_struct,NULL,params );





            bool no_error=true;
            printf ( "->tracker init\n" );
            //#pragma omp parallel for num_threads(num_workers)
            for ( int t=0; t<num_workers; t++ )
            {
                try
                {
                    if ( tracker_state!=NULL )
                    {

                        if ( tracker_state_multitracker )
                        {
// 		tracker[t]->init_tracker(feature_struct,
// 				//(t<num_tracker_states) ? mxGetField(tracker_state,t,(char *)("W")) : NULL,
// // 				(t<num_tracker_states) ? mxGetField(mxGetField(tracker_state,t,(char *)("W")),0,(char *)("A")) : NULL,
// 					 (t<num_tracker_states) ? mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("A")) : NULL,
// 				params);


// 		printf("adding graph\n");

// 		const  mxArray  * worker_data=mxGetField(tracker_state,0,(char *)("W"));
// 		sta_assert_error(worker_data!=NULL);
// 		const  mxArray  * sub_region=mxGetField(worker_data,t,(char *)("shape"));
// 		sta_assert_error(sub_region!=NULL);
//
// 		std::size_t shape[3];
// 		std::size_t offset[3];
//
// 		  double * rdata_p=(double  *)mxGetPr(sub_region);
// 		  shape[0]=rdata_p[0];
// 		  shape[1]=rdata_p[1];
// 		  shape[2]=rdata_p[2];
//
// 		  sub_region=mxGetField(worker_data,t,(char *)("offset"));
// 		  sta_assert_error(sub_region!=NULL);
// 		  rdata_p=(double  *)mxGetPr(sub_region);
// 		  offset[0]=rdata_p[0];
// 		  offset[1]=rdata_p[1];
// 		  offset[2]=rdata_p[2];


// 		single_tracker->init_tracker_add_graph ( feature_struct,
// 							 (t<num_tracker_states) ? mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("A")) : NULL,
// 							 params, offset );

                            single_tracker->init_tracker_add_graph ( feature_struct,
                                    ( t<num_tracker_states ) ? mxGetField ( mxGetField ( tracker_state,0, ( char * ) ( "W" ) ),t, ( char * ) ( "A" ) ) : NULL,
                                    params );
// 		printf("adding graph ok\n");
                        }
                        else
                        {
                            sta_assert_error ( num_workers==1 );
// 		tracker[t]->init_tracker(feature_struct,
// 				(t<1) ? tracker_state : NULL,
// 				params);
                            single_tracker->init_tracker_add_graph ( feature_struct,
                                    ( t<1 ) ? tracker_state : NULL,
                                    params );
                        }
                    }
                    else
                    {
                        sta_assert_error ( num_workers==1 );
// 	      tracker[t]->init_tracker(feature_struct,
// 				 NULL,
// 				params);

                        printf ( "no state?? hae\n" );
                        sta_assert_error ( 1==0 );
                    }
                }
                catch ( mhs::STAError & error )
                {
                    no_error=false;
                    printf ( "error in initing worker %d: %s\n",t,error.str().c_str() );
                }
            }
            mhs::mex_dumpStringNOW();
            if ( !no_error )
                throw ( mhs::STAError ( "exiting" ) );

            printf ( "---\n" );

            single_tracker->init_graph_structure();

            int total_num_threads=num_workers;


//
            single_tracker->check_consisty();

#ifdef GRID_DEBUG 
grid_verbose = 0;
#endif            
            
// 	{
// 	 std::size_t numconnections=single_tracker->connections.pool->get_numpts();
// 	   const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
// 	  for ( std::size_t i=0; i<numconnections; i++ ) {
//             const Connection<T,Dim> * con=*conptr;
// 	     printf("%d %d\n",i,numconnections);
// 	      sta_assert_error(con->pointA!=NULL);
// 		sta_assert_error(con->pointB!=NULL);
//             conptr++;
//
// 	  }
// 	}
//



            switch ( tool )
            {
            case 2:
            {
                printf ( "computing segment lengths\n" );
                sta_assert_error ( nlhs==2 );
                single_tracker->clear_path_labels();

                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                int path_id=2;

                typedef struct
                {
                    int path_id;
                    T length;
                } Tpathlength;
                std::list<Tpathlength> path_lenths;

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    T plength=0;
                    if ( CTracker<TData,T,Dim>::do_label ( point,path_id,plength ) )
                    {
                        Tpathlength pld;
                        pld.path_id=path_id;
                        pld.length=plength;
                        path_lenths.push_back ( pld );
                    }
                }

                mwSize outdim[2];


                outdim[0] = 2;
                outdim[1] = path_lenths.size();

                plhs[1] = mxCreateNumericArray ( 2,outdim,mhs::mex_getClassId< T >(),mxREAL );
                T *result = ( T * ) mxGetData ( plhs[1] );

                for ( typename std::list<Tpathlength>::iterator iter=path_lenths.begin(); iter!=path_lenths.end(); iter++ )
                {
                    Tpathlength & pld=*iter;
                    ( *result++ ) =pld.path_id;
                    ( *result++ ) =pld.length;
                }


            }
            break;

            case 3:
            {
                std::vector<T> v_scale;
                v_scale.resize ( 3 );
                v_scale[0]=v_scale[1]=v_scale[2]=1;
		bool only_mark_paths=false;
                T min_length=5;
		
                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"min_length" ) !=-1 )
                        min_length=mhs::mex_getParam<T> ( tool_params,"min_length",1 ) [0];

                    if ( mhs::mex_hasParam ( tool_params,"v_scale" ) !=-1 )
                    {
                        v_scale=mhs::mex_getParam<T> ( tool_params,"v_scale",3 );
                        //printf ( "v scale: %f %f %f\n",v_scale[0],v_scale[1],v_scale[2] );
                    }
                    
                    if ( mhs::mex_hasParam ( tool_params,"only_mark_paths" ) !=-1 )
                    {
                        only_mark_paths=mhs::mex_getParam<bool> ( tool_params,"only_mark_paths",1 )[0];
                        printf ( "setting POINT_A_DISTANCE to path length\n");
                    }
                }

	



                std::swap ( v_scale[0],v_scale[2] );
                printf ( "v scale: %f %f %f\n",v_scale[0],v_scale[1],v_scale[2] );

                printf ( "trim tree (min length %f)\n",min_length );
                sta_assert_error ( (nlhs==1) || ((nlhs==2) && only_mark_paths) );
                single_tracker->clear_path_labels();

                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                int path_id=2;



                std::list<Points<T,Dim> * > del_path_pts;
		
		
		
		
		mwSize outdim;
		outdim = n_points;
// 		
		T *result =NULL;
		if (only_mark_paths)
		{
		  plhs[1] = mxCreateNumericArray ( 1,&outdim,mhs::mex_getClassId< T >(),mxREAL );
		  result = ( T * ) mxGetData ( plhs[1] );
		  for ( unsigned int i=0; i<n_points; i++ )
		  {
		      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                      //printf("%d\n",point._path_id);
		      point.set_id(i);
                      //printf("%d\n",point._path_id);
                       point._path_id=-1;
		  }
		}
// 
// 		printf("pts: %d\n",n_points);
T ml=0;

Vector<T,Dim> min_pos=std::numeric_limits<T>::max();
Vector<T,Dim> max_pos=-std::numeric_limits<T>::max();

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    
                    min_pos = min_pos.min(point.get_position());
                    max_pos = max_pos.max(point.get_position());
                    
                    T plength=0;
                    
                    if (point.is_terminal())
                    {
                    
                        if ( CTracker<TData,T,Dim>::do_label_debug( point,path_id,plength,100000,0,NULL,-1,v_scale.data() ) )
                        {
                            
    ml=std::max(ml,plength);                        
                            
                            if ( ( plength<min_length ) )
                            {
                                del_path_pts.push_back ( &point );
                            }
                            if (only_mark_paths)
                            {
                            //result[n_points-i-1]=plength;
                            assign_attribute_to_path( point,
                            plength,
                            result,n_points);  
                            //result[i]=plength;
                            }
                        }
                    }
                }
                
printf("MIN POS  ##############\n");                
min_pos.print();
printf("MAX POS  ##############\n");
max_pos.print();
printf("##############\n");                
printf("LOGNEST PATH %f\n",ml);
printf("##############\n");


//     static bool do_label ( Points<T,Dim> & npoint,
//                     int & pathid,
// 		    T & path_length,
//                     int max_depths=100000,
//                     int depths=0,
//                     Points<T,Dim> * startpoint=NULL,int thread_id=-1, T * e_size  =NULL ) {

                for ( typename std::list<Points<T,Dim> *>::iterator iter=del_path_pts.begin(); iter!=del_path_pts.end(); iter++ )
                {
                    Points<T,Dim> & point=**iter;
                    CTracker<TData,T,Dim>::clear_path_ids ( point );
                }
                
                if (!only_mark_paths)
		{
		  std::list<Points<T,Dim> * > del_pts;
		  std::list<Connection<T,Dim> * > del_edges;

		  for ( unsigned int i=0; i<n_points; i++ )
		  {
		      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
		      if ( point._path_id==-1 )
		      {
			  del_pts.push_back ( &point );
		      }
		  }

		  std::size_t numconnections=single_tracker->connections.pool->get_numpts();

		  const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
		  for ( std::size_t i=0; i<numconnections; i++ )
		  {
		      Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );

		      //printf("%d %d\n",i,numconnections);
		      sta_assert_error ( con.pointA!=NULL );
		      sta_assert_error ( con.pointB!=NULL );

		      Points<T,Dim> & pointA=*con.pointA->point;
		      Points<T,Dim> & pointB=*con.pointB->point;
		      sta_assert_error ( pointA._path_id==pointB._path_id );
		      if ( pointA._path_id==-1 )
		      {
			  del_edges.push_back ( &con );
		      }
		      //connections.remove_connection ( con );

  // 		connections.pool->delete_obj(con);
		  }


		  printf ( "\n########################################\ndeleting %d edges\n",del_edges.size() );

		  for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
		  {
		      Connection<T,Dim> * con=*iter;
		      single_tracker->connections.remove_connection ( con );

		  }


		  printf ( "\n########################################\ndeleting %d pts\n",del_pts.size() );

		  for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
		  {
		      Points<T,Dim> * point=*iter;
		      single_tracker->particle_pool->delete_obj ( point );
		  }

		  printf ( "\n########################################\n" );
		}else
		{
		  for ( unsigned int i=0; i<n_points/2; i++ )
		  {
		     std::swap(result[n_points-1-i],result[i]);
   		  }
		  
		}
		

// 	    for (typename std::list<Tpathlength>::iterator iter=path_lenths.begin();iter!=path_lenths.end();iter++)
// 	    {
// 	      Tpathlength & pld=*iter;
//
//
// 	    }


            }
            break;


            case 4:
            {
                int coll_sharp=0;
                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"coll_sharp" ) !=-1 )
                        coll_sharp=mhs::mex_getParam<int> ( tool_params,"coll_sharp",1 ) [0];
                }


                printf ( "helper function for mhs_graph_closest_pts\n" );

                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();


                class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
//             class Collision<T,Dim>::Candidates candA ( query_bufferA );

                mwSize outdim;
                outdim = n_points;

                plhs[1] = mxCreateNumericArray ( 1,&outdim,mhs::mex_getClassId< T >(),mxREAL );
                T *result = ( T * ) mxGetData ( plhs[1] );


                Points<T,Dim> ** best_cand = new Points<T,Dim> * [n_points];

                T collision_search_rad=single_tracker->maxscale*2+1;

                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"collision_search_rad" ) !=-1 )
                        collision_search_rad=mhs::mex_getParam<T> ( tool_params,"collision_search_rad",1 ) [0];
                }

                printf ( "using collision searchrad: %f\n",collision_search_rad );

                Collision<T,Dim> &   collision_fun_p=* ( single_tracker->collision_fun );
                OctTreeNode<T,Dim> &		tree			=* ( single_tracker->tree );

                std::size_t pts_in_grid=0;
                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );

                    best_cand[i]=NULL;
                    pts_in_grid+=point.has_owner();

                    T tmp;


                    class OctPoints<T,Dim> ** candidates = query_bufferA;
                    std::size_t found=0;



                    try
                    {
                        tree.queryRange (
                            point.get_position(),collision_search_rad,candidates,query_buffer_size,found );
                    }
                    catch ( mhs::STAError & error )
                    {
                        throw error;
                    }

                    /*
                    	      bool collising= collision_fun_p.colliding (
                                                           tmp,
                                                           tree,
                                                           point,
                                                           collision_search_rad,
                                                           1,
                                                           &candA,true,
                                                           -9// collect colliding pts
                                                       );

                    	      class OctPoints<T,Dim> ** candidates=NULL;

                    	      candidates=candA.p_query_buffer;
                    	      std::size_t & found=candA.found;*/

                    T mindist=std::numeric_limits< T >::max();

// 	      std::size_t test=0;
//
                    //result[n_points-i-1]=point.point_cost;
                    for ( std::size_t a=0; a<found; a++ )
                    {
                        Points<T,Dim> & coll_point= * ( ( Points<T,Dim> * ) candidates[a] );

                        if ( coll_point.point_cost!=point.point_cost )
                        {
// 		    test++;
                            //T colldist=std::min(coll_point.get_scale(),point.get_scale());
                            T colldist;
// 		    if (coll_sharp)
// 		    {
// 		      colldist=std::min(coll_point.get_scale(),point.get_scale());
// 		    }else
// 		    {
// 		      colldist=(coll_point.get_scale()+point.get_scale());
// 		    }
                            switch ( coll_sharp )
                            {
                            case 0:
                            {
                                colldist=std::min ( coll_point.get_scale(),point.get_scale() );
                            }
                            break;
                            case 1:
                            {
                                colldist= ( coll_point.get_scale() +point.get_scale() );
                            }
                            break;
                            default:
                            {
                                colldist=2*std::min ( coll_point.get_scale(),point.get_scale() );
                            }
                            break;

                            }

                            colldist*=colldist;

                            T dist2= ( coll_point.get_position()-point.get_position() ).norm2();

                            if ( ( colldist>dist2 ) && ( mindist>dist2 ) )
                            {
                                best_cand[i]=&coll_point;
                                mindist=dist2;
                            }
                        }
                    }

                    /*
                    bool checked=false;
                    //mindist=std::numeric_limits< T >::max();
                    if (best_cand[i]!=NULL)
                    {
                    //T dist2=(best_cand[i]->get_position()-point.get_position()).norm2();


                    T mindist=10000000000000000;

                    if (mindist>0.00001)
                    {
                      checked=true;
                      for ( unsigned int j=0; j<n_points; j++ ) {
                          Points<T,Dim> & coll_point=* ( ( Points<T,Dim>* ) particle_ptr[j] );
                          if (coll_point.point_cost!=point.point_cost)
                          {
                    // 		    test++;
                    // 			T colldist=std::min(coll_point.get_scale(),point.get_scale());
                    // 			colldist*=colldist;

                    	T colldist=8;
                    	colldist*=colldist;

                    	T dist2=(coll_point.get_position()-point.get_position()).norm2();

                    	if ((colldist>dist2)&&(mindist>dist2))
                    	{
                    	  best_cand[i]=&coll_point;
                    	  mindist=dist2;
                    	}
                          }
                      }

                          }
                       }*/
//
// 	    if (mindist>0.0001)
// 		{
// 		      if (best_cand[i]!=NULL)
// 		    {
// 		      T dist2=(best_cand[i]->get_position()-point.get_position()).norm2();
// 		      printf("%f %f %d [%f %f] \n",mindist,dist2,checked,point.point_cost,best_cand[i]->point_cost);
//
// 		    }else
// 		    {
// 		      printf("meep\n");
// 		    }
// 		}






                }
                printf ( "num_owners: %d / %d\n",pts_in_grid,n_points );


// 	    for ( unsigned int j=0; j<n_points/2; j++ ) {
// 		   Points<T,Dim> & ptsA=* ( ( Points<T,Dim>* ) particle_ptr[j] );
//
// 		   unsigned int k=j+n_points/2;
// 		   sta_assert_error(k<n_points)
// 		   Points<T,Dim> & ptsB=* ( ( Points<T,Dim>* ) particle_ptr[k] );
// 		   T dist2=(ptsA.get_position()-ptsB.get_position()).norm2();
// 		   if (dist2>std::numeric_limits< T >::epsilon())
// 		   {
// 		    ptsA.print();
// 		    ptsB.print();
// 		   }
// 		   if (ptsA.point_cost==ptsB.point_cost)
// 		   {
// 		    printf("AHHHHHHHHHHHHH\n");
// 		   }
//
// 	    }


                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point._point_id=i;
                }

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    if ( best_cand[i]!=NULL )
                    {
                        result[n_points-i-1]=n_points-best_cand[i]->_point_id-1;
                    }
                    else
                    {
                        result[n_points-i-1]=-1;
                    }
                }




                delete [] best_cand;




            };
            break;


            case 5:
            {

                int del_id=-1;
                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"del_id" ) !=-1 )
                        del_id=mhs::mex_getParam<T> ( tool_params,"del_id",1 ) [0];
                }

                printf ( "del path id %d\n",del_id );
                mhs::mex_dumpStringNOW();
                sta_assert_error ( nlhs==1 );


                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                int path_id=2;


                std::list<Points<T,Dim> * > del_pts;
                std::list<Connection<T,Dim> * > del_edges;

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    if ( point.point_cost==del_id )
                    {
                        del_pts.push_back ( &point );
                    }
                }

                std::size_t numconnections=single_tracker->connections.pool->get_numpts();

                const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
                for ( std::size_t i=0; i<numconnections; i++ )
                {
                    Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );

                    //printf("%d %d\n",i,numconnections);
                    sta_assert_error ( con.pointA!=NULL );
                    sta_assert_error ( con.pointB!=NULL );

                    Points<T,Dim> & pointA=*con.pointA->point;
                    Points<T,Dim> & pointB=*con.pointB->point;
                    sta_assert_error ( pointA.point_cost==pointB.point_cost );
                    if ( pointA.point_cost==del_id )
                    {
                        del_edges.push_back ( &con );
                    }
                }


                printf ( "\n########################################\ndeleting %d edges\n",del_edges.size() );
                mhs::mex_dumpStringNOW();

                for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
                {
                    Connection<T,Dim> * con=*iter;
                    single_tracker->connections.remove_connection ( con );

                }


                printf ( "\n########################################\ndeleting %d pts\n",del_pts.size() );
                mhs::mex_dumpStringNOW();

                for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
                {
                    Points<T,Dim> * point=*iter;
                    single_tracker->particle_pool->delete_obj ( point );
                }

                printf ( "\n########################################\n" );


// 	    for (typename std::list<Tpathlength>::iterator iter=path_lenths.begin();iter!=path_lenths.end();iter++)
// 	    {
// 	      Tpathlength & pld=*iter;
//
//
// 	    }


            }
            break;


            case 6:
	    {
	      
	      

                printf ( "remove overlapping terminals\n" );
		mhs::mex_dumpStringNOW();
                class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
                class Collision<T,Dim>::Candidates candA ( query_bufferA );



                T collision_search_rad=single_tracker->maxscale*2+1;

                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"collision_search_rad" ) !=-1 )
                        collision_search_rad=mhs::mex_getParam<T> ( tool_params,"collision_search_rad",1 ) [0];
                }

                printf ( "using collision searchrad: %f\n",collision_search_rad );

                printf ( "\n########################################\n" );
		mhs::mex_dumpStringNOW();
                Collision<T,Dim> &   collision_fun_p=* ( single_tracker->collision_fun );
                OctTreeNode<T,Dim> &		tree			=* ( single_tracker->tree );
		
		
		std::list<Points<T,Dim> * > del_pts;
                std::list<Connection<T,Dim> * > del_edges;

		{
		  int path_id=2;
		  const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
		  std::size_t n_points=single_tracker->particle_pool->get_numpts();
		  for ( unsigned int i=0; i<n_points; i++ ) {
		      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
		      T plength=0;
		      if (CTracker<TData,T,Dim>::do_label ( point,path_id,plength))
		      {
		      }
		  }
		}
		

                std::size_t max_test=100000;
                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        point.point_cost=1;
                    }
                }

                // find all terminal candidates
                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
			if (point.is_terminal())
			{
			  //point.point_cost=-1;
			   mark_collisions (point,
			    tree,
			    collision_search_rad); 
			}
		    }
		}
		
		{
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
			if (point.point_cost==-2)
			{
			  del_pts.push_back ( &point );
			}
		    }
		}
		
		std::size_t numconnections=single_tracker->connections.pool->get_numpts();
		const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
		for ( std::size_t i=0; i<numconnections; i++ )
		{
		    Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );
		    sta_assert_error ( con.pointA!=NULL );
		    sta_assert_error ( con.pointB!=NULL );

		    Points<T,Dim> & pointA=*con.pointA->point;
		    Points<T,Dim> & pointB=*con.pointB->point;
		    //
		    if ( ( pointA.point_cost==-2 ) || ( pointB.point_cost==-2 ) )
		    {
			del_edges.push_back ( &con );
		    }
		}
		



	      printf ( "deleting %d edges and ",del_edges.size() );
	      mhs::mex_dumpStringNOW();

	      for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
	      {
		  Connection<T,Dim> * con=*iter;
		  single_tracker->connections.remove_connection ( con );
	      }



	      printf ( "deleting %d pts\n",del_pts.size() );
	      mhs::mex_dumpStringNOW();
	      for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
	      {
		  Points<T,Dim> * point=*iter;
		  single_tracker->particle_pool->delete_obj ( point );
	      }
	   

	      mhs::mex_dumpStringNOW();
               

                printf ( "\n########################################\n" );

            };	      
	      
	      
//             {
//                 printf ( "remove overlapping terminals\n" );
// 
// 
// 
//                 class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
//                 class Collision<T,Dim>::Candidates candA ( query_bufferA );
// 
// 
// 
//                 T collision_search_rad=single_tracker->maxscale*2+1;
// 
//                 if ( tool_params!=NULL )
//                 {
//                     if ( mhs::mex_hasParam ( tool_params,"collision_search_rad" ) !=-1 )
//                         collision_search_rad=mhs::mex_getParam<T> ( tool_params,"collision_search_rad",1 ) [0];
//                 }
// 
//                 printf ( "using collision searchrad: %f\n",collision_search_rad );
// 
//                 printf ( "\n########################################\n" );
// 
//                 Collision<T,Dim> &   collision_fun_p=* ( single_tracker->collision_fun );
//                 OctTreeNode<T,Dim> &		tree			=* ( single_tracker->tree );
// 
// 
//                 std::size_t max_test=100000;
// 
//                 {
//                     std::size_t n_points=single_tracker->particle_pool->get_numpts();
//                     const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
// 
//                     for ( unsigned int i=0; i<n_points; i++ )
//                     {
//                         Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
//                         point.point_cost=1;
//                     }
//                 }
// 
//                 {
//                     std::size_t n_points=single_tracker->particle_pool->get_numpts();
//                     const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
// 
//                     std::list<Points<T,Dim> * > del_pts;
//                     std::list<Connection<T,Dim> * > del_edges;
// 
// 
// 
//                     bool warning =false;
// 
// 
//                     for ( unsigned int i=0; i<n_points; i++ )
//                     {
//                         Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
//                         if ( ( point.point_cost<0 ) )
//                             continue;
// 
//                         point.point_cost=-1;
// 
//                         T tmp;
// 
//                         bool colliding= collision_fun_p.colliding (
//                                             tmp,
//                                             tree,
//                                             point,
//                                             collision_search_rad,
//                                             1,
//                                             &candA,true,
//                                             -9// collect colliding pts
//                                         );
// 
//                         class OctPoints<T,Dim> ** candidates=NULL;
// 
//                         candidates=candA.p_query_buffer;
//                         std::size_t & found=candA.found;
// 
// 
//                         std::size_t found_check=0;
// 
// 
//                         for ( std::size_t a=0; a<found; a++ )
//                         {
//                             Points<T,Dim> & coll_point= * ( ( Points<T,Dim> * ) candidates[a] );
//                             sta_assert_error ( ( &coll_point ) != ( &point ) );
//                             if ( ( coll_point._path_id!=point._path_id ) ) //&&(coll_point.point_cost>0))
//                             {
// 
//                                 found_check++;
//                             }
//                         }
// 
//                         if ( found_check>0 )
//                         {
//                             point.point_cost=-2;
//                             del_pts.push_back ( &point );
//                         }
//                     }
// 
//                     std::size_t numconnections=single_tracker->connections.pool->get_numpts();
// 
//                     const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
//                     for ( std::size_t i=0; i<numconnections; i++ )
//                     {
//                         Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );
//                         sta_assert_error ( con.pointA!=NULL );
//                         sta_assert_error ( con.pointB!=NULL );
// 
//                         Points<T,Dim> & pointA=*con.pointA->point;
//                         Points<T,Dim> & pointB=*con.pointB->point;
//                         //
//                         if ( ( pointA.point_cost==-2 ) || ( pointB.point_cost==-2 ) )
//                         {
//                             del_edges.push_back ( &con );
//                         }
//                     }
// 
// 
// 
//                     printf ( "deleting %d edges and ",del_edges.size() );
//                     mhs::mex_dumpStringNOW();
// 
//                     for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
//                     {
//                         Connection<T,Dim> * con=*iter;
//                         single_tracker->connections.remove_connection ( con );
//                     }
// 
// 
// 
//                     printf ( "deleting %d pts\n",del_pts.size() );
//                     mhs::mex_dumpStringNOW();
//                     for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
//                     {
//                         Points<T,Dim> * point=*iter;
//                         single_tracker->particle_pool->delete_obj ( point );
//                     }
//                     if ( warning )
//                         printf ( "warning\n" );
// 
//                     mhs::mex_dumpStringNOW();
//                 }
// 
//                 printf ( "\n########################################\n" );
// 
//             };
            break;
	    
	    
//             case 6:
//             {
//
// 	        std::size_t max_run=100;
//
//
//                 printf("remove overlapping terminals\n");
//
//
//
//                 class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
//                 class Collision<T,Dim>::Candidates candA ( query_bufferA );
//
//
//
//                 T collision_search_rad=single_tracker->maxscale*2+1;
//
//                 if (tool_params!=NULL)
//                 {
//                     if (mhs::mex_hasParam(tool_params,"collision_search_rad")!=-1)
//                         collision_search_rad=mhs::mex_getParam<T>(tool_params,"collision_search_rad",1)[0];
//
// 		     if (mhs::mex_hasParam(tool_params,"max_run")!=-1)
//                         max_run=mhs::mex_getParam<std::size_t>(tool_params,"max_run",1)[0];
//                 }
//
//                 printf("using collision searchrad: %f\n",collision_search_rad);
//
// 	      printf("\n########################################\n");
//
// 		for (std::size_t run=0;run<max_run;run++)
// 		{
// 		      std::size_t n_points=single_tracker->particle_pool->get_numpts();
// 		      const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
//
//
// 		    single_tracker->clear_path_labels();
//
//
// 		    int path_id=2;
//
// 		    for ( unsigned int i=0; i<n_points; i++ ) {
// 			Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
// 			T plength=0;
// 			if (CTracker<TData,T,Dim>::do_label ( point,path_id,plength))
// 			{
// 			}
// 		    }
// 		    std::list<Points<T,Dim> * > del_pts;
// 		    std::list<Connection<T,Dim> * > del_edges;
//
//
// 		    Collision<T,Dim> &   collision_fun_p=* ( single_tracker->collision_fun );
// 		    OctTreeNode<T,Dim> &		tree			=* ( single_tracker->tree );
//
// 		    bool warning =false;
// 		    for (int dmode=0;dmode<2;dmode++)
// 		    {
//
// 		    for ( unsigned int i=0; i<n_points; i++ ) {
// 			Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
//
// 			if (point._path_id==-1)
// 			  continue;
//
// 			if  (point.is_segment())
// 			    continue;
//
// 			switch (dmode)
// 			{
// 			  case 0:
// 			    break;
// 			    if  (point.is_terminal())
// 			      continue;
// 			  case 1:
// 			    if  (!point.is_terminal())
// 			      continue;
// // 			    {
// // 			      if ((dmode==1)&&(run>0))
// // 			      {
// // 			      warning =true;
// // 			      }
// // 			      continue;
// // 			    }
//
// 			    break;
// 			}
//
//
//
//
// 			T tmp;
//
// 			bool colliding= collision_fun_p.colliding (
// 					    tmp,
// 					    tree,
// 					    point,
// 					    collision_search_rad,
// 					    1,
// 					    &candA,true,
// 					    -9// collect colliding pts
// 					);
//
// 			class OctPoints<T,Dim> ** candidates=NULL;
//
// 			candidates=candA.p_query_buffer;
// 			std::size_t & found=candA.found;
//
//
// 			std::size_t found_check=0;
//
// 			//T mindist=std::numeric_limits< T >::max();
// 			for ( std::size_t a=0; a<found; a++ ) {
// 			    Points<T,Dim> & coll_point= *(( Points<T,Dim> * ) candidates[a]);
// 			    sta_assert_error((&coll_point)!=(&point));
// 			    if  ((coll_point._path_id!=-1)&&(coll_point._path_id!=point._path_id))
// 			    {
// 				found_check++;
// 			    }
//     // 		  if  (!coll_point.is_segment())
//     // 		      continue;
// 			}
//
// 			if (found_check>0)
// 			{
// 			    point._path_id=-1;
// 			    del_pts.push_back(&point);
//
// 			     if ((dmode==0)&&(run>0))
// 			      {
// 			      warning =true;
// 			      }
// 			}
// 		    }
// 		    }
//
// 		    std::size_t numconnections=single_tracker->connections.pool->get_numpts();
//
// 		    const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
// 		    for ( std::size_t i=0; i<numconnections; i++ ) {
// 			Connection<T,Dim> & con=* ((Connection<T,Dim>* ) conptr[i]);
// 			sta_assert_error(con.pointA!=NULL);
// 			sta_assert_error(con.pointB!=NULL);
//
// 			Points<T,Dim> & pointA=*con.pointA->point;
// 			Points<T,Dim> & pointB=*con.pointB->point;
//     //
// 			if ((pointA._path_id==-1)||(pointB._path_id==-1))
// 			{
// 			    del_edges.push_back(&con);
// 			}
// 		    }
//
// 		    if (del_pts.size()==0)
// 		    {
// 		     run=max_run;
// 		    }
//
// 		    printf("deleting %d edges and ",del_edges.size());
//
// 		    for (typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++)
// 		    {
// 			Connection<T,Dim> * con=*iter;
// 			single_tracker->connections.remove_connection ( con );
// 		    }
//
// 		    mhs::mex_dumpStringNOW();
//
// 		    printf("deleting %d pts\n",del_pts.size());
// 		    for (typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++)
// 		    {
// 			Points<T,Dim> * point=*iter;
// 			single_tracker->particle_pool->delete_obj ( point );
// 		    }
// 		    if (warning)
// 		      printf("warning\n");
//
// 		     mhs::mex_dumpStringNOW();
//
//
// 		}
//
//
// 		printf("\n########################################\n");
//
//             };
//             break;
            case 7:
            {




                printf ( "distorting data\n" );




                T sigma=0.5;

                T max_edge_length=5;

                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"sigma" ) !=-1 )
                        sigma=mhs::mex_getParam<T> ( tool_params,"sigma",1 ) [0];

                    if ( mhs::mex_hasParam ( tool_params,"max_edge_length" ) !=-1 )
                        max_edge_length=mhs::mex_getParam<T> ( tool_params,"max_edge_length",1 ) [0];
                }




                //printf("%d %d %d\n",single_tracker->shape[0],single_tracker->shape[0],single_tracker->shape[0]);





                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        Vector<T,Dim> vec;
                        vec.rand_normal ( sigma );


                        vec=point.get_position() +vec;

                        for ( int a=0; a<3; a++ )
                        {
                            vec[a]=std::max ( vec[a],T ( 0 ) );
                            vec[a]=std::min ( vec[a],T ( single_tracker->shape[a] )-T ( 0.0000001 ) );
                        }

                        point.set_position ( vec );
                    }
                }

                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();


                    std::list<Points<T,Dim> * > del_pts;
                    std::list<Connection<T,Dim> * > del_edges;




                    std::size_t numconnections=single_tracker->connections.pool->get_numpts();

                    const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
                    for ( std::size_t i=0; i<numconnections; i++ )
                    {
                        Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );
                        sta_assert_error ( con.pointA!=NULL );
                        sta_assert_error ( con.pointB!=NULL );

                        Points<T,Dim> & pointA=*con.pointA->point;
                        Points<T,Dim> & pointB=*con.pointB->point;
                        //
                        T dist_c=std::sqrt ( ( *con.pointA->position-*con.pointB->position ).norm2() );
                        if ( dist_c>max_edge_length )
                        {
                            del_edges.push_back ( &con );
                        }
                    }



                    printf ( "deleting %d edges and ",del_edges.size() );
                    mhs::mex_dumpStringNOW();

                    for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
                    {
                        Connection<T,Dim> * con=*iter;
                        single_tracker->connections.remove_connection ( con );
                    }



                    printf ( "deleting %d pts\n",del_pts.size() );
                    mhs::mex_dumpStringNOW();
                    for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
                    {
                        Points<T,Dim> * point=*iter;
                        single_tracker->particle_pool->delete_obj ( point );
                    }


                    mhs::mex_dumpStringNOW();


                }


                printf ( "\n########################################\n" );

            };
            break;

            case 8:
            {




                printf ( "removing all segment edges\n" );


                std::list<Connection<T,Dim> * > del_edges;



                std::size_t numconnections=single_tracker->connections.pool->get_numpts();

                const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
                for ( std::size_t i=0; i<numconnections; i++ )
                {
                    Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );
                    if ( ( con.edge_type==EDGE_TYPES::EDGE_SEGMENT ) )
                    {
                        del_edges.push_back ( &con );
                    }
                }



                printf ( "deleting %d edges and ",del_edges.size() );
                mhs::mex_dumpStringNOW();

                for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
                {
                    Connection<T,Dim> * con=*iter;
                    single_tracker->connections.remove_connection ( con );
                }


                printf ( "\n########################################\n" );

            };
            break;


            case 9:
            {
                printf ( "changing LOD\n" );

                std::vector<T> v_scale;
                v_scale.resize ( 3 );
                v_scale[0]=v_scale[1]=v_scale[2]=1;




                T edge_length_sqr=single_tracker->maxendpointdist;

                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"v_scale" ) !=-1 )
                        v_scale=mhs::mex_getParam<T> ( tool_params,"v_scale",3 );


                    if ( mhs::mex_hasParam ( tool_params,"edge_length" ) !=-1 )
                    {
                        edge_length_sqr=mhs::mex_getParam<T> ( tool_params,"edge_length",1 ) [0];
                        edge_length_sqr*=edge_length_sqr;
                    }
                }



                std::swap ( v_scale[0],v_scale[2] );

                printf ( "max edge length: %f\n",std::sqrt ( edge_length_sqr ) );

                /*
                        if (tool_params!=NULL)
                        {
                            if (mhs::mex_hasParam(tool_params,"edge_length")!=-1)
                                edge_length=mhs::mex_getParam<T>(tool_params,"edge_length",1)[0];
                        }*/

//                 edge_length*=edge_length;

//                 class OctPoints<T,Dim> * query_bufferA[query_buffer_size];
//                 class Collision<T,Dim>::Candidates candA ( query_bufferA );

                Vector<T,3> max_pos;
                max_pos=T ( 0 );

                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        point.point_cost=1;
                        for ( int a=0; a<3; a++ )
                        {
                            max_pos[a]=std::max ( max_pos[a],point.get_position() [a] );
                        }
                    }
                }

                printf ( "[%f %f %f][%f %f %f]\n",max_pos[0],max_pos[1],max_pos[2],v_scale[0],v_scale[1],v_scale[2] );



                bool test_again=true;
                std::size_t run=1;

                std::size_t del_total_pts=0;

                while ( test_again )
                {


                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                    std::list<Points<T,Dim> * > del_pts;
                    bool warning =false;




                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );


                        if ( point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT )
                        {
                            if ( ( point.endpoint_connections[0]>0 ) && ( point.endpoint_connections[1]>0 ) )
                            {

                                class Points<T,Dim>::CEndpoint  * epA=point.endpoints[0][0]->connected;
                                class Points<T,Dim>::CEndpoint  * epB=point.endpoints[1][0]->connected;

// 				sta_assert_error(epA->connected!=NULL);
// 				sta_assert_error(epB->connected!=NULL);
                                sta_assert_error ( epA!=epB );
                                sta_assert_error ( epA->point!=epB->point );

                                if ( ( epA->point->point_cost<=run ) &&
                                        ( epB->point->point_cost<=run ) )
                                {
                                    //T dist= (epA->connected->point->get_position()-epB->connected->point->get_position()).norm2();

                                    Vector<T,3> v;
                                    v= ( epA->point->get_position()-epB->point->get_position() );
                                    v[0]*=v_scale[0];
                                    v[1]*=v_scale[1];
                                    v[2]*=v_scale[2];
                                    T dist= v.norm2();

                                    if ( edge_length_sqr>dist )
                                    {
                                        del_pts.push_back ( &point );
                                        point.point_cost=run+1;
                                    }
                                }
                            }
                        }

                    }

                    printf ( "[%u] deleting %d pts\n",run,del_pts.size() );
                    std::size_t success=0;
                    mhs::mex_dumpStringNOW();
                    for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
                    {
                        Points<T,Dim> * point=*iter;

                        class Points<T,Dim>::CEndpoint  * pointA=point->endpoints[0][0];
                        class Points<T,Dim>::CEndpoint  * pointB=point->endpoints[1][0];

                        class Points<T,Dim>::CEndpoint  * next_pointA=pointA->connected;
                        class Points<T,Dim>::CEndpoint  * next_pointB=pointB->connected;


                        class Connection<T,Dim> edge_back[2];
                        class Connection<T,Dim> * delme;

                        delme=pointA->connection;
                        edge_back[0]=*delme;

                        single_tracker->connections.remove_connection ( delme );


                        delme=pointB->connection;
                        edge_back[1]=*delme;

                        single_tracker->connections.remove_connection ( delme );




                        class Connection<T,Dim> connection_new;
                        connection_new.pointA=next_pointA->point->getfreehub ( next_pointA->side );
                        connection_new.pointB=next_pointB->point->getfreehub ( next_pointB->side );

                        connection_new.edge_type=EDGE_TYPES::EDGE_SEGMENT;

                        sta_assert_error ( connection_new.pointA!=NULL );
                        sta_assert_error ( connection_new.pointB!=NULL );


                        if ( single_tracker->connections.connection_exists ( connection_new ) )
                        {
                            single_tracker->connections.add_connection ( edge_back[0] );
                            single_tracker->connections.add_connection ( edge_back[1] );
                        }
                        else
                        {
                            success++;
                            del_total_pts++;
                            single_tracker->particle_pool->delete_obj ( point );
                            single_tracker->connections.add_connection ( connection_new );

                        }




                    }
                    if ( warning )
                        printf ( "warning\n" );

                    mhs::mex_dumpStringNOW();

                    if ( ( run>100 ) || ( del_pts.size() ==0 ) || ( success==0 ) )
                    {
                        test_again=false;
                    }

// 		     test_again=false;
                    run++;

                }

                printf ( "in total: %u deleted\n",del_total_pts );


                std::size_t numconnections=single_tracker->connections.pool->get_numpts();

                const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
                for ( std::size_t i=0; i<numconnections; i++ )
                {
                    Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );

                    Vector<T,3> v;
                    v= ( con.pointA->point->get_position()-con.pointA->point->get_position() );
                    v[0]*=v_scale[0];
                    v[1]*=v_scale[1];
                    v[2]*=v_scale[2];
                    T dist= v.norm2();

                    sta_assert_error ( ! ( edge_length_sqr<dist ) );
                }

                printf ( "\n########################################\n" );

            };
            break;


            case 10:
            {
		printf ( "helper function for cropping graphs\n" );
		 mhs::mex_dumpStringNOW();
                Vector<T,3> crop_l;
                Vector<T,3> crop_r;

                crop_l=T(0);
                crop_r=std::numeric_limits<T>::max();;


                if (tool_params!=NULL)
                {
                    if (mhs::mex_hasParam(tool_params,"crop_l")!=-1)
                        crop_l=mhs::mex_getParam<T>(tool_params,"crop_l",3);

                    if (mhs::mex_hasParam(tool_params,"crop_r")!=-1)
                        crop_r=mhs::mex_getParam<T>(tool_params,"crop_r",3);
                }

                std::swap(crop_l[0],crop_l[2]);
                std::swap(crop_r[0],crop_r[2]);

                //single_tracker->clear_path_labels();

                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

		printf ( "initializing points \n" ); mhs::mex_dumpStringNOW();
		
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point.delete_me=false;
                    if ((point.get_position().any_smaller(crop_l))||(!point.get_position().all_smaller(crop_r)))
                    {
                        point.delete_me=true;
                    }
                }

                
                printf ( "determining out-of-border points\n" ); mhs::mex_dumpStringNOW();
                bool rerun=true;
                while(rerun)
                {
		    
                    rerun=false;
                    for ( unsigned int i=0; i<n_points; i++ ) {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        if (point.delete_me)
                        {
                            if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
                            {
                                for (int j=0; j<3; j++)
                                {
                                    if (!point.endpoints[Points<T,Dim>::bifurcation_center_slot][j]->connected->point->delete_me)
                                    {
                                        point.endpoints[Points<T,Dim>::bifurcation_center_slot][j]->connected->point->delete_me=true;
                                        rerun=true;
                                    }
                                }
                            }

                            if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)
                            {
                                for (int j=0; j<2; j++)
                                {
                                    if (point.endpoints[j][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
                                    {
                                        if (!point.endpoints[j][0]->connected->point->delete_me)
                                        {
                                            point.endpoints[j][0]->connected->point->delete_me=true;
                                            rerun=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }



                std::list<Points<T,Dim> * > del_pts;
                std::list<Connection<T,Dim> * > del_edges;

                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    if (point.delete_me)
                    {
                        del_pts.push_back(&point);
                    }
                }

                std::size_t numconnections=single_tracker->connections.pool->get_numpts();

                const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
                for ( std::size_t i=0; i<numconnections; i++ ) {
                    Connection<T,Dim> & con=* ((Connection<T,Dim>* ) conptr[i]);

                    //printf("%d %d\n",i,numconnections);
                    sta_assert_error(con.pointA!=NULL);
                    sta_assert_error(con.pointB!=NULL);

                    Points<T,Dim> & pointA=*con.pointA->point;
                    Points<T,Dim> & pointB=*con.pointB->point;
                    //sta_assert_error(pointA.delete_me!=pointB.delete_me);
                    if (pointA.delete_me||pointB.delete_me)
                    {
                        del_edges.push_back(&con);
                    }
                    //connections.remove_connection ( con );

// 		connections.pool->delete_obj(con);
                }


                printf("\n########################################\ndeleting %d edges\n",del_edges.size()); mhs::mex_dumpStringNOW();

                for (typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++)
                {
                    Connection<T,Dim> * con=*iter;
                    single_tracker->connections.remove_connection ( con );
                }


                printf("\n########################################\ndeleting %d pts\n",del_pts.size()); mhs::mex_dumpStringNOW();

                for (typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++)
                {
                    Points<T,Dim> * point=*iter;
                    single_tracker->particle_pool->delete_obj ( point );
                }

                printf("\n########################################\n"); mhs::mex_dumpStringNOW();


// 	    for (typename std::list<Tpathlength>::iterator iter=path_lenths.begin();iter!=path_lenths.end();iter++)
// 	    {
// 	      Tpathlength & pld=*iter;
//
//
// 	    }


            }
            break;


	    case 11:
            {
		printf ( "graph smoother\n" );
		 mhs::mex_dumpStringNOW();

	        T alpha=0.01;
		std::size_t iter=10;
		
		  std::vector<T> v_scale;
                v_scale.resize ( 3 );
                v_scale[0]=v_scale[1]=v_scale[2]=1;






                
		
		
		T weight=0.1;

                if (tool_params!=NULL)
                {
                    if (mhs::mex_hasParam(tool_params,"alpha")!=-1)
                        alpha=mhs::mex_getParam<T>(tool_params,"alpha",1)[0];
		    
		    if (mhs::mex_hasParam(tool_params,"iter")!=-1)
                        iter=mhs::mex_getParam<std::size_t>(tool_params,"iter",1)[0];
		    
		    if (mhs::mex_hasParam(tool_params,"weight")!=-1)
                        weight=mhs::mex_getParam<T>(tool_params,"weight",1)[0];
		    
		    
		  if ( mhs::mex_hasParam ( tool_params,"v_scale" ) !=-1 )
                        v_scale=mhs::mex_getParam<T> ( tool_params,"v_scale",3 );

                }
                
                std::swap ( v_scale[0],v_scale[2] );
		
		Vector<T,3> vscale(v_scale.data());
		vscale.print();


                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

		Vector<T,3> * pt_pos=new Vector<T,3>[n_points*2];
		Vector<T,3> * pt_dirs=new Vector<T,3>[n_points*2];
		
// 		class Points<T,Dim> ** pt_neigb=new Points<T,Dim> * [n_points*2];
		
		Vector<T,3> * pt_posA=pt_pos;
		Vector<T,3> * pt_posB=pt_pos+n_points;
		Vector<T,3> * pt_dirsA=pt_dirs;
		Vector<T,3> * pt_dirsB=pt_dirs+n_points;
		
		printf ( "initializing point positions\n" ); mhs::mex_dumpStringNOW();
		
		
		Vector<T,Dim> maxcv;
		maxcv=T(0);
                for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
		    point.array_indx=i;
		    
		    pt_posA[i]=point.get_position()*vscale;
		    pt_dirsA[i]=point.get_direction();
		    
		    pt_posB[i]=point.get_position()*vscale;
		    pt_dirsB[i]=point.get_direction();
		    
		    maxcv.max(pt_posA[i]);
		    maxcv.max(pt_posB[i]);
		    
// 		     if (point.is_segment())
// 		    {
// 		      pt_neigb[2*i]=point.endpoints[0][0]->connected->point;
// 		      pt_neigb[2*i+1]=point.endpoints[1][0]->connected->point;
// 		    }else
// 		    {
// 		      pt_neigb[2*i]=NULL;
// 		      pt_neigb[2*i+1]=NULL;
// 		    }
                }
                
                maxcv.print();

                const std::size_t * shape=single_tracker->shape;
		
		 Vector<T,3> shapev;
		 shapev[0]=shape[0];
		 shapev[1]=shape[1];
		 shapev[2]=shape[2];
		 
		 shapev=shapev*vscale;
		 
		std::size_t count=0;
                printf ( "applying the filter\n" ); mhs::mex_dumpStringNOW();
                for (std::size_t j=0;j<iter;j++)
                {
		  
			
		  
			printf ( "iter %d \n",j ); mhs::mex_dumpStringNOW();
			
			#pragma omp parallel for num_threads(omp_get_num_procs())
			for ( unsigned int i=0; i<n_points; i++ ) {
			    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                            if (point.is_segment())
                            {
			      
 			      const Vector<T,3> & pA=pt_posA[point.endpoints[0][0]->connected->point->array_indx];
			      const Vector<T,3> & pB=pt_posA[i];
 			      const Vector<T,3> & pC=pt_posA[point.endpoints[1][0]->connected->point->array_indx];
			      
			      T w1=1/(std::sqrt((pA-pB).norm2())+0.00000001);
			      T w2=1/(std::sqrt((pC-pB).norm2())+0.00000001);
			      
			      w1+=0.01;
			      w2+=0.01;
			      
			   
			      
			      
			      T weight2=std::max(std::max(w1,w2),weight);
			      
// 			      
// 			      const Vector<T,3> dA=point.endpoints[0][0]->connected->point->get_direction();
// 			      const Vector<T,3> dC=point.endpoints[1][0]->connected->point->get_direction();
// 			      const Vector<T,3> pA= pt_neigb[2*i]->get_position();
// 			      const Vector<T,3> pC= pt_neigb[2*i+1]->get_position();
// 			      
// 			      const Vector<T,3> dA=point.endpoints[0][0]->connected->point->get_direction();
// 			      const Vector<T,3> dC=point.endpoints[1][0]->connected->point->get_direction();			      
			      
 			      //pt_posB[i]=(pA-pt_posA[i]*2+pC)*alpha+pt_posA[i];
// 			      pt_dirsB[i]=(dA-pt_dirsA[i]*2+dC)*alpha+pt_dirsA[i];
			      pt_posB[i]=(pA*w1+pB*weight2+pC*w2)/(weight2+w1+w2);
			      //pt_posB[i]=(pA+pB+pC)/(3);
			      
// 			      pt_posB[i]=(pA+pB*2+pC)/(4);
			      
			      if ((pt_posB[i].any_smaller( Vector<T,3>(0,0,0))) || (!pt_posB[i].all_smaller(shapev-TOOL_EPS)))
			      {
				
// 				printf("#########\n");
// 				shapev.print();
// 				pt_posA[i].print();
// 				pt_posB[i].print();
				pt_posB[i]=pt_posA[i];
				
				 count++;
				
			      }
			      //pt_posB[i]=(pA+pt_posA[i]+pC)/3;
			      
// 			      if (count==10)
// 			      {
// 				printf("%f %f %f\n",w1,weight2,w2);
// 				pt_posB[i].print();
// 			      }
			     
			      //pt_dirsB[i]=(dA*0.25-pt_dirsA[i]*0.5+dC*0.25);
			    }
                        }
                        
                        std::swap(pt_posA,pt_posB);
			//std::swap(pt_dirsA,pt_dirsB);
                }


                printf("corrected %d times for pts out of grid\n",count); mhs::mex_dumpStringNOW();
		

                printf("finilizing\n"); mhs::mex_dumpStringNOW();
		for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
		   
		    
		    if (point.is_segment())
		    {
		      point.set_position(pt_posA[i]/vscale);
		       
		      const Vector<T,3> & pA=pt_posA[point.endpoints[0][0]->connected->point->array_indx];
		      const Vector<T,3> & pC=pt_posA[point.endpoints[1][0]->connected->point->array_indx];
		      
		      Vector<T,3> newdir=pA-pC;
		      newdir.normalize();
		      
		      T sim=newdir.dot(point.get_direction());
		      if (sim<0)
		      {
			newdir*=-1;
		      }
		      
		      if (sim>0.001)
		      {
			point.set_direction(newdir);
		      }
		      
		      
		    }
		    
// 		    Vector<T,3> d=pt_dirsA[i];
// 		    d.normalize();
// 		    point.set_direction(d);
                }

// 	    for (typename std::list<Tpathlength>::iterator iter=path_lenths.begin();iter!=path_lenths.end();iter++)
// 	    {
// 	      Tpathlength & pld=*iter;
//
//
// 	    }
	      delete [] pt_pos;
	      delete [] pt_dirs;
// 	      delete [] pt_neigb;

            }
            break;
	    
	    
	    
	    
	    case 12:
            {
		printf ( "graph interpolator\n" );
		 mhs::mex_dumpStringNOW();

		T dist=5;
		
		std::size_t min_particles=3;
		
		  std::vector<T> v_scale;
                v_scale.resize ( 3 );
                v_scale[0]=v_scale[1]=v_scale[2]=1;


                if (tool_params!=NULL)
                {
                    if (mhs::mex_hasParam(tool_params,"dist")!=-1)
                        dist=mhs::mex_getParam<T>(tool_params,"dist",1)[0];
		    
		    if (mhs::mex_hasParam(tool_params,"min_particles")!=-1)
                        min_particles=mhs::mex_getParam<std::size_t>(tool_params,"min_particles",1)[0];
		    
		      if ( mhs::mex_hasParam ( tool_params,"v_scale" ) !=-1 )
                        v_scale=mhs::mex_getParam<T> ( tool_params,"v_scale",3 );
                }
                
                    std::swap ( v_scale[0],v_scale[2] );
		
		Vector<T,3> vscale(v_scale.data());
		vscale.print();
                
		const std::size_t * shape=single_tracker->shape;
		
		 Vector<T,3> shapev;
		 shapev[0]=shape[0];
		 shapev[1]=shape[1];
		 shapev[2]=shape[2];
		 
// 		 shapev=shapev*vscale;

                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
		
		
		//delete_me = here checkpath
		
		 for ( unsigned int i=0; i<n_points; i++ ) {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point.delete_me=false;
                }
                
                std::list<Points<T,Dim> * > path_start_pts;

		
		printf("searching segment starting points\n"); mhs::mex_dumpStringNOW();
		for ( unsigned int i=0; i<n_points; i++ ) {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                       
                            if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
                            {
                                for (int j=0; j<3; j++)
                                {
                                    if (!point.endpoints[Points<T,Dim>::bifurcation_center_slot][j]->connected->point->delete_me)
                                    {
				      
                                           point.endpoints[Points<T,Dim>::bifurcation_center_slot][j]->connected->point->delete_me=true;
					   path_start_pts.push_back(point.endpoints[Points<T,Dim>::bifurcation_center_slot][j]->connected->point);
                                    }
                                }
                            }

                            if ((point.is_terminal())&&(!point.delete_me))
                            {
			      point.delete_me=true;
			      path_start_pts.push_back(&point);
                            }
                 }
                 
                 std::size_t path_num=0;
                 
		 std::size_t interp_array_siz_max=0;
		 std::size_t max_steps=0;
		T * positions=NULL;
		T * sampling=NULL;
		T * deriv=NULL;
		Vector<T,Dim> * new_point_pos=NULL;
		T * new_point_scales=NULL;
		 
                 printf("starting interpolation of %d paths\n",path_start_pts.size()); mhs::mex_dumpStringNOW();
                 for (typename std::list<Points<T,Dim> * >::iterator iter = path_start_pts.begin();iter!=path_start_pts.end();iter++)
		 {
		   
		     
		      
// 		      if (path_num>1)
// 			continue;
			
		      
		   
		      Points<T,Dim> & point=**iter;
		      if (point.delete_me)
		      {
			 path_num++;
			std::list<Points<T,Dim> * > del_pts;
			std::list<Connection<T,Dim> * > del_edges;
			
			typename Points<T,Dim>::CEndpoint * start_ep=NULL;
			typename Points<T,Dim>::CEndpoint * stop_ep=NULL;
			
			T path_length=0;
			
			std::size_t count=0;
			for (int j=0; j<2; j++)
			{
			    if ((point.endpoints[j][0]->connected!=NULL)&&(point.endpoints[j][0]->connected->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER))
			    {
				count++;
				
				
				
				bool track=true;
				
				start_ep=point.endpoints[j][0];
				typename Points<T,Dim>::CEndpoint * current_ep=start_ep;
				
				while (track)
				{
				  
				  typename Points<T,Dim>::CEndpoint * next_ep=current_ep->connected;
				  if ((next_ep!=NULL))
				  {
				    
				    
				    path_length+=std::sqrt((current_ep->point->get_position()-next_ep->point->get_position()).norm2());
				    
				    if ((next_ep->point->is_segment())&&((next_ep->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION)))
				    {
				      
					del_pts.push_back(next_ep->point);
					del_edges.push_back(current_ep->connection);
					
				        current_ep=next_ep->opposite_slots[0];
				      
					
				        //add current_ep and current edge to del list
				    }else
				    {
					current_ep=next_ep;
					track=false;
					del_edges.push_back(current_ep->connection);
					
					sta_assert_error(
					  (current_ep->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
					);
					
// 					sta_assert_error(
// 					  (!current_ep->point->is_segment())
// 					);
					
					sta_assert_error_c(
					  ((current_ep->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION)||(current_ep->point->is_terminal())),
					    printf("%d %d\n",current_ep->point->get_num_connections(),current_ep->point->particle_type)
					);
					//add edge to del list
				    }
				      
				    
				      
				    
				  }else{
				      sta_assert_error(1==0);
				  }
				  
				  if (!track)
				  {
				      stop_ep=current_ep;
				      sta_assert_error(current_ep->point->delete_me);
				      current_ep->point->delete_me=false;
				      point.delete_me=false; 
				  }
				  
				}
				
				
				
			    }
			}
			
			std::size_t steps=std::ceil((path_length+1)/dist); 
			
			if (del_pts.size()>=min_particles)
			{
			  
			    
			
			    std::size_t interp_array_size=(del_pts.size()+2);
			    
			    if (interp_array_size>interp_array_siz_max)
			    {
			      interp_array_siz_max=interp_array_size;
			      if (positions!=NULL)
			      {
				delete [] positions;
				delete [] sampling;
				delete [] deriv;
			      }
			    
			      positions=new T[interp_array_size*4];
			      
			      sampling=new T[interp_array_size*4];
			      
			      deriv=new T[interp_array_size*4];
			    }
			    
			    
			    
			    std::size_t t=0;
			    sta_assert_error(t<interp_array_size);
			    for (int i=0;i<3;i++)
			    {
			      positions[t+i*interp_array_size]=start_ep->point->get_position()[i]*vscale[i];
			      sampling[t+i*interp_array_size]=0;
			    }
			    positions[t+3*interp_array_size]=start_ep->point->get_scale();
			    sampling[t+3*interp_array_size]=0;
			    
			    t++;
			    
			    for (typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++)
			    {
				Points<T,Dim> * point=*iter;
				sta_assert_error(t<interp_array_size);
				for (int i=0;i<3;i++)
				{
				  positions[t+i*interp_array_size]=point->get_position()[i]*vscale[i];
				}
				positions[t+3*interp_array_size]=point->get_scale();
				
				for (int i=0;i<3;i++)
				{
				  T weight=dist*dist+1;
				  for (int j=0;j<3;j++)
				  {
				    if (j!=i)
				    {
				      T tmp=(positions[t+j*interp_array_size-1]-positions[t+j*interp_array_size]);
				      weight+=tmp*tmp;
				    }
				  }
				  sampling[t+i*interp_array_size]=sampling[t+i*interp_array_size-1]+std::sqrt(weight);
				}
				
				{
				  T weight=dist*dist+1;
				  for (int j=0;j<3;j++)
				  {
				      T tmp=(positions[t+j*interp_array_size-1]-positions[t+j*interp_array_size]);
				      weight+=tmp*tmp;
				  }
				 sampling[t+3*interp_array_size]=sampling[t+3*interp_array_size-1]+std::sqrt(weight);
				}
				

				t++;
			    }
			    
			    sta_assert_error(t<interp_array_size);
			    for (int i=0;i<3;i++)
			    {
			      positions[t+i*interp_array_size]=stop_ep->point->get_position()[i]*vscale[i];
			    }
			    positions[t+3*interp_array_size]=stop_ep->point->get_scale();
			    
			    
			    for (int i=0;i<3;i++)
			    {
			       T weight=dist*dist+1;
				  for (int j=0;j<3;j++)
				  {
				    if (j!=i)
				    {
				      T tmp=(positions[t+j*interp_array_size-1]-positions[t+j*interp_array_size]);
				      weight+=tmp*tmp;
				    }
				  }
				  sampling[t+i*interp_array_size]=sampling[t+i*interp_array_size-1]+std::sqrt(weight);
			    }
			    {
				  T weight=dist*dist+1;
				  for (int j=0;j<3;j++)
				  {
				      T tmp=(positions[t+j*interp_array_size-1]-positions[t+j*interp_array_size]);
				      weight+=tmp*tmp;
				  }
				 sampling[t+3*interp_array_size]=sampling[t+3*interp_array_size-1]+std::sqrt(weight);
			    }
				
			    
			    t++;
			    
			    sta_assert_error(t==interp_array_size);
			    
			    for (std::size_t t=0;t<interp_array_size;t++)
			    {
			      for (int i=0;i<4;i++)
			      {
				sampling[t+i*interp_array_size]/=(sampling[(interp_array_size-1)+i*interp_array_size]+std::numeric_limits< T >::epsilon());
			      }
			    }
			    
// 			    for (std::size_t t=0;t<interp_array_size;t++)
// 			    {
// 			      
// 				printf("%d : %f %f %f | %f %f %f |\n",
// 				       t,
// 					sampling[t+interp_array_size*0],
// 					sampling[t+interp_array_size*1],
// 					sampling[t+interp_array_size*2],
// 					positions[t+interp_array_size*0],
// 					positions[t+interp_array_size*1],
// 					positions[t+interp_array_size*2]
// 				);
// 			    }
			    
			    for (int i=0;i<4;i++)
			    {
				  spline(
				    sampling+i*interp_array_size,
				    positions+i*interp_array_size,
				    interp_array_size,
				    deriv+i*interp_array_size);
			    }
			    
			
		      
			    for (typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++)
			    {
				Connection<T,Dim> * con=*iter;
				sta_assert_error(con->pointA->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
				sta_assert_error(con->pointB->point->particle_type!=PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);
				single_tracker->connections.remove_connection ( con );
			    }
			    for (typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++)
			    {
				Points<T,Dim> * point=*iter;
				sta_assert_error(point->get_num_connections()==0);
				single_tracker->particle_pool->delete_obj ( point );
			    }
			    
			    
			  
			        
			
			  
			  
			  if (steps+1>max_steps) 
			  {
			      max_steps=steps+1;
			      if (new_point_pos!=NULL)
			      {
				delete [] new_point_pos;
				delete [] new_point_scales;
			      }
			       new_point_pos=new Vector<T,Dim> [steps+1];
			       new_point_scales=new T [steps+1];
			  }
			  
			  for (std::size_t t=0;t<=steps;t++)
			  {
			    T x=T(t)/T(steps);
			    Vector<T,3> new_pos;
			    T new_scale;
			    for (int i=0;i<3;i++)
			    {
			      try {
			      
				  splint(
				    sampling+i*interp_array_size,
				    positions+i*interp_array_size,
				    deriv+i*interp_array_size,
				    interp_array_size,
				    x,&new_pos[i]);
			      } catch (...)
			      {
				
				printf("error in path %d\n",path_num);
				
				for (std::size_t t=0;t<interp_array_size;t++)
				{
				  
				    printf("%d : %f %f %f | %f %f %f |\n",
					  t,
					    sampling[t+interp_array_size*0],
					    sampling[t+interp_array_size*1],
					    sampling[t+interp_array_size*2],
					    positions[t+interp_array_size*0],
					    positions[t+interp_array_size*1],
					    positions[t+interp_array_size*2]
				    );
				}
				throw mhs::STAError("interp\n");
			      }
				  
				  new_pos[i]=new_pos[i]/vscale[i];
				  new_pos[i]=std::max(new_pos[i],T(0));
				  //new_pos[i]=std::min(new_pos[i],shapev[i]-std::numeric_limits< T>::epsilon());
				  new_pos[i]=std::min(new_pos[i],shapev[i]-TOOL_EPS);
				  //new_pos[i]=std::min(new_pos[i],shapev[i]-10*std::numeric_limits< T>::epsilon());
				
				  sta_assert_error_c(new_pos[i]<shapev[i],{new_pos.print();shapev.print();printf("i:%d\n",i);}) ;
			     
			    }
			    

			    
			    
			    new_point_pos[t]=new_pos;
			    
			     for (int i=0;i<3;i++)
			     {
			      sta_assert_error(new_point_pos[t][i]<shapev[i]) ;
			     }

			    
			      try {
			      
				  splint(
				    sampling+3*interp_array_size,
				    positions+3*interp_array_size,
				    deriv+3*interp_array_size,
				    interp_array_size,
				    x,&new_scale);
			      } catch (...)
			      {
				
				
				throw mhs::STAError("interp scale\n");
			      }
				  
				  new_scale=std::max(new_scale,T(0));
// 				  new_scale=std::min(new_scale,);
				  
				  
			    new_point_scales[t]=new_scale;
			    
			    
			     
// 			     printf("%d : %f | %f %f %f |\n",
// 				       t,
// 					x,
// 					new_point_pos[t][0],
// 					new_point_pos[t][1],
// 					new_point_pos[t][2]
// 				);
			    
			  }
			  
			  
			  //TODO scale 
			  
			  T s0=start_ep->point->get_scale();
			  T s1=stop_ep->point->get_scale();
			  
			  int pathid=start_ep->point->_path_id;

			  typename Points<T,Dim>::CEndpoint * prev_ep_point=start_ep;
			   class Connection<T,Dim> connection_new;
			  
			  for (std::size_t t=1;t<steps;t++)
			  {
			     Points<T,Dim> * point=single_tracker->particle_pool->create_obj();
			     
			     //lin interp ver
			     T w=T(t)/T(steps);
			     T new_scale=w*s1+(1-w)*s0;
			     
			     //spline ver
			     new_scale=new_point_scales[t];
			     
			     Vector<T,3> new_dir;
			     new_dir=(new_point_pos[t+1]-new_point_pos[t-1])*vscale;
			     new_dir.normalize();
			     
			     for (int i=0;i<3;i++)
			     {
			      sta_assert_error(new_point_pos[t][i]<shapev[i]) ;
			     }
			     
			     point->set_pos_dir_scale(new_point_pos[t],new_dir,new_scale);
			     point->point_cost=0;
			     point->saliency=0;
			     point->_path_id=pathid;
			     
			    
			    connection_new.pointA=prev_ep_point->point->getfreehub ( prev_ep_point->side );
			    connection_new.pointB=point->getfreehub ( 0 );

			    connection_new.edge_type=EDGE_TYPES::EDGE_SEGMENT;
			    
			    sta_assert_error ( connection_new.pointA!=connection_new.pointB);
			    sta_assert_error ( connection_new.pointA!=NULL );
			    sta_assert_error ( connection_new.pointB!=NULL );
			    try {
			    single_tracker->connections.add_connection ( connection_new );
			    } catch (...)
			    {
			      
			     printf("path length : %f , steps : %d\n ",path_length,steps); 
			     throw mhs::STAError("failed adding edge"); 
			    }
			     
			    
			    prev_ep_point=connection_new.pointB->opposite_slots[0];
			    //prev_ep_point=point;
// 			      if (!tree.insert(*point))
// 			      {
// 				  particle_pool.delete_obj(point);
// 		  		  return false;
// 			      }
			  }
			  
			    connection_new.pointA=prev_ep_point->point->getfreehub ( prev_ep_point->side );
			    connection_new.pointB=stop_ep->point->getfreehub ( stop_ep->side );

			    connection_new.edge_type=EDGE_TYPES::EDGE_SEGMENT;
			    
			    sta_assert_error ( connection_new.pointA!=connection_new.pointB);
			    sta_assert_error ( connection_new.pointA!=NULL );
			    sta_assert_error ( connection_new.pointB!=NULL );
			    try {
			    single_tracker->connections.add_connection ( connection_new );
			    } catch (...)
			    {
			      
			     printf("path length : %f , steps : %d\n ",path_length,steps); 
			     throw mhs::STAError("failed adding edge"); 
			    }
			     

			  
			    
			    
// 			    delete [] new_point_pos;
// 			    delete [] positions;
// 			    delete [] sampling;
// 			    delete [] deriv;
			}
			
			
			sta_assert_error(count<2);
			
		      }
		   
		 }
                 
                 if (positions!=NULL)
		{
		  delete [] positions;
		  delete [] sampling;
		  delete [] deriv;
		}
		if (new_point_pos!=NULL)
		{
		  delete [] new_point_pos;
		  delete [] new_point_scales;
		}
                 
                    
                    
                    
                    
                    
                    

            }
            break;
	    
	    case 13:
	    {
                printf ( "masking tracks\n" );
		mhs::mex_dumpStringNOW();

                sta_assert_error(( tool_params!=NULL ))

                mhs::dataArray<TData>  mask;
                std::vector<T> v_scale;
                v_scale.resize ( 3 );
                v_scale[0]=v_scale[1]=v_scale[2]=1;
                
                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"mask" ) !=-1 )
                        mask=mhs::dataArray<TData > ( mhs::mex_getParamPtr(tool_params,"mask" )) ;
                    
                    if ( mhs::mex_hasParam ( tool_params,"v_scale" ) !=-1 )
                    v_scale=mhs::mex_getParam<T> ( tool_params,"v_scale",3 );
                }
                
                std::swap ( v_scale[0],v_scale[2] );
		
		Vector<T,3> vscale(v_scale.data());
		vscale.print();
                
                

                printf ( "\n########################################\n" );
		mhs::mex_dumpStringNOW();
                
           
                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        point.point_cost=1;
                    }
                }
                
		
		std::list<Points<T,Dim> * > del_pts;
                std::list<Connection<T,Dim> * > del_edges;

                std::size_t shape[3];
                std::size_t mask_num_voxel=1;
                for (int i=0;i<3;i++)
                {
                    shape[i]=mask.dim[i];
                    mask_num_voxel*=shape[i];
                }
		
                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                    for ( unsigned int i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        const Vector<T,Dim>  pos=point.get_position()*vscale;
                        int Z=std::floor ( pos[0]+T(0.5) );
                        int Y=std::floor ( pos[1]+T(0.5) );
                        int X=std::floor ( pos[2]+T(0.5) );
                        if ( ( Z>=shape[0] ) || ( Y>=shape[1] ) || ( X>=shape[2] ) ||
                                ( Z<0 ) || ( Y<0 ) || ( X<0 ) ) {
                            point.point_cost=-2;
                            del_pts.push_back ( &point );
                        }else
                        {
                            std::size_t indx=(((Z)*(shape[1])+(Y)) *(shape[2])+(X)) ;
                            sta_assert_error(indx<mask_num_voxel)
                            if (mask.data[indx]<0.5)
                            {
                                point.point_cost=-2;
                                del_pts.push_back ( &point );
                            }
                                //(SUB2IND(X  ,Y  ,Z  ,shape)*stride+indx)*num_alphas,  
                        }
		    }
		}
		
		std::size_t numconnections=single_tracker->connections.pool->get_numpts();
		const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
		for ( std::size_t i=0; i<numconnections; i++ )
		{
		    Connection<T,Dim> & con=* ( ( Connection<T,Dim>* ) conptr[i] );
		    sta_assert_error ( con.pointA!=NULL );
		    sta_assert_error ( con.pointB!=NULL );

		    Points<T,Dim> & pointA=*con.pointA->point;
		    Points<T,Dim> & pointB=*con.pointB->point;
		    //
		    if ( ( pointA.point_cost==-2 ) || ( pointB.point_cost==-2 ) )
		    {
			del_edges.push_back ( &con );
		    }
		}
		



	      printf ( "deleting %d edges and ",del_edges.size() );
	      mhs::mex_dumpStringNOW();

	      for ( typename std::list<Connection<T,Dim> *>::iterator iter=del_edges.begin(); iter!=del_edges.end(); iter++ )
	      {
		  Connection<T,Dim> * con=*iter;
		  single_tracker->connections.remove_connection ( con );
	      }



	      printf ( "deleting %d pts\n",del_pts.size() );
	      mhs::mex_dumpStringNOW();
	      for ( typename std::list<Points<T,Dim> *>::iterator iter=del_pts.begin(); iter!=del_pts.end(); iter++ )
	      {
		  Points<T,Dim> * point=*iter;
		  single_tracker->particle_pool->delete_obj ( point );
	      }
	   

	      mhs::mex_dumpStringNOW();
               

                printf ( "\n########################################\n" );

            };	    
            break;
	    
            
            
            
            
            case 14:
            {
                
                printf("#######################################################################\n");
                printf("#######################################################################\n");
                printf("#######################################################################\n");
                printf("#######################################################################\n");
                printf("#######################################################################\n");
                
                T search_rad=0;
                
                T track_noise=0;
                //std::vector<T> startpos;
                Vector<T,3> query_point;
                if ( tool_params!=NULL )
                {
                    if ( mhs::mex_hasParam ( tool_params,"track_noise" ) !=-1 )
                        track_noise=mhs::mex_getParam<float> ( tool_params,"track_noise",1 ) [0];
                    
                    if ( mhs::mex_hasParam ( tool_params,"search_rad" ) !=-1 )
                        search_rad=mhs::mex_getParam<float> ( tool_params,"search_rad",1 ) [0];
                    
                    if ( mhs::mex_hasParam ( tool_params,"startpos" ) !=-1 )
                        query_point=mhs::mex_getParam<T> ( tool_params,"startpos",3 );
                    
                    
                }
                
                printf("search rad :%f\n",search_rad);
                printf("track noise :%f\n",track_noise);
                query_point.print();

                //sta_assert(startpos.size()==3);
                //printf("startpos :%f %f %f\n",startpos[0],startpos[1],startpos[2]);
                
                //Vector<T,3> query_point(startpos.data());

                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                
                std::size_t my_query_buffer_size = n_points;

                class OctPoints<T,Dim> ** query_bufferA = new  class OctPoints<T,Dim> * [my_query_buffer_size];

                mwSize outdim;
                outdim = n_points;

                plhs[1] = mxCreateNumericArray ( 1,&outdim,mhs::mex_getClassId< T >(),mxREAL );
                T *result = ( T * ) mxGetData ( plhs[1] );


                Points<T,Dim> ** best_cand = new Points<T,Dim> * [n_points];

                T collision_search_rad=search_rad;

                
                std::list<Points<T,Dim> * > query_pointsA;
                std::list<Points<T,Dim> * > query_pointsB;
                
                std::list<Points<T,Dim> * > *  pquery_pointsA;
                std::list<Points<T,Dim> * > *  pquery_pointsB;
                
                pquery_pointsA = & query_pointsA;
                pquery_pointsB = & query_pointsB;
                
                printf ( "using collision searchrad: %f\n",collision_search_rad );

                Collision<T,Dim> &   collision_fun_p=* ( single_tracker->collision_fun );
//                 OctTreeNode<T,Dim> &		tree			=* ( single_tracker->tree );
                
                OctTreeNode<T,Dim> * p_tree =   new OctTreeNode<T,Dim> ( single_tracker->shape,single_tracker->options.opt_grid_spacing,
 					single_tracker->options.opt_particles_per_voxel,3,single_tracker->pointlimit );
                OctTreeNode<T,Dim> &		tree			=* (p_tree);

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point.point_cost = -1;
                    
                      if ( !tree.insert ( point ) ) {
                            point.print();
                            throw mhs::STAError ( "error insering point (PARTICLE_BLOB)\n" );

                        }
                    //point.life = 0;
                }
                
                
                 
                
                {
                    std::size_t found=0;
                    class OctPoints<T,Dim> ** candidates = query_bufferA;
                    try
                    {
                        tree.queryRange (
                        query_point,collision_search_rad,candidates,my_query_buffer_size,found );
                        //    point.get_position(),collision_search_rad,candidates,query_buffer_size,found );
                    }
                    catch ( mhs::STAError & error )
                    {
                        throw error;
                    }

                    
                    for ( std::size_t a=0; a<found; a++ )
                    {
                        Points<T,Dim> & coll_point= * ( ( Points<T,Dim> * ) candidates[a] );

                        if ( coll_point.point_cost<0)
                        {
                            coll_point.point_cost = 0;
                            query_pointsA.push_back(&coll_point);
                        }
                    }   
                }
                
                //printf("starting query with %d points\n",pquery_pointsA->size());
                
                std::size_t tagged = 0;
                
               
                
                std::size_t depth = 0;
                while ((!pquery_pointsA->size()==0) && (depth<1000))
                {
                    
                    printf("starting next query with %d points\n",pquery_pointsA->size());
                    mhs::mex_dumpStringNOW();
                    depth++;
                    pquery_pointsB->clear();
                    typename std::list<Points<T,Dim> * >::iterator iter =pquery_pointsA->begin(); 
                    while (iter!=pquery_pointsA->end())
                    {
                            Points<T,Dim> & point=**iter;
                         
                            //if ( point.point_cost<0)
                            {
                                class OctPoints<T,Dim> ** candidates = query_bufferA;
                                std::size_t found=0;
                                try
                                {
                                    //T track_noise
                                    T collision_search_rad_;
                                    if (track_noise>0)
                                    {
                                        T r = ((T) rand() / (RAND_MAX));
                                        collision_search_rad_ =collision_search_rad*((1-track_noise)+track_noise*r);
                                    }else{
                                        collision_search_rad_ =collision_search_rad;
                                    }
                                    
                                    tree.queryRange (
                                        point.get_position(),collision_search_rad_,candidates,my_query_buffer_size,found );
                                }
                                catch ( mhs::STAError & error )
                                {
                                    throw error;
                                }

                                //printf("found %d\n",found);
                                for ( std::size_t a=0; a<found; a++ )
                                {
                                    Points<T,Dim> & coll_point= * ( ( Points<T,Dim> * ) candidates[a] );

                                    if ( coll_point.point_cost<0)
                                    {
                                        coll_point.point_cost = depth;
                                        pquery_pointsB->push_back(&coll_point);
                                        tagged++;
                                    }
                                }
                            }
                            point.unregister_from_grid(3);
                         iter++;
                    }
                    
                    
                        
                        
                        
                    
                    
                    printf("%d of %d points tagged\n",tagged,n_points);
                    //printf("ended query with %d points\n",pquery_pointsB->size());
                    std::swap(pquery_pointsB,pquery_pointsA);
                }
                
                printf("done\n");
                
                
                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    
                    point.unregister_from_grid(3);
                }
//                 
                delete  p_tree;
                delete [] query_bufferA;
                printf("preparing dist array\n");
                mhs::mex_dumpStringNOW();

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point._point_id=i;
                }

                for ( unsigned int i=0; i<n_points; i++ )
                {
                        result[n_points-i-1]=particle_ptr[i]->point_cost;
                }

            };
            break;
	    
	    
	    
            default:
            {
                printf ( "just " );
            }
            }

            printf ( "merging graphs\n" );



            if ( false )
            {
                std::size_t n_points=single_tracker->particle_pool->get_numpts();
                const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point._path_id=-1;
                }


// 	  class CGraphStat {
// 	    public:
// 	      T length;
// 	      std::size_t num_votes;
// 	      bool removeme;
// 	      CGraphStat ()
// 	      {
// 		removeme=false;
// 		num_votes=0;
// 		length=0;
// 	      }
// 	  };

                //std::list<CGraphStat> segment_lengths;
                //std::list<T> segments;


                std::list<bool> delete_segment;
                int path_id=0;
                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    if ( point._path_id>-1 )
                        continue;

                    std::size_t num_votes;
                    T length;
                    bool is_terminal;
                    std::list<T> segments;
                    do_label_segments ( point,path_id,num_votes,length,is_terminal,false,&segments );

                    if ( point._path_id>-1 )
                    {
                        T max_l=0;
                        for ( typename std::list<T>::iterator iter=segments.begin(); iter!=segments.end(); iter++ )
                        {
                            max_l=std::max ( *iter,max_l );
                        }
// 		printf("%f \n",max_l);
                        //delete_segment.push_back((length<50)&&(is_terminal));
                        delete_segment.push_back ( ( max_l<35 ) );

// // 		if (length>50)
// // 		printf("%f\n",length);
                    }

                }

                printf ( "%u %u\n",path_id,delete_segment.size() );
                sta_assert_error ( path_id==delete_segment.size() );


                std::vector<bool> graph_stats;
                graph_stats.resize ( path_id );

                std::size_t remove_paths=0;
                std::size_t keep_paths=0;
                std::size_t count=0;
                for ( typename std::list<bool> ::iterator iter=delete_segment.begin(); iter!=delete_segment.end(); iter++ )
                {
                    //printf("%d\n",*iter);
                    graph_stats[count++]=*iter;
                    if ( *iter )
                    {
                        remove_paths++;
                    }
                    else
                    {
                        keep_paths++;
                    }
                }

                printf ( "\n\nremoving %u paths, keeping %u paths\n\n\n",remove_paths,keep_paths );

                std::list<Points<T,Dim>  * > r_pts;
                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    int label=point._path_id;
// 	    if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
// 	    {
// 	      bool remove_bifurcation=false;
// 	      for (int a=0;a<3;a++)
// 	      {
// 		b_particle.endpoints[Points< T, Dim >::bifurcation_center_slot][i]->connected->point->particle_type;
//
// 	      }
//
// 	    }else
                    {
                        //if ((label<1)||((graph_stats[label-1])&&(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT)))
                        if ( ( label<1 ) || ( ( graph_stats[label-1] ) ) )
                        {
                            r_pts.push_back ( &point );
                        }
                    }
                }

                printf ( "removing points\n" );


                for ( typename std::list<Points<T,Dim>  * >::iterator iter=r_pts.begin(); iter!=r_pts.end(); iter++ )
                {
                    Points<T,Dim> & point=**iter;
                    for ( int end_id=0; end_id<2; end_id++ )
                    {
                        class Points<T,Dim>::CEndpoint * endpointsA=point.endpoints_[end_id];
                        for ( int i=0; i<Points<T,Dim>::maxconnections; i++ )
                        {
                            if ( endpointsA[i].connected!=NULL )
                            {
                                class Connection<T,Dim> * deletme=endpointsA[i].connection;
                                single_tracker->connections.remove_connection ( deletme );
                            }
                        }
                    }
                    Points<T,Dim> * p_center_point=&point;
                    single_tracker->particle_pool->delete_obj ( p_center_point );
                }

                printf ( "done\n" );

                for ( unsigned int i=0; i<n_points; i++ )
                {
                    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                    point._path_id=-1;
                }

                single_tracker->check_consisty ( true );
            }













// 	{
// 	  std::size_t n_points=single_tracker->particle_pool->get_numpts();
// 	  const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
// 	  for ( unsigned int i=0; i<n_points; i++ ) {
// 	    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
// 	    point._path_id=-1;
// 	  }
//
// 	  int path_id=0;
// 	  for ( unsigned int i=0; i<n_points; i++ ) {
// 	      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
// 	      CTracker<TData,T,Dim>::do_label ( point,path_id );
// 	  }
//
// 	  class CGraphStat {
// 	    public:
// 	      T length;
// 	      T votes[32];
// 	      std::size_t num_votes;
// 	      bool removeme;
// 	      CGraphStat ()
// 	      {
// 		for (int i=0;i<32;i++)
// 		{
// 		  votes[i]=0;
//
// 		}
// 		removeme=false;
// 		num_votes=0;
// 		length=0;
// 	      }
//
// 	      std::size_t get_main_dir(T & value, int & indx)
// 	      {
// 		indx=0;
// 		value=votes[0];
//
// 		for (int i=1;i<32;i++)
// 		{
// 		  if (votes[i]>value)
// 		  {
// 		   indx=i;
// 		   value=votes[i];
// 		  }
// 		}
//
// 		value/=num_votes;
// 		return num_votes;
// 	      }
//
//
// 	  };
// 	  std::vector<Vector<T,3> > sphere;
// 	  sphere.resize(32);
// 	  for (int i=0;i<sphere.size();i++)
// 	  {
// 	   sphere[i][0]=sym_sphere32[i][0];
// 	   sphere[i][1]=sym_sphere32[i][1];
// 	   sphere[i][2]=sym_sphere32[i][2];
// // 	   sphere[i].print();
// // 	   printf("%f\n",sphere[i].norm2());
// 	  }
//
//
// 	  std::vector<CGraphStat > graph_stats;
//
//
//
//
// 	  graph_stats.resize(path_id);
// 	  for ( unsigned int i=0; i<n_points; i++ ) {
// 	      Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
// 	      int label=point._path_id;
// 	      sta_assert_error(label!=0);
//
// 	      if ((point.get_num_connections()>0)&&((point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT))&&(label>0)&&(!(label>graph_stats.size())))
// 	      {
// 		sta_assert_error((point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT));
//
// 		int max_indx=0;
// 		T v=std::abs(point.get_direction().dot(sphere[max_indx]));
// 		T v_max=v;
// 		for (int a=1;a<32;a++)
// 		{
// 		  v=std::abs(point.get_direction().dot(sphere[a]));
// 		  if (v>v_max)
// 		  {
// 		    max_indx=a;
// 		    v_max=v;
// 		  }
// 		}
//
// 		graph_stats[label-1].votes[max_indx]++;
// 		graph_stats[label-1].num_votes++;
// 	      }
// 	  }
//
// 	std::size_t numconnections=single_tracker->connections.pool->get_numpts();
// 	const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
//         for ( std::size_t i=0; i<numconnections; i++ ) {
//             const Connection<T,Dim> * con=conptr[i];
// 	    //printf("%u %u\n",con->pointA->point->particle_type,con->pointB->point->particle_type);
//
//
//             if (( con->pointA->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT )&&
// 	      ( con->pointB->point->particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT )
// 	    )
// 	    {
// 	      Points<T,Dim> & pointA=*con->pointA->point;
// 	      Points<T,Dim> & pointB=*con->pointB->point;
// 	      int label=pointA._path_id;
// // 	      printf("bla\n");
// 	      if ((label>0)&&(!(label>graph_stats.size())))
// 	      {
// 		graph_stats[label-1].length+=std::sqrt((pointA.get_position()-pointB.get_position()).norm2());
//
// 	      }
// 	    }
// 	}
//
//
//
//
// 	  Vector<T,3> artifact_dir(1,0,0);
//
// 	  std::size_t remove_paths=0;
// 	  std::size_t keep_paths=0;
//
// 	  for ( unsigned int i=0; i<graph_stats.size(); i++ ) {
// 	    T value=-1;
// 	    int indx=-1;
// 	    std::size_t num_pts=graph_stats[i].get_main_dir(value,indx);
// 	    sta_assert_error(indx>-1);
// 	    sta_assert_error(indx<32);
// 	    if ((((std::abs(artifact_dir.dot(sphere[indx])))>0.9)&&(graph_stats[i].length<100))||(graph_stats[i].length<50))
// 	    {
//
// 	      graph_stats[i].removeme=true;
// 	      remove_paths++;
// 	    }else
// 	    {
// 	     keep_paths++;
// 	    }
// 	  }
//
// 	  printf("\n\nremoving %u paths, keeping %u paths\n\n\n",remove_paths,keep_paths);
//
//
// 	  std::list<Points<T,Dim>  * > r_pts;
// 	  for ( unsigned int i=0; i<n_points; i++ ) {
// 	    Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
// 	    int label=point._path_id;
// 	    if ((label<1)||(graph_stats[label-1].removeme))
// 	    {
// 	      //point._path_id=-2;
// 	      r_pts.push_back(&point);
// 	    }
// 	  }
//
//
// 	  for (typename std::list<Points<T,Dim>  * >::iterator iter=r_pts.begin();iter!=r_pts.end();iter++)
// 	  {
// 		Points<T,Dim> & point=**iter;
// 		for ( int end_id=0; end_id<2; end_id++ ) {
//                 class Points<T,Dim>::CEndpoint * endpointsA=point.endpoints_[end_id];
// 		for ( int i=0; i<Points<T,Dim>::maxconnections; i++ ) {
// 			if ( endpointsA[i].connected!=NULL ) {
// 			  class Connection<T,Dim> * deletme=endpointsA[i].connection;
// 			  single_tracker->connections.remove_connection(deletme);
// 			}
// 		    }
// 		}
// 		Points<T,Dim> * p_center_point=&point;
// 		single_tracker->particle_pool->delete_obj(p_center_point);
// 	  }
// 	}
//  single_tracker->check_consisty();


            std::vector<std::size_t> sub_regions;
            sub_regions.resize ( 3 );
            sub_regions[0]=sub_regions[1]=sub_regions[2]=1;

            if ( tool_params!=NULL )
            {
                if ( mhs::mex_hasParam ( tool_params,"sub_region" ) !=-1 )
                {
                    sub_regions=mhs::mex_getParam<std::size_t> ( tool_params,"sub_region",3 );
                }
            }
            
            std::swap(sub_regions[0],sub_regions[2]);

            std::size_t num_sub_regions=sub_regions[0]*sub_regions[1]*sub_regions[2];

            if ( num_sub_regions==1 )
            {
                plhs[0]=single_tracker->save_tracker_state();
                mhs::copyField ( tracker_state,plhs[0],"cshape" );
                mhs::copyField ( tracker_state,plhs[0],"scales" );
                mhs::copyToField ( params,plhs[0],"options" );
            }
            else
            {

                std::size_t num_workers=num_sub_regions;


                tracker= new CTracker<TData,T,Dim>*[num_workers];
                grid_helper_data_fun= new CDataGrid<T,TData,Dim>*[num_workers];


                const std::size_t * shape=single_tracker->shape;

                std::size_t sub_particles[sub_regions[0]][sub_regions[1]][sub_regions[2]];
                std::size_t tracker_id[sub_regions[0]][sub_regions[1]][sub_regions[2]];
                std::size_t count=0;
                sta_assert_error ( sub_regions[0]>0 );
                sta_assert_error ( sub_regions[1]>0 );
                sta_assert_error ( sub_regions[2]>0 );
                for ( std::size_t x=0; x<sub_regions[0]; x++ )
                {
                    for ( std::size_t y=0; y<sub_regions[1]; y++ )
                    {
                        for ( std::size_t z=0; z<sub_regions[2]; z++ )
                        {
                            sub_particles[x][y][z]=0;
                            tracker_id[x][y][z]=count;
                            count++;
                        }
                    }
                }

//                 printf("shape: %u %u %u\n",
// 		       shape[0],
// 		       shape[1],
// 		       shape[2]
// 		      );

                printf ( "%f %f %f | %u %u %u\n",
                         std::floor ( ( shape[0] ) / ( sub_regions[0] ) ),
                         std::floor ( ( shape[1] ) / ( sub_regions[1] ) ),
                         std::floor ( ( shape[2] ) / ( sub_regions[2] ) ),
                         shape[0],
                         shape[1],
                         shape[2]
                       );

                std::vector<std::size_t> pt_worker_mapping;

                {
                    std::size_t n_points=single_tracker->particle_pool->get_numpts();
                    pt_worker_mapping.resize ( n_points );

                    const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                    for ( std::size_t i=0; i<n_points; i++ )
                    {
                        Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                        Vector<T,Dim> pos=point.get_position();

//                         std::size_t x=std::floor(pos[0]/(T(shape[0])/T(sub_regions[0])));
//                         std::size_t y=std::floor(pos[1]/(T(shape[1])/T(sub_regions[1])));
//                         std::size_t z=std::floor(pos[2]/(T(shape[2])/T(sub_regions[2])));

                        std::size_t x=std::floor ( pos[0]/std::floor ( ( shape[0] ) / ( sub_regions[0] ) ) );
                        std::size_t y=std::floor ( pos[1]/std::floor ( ( shape[1] ) / ( sub_regions[1] ) ) );
                        std::size_t z=std::floor ( pos[2]/std::floor ( ( shape[2] ) / ( sub_regions[2] ) ) );

// 			if (pos[1]>(shape[1]/2))
// 			{
// 			 pos.print();
// 			 printf("%u %u %u\n",x,y,z);
// 			}

                        if ( x==sub_regions[0] )
                        {
                            x--;
                        };
                        if ( y==sub_regions[1] )
                        {
                            y--;
                        };
                        if ( z==sub_regions[2] )
                        {
                            z--;
                        };

                        sta_assert_error ( ( ! ( x<0 ) ) && ( ! ( y<0 ) ) && ( ! ( z<0 ) ) );
                        sta_assert_error ( ( ( x<sub_regions[0] ) ) && ( ( y<sub_regions[1] ) ) && ( ( z<sub_regions[2] ) ) );

                        sub_particles[x][y][z]++;

                        pt_worker_mapping[i]=tracker_id[x][y][z];
                        point._point_id=i;//tracker_id[x][y][z];
                    }
                }


                count=0;
                for ( std::size_t x=0; x<sub_regions[0]; x++ )
                {
                    for ( std::size_t y=0; y<sub_regions[1]; y++ )
                    {
                        for ( std::size_t z=0; z<sub_regions[2]; z++ )
                        {
                            printf ( "%d %d %d | %u : %u\n",x,y,z,tracker_id[x][y][z],sub_particles[x][y][z] );
                            count+=sub_particles[x][y][z];
                        }
                    }
                }
                printf ( "total pts  %u : %u\n",single_tracker->particle_pool->get_numpts(),count );

                std::size_t w_shape_s[num_workers][3];
                std::size_t w_offset_s[num_workers][3];

                if ( true )
                {
                    for ( std::size_t x=0; x<sub_regions[0]; x++ )
                        for ( std::size_t y=0; y<sub_regions[1]; y++ )
                            for ( std::size_t z=0; z<sub_regions[2]; z++ )
                            {
                                std::size_t t=tracker_id[x][y][z];
                                sta_assert_error ( t<num_workers );
                                grid_helper_data_fun[t]= new CDataGrid<T,TData,Dim> ( data_fun );


                                std::size_t w_shape[3];
                                std::size_t w_offset[3];
                                std::size_t fill[3];

                                for ( int i=0; i<3; i++ )
                                {
                                    w_shape[i]=std::floor ( ( shape[i] ) / ( sub_regions[i] ) );
                                    fill[i]=shape[i]-std::floor ( ( shape[i] ) / ( sub_regions[i] ) ) *sub_regions[i];
                                }

                                w_offset[0]=w_shape[0]*x;
                                w_offset[1]=w_shape[1]*y;
                                w_offset[2]=w_shape[2]*z;

                                if ( x+1==sub_regions[0] )
                                {
                                    w_shape[0]=w_shape[0]+fill[0];
                                }
                                if ( y+1==sub_regions[1] )
                                {
                                    w_shape[1]=w_shape[1]+fill[1];
                                }
                                if ( z+1==sub_regions[2] )
                                {
                                    w_shape[2]=w_shape[2]+fill[2];
                                }

                                for ( int i=0; i<3; i++ )
                                {
                                    w_shape_s[t][i]=w_shape[i];
                                    w_offset_s[t][i]=w_offset[i];
                                }

                                printf ( "offset: %u %u %u | shape: %u %u %u\n",w_shape[0],w_shape[1],w_shape[2],w_offset[0],w_offset[1],w_offset[2] );
                                grid_helper_data_fun[t]->set_region ( w_offset,w_shape );


                                //std::size_t num_pts=std::max(sub_particles[x][y][z],std::size_t(1000));
                                std::size_t num_pts=sub_particles[x][y][z];

                                printf ( "initializing tracker %u with a pool of size %u\n",t,num_pts );
                                tracker[t]= new CTracker<TData,T,Dim> ( *grid_helper_data_fun[t],
                                                                        edgecost_fun_p,collision_fun_p,num_pts );
                                printf ( "done\n" );
                            }
                    mhs::mex_dumpStringNOW();


                    bool no_error=true;
                    printf ( "->tracker init\n" );
                    mhs::mex_dumpStringNOW();

                    //#pragma omp parallel for num_threads(num_workers)
                    for ( int t=0; t<num_workers; t++ )
                    {
                        try
                        {
                            tracker[t]->init_tracker ( feature_struct, NULL,params );
                        }
                        catch ( mhs::STAError & error )
                        {
                            no_error=false;
                            printf ( "error in initing worker %d: %s\n",t,error.str().c_str() );
                        }
                    }
                    mhs::mex_dumpStringNOW();
                    if ( !no_error )
                        throw ( mhs::STAError ( "exiting" ) );


                    printf ( "->distribute pts to workers\n" );
                    std::vector<Points<T,Dim> * > pt_pts;
                    //pt_worker_mapping

                    mhs::mex_dumpStringNOW();
                    {
                        std::size_t n_points=single_tracker->particle_pool->get_numpts();
                        pt_pts.resize ( n_points );

                        const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();

                        for ( std::size_t i=0; i<n_points; i++ )
                        {
                            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[i] );
                            pt_pts[i]=NULL;

                            // we don't add inter-wokrker connecting bifurcations
                            if ( point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER )
                            {
                                std::size_t id1=pt_worker_mapping[point.endpoints[Points<T,Dim>::bifurcation_center_slot][0]->connected->point->_point_id];
                                std::size_t id2=pt_worker_mapping[point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->connected->point->_point_id];
                                std::size_t id3=pt_worker_mapping[point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->connected->point->_point_id];
				
//       sta_assert_error((
// 		point.endpoints[Points<T,Dim>::bifurcation_center_slot][2]->point!=
// 		  point.endpoints[Points<T,Dim>::bifurcation_center_slot][1]->point
// 			));
				
                                if ( ( id1!=id2 ) || ( id1!=id3 ) || ( id2!=id3 ) )
                                {
                                    continue;
                                }
                            }
                            
                            


                            Vector<T,Dim> pos=point.get_position();


                            //std::size_t t=point._point_id;
                            sta_assert_error ( point._point_id==i );
                            std::size_t t=pt_worker_mapping[i];

                            Points<T,Dim> * ppoint=tracker[t]->particle_pool->create_obj();
                            pt_pts[i]=ppoint;
                            Points<T,Dim> & cpoint=*ppoint;

                            pos[0]-=w_offset_s[t][0];
                            pos[1]-=w_offset_s[t][1];
                            pos[2]-=w_offset_s[t][2];

                            cpoint.set_pos_dir_scale (
                                pos,
                                point.get_direction(),
                                point.get_scale()
                            );

                            cpoint.tracker_birth=point.tracker_birth;

                            cpoint.point_cost=point.point_cost;
                            cpoint.saliency=point.saliency;

                            cpoint.freeze_topology=point.freeze_topology;
                            cpoint.freezed=point.freezed;
                            cpoint.freezed_endpoints[0]=point.freezed_endpoints[0];
                            cpoint.freezed_endpoints[1]=point.freezed_endpoints[1];

                            cpoint.particle_type=PARTICLE_TYPES::PARTICLE_SEGMENT;

                            sta_assert_error ( ( tracker[t]->tree->insert ( *ppoint ) ) );
                        }
                    }

                    printf ( "->creating edges \n" );
                    mhs::mex_dumpStringNOW();
                    {
                        std::size_t n_points=single_tracker->particle_pool->get_numpts();

                        const class Points<T,Dim> ** particle_ptr=single_tracker->particle_pool->getMem();
                        for ( std::size_t j=0; j<n_points; j++ )
                        {
                            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[j] );
                            sta_assert_error ( point._point_id==j );
                        }

                        for ( std::size_t j=0; j<n_points; j++ )
                        {
                            Points<T,Dim> & point=* ( ( Points<T,Dim>* ) particle_ptr[j] );

                            if ( pt_pts[j]==NULL )
                            {
                                continue;
                            }

                            for ( int end_id=0; end_id<2; end_id++ )
                            {
                                class Points<T,Dim>::CEndpoint * endpointsA=point.endpoints_[end_id];

                                for ( int i=0; i<Points<T,Dim>::maxconnections; i++ )
                                {
                                    if ( endpointsA[i].connected!=NULL )
                                    {
                                        class Connection<T,Dim> * connection_old=endpointsA[i].connection;

                                        //if (connection_old->edge_type==EDGE_TYPES::EDGE_SEGMENT)
                                        {
                                            class Connection<T,Dim> connection_new;

                                            class Points<T,Dim> * pointA= connection_old->pointA->point;
                                            class Points<T,Dim> * pointB= connection_old->pointB->point;

                                            sta_assert_error ( pointA->_point_id<n_points );
                                            sta_assert_error ( pointB->_point_id<n_points );
                                            sta_assert_error ( pointA->_point_id>=0 );
                                            sta_assert_error ( pointB->_point_id>=0 );

                                            // only add edges ones
                                            //if (pointA->_point_id>pointB->_point_id)
                                            if ( pointA==&point )
                                            {
                                                // not part a bifurcation point that has not been added due to inter-workeer connectivity
                                                if ( ( pt_pts[pointA->_point_id]!=NULL ) && ( pt_pts[pointB->_point_id]!=NULL ) )
                                                {
                                                    // both points in same worker --> add egde
                                                    if ( pt_worker_mapping[pointA->_point_id]==pt_worker_mapping[pointB->_point_id] )
                                                    {

                                                        sta_assert_error ( pt_worker_mapping[pointA->_point_id]<num_workers );

                                                        class Points<T,Dim> * new_pointA= pt_pts[pointA->_point_id];
                                                        class Points<T,Dim> * new_pointB= pt_pts[pointB->_point_id];

                                                        connection_new.pointA=new_pointA->getfreehub ( connection_old->pointA->side );
                                                        connection_new.pointB=new_pointB->getfreehub ( connection_old->pointB->side );

                                                        connection_new.edge_type=connection_old->edge_type;
                                                        connection_new.freeze_topology=connection_old->freeze_topology;

                                                        sta_assert_error ( connection_new.pointA!=NULL );
                                                        sta_assert_error ( connection_new.pointB!=NULL );
                                                        /* printf("adding [%u %u][%d %d] %u %u\n",j,n_points,end_id,i,pointA->_point_id,pointB->_point_id);
                                                         mhs::mex_dumpStringNOW();	*/
                                                        tracker[pt_worker_mapping[pointA->_point_id]]->connections.add_connection ( connection_new );
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
// 		std::size_t numconnections=single_tracker->connections.pool->get_numpts();
//
//                 const Connection<T,Dim> ** conptr=single_tracker->connections.pool->getMem();
//                 for ( std::size_t i=0; i<numconnections; i++ ) {
//                     Connection<T,Dim> & con=* ((Connection<T,Dim>* ) conptr[i]);
//
//                     sta_assert_error(con.pointA!=NULL);
//                     sta_assert_error(con.pointB!=NULL);
//
//                     Points<T,Dim> & pointA=*con.pointA->point;
//                     Points<T,Dim> & pointB=*con.pointB->point;
//
//                     if(pointA._point_id==pointB._point_id)
// 		    {
//
// 		    }
//                 }



                    printf ( "->tracker state to array\n" );
                    mhs::mex_dumpStringNOW();
                    {
                        mwSize dim=1;
                        mwSize dims=1;
                        mwSize nfields=1;

                        const char *field_names[] =
                        {
                            "W",			// single tracker states
                        };

                        printf ( "->init main struct\n" );
                        mhs::mex_dumpStringNOW();

                        plhs[0]=mxCreateStructArray ( dim, &dims, nfields,field_names );
                        mxArray * worker_state=mxGetField ( plhs[0],0, ( char * ) ( "W" ) );
                        mhs::copyField ( tracker_state,plhs[0],"cshape" );
                        mhs::copyField ( tracker_state,plhs[0],"scales" );
                        mhs::copyToField ( params,plhs[0],"options" );

// 		  mhs::copyField(feature_struct,plhs[0],"cshape");
// 		  mhs::copyField(feature_struct,plhs[0],"scales");
// 		  mhs::copyToField(params,plhs[0],"options");

                        dim=1;
                        dims=num_workers;
                        nfields=3;
                        const char *field_namesW[] =
                        {
                            "A",			// status
                            "shape",			// status
                            "offset",			// status
                        };

                        printf ( "->init worker structs\n" );
                        mhs::mex_dumpStringNOW();
                        mxArray * settings=mxCreateStructArray ( dim, &dims, nfields,field_namesW );
                        mxSetField ( plhs[0],0, ( char * ) ( "W" ),settings );
                        settings=mxGetField ( plhs[0],0, ( char * ) ( "W" ) );



                        mwSize outdim[2];
                        outdim[0]=3;
                        outdim[1]=1;
                        mxArray * tmp = mxCreateNumericArray ( 2,outdim,mhs::mex_getClassId< T >(),mxREAL );
                        T *result = ( T * ) mxGetData ( tmp );
                        for ( int t=0; t<num_workers; t++ )
                        {
                            printf ( "->saving worker %d\n",t );
                            mhs::mex_dumpStringNOW();


                            mxSetField ( settings, t,"A", tracker[t]->save_tracker_state() );

                            printf ( "->saving worker arrtibutes\n" );
                            mhs::mex_dumpStringNOW();

                            for ( int i=0; i<3; i++ )
                            {
                                result[i]=w_shape_s[t][i];
                            }
//   		    mxSetField(settings, t,"shape", mxDuplicateArray(mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("shape"))));
//   		    mxSetField(settings, t,"offset", mxDuplicateArray(mxGetField(mxGetField(tracker_state,0,(char *)("W")),t,(char *)("offset"))));
                            mxSetField ( settings, t,"shape", mxDuplicateArray ( tmp ) );
                            for ( int i=0; i<3; i++ )
                            {
                                result[i]=w_offset_s[t][i];
                            }
                            mxSetField ( settings, t,"offset", mxDuplicateArray ( tmp ) );
                        }
                    }
                }

            }



        }
        catch ( mhs::STAError & error )
        {
            printf ( "!################################!\n" );
            printf ( "ntracker died!!\n" );
            printf ( "!################################!\n" );
            printf ( "%s \n",error.what() );
        }
        catch ( ... )
        {
            printf ( "ntracker died!!\n" );
            mexErrMsgTxt ( "exeption ?" );
        }

        try
        {
            printf ( ".. cleaning up\n" );
            
//       if (tracker!=NULL)
//       {
// 	  for (int t=0;t<num_workers;t++)
// 	  {
// 	    if ( tracker[t]!=NULL)
// 		delete tracker[t];
// 	    if ( grid_helper_data_fun[t]!=NULL)
// 	  delete grid_helper_data_fun[t];
// 	  }
//
// 	  delete [] tracker;
// 	  delete [] grid_helper_data_fun;
//       }

            if ( new_grid_helper_data_fun!=NULL )
                {
                    printf ( "deleting new_grid_helper_data_fun\n" );
                    mhs::mex_dumpStringNOW();
                    delete new_grid_helper_data_fun;
                }

            if ( single_tracker!=NULL )
            {
                printf ( "deleting single_tracker\n" );
                    mhs::mex_dumpStringNOW();
#ifdef GRID_DEBUG 
grid_verbose = 1;
#endif                        
                delete single_tracker;
#ifdef GRID_DEBUG 
grid_verbose = 0;
#endif                         
            }



            if ( data_fun!=NULL )
                delete data_fun;
            if ( edgecost_fun!=NULL )
                delete edgecost_fun;
            if ( edgecost_fun!=NULL )
                delete collision_fun;

            if ( grid_helper_data_fun!=NULL )
                delete [] grid_helper_data_fun;

            if ( tracker!=NULL )
            {
                printf ( "deleting tracker\n" );
                mhs::mex_dumpStringNOW();
                delete [] tracker;
            }

        }
        catch ( mhs::STAError & error )
        {
            printf ( "error cleaning up!!\n" );
            mexErrMsgTxt ( error.what() );
        }
        catch ( ... )
        {
            printf ( "error cleaning up!!\n" );
            mexErrMsgTxt ( "exeption ?" );
        }
    }
}





void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

#ifdef _DEBUG_CONNECT_LOOP_EXTRA_CHECK
    printf ( "_DEBUG_CONNECT_LOOP_EXTRA_CHECK enabled\n" );
#endif

    set_application_directory();

    printf ( "mexcfunc #params: %d\n",nrhs );
    if ( nrhs<1 )
        mexErrMsgTxt ( "error: nrhs<1\n" );
    if ( !mxIsStruct ( prhs[0] ) )
        mexErrMsgTxt ( "error: expecting feature struct\n" );

//     const mxArray *img=mxGetField(prhs[0],0,(char *)("img"));
//     if (img==NULL){
// 	img=mxGetField(prhs[0],0,(char *)("scales"));
//     }
//
//     if (img==NULL){
//       printf("could not determine data precision\n");
//     }


     _mexFunction<double,float> ( nlhs, plhs,  nrhs, prhs );
//     _mexFunction<float,float> ( nlhs, plhs,  nrhs, prhs );

    
    
//     if (mxGetClassID(img)==mxDOUBLE_CLASS)
//         _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
//     else if (mxGetClassID(img)==mxSINGLE_CLASS)
//         _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
//     if (mxGetClassID(img)==mxDOUBLE_CLASS)
//         _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
//      if (mxGetClassID(img)==mxSINGLE_CLASS)
//         _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
//     else
//         mexErrMsgTxt("error: unsupported data type\n");


}







