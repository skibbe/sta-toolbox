#ifndef PROPOSAL_SCALE_H
#define PROPOSAL_SCALE_H

#include "proposals.h"


template<typename TData,typename T,int Dim> class CProposal;




template<typename TData,typename T,int Dim>
class CScale : public CProposal<TData,T,Dim>
{
protected:
      bool temp_dependent;
   bool sample_scale_temp_dependent;
  
public:
  
       T dynamic_weight(
      std::size_t num_edges,
      std::size_t num_particles,
      std::size_t num_connected_particles,
      std::size_t num_bifurcations,
      std::size_t num_terminals,
      std::size_t num_segments,
      T volume_img,
      T volume_particles
    )
    {
      return CProposal<TData,T,Dim>::dynamic_weight_particle_update(num_edges,
		      num_particles,
		      num_connected_particles,
		      num_bifurcations,
		      num_terminals,
		      num_segments,
		      volume_img,
		      volume_particles);
    }
  
  
    CScale(): CProposal<TData,T,Dim>()
    {
      
      temp_dependent=true;
    }
    

    std::string get_name() {
        return enum2string(PROPOSAL_TYPES::PROPOSAL_SCALE);
    };
    
    
     PROPOSAL_TYPES get_type()
    {
         return (PROPOSAL_TYPES::PROPOSAL_SCALE);
    }


    void set_params(const mxArray * params=NULL)
    {
        sta_assert_error(this->tracker!=NULL);

        if (params==NULL)
            return;
	
		if (mhs::mex_hasParam(params,"temp_dependent")!=-1)
            temp_dependent=mhs::mex_getParam<bool>(params,"temp_dependent",1)[0];
	
	 if (mhs::mex_hasParam(params,"sample_scale_temp_dependent")!=-1)
            sample_scale_temp_dependent=mhs::mex_getParam<bool>(params,"sample_scale_temp_dependent",1)[0];   
    }


    void init(const mxArray * feature_struct)
    {
        sta_assert_error(this->tracker!=NULL);
        if (feature_struct==NULL)
            return;

        try {

        } catch (mhs::STAError error)
        {
            throw error;
        }

    }

    bool propose()
    {
        pool<class Points<T,Dim> > &	particle_pool		=*(this->tracker->particle_pool);
        const std::size_t *		shape			=this->tracker->shape;
        const T & 			maxscale		=this->tracker->maxscale;
        const T & 			minscale		=this->tracker->minscale;
        CData<T,TData,Dim> & 		data_fun		=*(this->tracker->data_fun);
        const T &			temp			=this->tracker->opt_temp;
        OctTreeNode<T,Dim> &		tree			=*(this->tracker->tree);
//         const std::vector<T> &  particle_connection_state_cost_scale	=this->tracker->options.particle_connection_state_cost_scale;
//         const std::vector<T> &  particle_connection_state_cost_offset	=this->tracker->options.particle_connection_state_cost_offset;
        const T &			collision_search_rad	=this->tracker->collision_search_rad;
        const std::size_t & 		nvoxel			=this->tracker->numvoxel;
        const T & 			maxendpointdist		=this->tracker->maxendpointdist;
        CEdgecost<T,Dim>  &		edgecost_fun		=*(this->tracker->edgecost_fun);
        class CTracker<TData,T,Dim>::COptions & options		=this->tracker->options;
	Collision<T,Dim> &   collision_fun_p=*(this->tracker->collision_fun);
	bool  single_scale		=(this->tracker->single_scale);
	
	   this->proposal_called++;
	   
	   if (single_scale)
	     return false;
	
	   if (sample_scale_temp_dependent && (!(temp<maxscale)))
	  {
		return false; 
	  }
	   
        OctPoints<T,Dim> *  cpoint;
        bool empty;

        cpoint=tree.get_rand_point();
        if (cpoint==NULL)
            return false;



     

        Points<T,Dim> &  point= *((Points<T,Dim>*)(cpoint));
	
	if (point.isfreezed())
            {
		return false;
	    }
	
	// do not touch bifurcation centers !
	if (point.particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER)
		 return false;
	
	

//         std::size_t center=(std::floor(point.position[0])*shape[1]
//                             +std::floor(point.position[1]))*shape[2]
//                            +std::floor(point.position[2]);
        /*
          if (return_statistic)
          {
            static_data[center*2*numproposals+8]+=1;
          }
        */


        typename CEdgecost<T,Dim>::EDGECOST_STATE inrange;
        T old_cost=point.e_cost(edgecost_fun,
                               options.connection_bonus_L,
			       options.bifurcation_bonus_L,
                               inrange);
        sta_assert_error_c(inrange==(CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE),printf("[%d]",inrange));

/*{
  int side=-1;
  if (point.endpoint_connections[0]==2) 
    side=0;
  if (point.endpoint_connections[1]==2) 
    side=1;
  if (side>-1)
  {
    int inrange_test[2];
    for (int a=0;a<2;a++)
    {
    point.endpoints_[side][a].connection->compute_cost(
      edgecost_fun,
    options.connection_bonus_L,
    options.bifurcation_bonus_L,
    maxendpointdist,
    inrange_test[a],
    false);	
    if (inrange_test[a]<1)
    {
    printf("BEFORE (%d) %d \n",a,inrange_test[a]);  
    sta_assert_error(inrange_test[a]>0);
    }
    }
  }
}*/	


        T oldenergy_data=point.point_cost;
        T old_scale=point.get_scale();

	T temp_fact_scale=2;
	
	if (temp_dependent)
		//temp_fact_scale=1+std::sqrt(temp);
		temp_fact_scale=1+proposal_temp_trafo(temp,old_scale,T(1));
	
	T scale_lower;
        T scale_upper;
  
	if (sample_scale_temp_dependent)
	{
	  scale_lower=std::max(std::max(minscale,temp),old_scale/temp_fact_scale);
	  scale_upper=std::min(maxscale,old_scale*temp_fact_scale);
	  sta_assert_debug0(scale_lower<scale_upper);
	}else
	{
	  scale_lower=std::max(minscale,old_scale/temp_fact_scale);
	  scale_upper=std::min(maxscale,old_scale*temp_fact_scale);
	}	

       
        T new_scale=myrand(scale_lower,scale_upper);


	T particle_interaction_cost_old=0;
	T particle_interaction_cost_new=0;
	
	//class Collision<T,Dim>::Candidates cand;
	class OctPoints<T,Dim> * query_buffer[query_buffer_size];
	class Collision<T,Dim>::Candidates cand(query_buffer);
	
	if (collision_fun_p.is_soft())    
	{
	  T max_dist=Points<T,Dim>::predict_outer_sphere_rad(new_scale)-Points<T,Dim>::predict_outer_sphere_rad(old_scale);
	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old, tree,point,collision_search_rad+(max_dist>0)*max_dist,temp,&cand,true,max_dist ));
	}
	
	//NOTE: debug collision sorting
	
// 	  class OctPoints<T,Dim> * query_buffer2[query_buffer_size];
// 	  class Collision<T,Dim>::Candidates cand2(query_buffer2);
// 	  T particle_interaction_cost_old2=0;
// 	  sta_assert_error(!collision_fun_p.colliding(particle_interaction_cost_old2, tree,point,collision_search_rad,&cand2,true));
// 	


	
// 	std::size_t id_no_connection=static_cast<std::size_t>(point.get_connection_weight());

//         int connection_state=0;
//         {
//             bool check_connect[2];
//             check_connect[0]=(point.endpoint_connections[0]>0);
//             check_connect[1]=(point.endpoint_connections[1]>0);
//             if (check_connect[0]&&check_connect[1])
//                 connection_state=2;
//             else if (check_connect[0]||check_connect[1])
//                 connection_state=1;
//         }

        //TODO [scheint ok, da unten einbezogen] hier ist was faul!! vermisse weights!!!
        T old_pointcost=point.compute_cost3(temp);////+PointScaleCost*point.compute_cost_scale();


       
	
	if (temp_dependent)
		temp_fact_scale=1+proposal_temp_trafo(temp,new_scale,T(1));
		//temp_fact_scale=1+std::sqrt(temp);
	
	
	T scale_lower_new;
	 T scale_upper_new;
	if (sample_scale_temp_dependent)
	{
	  scale_lower_new=std::max(std::max(minscale,temp),new_scale/temp_fact_scale);
	  scale_upper_new=std::min(maxscale,new_scale*temp_fact_scale);
	  sta_assert_debug0(scale_lower_new<scale_upper_new);
	}else
	{
	  scale_lower_new=std::max(minscale,new_scale/temp_fact_scale);
	  scale_upper_new=std::min(maxscale,new_scale*temp_fact_scale);
	}

        T probability=(scale_upper-scale_lower)/((scale_upper_new-scale_lower_new)+std::numeric_limits<T>::epsilon());

        sta_assert_error(probability>=0);



        if (std::abs(new_scale-point.get_scale())<0.01)
        {
            return false;
        }
        
//         T collision_finite_costs=0;
// 	if (Points<T,Dim>::collision_two_steps>0)
// 	{
// 	  sta_assert_error((!colliding( tree,point,collision_search_rad,collision_finite_costs)));
// 	}	
        
        point.set_scale(new_scale);


        // collision cost
// 	if (Points<T,Dim>::collision_two_steps>0)	    
// 	{
// 	  T collision_finite_costs_old=0;
// 	  if (colliding( tree,point,collision_search_rad,collision_finite_costs_old))
// 	  {
// 	    point.scale=old_scale;
// 	    return false;
// 	  }
// 	  collision_finite_costs=collision_finite_costs_old-collision_finite_costs;
// 	}else
	
    bool out_of_bounce=false;
	  bool check_for_bif=point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
	  
	  if (out_of_bounce)
	  {
	    point.set_scale(old_scale);
	    point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
	    sta_assert_debug0(!out_of_bounce);
	    return false;
	  }
	  
	  if (options.bifurcation_particle_hard && check_for_bif)
	  {
	      for (int i=0;i<2;i++) 
	      {
		  if ((point.endpoint_connections[i]==1) && 
		     (point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
		  {
		    sta_assert_debug0(point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);  
		    
		    Points<T,Dim> & bif_center_point=*(point.endpoints[i][0]->connected->point);
		    
		      if (collision_fun_p.colliding( tree,bif_center_point,collision_search_rad,temp))
		      {
			point.set_scale(old_scale);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
			return false;
		      }
		  }
	      }
	  }
	  
	
	/*
	 if (options.bifurcation_particle_hard &&
	    point.bifurcation_center_neighbourhood_update())
	  {
	      for (int i=0;i<2;i++) 
	      {
		  if ((point.endpoint_connections[i]==1) && 
		     (point.endpoints[i][0]->connection->edge_type==EDGE_TYPES::EDGE_BIFURCATION))
		  {
		    sta_assert_debug0(point.endpoints[i][0]->connected->point->particle_type==PARTICLE_TYPES::PARTICLE_BIFURCATION_CENTER);  
		    
		     Points<T,Dim> & bif_center_point=*(point.endpoints[i][0]->connected->point);
		    
		      if ((bif_center_point.position[0]<0)||
		    (bif_center_point.position[1]<0)||
		    (bif_center_point.position[2]<0)||
		    (bif_center_point.position[0]>shape[0]-1)||
		    (bif_center_point.position[1]>shape[1]-1)||
		    (bif_center_point.position[2]>shape[2]-1))
		      {
			point.scale=old_scale;
			point.bifurcation_center_neighbourhood_update();
		      }
		      
		    if (colliding( tree,bif_center_point,collision_search_rad))
		      {
			point.scale=old_scale;
			point.bifurcation_center_neighbourhood_update();
			return false;
		      }
		    
		  }
	      }
	  }
	*/
	
	
	
	
 //NOTE: debug collision sorting
//   {
//     if (collision_fun_p.is_soft())
//     {
//       T eA=0;
//       T eB=0;
//       
//       bool checkA=(collision_fun_p.colliding(eA,tree,point,collision_search_rad,&cand,false));
//       bool checkB=(collision_fun_p.colliding(eB,tree,point,collision_search_rad,&cand2,false));
//     
// 
//       if ((eA!=eB)||(checkA!=checkB))
//       {
// 	printf("%d %d / %f %f / %d %d\n",checkA,checkB,eA,eB,cand.found,cand2.found);
// 	printf("%f %f \n",old_scale,new_scale);
//       }
//       
//       //if ((10+cand.found)!=(cand2.found))
//       if ((cand.found+10)<(cand2.found))
//       {
// 	printf("%d %d\n",cand.found,cand2.found);
//       }
//   
//   
//     }
//   }	
	
	
        if (collision_fun_p.colliding(particle_interaction_cost_new,tree,point,collision_search_rad,temp,&cand,!(collision_fun_p.is_soft())))
        {
	  
            point.set_scale(old_scale);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
            return false;
        }

// sta_assert_error(!std::isnan(old_cost));        
        // edge cost
        T new_cost=point.e_cost(edgecost_fun,
                               options.connection_bonus_L,
			       options.bifurcation_bonus_L,
                               inrange);
// if (std::isnan(new_cost))	
// {
//   
//   printf("%f %f [%d]\n",old_cost,new_cost,(point.particle_type==PARTICLE_TYPES::PARTICLE_SEGMENT));
//   T new_cost=point.edge_energy(edgecost_fun,
//                                options.connection_bonus_L,
//                                maxendpointdist,
//                                inrange,true);
// }
/*sta_assert_error(!std::isnan(new_cost));*/	

        if (inrange!=CEdgecost<T,Dim>::EDGECOST_STATE::EDGECOST_STATE_IN_RANGE)
        {
            point.set_scale(old_scale);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
            return false;
        }
        
// {
//   int side=-1;
//   if (point.endpoint_connections[0]==2) 
//     side=0;
//   if (point.endpoint_connections[1]==2) 
//     side=1;
//   if (side>-1)
//   {
//     int inrange_test[2];
//     for (int a=0;a<2;a++)
//     {
//     point.endpoints_[side][a].connection->compute_cost(
//       edgecost_fun,
//     options.connection_bonus_L,
//     options.bifurcation_bonus_L,
//     maxendpointdist,
//     inrange_test[a],
//     false);	
//     if (inrange_test[a]<1)
//     {
//     printf("(%d) %d \n",a,inrange_test[a]);  
//     
//      point.edge_energy(edgecost_fun,
// 			       options.connection_bonus_L,
// 			      options.bifurcation_bonus_L,
// 				maxendpointdist,
// 			      inrange,true);
//     for (int a=0;a<2;a++)
//     point.endpoints_[side][a].connection->compute_cost(
//       edgecost_fun,
//     options.connection_bonus_L,
//     options.bifurcation_bonus_L,
//     maxendpointdist,
//     inrange_test[a],
//     true);	
//     printf("ranges: %d %d %d\n",inrange,inrange_test[0],inrange_test[1]);
//     sta_assert_error(inrange_test[a]>0);
//     }
//     }
//   }
// }      


        T newenergy_data;
        if (!data_fun.eval_data(
                    newenergy_data,
		    point))
//                     point.get_direction(),
//                     point.get_position(),
//                     point.get_scale()))
        {
            point.set_scale(old_scale);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
            return false;
        }

//         T E=collision_finite_costs/temp;
// sta_assert_error(!std::isnan(E));
        T E=(new_cost-old_cost)/temp;
// sta_assert_error(!std::isnan(E));	
        E+=(newenergy_data-oldenergy_data)/temp;


        //TODO [scheint ok, da unten einbezogen] hier ist was faul!! vermisse weights!!!
//         T new_pointcost=point.compute_cost();////+PointScaleCost*point.compute_cost_scale();

// sta_assert_error(!std::isnan(E));
        
//         E+=particle_connection_state_cost_scale[id_no_connection]*(new_pointcost-old_pointcost)/temp;
	    T new_pointcost=point.compute_cost3(temp);
	    
            E+=(new_pointcost-old_pointcost)/temp;
// 	  E+=particle_connection_state_cost_scale[id_no_connection]*(new_pointcost-old_pointcost)/temp;


// sta_assert_error(!std::isnan(E));
	    
	if (collision_fun_p.is_soft())   
	{
	    E+=(particle_interaction_cost_new-particle_interaction_cost_old)/temp;
	}
	
	

        T R=mhs_fast_math<T>::mexp(E);



        R*=probability;

        if (R>=myrand(1)+std::numeric_limits<T>::epsilon())
        {

            this->tracker->update_energy(E,static_cast<int>(get_type()));
            this->proposal_accepted++;

            point.point_cost=newenergy_data;

            /*
            if (return_statistic)
            {
              static_data[center*2*numproposals+9]+=1;
            }
            */
	    #ifdef _DEBUG_PARTICLE_HISTORY_
	      point.last_successful_proposal=PARTICLE_HISTORY::PARTICLE_HISTORY_SCALE;
	    #endif
	    #ifdef  D_USE_GUI
// 	      (GuiPoints<T>* (&point))->touch(get_type());
	      point.touch(get_type());
	    #endif
	      
	      this->tracker->update_volume(-old_scale);
	      this->tracker->update_volume(new_scale);
	      
	      //point.bifurcation_center_neighbourhood_update();
            return true;
        }

        point.set_scale(old_scale);
			point.bifurcation_center_neighbourhood_update(shape,out_of_bounce);
			sta_assert_debug0(!out_of_bounce);
        return false;
    }
};



#endif


